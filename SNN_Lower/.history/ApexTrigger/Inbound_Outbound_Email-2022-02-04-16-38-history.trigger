trigger Inbound_Outbound_Email on AxtriaSalesIQST__Scheduler_Log__c (after insert) {

	System.debug('@@@ Inbound_Outbound_Email trigger  invoked!!!');
    
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('Inbound_Outbound_Email');

    if (!test.isRunningTest() && myCS1.AxtriaSalesIQTM__IsStopTrigger__c !=true){

    		if(trigger.isUpdate){


    			for (AxtriaSalesIQST__Scheduler_Log__c sc : trigger.new)
    			{

    				if(trigger.newMap.get(sc.ID).AxtriaSalesIQST__Job_Status__c !=null &&
    				 sc.AxtriaSalesIQST__Job_Status__c != trigger.newMap.get(sc.ID).AxtriaSalesIQST__Job_Status__c)

    				{

    					String Job_Status = trigger.newMap.get(sc.ID).AxtriaSalesIQST__Job_Status__c;

    					EmailFuctionality ep = new EmailFuctionality(sc.AxtriaSalesIQST__Job_Name__c,sc.AxtriaSalesIQST__Job_Type__c,Job_Status);
    					ep.sendEmail();
    				}
    			}
    		}

    }

}