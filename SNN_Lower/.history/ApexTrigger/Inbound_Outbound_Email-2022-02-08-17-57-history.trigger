trigger Inbound_Outbound_Email on AxtriaSalesIQST__Scheduler_Log__c (after update) {

	System.debug('@@@ Inbound_Outbound_Email trigger  invoked!!!');
    
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('Inbound_Outbound_Email');

    list<String> mailchecktrue = new list<String>();

    list<AxtriaSalesIQTM__ETL_Config__c> MailSend = [Select id,name,SendMail__c from AxtriaSalesIQTM__ETL_Config__c where SendMail__c = true];

    System.debug('MailSend'+MailSend);
    for (AxtriaSalesIQTM__ETL_Config__c etl : MailSend)
    {
        mailchecktrue.add(etl.Name);

    }
   System.debug('mailchecktrue'+mailchecktrue);
   //
   System.debug('AxtriaSalesIQTM__IsStopTrigger__c'+myCS1.AxtriaSalesIQTM__IsStopTrigger__c);
    if ( !test.isRunningTest() && myCS1.AxtriaSalesIQTM__IsStopTrigger__c !=true){
         System.debug('first if');
    		if(trigger.isUpdate){

     
    			for (AxtriaSalesIQST__Scheduler_Log__c sc : trigger.new)
    			{
 
                System.debug('checkboolean mailchecktrue'+mailchecktrue.contains(sc.AxtriaSalesIQST__Job_Name__c));
                if(mailchecktrue.contains(sc.AxtriaSalesIQST__Job_Name__c)){
    				if(trigger.newMap.get(sc.ID).AxtriaSalesIQST__Job_Status__c !=null &&
    				 sc.AxtriaSalesIQST__Job_Status__c != trigger.oldMap.get(sc.ID).AxtriaSalesIQST__Job_Status__c)

                        System.debug('New Status value'+sc.AxtriaSalesIQST__Job_Status__c);

    				{

    					String Job_Status = sc.AxtriaSalesIQST__Job_Status__c;

    					if(Job_Status == 'Successfull' || Job_Status == 'Successful' )
    					{
    						EmailFuctionality ep = new EmailFuctionality(sc.AxtriaSalesIQST__Job_Name__c,sc.AxtriaSalesIQST__Job_Type__c,'Success');
                            System.debug('Success Mail Triggered');
    					    ep.sendEmail();
    					}

    					else if(Job_Status  == 'Error' )
    					{
    						EmailFuctionality ep = new EmailFuctionality(sc.AxtriaSalesIQST__Job_Name__c,sc.AxtriaSalesIQST__Job_Type__c,Job_Status);
                            System.debug('Error Mail Triggered');
    					    ep.sendEmail();
    					}

    					else if(Job_Status == 'Failed Records')
    					{
    						EmailFuctionality ep = new EmailFuctionality(sc.AxtriaSalesIQST__Job_Name__c,sc.AxtriaSalesIQST__Job_Type__c,Job_Status);
                            System.debug('Failed Records Mail Triggered');
    					    ep.sendEmail();
    					}

    					
    				}
                }
    		}
    	}

    }

}