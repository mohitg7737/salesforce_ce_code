global class EmailFuctionality {

  global string mainContent;
  global List<AxtriaSalesIQST__SIQ_Employee_Master__c> stagingEmpList;
  global String Job_name , Job_type , Job_Status;
  global list<String> toAddresses = new List<String>();
   

global EmailFuctionality(String Jobname,String Jobtype,String Status){

    Job_name = Jobname;
    Job_type = Jobtype;
    Job_Status = Status; 

}

 
      
   global void sendEmail() {
       
      
       /* Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment ();
        //Create CSV file using Blob
        blob csvBlob = Blob.valueOf (mainContent);
        string csvname= 'ErroredRosterData.csv';
        csvAttcmnt.setFileName (csvname);
        csvAttcmnt.setBody (csvBlob);*/

        system.debug('Job_name'+Job_name);
        system.debug('Job_type'+Job_type);
        system.debug('Job_Status'+Job_Status);
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
        String [] toAddresses = new list<string> ();
        
        mailtosend__mdt[] mailtosend = [SELECT MasterLabel,DeveloperName FROM mailtosend__mdt];

         for (mailtosend__mdt email : mailtosend ){
            
            toAddresses.add(email.MasterLabel);
        }

        system.debug('toAddresses'+toAddresses);

        //Set recipient list
        String todayDate = string.valueOf(Date.today());
        singEmail.setToAddresses (toAddresses);
        String subject = '';
        String bodypart = '';
        
        String yesterdayDate = string.valueOf(Date.today()-1);
        if (Job_type == 'Outbound '){
            if(Job_name == 'WinShuttle')
            {
               subject = 'Data Load Status:' + 'WinShuttle Outbound: ' + Job_Status + todayDate;
               bodypart = 'Data load for WinShuttle Outbound' + 'has been processed successfully ';
            }

            else {
                subject = 'Data Load Status:' + Job_name + Job_type + 'To Beacon : ' + Job_Status + todayDate;
                bodypart = 'Data load for ' + Job_name + Job_type + 'to beacon' + 'has been processed successfully ';
            }
        }
        else if (Job_type == 'Inbound'){

            subject = 'Data Load Status:' + Job_name + Job_type + 'From Beacon : ' + Job_Status + todayDate;
            bodypart = 'Data load for ' + Job_name + Job_type + 'from beacon' + 'has been processed successfully in Axtria SalesIQ.';
        }
        String recordCount = string.valueOf(stagingEmpList.size());
        singEmail.setSubject (subject);
        string content = 'Hi, <br/><br/>'+bodypart+'<br/> Thank you, <br/> Axtria SalesIQ Team';
        singEmail.setHtmlBody(content);
            
       
        
        
        //Set blob as CSV file attachment
        /*if(stagingEmpList.size()>0){
            singEmail.setFileAttachments (new Messaging.EmailFileAttachment []{csvAttcmnt});
        }*/
        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
   
}

}