global class EmailFuctionality {

  global string mainContent;
  global List<AxtriaSalesIQST__SIQ_Employee_Master__c> stagingEmpList;
  global String Job_name , Job_type , Job_Status;
  global list<String> toAddresses = new List<String>();
   

global EmailFuctionality(String Jobname,String Jobtype,String Status){

    Job_name = Jobname;
    Job_type = Jobtype;
    Job_Status = Status;

    if (Job_Status == 'Scuccessfull' || Job_Status == 'Scuccessful' || Job_Status == 'Completed'){


     }

     if (Job_Status == 'Error' ){


     }

     if (Job_Status == 'Failed Records' ){


     }
 

}

 
      
   global void sendEmail( String jobname , String jobtype , String staus) {
       
      
       /* Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment ();
        //Create CSV file using Blob
        blob csvBlob = Blob.valueOf (mainContent);
        string csvname= 'ErroredRosterData.csv';
        csvAttcmnt.setFileName (csvname);
        csvAttcmnt.setBody (csvBlob);*/
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
        String [] toAddresses = new list<string> ();
        
        /*EmailRoster__mdt[] emailroster = [SELECT MasterLabel,DeveloperName FROM EmailRoster__mdt];

         for (EmailRoster__mdt email : emailroster ){
            
            toAddresses.add(email.MasterLabel);
        }

        system.debug('toAddresses'+toAddresses);*/
        
        //Set recipient list
        singEmail.setToAddresses (toAddresses);
        String subject = 'DSI Roster Kickout Response';
        singEmail.setSubject (subject);
        String yesterdayDate = string.valueOf(Date.today()-1);
        String recordCount = string.valueOf(stagingEmpList.size());
        string content = 'Hi, <br/><br/>The Workday feed received on  '+yesterdayDate+'has '+recordCount+' records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team';
        if(stagingEmpList.size()>0){
            singEmail.setHtmlBody(content);
            
        }
        else{
            singEmail.setHtmlBody(content);
            //singEmail.setHtmlBody('Hi, <br/><br/>The Workday feed received on yesterday has some records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team');
            //singEmail.setPlainTextBody ('There were no Errored records reported in Last Workday Feed');
        }
        
        
        //Set blob as CSV file attachment
        /*if(stagingEmpList.size()>0){
            singEmail.setFileAttachments (new Messaging.EmailFileAttachment []{csvAttcmnt});
        }*/
        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
   
}

}