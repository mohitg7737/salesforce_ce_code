global class EmailFuctionality {

  global string mainContent;
  global List<AxtriaSalesIQST__SIQ_Employee_Master__c> stagingEmpList;
  global String Job_name , Job_type , Job_Status;
  global list<String> toAddresses = new List<String>();
   

global EmailFuctionality(String Jobname,String Jobtype,String Status){

    Job_name = Jobname;
    Job_type = Jobtype;
    Job_Status = Status; 

}

 
      
   global void sendEmail() {
       

        system.debug('Job_name'+Job_name);
        system.debug('Job_type'+Job_type);
        system.debug('Job_Status'+Job_Status);
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
        String [] toAddresses = new list<string> ();

        //to & CC from ETL// Employee outbound // User territory //Account Territory 
        
        mailtosend__mdt[] mailtosend = [SELECT MasterLabel,DeveloperName FROM mailtosend__mdt];

         for (mailtosend__mdt email : mailtosend ){
            
            toAddresses.add(email.MasterLabel);
        }

        system.debug('toAddresses'+toAddresses);

        //Set recipient list
        String todayDate = string.valueOf(Date.today());
        singEmail.setToAddresses (toAddresses);
        String subject = '';
        String bodypart = '';
        
        String yesterdayDate = string.valueOf(Date.today()-1);
        if (Job_type == 'Outbound'){
            if(Job_name == 'WinShuttle')
            {
               subject = 'Data Load Status:' + ' WinShuttle Outbound: ' + Job_Status + ' '+todayDate;

               if (Job_Status == 'Success')
               {
               bodypart = 'Data load for WinShuttle Outbound' + ' has been processed successfully ';
               }
               else if (Job_Status == 'Error')
               {
                bodypart = 'Data load for WinShuttle Outbound' + ' has been Errored Out, kindly contact Axtria admin';
               }
               else if (Job_Status == 'Failed Records')
               {
                bodypart = 'Data load for WinShuttle Outbound' + ' has Failed Records, kindly contact Axtria admin';
               }
            }

            else {
                subject = 'Data Load Status:' + Job_name +' '+ Job_type + ' To Beacon : ' + Job_Status +' ' +todayDate;

                if (Job_Status == 'Success')
                {
                bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' to beacon ' + 'has been processed successfully ';
                }
                else if (Job_Status == 'Error')
                {
                    bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' to beacon ' + 'has been Errored Out, kindly contact Axtria admin';
                }
                else if (Job_Status == 'Failed Records')
                {
                    bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' to beacon ' + 'has Failed Records, kindly contact Axtria admin';
                }
            }
        }
        else if (Job_type == 'Inbound'){

            subject = 'Data Load Status:' + Job_name +' ' +Job_type + ' From Beacon : ' + Job_Status +' '+ todayDate;

            if (Job_Status == 'Success')
            {
            bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' from beacon ' + 'has been processed successfully in Axtria SalesIQ.';
            }
            else if (Job_Status == 'Error')
            {
                bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' from beacon ' + 'has been Errored Out, kindly contact Axtria admin';
            }
            else if (Job_Status == 'Failed Records')
            {
                bodypart = 'Data load for ' + Job_name +' '+ Job_type + ' from beacon ' + 'has Failed Records,, kindly contact Axtria admin';
            }
        }
        singEmail.setSubject (subject);
        string content = 'Hi, <br/><br/>'+bodypart+'<br/><br/> Thank you, <br/> Axtria SalesIQ Team';
        singEmail.setHtmlBody(content);

        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
   
}

}