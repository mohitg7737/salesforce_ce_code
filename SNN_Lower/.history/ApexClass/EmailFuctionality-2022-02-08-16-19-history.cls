global class EmailFuctionality {

  global string mainContent;
  global List<AxtriaSalesIQST__SIQ_Employee_Master__c> stagingEmpList;
  global String Job_name , Job_type , Job_Status;
  global list<String> toAddresses = new List<String>();
   

global EmailFuctionality(String Jobname,String Jobtype,String Status){

    Job_name = Jobname;
    Job_type = Jobtype;
    Job_Status = Status; 

}

 
      
   global void sendEmail() {
       
      
       /* Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment ();
        //Create CSV file using Blob
        blob csvBlob = Blob.valueOf (mainContent);
        string csvname= 'ErroredRosterData.csv';
        csvAttcmnt.setFileName (csvname);
        csvAttcmnt.setBody (csvBlob);*/
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
        String [] toAddresses = new list<string> ();
        
        mailtosend__mdt[] mailtosend = [SELECT MasterLabel,DeveloperName FROM mailtosend__mdt];

         for (mailtosend__mdt email : mailtosend ){
            
            toAddresses.add(email.MasterLabel);
        }

        system.debug('toAddresses'+toAddresses);

        //Set recipient list
        String todayDate = string.valueOf(Date.today());
        singEmail.setToAddresses (toAddresses);
        String subject = '';
        String bodypart = '';
        singEmail.setSubject (subject);
        String yesterdayDate = string.valueOf(Date.today()-1);
        if (Job_type == 'Outbound '){
            if(Job_name == 'WinShuttle')
            {

            }

            else {

            }
        }
        else if (Job_type == 'Inbound'){


        }
        String recordCount = string.valueOf(stagingEmpList.size());
        string content = 'Hi, <br/><br/>The Workday feed received on  '+yesterdayDate+'has '+recordCount+' records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team';
        if(stagingEmpList.size()>0){
            singEmail.setHtmlBody(content);
            
        }
        else{
            singEmail.setHtmlBody(content);
            //singEmail.setHtmlBody('Hi, <br/><br/>The Workday feed received on yesterday has some records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team');
            //singEmail.setPlainTextBody ('There were no Errored records reported in Last Workday Feed');
        }
        
        
        //Set blob as CSV file attachment
        /*if(stagingEmpList.size()>0){
            singEmail.setFileAttachments (new Messaging.EmailFileAttachment []{csvAttcmnt});
        }*/
        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
   
}

}