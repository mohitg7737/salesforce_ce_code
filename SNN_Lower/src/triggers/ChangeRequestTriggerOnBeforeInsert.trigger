trigger ChangeRequestTriggerOnBeforeInsert on AxtriaSalesIQTM__Change_Request__c (before update, before insert,after update,after insert) {
   
   AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('ChangeRequestTriggerOnBeforeInsert');
   Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ; 


    If(!myCCVal){
        system.debug('+++++++++++++++++Before and Update CR :'+ChangeRequestFetchApprover.isFirstTime);
        System.debug(' ---- after insert : '+ChangeRequestFetchApprover.executeAfterInsert);
       if(trigger.isAfter && trigger.isUpdate)
        {
            system.debug('+++++++++++++++++  In Change Request Before and Update :'+ChangeRequestFetchApprover.isFirstTime);
            if(trigger.new[0].Team_Instance_Name_Formula__c != 'FY2021P9 Ortho'){
            
                ChangeRequestFetchApprover.updateApproverNameProcessBuilder(trigger.new[0].Id); 
                ChangeRequestFetchApprover.isFirstTime = false;
            }
            
        }
        if(trigger.isAfter && trigger.isInsert ){
          
          if(trigger.new[0].Team_Instance_Name_Formula__c != 'FY2021P9 Ortho'){
               ChangeRequestFetchApprover.isFirstTime = false;
              system.debug('+++++++++++++++++  after insert : '+ChangeRequestFetchApprover.isFirstTime);
              //ChangeRequestFetchApprover.updateApproverNameProcessBuilder(trigger.new[0].ID); 
              ChangeRequestFetchApprover.updateApproverName(trigger.new[0].Id); 
          }
          
            
        }
       else if(trigger.isbefore && trigger.isInsert){ 
           if (test.isRunningTest()) {
               system.debug('+++++++++++++++++  In Change Request TEST');
               // ChangeRequestFetchApprover cf = new ChangeRequestFetchApprover();
               if(trigger.new[0].AxtriaSalesIQTM__Request_Type_Change__c != 'Update Overlay Position'){
                   ChangeRequestFetchApprover.FetchApprover(trigger.new); 
            
               }
           } 
           else
           { 
           
               system.debug('+++++++++++++++++  In Change Request ELSE');
               if(trigger.new[0].AxtriaSalesIQTM__Request_Type_Change__c != 'Update Overlay Position'){
                    ChangeRequestFetchApprover.FetchApprover(trigger.new); 
               }          
           }
        }
       
    } 
    
}