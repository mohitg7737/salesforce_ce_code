trigger PODPositionCreation on AxtriaSalesIQTM__Change_Request__c (after Update) {
    
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('PODPositionCreation');
    List<AxtriaSalesIQTM__Position__c> insertPODPositionList = new List<AxtriaSalesIQTM__Position__c>();
    set<id> posid = new set<id>();
    list<AxtriaSalesIQTM__Position_Team_Instance__c> insertpI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
        
    // if(myCS1 == null || myCS1.AxtriaSalesIQTM__IsStopTrigger__c != true)
        if(trigger.isUpdate){
            for(AxtriaSalesIQTM__Change_Request__c cr : trigger.new){
                
                if(cr.TeamType__c == 'SPM' && cr.AxtriaSalesIQTM__Request_Type_Change__c == 'Create Position' && cr.AxtriaSalesIQTM__Status__c == 'Approved'){
                    //posid.add(cr.AxtriaSalesIQTM__Source_Position__c);


                    list<AxtriaSalesIQTM__Position__c> poslist = [select AxtriaSalesIQTM__Parent_Position_Code__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__Parent_Position__c,IsDummyPOD__c,AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__RGB__c,AxtriaSalesIQTM__Position_Type__c,Name,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__IsMaster__c,AxtriaSalesIQTM__Related_Position_Type__c from AxtriaSalesIQTM__Position__c where id =: cr.AxtriaSalesIQTM__Source_Position__c AND AxtriaSalesIQTM__Position_Type__c = 'District'  limit 1];
                   
            

                      String positionCode = generatePositionCode(cr.AxtriaSalesIQTM__Team_Instance_ID__c, 'POD', '2');
                      for(AxtriaSalesIQTM__Position__c p :poslist)
                      {
                            AxtriaSalesIQTM__Position__c new_pos = new AxtriaSalesIQTM__Position__c();
                            new_pos.Name = p.Name;
                            new_pos.AxtriaSalesIQTM__Position_Type__c= 'POD';
                            new_pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
                            new_pos.AxtriaSalesIQTM__Parent_Position__c = p.id;
                            new_pos.IsDummyPOD__c = True;
                            new_pos.AxtriaSalesIQTM__Client_Territory_Code__c = positionCode;
                            new_pos.AxtriaSalesIQTM__Client_Position_Code__c = positionCode;
                            new_pos.AxtriaSalesIQTM__inactive__c = False;
                            new_pos.AxtriaSalesIQTM__RGB__c = p.AxtriaSalesIQTM__RGB__c;
                            new_pos.AxtriaSalesIQTM__Client_Territory_Name__c = p.Name + '('+positionCode+')';
                            new_pos.AxtriaSalesIQTM__Team_iD__c = p.AxtriaSalesIQTM__Team_iD__c;
                            new_pos.AxtriaSalesIQTM__Team_Instance__c = p.AxtriaSalesIQTM__Team_Instance__c;
                            new_pos.AxtriaSalesIQTM__Related_Position_Type__c = p.AxtriaSalesIQTM__Related_Position_Type__c;
                            new_pos.AxtriaSalesIQTM__Effective_Start_Date__c = p.AxtriaSalesIQTM__Effective_Start_Date__c;
                            new_pos.AxtriaSalesIQTM__Effective_End_Date__c  = p.AxtriaSalesIQTM__Effective_End_Date__c;
                            new_pos.AxtriaSalesIQTM__Parent_Position_Code__c = p.AxtriaSalesIQTM__Client_Territory_Code__c;
                            new_pos.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
                            new_pos.AxtriaSalesIQTM__IsMaster__c = p.AxtriaSalesIQTM__IsMaster__c;

                            insertPODPositionList.add(new_pos);
                            System.debug('Position name is  ' +insertPODPositionList);
                      }
                }
                         
            }
            
            if(insertPODPositionList.size() >0){
                  insert insertPODPositionList ;
            }
            
            for(AxtriaSalesIQTM__Position__c pnew : insertPODPositionList )
            {
              AxtriaSalesIQTM__Position_Team_Instance__c pTI = new AxtriaSalesIQTM__Position_Team_Instance__c();
                    pTI.AxtriaSalesIQTM__Team_Instance_ID__c = pnew.AxtriaSalesIQTM__Team_Instance__c;
                    pTI.AxtriaSalesIQTM__Effective_Start_Date__c = pnew.AxtriaSalesIQTM__Effective_Start_Date__c;
                    pTI.AxtriaSalesIQTM__Effective_End_Date__c = pnew.AxtriaSalesIQTM__Effective_End_Date__c;
                    pTI.AxtriaSalesIQTM__Position_ID__c = pnew.Id;
                    pTI.AxtriaSalesIQTM__Parent_Position_ID__c = pnew.AxtriaSalesIQTM__Parent_Position__c;
                    insertpI.add(pTI);
            }
            if(insertpI.size() >0){
                insert insertpI;
            }
            
        }


    public static string generatePositionCode(string teamInstanceId, string positionType, string hLevel)
    {   

        
        list<AxtriaSalesIQTM__Team_Instance_Configuration__c> teamInstanceConfigList = [SELECT Id, AxtriaSalesIQTM__Configuration_Type__c, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Pattern_Value__c, AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Team__c, AxtriaSalesIQTM__Team_Instance__c 
                                                                       FROM AxtriaSalesIQTM__Team_Instance_Configuration__c WHERE AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId and AxtriaSalesIQTM__Position_Type__c  = : positionType 
                                                                       AND AxtriaSalesIQTM__Configuration_Type__c = 'Client Position Code'];
        string patternType = 'POS-{0000}';
        if(teamInstanceConfigList.size() > 0){
            for(AxtriaSalesIQTM__Team_Instance_Configuration__c teamInsConfig : teamInstanceConfigList){
                patternType = teamInsConfig.AxtriaSalesIQTM__Pattern_Value__c;
                break;
            }
        }
        string posCodePatternStart;
        string posCodePatternEnd;

        if(patternType.split('\\{').size() > 0){
            posCodePatternStart = patternType.split('\\{')[0];
            posCodePatternEnd = patternType.split('\\{')[1];
        }
        
        string patternFilter = posCodePatternStart + '%';
            
        list<AxtriaSalesIQTM__Position__c> allPositionWithPattern = [SELECT id, AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position__c 
                                                    WHERE AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId AND AxtriaSalesIQTM__Client_Position_Code__c LIKE : patternFilter AND (AxtriaSalesIQTM__Hierarchy_Level__c =: hLevel OR AxtriaSalesIQTM__Hierarchy_Level__c = '0')];
               
        Integer latestPositionCode;
        for(AxtriaSalesIQTM__Position__c pos : allPositionWithPattern)
        {
            string patternIntegerString = pos.AxtriaSalesIQTM__Client_Position_Code__c.remove(posCodePatternStart);
            String regex = '[a-zA-Z]{1,}|\\-';
            String positionCodeNumber = pos.AxtriaSalesIQTM__Client_Position_Code__c.replaceAll(regex, '');

            if(positionCodeNumber.isNumeric())
            {
                if(latestPositionCode == null)
                {
                    latestPositionCode = Integer.valueOf(positionCodeNumber);
                }
                else if(Integer.valueOf(positionCodeNumber) > latestPositionCode)
                {
                    latestPositionCode = Integer.valueOf(positionCodeNumber);
                }
            }
        }

        string newPositionCodeString;

        if(latestPositionCode != null)
        {
            newPositionCodeString = string.valueOf(latestPositionCode+1);
        }
        else
        {
            newPositionCodeString = string.valueOf(1);
        }

        Integer posCodePatterLength = posCodePatternEnd.removeEnd('}').length();
        while(newPositionCodeString.length() != posCodePatterLength && newPositionCodeString.length() < 5)
        {
            newPositionCodeString = '0'+newPositionCodeString;
        }

        return (posCodePatternStart + newPositionCodeString);
    }

}