trigger PhasingGrowthTrigger on Phasing_Growth_Percentage__c (before insert, before update) {
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('PhasingGrowthTrigger');
   Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ; 

   If(!myCCVal){
       if(trigger.isbefore && (trigger.isInsert || trigger.isUpdate) ){
            system.debug('+++++++++++++++++ in if');
            System.debug('Trigger.new--'+Trigger.New);
           	System.debug('Trigger.old--'+Trigger.Old);
           	System.debug('Trigger.newMap--'+Trigger.newMap);
           	System.debug('Trigger.oldMap--'+Trigger.oldMap);
            PhasingGrowthTriggerHandler.fetchNonEditablePeriodError(trigger.newMap,trigger.oldMap,trigger.New);
        }
    }


}