trigger splitAssignmentOnBusinessTitleChange on AxtriaSalesIQTM__Position_Employee__c(before insert, after insert, after update) {
    try{
        Set<Id> posEmpId = new Set<Id>();
        Set<Id> posEmployeeIds = new Set<Id>();
        List<AxtriaSalesIQTM__Position_Employee__c> updatePosEmpList = new List<AxtriaSalesIQTM__Position_Employee__c>();
        List<AxtriaSalesIQTM__Position_Employee__c> insertPosEmpList = new List<AxtriaSalesIQTM__Position_Employee__c>();
        System.debug('splitAssignmentChangeHandler.checkInsertOnce>>>>>>>>>>>'+splitAssignmentChangeHandler.checkInsertOnce);
        System.debug('splitAssignmentChangeHandler.checkUpdateOnce>>>>>>>>>>>'+splitAssignmentChangeHandler.checkUpdateOnce);
        System.debug('is batch>>>>>>>>>>>'+system.isBatch());
        if(!System.isBatch()){
            for(AxtriaSalesIQTM__Position_Employee__c objPosEmployee: Trigger.new){
                    System.debug('objPosEmployee.Business_Title__c>>>>>>>'+objPosEmployee.Business_Title__c);
                    System.debug('objPosEmployee.Plan_Date__c>>>>>>>'+objPosEmployee.Plan_Date__c);
                    //System.debug('objPosEmployee.AxtriaSalesIQTM__Effective_Start_Date__c>>>>>>>'+objPosEmployee.AxtriaSalesIQTM__Effective_Start_Date__c);
                    //System.debug('objPosEmployee.AxtriaSalesIQTM__Effective_End_Date__c>>>>>>>'+objPosEmployee.AxtriaSalesIQTM__Effective_End_Date__c);
                    if(splitAssignmentChangeHandler.checkUpdateOnce == false){
                        if((objPosEmployee.Plan_Date__c != null && objPosEmployee.Business_Title__c == null) || (objPosEmployee.Plan_Date__c == null && objPosEmployee.Business_Title__c != null)){
                            System.debug('1st validation>>>>>>>>>');
                            objPosEmployee.addError('Plan Date and Business Title both should be filled while doing promotion.');
                        }
                    }
                    if(splitAssignmentChangeHandler.checkUpdateOnce == false){
                      if(objPosEmployee.Plan_Date__c != null && (objPosEmployee.Plan_Date__c < objPosEmployee.AxtriaSalesIQTM__Effective_Start_Date__c || objPosEmployee.Plan_Date__c >  objPosEmployee.AxtriaSalesIQTM__Effective_End_Date__c)){
                          System.debug('2nd validation>>>>>>>>>');
                          objPosEmployee.addError('Plan Date should lie between the Assignment Start Date and Assignment End Date.');
                      }
                    }
                    if(Trigger.isInsert && splitAssignmentChangeHandler.checkInsertOnce == false){
                        if(Trigger.isBefore && objPosEmployee.Plan_Date__c == null){
                            objPosEmployee.Plan_Date__c = objPosEmployee.AxtriaSalesIQTM__Effective_Start_Date__c;
                            splitAssignmentChangeHandler.checkUpdateOnce = true;
                        }
                        if(Trigger.isAfter && objPosEmployee.Business_Title__c == null){
                            posEmpId.add(objPosEmployee.Id);
                        }              
                    }
                    //Check change in Business Title Condition
                    if(Trigger.isUpdate && splitAssignmentChangeHandler.checkUpdateOnce == false){
                        if(objPosEmployee.Plan_Date__c >= objPosEmployee.AxtriaSalesIQTM__Effective_Start_Date__c){ // Changed != to >=
                            if(objPosEmployee.Business_Title__c != null && ((Trigger.oldMap.get(objPosEmployee.Id).Business_Title__c != null && Trigger.oldMap.get(objPosEmployee.Id).Business_Title__c != objPosEmployee.Business_Title__c) || (Trigger.oldMap.get(objPosEmployee.Id).Business_Title__c == null)) && objPosEmployee.AxtriaSalesIQTM__Employee__c != null && objPosEmployee.AxtriaSalesIQTM__Position__c != null){
                                if(objPosEmployee.Plan_Date__c != null){
                                    AxtriaSalesIQTM__Position_Employee__c newPosEmp = objPosEmployee.clone();
                                    newPosEmp.AxtriaSalesIQTM__Effective_Start_Date__c = objPosEmployee.Plan_Date__c;
                                    insertPosEmpList.add(newPosEmp);
                                    splitAssignmentChangeHandler.checkInsertOnce = true;
                                    posEmployeeIds.add(objPosEmployee.Id);
                                }
                            }
                        }else{
                            objPosEmployee.addError('Plan Date should be greater than the Assignment Start Date.');
                        }
                    }
            }
    
            if(!posEmpId.isEmpty()){
                for(AxtriaSalesIQTM__Position_Employee__c objPosEmployee: [Select Id,Business_Title__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Business_Title__c from AxtriaSalesIQTM__Position_Employee__c where Id in: posEmpId and AxtriaSalesIQTM__Employee__r.Business_Title__c != null]){
                    objPosEmployee.Business_Title__c = objPosEmployee.AxtriaSalesIQTM__Employee__r.Business_Title__c;
                    updatePosEmpList.add(objPosEmployee);
                    splitAssignmentChangeHandler.checkUpdateOnce = true;
                } 
            }
          
            if(!posEmployeeIds.isEmpty()){
                for(AxtriaSalesIQTM__Position_Employee__c objPosEmployee: [Select Id,AxtriaSalesIQTM__Effective_End_Date__c,Plan_Date__c,Business_Title__c from AxtriaSalesIQTM__Position_Employee__c where Id in: posEmployeeIds]){
                    if(objPosEmployee.Plan_Date__c != null){
                        objPosEmployee.AxtriaSalesIQTM__Effective_End_Date__c = objPosEmployee.Plan_Date__c - 1;
                        objPosEmployee.Business_Title__c = Trigger.oldMap.get(objPosEmployee.Id).Business_Title__c != null ? Trigger.oldMap.get(objPosEmployee.Id).Business_Title__c : '';
                        updatePosEmpList.add(objPosEmployee);
                        splitAssignmentChangeHandler.checkUpdateOnce = true; 
                    }
                } 
            }
    
            System.debug('updatePosEmpList>>>>>>>>>>'+updatePosEmpList);
            System.debug('insertPosEmpList>>>>>>>>>>'+insertPosEmpList);
            if(insertPosEmpList != null && insertPosEmpList.size() > 0)
                Database.insert(insertPosEmpList, false);
    
            if(updatePosEmpList != null && updatePosEmpList.size() > 0)
                Database.update(updatePosEmpList, false);
       }
   }catch(Exception e){
      System.debug('e>>>>>>>>>>'+e.getLineNumber());
   }
}