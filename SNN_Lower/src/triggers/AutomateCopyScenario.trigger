trigger AutomateCopyScenario on AxtriaSalesIQTM__Scenario__c (after update) {
	
    if(trigger.new[0].AxtriaSalesIQTM__Scenario_Stage__c == 'Design' && trigger.new[0].AxtriaSalesIQTM__Scenario_Source__c == 'Existing Scenario' && trigger.new[0].AxtriaSalesIQTM__Request_Process_Stage__c == 'Ready' && trigger.new[0].AxtriaSalesIQTM__Scenario_Status__c == 'Active' && AutomateCopyScenarioHandler.isFirstTime){
		
        AutomateCopyScenarioHandler.isFirstTime = false;
        String newTI = trigger.new[0].AxtriaSalesIQTM__Team_Instance__c;
        String sourceTI = trigger.new[0].AxtriaSalesIQTM__Source_Team_Instance__c;
        String teamID = trigger.new[0].AxtriaSalesIQTM__Team_Name__c;
        
        AutomateCopyScenarioHandler.updateSAPPositionID(sourceTI,newTI);
    }
}