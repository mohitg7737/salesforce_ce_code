@isTest
public class TestPhasingGrowthTrigger{
    
    static testMethod void unitTest1(){
    
        AxtriaSalesIQTM__TriggerContol__c myCS1 = new AxtriaSalesIQTM__TriggerContol__c();
        myCS1.name = 'PhasingGrowthTrigger' ; 
        myCS1.AxtriaSalesIQTM__IsStopTrigger__c = false ; 
        insert myCS1;
        
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

 

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;   
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2022';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Request_Process_Stage__c = 'Ready';
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Status__c = 'Active';
        insert newcreateScenarioObj;    
        
        
        objTeamInstance.AxtriaSalesIQTM__scenario__c = newcreateScenarioObj.id;            
        update objTeamInstance;   
 
        
        
        Phasing_Growth_Percentage__c pgp1 = new Phasing_Growth_Percentage__c();
        pgp1.Name = '2021';
        pgp1.P1_Value__c = 100; 
        pgp1.P2_Value__c = 0;
        pgp1.P3_Value__c = 0;
        pgp1.P4_Value__c = 0;
        pgp1.P5_Value__c = 0;
        pgp1.P6_Value__c = 0;
        pgp1.P7_Value__c = 0;
        pgp1.P8_Value__c = 0;
        pgp1.P9_Value__c = 0;
        pgp1.P10_Value__c = 0;
        pgp1.P11_Value__c = 0;
        pgp1.P12_Value__c = 0;
        
        pgp1.Product_Category__c = 'C1-CAP SALES';
        pgp1.Type__c = 'Phasing %';
        insert pgp1;
        
        pgp1.P1_Value__c = 90; 
        pgp1.P2_Value__c = 10;
        update pgp1;
  
    }
}