@isTest
public class AutomateCopyScenarioTest {
	 public static testMethod void test1()
    {
       
         AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2021';        
        insert workspace;
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert objTeam;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        //newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        //newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Stage__c = 'Design';
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Source__c  = 'Existing Scenario';
        newcreateScenarioObj.AxtriaSalesIQTM__Request_Process_Stage__c = 'Ready';
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Status__c  = 'Active';
        insert newcreateScenarioObj;
        
        
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPMP8';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;
        
        AxtriaSalesIQTM__Team_Instance__c Teaminstance2= new AxtriaSalesIQTM__Team_Instance__c();
        Teaminstance2.name = 'SPM9';
        Teaminstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Future';
        Teaminstance2.AxtriaSalesIQTM__Team__c = objTeam.id;
        Teaminstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        Teaminstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00011'; 
        Teaminstance2.AxtriaSalesIQTM__Scenario__c = newcreateScenarioObj.ID;
        
        insert Teaminstance2;
        
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = Teaminstance2.id;
        update newcreateScenarioObj;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.name = 'New York';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='REP';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();

        pos2.name = 'New York';
        pos2.AxtriaSalesIQTM__Client_Territory_Name__c='New';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c = 'P100';
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='1NE300000';
        pos2.AxtriaSalesIQTM__Position_Type__c='FLSM';
        pos2.AxtriaSalesIQTM__RGB__c = '41,210,118';
        pos2.AxtriaSalesIQTM__inactive__c = false;
        pos2.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos2.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos2.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos2.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos2.axtriasalesIQTM__Team_Instance__C = Teaminstance2.Id;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos2;
        UpdatePositionCopyScenarioBatch obj = new UpdatePositionCopyScenarioBatch(objTeamInstance.Id, Teaminstance2.Id);
        database.executebatch(obj);
        
        
        
    }
}