@isTest
public class BatchUpdateBeforeFlagCRAccTest {
    
    public static testMethod void test1()
    {
        Account acc = new Account();
        acc.AccountNumber='12345';
        acc.Name='ABCDTest';
        acc.AxtriaSalesIQST__Marketing_Code__c='IN';
        acc.AssociatedSold__c ='6789';
        acc.AxtriaSalesIQTM__Active__c='Active';
        insert acc;
        
        Account acc2 = new Account();
        acc2.AccountNumber='6789';
        acc2.Name='SoldTest';
        acc2.AxtriaSalesIQST__Marketing_Code__c='IN';
        acc2.AxtriaSalesIQTM__Active__c='Active';
       
        insert acc2;
        
        /*Account Ship2 = new Account();
        Ship2.AccountNumber='1011';
        Ship2.Name='ABCD';
        Ship2.AxtriaSalesIQST__Marketing_Code__c='IN';
        Ship2.AssociatedSold__c ='1012';
        Ship2.AxtriaSalesIQTM__Active__c='Active';
        insert Ship2;*/
        
        /*Account Sold2 = new Account();
        Sold2.AccountNumber='1012';
        Sold2.Name='test';
        Sold2.AxtriaSalesIQST__Marketing_Code__c='IN';
        Sold2.AxtriaSalesIQTM__Active__c='Active';
        insert Sold2;*/
        
        
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert objTeam;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPMP8';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;
        
        AxtriaSalesIQTM__Team_Instance__c Teaminstance2= new AxtriaSalesIQTM__Team_Instance__c();
        Teaminstance2.name = 'SPM9';
        Teaminstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Future';
        Teaminstance2.AxtriaSalesIQTM__Team__c = objTeam.id;
        Teaminstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        Teaminstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00011'; 
        
        insert Teaminstance2;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.name = 'New York';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='REP';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();

        pos2.name = 'New York';
        pos2.AxtriaSalesIQTM__Client_Territory_Name__c='New';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c = 'P100';
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='1NE300000';
        pos2.AxtriaSalesIQTM__Position_Type__c='FLSM';
        pos2.AxtriaSalesIQTM__RGB__c = '41,210,118';
        pos2.AxtriaSalesIQTM__inactive__c = false;
        pos2.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos2.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos2.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos2.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos2.axtriasalesIQTM__Team_Instance__C = Teaminstance2.Id;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos2;
        
        AxtriaSalesIQTM__Position__c pos3 = new AxtriaSalesIQTM__Position__c();

        pos3.name = 'New1';
        pos3.AxtriaSalesIQTM__Client_Territory_Name__c='New1';
        pos3.AxtriaSalesIQTM__Client_Position_Code__c = 'P100';
        pos3.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos3.AxtriaSalesIQTM__Position_Type__c='FLSM';
        pos3.AxtriaSalesIQTM__RGB__c = '41,210,118';
        pos3.AxtriaSalesIQTM__inactive__c = false;
        pos3.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos3.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos3.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos3.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos3.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos3.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        
        insert pos3;
        
        AxtriaSalesIQTM__Position_Account__c  pA= new AxtriaSalesIQTM__Position_Account__c();
        pA.AxtriaSalesIQTM__Position__c=pos.id;
        pA.AxtriaSalesIQTM__Account__c=acc.id;
        pA.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        pA.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        pA.Ship_To_Exception__c=True;
       
        insert pA;
        
        AxtriaSalesIQTM__Position_Account__c  pA1= new AxtriaSalesIQTM__Position_Account__c();
        pA1.AxtriaSalesIQTM__Position__c=pos2.id;
        pA1.AxtriaSalesIQTM__Account__c=acc2.id;
        pA1.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        pA1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA1;
        
        AxtriaSalesIQTM__Position_Account__c  pA2= new AxtriaSalesIQTM__Position_Account__c();
        pA2.AxtriaSalesIQTM__Position__c=pos.id;
        pA2.AxtriaSalesIQTM__Account__c=acc2.id;
        pA2.AxtriaSalesIQTM__Team_Instance__c=Teaminstance2.id;
        pA2.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA2.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA2;
        
        /*AxtriaSalesIQTM__Position_Account__c  Ship= new AxtriaSalesIQTM__Position_Account__c();
        Ship.AxtriaSalesIQTM__Position__c=pos.id;
        Ship.AxtriaSalesIQTM__Account__c=Ship2.id;
        Ship.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        Ship.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        Ship.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        Ship.Ship_To_Exception__c=False;*/
       
        //insert Ship;
        
        /*AxtriaSalesIQTM__Position_Account__c  Sold= new AxtriaSalesIQTM__Position_Account__c();
        Sold.AxtriaSalesIQTM__Position__c=pos3.id;
        Sold.AxtriaSalesIQTM__Account__c=Sold2.id;
        Sold.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        Sold.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        Sold.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert Sold;*/
        
        AxtriaSalesIQTM__CR_Account__c cr = new AxtriaSalesIQTM__CR_Account__c();
        cr.AxtriaSalesIQTM__Account__c = acc.Id;
        cr.ShipToException__c=True;
        //cr.AxtriaSalesIQTM__Account__r.accountnumber=acc.AccountNumber;
        //cr.AxtriaSalesIQTM__Account__r.AssociatedSold__c=acc.AssociatedSold__c;
        cr.AxtriaSalesIQTM__Source_Position__c=pos3.id;
        cr.AxtriaSalesIQTM__Destination_Position__c=pos.id;
        //cr.Team_Instance__c='SPMP8';
        insert cr;
        
        
        
        Test.startTest();
        
        database.executebatch(new BatchUpdateBeforeFlagCRAcc());
        Test.stopTest();
        
        
    }

}