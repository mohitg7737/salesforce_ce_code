global class BatchUpdateShipAndSoldValue implements Database.Batchable<sObject>,Schedulable
{

    public String query;
    public String teamInstance;
    public List<AxtriaSalesIQTM__Position_Account__c> UpdatePosAcc;
    Set<String> accset;
    Map<String,String> MapShipToSold;
    Map<String,list<AxtriaSalesIQTM__Position_Account__c>>MapAccToPosAcc;

    Map<String,AxtriaSalesIQTM__Position_Account__c>MapAccPosTIToPosAcc;
    
   
    String SoldAccKey;
    String ShipAccKey;
    
    
    global BatchUpdateShipAndSoldValue()
    {
        accset = new Set<String>();
        //posacclistShip= new list<AxtriaSalesIQTM__Position_Account__c>();
        String ShipAccKey;
        UpdatePosAcc = new list<AxtriaSalesIQTM__Position_Account__c>();
        MapShipToSold = new Map<String,String>();
        MapAccToPosAcc = new Map<String,list<AxtriaSalesIQTM__Position_Account__c>>();
        MapAccPosTIToPosAcc = new Map<String,AxtriaSalesIQTM__Position_Account__c>();

        
        query ='Select id ,AccountNumber, AssociatedSold__c,RecordType.name,RecordTypeId, AxtriaSalesIQTM__Active__c from Account where AxtriaSalesIQTM__Active__c = \'Active\' and AssociatedSold__c !=null order by AccountNumber ';
    }
    

  
     global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }
    
   global void execute(SchedulableContext sc) {
      BatchUpdateShipAndSoldValue  b = new BatchUpdateShipAndSoldValue (); 
      database.executebatch(b);
   }
    
    global void execute(Database.BatchableContext bc, List<Account> scope)
    {
        
        UpdatePosAcc = new list<AxtriaSalesIQTM__Position_Account__c>();

        set<String> shipToAccUpdateSet = new set<String>();

        accset = new Set<String>();
        MapShipToSold = new Map<String,String>();
        List<AxtriaSalesIQTM__Position_Account__c> PosAcc = new List<AxtriaSalesIQTM__Position_Account__c>();
        MapAccToPosAcc = new Map<String,list<AxtriaSalesIQTM__Position_Account__c>>();
        MapAccPosTIToPosAcc = new Map<String,AxtriaSalesIQTM__Position_Account__c>();


        for(Account acc: scope)
        {
            accset.add(acc.AccountNumber);
            accset.add(acc.AssociatedSold__c);    
            MapShipToSold.put(acc.AccountNumber,acc.AssociatedSold__c);
            
        }
        System.debug('Account Set  ' +accset );
        System.debug('Map contains  '+MapShipToSold);

        PosAcc = [select id,AxtriaSalesIQTM__Position__c,Ship_To_Exception__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.name,AxtriaSalesIQTM__Assignment_Status__c  from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Assignment_Status__c !='InActive' AND AxtriaSalesIQTM__Account__r.AccountNumber IN :accset];
        
        System.debug('Values of Position Account are   '+PosAcc);
        System.debug('Values of Position Account are   '+PosAcc.size());
        
        for(AxtriaSalesIQTM__Position_Account__c p : PosAcc)
        {
            list<AxtriaSalesIQTM__Position_Account__c> MapPosAccList = new list<AxtriaSalesIQTM__Position_Account__c>();
            if(MapAccToPosAcc.containsKey(p.AxtriaSalesIQTM__Account__r.AccountNumber)){
                
                MapPosAccList = MapAccToPosAcc.get(p.AxtriaSalesIQTM__Account__r.AccountNumber);
                MapPosAccList.add(p);
                MapAccToPosAcc.put(p.AxtriaSalesIQTM__Account__r.AccountNumber, MapPosAccList);
                System.debug('MapAccToPosAcc ' +MapAccToPosAcc);
                
            }
            
            else{
                
            
                MapAccToPosAcc.put(p.AxtriaSalesIQTM__Account__r.AccountNumber,new list<AxtriaSalesIQTM__Position_Account__c>{p});
                System.debug('MapAccToPosAcc ' +MapAccToPosAcc);
            }

            String key = p.AxtriaSalesIQTM__Account__r.AccountNumber + '_'+p.AxtriaSalesIQTM__Position__c + '_' + p.AxtriaSalesIQTM__Team_Instance__c;

            MapAccPosTIToPosAcc.put(key, p);
        }
        //System.debug('Account To Pos Account Map Data  '+MapAccToPosAcc);
        
        for(String ShipAcc : MapShipToSold.keySet())
        {
           list<AxtriaSalesIQTM__Position_Account__c> posacclistShip  = new list<AxtriaSalesIQTM__Position_Account__c>(); 
           
           posacclistShip = MapAccToPosAcc.get(ShipAcc);
           System.debug('List of Ship Pos Acc  ' +posacclistShip);
           //System.debug('List Size of Ship Position Account for 1 account num  ' +posacclistShip.size());
            if(posacclistShip!=null)
            {
                 for(AxtriaSalesIQTM__Position_Account__c paShip:posacclistShip)
                 {
              
                ShipAccKey = paShip.AxtriaSalesIQTM__Account__r.AccountNumber +'_'+ paShip.AxtriaSalesIQTM__Position__c + '_'+ paShip.AxtriaSalesIQTM__Team_Instance__c;
                
                System.debug('ShipAcctnumber to PosAcc Key  '+ShipAccKey);
                System.debug('pos account record  '+paShip);
                String SoldAccnum = MapShipToSold.get(ShipAcc);
                System.debug('Sold Account Number is '+SoldAccnum);
                
                list<AxtriaSalesIQTM__Position_Account__c> SoldPosac = new list<AxtriaSalesIQTM__Position_Account__c>();
                //if(MapAccPosTIToPosAcc.get(SoldAccnum +'_'+ paShip.AxtriaSalesIQTM__Position__c + '_'+ paShip.AxtriaSalesIQTM__Team_Instance__c)!=null)
                //{
                    //SoldPosac = MapAccPosTIToPosAcc.get(SoldAccnum +'_'+ paShip.AxtriaSalesIQTM__Position__c + '_'+ paShip.AxtriaSalesIQTM__Team_Instance__c);
                    if(MapAccToPosAcc.get(SoldAccnum) != null)
                        SoldPosac.addAll(MapAccToPosAcc.get(SoldAccnum));
                    
                    //String soldToKey = SoldPosac.AxtriaSalesIQTM__Account__r.AccountNumber +'_'+SoldPosac.AxtriaSalesIQTM__Position__c + '_' + SoldPosac.AxtriaSalesIQTM__Team_Instance__c;
                    //System.debug('Sold Account Value ' +soldToKey);
                    if(SoldPosac.size() >0){
                        for(AxtriaSalesIQTM__Position_Account__c paSold:SoldPosac){
                            if(paShip.AxtriaSalesIQTM__Team_Instance__c == paSold.AxtriaSalesIQTM__Team_Instance__c){
                                if(paShip.AxtriaSalesIQTM__Position__c != paSold.AxtriaSalesIQTM__Position__c)
                                {

                                      paShip.Ship_To_Exception__c = True;
                                      if(!shipToAccUpdateSet.contains(ShipAccKey)){
                                            UpdatePosAcc.add(paShip);
                                            shipToAccUpdateSet.add(ShipAccKey);
                                      }
                                      System.debug('Updated Pos Acc list  '+UpdatePosAcc);
                                      
                                      
                                }
                                else
                                {
                              
                                    paShip.Ship_To_Exception__c = False;
                                    if(!shipToAccUpdateSet.contains(ShipAccKey)){
                                            UpdatePosAcc.add(paShip);
                                            shipToAccUpdateSet.add(ShipAccKey);
                                    }
                                    System.debug('Updated Pos Acc list  '+UpdatePosAcc);
                                    
                                }
                            }
                            
                        }
                    }
                    
                    
                //}
                
                /*for(AxtriaSalesIQTM__Position_Account__c paSold:SoldPosac)
                {
                    SoldAccKey = paSold.AxtriaSalesIQTM__Account__r.AccountNumber +'_'+paSold.AxtriaSalesIQTM__Position__c + '_' + paSold.AxtriaSalesIQTM__Team_Instance__c;
                    System.debug('Sold Account Number PosAcc key  '+SoldAccKey);
            
                    if(ShipAccKey !=SoldAccKey)
                    {
                          paShip.Ship_To_Exception__c =True;
                          UpdatePosAcc.add(paShip);
                          System.debug('Updated Pos Acc list  '+UpdatePosAcc);
                          
                    }
                    else
                    {
                  
                        paShip.Ship_To_Exception__c =False;
                        UpdatePosAcc.add(paShip);
                        System.debug('Updated Pos Acc list  '+UpdatePosAcc);
                    }
                }*/
      
            }
            }
        }
        System.debug('New Updated Pos Acc list  '+UpdatePosAcc);
        Update UpdatePosAcc;
        
    }
    global void finish(Database.BatchableContext BC){
    try{
            list<AxtriaSalesIQST__Scheduler_Log__c> scLog = new list<AxtriaSalesIQST__Scheduler_Log__c>();
            scLog = [select id, Name, AxtriaSalesIQST__Job_Type__c,AxtriaSalesIQST__Job_Name__c, AxtriaSalesIQST__Job_Status__c from AxtriaSalesIQST__Scheduler_Log__c where AxtriaSalesIQST__Job_Type__c =  'Inbound' AND AxtriaSalesIQST__Job_Name__c =  'Batch Update Position Account Ship Exception' and AxtriaSalesIQST__Job_Status__c = 'Job Initiated' and createdDate = today limit 1];

            scLog[0].AxtriaSalesIQST__Job_Status__c = 'Completed';
            update scLog[0];

          }catch(Exception e){

          }
        
    }

     
}