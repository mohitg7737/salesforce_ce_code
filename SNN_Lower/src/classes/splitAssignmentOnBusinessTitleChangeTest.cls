@isTest
private class splitAssignmentOnBusinessTitleChangeTest{

    private static testMethod void test1(){
       
        AxtriaSalesIQTM__Country__c country= new AxtriaSalesIQTM__Country__c();
        country.Name =System.Label.USA;
        country.AxtriaSalesIQTM__Status__c='Active';
        country.AxtriaSalesIQTM__Parent_Organization__c = 'a1I5e000000yxx0';
        insert country;
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);

        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        //objTeamInstance.isActiveCycle__c = 'Y';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;
        
        AxtriaSalesIQTM__Employee__c emp1 = new AxtriaSalesIQTM__Employee__c();
        emp1.Name ='AA CC'; 
        emp1.AxtriaSalesIQTM__Employee_ID__c = 'ABC1231';
        emp1.AxtriaSalesIQTM__FirstName__c ='AA';
        emp1.Business_Title__C = 'test1';
        emp1.AxtriaSalesIQTM__Middle_Name__c='B';
        emp1.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp1.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp1.Job_Title_Code__c='ABA';
        emp1.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp1.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp1.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp1.Business_Email_Address__c='hw@gkd.com';
        emp1.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp1.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp1.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp1.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp1.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp1.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp1.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp1.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp1.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp1.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp1.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp1.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp1.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp1.Personal_Email_Address__c='hw@gkd.com';
        emp1.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp1.AxtriaSalesIQTM__HR_Status__c = 'Inactive';
        emp1.AxtriaSalesIQTM__HR_Termination_Date__c=Date.newInstance(2019, 10, 01);       
        insert emp1;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.name = 'New York, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
         pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos;
  
        
        AxtriaSalesIQTM__Position_Employee__c pos_emp5 = new AxtriaSalesIQTM__Position_Employee__c();
        pos_emp5.AxtriaSalesIQTM__Position__c = pos.id;
        pos_emp5.AxtriaSalesIQTM__Employee__c = emp1.id;
        pos_emp5.AxtriaSalesIQTM__Assignment_Type__c='Primary';
        pos_emp5.AxtriaSalesIQTM__Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
        pos_emp5.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Effective_End_Date__c;
        insert pos_emp5;
    }
    
    @isTest(SeeAllData=true)
    static void test2(){       
        AxtriaSalesIQTM__Position_Employee__c lstPosEmp = [Select id,name,Business_Title__c,Plan_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Employee__c from AxtriaSalesIQTM__Position_Employee__c where Business_Title__c = 'SPM AVP' LIMIT 1];
        lstPosEmp.Business_Title__c = 'Associate Sales Representative';
        update lstPosEmp;      
    }
    
}