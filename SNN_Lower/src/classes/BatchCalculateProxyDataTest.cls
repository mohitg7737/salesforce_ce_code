@isTest
public class BatchCalculateProxyDataTest{
    static testMethod void unitTest1(){
        String nextFireTime = '0 0 0 3 9 ? 2045';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'US';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.Name = 'USA';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        AxtriaSalesIQTM__TriggerContol__c cs = new AxtriaSalesIQTM__TriggerContol__c();
        cs.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs.Name = 'Inbound_Outbound_Email';
        insert cs;
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;        
      
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2021';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        insert newcreateScenarioObj;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance2 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance2.name = 'SPM_Q2_2021';
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance2.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00002';
        objTeamInstance2.AxtriaSalesIQTM__Scenario__c =  newcreateScenarioObj.Id;            
        insert objTeamInstance2;     
        
        Account objAccount = new Account();
        objAccount.Name = 'Test ACC';
        objAccount.AxtriaSalesIQST__Marketing_Code__c = 'USA';
        objAccount.PY_C1_Sales__c = 100;
        objAccount.PY_C2_Sales__c = 200;
        objAccount.PY_C3_Sales__c = 300;
        objAccount.PY_C4_Sales__c = 400;
        objAccount.PY_C5_Sales__c = 500;
        objAccount.PY_C6_Sales__c = 600;
        insert objAccount;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();    
        pos.name = 'Test NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  = date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  = date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance2.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Is_Unassigned_Position__c = false;
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos; 
        
        AxtriaSalesIQTM__Position_Account__c objPosAcc = new AxtriaSalesIQTM__Position_Account__c();
        objPosAcc.AxtriaSalesIQTM__Account__c = objAccount.Id;
        objPosAcc.AxtriaSalesIQTM__Position__c = pos.Id;
        objPosAcc.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.Id;
        insert objPosAcc;
        
        AxtriaSalesIQTM__TriggerContol__c myCS = new AxtriaSalesIQTM__TriggerContol__c(Name = 'PhasingGrowthTrigger');
        Boolean  myCCVal = myCS.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal = false;
        insert myCS;
        
        Phasing_Growth_Percentage__c objPhsGrwth = new Phasing_Growth_Percentage__c();
        objPhsGrwth.P1_Value__c = 10;
        objPhsGrwth.P2_Value__c = 10;
        objPhsGrwth.P3_Value__c = 10;
        objPhsGrwth.P4_Value__c = 10;
        objPhsGrwth.P5_Value__c = 20;
        objPhsGrwth.P6_Value__c = 10;
        objPhsGrwth.P7_Value__c = 10;
        objPhsGrwth.P8_Value__c = 20;
        objPhsGrwth.Product_Category__c = 'C6-ENT TULA SALES';
        objPhsGrwth.Type__c = 'Phasing %';
        objPhsGrwth.Name = '2021';
        insert objPhsGrwth;
        
        Phasing_Growth_Percentage__c objPhsGrwth2 = new Phasing_Growth_Percentage__c();
        objPhsGrwth2.P1_Value__c = 10;
        objPhsGrwth2.P2_Value__c = 10;
        objPhsGrwth2.P3_Value__c = 10;
        objPhsGrwth2.P4_Value__c = 10;
        objPhsGrwth2.P5_Value__c = 20;
        objPhsGrwth2.P6_Value__c = 10;
        objPhsGrwth2.P7_Value__c = 10;
        objPhsGrwth2.P8_Value__c = 20;
        objPhsGrwth2.Product_Category__c = 'C6-ENT TULA SALES';
        objPhsGrwth2.Type__c = 'Growth %';
        objPhsGrwth2.Name = '2021';
        insert objPhsGrwth2;
        
        Test.startTest();
        String jobId2 = System.schedule('Test Proxy Data',  nextFireTime, new ScheduleCalculateProxyData());
        Test.stopTest();
    }
    
    static testMethod void unitTest2(){
        String nextFireTime = '0 0 0 3 9 ? 2045';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        AxtriaSalesIQTM__TriggerContol__c cs = new AxtriaSalesIQTM__TriggerContol__c();
        cs.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs.Name = 'Inbound_Outbound_Email';
        insert cs;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'US';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.Name = 'USA';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;        
      
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';      
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2021';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        insert newcreateScenarioObj;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance2 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance2.name = 'SPM_Q2_2021';
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance2.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00002';
        objTeamInstance2.AxtriaSalesIQTM__Scenario__c =  newcreateScenarioObj.Id;            
        insert objTeamInstance2;     
        
        Account objAccount = new Account();
        objAccount.Name = 'Test ACC';
        objAccount.AxtriaSalesIQST__Marketing_Code__c = 'USA';
        objAccount.PY_C1_Sales__c = 100;
        objAccount.PY_C2_Sales__c = 200;
        objAccount.PY_C3_Sales__c = 300;
        objAccount.PY_C4_Sales__c = 400;
        objAccount.PY_C5_Sales__c = 500;
        objAccount.PY_C6_Sales__c = 600;
        insert objAccount;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();    
        pos.name = 'Test NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  = date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  = date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance2.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Is_Unassigned_Position__c = false;
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos; 
        
        AxtriaSalesIQTM__Position_Account__c objPosAcc = new AxtriaSalesIQTM__Position_Account__c();
        objPosAcc.AxtriaSalesIQTM__Account__c = objAccount.Id;
        objPosAcc.AxtriaSalesIQTM__Position__c = pos.Id;
        objPosAcc.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.Id;
        insert objPosAcc;
        AxtriaSalesIQTM__Position_Account__c objPosAcc1 = new AxtriaSalesIQTM__Position_Account__c();
        objPosAcc1.AxtriaSalesIQTM__Account__c = '0015e00000VHSDA';
        objPosAcc1.AxtriaSalesIQTM__Position__c = pos.Id;
        objPosAcc1.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.Id;
        insert objPosAcc1;
        
        AxtriaSalesIQTM__TriggerContol__c myCS = new AxtriaSalesIQTM__TriggerContol__c(Name = 'PhasingGrowthTrigger');
        Boolean  myCCVal = myCS.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal = false;
        insert myCS;
        
        Phasing_Growth_Percentage__c objPhsGrwth = new Phasing_Growth_Percentage__c();
        objPhsGrwth.P1_Value__c = 10;
        objPhsGrwth.P2_Value__c = 10;
        objPhsGrwth.P3_Value__c = 10;
        objPhsGrwth.P4_Value__c = 10;
        objPhsGrwth.P5_Value__c = 20;
        objPhsGrwth.P6_Value__c = 10;
        objPhsGrwth.P7_Value__c = 10;
        objPhsGrwth.P8_Value__c = 20;
        objPhsGrwth.Product_Category__c = 'C5-ENT LEGACY SALES';
        objPhsGrwth.Type__c = 'Phasing %';
        objPhsGrwth.Name = '2021';
        insert objPhsGrwth;
        
        
        Phasing_Growth_Percentage__c objPhsGrwth2 = new Phasing_Growth_Percentage__c();
        objPhsGrwth2.P1_Value__c = 10;
        objPhsGrwth2.P2_Value__c = 10;
        objPhsGrwth2.P3_Value__c = 10;
        objPhsGrwth2.P4_Value__c = 10;
        objPhsGrwth2.P5_Value__c = 20;
        objPhsGrwth2.P6_Value__c = 10;
        objPhsGrwth2.P7_Value__c = 10;
        objPhsGrwth2.P8_Value__c = 20;
        objPhsGrwth2.Product_Category__c = 'C5-ENT LEGACY SALES';
        objPhsGrwth2.Type__c = 'Growth %';
        objPhsGrwth2.Name = '2021';
        insert objPhsGrwth2;
        
        Test.startTest();
        String jobId2 = System.schedule('Test Proxy Data2',  nextFireTime, new ScheduleCalculateProxyData());
        Test.stopTest();
    }
    
    static testMethod void unitTest3(){

        AxtriaSalesIQTM__TriggerContol__c cs = new AxtriaSalesIQTM__TriggerContol__c();
        cs.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs.Name = 'Inbound_Outbound_Email';
        insert cs;
        String nextFireTime = '0 0 0 3 9 ? 2045';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'US';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.Name = 'USA';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;        
      
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';      
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2021';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        insert newcreateScenarioObj;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance2 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance2.name = 'SPM_Q2_2021';
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance2.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00002';
        objTeamInstance2.AxtriaSalesIQTM__Scenario__c =  newcreateScenarioObj.Id;            
        insert objTeamInstance2;     
        
        Account objAccount = new Account();
        objAccount.Name = 'Test ACC';
        objAccount.AxtriaSalesIQST__Marketing_Code__c = 'USA';
        objAccount.PY_C1_Sales__c = 100;
        objAccount.PY_C2_Sales__c = 200;
        objAccount.PY_C3_Sales__c = 300;
        objAccount.PY_C4_Sales__c = 400;
        objAccount.PY_C5_Sales__c = 500;
        objAccount.PY_C6_Sales__c = 600;
        insert objAccount;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();    
        pos.name = 'Test NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  = date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  = date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance2.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Is_Unassigned_Position__c = false;
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos; 
        
        AxtriaSalesIQTM__Position_Account__c objPosAcc = new AxtriaSalesIQTM__Position_Account__c();
        objPosAcc.AxtriaSalesIQTM__Account__c = objAccount.Id;
        objPosAcc.AxtriaSalesIQTM__Position__c = pos.Id;
        objPosAcc.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.Id;
        insert objPosAcc;
        
        AxtriaSalesIQTM__TriggerContol__c myCS = new AxtriaSalesIQTM__TriggerContol__c(Name = 'PhasingGrowthTrigger');
        Boolean  myCCVal = myCS.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal = false;
        insert myCS;
        
        Phasing_Growth_Percentage__c objPhsGrwth = new Phasing_Growth_Percentage__c();
        objPhsGrwth.P1_Value__c = 10;
        objPhsGrwth.P2_Value__c = 10;
        objPhsGrwth.P3_Value__c = 10;
        objPhsGrwth.P4_Value__c = 10;
        objPhsGrwth.P5_Value__c = 20;
        objPhsGrwth.P6_Value__c = 10;
        objPhsGrwth.P7_Value__c = 10;
        objPhsGrwth.P8_Value__c = 20;
        objPhsGrwth.Product_Category__c = 'C5-ENT LEGACY SALES';
        objPhsGrwth.Type__c = 'Phasing %';
        objPhsGrwth.Name = '2021';
        insert objPhsGrwth;
        
        Phasing_Growth_Percentage__c objPhsGrwth2 = new Phasing_Growth_Percentage__c();
        objPhsGrwth2.P1_Value__c = 10;
        objPhsGrwth2.P2_Value__c = 10;
        objPhsGrwth2.P3_Value__c = 10;
        objPhsGrwth2.P4_Value__c = 10;
        objPhsGrwth2.P5_Value__c = 20;
        objPhsGrwth2.P6_Value__c = 10;
        objPhsGrwth2.P7_Value__c = 10;
        objPhsGrwth2.P8_Value__c = 20;
        objPhsGrwth2.Product_Category__c = 'C5-ENT LEGACY SALES';
        objPhsGrwth2.Type__c = 'Growth %';
        objPhsGrwth2.Name = '2021';
        insert objPhsGrwth2;
        
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Job_Type__c = 'Inbound';
        scLog.AxtriaSalesIQST__Job_Name__c = 'KPI & Proxy Quota Calc & Rollup Batch - Current & Future TIs';
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        
        AxtriaSalesIQTM__CIM_Config__c objCimConfig = new AxtriaSalesIQTM__CIM_Config__c();
        objCimConfig.Name = 'Test';
        objCimConfig.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance2.Id;
        objCimConfig.AxtriaSalesIQTM__Enable__c = true;
        objCimConfig.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        objCimConfig.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric2__c';
        objCimConfig.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account__c';
        insert objCimConfig;
        
        Test.startTest();
        Database.executeBatch(new BatchCalculateProxyData(objTeamInstance2.Id,scLog));
        Test.stopTest();
    }
}