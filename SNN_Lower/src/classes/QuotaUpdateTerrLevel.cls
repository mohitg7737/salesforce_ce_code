global class QuotaUpdateTerrLevel implements Database.Batchable<sObject>{
    public String query;
    public String teamInstanceId;
    public List<String> teamInstanceIdslist;
    public AxtriaSalesIQST__Scheduler_Log__c scLog;
    public List<String> lstTeamInstanceId;
    public Map<String, String> mapOfFiscalPeriodToPSalesField;

    global QuotaUpdateTerrLevel(AxtriaSalesIQST__Scheduler_Log__c scLog1){
        this.teamInstanceId = teamInstanceId;
        //AxtriaSalesIQTM__Team_Instance__c objTeamInstance = [Select Id, Fiscal_Period__c from AxtriaSalesIQTM__Team_Instance__c where Id=: teamInstanceId and Fiscal_Period__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c != null];
        

        query = 'Select Id,Quota_C1__c,Quota_C2__c,Quota_C3__c,Quota_C4__c,Quota_C5__c,Quota_C6__c,SAP_Position_Id__c,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position_Code__c, AxtriaSalesIQTM__Position_Status__c, AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,IsDummyPOD__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c != null Order by SAP_Position_Id__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c';
        
        if(sclog1!=null){
            scLog = scLog1;
        }
        else{
            scLog = new AxtriaSalesIQST__Scheduler_Log__c();
            scLog.AxtriaSalesIQST__Created_Date__c = System.today();
            scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
            scLog.AxtriaSalesIQST__Job_Type__c = 'Inbound';
            scLog.AxtriaSalesIQST__Job_Name__c = 'Quota Update Territory Level Batch - All Pos';
            scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
            insert scLog;
        }
        
    }

    global QuotaUpdateTerrLevel(String teamInstanceId){
        this.teamInstanceId = teamInstanceId;
        //AxtriaSalesIQTM__Team_Instance__c objTeamInstance = [Select Id, Fiscal_Period__c from AxtriaSalesIQTM__Team_Instance__c where Id=: teamInstanceId and Fiscal_Period__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c != null];
        

        query = 'Select Id,Quota_C1__c,Quota_C2__c,Quota_C3__c,Quota_C4__c,Quota_C5__c,Quota_C6__c,SAP_Position_Id__c,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position_Code__c, AxtriaSalesIQTM__Position_Status__c, AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,IsDummyPOD__c   from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceId  Order by SAP_Position_Id__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c';
        
        scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        scLog.AxtriaSalesIQST__Job_Type__c = 'Inbound';
        scLog.AxtriaSalesIQST__Job_Name__c = 'Quota Update Territory Level Batch - ' + teamInstanceId;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        scLog.AxtriaSalesIQST__Team_Instance__c = teamInstanceId;
        insert scLog;
        
    }

    global QuotaUpdateTerrLevel(List<String> teamInstanceIds){
        this.teamInstanceIdslist = new List<String>();
        this.teamInstanceIdslist = teamInstanceIds;
        
        query = 'Select Id,Quota_C1__c,Quota_C2__c,Quota_C3__c,Quota_C4__c,Quota_C5__c,Quota_C6__c,SAP_Position_Id__c,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position_Code__c, AxtriaSalesIQTM__Position_Status__c, AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,IsDummyPOD__c   from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :teamInstanceIdslist  Order by SAP_Position_Id__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c';
        
              
        scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        scLog.AxtriaSalesIQST__Job_Type__c = 'Inbound';
        scLog.AxtriaSalesIQST__Job_Name__c = 'Quota Update Territory Level Batch All TIs';
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){       
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position__c> scope){

        Set<String> SapIdList = new Set<String>();
        Set<String> periodList = new Set<String>();
        Set<String> yearList = new Set<String>();

        for(AxtriaSalesIQTM__Position__c p: scope){
            SapIdList.add(p.SAP_Position_Id__c);
            periodList.add(p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c);
            yearList.add(p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c);
        }

        List<Beacon_Quota__c> bqList = new List<Beacon_Quota__c>();
        bqList= [Select Id,Parent_Position_Id__c, Parent_Position_Name__c, Period__c, Position_Id__c, Position_Name__c, Position_Type__c, Product__c, Quota__c, Quota_Id__c, Team__c, Year__c from Beacon_Quota__c where Position_Id__c in :SapIdList and Period__c in :periodList and Year__c in :yearList ];

        Map<String,Decimal> quotaIdToQuotaValue = new Map<String,Decimal>();

        for(Beacon_Quota__c bq: bqList) {
            quotaIdToQuotaValue.put(bq.Quota_Id__c, bq.Quota__c);         //Quota ID ==> SAPID+Year+Period+Product
            System.debug('quotaIdToQuotaValue map key--'+bq.Quota_Id__c +'  val --- '+ bq.Quota__c);
        }

        for(AxtriaSalesIQTM__Position__c p: scope){
            
            
            p.Quota_C1__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'CAP') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'CAP') : 0.0; //p.Quota_C1__c;

            p.Quota_C2__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'REGEN') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'REGEN') : 0.0; //p.Quota_C2__c;

            p.Quota_C3__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDJR') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDJR') : 0.0; //p.Quota_C3__c;

            p.Quota_C4__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDAET') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDAET') : 0.0; //p.Quota_C4__c;

            p.Quota_C5__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT LEGACY') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT LEGACY') : 0.0; //p.Quota_C5__c;

            p.Quota_C6__c = quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT TULA') ? quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT TULA') : 0.0; //p.Quota_C6__c;
            
            
            /*System.debug('Quota ID: '+p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c);
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'CAP')){
            System.debug('Quota Value +CAP: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'CAP'));
            }
            else{
                System.debug('LOLL CAP');
            }
            
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'REGEN')){
            System.debug('Quota Value +REGEN: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'REGEN'));
            }
            else{
                System.debug('LOLL REGEN');
            }
            
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDJR')){
            System.debug('Quota Value +SMDJR: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDJR'));
            }
            else{
                System.debug('LOLL SMDJR');
            }
            
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDAET')){
            System.debug('Quota Value +SMDAET: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'SMDAET'));
            }
            else{
                System.debug('LOLL SMDAET');
            }
            
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT LEGACY')){
            System.debug('Quota Value +ENT LEGACY: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT LEGACY'));
            }
            else{
                System.debug('LOLL ENT LEGACY');
            }
            
            if(quotaIdToQuotaValue.containskey(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT TULA')){
            System.debug('Quota Value +ENT TULA: '+quotaIdToQuotaValue.get(p.SAP_Position_Id__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c+p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+'ENT TULA'));
            }
            else{
                System.debug('LOLL ENT TULA');
            }*/
            
            
            
        }

        update scope;
         
    }

    global void finish(Database.BatchableContext bc){
        try{
            scLog.AxtriaSalesIQST__Job_Status__c = 'Successful';
            update scLog;                     
        }catch(Exception exc){
            scLog.AxtriaSalesIQST__Job_Status__c = 'Error';
            update scLog;
        }
    }    
}