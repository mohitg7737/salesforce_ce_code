@isTest //(SeeAllData=true)
private class BatchComputeEventsTest {

    private static testMethod void test1() {
        
        AxtriaSalesIQTM__Country__c country= new AxtriaSalesIQTM__Country__c();
        country.Name =System.Label.USA;
        country.AxtriaSalesIQTM__Status__c='Active';
        country.AxtriaSalesIQTM__Parent_Organization__c='a1I5e000000yxx0';
        insert country;
        
        RosterToEmailList__c toEmail = new RosterToEmailList__c();
        toEmail.Name='abc';
        toEmail.Email__c='lagnika.sharma@axtria.com';
        toEmail.Type__c='Roster';
        insert toEmail;
        
          AxtriaSalesIQTM__TriggerContol__c rosval = new AxtriaSalesIQTM__TriggerContol__c();
        rosval.name='Roster_Val_Log';
        rosval.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert rosval;
        
        
          
            /****** NEW HIRE ******/
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj.AxtriaSalesIQST__Employee_ID__c='ABC123' ;
        ros_obj.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj.AxtriaSalesIQST__JobCodeName__c= 'AAA' ;
        ros_obj.AxtriaSalesIQST__JobCode__c= 'ABA';
        ros_obj.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2019, 7, 01);
        ros_obj.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2019, 7, 01);
        ros_obj.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2019, 7, 01);
        ros_obj.AxtriaSalesIQST__First_Day_of_Leave__c= null;
        ros_obj.AxtriaSalesIQST__Last_Day_of_Leave__c= null;
        ros_obj.AxtriaSalesIQST__TerminationDate__c=null;
        ros_obj.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2019, 7, 01);
        ros_obj.isIncluded__c=true;
        ros_obj.AxtriaSalesIQST__AssociateOID__c = 'ABC123';
        ros_obj.Employee_Type__c = 'FT';
                
        insert ros_obj;
    
                /****** RE HIRE ******/
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj1 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj1.AxtriaSalesIQST__Employee_ID__c='ABC1231' ;
        ros_obj1.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj1.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj1.AxtriaSalesIQST__JobCodeName__c= 'AAA' ;
        ros_obj1.AxtriaSalesIQST__JobCode__c= 'ABA';
        ros_obj1.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj1.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj1.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj1.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj1.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj1.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj1.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj1.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj1.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj1.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj1.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj1.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj1.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj1.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj1.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj1.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj1.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj1.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj1.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj1.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj1.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2019, 7, 01);
        ros_obj1.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj1.AxtriaSalesIQST__First_Day_of_Leave__c= null;
        ros_obj1.AxtriaSalesIQST__Last_Day_of_Leave__c= null;
        ros_obj1.AxtriaSalesIQST__TerminationDate__c=Date.newInstance(2019, 10, 01) ;
        ros_obj1.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj1.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj1.isIncluded__c=true;
        ros_obj1.AxtriaSalesIQST__AssociateOID__c = 'ABC123';
        ros_obj1.Employee_Type__c = 'FT';
        insert ros_obj1;

        
                /****** Promotion ******/
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj2 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj2.AxtriaSalesIQST__Employee_ID__c='ABC1233' ;
        ros_obj2.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj2.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj2.Business_Title__C = 'AAAB' ;
        ros_obj2.AxtriaSalesIQST__JobCode__c= 'ABAB';
        ros_obj2.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj2.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj2.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj2.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj2.AxtriaSalesIQST__AddressLine1__c='121 Ave';
        ros_obj2.AxtriaSalesIQST__AddressLine2__c= 'road1';
        ros_obj2.AxtriaSalesIQST__AddressCity__c= 'City2';
        ros_obj2.AxtriaSalesIQST__AddressStateCode__c= 'AD';
        ros_obj2.AxtriaSalesIQST__AddressPostalCode__c='07621' ;
        ros_obj2.AxtriaSalesIQST__LegalAddressLine1__c= '121 Ave';
        ros_obj2.AxtriaSalesIQST__LegalAddressLine2__c=  'road1';
        ros_obj2.AxtriaSalesIQST__LegalAddressCityName__c= 'City2';
        ros_obj2.AxtriaSalesIQST__StateTerritory__c= 'AD';
        ros_obj2.AxtriaSalesIQST__LegalAddressPostalCode__c='07621' ;
        ros_obj2.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj2.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj2.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj2.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj2.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj2.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj2.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2020, 7, 01);
        ros_obj2.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj2.AxtriaSalesIQST__First_Day_of_Leave__c= null;
        ros_obj2.AxtriaSalesIQST__Last_Day_of_Leave__c= null;
        ros_obj2.AxtriaSalesIQST__TerminationDate__c=null ;
        ros_obj2.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj2.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj2.isIncluded__c=true;
        ros_obj2.AxtriaSalesIQST__AssociateOID__c = 'ABC123';
        ros_obj2.Employee_Type__c = '1099';
        insert ros_obj2;
    
        
                /****** TERMINATION ******/
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj3 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj3.AxtriaSalesIQST__Employee_ID__c='abc567' ;
        ros_obj3.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj3.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj3.AxtriaSalesIQST__JobCodeName__c= 'AAAB' ;
        ros_obj3.AxtriaSalesIQST__JobCode__c= 'ABAB';
        ros_obj3.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj3.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj3.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj3.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj3.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj3.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj3.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj3.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj3.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj3.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj3.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj3.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj3.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj3.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj3.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj3.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj3.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj3.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj3.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj3.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj3.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2020, 7, 01);
        ros_obj3.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj3.AxtriaSalesIQST__First_Day_of_Leave__c= null;
        ros_obj3.AxtriaSalesIQST__Last_Day_of_Leave__c= null;
        ros_obj3.AxtriaSalesIQST__TerminationDate__c=Date.newInstance(2020, 10, 01) ;
        ros_obj3.AxtriaSalesIQST__HR_Status__c= 'Inactive';
        ros_obj3.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj3.isIncluded__c=true;
        ros_obj3.AxtriaSalesIQST__AssociateOID__c = 'ABC123';
        ros_obj3.Employee_Type__c = 'FT';                
        insert ros_obj3;
    
    

                /****** LOA Create ******/
       AxtriaSalesIQST__CurrentPersonFeed__c ros_obj4 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj4.AxtriaSalesIQST__Employee_ID__c='abc678' ;
        ros_obj4.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj4.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj4.AxtriaSalesIQST__JobCodeName__c= 'AAAB' ;
        ros_obj4.AxtriaSalesIQST__JobCode__c= 'ABAB';
        ros_obj4.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj4.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj4.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj4.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj4.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj4.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj4.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj4.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj4.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj4.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj4.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj4.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj4.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj4.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj4.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj4.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj4.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj4.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj4.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj4.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj4.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2020, 7, 01);
        ros_obj4.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj4.AxtriaSalesIQST__First_Day_of_Leave__c= Date.newInstance(2020, 10, 01);
        ros_obj4.AxtriaSalesIQST__Last_Day_of_Leave__c= null;
        ros_obj4.AxtriaSalesIQST__On_Leave__c = true;
        ros_obj4.AxtriaSalesIQST__TerminationDate__c=null ;
        ros_obj4.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj4.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj4.isIncluded__c=true;
        ros_obj4.AxtriaSalesIQST__AssociateOID__c = 'ABC123';
        ros_obj4.Employee_Type__c = 'FT';
        insert ros_obj4;

         /*        // LOA Update 
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj5 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj5.AxtriaSalesIQST__Employee_ID__c='ABC11' ;
        ros_obj5.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj5.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj5.AxtriaSalesIQST__JobCodeName__c= 'AAAB' ;
        ros_obj5.AxtriaSalesIQST__JobCode__c= 'ABAB';
        ros_obj5.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj5.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj5.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj5.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj5.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj5.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj5.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj5.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj5.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj5.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj5.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj5.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj5.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj5.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj5.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj5.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj5.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj5.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj5.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj5.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj5.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2020, 7, 01);
        ros_obj5.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj5.AxtriaSalesIQST__First_Day_of_Leave__c= Date.newInstance(2020, 10, 01);
        ros_obj5.AxtriaSalesIQST__Last_Day_of_Leave__c= Date.newInstance(2020, 10, 10);
        ros_obj5.AxtriaSalesIQST__TerminationDate__c=null ;
        ros_obj5.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj5.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj5.isIncluded__c=true;
                
        insert ros_obj5;
    
    
                 // LOA Error 
        AxtriaSalesIQST__CurrentPersonFeed__c ros_obj6 = new AxtriaSalesIQST__CurrentPersonFeed__c();
        ros_obj6.AxtriaSalesIQST__Employee_ID__c='ABC112' ;
        ros_obj6.AxtriaSalesIQST__ReportingToWorkerName__c= 'ABC';
        ros_obj6.AxtriaSalesIQST__ReportsToAssociateOID__c= 'XYZ123';
        ros_obj6.AxtriaSalesIQST__JobCodeName__c= 'AAAB' ;
        ros_obj6.AxtriaSalesIQST__JobCode__c= 'ABAB';
        ros_obj6.AxtriaSalesIQST__First_Name__c= 'AA';
        ros_obj6.AxtriaSalesIQST__Middle_Name__c= 'B';
        ros_obj6.AxtriaSalesIQST__Last_Name__c= 'CC';
        ros_obj6.AxtriaSalesIQST__NickName__c= 'Some';
        ros_obj6.AxtriaSalesIQST__AddressLine1__c='123 Ave';
        ros_obj6.AxtriaSalesIQST__AddressLine2__c= 'road';
        ros_obj6.AxtriaSalesIQST__AddressCity__c= 'City1';
        ros_obj6.AxtriaSalesIQST__AddressStateCode__c= 'NY';
        ros_obj6.AxtriaSalesIQST__AddressPostalCode__c='07651' ;
        ros_obj6.AxtriaSalesIQST__LegalAddressLine1__c= '123 Ave';
        ros_obj6.AxtriaSalesIQST__LegalAddressLine2__c=  'road';
        ros_obj6.AxtriaSalesIQST__LegalAddressCityName__c= 'City1';
        ros_obj6.AxtriaSalesIQST__StateTerritory__c= 'NY';
        ros_obj6.AxtriaSalesIQST__LegalAddressPostalCode__c='07651' ;
        ros_obj6.AxtriaSalesIQST__HomePhone__c= '93200008';
        ros_obj6.AxtriaSalesIQST__PersonalCell__c='82196199' ;
        ros_obj6.AxtriaSalesIQST__Email__c= 'hw@gkd.com';
        ros_obj6.AxtriaSalesIQST__Segment_1__c= 'hw@gkd.com';
        ros_obj6.AxtriaSalesIQST__Segment_2__c='hw@gkd.com' ;
        ros_obj6.AxtriaSalesIQST__Rehire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj6.AxtriaSalesIQST__OriginalHireDate__c= Date.newInstance(2020, 7, 01);
        ros_obj6.AxtriaSalesIQST__Hire_Date__c= Date.newInstance(2020, 7, 01);
        ros_obj6.AxtriaSalesIQST__First_Day_of_Leave__c= null;
        ros_obj6.AxtriaSalesIQST__Last_Day_of_Leave__c= Date.newInstance(2020, 10, 10);
        ros_obj6.AxtriaSalesIQST__TerminationDate__c=null ;
        ros_obj6.AxtriaSalesIQST__HR_Status__c= 'Active';
        ros_obj6.AxtriaSalesIQST__Last_Modified_Date__c =Date.newInstance(2020, 7, 01);
        ros_obj6.isIncluded__c=true;
                
        insert ros_obj6;*/
        
                /***** REHIRE EMPLOYEE ******/
        AxtriaSalesIQTM__Employee__c emp1 = new AxtriaSalesIQTM__Employee__c();
        emp1.Name ='AA CC'; 
        emp1.AxtriaSalesIQTM__Employee_ID__c = 'ABC1231';
        emp1.AxtriaSalesIQTM__FirstName__c ='AA';
        emp1.Business_Title__C = 'test1';
        emp1.AxtriaSalesIQTM__Middle_Name__c='B';
        emp1.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp1.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp1.Job_Title_Code__c='ABA';
        emp1.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp1.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp1.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp1.Business_Email_Address__c='hw@gkd.com';
        emp1.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp1.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2019, 7, 01);
        emp1.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp1.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp1.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp1.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp1.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp1.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp1.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp1.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp1.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp1.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp1.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp1.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp1.Personal_Email_Address__c='hw@gkd.com';
        emp1.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp1.AxtriaSalesIQTM__HR_Status__c = 'Inactive';
        emp1.AxtriaSalesIQTM__HR_Termination_Date__c=Date.newInstance(2019, 10, 01);
       
        insert emp1;


                /***** Promotion ******/
        AxtriaSalesIQTM__Employee__c emp2 = new AxtriaSalesIQTM__Employee__c();
        emp2.Name ='AA CC'; 
        emp2.AxtriaSalesIQTM__Employee_ID__c = 'ABC1233';
        emp2.AxtriaSalesIQTM__Employee_Type__c = 'FTE';
        emp2.AxtriaSalesIQTM__FirstName__c ='AA';
        emp2.AxtriaSalesIQTM__Middle_Name__c='B';
        emp2.Business_Title__C = 'test1';
        emp2.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp2.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp2.Job_Title_Code__c='ABA';
        emp2.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp2.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp2.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp2.Business_Email_Address__c='hw@gkd.com';
        emp2.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp2.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp2.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp2.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp2.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp2.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp2.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp2.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp2.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp2.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp2.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp2.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp2.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp2.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp2.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp2.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp2.Personal_Email_Address__c='hw@gkd.com';
        emp2.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp2.AxtriaSalesIQTM__HR_Status__c = 'Active';
        
        insert emp2;

       
                /***** Employee Termination ******/
        AxtriaSalesIQTM__Employee__c emp3 = new AxtriaSalesIQTM__Employee__c();
        emp3.Name ='AA CC'; 
        emp3.AxtriaSalesIQTM__Employee_ID__c = 'abc567';
        emp3.AxtriaSalesIQTM__FirstName__c ='AA';
        emp3.AxtriaSalesIQTM__Middle_Name__c='B';
        emp3.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp3.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp3.Job_Title_Code__c='ABA';
        emp3.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp3.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp3.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp3.Business_Email_Address__c='hw@gkd.com';
        emp3.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp3.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp3.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp3.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp3.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp3.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp3.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp3.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp3.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp3.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp3.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp3.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp3.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp3.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp3.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp3.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp3.Personal_Email_Address__c='hw@gkd.com';
        emp3.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp3.AxtriaSalesIQTM__HR_Status__c = 'Active';
        
        insert emp3;


                /***** Employee LOA Create ******/
        AxtriaSalesIQTM__Employee__c emp4 = new AxtriaSalesIQTM__Employee__c();
        emp4.Name ='AA CC'; 
        emp4.AxtriaSalesIQTM__Employee_ID__c = 'abc678';
        emp4.AxtriaSalesIQTM__FirstName__c ='AA';
        emp4.AxtriaSalesIQTM__Middle_Name__c='B';
        emp4.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp4.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp4.Job_Title_Code__c='ABA';
        emp4.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp4.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp4.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp4.Business_Email_Address__c='hw@gkd.com';
        emp4.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp4.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp4.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp4.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp4.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp4.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp4.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp4.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp4.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp4.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp4.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp4.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp4.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp4.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp4.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp4.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp4.Personal_Email_Address__c='hw@gkd.com';
        emp4.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp4.AxtriaSalesIQTM__HR_Status__c = 'Active';
        
        insert emp4;


                /***** Employee LOA Update ******/
        AxtriaSalesIQTM__Employee__c emp5 = new AxtriaSalesIQTM__Employee__c();
        emp5.Name ='AA CC'; 
        emp5.AxtriaSalesIQTM__Employee_ID__c = 'ABC11';
        emp5.AxtriaSalesIQTM__FirstName__c ='AA';
        emp5.AxtriaSalesIQTM__Middle_Name__c='B';
        emp5.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp5.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp5.Job_Title_Code__c='ABA';
        emp5.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp5.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp5.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp5.Business_Email_Address__c='hw@gkd.com';
        emp5.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp5.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp5.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp5.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp5.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp5.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp5.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp5.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp5.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp5.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp5.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp5.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp5.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp5.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp5.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp5.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp5.Personal_Email_Address__c='hw@gkd.com';
        emp5.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp5.AxtriaSalesIQTM__HR_Status__c = 'Active';
        emp5.First_Day_Of_Leave__c=Date.newInstance(2020, 10, 01);
        
        insert emp5;


                /***** Employee Missing Error ******/
        AxtriaSalesIQTM__Employee__c emp6 = new AxtriaSalesIQTM__Employee__c();
        emp6.Name ='AA CC'; 
        emp6.AxtriaSalesIQTM__Employee_ID__c = 'ABC113344';
        emp6.AxtriaSalesIQTM__FirstName__c ='AA';
        emp6.AxtriaSalesIQTM__Middle_Name__c='B';
        emp6.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp6.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp6.Job_Title_Code__c='ABA';
        emp6.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp6.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp6.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp6.Business_Email_Address__c='hw@gkd.com';
        emp6.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp6.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp6.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp6.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp6.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp6.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp6.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp6.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp6.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp6.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp6.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp6.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp6.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp6.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp6.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp6.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp6.Personal_Email_Address__c='hw@gkd.com';
        emp6.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp6.AxtriaSalesIQTM__HR_Status__c = 'Active';
        
        insert emp6;

                /***** Employee LOA Error ******/
        AxtriaSalesIQTM__Employee__c emp7 = new AxtriaSalesIQTM__Employee__c();
        emp7.Name ='AA CC'; 
        emp7.AxtriaSalesIQTM__Employee_ID__c = 'ABC112';
        emp7.AxtriaSalesIQTM__FirstName__c ='AA';
        emp7.AxtriaSalesIQTM__Middle_Name__c='B';
        emp7.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp7.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp7.Job_Title_Code__c='ABA';
        emp7.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp7.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp7.AxtriaSalesIQTM__Email__c = 'hw@gkd.com';
        emp7.Business_Email_Address__c='hw@gkd.com';
        emp7.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp7.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2020, 7, 01);
        emp7.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2020, 7, 01);
        emp7.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2020, 7, 01);
        emp7.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp7.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp7.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp7.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp7.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp7.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp7.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp7.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp7.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp7.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp7.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp7.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp7.Personal_Email_Address__c='hw@gkd.com';
        emp7.AxtriaSalesIQTM__Country_Name__c = country.id;
        emp7.AxtriaSalesIQTM__HR_Status__c = 'Active';
        
        insert emp7;

        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);

        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        //objTeamInstance.isActiveCycle__c = 'Y';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.name = 'New York, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
         pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos;

        
       
        
        
        AxtriaSalesIQTM__Position_Employee__c pos_emp5 = new AxtriaSalesIQTM__Position_Employee__c();
        pos_emp5.AxtriaSalesIQTM__Position__c = pos.id;
        pos_emp5.AxtriaSalesIQTM__Employee__c = emp1.id;
        pos_emp5.AxtriaSalesIQTM__Assignment_Type__c='Primary';
        pos_emp5.AxtriaSalesIQTM__Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
        pos_emp5.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Effective_End_Date__c;
        insert pos_emp5;
        
       
        
        /*CallRosterTalendJob cobj= new CallRosterTalendJob();
        cobj.actionBeforecallingTalendjob();*/
        //cobj.validationFunc();

        RosterWorkbenchCtrl obj = new RosterWorkbenchCtrl();
        obj.dummyFunction();

        SalesIQDataTableWrapper.dummyFunction();
        
        Database.executeBatch(new BatchComputeEvents());
    }

}