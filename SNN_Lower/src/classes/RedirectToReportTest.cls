@istest
public with sharing class RedirectToReportTest{

    RedirectToReportTest()
    {
        redirecttest1();
        redirecttest2();
    }

    static testMethod void redirecttest1()
    {

        AxtriaSalesIQTM__TriggerContol__c myCS1 = new AxtriaSalesIQTM__TriggerContol__c(Name = 'NoDuplTeamName');

        Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal = false;

        insert myCS1;

        AxtriaSalesIQTM__TriggerContol__c myCS2 = new AxtriaSalesIQTM__TriggerContol__c(Name = 'NoDuplicateBU');
        Boolean  myCCVal1 = myCS2.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal1 = false;
        insert myCS2;

        User loggedInUser = [select id, userrole.name from User where id = :UserInfo.getUserId()];

        system.runAs(loggedInUser)
        {

            AxtriaSalesIQTM__Organization_Master__c orgMast = new AxtriaSalesIQTM__Organization_Master__c();
            orgMast.Name = 'Axtria';
            orgMast.AxtriaSalesIQTM__Org_Level__c = 'Global';
            orgMast.AxtriaSalesIQTM__Parent_Country_Level__c = true;
            insert orgMast;

            AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c();
            country.Name = System.Label.USA;
            country.AxtriaSalesIQTM__Status__c = 'Active';
            country.AxtriaSalesIQTM__Parent_Organization__c = orgMast.Id; //'a1I4T000000ekBEUAY';
            insert country;

            AxtriaSalesIQTM__Business_Unit__c bu = new AxtriaSalesIQTM__Business_Unit__c();
            bu.AxtriaSalesIQTM__Code__c = 'RNIP';
            bu.Name = 'Radiology Portfolio';
            insert bu;

            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name = 'ONCO12';
            team.AxtriaSalesIQTM__Business_Unit__c = bu.id;
            team.AxtriaSalesIQTM__Type__c = 'Base';
            insert team;

            AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c();
            ti.Name = 'ONCO21';
            ti.AxtriaSalesIQTM__Team__c = team.Id;
            ti.AxtriaSalesIQTM__IC_EffstartDate__c = System.today() - 5;
            ti.AxtriaSalesIQTM__IC_EffEndDate__c = System.today() + 5;
            ti.AxtriaSalesIQTM__Alignment_Period__c = 'period';
            ti.AxtriaSalesIQTM__Create_Mirror_Position__c = false;
            ti.AxtriaSalesIQTM__Show_Custom_Metric__c = false;
            insert ti;

            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name = 'Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = ti.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = '1,1,1';
            insert po1;

            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name = 'NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = ti.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y01';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y01';
            po.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = '1,1,1';
            insert po;

            AxtriaSalesIQTM__TriggerContol__c tc = new AxtriaSalesIQTM__TriggerContol__c();
            tc.Name = 'UserPositionTrigger';
            tc.AxtriaSalesIQTM__IsStopTrigger__c = true;
            insert tc;

            AxtriaSalesIQTM__User_Access_Permission__c up = new AxtriaSalesIQTM__User_Access_Permission__c();
            up.AxtriaSalesIQTM__Position__c = po.Id;
            up.AxtriaSalesIQTM__Team_Instance__c = ti.Id;
            up.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            up.AxtriaSalesIQTM__Is_Active__c = false;
            insert up;

            AxtriaSalesIQST__Product_Catalog__c prod = new AxtriaSalesIQST__Product_Catalog__c();
            prod.Name = 'ProdTest';
            prod.AxtriaSalesIQST__Team_Instance__c = ti.Id;
            prod.AxtriaSalesIQST__IsActive__c = true;
            prod.AxtriaSalesIQST__Product_Code__c = 'ProdTest';
            prod.AxtriaSalesIQST__Veeva_External_ID__c = 'ProdTest';
            prod.AxtriaSalesIQST__Country_Lookup__c = country.Id;
            insert prod;

            string loggedInPosition = 'abc';

            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(ti.Id, ti.Name));

            RedirectToReport report = new RedirectToReport();

            System.currentPageReference().getParameters().put('buSelected', ti.Id);
            // System.currentPageReference().getParameters().put('selectedTeam', team.Id);
            report.buSelected = ti.Id;
            // zc.selectedTeam = team.Id;

            report.reportChangedA();
            report.teamInstanceChangedA();
            report.openReportA();
            report.openReportR();
            report.ListOfTeamInstance1();
            report.buChanged();
            report.dummyMethod();

        }
    }

    static testMethod void redirecttest2()
    {

        AxtriaSalesIQTM__TriggerContol__c myCS1 = new AxtriaSalesIQTM__TriggerContol__c(Name = 'NoDuplTeamName');

        Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal = false;

        insert myCS1;

        AxtriaSalesIQTM__TriggerContol__c myCS2 = new AxtriaSalesIQTM__TriggerContol__c(Name = 'NoDuplicateBU');
        Boolean  myCCVal1 = myCS2.AxtriaSalesIQTM__IsStopTrigger__c ;
        myCCVal1 = false;
        insert myCS2;

        Profile p1 = [SELECT Id FROM Profile WHERE Name Like '%System Administrator%'];
        User u1 = new User(Alias = 'standt1', Email = 'standarduser1@testorg.com', EmailEncodingKey = 'UTF-8', LastName = 'Testing1',
                           LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = p1.Id, TimeZoneSidKey = 'America/Los_Angeles',
                           UserName = 'rm_pharma@testorg.com');

        insert u1;

        User loggedInUser = [select id, userrole.name from User where id = :UserInfo.getUserId()];

        system.runAs(u1)
        {

            AxtriaSalesIQTM__Organization_Master__c orgMast = new AxtriaSalesIQTM__Organization_Master__c();
            orgMast.Name = 'Axtria';
            orgMast.AxtriaSalesIQTM__Org_Level__c = 'Global';
            orgMast.AxtriaSalesIQTM__Parent_Country_Level__c = true;
            insert orgMast;

            AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c();
            country.Name = System.Label.USA;
            country.AxtriaSalesIQTM__Status__c = 'Active';
            country.AxtriaSalesIQTM__Parent_Organization__c = orgMast.Id;            
            insert country;

            AxtriaSalesIQTM__Business_Unit__c bu = new AxtriaSalesIQTM__Business_Unit__c();
            bu.AxtriaSalesIQTM__Code__c = 'RNIP';
            bu.Name = 'Radiology Portfolio';
            insert bu;

            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name = 'ONCO12';
            team.AxtriaSalesIQTM__Business_Unit__c = bu.id;
            team.AxtriaSalesIQTM__Type__c = 'Base';
            insert team;

            AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c();
            ti.Name = 'ONCO21';
            ti.AxtriaSalesIQTM__Team__c = team.Id;
            ti.AxtriaSalesIQTM__IC_EffstartDate__c = System.today() - 5;
            ti.AxtriaSalesIQTM__IC_EffEndDate__c = System.today() + 5;
            ti.AxtriaSalesIQTM__Alignment_Period__c = 'period';
            ti.AxtriaSalesIQTM__Create_Mirror_Position__c = false;
            ti.AxtriaSalesIQTM__Show_Custom_Metric__c = false;
            insert ti;

            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name = 'Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = ti.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = '1,1,1';
            insert po1;

            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name = 'NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = ti.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y01';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y01';
            po.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = '1,1,1';
            insert po;

            AxtriaSalesIQTM__TriggerContol__c tc = new AxtriaSalesIQTM__TriggerContol__c();
            tc.Name = 'UserPositionTrigger';
            tc.AxtriaSalesIQTM__IsStopTrigger__c = true;
            insert tc;

            AxtriaSalesIQTM__User_Access_Permission__c up = new AxtriaSalesIQTM__User_Access_Permission__c();
            up.AxtriaSalesIQTM__Position__c = po.Id;
            up.AxtriaSalesIQTM__Team_Instance__c = ti.Id;
            up.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            up.AxtriaSalesIQTM__Is_Active__c = false;
            insert up;

            AxtriaSalesIQST__Product_Catalog__c prod = new AxtriaSalesIQST__Product_Catalog__c();
            prod.Name = 'ProdTest';
            prod.AxtriaSalesIQST__Team_Instance__c = ti.Id;
            prod.AxtriaSalesIQST__IsActive__c = true;
            prod.AxtriaSalesIQST__Product_Code__c = 'ProdTest';
            prod.AxtriaSalesIQST__Veeva_External_ID__c = 'ProdTest';
            prod.AxtriaSalesIQST__Country_Lookup__c = country.Id;
            insert prod;

            string loggedInPosition = 'abc';

            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(ti.Id, ti.Name));

            RedirectToReport report = new RedirectToReport();

            System.currentPageReference().getParameters().put('buSelected', ti.Id);
            // System.currentPageReference().getParameters().put('selectedTeam', team.Id);
            report.buSelected = ti.Id;
            // zc.selectedTeam = team.Id;
            report.reportChangedA();
            report.teamInstanceChangedA();
            report.openReportA();
            report.openReportR();
            report.ListOfTeamInstance1();
            report.buChanged();


        }
    }

}