public with sharing class SalesIQUtility3 {
     public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistion(string UserID){
        System.debug('@@@ UserID ='+UserID);
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Restrict_On_Alignment__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c,AxtriaSalesIQTM__User__r.Profile.Name, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__team__r.AxtriaSalesIQTM__type__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQTM__Position__r.Name!='Unassigned' order by AxtriaSalesIQTM__Team_Instance__r.Name desc];
        System.debug('@@@ accessRecs.size() = '+accessRecs);
        return accessRecs;
    }
}