global class UpdatePositionCopyScenarioBatch implements Database.Batchable<Sobject>, Database.Stateful{
    public String sourceTI;
    public String newTI;
    public String queryString;
    public UpdatePositionCopyScenarioBatch(String sourceTI, String newTI){
        
        this.sourceTI = sourceTI;
        this.newTI = newTI;
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        queryString = 'select id, IsDummyPOD__c,SAP_Position_Id__c, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =: newTI';
        System.debug('query -'+queryString);
        return Database.getQueryLocator(queryString);
    }
    
    public void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position__c> scope){
        
        map<String, AxtriaSalesIQTM__Position__c> sourcePosMap = new map<String, AxtriaSalesIQTM__Position__c>();
        for(AxtriaSalesIQTM__Position__c obj : [select id,IsDummyPOD__c, SAP_Position_Id__c, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =: sourceTI])
        {
            sourcePosMap.put(obj.AxtriaSalesIQTM__Client_Position_Code__c, obj);
                        
        }
        list<AxtriaSalesIQTM__Position__c> updatePosList = new list<AxtriaSalesIQTM__Position__c>();
        for(AxtriaSalesIQTM__Position__c obj : [select id,IsDummyPOD__c, SAP_Position_Id__c, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =: newTI])
        {
            if(sourcePosMap.get(obj.AxtriaSalesIQTM__Client_Position_Code__c) != null){
                obj.SAP_Position_Id__c = sourcePosMap.get(obj.AxtriaSalesIQTM__Client_Position_Code__c).SAP_Position_Id__c;
                obj.IsDummyPOD__c = sourcePosMap.get(obj.AxtriaSalesIQTM__Client_Position_Code__c).IsDummyPOD__c;
                updatePosList.add(obj);
            }
        }
        
        if(updatePosList.size() >0){
            update updatePosList;
        }

    }
    
    public void finish(Database.BatchableContext BC)
    {
      
      
    }
}