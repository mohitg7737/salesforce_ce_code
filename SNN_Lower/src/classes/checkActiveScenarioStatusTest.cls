@isTest
public class checkActiveScenarioStatusTest{
    
    static testMethod void unitTest1(){
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;   
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2021';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        insert newcreateScenarioObj;
        
        newcreateScenarioObj.AxtriaSalesIQTM__Request_Process_Stage__c = 'Ready';
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Status__c = 'Active';
        update newcreateScenarioObj;
    }
}