global class Batch_Populate_User_Position implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    public String query;
    public Set<String> uapExists;
    public Set<String> posIDs;
    public Set<String> duplicateKey;
    public string countryname;
    public Map<String,String> mapUSerPosTeamInsKey2Id;
       
    global Batch_Populate_User_Position() 
    {

        list<AxtriaSalesIQTM__Position__c> gbUnPos = [SELECT AxtriaSalesIQTM__Is_Global_Unassigned_Position__c,AxtriaSalesIQTM__Is_Unassigned_Position__c FROM AxtriaSalesIQTM__Position__c WHERE Name LIKE '%Unassigned%' and AxtriaSalesIQTM__Is_Unassigned_Position__c = false];
        for(AxtriaSalesIQTM__Position__c pos: gbUnPos)
        {
            pos.AxtriaSalesIQTM__Is_Global_Unassigned_Position__c = true;
            pos.AxtriaSalesIQTM__Is_Unassigned_Position__c = true;
        }
        update gbUnPos;
        System.debug('gbUnPos.size():::: ' +gbUnPos.size());

        
        this.query = 'select id, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Assignment_Status__c =\'Active\'';
        
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=======Query is:::'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){

        database.executeBatch(new Batch_Populate_User_Position());                                                           
       
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {
        
        List<String> allUsers = new List<String>();
        posIDs = new Set<String>();
        duplicateKey = new Set<String>();
        uapExists = new Set<String>();
        mapUSerPosTeamInsKey2Id = new Map<String,String>();
        
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            allUsers.add(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
            posIDs.add(posEmp.AxtriaSalesIQTM__Position__c);
        }

        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c,AxtriaSalesIQST__isCallPlanEnabled__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Position__c in :posIDs];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
            uapExists.add(concat);
            mapUSerPosTeamInsKey2Id.put(concat,uap.Id);         
        }
        system.debug('+++++++++++++ uap Existing----' + uapExists);
        
        Map<String, ID> fedIdToUserId = new Map<String, ID>();
        
        for(User users : [select EmployeeNumber, id from User where EmployeeNumber in :allUsers and IsActive = true])
        {
            if(users.EmployeeNumber != null)
            {
                fedIdToUserId.put(users.EmployeeNumber, users.id);
            }
        }
        
        system.debug('+++++++++++++ fedIdToUserId' + fedIdToUserId);
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPInsert = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPUpdate = new List<AxtriaSalesIQTM__User_Access_Permission__c>();

        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            if(fedIdToUserId.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c))
            {
                String userRec = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
                
                String concat = posEmp.AxtriaSalesIQTM__Position__c + '_' + posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c + '_' + userRec;
                
                if(!uapExists.contains(concat))
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
            
                    uap.AxtriaSalesIQTM__Position__c = posEmp.AxtriaSalesIQTM__Position__c;
                    uap.AxtriaSalesIQTM__User__c = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
                    uap.AxtriaSalesIQTM__Team_Instance__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c;
                    
                    uap.AxtriaSalesIQST__isCallPlanEnabled__c = true;
                    
                   
                    uap.AxtriaSalesIQTM__Map_Access_Position__c = posEmp.AxtriaSalesIQTM__Position__c;
                    uap.AxtriaSalesIQTM__is_Active__c = true;
                    //uap.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
                    uap.AxtriaSalesIQTM__Effective_Start_Date__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                    uap.AxtriaSalesIQTM__Effective_End_Date__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                    uapExists.add(concat);
                    allUAPInsert.add(uap);
                }
                else
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uapExist = new AxtriaSalesIQTM__User_Access_Permission__c(id=mapUSerPosTeamInsKey2Id.get(concat));
                    uapExist.AxtriaSalesIQST__isCallPlanEnabled__c = true;
                    uapExist.AxtriaSalesIQTM__is_Active__c = true;

                    if(!duplicateKey.contains(concat))
                    {
                        allUAPUpdate.add(uapExist);
                        duplicateKey.add(concat);
                    }
                }
            }
        }
        
        system.debug('+++++++++++ Hey'+ allUAPInsert);
        if(allUAPInsert.size() > 0)
            insert allUAPInsert;

        //if(allUAPUpdate.size() > 0)
            //update allUAPUpdate;
        
        
    }

    global void finish(Database.BatchableContext BC) {


        List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select id, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Assignment_Status__c ='Inactive' and LastModifiedDate = TODAY];

        Set<String> userSet = new Set<string>();
        Set<String> posSet = new Set<string>();
        Set<String> inactiveUserPos = new Set<string>();

        for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList)
        {
            userSet.add(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
            posSet.add(posEmp.AxtriaSalesIQTM__Position__c);
            inactiveUserPos.add(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c + '_' +posEmp.AxtriaSalesIQTM__Position__c + '_' +posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c);
        }

        List<AxtriaSalesIQTM__User_Access_Permission__c> uapUpdList = new List<AxtriaSalesIQTM__User_Access_Permission__c>();

        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__User__r.EmployeeNumber,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c,AxtriaSalesIQST__isCallPlanEnabled__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Position__c in :posSet and AxtriaSalesIQTM__Is_Active__c=true and AxtriaSalesIQTM__User__r.EmployeeNumber in :userSet];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concatKey = uap.AxtriaSalesIQTM__User__r.EmployeeNumber + '_' + uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c;
            if(inactiveUserPos.contains(concatKey))
            {
                uap.AxtriaSalesIQTM__is_Active__c = true;
                uapUpdList.add(uap);
            }
        }
        system.debug('+++++++++++++ uap inactive----' + uapUpdList.size());
        
        update uapUpdList;
        database.executeBatch(new BatchCreateGlobalUnassignUserPosition());
    }

}