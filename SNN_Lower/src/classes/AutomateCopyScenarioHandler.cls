/**********************************************************************************************
@author employee Id    : A0858
@date       : 30 Sep 2021
@Author      : Lagnika Sharma
@description : This class is used to automate copy scenario process
Revison(s)  :
**********************************************************************************************/
global with sharing class AutomateCopyScenarioHandler {
	
    public static Boolean isFirstTime = true;
    
    public static void updateSAPPositionID(String sourceTI, String newTI){
        
        UpdatePositionCopyScenarioBatch obj = new UpdatePositionCopyScenarioBatch(sourceTI, newTI);
        Database.executeBatch(obj, 2000);
    }
}