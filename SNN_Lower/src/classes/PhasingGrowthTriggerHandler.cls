//This class is used to prevent editing on those records for which TI is already existing
public class PhasingGrowthTriggerHandler {//with sharing 
    public static void fetchNonEditablePeriodError(Map <id, Phasing_Growth_Percentage__c > MapPGPnew,Map <id, Phasing_Growth_Percentage__c > MapPGPold, List < Phasing_Growth_Percentage__c > ListPGPnew) {
        //public static void fetchNonEditablePeriodError(List < Phasing_Growth_Percentage__c > ListPGPnew,List < Phasing_Growth_Percentage__c > ListPGPold) {
        System.debug('print MapPGPnew--'+MapPGPnew);
        System.debug('print MapPGPold--'+MapPGPold);
        Map<String,String> TIMap = new Map<String,String>();
       	for(AxtriaSalesIQTM__Team_Instance__c ti : [Select id,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,AxtriaSalesIQTM__Team__r.Name  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Status__c = 'Active' and AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c = 'Base'])
        {
            if(ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c.contains('P'))
                TIMap.put(ti.AxtriaSalesIQTM__Team__r.Name+ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,'NonEditable');
            else
                TIMap.put(ti.AxtriaSalesIQTM__Team__r.Name+'P'+ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Fiscal_Period__c+ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c,'NonEditable');
        }
        System.debug('TIMap size and values--'+TIMap.size()+'  -- values--'+TIMap);
        
        if(MapPGPnew != null && MapPGPnew.size()>0)
        {
            for(Phasing_Growth_Percentage__c pgp : MapPGPnew.values())
            {
                System.debug('print pgp--'+pgp);

                if(((pgp.P1_Value__c != MapPGPold.get(pgp.Id).P1_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P1'+pgp.Name)) || ((pgp.P2_Value__c != MapPGPold.get(pgp.Id).P2_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P2'+pgp.Name)) || ((pgp.P3_Value__c != MapPGPold.get(pgp.Id).P3_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P3'+pgp.Name)) || ((pgp.P4_Value__c != MapPGPold.get(pgp.Id).P4_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P4'+pgp.Name)) || ((pgp.P5_Value__c != MapPGPold.get(pgp.Id).P5_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P5'+pgp.Name)) || ((pgp.P6_Value__c != MapPGPold.get(pgp.Id).P6_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P6'+pgp.Name)) || ((pgp.P7_Value__c != MapPGPold.get(pgp.Id).P7_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P7'+pgp.Name)) || ((pgp.P8_Value__c != MapPGPold.get(pgp.Id).P8_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P8'+pgp.Name)) || ((pgp.P9_Value__c != MapPGPold.get(pgp.Id).P9_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P9'+pgp.Name)) || ((pgp.P10_Value__c != MapPGPold.get(pgp.Id).P10_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P10'+pgp.Name)) || ((pgp.P11_Value__c != MapPGPold.get(pgp.Id).P11_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P11'+pgp.Name)) || ((pgp.P12_Value__c != MapPGPold.get(pgp.Id).P12_value__c) && TIMap.containsKey(pgp.Team_Formula__c+'P12'+pgp.Name)))
                {
                    pgp.addError('Editing existing periods is not allowed!!');
                }       
            }
        }
        else
        {
            if(ListPGPnew != null && ListPGPnew.size()>0)
            {
                for(Phasing_Growth_Percentage__c pgp1 : ListPGPnew)
                {
                    if(TIMap.containsKey(pgp1.Team_Formula__c+'P1'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P2'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P3'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P4'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P5'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P6'+pgp1.Name) ||TIMap.containsKey(pgp1.Team_Formula__c+'P7'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P8'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P9'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P10'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P11'+pgp1.Name) || TIMap.containsKey(pgp1.Team_Formula__c+'P12'+pgp1.Name))
                    {
                        System.debug('I am not allowed to insert--'+pgp1);
                        pgp1.addError('Inserting records for year with existing periods is not allowed!!');
                    }
                    else
                    {
                        System.debug('I am allowed to insert--'+pgp1);
                    }

                }
            }
            
        }
        
        
    }
    
}