public class Scheduled_Job_Run_Ctrl {

    public List<SelectOption>                                           jobList             {get;set;}
    public String                                                       selectedJobType     {get;set;}
    public String                                                       selectedJobName     {get;set;}
    public Boolean                                                      pollerBool          {get;set;}
    public List<AxtriaSalesIQST__Scheduler_Log__c>                      scLogList           {get;set;}

    public Scheduled_Job_Run_Ctrl() {
        jobList = new List<SelectOption>();
        jobList.add(new SelectOption('None','--None--'));
        checkStatus();
    }

    public void checkStatus()
    {
        scLogList = new List<AxtriaSalesIQST__Scheduler_Log__c>();
        scLogList = [Select AxtriaSalesIQST__Job_Type__c,AxtriaSalesIQST__Job_Name__c,AxtriaSalesIQST__Job_Status__c,AxtriaSalesIQST__Created_Date2__c,CreatedBy.Name from AxtriaSalesIQST__Scheduler_Log__c order by AxtriaSalesIQST__Created_Date2__c desc LIMIT 10];
        pollerBool = false;
        for(Integer i=0,j=scLogList.size();i<j;i++)
        {
            if(scLogList[i].AxtriaSalesIQST__Job_Status__c == 'Job Initiated' || scLogList[i].AxtriaSalesIQST__Job_Status__c.contains('In Progress'))
            {
                pollerBool = true;
                break;
            }
        }
    }

    public void jobNameRefresh()
    {
        jobList = new List<SelectOption>();
        jobList.add(new SelectOption('None','--None--'));
        if(selectedJobType == 'Inbound')
        {   
            jobList.add(new SelectOption('Employee','Employee'));
            jobList.add(new SelectOption('Workbench Event Creation','Workbench Event Creation'));
            jobList.add(new SelectOption('Account','Account'));
            jobList.add(new SelectOption('Quota','Quota'));
            jobList.add(new SelectOption('Batch Update Quota at Territory Level','Batch Update Quota at Territory Level'));
            jobList.add(new SelectOption('Batch Update Position Account Ship Exception','Batch Update Position Account Ship Exception'));
            jobList.add(new SelectOption('Proxy Quota Calculation','Proxy Quota Calculation'));
            jobList.add(new SelectOption('Merge','Merge'));
            jobList.add(new SelectOption('BatchMerge','BatchMerge'));
            //jobList.add(new SelectOption('Master Data SFDC Push','Master Data SFDC Push'));
        }
        else if(selectedJobType == 'Outbound')
        {
            jobList.add(new SelectOption('Beacon Employee Outbound','Beacon Employee Outbound'));
            jobList.add(new SelectOption('Batch Update Ship To Exception','Batch Update Ship To Exception'));
           
            jobList.add(new SelectOption('WinShuttle','WinShuttle'));
            jobList.add(new SelectOption('Territory','Territory'));
            jobList.add(new SelectOption('UserTerritory','UserTerritory'));
            jobList.add(new SelectOption('AccountTerritory','AccountTerritory'));
        }
    }

    public void runJob()
    {
        if(selectedJobName != 'None')
        {
            
            //IntegrationScheduledClass a = new IntegrationScheduledClass(selectedJobType,selectedJobName);
            IntegrationScheduledClass.RunScheduledJobsSNN(selectedJobType,selectedJobName);
            checkStatus();
        }
    }
    
}