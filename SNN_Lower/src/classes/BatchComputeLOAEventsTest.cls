@isTest
public class BatchComputeLOAEventsTest {
	
     public static testMethod void test1()
    {
    	Beacon_Employee_LOA__c obj = new Beacon_Employee_LOA__c();
        obj.Beacon_User_SFDC_ID__c = '123452021_01';
        obj.Employee_Id__c = '12345';
        obj.Leave_Type__c = '';
        obj.LOA_End_Date__c = Date.newInstance(2021, 10, 01);
        obj.LOA_Start_Date__c = Date.newInstance(2021, 10, 15);
        obj.RecordID__c = '123452021_01';
        obj.Leave_Status__c = 'Current';
        obj.Leave_Type__c = 'Test';
        
        insert obj;
        
        Beacon_Employee_LOA__c obj1 = new Beacon_Employee_LOA__c();
        obj1.Beacon_User_SFDC_ID__c = '1234562021_01';
        obj1.Employee_Id__c = '123456';
        obj1.Leave_Type__c = '';
        obj1.LOA_End_Date__c = Date.newInstance(2021, 10, 01);
        obj1.LOA_Start_Date__c = Date.newInstance(2021, 10, 15);
        obj1.RecordID__c = '1234562021_01';
        obj1.Leave_Status__c = 'Current';
        obj1.Leave_Type__c = 'Test';
        
        insert obj1;
        
         AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c(); 
         emp.AxtriaSalesIQTM__Employee_ID__c = '12345';
         emp.AxtriaSalesIQTM__HR_Status__c = 'Active';
         emp.AxtriaSalesIQTM__Employee_Type__c = 'FT';
         emp.AxtriaSalesIQTM__FirstName__c = 'abc';
         emp.AxtriaSalesIQTM__Last_Name__c = 'abc';
         insert emp;
        
        AxtriaSalesIQTM__Employee__c emp1 = new AxtriaSalesIQTM__Employee__c(); 
         emp1.AxtriaSalesIQTM__Employee_ID__c = '123456';
         emp1.AxtriaSalesIQTM__HR_Status__c = 'Active';
         emp1.AxtriaSalesIQTM__Employee_Type__c = 'FT';
         emp1.AxtriaSalesIQTM__FirstName__c = 'abc';
         emp1.AxtriaSalesIQTM__Last_Name__c = 'abc';
        emp1.First_Day_Of_Leave__c = Date.newInstance(2021, 10, 15);
         insert emp1;
         BatchComputeLOAEvents objBatch = new BatchComputeLOAEvents ();
         Database.executeBatch(objBatch);
    }
}