global class BatchUpdateShipExceptionCRAcc implements Database.Batchable<sObject> {
    
    public string query;
     Map<String,String> MapShipToSold;
     Set<String> accset;
    string ShipAccKey;
     //Map<String,list<AxtriaSalesIQTM__Position_Account__c>>MapAccToPosAcc;
    Set<String> teaminstance;
    Set<String> desPosition;
    Map<String,list<AxtriaSalesIQTM__Position_Account__c>>MapAccToPosAcc;
    public List<AxtriaSalesIQTM__CR_Account__c> UpdatePosAcc;
    Map<String,AxtriaSalesIQTM__CR_Account__c> cRMap;
    set<String> crID = new set<String>();

    
    global BatchUpdateShipExceptionCRAcc(){
        MapShipToSold = new Map<String,String>();
        accset = new Set<String>();
        teaminstance = new Set<String>();
        cRMap =  new Map<String,AxtriaSalesIQTM__CR_Account__c>();
        desPosition = new Set<String>();
        MapAccToPosAcc = new Map<String,list<AxtriaSalesIQTM__Position_Account__c>>();
        UpdatePosAcc =new List<AxtriaSalesIQTM__CR_Account__c>();
        
        
        query='select id,AxtriaSalesIQTM__Account__c,ShipToException__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AssociatedSold__c, AxtriaSalesIQTM__Source_Position__c, AxtriaSalesIQTM__Destination_Position__c, Team_Instance__c from AxtriaSalesIQTM__CR_Account__c where AxtriaSalesIQTM__Account__r.AssociatedSold__c!=null ';
    }
     global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__CR_Account__c> scope)
    {
        MapShipToSold = new Map<String,String>();
        UpdatePosAcc =new List<AxtriaSalesIQTM__CR_Account__c>();
        //cRMap =  new Map<String,AxtriaSalesIQTM__CR_Account__c>();
        accset = new Set<String>();
        teaminstance = new Set<String>();
        desPosition = new Set<String>();
        MapAccToPosAcc = new Map<String,list<AxtriaSalesIQTM__Position_Account__c>>();
        
        
        List<AxtriaSalesIQTM__Position_Account__c> PosAcc = new List<AxtriaSalesIQTM__Position_Account__c>();
        for(AxtriaSalesIQTM__CR_Account__c acc:scope){
            accset.add(acc.AxtriaSalesIQTM__Account__r.AccountNumber);
            accset.add(acc.AxtriaSalesIQTM__Account__r.AssociatedSold__c);
            teaminstance.add(acc.Team_Instance__c);
            desPosition.add(acc.AxtriaSalesIQTM__Destination_Position__c);
            System.debug('1 :'+acc.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+acc.AxtriaSalesIQTM__Destination_Position__c+'_'+acc.Team_Instance__c);
            cRMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+acc.AxtriaSalesIQTM__Destination_Position__c+'_'+acc.Team_Instance__c , acc);
       

            MapShipToSold.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber,acc.AxtriaSalesIQTM__Account__r.AssociatedSold__c);
        }

        System.debug('teaminstance :'+teaminstance);
        
        PosAcc =[select id,AxtriaSalesIQTM__Position__c,Ship_To_Exception__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.name,AxtriaSalesIQTM__Assignment_Status__c  from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Assignment_Status__c !='Inactive' AND AxtriaSalesIQTM__Account__r.AccountNumber IN :accset AND AxtriaSalesIQTM__Team_Instance__r.name IN :teaminstance];
        System.debug('Values of Position Account are   '+PosAcc);
        System.debug('Values of Position Account are   '+PosAcc.size());
        if(PosAcc!=null && PosAcc.size()>0){
           for(AxtriaSalesIQTM__Position_Account__c p : PosAcc){

            if(MapAccToPosAcc.containsKey(p.AxtriaSalesIQTM__Account__r.AccountNumber)){
                list<AxtriaSalesIQTM__Position_Account__c> paList = MapAccToPosAcc.get(p.AxtriaSalesIQTM__Account__r.AccountNumber);
                    paList.add(p);
                MapAccToPosAcc.put(p.AxtriaSalesIQTM__Account__r.AccountNumber,paList);
            }
            else{
                MapAccToPosAcc.put(p.AxtriaSalesIQTM__Account__r.AccountNumber,new list<AxtriaSalesIQTM__Position_Account__c>{p});
            }
            //cRMap.put(p.AxtriaSalesIQTM__Account__r.AccountNumber +'_' +p.AxtriaSalesIQTM__Team_Instance__r.name+'_'+p.AxtriaSalesIQTM__Position__c, p);
           }
        }
        System.debug('Account to Pos Account value is  ' +MapAccToPosAcc);
        for(String ShipAcc : MapShipToSold.keySet())
        {
            AxtriaSalesIQTM__Position_Account__c posacc1= new AxtriaSalesIQTM__Position_Account__c(); 
            list<AxtriaSalesIQTM__Position_Account__c> posacclist= new list<AxtriaSalesIQTM__Position_Account__c>(); 
            System.debug('Ship Account Number is  '+ShipAcc);
            if(MapAccToPosAcc.containsKey(ShipAcc) && MapAccToPosAcc.get(ShipAcc)!=null){
                //posacc1 =MapAccToPosAcc.get(ShipAcc);
                posacclist.addAll(MapAccToPosAcc.get(ShipAcc));
            }
            System.debug('ValueofPossAcc for Ship  '+ posacc1 );
            System.debug('List of Ship Pos Acc  ' +posacclist);
            if(posacclist!=null && posacclist.size()>0){

            for(AxtriaSalesIQTM__Position_Account__c paShip:posacclist)
            {

            //ShipAccKey = paShip.AxtriaSalesIQTM__Account__r.AccountNumber +'_'+ paShip.AxtriaSalesIQTM__Position__c + '_'+ paShip.AxtriaSalesIQTM__Team_Instance__c;
                
                //System.debug('ShipAcctnumber to PosAcc Key  '+ShipAccKey);
                System.debug('pos account record  '+paShip);
                String SoldAccnum = MapShipToSold.get(ShipAcc);
                System.debug('Sold Account Number is '+SoldAccnum);
                
                AxtriaSalesIQTM__Position_Account__c SoldPosac = new AxtriaSalesIQTM__Position_Account__c();
                list<AxtriaSalesIQTM__Position_Account__c> addSoldPosAcc = new list<AxtriaSalesIQTM__Position_Account__c>();
              
                if(MapAccToPosAcc.get(SoldAccnum) != null){
                        //SoldPosac=MapAccToPosAcc.get(SoldAccnum);
                        addSoldPosAcc.addAll(MapAccToPosAcc.get(SoldAccnum));
                }
                System.debug('addSoldPosAcc is ' +addSoldPosAcc);
                   
                    
                    if(addSoldPosAcc!=null && addSoldPosAcc.size()>0)
                    {
                        for(AxtriaSalesIQTM__Position_Account__c paSold:addSoldPosAcc)
                        {
                            System.debug('paShip  ' +paShip);
                            System.debug('paSold  ' +paSold);
                            AxtriaSalesIQTM__CR_Account__c updCr = new AxtriaSalesIQTM__CR_Account__c();
                            System.debug('paShip.AxtriaSalesIQTM__Team_Instance__c '+paShip.AxtriaSalesIQTM__Team_Instance__c);
                            System.debug('paSold.AxtriaSalesIQTM__Team_Instance__c '+paSold.AxtriaSalesIQTM__Team_Instance__c);
                            if(paShip.AxtriaSalesIQTM__Team_Instance__c == paSold.AxtriaSalesIQTM__Team_Instance__c)
                            {
                                if(paShip.AxtriaSalesIQTM__Position__c != paSold.AxtriaSalesIQTM__Position__c)
                                {
                                    //paShip.Ship_To_Exception__c = True;
                                    //UpdatePosAcc.add(paShip);
                                    //AxtriaSalesIQTM__CR_Account__c updCr = new AxtriaSalesIQTM__CR_Account__c();
                                    String pAKey = paShip.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+paShip.AxtriaSalesIQTM__Position__c+'_'+paShip.AxtriaSalesIQTM__Team_Instance__r.name;
                                    System.debug('2 :'+pAKey);
                                    if(cRMap.get(pAKey) != null && !crID.contains(pAKey)){
                                        updCr=cRMap.get(pAKey);
                                        updCr.ShipToException__c = True;
                                        crID.add(pAKey);
                                        UpdatePosAcc.add(updCr);
                                    }
                                }
                                else
                                {

                                    String pAKey = paShip.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+paShip.AxtriaSalesIQTM__Position__c+'_'+paShip.AxtriaSalesIQTM__Team_Instance__r.name;
                                    System.debug('2 :'+pAKey);
                                    if(cRMap.get(pAKey) != null && !crID.contains(pAKey)){
                                        updCr=cRMap.get(pAKey);
                                        updCr.ShipToException__c = false;
                                        crID.add(pAKey);
                                        UpdatePosAcc.add(updCr);
                                    }
                                    System.debug('Updated Pos Acc list  '+UpdatePosAcc);
                                    
                                }
                                

                                
                            }


                            
                        }
                            
                    }
                }
            }
        }
        System.debug('New Updated Pos Acc list  '+UpdatePosAcc);
        Update UpdatePosAcc;
        
    }
        

    global void finish(Database.BatchableContext BC){
    
      try{
            list<AxtriaSalesIQST__Scheduler_Log__c> scLog = new list<AxtriaSalesIQST__Scheduler_Log__c>();
            scLog = [select id, Name, AxtriaSalesIQST__Job_Type__c,AxtriaSalesIQST__Job_Name__c, AxtriaSalesIQST__Job_Status__c from AxtriaSalesIQST__Scheduler_Log__c where AxtriaSalesIQST__Job_Type__c =  'Outbound' AND AxtriaSalesIQST__Job_Name__c =  'Batch Update Ship To Exception' and AxtriaSalesIQST__Job_Status__c = 'Job Initiated' and createdDate = today limit 1];

            scLog[0].AxtriaSalesIQST__Job_Status__c = 'Completed';
            update scLog[0];

          }catch(Exception e){

          }
        
    }
}