global class BatchCreateGlobalUnassignUserPosition implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    public String query;
    public Map<String,String> existingUserPosRec;
    public Map<String,AxtriaSalesIQTM__User_Access_Permission__c> userPosRecordMap ;
    public Map<String,String> UnassignedPosMap  ;

    global BatchCreateGlobalUnassignUserPosition() 
    {
        UnassignedPosMap =  new Map<String,String>();
        existingUserPosRec = new Map<String,String>();


        // Extracting all global unassigned position
        for(AxtriaSalesIQTM__Position__c pos : [Select Id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Is_Global_Unassigned_Position__c = true])
        {

            String key = pos.AxtriaSalesIQTM__Team_Instance__c +'_'+ pos.AxtriaSalesIQTM__Position_Type__c;
            UnassignedPosMap.put(key,pos.id); //map to store team instance wise global unassigned position
        }

        System.debug('::::::UnassignedPosMap::::::::'+UnassignedPosMap);


        // Extracting all global unassigned user position
        for(AxtriaSalesIQTM__User_Access_Permission__c userPos : [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Is_Global_Unassigned_Position__c = true])
        {
            String key = userPos.AxtriaSalesIQTM__Position__c + '_' + userPos.AxtriaSalesIQTM__Team_Instance__c + '_' + userPos.AxtriaSalesIQTM__User__c;
            existingUserPosRec.put(key,userPos.Id);
        }
        System.debug('::::::existingUserPosRec::::::::'+existingUserPosRec);


        
        this.query = 'select id, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Assignment_Status__c =\'Active\'';
        
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=======Query is:::'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){

        database.executeBatch(new BatchCreateGlobalUnassignUserPosition());                                                           
       
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {

        List<String> allEmployeeId = new List<String>();
        Map<String, ID> fedIdToUserId = new Map<String, ID>();
        userPosRecordMap = new  Map<String,AxtriaSalesIQTM__User_Access_Permission__c>();


        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            allEmployeeId.add(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c); //employee id of all active position employees.
        }
        
        for(User users : [select EmployeeNumber, id from User where EmployeeNumber in :allEmployeeId and IsActive = true])
        {
            if(users.EmployeeNumber != null)
            {
                fedIdToUserId.put(users.EmployeeNumber, users.id);
            }
        }

        System.debug('::::::::fedIdToUserId:::::::'+fedIdToUserId);
        
        
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
        if(fedIdToUserId.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c)){
            String territorytype = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c +'_'+ posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;

            System.debug('posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c +\'_\'+ posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c:::: '+posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c +'_'+ posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c);

            System.debug('Team instance Id :::: '+posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c);

            if(UnassignedPosMap.containsKey(territorytype))
            {
                String unassignedPositionId = UnassignedPosMap.get(territorytype);
                System.debug('Unassigned position Id of above team instance :::: '+unassignedPositionId);

                String userRec = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);


                String uniquekey = unassignedPositionId + '_' + posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c + '_' + userRec;

                
                System.debug('::::::::posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c::::::::::'+posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
                System.debug('::::::::uniquekey::::::::::'+uniquekey);

                if(!existingUserPosRec.containsKey(uniquekey))
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
            
                    uap.AxtriaSalesIQTM__Position__c = unassignedPositionId;
                    uap.AxtriaSalesIQTM__User__c = userRec;
                    uap.AxtriaSalesIQTM__Team_Instance__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c;
                    //uap.isCallPlanEnabled__c = true;
                    uap.AxtriaSalesIQTM__Map_Access_Position__c = unassignedPositionId;
                    uap.AxtriaSalesIQTM__is_Active__c = true;
                    //uap.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
                    uap.AxtriaSalesIQTM__Effective_Start_Date__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                    uap.AxtriaSalesIQTM__Effective_End_Date__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;

                    userPosRecordMap.put(uniquekey, uap);
                }
                else
                {
                    AxtriaSalesIQTM__User_Access_Permission__c userpos = new AxtriaSalesIQTM__User_Access_Permission__c(id=existingUserPosRec.get(uniquekey));

                    userpos.AxtriaSalesIQTM__is_Active__c = true;
                    userPosRecordMap.put(uniquekey, userpos);
                }
            }

        }
        }

        if(!userPosRecordMap.isEmpty())
        {
            upsert userPosRecordMap.values();
        }
    }

    global void finish(Database.BatchableContext BC) {
    

    }
}