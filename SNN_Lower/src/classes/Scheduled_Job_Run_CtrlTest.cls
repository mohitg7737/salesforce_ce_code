@isTest
private class Scheduled_Job_Run_CtrlTest{

    private static testMethod void test1(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'Event Creation';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Inbound';
        objSc.selectedJobName = 'Event Creation';
        objSc.jobNameRefresh();
        objSc.runJob();
    }

}