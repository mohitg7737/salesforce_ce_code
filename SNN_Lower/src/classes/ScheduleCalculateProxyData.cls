global class ScheduleCalculateProxyData implements Schedulable{
   
    global ScheduleCalculateProxyData(){}
    
    global void execute(System.SchedulableContext SC){
        List<String> lstTeamInstance = new List<String>();
        for(AxtriaSalesIQTM__Team_Instance__c objTeamInstance: [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where (AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Scenario__c != null and (NOT(Name like '%test%' OR Name like '%delete%' OR Name like '%old%'))]){
            lstTeamInstance.add(objTeamInstance.Id);
        }
        Database.executeBatch(new BatchupdatePA(lstTeamInstance),2000); 
    }  
}