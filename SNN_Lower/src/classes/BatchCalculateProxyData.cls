global class BatchCalculateProxyData implements Database.Batchable<sObject>{
    public String query;
    public String teamInstanceId;
    public AxtriaSalesIQST__Scheduler_Log__c scLog;
    public List<String> lstTeamInstanceId;
    public Map<String, String> mapOfFiscalPeriodToPSalesField;
    public String LastModCondition;

    global BatchCalculateProxyData(String teamInstanceId,AxtriaSalesIQST__Scheduler_Log__c scLog1){
        this.teamInstanceId = teamInstanceId;
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = [Select Id, Fiscal_Period__c from AxtriaSalesIQTM__Team_Instance__c where Id=: teamInstanceId and Fiscal_Period__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c != null];
        String fiscalPeriod;
        if(objTeamInstance != null)
           fiscalPeriod = objTeamInstance.Fiscal_Period__c;
        System.debug('fiscalPeriod>>>>>>>>>'+fiscalPeriod); 
        mapOfFiscalPeriodToPSalesField = new Map<String, String>();
        for(FiscalPeriodToSalesField__mdt objMdt: [Select Fiscal_Period__c, ProxyFieldName__c from FiscalPeriodToSalesField__mdt]){
            if(objMdt.Fiscal_Period__c.equals(fiscalPeriod))
              mapOfFiscalPeriodToPSalesField.put(objMdt.Fiscal_Period__c, objMdt.ProxyFieldName__c);
        }

        query = 'select Id, AxtriaSalesIQTM__Team_Instance__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric11__c, AxtriaSalesIQTM__Metric12__c, AxtriaSalesIQTM__Metric13__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.PY_C1_Sales__c, AxtriaSalesIQTM__Account__r.PY_C2_Sales__c, AxtriaSalesIQTM__Account__r.PY_C3_Sales__c, AxtriaSalesIQTM__Account__r.PY_C4_Sales__c, AxtriaSalesIQTM__Account__r.PY_C5_Sales__c, AxtriaSalesIQTM__Account__r.PY_C6_Sales__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Team_Instance__c=: teamInstanceId and AxtriaSalesIQTM__Account__c = \'0015e00000VHSDA\'';
        scLog = new AxtriaSalesIQST__Scheduler_Log__c() ;
        scLog=scLog1;
        
    }

    global BatchCalculateProxyData(List<String> teamInstanceIds,AxtriaSalesIQST__Scheduler_Log__c scLog1){
        lstTeamInstanceId = new List<String>(teamInstanceIds);
        System.debug('lstTeamInstanceId>>>>>>>>>'+lstTeamInstanceId);
        mapOfFiscalPeriodToPSalesField = new Map<String, String>();
        for(FiscalPeriodToSalesField__mdt objMdt: [Select Fiscal_Period__c, ProxyFieldName__c from FiscalPeriodToSalesField__mdt]){
            mapOfFiscalPeriodToPSalesField.put(objMdt.Fiscal_Period__c, objMdt.ProxyFieldName__c);
        }        
        query = 'select Id, AxtriaSalesIQTM__Team_Instance__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric11__c, AxtriaSalesIQTM__Metric12__c, AxtriaSalesIQTM__Metric13__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.PY_C1_Sales__c, AxtriaSalesIQTM__Account__r.PY_C2_Sales__c, AxtriaSalesIQTM__Account__r.PY_C3_Sales__c, AxtriaSalesIQTM__Account__r.PY_C4_Sales__c, AxtriaSalesIQTM__Account__r.PY_C5_Sales__c, AxtriaSalesIQTM__Account__r.PY_C6_Sales__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Team_Instance__c in: lstTeamInstanceId and AxtriaSalesIQTM__Account__c = \'0015e00000VHSDA\'';
        //and AxtriaSalesIQTM__Account__c = \'0010200000BiJRrAAN\'        
        scLog = new AxtriaSalesIQST__Scheduler_Log__c() ;
        scLog=scLog1;
    }
    global BatchCalculateProxyData(List<String> teamInstanceIds,AxtriaSalesIQST__Scheduler_Log__c scLog1,String LastModCondition1){
        lstTeamInstanceId = new List<String>(teamInstanceIds);
        System.debug('lstTeamInstanceId>>>>>>>>>'+lstTeamInstanceId);
        mapOfFiscalPeriodToPSalesField = new Map<String, String>();
        for(FiscalPeriodToSalesField__mdt objMdt: [Select Fiscal_Period__c, ProxyFieldName__c from FiscalPeriodToSalesField__mdt]){
            mapOfFiscalPeriodToPSalesField.put(objMdt.Fiscal_Period__c, objMdt.ProxyFieldName__c);
        }        
        query = 'select Id, AxtriaSalesIQTM__Team_Instance__r.Fiscal_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric11__c, AxtriaSalesIQTM__Metric12__c, AxtriaSalesIQTM__Metric13__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.PY_C1_Sales__c, AxtriaSalesIQTM__Account__r.PY_C2_Sales__c, AxtriaSalesIQTM__Account__r.PY_C3_Sales__c, AxtriaSalesIQTM__Account__r.PY_C4_Sales__c, AxtriaSalesIQTM__Account__r.PY_C5_Sales__c, AxtriaSalesIQTM__Account__r.PY_C6_Sales__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Team_Instance__c in: lstTeamInstanceId';
        //and AxtriaSalesIQTM__Account__c = \'0010200000BiJRrAAN\'  
        LastModCondition = LastModCondition1;
        query+= ' and AxtriaSalesIQTM__Account__r.LastModifiedDate='+LastModCondition;
              
        scLog = new AxtriaSalesIQST__Scheduler_Log__c() ;
        scLog=scLog1;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){       
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Account__c> lstPositionAccount){
         try{
             System.debug('lstPositionAccount>>>>>>>>>>'+lstPositionAccount);
             System.debug('lstPositionAccount size>>>>>>>>>>'+lstPositionAccount.size());
             String phasingGrowthQuery = 'select Id, Name, P1_Value__c, P2_Value__c, P3_Value__c, P4_Value__c, P5_Value__c, P6_Value__c, P7_Value__c, P8_Value__c, P9_Value__c, P10_Value__c, P11_Value__c, P12_Value__c, Product_Category__c, Type__c from Phasing_Growth_Percentage__c order by Type_Product_Category__c DESC';
             List<SObject> lstPhasingGrowth = Database.query(phasingGrowthQuery);
             System.debug('mapOfFiscalPeriodToPSalesField>>>>>>>>>>>'+mapOfFiscalPeriodToPSalesField);            
             System.debug('lstPhasingGrowth>>>>>>>>>>'+lstPhasingGrowth);
             System.debug('lstPhasingGrowth size>>>>>>>>>>'+lstPhasingGrowth.size());
             List<AxtriaSalesIQTM__Position_Account__c> listToUpdatePosAcc = new List<AxtriaSalesIQTM__Position_Account__c>();
             for(AxtriaSalesIQTM__Position_Account__c objPosAcc: lstPositionAccount){
                 Decimal c1Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C1_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C1_Sales__c : 0.0;
                 Decimal c2Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C2_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C2_Sales__c : 0.0;
                 Decimal c3Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C3_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C3_Sales__c : 0.0;
                 Decimal c4Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C4_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C4_Sales__c : 0.0;
                 Decimal c5Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C5_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C5_Sales__c : 0.0;
                 Decimal c6Sales = objPosAcc.AxtriaSalesIQTM__Account__r.PY_C6_Sales__c != null ? objPosAcc.AxtriaSalesIQTM__Account__r.PY_C6_Sales__c : 0.0;
                 Decimal totalc1Sales, totalc2Sales, totalc3Sales, totalc4Sales, totalc5Sales, totalc6Sales = 0.0;
                 String workspacefiscalPeriod = objPosAcc.AxtriaSalesIQTM__Team_Instance__r.Fiscal_Period__c;
                 String workspaceyear = objPosAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Year__c;
                 System.debug('workspacefiscalPeriod>>>>>>>>>>'+workspacefiscalPeriod);
                 String pSalesFieldName = mapOfFiscalPeriodToPSalesField.get(workspacefiscalPeriod) != null ? mapOfFiscalPeriodToPSalesField.get(workspacefiscalPeriod) : '';
                 System.debug('pSalesFieldName>>>>>>>>>>'+pSalesFieldName);
                 System.debug('C2 Sales Value>>>>>>>>>>'+c2Sales);
                 System.debug('C3 Sales Value>>>>>>>>>>'+c3Sales);
                 System.debug('C4 Sales Value>>>>>>>>>>'+c4Sales);
                 for(SObject sobj: lstPhasingGrowth){
                    String phasingGrowthYear = String.valueOf(sobj.get('Name'));
                    if(phasingGrowthYear.equals(workspaceyear)){
                        Decimal pSelectedField = (Decimal)sobj.get(pSalesFieldName);
                        String productCategory = String.valueOf(sobj.get('Product_Category__c'));
                        String type = String.valueOf(sobj.get('Type__c'));
                        if(String.isNotBlank(type) && type.equalsIgnoreCase('Phasing %')){
                            System.debug('pSelectedField>>>>>>>>>>'+pSelectedField);
                          if(productCategory.contains('1'))
                            totalc1Sales = c1Sales * pSelectedField/100.0;
                          if(productCategory.contains('2'))
                            totalc2Sales = c2Sales * pSelectedField/100.0;
                          if(productCategory.contains('3'))
                            totalc3Sales = c3Sales * pSelectedField/100.0;
                          if(productCategory.contains('4'))
                            totalc4Sales = c4Sales * pSelectedField/100.0;
                          if(productCategory.contains('5'))
                            totalc5Sales = c5Sales * pSelectedField/100.0;
                          if(productCategory.contains('6'))
                            totalc6Sales = c6Sales * pSelectedField/100.0;
                              
                        }
                        System.debug('totalc2Sales>>>>>>>>>>'+totalc2Sales);
                        System.debug('totalc3Sales>>>>>>>>>>'+totalc3Sales);
                        System.debug('totalc4Sales>>>>>>>>>>'+totalc4Sales);
                        if(String.isNotBlank(type) && type.equalsIgnoreCase('Growth %')){
                          if(productCategory.contains('1'))
                            totalc1Sales = totalc1Sales * (1 + pSelectedField/100.0);                  
                          if(productCategory.contains('2'))
                            totalc2Sales = totalc2Sales * (1 + pSelectedField/100.0);
                          if(productCategory.contains('3'))
                            totalc3Sales = totalc3Sales * (1 + pSelectedField/100.0);
                          if(productCategory.contains('4'))
                            totalc4Sales = totalc4Sales * (1 + pSelectedField/100.0);
                          if(productCategory.contains('5'))
                            totalc5Sales = totalc5Sales * (1 + pSelectedField/100.0);
                          if(productCategory.contains('6'))
                            totalc6Sales = totalc6Sales * (1 + pSelectedField/100.0);
                        }
                        System.debug('totalc2Sales>>>>Growth>>>>>>'+totalc2Sales);
                        System.debug('totalc3Sales>>>>Growth>>>>>>'+totalc3Sales);
                        System.debug('totalc4Sales>>>>Growth>>>>>>'+totalc4Sales);
                    }
                 }
                 objPosAcc.AxtriaSalesIQTM__Metric8__c = totalc1Sales;
                 objPosAcc.AxtriaSalesIQTM__Metric9__c = totalc2Sales;
                 objPosAcc.AxtriaSalesIQTM__Metric10__c = totalc3Sales;
                 objPosAcc.AxtriaSalesIQTM__Metric11__c = totalc4Sales;
                 objPosAcc.AxtriaSalesIQTM__Metric12__c = totalc5Sales;
                 objPosAcc.AxtriaSalesIQTM__Metric13__c = totalc6Sales;
                 listToUpdatePosAcc.add(objPosAcc); 
             }
             System.debug('listToUpdatePosAcc>>>>>>>>>>>'+listToUpdatePosAcc);
             System.debug('listToUpdatePosAcc size>>>>>>>>>>>'+listToUpdatePosAcc.size());
             if(listToUpdatePosAcc != null && listToUpdatePosAcc.size() > 0)
                Database.update(listToUpdatePosAcc, false);
         }catch(Exception e){
            System.debug('exception>>>>>>>>>>>'+e + ' ' + e.getLineNumber());
         }                     
    }

    global void finish(Database.BatchableContext bc){
        System.debug('****** finish lstTeamInstanceId********'+lstTeamInstanceId);
        System.debug('****** finish teamInstanceId********'+teamInstanceId);        
        if(teamInstanceId != null){
            Database.executeBatch(new batchdeleteCIMPosMetricdata(teamInstanceId,scLog ),2000);
        }
        else if(lstTeamInstanceId != null && lstTeamInstanceId.size() > 0){
                Database.executeBatch(new batchdeleteCIMPosMetricdata(lstTeamInstanceId,scLog ),2000);
        }
        /*try{
                Id tiID = teamInstanceId;
                delete[Select Id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__Team_Instance__c =: tiID];
                Map<String,List<String>> cimMap = new Map<String,List<String>>();
                               
                for(AxtriaSalesIQTM__CIM_Config__c cim : [Select ID,AxtriaSalesIQTM__Team_Instance__c,Name from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c = : tiID and AxtriaSalesIQTM__Enable__c=true and (AxtriaSalesIQTM__Hierarchy_Level__c ='1' or AxtriaSalesIQTM__Hierarchy_Level__c = null or AxtriaSalesIQTM__Hierarchy_Level__c = '')]){
                    if(cimMap.containsKey(cim.AxtriaSalesIQTM__Team_Instance__c)){
                        cimMap.get(cim.AxtriaSalesIQTM__Team_Instance__c).add(cim.id);
                    }else{
                        list<String> cimId = new list<String>();
                        cimId.add(cim.Id);
                        cimMap.put(cim.AxtriaSalesIQTM__Team_Instance__c,cimId);
                    }
                }                
                Database.executeBatch(new AxtriaSalesIQST.BatchCIMPositionMatrixSummaryUpdate(cimMap.get(tiID),tiID ),1000);
                scLog.AxtriaSalesIQST__Job_Status__c = 'Successfull';
                update scLog;                     
        }catch(Exception exc){
            scLog.AxtriaSalesIQST__Job_Status__c = 'Error';
            update scLog;
        }*/
    }    
}