@isTest
public class BatchUpdatePATest{

    public static testMethod void test1()
    {
        Account acc = new Account();
        acc.AccountNumber='12345';
        acc.Name='ABCDTest';
        acc.AxtriaSalesIQST__Marketing_Code__c='IN';
        acc.PY_C1_Sales__c=100;
        acc.PY_C2_Sales__c=101;
        acc.PY_C3_Sales__c=102;
        acc.PY_C4_Sales__c=103;
        acc.PY_C5_Sales__c=104;
        acc.PY_C6_Sales__c=105;
        acc.PQ_C1_Sales__c=106;
        acc.PQ_C2_Sales__c=107;
        acc.PQ_C3_Sales__c=108;
        acc.PQ_C4_Sales__c=109;
        acc.CY_C1_Sales__c=110;
        acc.CY_C2_Sales__c=111;
        acc.CY_C3_Sales__c=112;
        acc.CY_C4_Sales__c=113;
        acc.CY_C5_Sales__c=114;
        acc.CY_C6_Sales__c=115;
        acc.PQ_C5_Sales__c=116;
        acc.PQ_C6_Sales__c=117;
        insert acc;
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert objTeam;
        
        AxtriaSalesIQTM__Team__c Team2 = new AxtriaSalesIQTM__Team__c();
        Team2.Name='ENT';
        Team2.AxtriaSalesIQTM__Type__c ='Base'; 
        Team2.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        Team2.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert Team2;
        
        AxtriaSalesIQTM__Team__c Team3 = new AxtriaSalesIQTM__Team__c();
        Team3.Name='Biologics';
        Team3.AxtriaSalesIQTM__Type__c ='Base'; 
        Team3.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        Team3.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert Team3;
        
        AxtriaSalesIQTM__Team__c Team4 = new AxtriaSalesIQTM__Team__c();
        Team4.Name='Capital Sales';
        Team4.AxtriaSalesIQTM__Type__c ='Base'; 
        Team4.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        Team4.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert Team4;

        
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance2 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance2.name = 'ENT_2016';
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance2.AxtriaSalesIQTM__Team__c = team2.id;
        objTeamInstance2.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance2.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-000012'; 
        
        insert objTeamInstance2 ;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance3 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance3.name = 'Bio_2016';
        objTeamInstance3.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance3.AxtriaSalesIQTM__Team__c = team3.id;
        objTeamInstance3.AxtriaSalesIQTM__Alignment_Type__c = 'Overlay';
        objTeamInstance3.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00003'; 
        
        insert objTeamInstance3;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance4 = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance4.name = 'Cap_2016';
        objTeamInstance4.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance4.AxtriaSalesIQTM__Team__c = team4.id;
        objTeamInstance4.AxtriaSalesIQTM__Alignment_Type__c = 'Overlay';
        objTeamInstance4.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00004'; 
        
        insert objTeamInstance4;

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.name = 'New York, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();

        pos2.name = 'New York, NY';
        pos2.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos2.AxtriaSalesIQTM__Position_Type__c='District';
        pos2.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos2.AxtriaSalesIQTM__inactive__c = false;
        pos2.AxtriaSalesIQTM__Team_iD__c = team2.id;
         pos2.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos2.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos2.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos2.axtriasalesIQTM__Team_Instance__C = objTeamInstance2.Id;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos2;
        
        AxtriaSalesIQTM__Position__c pos3 = new AxtriaSalesIQTM__Position__c();

        pos3.name = 'New York, NY';
        pos3.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos3.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos3.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos3.AxtriaSalesIQTM__Position_Type__c='District';
        pos3.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos3.AxtriaSalesIQTM__inactive__c = false;
        pos3.AxtriaSalesIQTM__Team_iD__c = team3.id;
         pos3.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos3.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos3.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos3.axtriasalesIQTM__Team_Instance__C = objTeamInstance3.Id;
        pos3.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos3;
        
        AxtriaSalesIQTM__Position__c pos4 = new AxtriaSalesIQTM__Position__c();

        pos4.name = 'New York, NY';
        pos4.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos4.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos4.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos4.AxtriaSalesIQTM__Position_Type__c='District';
        pos4.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos4.AxtriaSalesIQTM__inactive__c = false;
        pos4.AxtriaSalesIQTM__Team_iD__c = team4.id;
         pos4.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos4.AxtriaSalesIQTM__Effective_End_Date__c  = date.today()+5;
        pos4.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos4.axtriasalesIQTM__Team_Instance__C = objTeamInstance4.Id;
        pos4.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        
        insert pos4;
        
        
        AxtriaSalesIQTM__Position_Account__c  pA= new AxtriaSalesIQTM__Position_Account__c();
        pA.AxtriaSalesIQTM__Position__c=pos.id;
        pA.AxtriaSalesIQTM__Account__c=acc.id;
        pA.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        pA.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA;
        
        AxtriaSalesIQTM__Position_Account__c  pA2= new AxtriaSalesIQTM__Position_Account__c();
        pA2.AxtriaSalesIQTM__Position__c=pos2.id;
        pA2.AxtriaSalesIQTM__Account__c=acc.id;
        pA2.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance2.id;
        pA2.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA2.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA2;
        
        AxtriaSalesIQTM__Position_Account__c  pA3= new AxtriaSalesIQTM__Position_Account__c();
        pA3.AxtriaSalesIQTM__Position__c=pos3.id;
        pA3.AxtriaSalesIQTM__Account__c=acc.id;
        pA3.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance3.id;
        pA3.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA3.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA3;
        
        AxtriaSalesIQTM__Position_Account__c  pA4= new AxtriaSalesIQTM__Position_Account__c();
        pA4.AxtriaSalesIQTM__Position__c=pos4.id;
        pA4.AxtriaSalesIQTM__Account__c=acc.id;
        pA4.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance4.id;
        pA4.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5; 
        pA4.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA4;
        
        List<String> lstTeamInstance = new List<String>();
        lstTeamInstance.add(objTeamInstance4.Id);
        Test.startTest();
        database.executeBatch(new BatchupdatePA(lstTeamInstance),2000);
        Test.stopTest();
        
    }
    
    public static testMethod void test2()
    {
        Account acc = new Account();
        acc.AccountNumber='12345';
        acc.Name='ABCDTest';
        acc.AxtriaSalesIQST__Marketing_Code__c='IN';
        acc.PY_C1_Sales__c=100;
        acc.PY_C2_Sales__c=101;
        acc.PY_C3_Sales__c=102;
        acc.PY_C4_Sales__c=103;
        acc.PY_C5_Sales__c=104;
        acc.PY_C6_Sales__c=105;
        acc.PQ_C1_Sales__c=106;
        acc.PQ_C2_Sales__c=107;
        acc.PQ_C3_Sales__c=108;
        acc.PQ_C4_Sales__c=109;
        acc.CY_C1_Sales__c=110;
        acc.CY_C2_Sales__c=111;
        acc.CY_C3_Sales__c=112;
        acc.CY_C4_Sales__c=113;
        acc.CY_C5_Sales__c=114;
        acc.CY_C6_Sales__c=115;
        acc.PQ_C5_Sales__c=116;
        acc.PQ_C6_Sales__c=117;
        insert acc;

        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='ENT';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);        
        insert objTeam;
      
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';         
        insert objTeamInstance;


        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'New York, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';       
        insert pos;        
        
        AxtriaSalesIQTM__Position_Account__c  pA= new AxtriaSalesIQTM__Position_Account__c();
        pA.AxtriaSalesIQTM__Position__c=pos.id;
        pA.AxtriaSalesIQTM__Account__c=acc.id;
        pA.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        pA.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA;       
        
        List<String> lstTeamInstance = new List<String>();
        lstTeamInstance.add(objTeamInstance.Id);
        Test.startTest();
        database.executeBatch(new BatchupdatePA(lstTeamInstance),2000);
        Test.stopTest();
        
    }
    
    public static testMethod void test3()
    {
        Account acc = new Account();
        acc.AccountNumber='12345';
        acc.Name='ABCDTest';
        acc.AxtriaSalesIQST__Marketing_Code__c='IN';
        acc.PY_C1_Sales__c=100;
        acc.PY_C2_Sales__c=101;
        acc.PY_C3_Sales__c=102;
        acc.PY_C4_Sales__c=103;
        acc.PY_C5_Sales__c=104;
        acc.PY_C6_Sales__c=105;
        acc.PQ_C1_Sales__c=106;
        acc.PQ_C2_Sales__c=107;
        acc.PQ_C3_Sales__c=108;
        acc.PQ_C4_Sales__c=109;
        acc.CY_C1_Sales__c=110;
        acc.CY_C2_Sales__c=111;
        acc.CY_C3_Sales__c=112;
        acc.CY_C4_Sales__c=113;
        acc.CY_C5_Sales__c=114;
        acc.CY_C6_Sales__c=115;
        acc.PQ_C5_Sales__c=116;
        acc.PQ_C6_Sales__c=117;
        insert acc;

        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='ENT';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);        
        insert objTeam;
      
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';         
        insert objTeamInstance;


        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'New York, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';       
        insert pos;        
        
        AxtriaSalesIQTM__Position_Account__c  pA= new AxtriaSalesIQTM__Position_Account__c();
        pA.AxtriaSalesIQTM__Position__c=pos.id;
        pA.AxtriaSalesIQTM__Account__c=acc.id;
        pA.AxtriaSalesIQTM__Team_Instance__c=objTeamInstance.id;
        pA.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-5;
        pA.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+5;
        insert pA;       
               
        Test.startTest();
        database.executeBatch(new BatchupdatePA(),2000);
        Test.stopTest();
        
    }
    
    public static testMethod void test6()
    {
    BatchUpdatePA A = new BatchUpdatePA();
    A.dummyFunction();
    }
}