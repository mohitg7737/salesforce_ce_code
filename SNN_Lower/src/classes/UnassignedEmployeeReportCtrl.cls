public Without Sharing class UnassignedEmployeeReportCtrl {
    list<AxtriaSalesIQTM__Employee__c> empList; 
    public List<EmpWrapper> wrapperlist {get; set;}
    public Integer totalCount {get;set;}
    public UnassignedEmployeeReportCtrl(){
        String status = 'Active';
        empList = new list<AxtriaSalesIQTM__Employee__c>();
        wrapperlist = new List<EmpWrapper>();
        
       empList = [select id, AxtriaSalesIQTM__Employee_ID__c,Name,AxtriaSalesIQTM__Cost_Center__c,AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Email__c,AxtriaSalesIQTM__SalesforceUserName__c,AxtriaSalesIQTM__Company__c,Business_Unit__c,Business_Title__c,AxtriaSalesIQTM__Job_Title__c,AxtriaSalesIQST__Job_Title__c,Job_Title_Code__c,AxtriaSalesIQTM__Joining_Date__c,Job_Profile__c,Time_in_Job_Profile_Start_Date__c,Position_Start_Date__c,Position_In_Organization__c,AxtriaSalesIQST__Manager_Name__c,AxtriaSalesIQST__ReportsToAssociateOID__c,AxtriaSalesIQTM__Original_Hire_Date__c,AxtriaSalesIQTM__Rehire_Date__c,Is_Rehire__c,AxtriaSalesIQTM__HR_Status__c,AxtriaSalesIQTM__HR_Termination_Date__c,Management_Level__c from AxtriaSalesIQTM__Employee__c  where  AxtriaSalesIQTM__HR_Status__c =: status AND  id not in (select AxtriaSalesIQTM__Employee__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Effective_Start_Date__c <: System.today() and AxtriaSalesIQTM__Effective_End_Date__c >: System.today()) WITH SECURITY_ENFORCED];
        
       
        totalCount = empList.size();
        for(AxtriaSalesIQTM__Employee__c emp: empList){
            EmpWrapper obj = new EmpWrapper();
            obj.employeeNumber = emp.AxtriaSalesIQTM__Employee_ID__c;
            obj.empName = emp.Name;
            obj.costCenterID = emp.AxtriaSalesIQTM__Cost_Center__c;
            obj.firstName = emp.AxtriaSalesIQTM__FirstName__c;
            obj.lastName = emp.AxtriaSalesIQTM__Last_Name__c;
            obj.email = emp.AxtriaSalesIQTM__Email__c;
            obj.userName = emp.AxtriaSalesIQTM__SalesforceUserName__c;
            obj.company = emp.AxtriaSalesIQTM__Company__c;
            obj.businessUnit = emp.Business_Unit__c;
            obj.jobTitle = emp.AxtriaSalesIQTM__Job_Title__c;
            obj.jobProfile = emp.Job_Profile__c;
            obj.timeinJobProfile = getFormattedDate(emp.Time_in_Job_Profile_Start_Date__c);
            obj.positionStartDate = getFormattedDate(emp.Position_Start_Date__c);
            obj.positionInOrganisation = emp.Position_In_Organization__c;
            obj.businessTitle = emp.Business_Title__c;
            obj.managerName = emp.AxtriaSalesIQST__Manager_Name__c;
            obj.managerID = emp.AxtriaSalesIQST__ReportsToAssociateOID__c;
            obj.originalHireDate = getFormattedDate(emp.AxtriaSalesIQTM__Joining_Date__c);
            obj.hireDate = getFormattedDate(emp.AxtriaSalesIQTM__Rehire_Date__c);
            obj.isRehire = emp.Is_Rehire__c;
            obj.status = emp.AxtriaSalesIQTM__HR_Status__c;
            obj.termDate = getFormattedDate(emp.AxtriaSalesIQTM__HR_Termination_Date__c);
            obj.mgmtLevel = emp.Management_Level__c;
            wrapperlist.add(obj);
        }
    }    
    
    
    public String getFormattedDate(Date dToday)
    {
        if(dToday != null){
            DateTime dt = DateTime.newInstance(dToday.year(), dToday.month(),dToday.day());
            return dt.format('MM/dd/yyyy'); 
        }
        else 
            return '';
        
    }
    
    
    public Class EmpWrapper
    {
        public  String employeeNumber {get; set;}
        public  String empName {get; set;}
        public  String costCenterID {get; set;}
        public  String firstName {get; set;}
        public  String lastName {get; set;}
        public  String email {get; set;}
        public  String userName {get; set;}
        public  String company {get; set;}
        public  String businessUnit {get; set;}
        public  String jobTitle {get; set;}
        public  String jobProfile {get; set;}
        public  String timeinJobProfile {get; set;}
        public  String positionStartDate {get; set;}
        public  String positionInOrganisation {get; set;}
        public  String businessTitle {get; set;}
        public  String managerName {get; set;}
        public  String managerID {get; set;}
        public  String originalHireDate {get; set;}
        public  String hireDate {get; set;}
        public  boolean isRehire {get; set;}
        public  String status {get; set;}
        public  String termDate {get; set;}
        public  String mgmtLevel {get; set;}
        
    }

}