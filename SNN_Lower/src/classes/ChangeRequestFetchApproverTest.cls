@isTest
public class ChangeRequestFetchApproverTest{
    
     static testMethod void unitTest1(){        
   
            AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert triggerControl;
                 
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name= 'Test Team';
            team.AxtriaSalesIQTM__Type__c = 'Base';
            team.AxtriaSalesIQTM__Effective_End_Date__c = Date.parse('12/31/4000');
            insert team;
        
            AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
            teamins.Name = 'ENT';
            teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
            teamins.AxtriaSalesIQTM__Team__c = team.Id;
            teamins.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
            teamins.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
            teamins.AxtriaSalesIQTM__Team_Instance_Code__c ='TI-001';
            insert teamins;
            
            AxtriaSalesIQTM__Position__c po2 = new AxtriaSalesIQTM__Position__c();
            po2.Name='Nation';
            po2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po2.AxtriaSalesIQTM__Team_iD__c = team.id;
            po2.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po2.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
            po2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po2.AxtriaSalesIQTM__RGB__c= 'X';
            insert po2;
            
            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name='Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
            po1.AxtriaSalesIQTM__Parent_Position__c = po2.Id;
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = 'X';
            insert po1;
    
            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name='NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = 'x';
            insert po;
            
            AxtriaSalesIQTM__User_Access_Permission__c userPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
            userPerm.AxtriaSalesIQTM__Position__c = po.Id;
            userPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
            userPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            userPerm.AxtriaSalesIQTM__Is_Active__c = true;
            insert userPerm; 
           
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.AxtriaSalesIQTM__Request_Type_Change__c = 'Employee NewHire';
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            changeRequest.AxtriaSalesIQTM__Change_Effective_Date__c = Date.parse('09/20/2019');
            changeRequest.AxtriaSalesIQTM__Is_Auto_Approved__c = false;
            changeRequest.AxtriaSalesIQTM__Source_Position__c = po1.Id;
            changeRequest.AxtriaSalesIQTM__Destination_Position__c = po2.Id;
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            insert changeRequest;    
         
          ChangeRequestFetchApprover obj = new ChangeRequestFetchApprover();
         obj.dummyMethod();
      } 
      
      static testMethod void unitTest2(){        
   
            AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert triggerControl;
                 
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name= 'Test Team';
            team.AxtriaSalesIQTM__Type__c = 'Base';
            team.AxtriaSalesIQTM__Effective_End_Date__c = Date.parse('12/31/4000');
            insert team;
        
            AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
            teamins.Name = 'ENT';
            teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
            teamins.AxtriaSalesIQTM__Team__c = team.Id;
            teamins.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
            teamins.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
            teamins.AxtriaSalesIQTM__Team_Instance_Code__c ='TI-001';
            insert teamins;
            
            AxtriaSalesIQTM__Position__c po2 = new AxtriaSalesIQTM__Position__c();
            po2.Name='Nation';
            po2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po2.AxtriaSalesIQTM__Team_iD__c = team.id;
            po2.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po2.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
            po2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po2.AxtriaSalesIQTM__RGB__c= 'X';
            insert po2;
            
            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name='Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
            po1.AxtriaSalesIQTM__Parent_Position__c = po2.Id;
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = 'X';
            insert po1;
    
            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name='NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = 'x';
            insert po;
            
            AxtriaSalesIQTM__User_Access_Permission__c userPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
            userPerm.AxtriaSalesIQTM__Position__c = po.Id;
            userPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
            userPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            userPerm.AxtriaSalesIQTM__Is_Active__c = true;
            insert userPerm; 
           
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.AxtriaSalesIQTM__Request_Type_Change__c = 'Employee NewHire';
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            changeRequest.AxtriaSalesIQTM__Change_Effective_Date__c = Date.parse('09/20/2019');
            changeRequest.AxtriaSalesIQTM__Is_Auto_Approved__c = false;
            changeRequest.AxtriaSalesIQTM__Source_Position__c = po1.Id;
            changeRequest.AxtriaSalesIQTM__Destination_Position__c = po2.Id;
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            insert changeRequest;        
          
          AxtriaSalesIQTM__Change_Request_Approver_Detail__c crObj = new AxtriaSalesIQTM__Change_Request_Approver_Detail__c();
          crObj.AxtriaSalesIQTM__Change_Request__c = changeRequest.ID;
          insert crObj;
          
          changeRequest.AxtriaSalesIQTM__Status__c = 'Approved';
          update changeRequest;
      }
      
      static testMethod void unitTest3(){        
   
            AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert triggerControl;
                 
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name= 'Test Team';
            team.AxtriaSalesIQTM__Type__c = 'Base';
            team.AxtriaSalesIQTM__Effective_End_Date__c = Date.parse('12/31/4000');
            insert team;
        
            AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
            teamins.Name = 'SPM';
            teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
            teamins.AxtriaSalesIQTM__Team__c = team.Id;
            teamins.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
            teamins.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
            teamins.AxtriaSalesIQTM__Team_Instance_Code__c ='TI-001';
            insert teamins;
            
            AxtriaSalesIQTM__Position__c po2 = new AxtriaSalesIQTM__Position__c();
            po2.Name='Nation';
            po2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po2.AxtriaSalesIQTM__Team_iD__c = team.id;
            po2.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po2.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
            po2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po2.AxtriaSalesIQTM__RGB__c= 'X';
            insert po2;
            
            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name='Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
            po1.AxtriaSalesIQTM__Parent_Position__c = po2.Id;
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = 'X';
            insert po1;
    
            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name='NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = 'x';
            insert po;
            
            AxtriaSalesIQTM__User_Access_Permission__c userPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
            userPerm.AxtriaSalesIQTM__Position__c = po.Id;
            userPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
            userPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            userPerm.AxtriaSalesIQTM__Is_Active__c = true;
            insert userPerm; 
           
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.AxtriaSalesIQTM__Request_Type_Change__c = 'Employee NewHire';
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            changeRequest.AxtriaSalesIQTM__Change_Effective_Date__c = Date.parse('09/20/2019');
            changeRequest.AxtriaSalesIQTM__Is_Auto_Approved__c = false;
            changeRequest.AxtriaSalesIQTM__Source_Position__c = po1.Id;
            changeRequest.AxtriaSalesIQTM__Destination_Position__c = po2.Id;
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            insert changeRequest;                                                 
      } 
      
      static testMethod void unitTest4(){        
   
            AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert triggerControl;
                 
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name= 'Test Team';
            team.AxtriaSalesIQTM__Type__c = 'Base';
            team.AxtriaSalesIQTM__Effective_End_Date__c = Date.parse('12/31/4000');
            insert team;
        
            AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
            teamins.Name = 'SPM';
            teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
            teamins.AxtriaSalesIQTM__Team__c = team.Id;
            teamins.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
            teamins.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
            teamins.AxtriaSalesIQTM__Team_Instance_Code__c ='TI-001';
            insert teamins;
            
            AxtriaSalesIQTM__Position__c po2 = new AxtriaSalesIQTM__Position__c();
            po2.Name='Nation';
            po2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po2.AxtriaSalesIQTM__Team_iD__c = team.id;
            po2.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po2.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
            po2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po2.AxtriaSalesIQTM__RGB__c= 'X';
            insert po2;
            
            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name='Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
            po1.AxtriaSalesIQTM__Parent_Position__c = po2.Id;
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = 'X';
            insert po1;
    
            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name='NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = 'x';
            insert po;
            
            AxtriaSalesIQTM__User_Access_Permission__c userPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
            userPerm.AxtriaSalesIQTM__Position__c = po.Id;
            userPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
            userPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            userPerm.AxtriaSalesIQTM__Is_Active__c = true;
            insert userPerm; 
           
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.AxtriaSalesIQTM__Request_Type_Change__c = 'Employee NewHire';
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            changeRequest.AxtriaSalesIQTM__Change_Effective_Date__c = Date.parse('09/20/2019');
            changeRequest.AxtriaSalesIQTM__Is_Auto_Approved__c = false;
            changeRequest.AxtriaSalesIQTM__Source_Position__c = po1.Id;
            changeRequest.AxtriaSalesIQTM__Destination_Position__c = po2.Id;
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            insert changeRequest;                                                          
      }  
      
      static testMethod void unitTest5(){        
   
            AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert triggerControl;
                 
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name= 'Test Team';
            team.AxtriaSalesIQTM__Type__c = 'Base';
            team.AxtriaSalesIQTM__Effective_End_Date__c = Date.parse('12/31/4000');
            insert team;
        
            AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
            teamins.Name = 'SPM';
            teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
            teamins.AxtriaSalesIQTM__Team__c = team.Id;
            teamins.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
            teamins.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
            teamins.AxtriaSalesIQTM__Team_Instance_Code__c ='TI-001';
            insert teamins;
            
            AxtriaSalesIQTM__Position__c po2 = new AxtriaSalesIQTM__Position__c();
            po2.Name='Nation';
            po2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po2.AxtriaSalesIQTM__Team_iD__c = team.id;
            po2.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Client_Position_Code__c = 'Y';
            po2.AxtriaSalesIQTM__Position_Type__c = 'Nation';
            po2.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
            po2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po2.AxtriaSalesIQTM__RGB__c= 'X';
            insert po2;
            
            AxtriaSalesIQTM__Position__c po1 = new AxtriaSalesIQTM__Position__c();
            po1.Name='Nation';
            po1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po1.AxtriaSalesIQTM__Team_iD__c = team.id;
            po1.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Client_Position_Code__c = 'Y1';
            po1.AxtriaSalesIQTM__Position_Type__c = 'Area';
            po1.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po1.AxtriaSalesIQTM__Parent_Position__c = po2.Id;
            po1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po1.AxtriaSalesIQTM__RGB__c = 'X';
            insert po1;
    
            AxtriaSalesIQTM__Position__c po = new AxtriaSalesIQTM__Position__c();
            po.Name='NORTHEAST1';
            po.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            po.AxtriaSalesIQTM__Team_iD__c = team.id;
            po.AxtriaSalesIQTM__Client_Territory_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Client_Position_Code__c = 'Y001';
            po.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            po.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
            po.AxtriaSalesIQTM__Parent_Position__c = po1.Id;
            po.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
            po.AxtriaSalesIQTM__RGB__c = 'x';
            insert po;
            
            AxtriaSalesIQTM__User_Access_Permission__c userPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
            userPerm.AxtriaSalesIQTM__Position__c = po.Id;
            userPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
            userPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
            userPerm.AxtriaSalesIQTM__Is_Active__c = true;
            insert userPerm; 
           
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.AxtriaSalesIQTM__Request_Type_Change__c = 'Employee NewHire';
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            changeRequest.AxtriaSalesIQTM__Change_Effective_Date__c = Date.parse('09/20/2019');
            changeRequest.AxtriaSalesIQTM__Is_Auto_Approved__c = false;
            changeRequest.AxtriaSalesIQTM__Source_Position__c = po1.Id;
            changeRequest.AxtriaSalesIQTM__Destination_Position__c = po2.Id;
            changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
            insert changeRequest;                                                          
      }        
      
}