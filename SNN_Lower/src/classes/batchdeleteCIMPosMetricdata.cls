global class batchdeleteCIMPosMetricdata implements Database.Batchable<sObject>{
    
    global List<string> teamInstList;
    global string teamInstID;
    global string Query;
    
    AxtriaSalesIQST__Scheduler_Log__c scLog1= new AxtriaSalesIQST__Scheduler_Log__c();
    
  global batchdeleteCIMPosMetricdata (String TIid,AxtriaSalesIQST__Scheduler_Log__c scLog) {
        Query='';
        teamInstID= TIid;
        scLog1=scLog;
        
        Query='SELECT Id FROM AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstID';
        
    }
    
    global batchdeleteCIMPosMetricdata (List<string> TIList,AxtriaSalesIQST__Scheduler_Log__c scLog) {
        Query='';
        teamInstList = new List<string>(TIList);
        scLog1=scLog;
        
        Query='SELECT Id FROM AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__Team_Instance__c in :teamInstList';
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('=========Query is::'+query);

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {

      Database.DeleteResult[] DR_Dels = Database.delete(scope , false);
      //Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 

    }


    global void finish(Database.BatchableContext bc){
        try{
                //Id tiID = teamInstanceId;
                //delete[Select Id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__Team_Instance__c =: tiID];
            Map<String,List<String>> cimMap;
            System.debug('teamInstList>>>>>>>>>>'+teamInstList);  
            if(teamInstID!=null){
              Id tiID = teamInstID;
              cimMap = new Map<String,List<String>>();
                               
                for(AxtriaSalesIQTM__CIM_Config__c cim : [Select ID,AxtriaSalesIQTM__Team_Instance__c,Name from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c = : tiID and AxtriaSalesIQTM__Enable__c=true and (AxtriaSalesIQTM__Hierarchy_Level__c ='1' or AxtriaSalesIQTM__Hierarchy_Level__c = null or AxtriaSalesIQTM__Hierarchy_Level__c = '')]){
                    if(cimMap.containsKey(cim.AxtriaSalesIQTM__Team_Instance__c)){
                        cimMap.get(cim.AxtriaSalesIQTM__Team_Instance__c).add(cim.id);
                    }else{
                        list<String> cimId = new list<String>();
                        cimId.add(cim.Id);
                        cimMap.put(cim.AxtriaSalesIQTM__Team_Instance__c,cimId);
                    }
                }
                Database.executeBatch(new AxtriaSalesIQST.BatchCIMPositionMatrixSummaryUpdate(cimMap.get(tiID),tiID ),1000);
            }
            else if(teamInstList != null && teamInstList.size() > 0){
                
              cimMap = new Map<String,List<String>>();
                for(String ti :teamInstList){
                    Id tiID = ti;
                    for(AxtriaSalesIQTM__CIM_Config__c cim : [Select ID,AxtriaSalesIQTM__Team_Instance__c,Name from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c = : tiID and AxtriaSalesIQTM__Enable__c=true and (AxtriaSalesIQTM__Hierarchy_Level__c ='1' or AxtriaSalesIQTM__Hierarchy_Level__c = null or AxtriaSalesIQTM__Hierarchy_Level__c = '')]){
                        if(cimMap.containsKey(cim.AxtriaSalesIQTM__Team_Instance__c)){
                            cimMap.get(cim.AxtriaSalesIQTM__Team_Instance__c).add(cim.id);
                        }else{
                            list<String> cimId = new list<String>();
                            cimId.add(cim.Id);
                            cimMap.put(cim.AxtriaSalesIQTM__Team_Instance__c,cimId);
                        }
                    }
                    if(cimMap.get(tiID) != null)
                        Database.executeBatch(new AxtriaSalesIQST.BatchCIMPositionMatrixSummaryUpdate(cimMap.get(tiID),tiID ),1000);
                }
            }
            
            scLog1.AxtriaSalesIQST__Job_Status__c = 'Successfull';
            update scLog1;                     
        }catch(Exception exc){
            scLog1.AxtriaSalesIQST__Job_Status__c = 'Error';
            update scLog1;
        }
    }    
}