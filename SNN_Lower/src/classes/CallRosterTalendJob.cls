public class CallRosterTalendJob {
    
    public  Roster_Val_Log__c s3obj{get;set;}
    public integer seconds{get;set;}
    public boolean enableRefresh{get;set;}
    
    public CallRosterTalendJob(){
       // actionBeforecallingTalendjob();
        //validationFunc();
        //callTalendJob();
        
        
    }
     public void actionBeforecallingTalendjob()
    {
       seconds=0;
       enableRefresh=false;
       Roster_Val_Log__c obj= new Roster_Val_Log__c();
       obj.Status__c='Started';
       obj.Name='Roster_'+System.Now();
       obj.Reason__c='Data Loaded';
       obj.logs__c='Started';
       obj.File_Date__c=System.Today();
       insert obj;
       s3obj=obj;
       if(Test.isRunningTest())
       {
           Roster_Val_Log__c obj1= [select name,status__c,createddate, logs__c,File_Date__c from Roster_Val_Log__c  order by createddate  desc limit 1];
           obj1.status__c='Loaded';
           obj1.logs__c='Loaded';
           update obj1;
       }
       else
       {
           callTalendJob();
       }
    }
  
    @future(callout=true)
    public static void callTalendJob()
    {
       /* AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallRosterTalendJob');
        String mainURL = myCS1.DomainURL__c;
       */
     //  String mainURL = 'http://54.191.113.229:8080/SKLSI_Error_Handling_Job_final_new/services/SKLSI_Error_Handling_Job_final_new?method=runJob';
                             
            String mainURL ='';
            list<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select AxtriaSalesIQTM__End_Point__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'Roster_Inbound'];
            System.debug('etlConfig '+etlConfig);
            if(etlConfig.size() > 0){
                 mainURL = etlConfig[0].AxtriaSalesIQTM__End_Point__c;
             }
            String endPointURL = mainURL.replaceAll( '\\s+', '%20');
            
            System.debug('mainURL-->'+mainURL);
            System.debug('endPointURL-->'+endPointURL);
            Http h=new Http();
            HttpRequest request= new HttpRequest();
            request.setEndpoint(endPointURL);
            request.setHeader('Content-type', 'application/json');
            request.setMethod('GET');
            request.setTimeout(120000);
            HttpResponse response=h.send(request);
           
          
    }
    public void validationFunc()
    {       
            system.debug('inside --------- validation');
            set<string>errorEmpSet=new  set<string>();
            set<string>empNumSet=new  set<string>();
            //set<string>UserIdSet=new  set<string>();
            map<string,AxtriaSalesIQTM__Employee__c> mapEmp=new  map<string,AxtriaSalesIQTM__Employee__c>();
            map<string,AxtriaSalesIQST__CurrentPersonFeed__c>  mapStagingObj = new Map<String,AxtriaSalesIQST__CurrentPersonFeed__c>();
            set<string>activeEmpSet=new  set<string>();
            
           string commonBody='';
            
            string body='';
            String subject='';
            Roster_Val_Log__c log=[select name,status__c,createddate, logs__c,File_Date__c from Roster_Val_Log__c  order by createddate  desc limit 1];
            system.debug('LOG----- '+ log.status__c + ' ------ '+log.logs__c);
            DateTime d = log.File_Date__c;///Date.Today().addDays(-1) ;
            String currentDate =  d.format('MM/dd/yyyy') ;
            
            if(log.status__c=='Loaded' && log.logs__c=='Loaded')
            { 
                    Boolean errorFlag=false;
                    subject='Records with data validation issues'; 
                    commonBody='<p style="font-family:Calibri;size=6">';
                    commonBody+='Hi, <br/> <br/>';
                    commonBody = commonBody.replace('MM/DD/YYYY',currentDate);
                    commonBody = commonBody.replace('Employee DW File name','Roster_To_Axtria_'+ currentDate+'.txt' );
                   
                    body=commonBody+'<table border="1"><tr><th>Emp Num</th> ';
                    body+=' <th>Description</th></tr>';
                     
                    
                     Date createDate=[select name,createddate, logs__c,File_Date__c from Roster_Val_Log__c where status__c='Loaded' order by createddate  desc  limit 1].File_Date__c;
                     Date lastProcessingDate=createDate; 
                     system.debug('lastProcessingDate-----'+lastProcessingDate);
                    
                      for(AxtriaSalesIQTM__Employee__c emp : [SELECT Id,Name,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Current_Territory__c,AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Territory_Code__c, First_Day_Of_Leave__c,Last_Day_Of_Leave__c, AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__HR_Termination_Date__c,AxtriaSalesIQTM__HR_Status__c FROM AxtriaSalesIQTM__Employee__c])
                      {     system.debug('inside --------- FOR Employee------');
                             mapEmp.put(emp.AxtriaSalesIQTM__Employee_ID__c, emp);
                             if(emp.AxtriaSalesIQTM__HR_Status__c=='Active' && emp.AxtriaSalesIQTM__HR_Termination_Date__c==null)
                             {  system.debug('inside --------- FOR EMployee IF------');
                                 activeEmpSet.add(emp.AxtriaSalesIQTM__Employee_ID__c);
                             }

                     } 
                      

                     /************* Check  other validations****************/
                     for(AxtriaSalesIQST__CurrentPersonFeed__c stagObj:[ SELECT AxtriaSalesIQST__Employee_ID__c,AxtriaSalesIQST__ReportingToWorkerName__c,AxtriaSalesIQST__ReportsToAssociateOID__c,AxtriaSalesIQST__JobCodeName__c,AxtriaSalesIQST__JobCode__c,AxtriaSalesIQST__First_Name__c,AxtriaSalesIQST__Middle_Name__c,AxtriaSalesIQST__Last_Name__c,AxtriaSalesIQST__NickName__c,AxtriaSalesIQST__AddressLine1__c,AxtriaSalesIQST__AddressLine2__c,AxtriaSalesIQST__AddressCity__c,AxtriaSalesIQST__AddressStateCode__c,AxtriaSalesIQST__AddressPostalCode__c,AxtriaSalesIQST__LegalAddressLine1__c,AxtriaSalesIQST__LegalAddressLine2__c,AxtriaSalesIQST__LegalAddressCityName__c,AxtriaSalesIQST__StateTerritory__c,AxtriaSalesIQST__LegalAddressPostalCode__c,AxtriaSalesIQST__HomePhone__c,AxtriaSalesIQST__PersonalCell__c,AxtriaSalesIQST__Email__c,AxtriaSalesIQST__Segment_1__c,AxtriaSalesIQST__Segment_2__c,AxtriaSalesIQST__Rehire_Date__c,AxtriaSalesIQST__OriginalHireDate__c,AxtriaSalesIQST__Hire_Date__c,AxtriaSalesIQST__First_Day_of_Leave__c,AxtriaSalesIQST__Last_Day_of_Leave__c,AxtriaSalesIQST__TerminationDate__c,AxtriaSalesIQST__HR_Status__c,AxtriaSalesIQST__Last_Modified_Date__c from AxtriaSalesIQST__CurrentPersonFeed__c where AxtriaSalesIQST__Employee_ID__c != null])
                        {   system.debug('inside --------- FOR Staging Object------');
                            mapStagingObj.put(stagObj.AxtriaSalesIQST__Employee_ID__c,stagObj);
                            
                         //   Date geoEndDate; 
                          //  Date GEO_START_DATE; 
                            Date hireDate;  
                            /**************** Date Validations**************/
                            try{ system.debug('inside --------- Date Validation------');
                                
                                if(stagObj.AxtriaSalesIQST__OriginalHireDate__c!=null)
                                {  if(stagObj.AxtriaSalesIQST__Hire_Date__c==null)
                                        {hireDate = Date.valueOf(stagObj.AxtriaSalesIQST__OriginalHireDate__c ); }
                                   else
                                        {hireDate = Date.valueOf(stagObj.AxtriaSalesIQST__Hire_Date__c );}
                                }  
                                 
                               
                            }
                            catch(Exception e)
                            {  system.debug('inside CATCH --------- Date Validation------');
                                body=commonBody;
                                body+='<table>';
                                body+='<tr> <th>Description</th></tr>';
                                body+='<tr><td>Dates are not in correct Format</td> </tr>';
                                errorFlag=true;
                                if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                                break;
                            }



                        /****************Event Dates Validation**************/
                        if(stagObj.AxtriaSalesIQST__Rehire_Date__c!=null && stagObj.AxtriaSalesIQST__Rehire_Date__c<stagObj.AxtriaSalesIQST__OriginalHireDate__c)
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> Rehire date is less than Original Hire Date</td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY2------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                         if(stagObj.AxtriaSalesIQST__First_Day_of_Leave__c!=null && stagObj.AxtriaSalesIQST__First_Day_of_Leave__c < stagObj.AxtriaSalesIQST__Rehire_Date__c)
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> LOA Start Date is less than Employee Hire Date</td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY2------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                        if(stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c!=null && stagObj.AxtriaSalesIQST__First_Day_of_Leave__c!=null && stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c < stagObj.AxtriaSalesIQST__First_Day_of_Leave__c)
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> LOA End Date is less than LOA Start Date</td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY2------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                         /****************Event specific Mandatory Columns**************/
                         
                        
                         // CASE 3
                         if( mapEmp.containsKey(stagObj.AxtriaSalesIQST__Employee_ID__c) && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).First_Day_of_Leave__c!=null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).Last_Day_of_Leave__c==null && stagObj.AxtriaSalesIQST__First_Day_of_Leave__c == null && stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c == null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).AxtriaSalesIQTM__HR_Status__c=='Active' && stagObj.AxtriaSalesIQST__HR_Status__c=='Active')
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> Mandatory columns are null for LOA Event to occur</td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY2------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                        //CASE 6
                         if(  mapEmp.containsKey(stagObj.AxtriaSalesIQST__Employee_ID__c) && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).First_Day_of_Leave__c!=null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).Last_Day_of_Leave__c!=null && stagObj.AxtriaSalesIQST__First_Day_of_Leave__c == null &&stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c != null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).Last_Day_of_Leave__c!=stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c && stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c != null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).AxtriaSalesIQTM__HR_Status__c=='Active' && stagObj.AxtriaSalesIQST__HR_Status__c=='Active')
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> Mandatory columns are null for LOA Event to occur </td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY3------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                        //CASE 13
                         if(mapEmp.containsKey(stagObj.AxtriaSalesIQST__Employee_ID__c) && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).First_Day_of_Leave__c==null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).Last_Day_of_Leave__c==null && stagObj.AxtriaSalesIQST__First_Day_of_Leave__c == null &&stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c != null && stagObj.AxtriaSalesIQST__Last_Day_of_Leave__c == null && mapEmp.get(stagObj.AxtriaSalesIQST__Employee_ID__c).AxtriaSalesIQTM__HR_Status__c=='Active' && stagObj.AxtriaSalesIQST__HR_Status__c=='Active')
                            {
                                body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> Mandatory columns are null for LOA Event to occur </td></tr>';
                                errorFlag=true;
                                 system.debug('--------BODY3------' +body);
                                 if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                    errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                            }
                         
                         
                           if(stagObj.AxtriaSalesIQST__Employee_ID__c!=null){
                                
                                 /********* Employee Duplicate validation***********/
                                if((!empNumSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c)))
                                {
                                    empNumSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                                     system.debug('------EMP DUP Val-----' );
                                }
                                else
                                {
                                    body+='<tr><td>'+stagObj.AxtriaSalesIQST__Employee_ID__c +'</td><td> Duplicate Employee Id ' + stagObj.AxtriaSalesIQST__Employee_ID__c+' in feed</td></tr>';
                                    errorFlag=true;
                                    if(!errorEmpSet.contains(stagObj.AxtriaSalesIQST__Employee_ID__c))
                                        errorEmpSet.add(stagObj.AxtriaSalesIQST__Employee_ID__c);
                                     system.debug('--------BODY16------' +body);
                                }
                          
                            }    
                        }
                    /********* Employee Missing Validation***********/
                    for(String emp : activeEmpSet)
                        {system.debug('--------activeEmpSet------' +emp);
                    system.debug('--------mapStagingObj------' +mapStagingObj.get(emp));
                            if(!mapStagingObj.containsKey(emp))
                            {   Date d1=System.Today();
                                body+='<tr><td>'+emp+'</td><td>Employee is missing from current feed ADMS_EMP_ROSTER_DATA_'+d1.month()+d1.day()+d1.year()+'.txt </td></tr>';
                                     errorFlag=true;
                                     if(!errorEmpSet.contains(emp))
                                        errorEmpSet.add(emp);
                                      system.debug('--------BOD18Y------' +body);
                            }
                        }
                    
                    
                    
                    if(errorFlag==true)
                    {
                        system.debug('--------ERRROR FLAG------' +errorFlag);
                        log.status__c='Error';   
                        log.logs__c='Validation Error';
                        update log;
                        List<AxtriaSalesIQST__CurrentPersonFeed__c> cfeed = new List<AxtriaSalesIQST__CurrentPersonFeed__c>();
                        for (AxtriaSalesIQST__CurrentPersonFeed__c c : [select id, isIncluded__c,AxtriaSalesIQST__Employee_ID__c from AxtriaSalesIQST__CurrentPersonFeed__c where AxtriaSalesIQST__Employee_ID__c in :errorEmpSet])
                        {
                            c.isIncluded__c=false;
                            cfeed.add(c);
                        }
                        if(cfeed.size()>0)
                            update cfeed;
                        system.debug(body);
                        body+='</table><br/>Please reach out to Axtria SalesIQ Support for further details.<br/>';
                        body+='Axtria SalesIQ Support';
                        body=body+'</p>';
                       sendMail(subject,body);
                        Database.executeBatch(new BatchComputeEvents(),2000); 
                    }
                    else
                    {
                        
                        log.status__c='Validation Successfull';   
                        log.logs__c='Successful';
                        update log;
                        Database.executeBatch(new BatchComputeEvents(),2000); 
                        
                    }
                    
                  
                   
                 }    
                
            
        }
    
    public void sendMail(String subject,String body)
    {
        system.debug('Subject=========='+subject);
        system.debug('body=========='+body);
        List<string> toAddress = new List<string>();
        List<RosterToEmailList__c> con= [select id, Email__c from RosterToEmailList__c where Type__c='Roster'];
         for(RosterToEmailList__c toemail : con)
              {  toAddress.add(toemail.Email__c);
                System.debug('--TO EMAIL--'+toemail.Email__c);
                  
              }
      //  toAddress.add('arundhati.dar@axtria.com');
       List<String> ccTo = new List<String>();
             //   ccTo.add('arundhati.dar@axtria.com');
             //   ccTo.add('sklsi_salesiq_support@Axtria.com');
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'adamas.salesiqsupport@axtria.com'];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddress);
        mail.setCcAddresses(ccTo);
        mail.setSubject(subject);
        /*mail.setReplyTo('sklsi_salesiq_support@Axtria.com');
        mail.setSenderDisplayName('Axtria SalesIQTM');*/
        mail.setHtmlBody(body);
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        allmsg.add(mail);
        
        try {
            system.debug('sending email try1-----------------');
            Messaging.sendEmail(allmsg,false);
            system.debug('sending email try2-----------------');
            return;
        } catch (Exception e) {
            system.debug('sending email catch-----------------');
            System.debug(e.getMessage());
        }
        
    }
    
   
    
    
}