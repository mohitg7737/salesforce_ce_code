@isTest
private class IntegrationScheduledClassTest{

    private static testMethod void test1(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'Event Creation';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Inbound';
        objSc.selectedJobName = 'Event Creation';
        objSc.jobNameRefresh();
        objSc.runJob();
        String nextFireTime = '0 0 0 3 9 ? 2025';
        Test.startTest();
        String jobId = System.schedule('Integration Scheduled Test',  nextFireTime, new IntegrationScheduledClass());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(nextFireTime, ct.CronExpression);
        Test.stopTest();
    }
    
    private static testMethod void test2(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'Event Creation';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Inbound';
        objSc.selectedJobName = 'Event Creation';
        objSc.jobNameRefresh();
        objSc.runJob();
        String nextFireTime = '0 0 0 3 9 ? 2025';
        Test.startTest();
        String jobId = System.schedule('Integration Scheduled Test',  nextFireTime, new IntegrationScheduledClass(1));
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(nextFireTime, ct.CronExpression);
        Test.stopTest();
    }
    
    private static testMethod void test3(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'Event Creation';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Inbound';
        objSc.selectedJobName = 'Event Creation';
        objSc.jobNameRefresh();
        objSc.runJob();
        String nextFireTime = '0 0 0 3 9 ? 2025';
        Test.startTest();
        String jobId = System.schedule('Integration Scheduled Test',  nextFireTime, new IntegrationScheduledClass(2));
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(nextFireTime, ct.CronExpression);
        Test.stopTest();
    }
    
    private static testMethod void test4(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'Workbench Event Creation';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Inbound';
        objSc.selectedJobName = 'Event Creation';
        objSc.jobNameRefresh();
        objSc.runJob();
        String nextFireTime = '0 0 0 3 9 ? 2025';
        Test.startTest();
        String jobId = System.schedule('Integration Scheduled Test',  nextFireTime, new IntegrationScheduledClass(2));
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(nextFireTime, ct.CronExpression);
        Test.stopTest();
    }
    
    private static testMethod void test5(){
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        //scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        //scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
        insert scLog;
        AxtriaSalesIQTM__ETL_Config__c objETL = new AxtriaSalesIQTM__ETL_Config__c();
        objETL.Name = 'WinShuttle';
        insert objETL;      
        Scheduled_Job_Run_Ctrl objSc = new Scheduled_Job_Run_Ctrl();
        objSc.selectedJobType = 'Outbound';
        objSc.selectedJobName = 'WinShuttle';
        objSc.jobNameRefresh();
        objSc.runJob();
        String nextFireTime = '0 0 0 4 9 ? 2025';
        Test.startTest();
        String jobId = System.schedule('Integration Scheduled Test',  nextFireTime, new IntegrationScheduledClass(2));
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(nextFireTime, ct.CronExpression);
        
        Test.stopTest();
        
    
    }
    
    public static testMethod void test6()
    {
    IntegrationScheduledClass A = new IntegrationScheduledClass();
    A.dummyFunction();
    }


}