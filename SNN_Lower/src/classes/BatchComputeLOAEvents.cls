global class BatchComputeLOAEvents implements Database.Stateful,Database.Batchable<sObject> 
{

    public String query;
    Map<String,AxtriaSalesIQST__Current_Leave_Data_Feed__c> loaMap = new Map<String,AxtriaSalesIQST__Current_Leave_Data_Feed__c>();
    Map<String,AxtriaSalesIQTM__Employee__c> mapEmp = new  Map<String,AxtriaSalesIQTM__Employee__c>();
    List<AxtriaSalesIQTM__Employee__c> listEmpUpd = new List<AxtriaSalesIQTM__Employee__c>();
    Map<String,Integer> eventCountMap = new Map<String,Integer>();
    Map<String,AxtriaSalesIQTM__Employee__c> updatedEmpRecords = new Map<String,AxtriaSalesIQTM__Employee__c>();
     Map<String,AxtriaSalesIQTM__Employee__c> updEmpMap = new Map<String,AxtriaSalesIQTM__Employee__c>();
    Map<string,List<string>> empEventMap = new Map<string,List<string>>();
    List<AxtriaSalesIQST__CR_Employee_Feed__c> listEvent = new List<AxtriaSalesIQST__CR_Employee_Feed__c>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        query = 'SELECT Beacon_User_SFDC_ID__c,Employee_Id__c,Leave_Status__c,Leave_Type__c,LOA_End_Date__c,LOA_Start_Date__c,RecordID__c from Beacon_Employee_LOA__c where Employee_Id__c != null  and createdDate = today';
        
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext bc, List<Beacon_Employee_LOA__c> scope)
    {
        
        listEvent = new list<AxtriaSalesIQST__CR_Employee_Feed__c>();
        empEventMap = new Map<string,List<string>>();
        createEmpMap();
        
         for(Beacon_Employee_LOA__c cw : scope)
        {
            
              /* LOA event */
              if(mapEmp.containsKey(cw.Employee_Id__c) && (mapEmp.get(cw.Employee_Id__c).AxtriaSalesIQTM__HR_Status__c)=='Active' && cw.LOA_Start_Date__c != null && mapEmp.get(cw.Employee_ID__c).AxtriaSalesIQTM__Employee_Type__c != '1099' && mapEmp.get(cw.Employee_ID__c).First_Day_Of_Leave__c != cw.LOA_Start_Date__c)
              {
                  System.debug('--------LOA ------------');
                  //updateEmployee(cw,System.Label.LOA);
                  Integer count=0;
                  createNewEvent(cw,System.Label.LOA);
                  
                  updateLOAData(cw);
                  
                  if(!eventCountMap.containsKey(System.Label.LOA))
                  {    count = count+1;
                        eventCountMap.put(System.Label.LOA,count); 
                  }
                  else
                  {   count = eventCountMap.get(System.Label.LOA);
                      count = count+1;
                      eventCountMap.put(System.Label.LOA, count);
                  }
              }

              /* LOA event Case-1 : new LOA Case2 : LOA end date is updated*/
              if(mapEmp.containsKey(cw.Employee_Id__c) && (mapEmp.get(cw.Employee_Id__c).AxtriaSalesIQTM__HR_Status__c)=='Active' && cw.LOA_Start_Date__c != null && mapEmp.get(cw.Employee_Id__c).AxtriaSalesIQTM__Employee_Type__c != '1099' && mapEmp.get(cw.Employee_ID__c).First_Day_Of_Leave__c == cw.LOA_Start_Date__c && mapEmp.get(cw.Employee_ID__c).Last_Day_Of_Leave__c != cw.LOA_End_Date__c)
              {
                  System.debug('--------LOA end date update------------');
                  //updateEmployee(cw,System.Label.LOA);
                  Integer count=0;
                  createNewEvent(cw,System.Label.LOA);
                  System.debug('cw for load end date : '+cw);
                  updateLOAData(cw);
                  
                  if(!eventCountMap.containsKey(System.Label.LOA))
                  {    count = count+1;
                        eventCountMap.put(System.Label.LOA,count); 
                  }
                  else
                  {   count = eventCountMap.get(System.Label.LOA);
                      count = count+1;
                      eventCountMap.put(System.Label.LOA, count);
                  }
              }
        }
        
        if(listEvent.size()>0){
            insert listEvent;
        }
    }
    
    global void finish(Database.BatchableContext bc)
    {
    
    }
    
    void createEmpMap(){
        for(AxtriaSalesIQTM__Employee__c emp : [SELECT Id,Name,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Current_Territory__c,AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Job_Title__c,Job_Title_Code__c, AxtriaSalesIQTM__HR_Termination_Date__c,AxtriaSalesIQTM__HR_Status__c,Last_Day_Of_Leave__c,First_Day_Of_Leave__c,AxtriaSalesIQST__AddressLine1__c,AxtriaSalesIQST__AddressLine2__c, AxtriaSalesIQST__AddressCity__c,AxtriaSalesIQST__AddressStateCode__c,AxtriaSalesIQST__AddressPostalCode__c,AxtriaSalesIQTM__Rehire_Date__c,Business_Title__c,AxtriaSalesIQTM__Employee_Type__c,Job_Profile__c,Position_In_Organization__c,AxtriaSalesIQST__ReportsToAssociateOID__c,Business_Unit__c,AxtriaSalesIQST__TerminationType__c,Is_Rehire__c,AxtriaSalesIQTM__Joining_Date__c FROM AxtriaSalesIQTM__Employee__c]){
                mapEmp.put(emp.AxtriaSalesIQTM__Employee_ID__c, emp);

        }
    }
    
   
    
    void createNewEvent(Beacon_Employee_LOA__c loaFeed, String eventName){
    
        list<AxtriaSalesIQST__CR_Employee_Feed__c> loaEventExist = new list<AxtriaSalesIQST__CR_Employee_Feed__c>();
        
        loaEventExist = [select id from AxtriaSalesIQST__CR_Employee_Feed__c where AxtriaSalesIQST__Event_Name__c =: eventName and Employee_ID__c =: loaFeed.Employee_Id__c and AxtriaSalesIQST__Leave_Start_Date__c =: loaFeed.LOA_Start_Date__c and AxtriaSalesIQST__Leave_End_Date__c =: loaFeed.LOA_End_Date__c];
        
        if(loaEventExist.size() > 0){
           system.debug('--- event already exist----');
        }
        else{
       
           AxtriaSalesIQST__CR_Employee_Feed__c event = new AxtriaSalesIQST__CR_Employee_Feed__c();
            
            event.AxtriaSalesIQST__Event_Name__c = eventName;
            event.AxtriaSalesIQST__Leave_Start_Date__c = loaFeed.LOA_Start_Date__c;
            event.AxtriaSalesIQST__Leave_End_Date__c = loaFeed.LOA_End_Date__c;
            event.Leave_Type__c = loaFeed.Leave_Type__c;
          
            if(mapEmp.containsKey(loaFeed.Employee_Id__c)){
         
                  event.AxtriaSalesIQST__Employee__c = mapEmp.get(loaFeed.Employee_Id__c).id;
                  event.AxtriaSalesIQST__Job_Profile__c = mapEmp.get(loaFeed.Employee_Id__c).Job_Profile__c;
                  event.AxtriaSalesIQST__Manager_Name__c = mapEmp.get(loaFeed.Employee_Id__c).AxtriaSalesIQST__ReportsToAssociateOID__c;
                  event.Business_Unit__c = mapEmp.get(loaFeed.Employee_Id__c).Business_Unit__c;
                  event.AxtriaSalesIQST__Termination_Date__c = mapEmp.get(loaFeed.Employee_Id__c).AxtriaSalesIQTM__HR_Termination_Date__c;
                  event.Termination_Type__c = mapEmp.get(loaFeed.Employee_Id__c).AxtriaSalesIQST__TerminationType__c;
                  event.BusinessTitle__c = mapEmp.get(loaFeed.Employee_Id__c).Business_Title__c;
                  event.Position_In_Organization__c = mapEmp.get(loaFeed.Employee_Id__c).Position_In_Organization__c;
                  event.Hire_Date__c = mapEmp.get(loaFeed.Employee_Id__c).AxtriaSalesIQTM__Joining_Date__c;
             }
         
             event.AxtriaSalesIQST__Created_Date__c = System.today();
             event.AxtriaSalesIQST__Request_Date1__c = System.today();
             event.AxtriaSalesIQST__Status__c = 'No Action';
        
            listEvent.add(event);
            System.debug('event :'+event);
        }
        
            
    }
    
     void updateLOAData(Beacon_Employee_LOA__c loaFeed){

        // insert record in LOA object
        list<AxtriaSalesIQST__Current_Leave_Data_Feed__c> loaDataList = new list<AxtriaSalesIQST__Current_Leave_Data_Feed__c>();

        list<AxtriaSalesIQST__Current_Leave_Data_Feed__c> loaDataListInsert = new list<AxtriaSalesIQST__Current_Leave_Data_Feed__c>();
        list<AxtriaSalesIQST__Current_Leave_Data_Feed__c> loaDataListUpdate = new list<AxtriaSalesIQST__Current_Leave_Data_Feed__c>();

        loaDataList = [select id,Name, On_Leave__c, Employee_Id__c,Employee_Number__c,AxtriaSalesIQST__LOA_StartDate__c,AxtriaSalesIQST__LOA_EndDate__c,Leave_Type__c,RecordID__c from  AxtriaSalesIQST__Current_Leave_Data_Feed__c where RecordID__c =: loaFeed.RecordID__c];
        
        if(loaDataList.size() > 0){
            for(AxtriaSalesIQST__Current_Leave_Data_Feed__c loaObj : loaDataList){
              /*case when loa end date is not received with start date*/
                if(loaObj.Employee_Number__c == loaFeed.Employee_Id__c)
                {
                        loaObj.AxtriaSalesIQST__LOA_StartDate__c = loaFeed.LOA_Start_Date__c;
                        loaObj.AxtriaSalesIQST__LOA_EndDate__c = loaFeed.LOA_End_Date__c;
                        loaObj.Leave_Type__c = loaFeed.Leave_Type__c;
                        loaMap.put(loaObj.Employee_Number__c, loaObj);
                        loaDataListUpdate.add(loaObj);
                    
                }
            }
        }
        else{
            /*case when new event is created and new record is created for LOA object*/
            AxtriaSalesIQST__Current_Leave_Data_Feed__c loaData = new AxtriaSalesIQST__Current_Leave_Data_Feed__c();
            loaData.Name = mapEmp.get(loaFeed.Employee_Id__c).Name;
            loaData.Employee_Id__c = mapEmp.get(loaFeed.Employee_Id__c).id;
            loaData.AxtriaSalesIQST__LOA_StartDate__c = loaFeed.LOA_Start_Date__c;
            loaData.AxtriaSalesIQST__LOA_EndDate__c = loaFeed.LOA_End_Date__c;
            loaData.Leave_Type__c = loaFeed.Leave_Type__c;
            loaData.RecordID__c = loaFeed.RecordID__c;
            loaMap.put(loaFeed.Employee_Id__c,loaData);
            loaDataListInsert.add(loaData);
        }
        
        if(loaDataListInsert.size() > 0){
          insert loaDataListInsert;
        }

        if(loaDataListUpdate.size() > 0){
          update loaDataListUpdate;
        }

    }

}