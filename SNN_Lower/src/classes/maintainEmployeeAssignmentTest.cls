@istest
public with sharing class maintainEmployeeAssignmentTest{

    @isTest(seeAllData=true)
    static void test1(){
        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.Name ='AA CC'; 
        emp.AxtriaSalesIQTM__Employee_ID__c = 'ABC1231';
        emp.AxtriaSalesIQTM__Employee_Type__c = 'New Hire';
        emp.AxtriaSalesIQTM__FirstName__c ='AA';
        emp.Business_Title__C = 'test1';
        emp.AxtriaSalesIQTM__Middle_Name__c='B';
        emp.AxtriaSalesIQTM__Last_Name__c ='CC' ;
        emp.AxtriaSalesIQTM__Job_Title__c ='AAA';
        emp.Job_Title_Code__c='ABA';
        emp.AxtriaSalesIQST__ReportingToWorkerName__c='ABC';
        emp.AxtriaSalesIQST__ReportsToAssociateOID__c='XYZ123';
        emp.AxtriaSalesIQTM__Email__c = 'Justin.Skeenes@smith-nephew.com';
        emp.Business_Email_Address__c='hw@gkd.com';
        emp.AxtriaSalesIQTM__SalesforceUserName__c = 'hw@gkd.com'+'.ad';
        emp.AxtriaSalesIQTM__Original_Hire_Date__c =  Date.newInstance(2019, 7, 01);
        emp.AxtriaSalesIQTM__Rehire_Date__c=  Date.newInstance(2019, 7, 01);
        emp.AxtriaSalesIQTM__Joining_Date__c=  Date.newInstance(2019, 7, 01);
        emp.AxtriaSalesIQST__AddressLine1__c ='123 Ave' ;
        emp.AxtriaSalesIQST__AddressLine2__c = 'road';
        emp.AxtriaSalesIQST__AddressCity__c ='City1' ;
        emp.AxtriaSalesIQST__AddressStateCode__c ='NY';
        emp.AxtriaSalesIQST__AddressPostalCode__c = '07651';
        emp.AxtriaSalesIQST__Secondary_Address_Line_1__c ='123 Ave' ;
        emp.AxtriaSalesIQST__Secondary_Address_Line_2__c= 'road';
        emp.AxtriaSalesIQST__Secondary_City__c= 'City1';
        emp.AxtriaSalesIQST__Secondary_State__c= 'NY';
        emp.AxtriaSalesIQST__Secondary_ZIP__c = '07651';
        emp.AxtriaSalesIQTM__Cellphone_Number__c ='93200008' ;
        emp.AxtriaSalesIQST__WorkPhone__c ='82196199' ;
        emp.Personal_Email_Address__c='hw@gkd.com';
        emp.AxtriaSalesIQTM__HR_Status__c = 'Active';
        emp.AxtriaSalesIQTM__HR_Termination_Date__c=Date.newInstance(2019, 10, 01);      
        insert emp;
        
        List<String> emailAddress = new List<String>();
        emailAddress.add(emp.AxtriaSalesIQTM__Email__c);
        
        List<Id> lstEmpId = new List<Id>();
        lstEmpId.add(emp.Id);
        
        List<AxtriaSalesIQTM__Employee__c> lstEmployee = [Select Id, AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Employee_Type__c, AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__HR_Status__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Email__c != null and AxtriaSalesIQTM__Email__c in: emailAddress and Id not in: lstEmpId and AxtriaSalesIQTM__Employee_Type__c != null and AxtriaSalesIQTM__Employee_ID__c != null and AxtriaSalesIQTM__HR_Status__c != null and AxtriaSalesIQTM__HR_Status__c = 'Inactive'];        
        
    }    

}