@isTest
public class TestInbound_Outbound_Email {
   
    static testMethod void unitTest1(){
    
        AxtriaSalesIQTM__TriggerContol__c myCS1 = new AxtriaSalesIQTM__TriggerContol__c();
        myCS1.name = 'Inbound_Outbound_Email' ; 
        myCS1.AxtriaSalesIQTM__IsStopTrigger__c = false ; 
        insert myCS1;
        
        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name = 'SPM';
        objTeam.AxtriaSalesIQTM__Type__c = 'Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        insert objTeam;

 

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'SPM_Q1_2021';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001';            
        insert objTeamInstance;   
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = Date.newInstance(2021, 9, 5);
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = Date.newInstance(2021, 12, 5);
        workspace.Fiscal_Period__c = '8';
        workspace.Year__c = '2022';        
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = objTeam.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Request_Process_Stage__c = 'Ready';
        newcreateScenarioObj.AxtriaSalesIQTM__Scenario_Status__c = 'Active';
        insert newcreateScenarioObj;    
        
        
        objTeamInstance.AxtriaSalesIQTM__scenario__c = newcreateScenarioObj.id;            
        update objTeamInstance;   
 
        AxtriaSalesIQTM__ETL_Config__c etl1 = new AxtriaSalesIQTM__ETL_Config__c();
        etl1.Name = 'Account';
        
        etl1.SendMail__c = true;
        insert etl1;
        
        
        AxtriaSalesIQST__Scheduler_Log__c sl1 = new AxtriaSalesIQST__Scheduler_Log__c();
        //sl1.Name = 'Account';
        
        
        sl1.AxtriaSalesIQST__Job_Name__c = 'Account';
        sl1.AxtriaSalesIQST__Job_Type__c = 'Inbound';
        sl1.AxtriaSalesIQST__Job_Status__c = 'In Progress';
        insert sl1;
        
        sl1.AxtriaSalesIQST__Job_Status__c = 'Successful';
        update sl1;
  
    }
    public static testMethod void test6()
    {
    EmailFuctionality A = new EmailFuctionality('','','');
    A.dummyFunction();
    
    }
}