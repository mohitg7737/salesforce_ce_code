public class HomePageSetupController {
    @AuraEnabled
    public static string getMessage(){
        System.debug('@@@ getMessage invoked = ');
        List<Home_Page_Setup__c> homePageSetupList = [SELECT Id, OwnerId, Name, CreatedById, LastModifiedById, HomePageMessage2__c FROM Home_Page_Setup__c];
        String message = homePageSetupList[0].HomePageMessage2__c;
        System.debug('@@@ message = '+message);
        return message;
    }

}