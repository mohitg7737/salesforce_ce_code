@isTest(SeeAllData=true)

public class PODPositionCreationTest{
public static testMethod void test1(){
  
    //Test.startTest();
   AxtriaSalesIQTM__TriggerContol__c myCS1 = new AxtriaSalesIQTM__TriggerContol__c();
        myCS1.Name = 'testPod';
        myCS1.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert myCS1;

        
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='SPM';
        objTeam.AxtriaSalesIQTM__Type__c ='Base'; 
        objTeam.AxtriaSalesIQTM__Effective_start_Date__c = Date.today();
        objTeam.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        
        insert objTeam;
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'FY2021P8SPM';
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = objTeam.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Base';
        objTeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        
        insert objTeamInstance;
        
       
    
       
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();

        pos.Name = 'TestArea';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='testarea';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P100';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE309000';
        pos.AxtriaSalesIQTM__Position_Type__c='Area';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Assignment_status__c ='Vacant';
        pos.AxtriaSalesIQTM__IsMaster__c =False;
    
        insert pos;
        system.debug('positon for level 4 is  '+pos);
        
        AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();

        pos1.Name = 'TestDistrcit';
        pos1.AxtriaSalesIQTM__Client_Territory_Name__c='TestDistrcit';
        pos1.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos1.AxtriaSalesIQTM__Client_Territory_Code__c='P10';
        pos1.AxtriaSalesIQTM__Position_Type__c='District';
        pos1.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos1.AxtriaSalesIQTM__inactive__c = false;
        pos1.AxtriaSalesIQTM__Team_iD__c = objTeam.id;
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos1.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos1.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        pos1.axtriasalesIQTM__Team_Instance__C = objTeamInstance.Id;
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        pos1.AxtriaSalesIQTM__Assignment_status__c ='Vacant';
        pos1.AxtriaSalesIQTM__IsMaster__c =False;
        pos1.AxtriaSalesIQTM__Parent_Position__c= pos.id;
        
      
        insert pos1;
        system.debug('positon for level 3 is  '+pos1);
    
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    AxtriaSalesIQTM__User_Access_Permission__c objUserAccessPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
        objUserAccessPerm.name = 'access';
        objUserAccessPerm.AxtriaSalesIQTM__Position__c =pos1.id;
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = true;
        objUserAccessPerm.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        objUserAccessPerm.AxtriaSalesIQTM__User__c = UserInfo.getUserId();
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = True;
    
    Insert objUserAccessPerm;
    
    system.debug('user position  '+objUserAccessPerm);
   
    
    AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
    cr.AxtriaSalesIQTM__Status__c= 'In Progress';
    cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Create Position';
    cr.AxtriaSalesIQTM__Source_Position__C=pos1.id;
    cr.AxtriaSalesIQTM__is_Active__c =True;
    cr.AxtriaSalesIQTM__Approver1__c = UserInfo.getUserId();
    cr.AxtriaSalesIQTM__Destination_Position__c = pos1.id;
    cr.AxtriaSalesIQTM__Team_Instance_ID__c =objTeamInstance.id;
    insert cr;
    
    System.debug('CR generated is   '+cr);

    AxtriaSalesIQTM__Position__c Podpos = new AxtriaSalesIQTM__Position__c();
    Podpos.Name = pos.Name;
    Podpos.AxtriaSalesIQTM__Position_Type__c ='POD';
    Podpos.AxtriaSalesIQTM__Hierarchy_Level__c ='2';
    Podpos.AxtriaSalesIQTM__Parent_Position__c =pos1.id;
    Podpos.IsDummyPOD__c = True;
    Podpos.AxtriaSalesIQTM__Client_Territory_Code__c ='SA1000';
    Podpos.AxtriaSalesIQTM__Client_Territory_Name__c='abcd';
    Podpos.AxtriaSalesIQTM__Client_Position_Code__c = 'SA1000';
    Podpos.AxtriaSalesIQTM__RGB__c = '41,210,117';
    Podpos.AxtriaSalesIQTM__Related_Position_Type__c='Base';
    Podpos.AxtriaSalesIQTM__Team_iD__c =objTeam.id;
    insert Podpos;
    
        
    AxtriaSalesIQTM__Position_Team_Instance__c pt= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pt.AxtriaSalesIQTM__Position_ID__c=Podpos.Id;
    pt.AxtriaSalesIQTM__Team_Instance_ID__c=objTeamInstance.Id;
    pt.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
    pt.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    pt.AxtriaSalesIQTM__Parent_Position_ID__c =pos.id;
    insert pt;
      
        
    AxtriaSalesIQTM__Team_Instance_Configuration__c tc = new AxtriaSalesIQTM__Team_Instance_Configuration__c();
    tc.AxtriaSalesIQTM__Configuration_Type__c ='Client Position Code';
    tc.AxtriaSalesIQTM__Position_Type__c = 'POD';
    tc.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
    
    cr.AxtriaSalesIQTM__Status__c= 'Approved';
    update cr;
   // Test.stopTest();

    
    
   }       
                                                                  
 }