<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_External_ID</fullName>
        <description>Update External ID</description>
        <field>ExternalID__c</field>
        <formula>IF(RecordType.DeveloperName=&apos;Country&apos;,Country__r.Name+TEXT(Channel_Name__c)+Team__r.Name,Team_Instance__c+TEXT(Channel_Name__c)+Team__r.Name)</formula>
        <name>Update External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate ExternalID</fullName>
        <actions>
            <name>Update_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Channel_Info__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate ExternalID field on Channel Info</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
