global class Call_Plan_Data_to_PACP1 implements Database.Batchable<sObject>  {
    
    global string queri;
    global string teamID;
    public String selectedTeamInstance;
    //global string teamInstance;
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Account__c > accTI=new list<AxtriaSalesIQTM__Team_Instance_Account__c >();
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}

    
    
    global Call_Plan_Data_to_PACP1(String teamInstance){
     /* system.debug('++++teamInstance' );
      customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
      
        selectedTeamInstance=teamInstance;
        
        queri = 'Select Id,CurrencyIsoCode,Name,AxtriaARSnT__Cycle__c, AxtriaARSnT__Account_ID__c, AxtriaARSnT__Team_ID__c, AxtriaARSnT__Territory_ID__c, AxtriaARSnT__Segment__c, AxtriaARSnT__Objective__c, AxtriaARSnT__Product_ID__c, AxtriaARSnT__Adoption__c, AxtriaARSnT__Potential__c, AxtriaARSnT__Target__c, AxtriaARSnT__Previous_Cycle_Calls__c, AxtriaARSnT__Account__c , AxtriaARSnT__Position__c , AxtriaARSnT__Position_Account__c , AxtriaARSnT__Position_Team_Instance__c, AxtriaARSnT__Team_Instance__c, AxtriaARSnT__isError__c, AxtriaARSnT__Product_Name__c  FROM AxtriaARSnT__Call_Plan__c where AxtriaARSnT__Team_ID__c= \'' + teamInstance + '\' and AxtriaARSnT__isError__c = false';*/
    } //Corresponding_HCP__c,Gain_Loss_AxtriaARSnT__Position__c ,
    
    global Database.Querylocator start(Database.BatchableContext bc){
      // system.debug('*************************Checkpoint3');
       /* try{
        return Database.getQueryLocator(queri);
        }
        catch(Exception e){
        System.debug(e.getMessage()); */
        return Database.getQueryLocator(queri);
       // }
         
    } 
    
    global void execute (Database.BatchableContext BC, List<AxtriaARSnT__Call_Plan__c>callplnlist){
        
       /* system.debug('*************************Checkpoint4');
        list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
        List<AxtriaARSnT__Error_Object__c > errorList = new List<AxtriaARSnT__Error_Object__c >();
        AxtriaARSnT__Error_Object__c  tempErrorObj;
        
        Set<string> accountNumberSet = new Set<string>();
        Set<string> posIDSet = new Set<string>();
        Set<string> brandSet = new Set<string>();
        for(AxtriaARSnT__Call_Plan__c cprcd : callplnlist){
            if(!string.IsBlank(cprcd.AxtriaARSnT__Account__c )){            //Accounts Set
                accountNumberSet.add(cprcd.AxtriaARSnT__Account__c );
            }
            if(!string.IsBlank(cprcd.AxtriaARSnT__Position__c )){            //Pos Set
                posIDSet.add(cprcd.AxtriaARSnT__Position__c );
            }
            if(!string.IsBlank(cprcd.AxtriaARSnT__Product_Name__c )){            //brand Set
                brandSet.add(cprcd.AxtriaARSnT__Product_Name__c );
            }
        }
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpListToCheck = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        pacpListToCheck = [Select id,AxtriaARSnT__Cycle_Name__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c ,AxtriaSalesIQTM__Position__c ,AxtriaARSnT__Party_ID__c,AxtriaARSnT__P1__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c  in :accountNumberSet and AxtriaSalesIQTM__Position__c  in :posIDSet and AxtriaARSnT__P1__c in :brandSet and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__lastApprovedTarget__c = true];// limit 30000
        Set<String> pacpSet = new Set<String>();
        Map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c> Cycle_HCP_Product_Positionmap= new   Map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :pacpListToCheck){
            pacpSet.add(String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c )+String.valueof(pacp.AxtriaSalesIQTM__Position__c )+String.valueof(pacp.AxtriaARSnT__P1__c));
            Cycle_HCP_Product_Positionmap.put(String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c )+String.valueof(pacp.AxtriaSalesIQTM__Position__c )+String.valueof(pacp.AxtriaARSnT__P1__c),pacp);
        }
        
        system.debug('before callplnlist ' + callplnlist);
       
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
              for(AxtriaARSnT__Call_Plan__c cprcd : callplnlist){
                
                AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc= new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                
                if(cprcd.AxtriaARSnT__Account__c  != null && cprcd.AxtriaARSnT__Position__c  != null  &&  cprcd.AxtriaARSnT__Position_Team_Instance__c != null && cprcd.AxtriaARSnT__Team_Instance__c != null && cprcd.AxtriaARSnT__Position_Account__c  != null && cprcd.AxtriaARSnT__Product_Name__c  != null && cprcd.AxtriaARSnT__Product_Name__c  != ''){
                    posAcc.AxtriaSalesIQTM__Account__c   =  cprcd.AxtriaARSnT__Account__c ;
                    posAcc.AxtriaSalesIQTM__Position__c   =  cprcd.AxtriaARSnT__Position__c ;
                    posAcc.AxtriaSalesIQTM__Position_Team_Instance__c  =  cprcd.AxtriaARSnT__Position_Team_Instance__c;
                    posAcc.AxtriaSalesIQTM__Team_Instance__c  =  cprcd.AxtriaARSnT__Team_Instance__c;
                    posAcc.AxtriaARSnT__Party_ID__c  =  cprcd.AxtriaARSnT__Position_Account__c ;
                    posAcc.AxtriaSalesIQTM__Effective_Start_Date__c= cprcd.AxtriaARSnT__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                    posAcc.AxtriaSalesIQTM__Effective_End_Date__c= cprcd.AxtriaARSnT__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                    
                    posAcc.AxtriaSalesIQTM__isincludedCallPlan__c  =  cprcd.AxtriaARSnT__Target__c;
                    posAcc.AxtriaSalesIQTM__isAccountTarget__c  =  cprcd.AxtriaARSnT__Target__c;
                    posAcc.AxtriaSalesIQTM__lastApprovedTarget__c  =  cprcd.AxtriaARSnT__Target__c;
                    
                    posAcc.CurrencyIsoCode  =  cprcd.CurrencyIsoCode;
                    
                    
                    posAcc.AxtriaARSnT__Previous_Calls__c  = cprcd.AxtriaARSnT__Previous_Cycle_Calls__c;
                    posAcc.AxtriaARSnT__P1__c = cprcd.AxtriaARSnT__Product_Name__c ;
                    posAcc.AxtriaARSnT__P1_Original__c = cprcd.AxtriaARSnT__Product_Name__c ;
                    posAcc.AxtriaARSnT__Adoption__c = cprcd.AxtriaARSnT__Adoption__c;
                    posAcc.AxtriaARSnT__Potential__c = cprcd.AxtriaARSnT__Potential__c;
                    if(cprcd.AxtriaARSnT__Objective__c!=null){
                        posAcc.AxtriaARSnT__Final_TCF__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c);
                        posAcc.AxtriaARSnT__Final_TCF_Approved__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c);
                        posAcc.AxtriaARSnT__Final_TCF_Original__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c);
                    }
                    posAcc.AxtriaARSnT__Segment__c = cprcd.AxtriaARSnT__Segment__c;
                    posAcc.AxtriaARSnT__Segment_Approved__c = cprcd.AxtriaARSnT__Segment__c;
                    posAcc.AxtriaARSnT__Segment_Original__c = cprcd.AxtriaARSnT__Segment__c;
                    if(cprcd.AxtriaARSnT__Previous_Cycle_Calls__c != null)
                    {
                        posAcc.AxtriaSalesIQTM__Segment10__c = cprcd.AxtriaARSnT__Previous_Cycle_Calls__c;
                    }
                    
                    posAcc.lastApprovedTarget__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget_Approved__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget_Updated__c  =  cprcd.Target__c;
                    
                    posAcc.AxtriaSalesIQTM__Picklist1_Updated__c  =  cprcd.Picklist1_Updated__c;
                    posAcc.AxtriaSalesIQTM__Change_Status__c  =  cprcd.Change_Status__c;
                    posAcc.AxtriaSalesIQTM__Checkbox1__c  =  cprcd.Checkbox1__c;
                    posAcc.AxtriaSalesIQTM__Metric1__c  =  cprcd.Metric1__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric__c  =  cprcd.Picklist1_Metric__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric_Approved__c  =  cprcd.Picklist1_Metric_Approved__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric_Updated__c  =  cprcd.Picklist1_Metric_Updated__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment__c  =  cprcd.Picklist1_Segment__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment_Approved__c  =  cprcd.Picklist1_Segment_Approved__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment_Updated__c  =  cprcd.Picklist1_Segment_Updated__c;
                    posAcc.AxtriaSalesIQTM__Segment1__c  =  cprcd.Segment1__c;
                    
                    
                    if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c )+String.valueof(posAcc.AxtriaSalesIQTM__Position__c )+String.valueof(posAcc.AxtriaARSnT__P1__c))){
                        posAccListUpdate.add(posAcc);
                        pacpSet.add(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c )+String.valueof(posAcc.AxtriaSalesIQTM__Position__c )+String.valueof(posAcc.AxtriaARSnT__P1__c));
                    }else{
                    
                    AxtriaSalesIQTM__Position_Account_Call_Plan__c updateposacc= new AxtriaSalesIQTM__Position_Account_Call_Plan__c ();
                    updateposacc= Cycle_HCP_Product_Positionmap.get(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c )+String.valueof(posAcc.AxtriaSalesIQTM__Position__c )+String.valueof(posAcc.AxtriaARSnT__P1__c));
                    updateposacc.AxtriaARSnT__Final_TCF__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c);
                    updateposacc.AxtriaARSnT__Final_TCF_Approved__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c);
                    updateposacc.AxtriaARSnT__Final_TCF_Original__c = Integer.valueOf(cprcd.AxtriaARSnT__Objective__c); 
                    
                    update updateposacc;
                       
                        //posAccListUpdate.add(posAcc);
                    }
              }
                
             
            }
          if(errorList.size() > 0){
                insert errorList;
          }
           // AxtriaARSnT.CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
            customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
            customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
            system.debug('==========customsetting========'+customsetting);
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            //update customsetting ;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;

            upsert posAccListUpdate;
            system.debug('============upsert done===================');
            customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
            //update customsetting ;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;*/
       
            //CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.

          
      }
    
          global void finish(Database.BatchableContext BC){

          /*  try
            {
               // Checkuserpositions cup = new Checkuserpositions(selectedTeamInstance);    
            }
            catch(Exception e)
            {
                system.debug('++++++++++++ ISSUE IN User Pos '+ e.getMessage());
            }*/
            
            
            //Database.executebatch(new AxtriaARSnT.BatchUpdateSharedFlag(selectedTeamInstance,'directLoad'),2000);
            
       }

}