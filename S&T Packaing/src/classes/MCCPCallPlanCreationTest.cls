/**
@author       : A1942
@createdDate  : 07-08-2020
@description  : Test class for the MCCPCallPlanCreation.cls 
@Revison(s)   : v1.0
 */
@isTest
public with sharing class MCCPCallPlanCreationTest 
{
    public static testmethod void callPlanCreationTest()
    {
        //Basic Data preperation
        String className = 'MCCPCallPlanCreationTest';
        AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgMaster,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        country.Channels__c = 'F2F;Email';
        country.MCCP_Enabled__c = true;
        SnTDMLSecurityUtil.insertRecords(country,className);

        String workspaceName = 'TestWorkspace';
        Date startDate = Date.newInstance(2020, 2, 17);
        Date endDate = Date.newInstance(2020, 8, 17);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace(workspaceName, startDate, endDate);
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);

        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team, teamInstance);
        SnTDMLSecurityUtil.insertRecords(position,className);

        String stage = 'Design';
        String processStage = 'Ready';
        String status = 'Active';
        AxtriaSalesIQTM__User_Access_Permission__c userPosition = TestDataFactory.createUserAccessPerm(position, teamInstance, UserInfo.getUserId());
        SnTDMLSecurityUtil.insertRecords(userPosition,className);

        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.createScenario(workspace.id, teamInstance.id, null, stage, processStage, status, startDate, endDate); 
        SnTDMLSecurityUtil.insertRecords(scenario,className);

        Product_Catalog__c prodCatalog = TestDataFactory.productCatalog(team, teamInstance, country);
        prodCatalog.Veeva_External_ID__c = prodCatalog.Product_Code__c;//Veeva_External_Id must be equal to Product_Code;
        SnTDMLSecurityUtil.insertRecords(prodCatalog,className);

        Measure_Master__c measureMaster = TestDataFactory.createMeasureMaster(prodCatalog, team, teamInstance); 
        measureMaster.MCCP_Selected_Channels__c = 'Email';
        measureMaster.MCCP_Selected_Products__c = 'Product1';
        SnTDMLSecurityUtil.insertRecords(measureMaster,className);

        List<String> productNameList = new List<String>();
        productNameList.add(prodCatalog.Name);
        List<String> channelNameList = new List<String>();
        channelNameList.add('F2F');

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        MCCPCallPlanCreation.getAllWorkspaces();
        MCCPCallPlanCreation.getAllTeams();
        MCCPCallPlanCreation.getTeamInstances(workspace.id, team.id);
        MCCPCallPlanCreation.getProductCatalogs(teamInstance.id);
        MCCPCallPlanCreation.getChannels();
        MCCPCallPlanCreation.getMccpCallPlans(productNameList, teamInstance.id,'');
        MCCPCallPlanCreation.fetchMeasureMaster(measureMaster.id,'');
        MCCPCallPlanCreation.createMeasureMaster('New',measureMaster.id,'TestRule',team.id,workspace.id,teamInstance.id,'','',productNameList,channelNameList,new List<String>());

        System.Test.stopTest();
    }
}