global with sharing class BatchMergeUnmerge implements Database.Batchable<sObject>, Database.Stateful, schedulable {
  //public list<SIQ_CM_Merge_Unmerge__c> MergeList {get;set;}
   public String query {get;set;}
   public DateTime lastjobDate;
   public Integer recordsProcessed;
   public map<string,string>accountNum2Idmap {get;set;}
   //Change by Ayushi
   public map<string,string>mapAccNumber2Status {get;set;}
   public set<String> allInputAccNumberSet {get;set;}
   public list<Merge_Unmerge__c> insertMergeList{get;set;}
   public list<SIQ_CM_Merge_Unmerge__c> updateInputList{get;set;}
   public map<String, String> mapVeevaCode ;
   public map<String, String> mapMarketingCode ;
   public Integer errorcount;
   public String nonProcessedAcs {get;set;}
   public String batchID;
   public list<Cluster_Country__mdt> clusterCountry{get;set;}// Shivansh - A1450 -- Object Purging
   public map<string,string>mergeAccNum2IDmap {get;set;}

    global BatchMergeUnmerge() {
         this.query = query;
         query = '';
         nonProcessedAcs = '';
         recordsProcessed = 0;
         errorcount=0;
         lastjobDate=null;
         accountNum2Idmap = new map<String,string>();
         mergeAccNum2IDmap = new map<String,string>();
         mapAccNumber2Status = new map<String,String>();
         allInputAccNumberSet = new set<String>();
         insertMergeList = new list<Merge_Unmerge__c>();
         updateInputList = new list<SIQ_CM_Merge_Unmerge__c>();
         mapVeevaCode=new map<String, String>();
         mapMarketingCode =new map<String, String>();
			//testupdate
         //Populate method code
         list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c  WHERE SIQ_Country_Code__c != NULL];
         for(SIQ_MC_Country_Mapping__c c : countryMap){
            if(!mapVeevaCode.containskey(c.SIQ_Country_Code__c)){
                mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
                SnTDMLSecurityUtil.printDebugMessage('SIQ_Country_Code__c :::::' +c.SIQ_Country_Code__c);
                SnTDMLSecurityUtil.printDebugMessage('SIQ_Veeva_Country_Code__c :::::' +c.SIQ_Veeva_Country_Code__c);
            }

            SnTDMLSecurityUtil.printDebugMessage('mapVeevaCode :::::' +mapVeevaCode);
            if(!mapMarketingCode.containskey(c.SIQ_Country_Code__c)){
              mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
              SnTDMLSecurityUtil.printDebugMessage('SIQ_Country_Code__c for Marketing Code:::::' +c.SIQ_Country_Code__c);
              SnTDMLSecurityUtil.printDebugMessage('SIQ_MC_Code__c :::::' +c.SIQ_MC_Code__c);
            }
          } 
        
         List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         String cycle=null;
         cycleList=[Select Name,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and AxtriaSalesIQTM__Scenario__c != null];
         if(cycleList!=null)
         {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name !=null && t1.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name !='')
                cycle = t1.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name;
            }
          }
        
         schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MergeUnmerge Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
         if(schLogList.size()>0)
         {
            lastjobDate=schLogList[0].Created_Date2__c;  
         }
         else
         {
            lastjobDate=null;
         }
         SnTDMLSecurityUtil.printDebugMessage('last job'+lastjobDate);
         Scheduler_Log__c sJob = new Scheduler_Log__c();
         sJob.Job_Name__c = 'MergeUnmerge Delta';
         sJob.Job_Status__c = 'Failed';
         sJob.Job_Type__c='Inbound';
         sJob.Cycle__c=cycle;
         sJob.Created_Date2__c = DateTime.now();
         //insert sJob;
         SnTDMLSecurityUtil.insertRecords(sJob, 'BatchMergeUnmerge');

         batchID = sJob.Id;
         recordsProcessed =0;
         SnTDMLSecurityUtil.printDebugMessage('============lastjobDate::'+lastjobDate);
         query = 'select SIQ_Country_Code__c,SIQ_Customer_Class__c,SIQ_External_ID__c,SIQ_Losing_Source_ID__c,' +
                'SIQ_Market_Code__c,SIQ_MDM_Customer_GUID_1__c,SIQ_MDM_Customer_GUID_2__c,SIQ_MDM_Customer_ID_1__c, '+
                'SIQ_MDM_Customer_ID_2__c,SIQ_MDM_Previous_Customer_GUID__c,SIQ_Surviving_Source_ID__c '+
                ' from SIQ_CM_Merge_Unmerge__c where Status__c = \'New\'';

         if(lastjobDate!=null)
         {
                query = query + ' and LastModifiedDate  >=:  lastjobDate WITH SECURITY_ENFORCED'; 
         }
         SnTDMLSecurityUtil.printDebugMessage('query'+ query);
            
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
            
            return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchMergeUnmerge());                                                           
       
    }

    global void execute(Database.BatchableContext BC, list<SIQ_CM_Merge_Unmerge__c> MergeList) {
        SnTDMLSecurityUtil.printDebugMessage('=====MergeList====='+MergeList);
        SnTDMLSecurityUtil.printDebugMessage('======MergeList.Size():::'+MergeList.size());

        
        accountNum2Idmap = new map<String,string>();
        mergeAccNum2IDmap = new map<String,string>();
        mapAccNumber2Status = new map<String,String>();
        allInputAccNumberSet = new set<String>();
        insertMergeList = new list<Merge_Unmerge__c>();
        updateInputList = new list<SIQ_CM_Merge_Unmerge__c>();
        set<String>loosingAccountset = new set<String>();
        map<String,String> clustermap = new map<String,String>();

        // Shivansh - A1450 -- Object Purging

        clusterCountry = [SELECT Id, DeveloperName,Label, Country_Code__c FROM Cluster_Country__mdt WHERE DeveloperName != NULL];
        if(clusterCountry != null && clusterCountry.size() > 0){
           for(Cluster_Country__mdt cluster : clusterCountry){
             clustermap.put(cluster.DeveloperName,cluster.Country_Code__c);
             SnTDMLSecurityUtil.printDebugMessage('====cluster map:::::' +clustermap);
           }
        }

        // clusterCountry = ClusterCountry__c.getall().values();
        // if(clusterCountry != null && clusterCountry.size() > 0){
        //    for(ClusterCountry__c cluster : clusterCountry){
        //      clustermap.put(cluster.Name,cluster.Country_Code__c);
        //      SnTDMLSecurityUtil.printDebugMessage('====cluster map:::::' +clustermap);
        //    }
        // }


        
        for(SIQ_CM_Merge_Unmerge__c  obj : MergeList){
          allInputAccNumberSet.add(obj.SIQ_MDM_Customer_GUID_1__c);
          allInputAccNumberSet.add(obj.SIQ_MDM_Customer_GUID_2__c);
          loosingAccountset.add(obj.SIQ_MDM_Customer_GUID_2__c);
        }
        SnTDMLSecurityUtil.printDebugMessage('======allInputAccNumberSet::=='+allInputAccNumberSet);
        SnTDMLSecurityUtil.printDebugMessage('loosingAccountset :::::' +loosingAccountset);

        Set<String> existedAccountset = new Set<String>();
        List<Account>AccountList = [Select id,AccountNumber,Status__c,Merge_Account_Number__c from Account where AccountNumber IN:allInputAccNumberSet];
        for(Account acc : AccountList){
            existedAccountset.add(acc.AccountNumber);
            accountNum2Idmap.put(acc.AccountNumber,acc.Id);
            mergeAccNum2IDmap.put(acc.Merge_Account_Number__c,acc.Id);
            mapAccNumber2Status.put(acc.AccountNumber,acc.Status__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('=======Accountlist Size++++'+AccountList.size());

        SnTDMLSecurityUtil.printDebugMessage('existedAccountset :::::' +existedAccountset);
        SnTDMLSecurityUtil.printDebugMessage('accountNum2Idmap :::::' +accountNum2Idmap);
        SnTDMLSecurityUtil.printDebugMessage('mergeAccNum2IDmap :::::' +mergeAccNum2IDmap);
        SnTDMLSecurityUtil.printDebugMessage('mapAccNumber2Status :::::' +mapAccNumber2Status);
        
        for(SIQ_CM_Merge_Unmerge__c  obj : MergeList)
        {
          if(existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_1__c) && existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_2__c))
          {
              if(accountNum2Idmap.get(obj.SIQ_MDM_Customer_GUID_1__c) != null  && accountNum2Idmap.get(obj.SIQ_MDM_Customer_GUID_2__c) != null && mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_2__c) == 'Active')
              {  
                  if(!clustermap.containsKey(obj.SIQ_Country_Code__c))
                  {

                      SnTDMLSecurityUtil.printDebugMessage('Not a Cluster Market');
                      SnTDMLSecurityUtil.printDebugMessage('mapVeevaCode.get(obj.SIQ_Country_Code__c)' +mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      
                      Merge_Unmerge__c newobj = new Merge_Unmerge__c();
                      if(mapVeevaCode.get(obj.SIQ_Country_Code__c)!=null){
                           newobj.Country_Code__c=mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      }
                      newobj.Customer_Class__c=obj.SIQ_Customer_Class__c;
                      newobj.External_ID__c=obj.SIQ_MDM_Customer_GUID_1__c + '_' +obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      newobj.Losing_Source_ID__c = obj.SIQ_Losing_Source_ID__c;             
                      if(mapMarketingCode.get(obj.SIQ_Country_Code__c)!=null){
                           newobj.Market_Code__c=mapMarketingCode.get(obj.SIQ_Country_Code__c);
                      }
                      newobj.MDM_Customer_GUID_1__c = obj.SIQ_MDM_Customer_GUID_1__c;
                      newobj.MDM_Customer_GUID_2__c = obj.SIQ_MDM_Customer_GUID_2__c;
                      newobj.GUID_1_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_1__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      newobj.GUID_2_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      newobj.MDM_Customer_ID_1__c = obj.SIQ_MDM_Customer_ID_1__c;
                      newobj.MDM_Customer_ID_2__c = obj.SIQ_MDM_Customer_ID_2__c;
                      newobj.MDM_Previous_Customer_GUID__c =obj.SIQ_MDM_Previous_Customer_GUID__c;
                      newobj.Surviving_Source_ID__c = obj.SIQ_Surviving_Source_ID__c;
                      newobj.Looser_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      newobj.Winner_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_1__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      recordsProcessed ++;

                      insertMergeList.add(newobj);

                      obj.Status__c = 'Merged';
                      updateInputList.add(obj);
                  }
                  else
                  {

                      SnTDMLSecurityUtil.printDebugMessage('Cluster Market');
                      SnTDMLSecurityUtil.printDebugMessage('mapVeevaCode.get(obj.SIQ_Country_Code__c)' +mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      SnTDMLSecurityUtil.printDebugMessage('clustermap.get(obj.SIQ_Country_Code__c)' +clustermap.get(obj.SIQ_Country_Code__c));

                      Merge_Unmerge__c newobj = new Merge_Unmerge__c();
                      if(mapVeevaCode.get(obj.SIQ_Country_Code__c)!=null){
                           newobj.Country_Code__c=mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      }
                      newobj.Customer_Class__c=obj.SIQ_Customer_Class__c;
                      newobj.External_ID__c=obj.SIQ_MDM_Customer_GUID_1__c + '_' +obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      newobj.Losing_Source_ID__c = obj.SIQ_Losing_Source_ID__c;             
                      if(mapMarketingCode.get(obj.SIQ_Country_Code__c)!=null){
                           newobj.Market_Code__c=mapMarketingCode.get(obj.SIQ_Country_Code__c);
                      }
                      newobj.MDM_Customer_GUID_1__c = obj.SIQ_MDM_Customer_GUID_1__c;
                      newobj.MDM_Customer_GUID_2__c = obj.SIQ_MDM_Customer_GUID_2__c;
                      newobj.GUID_1_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_1__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      newobj.GUID_2_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c));
                      newobj.MDM_Customer_ID_1__c = obj.SIQ_MDM_Customer_ID_1__c;
                      newobj.MDM_Customer_ID_2__c = obj.SIQ_MDM_Customer_ID_2__c;
                      newobj.MDM_Previous_Customer_GUID__c =obj.SIQ_MDM_Previous_Customer_GUID__c;
                      newobj.Surviving_Source_ID__c = obj.SIQ_Surviving_Source_ID__c;
                      newobj.Looser_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_2__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      newobj.Winner_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_1__c + '_' + mapVeevaCode.get(obj.SIQ_Country_Code__c);
                      recordsProcessed ++;

                      insertMergeList.add(newobj);

                      obj.Status__c = 'Merged';
                      obj.Reason_Code__c = '';
                      updateInputList.add(obj);

                      SnTDMLSecurityUtil.printDebugMessage('Cluster Market newobj:::::::' +newobj);
                      
                      Merge_Unmerge__c clusterRec = new Merge_Unmerge__c();
                      if(clustermap.get(obj.SIQ_Country_Code__c)!=null){
                           clusterRec.Country_Code__c=clustermap.get(obj.SIQ_Country_Code__c);
                      }
                      clusterRec.Customer_Class__c=obj.SIQ_Customer_Class__c;
                      clusterRec.External_ID__c=obj.SIQ_MDM_Customer_GUID_1__c + '_' +obj.SIQ_MDM_Customer_GUID_2__c + '_' + clustermap.get(obj.SIQ_Country_Code__c);
                      clusterRec.Losing_Source_ID__c = obj.SIQ_Losing_Source_ID__c;             
                      if(mapMarketingCode.get(obj.SIQ_Country_Code__c)!=null){
                           clusterRec.Market_Code__c=mapMarketingCode.get(obj.SIQ_Country_Code__c);
                      }
                      clusterRec.MDM_Customer_GUID_1__c = obj.SIQ_MDM_Customer_GUID_1__c;
                      clusterRec.MDM_Customer_GUID_2__c = obj.SIQ_MDM_Customer_GUID_2__c;
                      clusterRec.GUID_1_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_1__c + '_' + clustermap.get(obj.SIQ_Country_Code__c));
                      clusterRec.GUID_2_ID__c = mergeAccNum2IDmap.get(obj.SIQ_MDM_Customer_GUID_2__c + '_' + clustermap.get(obj.SIQ_Country_Code__c));
                      clusterRec.MDM_Customer_ID_1__c = obj.SIQ_MDM_Customer_ID_1__c;
                      clusterRec.MDM_Customer_ID_2__c = obj.SIQ_MDM_Customer_ID_2__c;
                      clusterRec.MDM_Previous_Customer_GUID__c =obj.SIQ_MDM_Previous_Customer_GUID__c;
                      clusterRec.Surviving_Source_ID__c = obj.SIQ_Surviving_Source_ID__c;
                      clusterRec.Looser_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_2__c + '_' + clustermap.get(obj.SIQ_Country_Code__c);
                      clusterRec.Winner_Merge_Account__c  = obj.SIQ_MDM_Customer_GUID_1__c + '_' + clustermap.get(obj.SIQ_Country_Code__c);
                      recordsProcessed ++;

                      SnTDMLSecurityUtil.printDebugMessage('Cluster Market clusterRec:::::::' +clusterRec);

                      insertMergeList.add(clusterRec);

                  }
              }
              else
              {
                  if(mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_1__c) == 'Active' && mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_2__c) != 'Active')
                  {
                    obj.Status__c = 'Rejected';
                    obj.Reason_Code__c = 'Inactive Looser Account';
                    updateInputList.add(obj);
                  }
                /*  else if(mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_1__c) != 'Active' && mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_2__c) == 'Active')
                  {
                    obj.Status__c = 'Rejected';
                    obj.Reason_Code__c = 'Inactive Winner Account';
                    updateInputList.add(obj);
                  }*/
                  else if(mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_1__c) != 'Active' && mapAccNumber2Status.get(obj.SIQ_MDM_Customer_GUID_2__c) != 'Active')
                  {
                    obj.Status__c = 'Rejected';
                    obj.Reason_Code__c = 'Inactive Looser and Winner Account';
                    updateInputList.add(obj);
                  }
              }
          }
          else
          {
            if(!existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_1__c) && existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_2__c))
            {
                obj.Status__c = 'Rejected';
                obj.Reason_Code__c = 'Winner Account does not exist';
                updateInputList.add(obj);
            }
            else if(existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_1__c) && !existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_2__c))
            {
               obj.Status__c = 'Rejected';
               obj.Reason_Code__c = 'Looser Account does not exist';
               updateInputList.add(obj);
            }
            else if(!existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_1__c) && !existedAccountset.contains(obj.SIQ_MDM_Customer_GUID_2__c))
            {
               obj.Status__c = 'Rejected';
               obj.Reason_Code__c = 'Winner and Looser Account does not exist';
               updateInputList.add(obj);
            }
          }
        }


        // Upsert Insertmergelist External_ID__c;
       

        if (Schema.sObjectType.Merge_Unmerge__c.fields.External_ID__c.isUpdateable() && Schema.sObjectType.Merge_Unmerge__c.fields.External_ID__c.isCreateable() ) {

            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, Insertmergelist);
            //upsert securityDecision.getRecords() External_ID__c;
            List<Merge_Unmerge__c> temp = securityDecision.getRecords();
            Schema.SObjectField f = Merge_Unmerge__c.Fields.External_ID__c;
            database.upsert(temp ,f,false);

        //throw exception if permission is missing
            if(!securityDecision.getRemovedFields().isEmpty() )
            {
                if(Insertmergelist[0].External_ID__c != null)
                    SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'Survey_Data_Transformation_Utility', 'C' );
                else
                    SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'Survey_Data_Transformation_Utility', 'U' );
            }
        }
        else{
            SnTDMLSecurityUtil.printDebugMessage('You dont have permission to access External_Id');
        }

        if(updateInputList.size() > 0){
          //update updateInputList;
          SnTDMLSecurityUtil.updateRecords(updateInputList, 'BatchMergeUnmerge');
        }
       
        
    }

    global void finish(Database.BatchableContext BC) {
       // if(insertMergeList!=null && insertMergeList.size()>0){
	   if(recordsProcessed >=0){
          SnTDMLSecurityUtil.printDebugMessage('=====Calling Merge Event Processing+++++++');
          BatchMergeEventProcessing mep = new BatchMergeEventProcessing(lastjobDate);
          //mep.fillvalues(lastjobDate);
          Database.executeBatch(mep,1);

        }
          SnTDMLSecurityUtil.printDebugMessage(recordsProcessed + ' records processed. ');
          String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs;         
          Scheduler_Log__c sJob2 = new Scheduler_Log__c(id = batchID); 
          SnTDMLSecurityUtil.printDebugMessage('schedulerObj++++before'+sJob2);
          sJob2.No_Of_Records_Processed__c=recordsProcessed;
          sJob2.Job_Status__c='Successful';
          sjob2.Changes__c = ErrorMsg;      
          SnTDMLSecurityUtil.printDebugMessage('sJob++++++++'+sJob2);
          //update sJob2;
          SnTDMLSecurityUtil.updateRecords(sJob2, 'BatchMergeUnmerge');
    }
}