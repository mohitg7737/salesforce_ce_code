@isTest
private class PACP_ErrorFilter_UtilityTest { 
    static testMethod void testMethod1() {

        String className = 'PACP_ErrorFilter_UtilityTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        List<AxtriaSalesIQTM__TriggerContol__c> triggers = new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'CallPlanSummaryTrigger')};
        SnTDMLSecurityUtil.insertRecords(triggers,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='F55677';
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(acc,className);
        Account acc2= TestDataFactory.createAccount();
        acc2.AccountNumber ='F55644477';
        acc2.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc2.AxtriaSalesIQTM__Country__c = countr.id;
        acc2.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc2,className);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        SnTDMLSecurityUtil.insertRecords(affnet,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff.Account_Number__c = 'F55644477';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc2.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff1.Account_Number__c = 'F55677';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        accaff1.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff1,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Enable_Affiliation_CallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(teamins,className);  
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        //teamins1.Enable_Affiliation_CallPlan__c = false;
        teamins1.name ='testte';
        SnTDMLSecurityUtil.insertRecords(teamins1,className); 
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className); 
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Veeva_External_ID__c =  'testpeod';
        pcc.Product_Code__c = 'testpeod';
        SnTDMLSecurityUtil.insertRecords(pcc,className); 
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className); 
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className); 
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className); 
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        SnTDMLSecurityUtil.insertRecords(posTeam,className); 
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(product,className); 

        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(positionproduct,className); 
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Product_Name__c ='GIST';
        pPriority.Speciality_Name__c ='Cardiology';
        pPriority.CycleName__c = 'Oncology';
        pPriority.Product_Type__c ='Speciality';
        pPriority.Product__c = pcc.id;
        pPriority.TeamInstance__c =teamins.id;
        pPriority.Type__c ='Territory';
        pPriority.Position_Name__c =pos.Name;
        pPriority.TeamInstance__c =teamins.id;
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        SnTDMLSecurityUtil.insertRecords(posteamins,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Product_Type__c = 'Speciality';
        positionAccountCallPlan.P1__c = 'GIST';
        positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = false;
        positionAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c= teamins.Id;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Area__c = pos.id;
        area.Team_Instance__c = teamins.id;
        area.Position_Product__c = positionproduct.id;
        SnTDMLSecurityUtil.insertRecords(area,className);

        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='Processing';
        zt.Country__c = 'USA';
        zt.Event__c = 'Insert';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Object__c ='Call_Plan__c';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Parent_Account_ID__c = acc2.AccountNumber;
        zt.Product_Code__c = 'testpeod';
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        SnTDMLSecurityUtil.insertRecords(zt,className);

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            PACP_ErrorFilter_Utility obj=new PACP_ErrorFilter_Utility(teamins.name);
            obj.query = 'Select Id,Name,Cycle__c, AccountNumber__c, Parent_Account_ID__c, Team_Instance_Name__c,Product_Name__c, Territory_ID__c, Segment__c, Objective__c, Product_Code__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Parent_Account__c, Position__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM temp_Obj__c   '; 
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {

        String className = 'PACP_ErrorFilter_UtilityTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        List<AxtriaSalesIQTM__TriggerContol__c> triggers =  new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'CallPlanSummaryTrigger')};
        SnTDMLSecurityUtil.insertRecords(triggers,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='F55677';
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(acc,className);
        Account acc2= TestDataFactory.createAccount();
        acc2.AccountNumber ='F55644477';
        acc2.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc2.AxtriaSalesIQTM__Country__c = countr.id;
        acc2.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc2,className);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        SnTDMLSecurityUtil.insertRecords(affnet,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff.Account_Number__c = 'F55644477';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc2.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff1.Account_Number__c = 'F55677';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        accaff1.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Enable_Affiliation_CallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(teamins,className);   
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Veeva_External_ID__c =  'testpeod';
        pcc.Product_Code__c = 'testpeod';
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = null;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        SnTDMLSecurityUtil.insertRecords(posTeam,className);

        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(product,className);

        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(positionproduct,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Product_Name__c ='GIST';
        pPriority.Speciality_Name__c ='Cardiology';
        pPriority.CycleName__c = 'Oncology';
        pPriority.Product_Type__c ='Speciality';
        pPriority.Product__c = pcc.id;
        pPriority.TeamInstance__c =teamins.id;
        pPriority.Type__c ='Territory';
        pPriority.Position_Name__c =pos.Name;
        pPriority.TeamInstance__c =teamins.id;
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        SnTDMLSecurityUtil.insertRecords(posteamins,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Product_Type__c = 'Speciality';
        positionAccountCallPlan.P1__c = 'GIST';
        positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = false;
        positionAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c= teamins.Id;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Area__c = pos.id;
        area.Team_Instance__c = teamins.id;
        area.Position_Product__c = positionproduct.id;
        SnTDMLSecurityUtil.insertRecords(area,className);

        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='Processing';
        zt.Country__c = 'USA';
        zt.Event__c = 'Insert';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = null;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = null;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Object__c ='Call_Plan__c';
        zt.Territory_ID__c = null;
        zt.Parent_Account_ID__c = null;
        zt.Product_Code__c = null;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        SnTDMLSecurityUtil.insertRecords(zt,className);

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            PACP_ErrorFilter_Utility obj=new PACP_ErrorFilter_Utility(teamins.name);
            obj.query = 'Select Id,Name,Cycle__c, AccountNumber__c, Parent_Account_ID__c, Team_Instance_Name__c,Product_Name__c, Territory_ID__c, Segment__c, Objective__c, Product_Code__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Parent_Account__c, Position__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM temp_Obj__c   '; 
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {

        String className = 'PACP_ErrorFilter_UtilityTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        List<AxtriaSalesIQTM__TriggerContol__c> triggers =  new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'CallPlanSummaryTrigger')};
        SnTDMLSecurityUtil.insertRecords(triggers,className);
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='F55677';
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc2= TestDataFactory.createAccount();
        acc2.AccountNumber ='F55644477';
        acc2.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc2.AxtriaSalesIQTM__Country__c = countr.id;
        acc2.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc2,className);

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        SnTDMLSecurityUtil.insertRecords(affnet,className);

        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff.Account_Number__c = 'F55644477';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc2.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff,className);

        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff1.Account_Number__c = 'F55677';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        accaff1.Unique_Id__c =(string)acc.AccountNumber+'_'+(string)acc2.AccountNumber;
        SnTDMLSecurityUtil.insertRecords(accaff1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Enable_Affiliation_CallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        //teamins1.Enable_Affiliation_CallPlan__c = false;
        teamins1.name ='testte';
        SnTDMLSecurityUtil.insertRecords(teamins1,className); 

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className); 
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Veeva_External_ID__c =  'testpeod';
        pcc.Product_Code__c = 'testpeod';
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        SnTDMLSecurityUtil.insertRecords(posTeam,className);

        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(product,className);

        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        SnTDMLSecurityUtil.insertRecords(positionproduct,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Product_Name__c ='GIST';
        pPriority.Speciality_Name__c ='Cardiology';
        pPriority.CycleName__c = 'Oncology';
        pPriority.Product_Type__c ='Speciality';
        pPriority.Product__c = pcc.id;
        pPriority.TeamInstance__c =teamins.id;
        pPriority.Type__c ='Territory';
        pPriority.Position_Name__c =pos.Name;
        pPriority.TeamInstance__c =teamins.id;
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        SnTDMLSecurityUtil.insertRecords(posteamins,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Product_Type__c = 'Speciality';
        positionAccountCallPlan.P1__c = 'GIST';
        positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = false;
        positionAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c= teamins.Id;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Area__c = pos.id;
        area.Team_Instance__c = teamins.id;
        area.Position_Product__c = positionproduct.id;
        SnTDMLSecurityUtil.insertRecords(area,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='Processing';
        zt.Country__c = 'USA';
        zt.Event__c = 'Insert';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = 'acc.AccountNumber';
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = 'teamins.Name';
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Object__c ='Call_Plan__c';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Parent_Account_ID__c = 'acc2.AccountNumber';
        zt.Product_Code__c = 'tescccvtpeod';
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        SnTDMLSecurityUtil.insertRecords(zt,className);

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            PACP_ErrorFilter_Utility obj=new PACP_ErrorFilter_Utility(teamins.name);
            PACP_ErrorFilter_Utility obj1=new PACP_ErrorFilter_Utility(teamins.name, 'null');
            PACP_ErrorFilter_Utility obj2=new PACP_ErrorFilter_Utility(teamins.name, 'null', 'null');
            PACP_ErrorFilter_Utility obj3=new PACP_ErrorFilter_Utility(teamins.name, 'null', false);
            obj.query = 'Select Id,Name,Cycle__c, AccountNumber__c, Parent_Account_ID__c, Team_Instance_Name__c,Product_Name__c, Territory_ID__c, Segment__c, Objective__c, Product_Code__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Parent_Account__c, Position__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM temp_Obj__c   '; 
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    
}