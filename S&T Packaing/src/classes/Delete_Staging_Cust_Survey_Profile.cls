//Author Chirag Ahuja
global with sharing class Delete_Staging_Cust_Survey_Profile implements Database.Batchable<sObject> {
    public String query;
    public String teamInstance;
    public String type;
    public String product;
    public boolean HCOFlag = false;
    public String changeReqID;
    public String teamInstance_Prod;
    public Boolean proceedBatch = true;    
    public Boolean scheduledjob = false;
    public Boolean ondemand = false;    
    public Set<string> teaminstancelist;
    public List<string> teamInst_ProdList;
    public String selectedSourceRule;
    public String selectedDestinationRule;

    global Delete_Staging_Cust_Survey_Profile() {
        scheduledjob = true;
        SnTDMLSecurityUtil.printDebugMessage('+++empty construct');
        teaminstancelist = new Set<String>();
        teaminstancelist = StaticTeaminstanceList.getCompleteRuleTeamInstances();
        SnTDMLSecurityUtil.printDebugMessage('+++teaminstancelist' + teaminstancelist);
        query = 'SELECT Id FROM Staging_Cust_Survey_Profiling__c WHERE Team_Instance__c not in:teaminstancelist WITH SECURITY_ENFORCED';
    }
    global Delete_Staging_Cust_Survey_Profile(String teamInstanceProd)
    {
        scheduledjob=false;
        ondemand = true;
        teamInstance_Prod = teamInstanceProd;
        System.debug('teamInstance_Prod====?'+teamInstance_Prod);
        teamInst_ProdList=new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList=teamInstance_Prod.split(';');
        }
        system.debug('teamInst_ProdList====?'+teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        system.debug('teamInstance====?'+teamInstance+'=====product====?'+product);

        query='select id from Staging_Cust_Survey_Profiling__c WHERE BRAND_ID__c =: product and Team_Instance__c = \'' + teamInstance + '\' WITH SECURITY_ENFORCED ';
    }
    global Delete_Staging_Cust_Survey_Profile(String teamInstance, String type) {
        this.teamInstance = teamInstance;
        this.type = type;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance===?'+teamInstance);
        SnTDMLSecurityUtil.printDebugMessage('type===?'+type);
        query = 'SELECT Id FROM Staging_Cust_Survey_Profiling__c WHERE Team_Instance__c = \'' + teamInstance + '\' WITH SECURITY_ENFORCED';
        this.query = query;
    }
    global Delete_Staging_Cust_Survey_Profile(String teamInstance, String type, String crID) {
        this.teamInstance = teamInstance;
        this.type = type;
        changeReqID = crID;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance===?'+teamInstance+'==type===?'+type+'==changeReqID===?'+changeReqID);
        
        query = 'SELECT Id FROM Staging_Cust_Survey_Profiling__c WHERE Team_Instance__c = \'' + teamInstance + '\' WITH SECURITY_ENFORCED';
        this.query = query;
    }
    //Related to HCO Segmentation
    global Delete_Staging_Cust_Survey_Profile(String sourceRuleId, String destinationRuleId, String teamInstance, String prod, String type, boolean flag) {
        this.selectedSourceRule = sourceRuleId;
        this.selectedDestinationRule = destinationRuleId;
        this.teamInstance = teamInstance;
        this.type = type;
        this.product = prod;
        this.HCOFlag = flag;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance===?'+teamInstance+'==type===?'+type+'==product===?'+product+'==HCOFlag===?'+HCOFlag);

        query = 'SELECT Id FROM Staging_Cust_Survey_Profiling__c WHERE Team_Instance__c = \'' + teamInstance + '\'and Type__c = \'' + type + '\' and BRAND_ID__c = \'' + product + '\' WITH SECURITY_ENFORCED';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('query===?' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Cust_Survey_Profiling__c> scope) {
        SnTDMLSecurityUtil.printDebugMessage('scope size===?' + scope.size());
        if(scope.size() > 0 && scope != null)
        {
            if(Staging_Cust_Survey_Profiling__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
                Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope);
            }
            else
            {
                proceedBatch = false;
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Staging_BU_Response__c', 'Delete_Staging_BU_Response');
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

        if(scheduledjob){
            Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response();
            database.executeBatch(batch, 500);
        }
        else{
            if(ondemand){
                Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance_Prod);
                Database.executeBatch(batch, 500);
            }
            else{
                if(changeReqID!=null && changeReqID!=''){
                    if(proceedBatch){
                        Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance,type,changeReqID);
                        Database.executeBatch(batch, 500);
                    }
                    else{
                        SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
                    }
                }
                else{
                    //Related to HCO Segmentation
                    if(HCOFlag){
                        Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(selectedSourceRule,selectedDestinationRule,teamInstance,product,type,HCOFlag);
                        Database.executeBatch(batch, 500);
                    }
                    else{
                        Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance,type);
                        Database.executeBatch(batch, 500);
                    }
                }
            }
        }
    }
}