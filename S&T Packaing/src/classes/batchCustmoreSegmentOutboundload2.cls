global class batchCustmoreSegmentOutboundload2 implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';

        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Master_List_AccProd__c> scope) {}  
    
    global void finish(Database.BatchableContext BC) {
    }
}