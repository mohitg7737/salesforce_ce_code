global with sharing class CreateHCOSegBatch implements Database.Batchable<sObject> {
    public String query;
    public Map<String,Map<String,Decimal>> hcoToMapOfParamValMap;
    public String destinationRuleId;
    public String sourceRuleId;
    public Set<String> hcoSet;
    
    //Added by Chirag Ahuja for STIMPS-286
    public boolean HCOFlag = false;
    public String teamInstance;
    public String type;

    public static Boolean isInteger(String s){
        Boolean ReturnValue;
        try{
            Integer.valueOf(s);
            ReturnValue = TRUE; 
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }
    global CreateHCOSegBatch(String sourceRuleId,String destinationRuleId,Map<String,Map<String,Decimal>> hcoToMapOfParamValMap) {
        this.hcoToMapOfParamValMap = hcoToMapOfParamValMap;
        this.destinationRuleId = destinationRuleId;
        this.sourceRuleId = sourceRuleId;
        hcoSet = hcoToMapOfParamValMap.keySet();
        query = 'Select Id from Account where Id in :hcoSet WITH SECURITY_ENFORCED';
        this.query = query;
    }

    //Added by Chirag Ahuja for STIMPS-286
    global CreateHCOSegBatch(String sourceRuleId,String destinationRuleId,Map<String,Map<String,Decimal>> hcoToMapOfParamValMap, String teamInstance, String type, Boolean flag) {
        this.hcoToMapOfParamValMap = hcoToMapOfParamValMap;
        this.destinationRuleId = destinationRuleId;
        this.sourceRuleId = sourceRuleId;
        this.teamInstance = teamInstance;
        this.type = type;
        this.HCOFlag = flag;
        hcoSet = hcoToMapOfParamValMap.keySet();
        query = 'Select Id from Account where Id in :hcoSet WITH SECURITY_ENFORCED';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('query --> ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {
        
        List<Account_Compute_Final__c> acfListToInsert = new List<Account_Compute_Final__c>();
        Account_Compute_Final__c acf;
        Integer counter = 1;

        for(Account posAcc : scope)
        {
            if(hcoToMapOfParamValMap.containsKey(posAcc.Id))
            {
                counter = 1;
                acf = new Account_Compute_Final__c();
                acf.Measure_Master__c = destinationRuleId;
                acf.Physician_2__c = posAcc.Id;
                //acf.Position_ID__c = posAcc.AxtriaSalesIQTM__Position__c;
                //acf.Physician__c = posAcc.Id;
                acf.External_Id__c = posAcc.Id + '_' + destinationRuleId;
                for(String str : hcoToMapOfParamValMap.get(posAcc.Id).keySet())
                {
                    Decimal d = hcoToMapOfParamValMap.get(posAcc.Id).get(str);
                    Boolean flag = isInteger(String.valueOf(d));
                    d = flag == true? d : d.setScale(3);
                    acf.put('Output_Name_'+counter+'__c',str);
                    acf.put('Output_Value_'+counter+'__c',String.valueOf(d));
                    counter++;
                }
                acfListToInsert.add(acf);
            }
        }
        //upsert acfListToInsert External_Id__c;
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, acfListToInsert);
        List<Account_Compute_Final__c> acfUpsertList = securityDecision.getRecords();
        upsert acfUpsertList External_ID__c;

        //throw exception if permission is missing
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            if(acfListToInsert[0].External_ID__c != null)
                throw new SnTException(securityDecision, 'CreateHCOSegBatch', SnTException.OperationType.C );
            else
                throw new SnTException(securityDecision, 'CreateHCOSegBatch', SnTException.OperationType.U );
        }
    }

    global void finish(Database.BatchableContext BC) {
        //Added by Chirag Ahuja for STIMPS-286
        if(HCOFlag){
            Database.executeBatch(new BatchCreateStagingSurveyDataForHCO(sourceRuleId,destinationRuleId,teamInstance,type,HCOFlag),2000);
        }
        else{
            Database.executeBatch(new BatchCreateStagingSurveyDataForHCO(sourceRuleId,destinationRuleId),2000);
        }        
    }
}