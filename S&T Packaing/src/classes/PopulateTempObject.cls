public with sharing class PopulateTempObject implements Database.Batchable<String>, Database.Stateful, schedulable
{
    /*public AxtriaSalesIQTM__Change_Request__c changeRequestCreation;
    public String attachmentFileId;
    public String objectName;
    public list <HeaderFieldMappingWrapper> fieldlist;
    public Integer recordsToinsertSize;
    public String selectedJuncObj;
    public list<temp_Obj__c> recordsToinsert;*/
    public list<String> fileData;
    /*public String selectedTeam;
    public String selectedTeamIns;
    public String selectedLoadType;


    public PopulateTempObject(list<String> filelines ,AxtriaSalesIQTM__Change_Request__c crObj, String objName, String selJuncObj, list <HeaderFieldMappingWrapper> fieldlst,String team, String teamInstance, String loadType)
    {
        
        fileData = filelines;
        objectName = objName;
        changeRequestCreation = crObj;
        selectedJuncObj = selJuncObj;
        fieldlist = fieldlst;
        selectedTeam = team;
        selectedTeamIns = teamInstance;
        selectedLoadType = loadType;

    }*/
    
    public list<String> start(Database.BatchableContext BC)
    {
        
        /*//recordsToInsert = new list<temp_Obj__c>();
        
        //integer j = 0;    

        /*for (Integer i = 1; i < fileData.size(); i++)
        {
            rec = new Map <string, object> ();
            Integer j = 0;
            for (string inputValue : fileData[i].split(','))
            {
                if (fieldlist[j].selField != '---select---' && fieldlist[j].selField != null)
                {
                    if (inputValue != null)
                    {
                        inputValue = inputValue.normalizespace();
                    }

                    if (inputValue != null ){
                        if(inputValue.equalsIgnorecase('TRUE') || inputValue.equalsIgnorecase('FALSE'))
                        {
                            SnTDMLSecurityUtil.printDebugMessage(' input value for visible is  ::' + inputValue);
                            inputValue = inputValue.tolowercase();
                        }
                    }

                    if (string.IsBlank(inputValue))
                    {
                        inputValue = null;
                    }

                    rec.put(fieldlist[j].selField, inputValue);
                }
                j++;
            }
            
            recordsToinsert.add((temp_Obj__c) JSON.deserializestrict(JSON.serialize(rec), Type.forname('temp_Obj__c')));
        }*/

        return fileData;
    }
    public void execute(System.SchedulableContext SC)
    {
        //database.executeBatch(new BatchPopulateEmployee());
    }
    public void execute(Database.BatchableContext BC, List<String> csvFileData)
    {
        /*recordsToinsert = new list<temp_Obj__c>();
        Map <string, object> rec = new Map <string, object> ();
        for (Integer i = 0; i < csvFileData.size(); i++)
        {
            rec = new Map <string, object> ();
            Integer j = 0;
            for (string inputValue : fileData[i].split(','))
            {
                if (fieldlist[j].selField != '---select---' && fieldlist[j].selField != null)
                {
                    if (inputValue != null)
                    {
                        inputValue = inputValue.normalizespace();
                    }
                    if (inputValue != null ){
                        if(inputValue.equalsIgnorecase('TRUE') || inputValue.equalsIgnorecase('FALSE'))
                        {
                            SnTDMLSecurityUtil.printDebugMessage(' input value for visible is  ::' + inputValue);
                            inputValue = inputValue.tolowercase();
                        }
                    }
                    if (string.IsBlank(inputValue))
                    {
                        inputValue = null;
                    }

                    rec.put(fieldlist[j].selField, inputValue);
                }
                j++;
            }
            
            recordsToinsert.add((temp_Obj__c) JSON.deserializestrict(JSON.serialize(rec), Type.forname('temp_Obj__c')));
        }
        System.debug('objectName -'+objectName);
        System.debug('changeRequestCreation.Id -'+changeRequestCreation.Id);
        System.debug('recordsToinsert size -'+recordsToinsert.size());
        //list<temp_Obj__c> tempObjListToInsert = (list<temp_Obj__c>)csvFileData;

        for (temp_Obj__c a : recordsToinsert)
        {
            System.debug('tempobject - '+a);
            a.Change_Request__c = changeRequestCreation.Id;
            a.Status__c = 'New';
            a.Event__c = 'Insert';
            a.Object__c = objectName;
            a.Object_Name__c = objectName;
            System.debug('tempobject - '+a);
        }
        
        //recordsToinsertSize = tempObjListToInsert.size();
        System.debug('inside execute --- '+recordsToinsert.size());
        //insert tempObjListToInsert;
        SnTDMLSecurityUtil.insertRecords(recordsToinsert, 'Populate_Position_Product_Handler_PC');*/
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
        /*System.debug('--- inside finish ----');
        
        System.debug('selectedJuncObj -'+selectedJuncObj);
        Integer batchsize = 500;
        Id batchObj;
        if (selectedJuncObj == 'Populate Employee')
        {
             batchObj = Database.executeBatch(new BatchPopulateEmployee(changeRequestCreation.Id), batchsize);
            
        }
        else if (selectedJuncObj == 'Zip_Terr')
        {
            batchObj = Database.executeBatch(new UpdateZipTerr(selectedTeam, selectedTeamIns, changeRequestCreation.Id), batchsize); //batch to update geography and posiition lookups
            
        }
        else if (selectedJuncObj == 'Acc_Terr')
        {
            batchObj = Database.executeBatch(new UpdateAccTerr(selectedTeam, selectedTeamIns, changeRequestCreation.Id), batchsize); //batch to update position and account lookups
            
        }
        else if(selectedJuncObj == 'Call_Plan')
        {
            String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
            batchObj = Database.executeBatch(new PACP_ErrorFilter_Utility(TIname, changeRequestCreation.Id), batchsize); //batch to insert Call plan Data
            
        }
        //Survey
        //Changed by HT(A0994) on 16th June 2020
        else if(selectedJuncObj == 'Survey')
        {
            /*String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
            batchObj = Database.executeBatch(new Delete_Metadata_Definition(TIname, selectedLoadType), batchsize);*/
            /*String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
            batchObj = Database.executeBatch(new BatchPopulateSurveyDirectLoad(TIname,changeRequestCreation.Id,selectedLoadType), batchsize);
        }*/
        /*else if (selectedJuncObj == 'Position Product')
        {
            batchObj = Database.executeBatch(new PositionProductDirectLoad(selectedTeamIns, changeRequestCreation.Id), batchsize); //batch to insert position product Data
            
        }
        else if (selectedJuncObj == 'Assign Product Type')
        {
            batchObj = Database.executeBatch(new AssignProductType(selectedTeamIns), batchsize); //, crID //batch to insert position product Data
        }
        else if (selectedJuncObj == 'Populate HCP Metrics')
        {
            batchObj = Database.executeBatch(new BatchPopulatePAMetrics(selectedTeamIns, changeRequestCreation.Id), batchsize); //batch to insert HCP Metrics
            
        }
        /* Added by Shivansh(A1450) for SNT-110 (To provide direct load utility for Updating Zip Terr Metrics)*/
        //else if (selectedJuncObj == 'Populate Zip-Terr Metrics')
        /*{
            batchObj = Database.executeBatch(new BatchPopulatePosGeoMetrics(selectedTeamIns, changeRequestCreation.Id), batchsize); //batch to insert HCP Metrics
            
        }
        //Batch by ayush for SNT-110 (To provide direct load utility for Loading CIM Config)
        else if (selectedJuncObj == 'Populate CIM Configuration')
        {
            batchObj = Database.executeBatch(new BatchPopulateCimConfig(selectedTeamIns, changeRequestCreation.Id), batchsize);
            
        }
        else if (changeRequestCreation.Object_Name__c == 'Channel Preference')
        {
            Database.executeBatch(new BatchPopulateChannelPreferenceData(changeRequestCreation.Id), batchsize);
        }*/

    }
    

}