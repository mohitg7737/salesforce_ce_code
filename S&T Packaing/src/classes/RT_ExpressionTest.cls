@isTest class RT_ExpressionTest {

	/* Booleans
	 */
	
	@isTest static void testAndTokenizer() {
		RT_Expression e = new RT_Expression('1&&0');
		Iterator<RT_Expression.Token> it = e.getExpressionTokenizer();
		System.assertEquals('1', it.next().text);
		System.assertEquals('&&', it.next().text);
		System.assertEquals('0', it.next().text);
	}
	
	@isTest static void testAndRPN() {
		System.assertEquals('1 0 &&', new RT_Expression('1&&0').toRPN());
	}
	
	@isTest static void testAndEval() {
		System.assertEquals(false, new RT_Expression('1&&0').evalBool());
		System.assertEquals(true, new RT_Expression('1&&1').evalBool());
		System.assertEquals(false, new RT_Expression('0&&0').evalBool());
		System.assertEquals(false, new RT_Expression('0&&1').evalBool());
	}
	
	@isTest static void testOrEval() {
		System.assertEquals('1', new RT_Expression('1||0').eval() + '');
		System.assertEquals('1', new RT_Expression('1||1').eval() + '');
		System.assertEquals('0', new RT_Expression('0||0').eval() + '');
		System.assertEquals('1', new RT_Expression('0||1').eval() + '');
	}
	
	@isTest static void testCompare() {
		System.assertEquals('1', new RT_Expression('2>1').eval() + '');
		System.assertEquals('0', new RT_Expression('2<1').eval() + '');
		System.assertEquals('0', new RT_Expression('1>2').eval() + '');
		System.assertEquals('1', new RT_Expression('1<2').eval() + '');
		System.assertEquals('0', new RT_Expression('1=2').eval() + '');
		System.assertEquals('1', new RT_Expression('1=1').eval() + '');
		System.assertEquals('1', new RT_Expression('1>=1').eval() + '');
		System.assertEquals('1', new RT_Expression('1.1>=1').eval() + '');
		System.assertEquals('0', new RT_Expression('1>=2').eval() + '');
		System.assertEquals('1', new RT_Expression('1<=1').eval() + '');
		System.assertEquals('0', new RT_Expression('1.1<=1').eval() + '');
		System.assertEquals('1', new RT_Expression('1<=2').eval() + '');
		System.assertEquals('0', new RT_Expression('1=2').eval() + '');
		System.assertEquals('1', new RT_Expression('1=1').eval() + '');
		System.assertEquals('1', new RT_Expression('1!=2').eval() + '');
		System.assertEquals('0', new RT_Expression('1!=1').eval() + '');
	}
	
	@isTest static void testCompareCombined() {
		System.assertEquals('1', new RT_Expression('(2>1)||(1=0)').eval() + '');
		System.assertEquals('0', new RT_Expression('(2>3)||(1=0)').eval() + '');
		System.assertEquals('1', new RT_Expression('(2>3)||(1=0)||(1&&1)').eval() + '');
	}
	
	/*@isTest static void testMixed() {
		System.assertEquals('0', new RT_Expression('1.5 * 7 = 3').eval() + '');
		System.assertEquals('1', new RT_Expression('1.5 * 7 = 10.5').eval() + '');
	}*/
	
	/*@isTest static void testNot() {
		System.assertEquals('0', new RT_Expression('not(1)').eval() + '');
		System.assertEquals('1', new RT_Expression('not(0)').eval() + '');
		System.assertEquals('1', new RT_Expression('not(1.5 * 7 = 3)').eval() + '');
		System.assertEquals('0', new RT_Expression('not(1.5 * 7 = 10.5)').eval() + '');
	}*/
	
	@isTest static void testConstants() {
		System.assertEquals(true, new RT_Expression('TRUE!=FALSE').evalBool());
		System.assertEquals(false, new RT_Expression('TRUE==2').evalBool());
		System.assertEquals(true, new RT_Expression('NOT(TRUE)==FALSE').evalBool());
		System.assertEquals(true, new RT_Expression('NOT(FALSE)==TRUE').evalBool());
		System.assertEquals(false, new RT_Expression('TRUE && FALSE').evalBool());
		System.assertEquals(true, new RT_Expression('TRUE || FALSE').evalBool());
	}
	
	@isTest static void testIf() {
		System.assertEquals('5', new RT_Expression('if(TRUE, 5, 3)').eval() + '');
		System.assertEquals('3', new RT_Expression('IF(FALSE, 5, 3)').eval() + '');
		System.assertEquals('5.35', new RT_Expression('If(2, 5.35, 3)').eval() + '');
	}
	
	/* Strings
	 */
	
	@isTest static void testStringCompare() {
		System.assertEquals(true, new RT_Expression('"foo" == "foo"').evalBool());
		System.assertEquals(false, new RT_Expression('"foo" == "bar"').evalBool());
		System.assertEquals(true, new RT_Expression('"foo" != "bar"').evalBool());
		System.assertEquals(true, new RT_Expression('"foo" <> "bar"').evalBool());
		System.assertEquals(true, new RT_Expression('"foo" > \'bar\'').evalBool());
		System.assertEquals(true, new RT_Expression('"foo" >= "bar"').evalBool());
		System.assertEquals(false, new RT_Expression('"foo" < "bar"').evalBool());
		System.assertEquals(false, new RT_Expression('"foo" <= "bar"').evalBool());
	}
	
	/*@isTest static void testStringConcat() {
		System.assertEquals(true, new RT_Expression('"foo" + "bar" == "foobar"').evalBool());
		System.assertEquals(true, new RT_Expression('"foo" + 1 == \'foo1\'').evalBool());
		System.assertEquals(true, new RT_Expression('1+"foo" == \'1foo\'').evalBool());
		System.assertEquals(false, new RT_Expression('1+"foo" != \'1foo\'').evalBool());
		System.assertEquals(true, new RT_Expression('2+1+"foo" = \'3foo\'').evalBool());
	}*/
	
	
	
	/* Case Insensitive
	 */
	
	
	
	
	
	public class SumFunction extends RT_Expression.Function {
		public SumFunction(String name, Integer numParams) {
			super(name, numParams);
		}
		public override Object apply(List<Object> parameters, RT_Expression.MathContext mc) {
			Decimal value = null;
			for (Object parameter : parameters) {
				Decimal d = (Decimal) parameter;
				value = value == null ? d : value + d;
			}
			return value;
		}
	}
	
	/* Customs
	 */
	
	class BitRightOperator extends RT_Expression.Operator {
		public BitRightOperator(String oper, Integer precedence, Associability assoc) {
			super(oper, precedence, assoc);
		}
		public override Object apply(Object v1, Object v2, RT_Expression.MathContext mc) {
			Decimal d1 = (Decimal) v1, d2 = (Decimal) v2;
			return d1.longValue() >> d2.longValue();
		}
	}
	
	
	
	class AvgFunction extends RT_Expression.Function {
		public AvgFunction(String name, Integer numParams) {
			super(name, numParams);
		}
		public override Object apply(List<Object> parameters, RT_Expression.MathContext mc) {
			if (numParams > -1 && numParams != parameters.size()) {
				throw new RT_Expression.ExpressionException('AVG function requires ' + numParams + ' parameters');
			}
			Decimal avg = 0;
			for (Object parameter : parameters) {
				avg += (Decimal) parameter;
			}
			return avg.divide(Decimal.valueOf(parameters.size()), mc.precision, mc.roundingMode);
		}
	}
	
	
	
	
	
	
	
	@isTest static void testExpectedParameterNumbers() {
		String err;
		try {
			RT_Expression expression = new RT_Expression('Random(1)');
			expression.eval();
		}
		catch (RT_Expression.ExpressionException e) {
			err = e.getMessage();
		}
		System.assertEquals('Function RANDOM expected 0 parameters, got 1', err);
		
		try {
			RT_Expression expression = new RT_Expression('SIN(1, 6)');
			expression.eval();
		}
		catch (RT_Expression.ExpressionException e) {
			err = e.getMessage();
		}
		System.assertEquals('Function SIN expected 1 parameters, got 2', err);
	}
	
	
	
	
	@isTest static void testIsNumber() {
		System.assertEquals(false, RT_Expression.isNumber(''));
		System.assertEquals(false, RT_Expression.isNumber('-'));
		System.assertEquals(false, RT_Expression.isNumber('+'));
		System.assertEquals(false, RT_Expression.isNumber('e'));
		System.assertEquals(false, RT_Expression.isNumber(null));
		System.assertEquals(true, RT_Expression.isNumber('-1'));
		System.assertEquals(true, RT_Expression.isNumber('-10.5'));
		System.assertEquals(true, RT_Expression.isNumber('42.24'));
	}
	
	/* Exposed Components
	 */
	
	class NullOperator extends RT_Expression.Operator {
		public NullOperator(String oper, Integer precedence, Associability assoc) {
			super(oper, precedence, assoc);
		}
		public override Object apply(Object v1, Object v2, RT_Expression.MathContext mc) {
			return null;
		}
	}
	
	@isTest static void testDeclaredOperators() {
		RT_Expression expression = new RT_Expression('c+d');
		Integer originalOperator = expression.getDeclaredOperators().size();
		RT_Expression.addOperator(new NullOperator('$$', -1, RT_Expression.Associability.LEFT));
		
		System.assert(expression.getDeclaredOperators().contains('$$'), 'Operator List should have the new $$ operator');
		System.assertEquals(expression.getDeclaredOperators().size(), originalOperator + 1, 'Should have an extra operators');
	}
	
	@isTest static void testDeclaredVariables() {
		RT_Expression expression = new RT_Expression('c+d');
		Integer originalVarCounts = expression.getDeclaredVariables().size();
		expression.setVariable('var1', 12);
		
		System.assert(expression.getDeclaredVariables().contains('VAR1'), 'Variable list should have new var1 variable declared');
		System.assertEquals(expression.getDeclaredVariables().size(), originalVarCounts + 1, 'Variable list should have q more declared variable');
	}
	
	class NullFunction extends RT_Expression.Function {
		public NullFunction(String name, Integer numParams) {
			super(name, numParams);
		}
		public override Object apply(List<Object> parameters, RT_Expression.MathContext mc) {
			return null;
		}
	}
	
	@isTest static void testDeclaredFunctionGetter() {
		RT_Expression expression = new RT_Expression('a+b');
		Integer originalFunctionCount = expression.getDeclaredFunctions().size();
		RT_Expression.addFunction(new NullFunction('func1', 3));
		
		System.assert(expression.getDeclaredFunctions().contains('FUNC1'), 'Function list should have new func1 function declared');
		System.assertEquals(expression.getDeclaredFunctions().size(), originalFunctionCount + 1, 'Function list should have one more function declared');
	}
	
	/* Nested
	 */
	
	
	
	
	/* RPN
	 */
	
	/*@isTest static void testRPNSimple() {
		System.assertEquals('1 2 +', new RT_Expression('1+2').toRPN());
		System.assertEquals('1 2 4 / +', new RT_Expression('1+2/4').toRPN());
		System.assertEquals('1 2 + 4 /', new RT_Expression('(1+2)/4').toRPN());
		System.assertEquals('1.9 2.8 + 4.7 /', new RT_Expression('(1.9+2.8)/4.7').toRPN());
		System.assertEquals('1.98 2.87 + 4.76 /', new RT_Expression('(1.98+2.87)/4.76').toRPN());
		System.assertEquals('3 4 2 * 1 5 - 2 3 ^ ^ / +', new RT_Expression('3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3').toRPN());
	}
	
	@isTest static void testRPNFunctions() {
		System.assertEquals('( 23.6 SIN', new RT_Expression('SIN(23.6)').toRPN());
		System.assertEquals('( -7 8 MAX', new RT_Expression('MAX(-7,8)').toRPN());
		System.assertEquals('( ( 3.7 SIN ( 2.6 -8.0 MAX MAX', new RT_Expression('MAX(SIN(3.7),MAX(2.6,-8.0))').toRPN());
	} */
	
	
	/* Scientific Notation
	 */
	
	
	
	@isTest static void testSciError1() {
		RT_Expression e = new RT_Expression('1234e-2.3');
		try {
			e.eval();
			System.assert(false);
		}
		catch (TypeException expected) {}
	}
	
	@isTest static void testSciError2() {
		RT_Expression e = new RT_Expression('1234e2.3');
		try {
			e.eval();
			System.assert(false);
		}
		catch (TypeException expected) {}
	}
	
	
	
	/* Tokenizer
	 */
	
	@isTest static void testTokenizerNumbers() {
		RT_Expression e;
		Iterator<RT_Expression.Token> i;
		
		e = new RT_Expression('1');
		i = e.getExpressionTokenizer();
		System.assertEquals('1', i.next().text);
		System.assertEquals(false, i.hasNext());
		
		e = new RT_Expression('-1');
		i = e.getExpressionTokenizer();
		System.assertEquals('-1', i.next().text);
		System.assertEquals(false, i.hasNext());
		
		e = new RT_Expression('123');
		i = e.getExpressionTokenizer();
		System.assertEquals('123', i.next().text);
		System.assertEquals(false, i.hasNext());
		
		e = new RT_Expression('-123');
		i = e.getExpressionTokenizer();
		System.assertEquals('-123', i.next().text);
		System.assertEquals(false, i.hasNext());
		
		e = new RT_Expression('123.4');
		i = e.getExpressionTokenizer();
		System.assertEquals('123.4', i.next().text);
		System.assertEquals(false, i.hasNext());
		
		e = new RT_Expression('-123.456');
		i = e.getExpressionTokenizer();
		System.assertEquals('-123.456', i.next().text);
		System.assertEquals(false, i.hasNext());
	}
	
	
	@isTest static void testTokenizerNextThrows() {
		try {
			RT_Expression e = new RT_Expression('1');
			Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
			System.assertEquals('1', i.next().text);
			System.assertEquals(false, i.hasNext());
			i.next();
			System.assert(false, 'Expected an exception');
		}
		catch (Exception e) {
			System.assert(e instanceof RT_Expression.IllegalStateException);
		}
	}
	
	@isTest static void testTokenizer1() {
		RT_Expression e = new RT_Expression('1+2');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('1', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('2', i.next().text);
	}
	
	
	
	@isTest static void testTokenizer4() {
		RT_Expression e = new RT_Expression('1+2-3/4*5');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('1', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('2', i.next().text);
		System.assertEquals('-', i.next().text);
		System.assertEquals('3', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('4', i.next().text);
		System.assertEquals('*', i.next().text);
		System.assertEquals('5', i.next().text);
	}
	
	@isTest static void testTokenizer5() {
		RT_Expression e = new RT_Expression('1+2.1-3.45/4.982*5.0');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('1', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('2.1', i.next().text);
		System.assertEquals('-', i.next().text);
		System.assertEquals('3.45', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('4.982', i.next().text);
		System.assertEquals('*', i.next().text);
		System.assertEquals('5.0', i.next().text);
	}
	
	@isTest static void testTokenizer6() {
		RT_Expression e = new RT_Expression('-3+4*-1');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('-3', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('4', i.next().text);
		System.assertEquals('*', i.next().text);
		System.assertEquals('-1', i.next().text);
	}
	
	@isTest static void testTokenizer7() {
		RT_Expression e = new RT_Expression('(-3+4)*-1/(7-(5*-8))');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('(', i.next().text);
		System.assertEquals('-3', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('4', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals('*', i.next().text);
		System.assertEquals('-1', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('7', i.next().text);
		System.assertEquals('-', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('5', i.next().text);
		System.assertEquals('*', i.next().text);
		System.assertEquals('-8', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals(')', i.next().text);
	}
	
	@isTest static void testTokenizer8() {
		RT_Expression e = new RT_Expression('(1.9+2.8)/4.7');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('(', i.next().text);
		System.assertEquals('1.9', i.next().text);
		System.assertEquals('+', i.next().text);
		System.assertEquals('2.8', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('4.7', i.next().text);
	}
	
	
	
	@isTest static void testTokenizerFunction1() {
		RT_Expression e = new RT_Expression('ABS(3.5)');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('ABS', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('3.5', i.next().text);
		System.assertEquals(')', i.next().text);
	}
	
	@isTest static void testTokenizerFunction2() {
		RT_Expression e = new RT_Expression('3-ABS(3.5)/9');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('3', i.next().text);
		System.assertEquals('-', i.next().text);
		System.assertEquals('ABS', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('3.5', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('9', i.next().text);
	}
	@isTest static void testTokenizerFunction3() {
		RT_Expression e = new RT_Expression('MAX(3.5,5.2)');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('MAX', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('3.5', i.next().text);
		System.assertEquals(',', i.next().text);
		System.assertEquals('5.2', i.next().text);
		System.assertEquals(')', i.next().text);
	}
	
	@isTest static void testTokenizerFunction4() {
		RT_Expression e = new RT_Expression('3-MAX(3.5,5.2)/9');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('3', i.next().text);
		System.assertEquals('-', i.next().text);
		System.assertEquals('MAX', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('3.5', i.next().text);
		System.assertEquals(',', i.next().text);
		System.assertEquals('5.2', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('9', i.next().text);
	}
	
	@isTest static void testTokenizerFunction5() {
		RT_Expression e = new RT_Expression('3/MAX(-3.5,-5.2)/9');
		Iterator<RT_Expression.Token> i = e.getExpressionTokenizer();
		
		System.assertEquals('3', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('MAX', i.next().text);
		System.assertEquals('(', i.next().text);
		System.assertEquals('-3.5', i.next().text);
		System.assertEquals(',', i.next().text);
		System.assertEquals('-5.2', i.next().text);
		System.assertEquals(')', i.next().text);
		System.assertEquals('/', i.next().text);
		System.assertEquals('9', i.next().text);
	}
	
	/* Var Args
	 */
	
	
	@isTest static void testVarArgsError() {
		String err = '';
		RT_Expression e = new RT_Expression('max()');
		try {
			e.eval();
		}
		catch (RT_Expression.ExpressionException ex) {
			err = ex.getMessage();
		}
		System.assertEquals('Function MAX expected -1 parameters, got 0', err);
	}

	
	/* Variables
	 */
	
	
	
	@isTest static void testInvalidVariableType() {
		Datetime value = Datetime.now();
		try {
			new RT_Expression('x').with('x', value);
			System.assert(false);
		}
		catch (RT_Expression.IllegalArgumentException e) {
			System.assertEquals('Object value \'' + value + '\' is not of a supported type', e.getMessage());
		}
	}
	
	
	
	
	
	@isTest static void testNames() {

		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> FIELD_LIST = new List<String>{nameSpace+'Sequence__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Step__c.SObjectType, FIELD_LIST, false));
            
		System.assertEquals('21', new RT_Expression('3*longname').with('longname', 7).eval() + '');
		System.assertEquals('21', new RT_Expression('3*longname1').with('longname1', 7).eval() + '');
		System.assertEquals('21', new RT_Expression('3*_longname1').with('_longname1', 7).eval() + '');
	}
	
	
	@isTest static void testEqualAndHashCode() {
		RT_Expression e1 = new RT_Expression('((b*(-1)) + SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 4).with('c', 3);
		RT_Expression e2 = new RT_Expression('((b*(-1)) + SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 4).with('c', 3);
		System.assert(e1.equals(e2));
		System.assert(e1.hashCode() == e2.hashCode());
	}
	
	@isTest static void testNotEqualAndHashCode1() {
		RT_Expression e1 = new RT_Expression('((b*(-1)) + SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 4).with('c', 3);
		RT_Expression e2 = new RT_Expression('((b*(-1)) - SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 4).with('c', 3);
		System.assert(!e1.equals(e2));
		System.assert(e1.hashCode() != e2.hashCode());
	}
	
	@isTest static void testNotEqualAndHashCode2() {
		RT_Expression e1 = new RT_Expression('((b*(-1)) + SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 4).with('c', 3);
		RT_Expression e2 = new RT_Expression('((b*(-1)) + SQRT(b^2 - 4*a*c)) / (2*a)').with('a', 1).with('b', 5).with('c', 3);
		System.assert(!e1.equals(e2));
		System.assert(e1.hashCode() != e2.hashCode());
	}
	
	
}