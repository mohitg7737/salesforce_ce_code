@isTest
public class StaticTeaminstanceList_Test {


    static testMethod void testfilldata(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current'; 
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listTeamInstance = StaticTeaminstanceList.filldata();
            System.assertEquals(1,listTeamInstance.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testfilldataCallPlan(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current'; 
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listTeamInstance = StaticTeaminstanceList.filldataCallPlan();
            System.assertEquals(1,listTeamInstance.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetAllCountries(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current'; 
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listTeamInstance = StaticTeaminstanceList.getAllCountries();
            System.assertEquals(1,listTeamInstance.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetCountries(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        v.Add_Alignment_in_TSF__c = true;
        v.market__c ='USA';
        insert v;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
         
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listTeamInstance = StaticTeaminstanceList.getCountries();
            System.assertEquals(1,listTeamInstance.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetDeltaTeamInstances(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Delta';
        countr.Name ='USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        v.Add_Alignment_in_TSF__c = true;
        v.market__c ='USA';
        insert v;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
         
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listTeamInstance = StaticTeaminstanceList.getDeltaTeamInstances();
            System.assertEquals(1,listTeamInstance.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetCountriesSet(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Delta';
        countr.Name ='USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        v.Add_Alignment_in_TSF__c = true;
        v.market__c ='USA';
        insert v;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
         
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            Set<String> setCountries = StaticTeaminstanceList.getCountriesSet();
            List<String> listPlan = StaticTeaminstanceList.getDeltaTeamInstancesCallPlan();
            
            System.assertEquals(1,setCountries.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetFullLoadTeamInstancesCallPlan(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        countr.Name ='USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        v.Add_Alignment_in_TSF__c = true;
        v.market__c ='USA';
        insert v;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
         
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listPlan = StaticTeaminstanceList.getFullLoadTeamInstances();
            List<String> listPlan1 = StaticTeaminstanceList.getFullLoadTeamInstancesCallPlan();
            
            System.assertEquals(1,listPlan.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
    static testMethod void testgetSFEDeltaCountries(){
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.SFE_Load__c = 'Delta Load';
        countr.Name ='USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.hasCallPlan__c =true;
        insert team;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        v.Add_Alignment_in_TSF__c = true;
        v.market__c ='USA';
        insert v;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        update teamins;
         
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        Test.startTest();
            StaticTeaminstanceList objStaticTeamInstanceList = new  StaticTeaminstanceList ();
            List<String> listPlan = StaticTeaminstanceList.getSFEDeltaCountries();
            Set<String> setPlan1 = StaticTeaminstanceList.getCompleteRuleTeamInstances();
            
            System.assertEquals(1,listPlan.size(),'List is Empty.');
        Test.stopTest();
        
    }
    
}