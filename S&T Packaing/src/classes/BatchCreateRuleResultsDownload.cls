global  with sharing class BatchCreateRuleResultsDownload implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts
{   //Removed occurance of Line__c due to object purge activity A1422
    public String query;
    public String ruleId;
    global String results;
    public Measure_Master__c rule;

    public String allSegments;
    
    global BatchCreateRuleResultsDownload(String ruleId) 
    {
        this.ruleId = ruleId;
        rule = [SELECT Id, Name, File_Download_Id__c, State__c,/* Cycle__c, Line_2__c,*/ Brand_Lookup__c, Team_Instance__c,Team_Instance__r.IsHCOSegmentationEnabled__c,Measure_Type__c FROM Measure_Master__c WHERE Id =:ruleId];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        this.query  =  'SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.Name,Physician_2__r.AccountNumber, Physician_2__c, ';
        this.query  += 'Measure_Master__c, Measure_Master__r.Name, OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, ';
        this.query  += 'OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, ';
        this.query  += 'OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, ';
        this.query  += 'OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,Physician__c,Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c,OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c,OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c,';// Shivansh - A1450 -- SNT - 66
        this.query  += 'OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, ';
        this.query  += 'OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c,OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c,OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c, OUTPUT_VALUE_1__c, ';
        this.query  += 'OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, ';
        this.query  += 'OUTPUT_VALUE_8__c,Assigned_Rep__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c,CustonerSegVal__c ';
        this.query  += 'OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c, ';
        this.query  += 'OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, ';
        this.query  += 'OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c,Position__c ';
        this.query  += 'FROM Account_Compute_Final__c WHERE Measure_Master__c = \'' + this.ruleId + '\'';
        
        results = '';
        results = createCSVHeader(this.ruleId);
    }

    global BatchCreateRuleResultsDownload(String ruleId, String allSegments) {
        this.ruleId = ruleId;
        this.allSegments = allSegments;
        rule = [SELECT Id, Name, File_Download_Id__c, State__c, /*Cycle__c, Line_2__c,*/ Brand_Lookup__c, Team_Instance__c,Team_Instance__r.IsHCOSegmentationEnabled__c,Measure_Type__c FROM Measure_Master__c WHERE Id =:ruleId];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        this.query  =  'SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.Name,Physician_2__r.AccountNumber, Physician_2__c, ';
        this.query  += 'Measure_Master__c, Measure_Master__r.Name, OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, ';
        this.query  += 'OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, ';
        this.query  += 'OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, ';
        this.query  += 'OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,Physician__c,Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c,OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c,OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c,';// Shivansh - A1450 -- SNT - 66
        this.query  += 'OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, ';
        this.query  += 'OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c,OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c,OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c, OUTPUT_VALUE_1__c, ';
        this.query  += 'OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, ';
        this.query  += 'OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c, ';
        this.query  += 'OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c,CustonerSegVal__c,';
        this.query  += 'OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, ';
        this.query  += 'OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c,Position__c,Assigned_Rep__c ';
        this.query  += 'FROM Account_Compute_Final__c WHERE Measure_Master__c = \'' + this.ruleId + '\'';
        
        results = '';
        results = createCSVHeader(this.ruleId);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        for(sObject acf : scope){
          results += createCSVRow(acf, rule.Name);
        }
    }

    global void finish(Database.BatchableContext BC) {

        //createComparisonResultFile();

        /*ContentVersion file = new ContentVersion(title = rule.Name + '.csv', versionData = Blob.valueOf( results ), pathOnClient = '/' + rule.Name + '.csv');*/
        ContentVersion file = new ContentVersion(title = rule.Name + '.csv', versionData = Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + rule.Name + '.csv');
        insert file;
        
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: file.Id LIMIT 1];
        ContentDistribution cd = new ContentDistribution();
        cd.Name = rule.Name + '.csv';
        //cd.ContentDocumentId = cv.ContentDocumentId;
        cd.ContentVersionId = cv.Id;
        insert cd;


        
        ContentDistribution cdUrl = [SELECT Id, ContentDownloadUrl, DistributionPublicUrl FROM ContentDistribution WHERE Id=:cd.Id];
        /*list<Measure_Master__c> oldRules;
        if(rule.Team_Instance__r.IsHCOSegmentationEnabled__c == true){
            //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            oldRules = [SELECT Id FROM Measure_Master__c WHERE Team_Instance__c =: rule.Team_Instance__c  AND Brand_Lookup__c =: rule.Brand_Lookup__c AND is_complete__c = true AND State__c = 'Complete' AND Measure_Type__c =: rule.Measure_Type__c];//AND Line_2__c =: rule.Line_2__c
        }
        else{
            //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            oldRules = [SELECT Id FROM Measure_Master__c WHERE Team_Instance__c =: rule.Team_Instance__c  AND Brand_Lookup__c =: rule.Brand_Lookup__c AND is_complete__c = true AND State__c = 'Complete'];//AND Line_2__c =: rule.Line_2__c
        }
        rule.is_executed__c = true;*/
        rule.File_Download_Id__c = cdUrl.ContentDownloadUrl;    
        /*if(oldRules != null && oldRules.size() > 0){
            rule.State__c = 'Publish Not Allowed';
        }else{
            rule.State__c = 'Executed';
        }*/
        rule.All_Segments__c = allSegments;
        //update rule;
        SnTDMLSecurityUtil.updateRecords(rule, 'BatchCreateRuleResultsDownload');

        

        BatchFillUniqueAccountCountMM batch = new BatchFillUniqueAccountCountMM(rule.Team_Instance__c,rule.id,allSegments);
        Database.executeBatch(batch,2000);
  }
    
    private String createCSVRow(sObject acf, String ruleName){
        String headers = '"';
        headers += (String)acf.getsObject('Physician_2__r').get('Name') != null ? (String)acf.getsObject('Physician_2__r').get('Name') : '';
        headers +=  '","' +(String)acf.getsObject('Physician_2__r').get('AccountNumber');
        headers +=  '","' +ruleName;
        headers +=  '","' +(String)acf.get('Position__c')  ;

        // Shivansh - A1450 -- SNT - 66
        if((String)acf.get('Physician__c') == '' || (String)acf.get('Physician__c') == null){
            headers +=  '","' + '';
            headers +=  '","' +'False';
        }
        else{
            headers +=  '","' +((String)acf.getsObject('Physician__r').get('AxtriaSalesIQTM__SharedWith__c') != null ? (String)acf.getsObject('Physician__r').get('AxtriaSalesIQTM__SharedWith__c') :'') ;
            headers +=  '","' +(Boolean)acf.getsObject('Physician__r').get('AxtriaSalesIQTM__IsShared__c');
        }
        
        headers +=  '","' +(String)acf.get('Assigned_Rep__c') ; 
        headers +=  '","' +(String)acf.get('CustonerSegVal__c'); 
            
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_1__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_1__c') != null ? (String)acf.get('OUTPUT_VALUE_1__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_2__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_2__c') != null ? (String)acf.get('OUTPUT_VALUE_2__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_3__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_3__c') != null ? (String)acf.get('OUTPUT_VALUE_3__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_4__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_4__c') != null ? (String)acf.get('OUTPUT_VALUE_4__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_5__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_5__c') != null ? (String)acf.get('OUTPUT_VALUE_5__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_6__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_6__c') != null ? (String)acf.get('OUTPUT_VALUE_6__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_7__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_7__c') != null ? (String)acf.get('OUTPUT_VALUE_7__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_8__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_8__c') != null ? (String)acf.get('OUTPUT_VALUE_8__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_9__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_9__c') != null ? (String)acf.get('OUTPUT_VALUE_9__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_10__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_10__c') != null ? (String)acf.get('OUTPUT_VALUE_10__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_11__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_11__c') != null ? (String)acf.get('OUTPUT_VALUE_11__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_12__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_12__c') != null ? (String)acf.get('OUTPUT_VALUE_12__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_13__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_13__c') != null ? (String)acf.get('OUTPUT_VALUE_13__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_14__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_14__c') != null ? (String)acf.get('OUTPUT_VALUE_14__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_15__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_15__c') != null ? (String)acf.get('OUTPUT_VALUE_15__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_16__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_16__c') != null ? (String)acf.get('OUTPUT_VALUE_16__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_17__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_17__c') != null ? (String)acf.get('OUTPUT_VALUE_17__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_18__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_18__c') != null ? (String)acf.get('OUTPUT_VALUE_18__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_19__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_19__c') != null ? (String)acf.get('OUTPUT_VALUE_19__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_20__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_20__c') != null ? (String)acf.get('OUTPUT_VALUE_20__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_21__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_21__c') != null ? (String)acf.get('OUTPUT_VALUE_21__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_22__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_22__c') != null ? (String)acf.get('OUTPUT_VALUE_22__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_23__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_23__c') != null ? (String)acf.get('OUTPUT_VALUE_23__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_24__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_24__c') != null ? (String)acf.get('OUTPUT_VALUE_24__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_25__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_25__c') != null ? (String)acf.get('OUTPUT_VALUE_25__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_26__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_26__c') != null ? (String)acf.get('OUTPUT_VALUE_26__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_27__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_27__c') != null ? (String)acf.get('OUTPUT_VALUE_27__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_28__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_28__c') != null ? (String)acf.get('OUTPUT_VALUE_28__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_29__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_29__c') != null ? (String)acf.get('OUTPUT_VALUE_29__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_30__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_30__c') != null ? (String)acf.get('OUTPUT_VALUE_30__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_31__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_31__c') != null ? (String)acf.get('OUTPUT_VALUE_31__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_32__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_32__c') != null ? (String)acf.get('OUTPUT_VALUE_32__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_33__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_33__c') != null ? (String)acf.get('OUTPUT_VALUE_33__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_34__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_34__c') != null ? (String)acf.get('OUTPUT_VALUE_34__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_35__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_35__c') != null ? (String)acf.get('OUTPUT_VALUE_35__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_36__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_36__c') != null ? (String)acf.get('OUTPUT_VALUE_36__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_37__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_37__c') != null ? (String)acf.get('OUTPUT_VALUE_37__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_38__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_38__c') != null ? (String)acf.get('OUTPUT_VALUE_38__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_39__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_39__c') != null ? (String)acf.get('OUTPUT_VALUE_39__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_40__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_40__c') != null ? (String)acf.get('OUTPUT_VALUE_40__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_41__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_41__c') != null ? (String)acf.get('OUTPUT_VALUE_41__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_42__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_42__c') != null ? (String)acf.get('OUTPUT_VALUE_42__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_43__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_43__c') != null ? (String)acf.get('OUTPUT_VALUE_43__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_44__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_44__c') != null ? (String)acf.get('OUTPUT_VALUE_44__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_45__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_45__c') != null ? (String)acf.get('OUTPUT_VALUE_45__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_46__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_46__c') != null ? (String)acf.get('OUTPUT_VALUE_46__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_47__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_47__c') != null ? (String)acf.get('OUTPUT_VALUE_47__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_48__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_48__c') != null ? (String)acf.get('OUTPUT_VALUE_48__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_49__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_49__c') != null ? (String)acf.get('OUTPUT_VALUE_49__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_50__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_50__c') != null ? (String)acf.get('OUTPUT_VALUE_50__c') : '');


        headers += '"\n';
    return headers;
    }
    
    private string createCSVHeader(String ruleId){
        Account_Compute_Final__c acf = [SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c,Physician_2__r.AccountNumber, Physician_2__r.Name, Physician_2__c, Measure_Master__c, Measure_Master__r.Name, 
          OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c,
          OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,
          OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c,OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c,OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c,OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c,OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c, OUTPUT_VALUE_1__c,
          OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c,
          OUTPUT_VALUE_12__c,Assigned_Rep__c, OUTPUT_VALUE_13__c,Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c, OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c,
          OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c ,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c,Position__c,CustonerSegVal__c
          FROM Account_Compute_Final__c WHERE Measure_Master__c =: ruleId LIMIT  1];
        String headers = '';
        headers += '"Physician"';
        headers += ',"HCP Number"';
        headers += ',"Rule Name"';
        headers += ',"Position"';
        headers += ',"Shared With"';
        headers += ',"Is Shared"';
        headers += ',"Assigned Rep"';
        headers += ',"Customer Segment"';

        if(String.isNotBlank(acf.OUTPUT_NAME_1__c))
            /*headers += '","' +acf.OUTPUT_NAME_1__c;*/
            headers += ',"' +acf.OUTPUT_NAME_1__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_2__c))
            headers += '","' +acf.OUTPUT_NAME_2__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_3__c))
            headers += '","' +acf.OUTPUT_NAME_3__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_4__c))
            headers += '","' +acf.OUTPUT_NAME_4__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_5__c))
            headers += '","' +acf.OUTPUT_NAME_5__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_6__c))
            headers += '","' +acf.OUTPUT_NAME_6__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_7__c))
            headers += '","' +acf.OUTPUT_NAME_7__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_8__c))
            headers += '","' +acf.OUTPUT_NAME_8__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_9__c))
            headers += '","' +acf.OUTPUT_NAME_9__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_10__c))
            headers += '","' +acf.OUTPUT_NAME_10__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_11__c))
            headers += '","' +acf.OUTPUT_NAME_11__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_12__c))
            headers += '","' +acf.OUTPUT_NAME_12__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_13__c))
            headers += '","' +acf.OUTPUT_NAME_13__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_14__c))
            headers += '","' +acf.OUTPUT_NAME_14__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_15__c))
            headers += '","' +acf.OUTPUT_NAME_15__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_16__c))
            headers += '","' +acf.OUTPUT_NAME_16__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_17__c))
            headers += '","' +acf.OUTPUT_NAME_17__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_18__c))
            headers += '","' +acf.OUTPUT_NAME_18__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_19__c))
            headers += '","' +acf.OUTPUT_NAME_19__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_20__c))
            headers += '","' +acf.OUTPUT_NAME_20__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_21__c))
            headers += '","' +acf.OUTPUT_NAME_21__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_22__c))
            headers += '","' +acf.OUTPUT_NAME_22__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_23__c))
            headers += '","' +acf.OUTPUT_NAME_23__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_24__c))
            headers += '","' +acf.OUTPUT_NAME_24__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_25__c))
            headers += '","' +acf.OUTPUT_NAME_25__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_26__c))
            headers += '","' +acf.OUTPUT_NAME_26__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_27__c))
            headers += '","' +acf.OUTPUT_NAME_27__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_28__c))
            headers += '","' +acf.OUTPUT_NAME_28__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_29__c))
            headers += '","' +acf.OUTPUT_NAME_29__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_30__c))
            headers += '","' +acf.OUTPUT_NAME_30__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_31__c))
            headers += '","' +acf.OUTPUT_NAME_31__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_32__c))
            headers += '","' +acf.OUTPUT_NAME_32__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_33__c))
            headers += '","' +acf.OUTPUT_NAME_33__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_34__c))
            headers += '","' +acf.OUTPUT_NAME_34__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_35__c))
            headers += '","' +acf.OUTPUT_NAME_35__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_36__c))
            headers += '","' +acf.OUTPUT_NAME_36__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_37__c))
            headers += '","' +acf.OUTPUT_NAME_37__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_38__c))
            headers += '","' +acf.OUTPUT_NAME_38__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_39__c))
            headers += '","' +acf.OUTPUT_NAME_39__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_40__c))
            headers += '","' +acf.OUTPUT_NAME_40__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_41__c))
            headers += '","' +acf.OUTPUT_NAME_41__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_42__c))
            headers += '","' +acf.OUTPUT_NAME_42__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_43__c))
            headers += '","' +acf.OUTPUT_NAME_43__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_44__c))
            headers += '","' +acf.OUTPUT_NAME_44__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_45__c))
            headers += '","' +acf.OUTPUT_NAME_45__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_46__c))
            headers += '","' +acf.OUTPUT_NAME_46__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_47__c))
            headers += '","' +acf.OUTPUT_NAME_47__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_48__c))
            headers += '","' +acf.OUTPUT_NAME_48__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_49__c))
            headers += '","' +acf.OUTPUT_NAME_49__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_50__c))
            headers += '","' +acf.OUTPUT_NAME_50__c;

        headers += '"\n';
        
        return headers;
    }

    public void createComparisonResultFile(){
        SnTDMLSecurityUtil.printDebugMessage('Inside Callout ---> ');
        AxtriaSalesIQTM__ETL_Config__c etlConfig = [Select Id,AxtriaSalesIQTM__End_Point__c,AxtriaSalesIQTM__SF_UserName__c,
                                                        AxtriaSalesIQTM__SF_Password__c,AxtriaSalesIQTM__S3_Security_Token__c,
                                                        AxtriaSalesIQTM__S3_Bucket__c,AxtriaSalesIQTM__S3_Filename__c 
                                                        from AxtriaSalesIQTM__ETL_Config__c where Name = 'ComparativeAnalysis'];
        if(etlConfig != null)
        {
            ApexClass cs =[select NamespacePrefix from ApexClass where Name = 'BatchCreateRuleResultsDownload'];
            String nameSpacePrefix= cs.NamespacePrefix == null ? '' : cs.NamespacePrefix+'__';

            String objectName = nameSpacePrefix+'Account_Compute_Final__c';
            /*SObjectType objType = Schema.getGlobalDescribe().get(objectName);
            Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();*/
            String queryCols = '';
            /*for(String s : mfields.keySet()){
                queryCols += mfields.get(s) + ', ';
            }
            queryCols = queryCols.removeEnd(', ');*/
            queryCols = 'Id, '+nameSpacePrefix+'Account_Number__c, '+nameSpacePrefix+'Final_Segment__c, '+nameSpacePrefix+'Final_TCF__c ';



            String queryFinal = 'SELECT ' + queryCols + ' FROM ' + objectName + ' WHERE ' +nameSpacePrefix+'Measure_Master__c = \''+ruleId+'\' AND isDeleted = false';
            String fileName = etlConfig.AxtriaSalesIQTM__S3_Filename__c;
            fileName = fileName.replace('measureId',ruleId);
            String endPointURL = etlConfig.AxtriaSalesIQTM__End_Point__c + '?fileNameSource='+fileName+'&fileNameDestination='+fileName;
            endPointURL += '&sfdcUserName='+etlConfig.AxtriaSalesIQTM__SF_UserName__c+'&sfdcPassword='+etlConfig.AxtriaSalesIQTM__SF_Password__c;
            endPointURL += '&sfdcSecurityToken='+etlConfig.AxtriaSalesIQTM__S3_Security_Token__c+'&sourceRuleId='+ruleId+'&destinationRuleId=null'+'&query='+queryFinal;
            endPointURL += '&namespace='+nameSpacePrefix+'&objectName='+objectName+'&action=Fetch'+'&segUniv=Survey Data Accounts';
            SnTDMLSecurityUtil.printDebugMessage('endPointURL1 ==== ' + endPointURL);
            endPointURL = endPointURL.replaceAll( '\\s+', '%20');
            endPointURL = endPointURL.replaceAll('\'', '%27');
            SnTDMLSecurityUtil.printDebugMessage('endPointURL 222==== ' + endPointURL);
                Http h = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndPoint(endPointURL);
                request.setHeader('Content-type','application/json');
                request.setMethod('GET');
                request.setTimeout(120000);
                HttpResponse response = h.send(request);
                SnTDMLSecurityUtil.printDebugMessage('response body ---> '+ response.getBody());
                /*if(response.getStatusCode() == 200){
                    Measure_Master__c mmTemp = [Select State__c from Measure_Master__c where Id=:ruleId];
                    if(mmTemp.State__c!='Failed')
                    {
                        BatchExecuteRuleEngine batchExecute = new BatchExecuteRuleEngine(ruleId, WhereClause);
                        Database.executeBatch(batchExecute,500);
                    }
                }*/
            
        }

    }
}