global with sharing class BatchPositionAccountCount1 implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    
    map<String,Integer> Active_Map = New map<String,Integer>();
    Public String country;
    String query;

    global BatchPositionAccountCount1(){

    }
    
    global BatchPositionAccountCount1(String country){
        this.country = country;
        query = 'SELECT Id,One_day_Previous_Count__c,Current_Count__c, Name, PA_Active__c, PA_Inactive__c FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: country';
    }
      
    global Database.QueryLocator start(Database.BatchableContext BC) {       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position__c> posList){  
        SnTDMLSecurityUtil.printDebugMessage('position size is '+ posList.size());
        Set<String> posIdSet = new Set<String>();
        
        for(AxtriaSalesIQTM__Position__c position: posList){
            position.One_day_Previous_Count__c =  position.Current_Count__c; 
            if(position.One_day_Previous_Count__c == null){
               position.One_day_Previous_Count__c = 1;
            }
            posIdSet.add(position.Id);
        }
        
        List<AggregateResult> posIdCounts = [SELECT AxtriaSalesIQTM__Position__c, count(Id) con FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Assignment_Status__c = 'Active' group by AxtriaSalesIQTM__Position__c];
        SnTDMLSecurityUtil.printDebugMessage('posIdCounts is ' + posIdCounts.size());
        for(AggregateResult posIdcount : posIdCounts){
            Active_Map.put((String)posIdcount.get('AxtriaSalesIQTM__Position__c'), (Integer)posIdcount.get('con'));
        } 
        SnTDMLSecurityUtil.printDebugMessage('Active_Map size is ' + Active_Map.size());
        for(AxtriaSalesIQTM__Position__c POS : posList){
           POS.Current_Count__c = Active_Map.get(POS.ID); 
           POS.PA_Active__c = 0;
           POS.PA_Inactive__c = 0;
        }
        SnTDMLSecurityUtil.printDebugMessage('PosList size is '+PosList.size());
        //update posList;  
        SnTDMLSecurityUtil.updateRecords(posList, 'BatchPositionAccountCount1');
    }
       
    global void execute(SchedulableContext sc){
        /*List<Veeva_Job_Scheduling__c> fetchAllCountries = [Select Country__r.Name from Veeva_Job_Scheduling__c where Load_Type__c != 'No Load'];
            SnTDMLSecurityUtil.printDebugMessage(fetchAllCountries + 'Countr ');
            
        for(Veeva_Job_Scheduling__c Coun : fetchAllCountries){
            SnTDMLSecurityUtil.printDebugMessage(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchPositionAccountCount1(Coun.Country__r.Name),1);     
        } */
        /*Replaced Veeva_Job_Scheduling__c with field at country*/
        List<AxtriaSalesIQTM__Country__c> fetchAllCountries = [Select Name from AxtriaSalesIQTM__Country__c where Load_Type__c != 'No Load'];
            SnTDMLSecurityUtil.printDebugMessage(fetchAllCountries + 'Countr ');
            
        for(AxtriaSalesIQTM__Country__c Coun : fetchAllCountries){
            SnTDMLSecurityUtil.printDebugMessage(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchPositionAccountCount1(Coun.Name),1);     
        }
    }   

    global void finish(Database.BatchableContext BC) { 
        Database.executebatch(new BatchPositionAccountAddDropCount(country),10);     
    }
    
   }