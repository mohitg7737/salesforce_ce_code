global class changeMCProductMultiChannel implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<String> allTeamInstances;
    public Boolean flag = true;
    public Date lmd;
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCProductMultiChannel(string teamInstanceSelectedTemp, List<String> allChannelsTemp,boolean check)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         flag=check;
    }


    global changeMCProductMultiChannel(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,boolean check)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         flag=check;
    }
    global changeMCProductMultiChannel(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    {
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         //flag=check;
    }
     global changeMCProductMultiChannel(Boolean check)
    { 
        allTeamInstances = new List<String>();
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c'; //where Team_Instance__c in :allTeamInstances';
        flag=check;

        //teamInstanceSelected = teamInstanceSelectedTemp;
        //allChannels = allChannelsTemp;
    }

    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Product_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Product_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        if(flag)
        {
         AZ_IntegrationUtility_EU_MultiChannel u1 = new AZ_IntegrationUtility_EU_MultiChannel(allTeamInstances, allChannels);
          Database.executeBatch(u1, 2000);
        }
       /* else
        {
             lmd=Date.Today();
            BatchDeltaMCProduct u2 = new BatchDeltaMCProduct(lmd);
            database.executeBatch(u2,2000);

        }*/
        
       
        //Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}