public with sharing class CSVFileDataIterator implements Database.Batchable<Attachment>,Database.Stateful
{
    public List<String> attachmentFileIdList;
    public List<temp_Obj__c> recordsToinsert;
    public List<HeaderFieldMappingWrapper> fieldlist;
        
    public Integer recsProcessed;

    public String objectName;
    public String selectedJuncObj;
    public String lineRow;
    public String teamId;
    public String teamInstanceId;
    public String loadTypeName;
    public String selectedTeam;
    public String selectedTeamIns;
    public String selectedLoadType;
    public String errorMessage;
    public String changeReqID;
    public String selectedWSID; //Added by HT(A0994) on 15th August for SMCCP-15
    public String selectedEventType; //Added by HT(A0994) on 15th August for SMCCP-15
    public String sntNmsp; //Added by HT(A0994) on 8th July 2020 for SNT-514
    public String wsCountryID; //Added by HT(A0994) on 18th August for SMCCP-15

    public Boolean proceedBatch = true;
    
    /*public CSVFileDataIterator(List<String> attachmentIdList,String crID,String objName, String selJuncObj, 
                                List <HeaderFieldMappingWrapper> fieldlst,String team,
                                String teamInstance,String loadType)
    {
        objectName = objName;
        attachmentFileIdList = attachmentIdList;
        changeReqID = crID;
        selectedJuncObj = selJuncObj;
        fieldlist = fieldlst;
        teamId = team;
        teamInstanceId = teamInstance;
        loadTypeName = loadType;
        selectedTeam = team;
        selectedTeamIns = teamInstance;
        selectedLoadType = loadType;
        selectedWSID = workspaceID;
        selectedEventType = eventType;
        wsCountryID = countryID;
        recsProcessed = 0;

        System.debug('fieldlist--'+fieldlist);

        //Added by HT(A0994) on 8th July 2020 for SNT-514 : Start
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchExecuteRuleEngine'];
        sntNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix : '';
        //Added by HT(A0994) on 8th July 2020 for SNT-514 : End
    }*/
    
    public CSVFileDataIterator(List<String> attachmentIdList,String crID,String objName, String selJuncObj, 
                                List <HeaderFieldMappingWrapper> fieldlst,String team,
                                String teamInstance,String loadType,String workspaceID,String eventType,String countryID)
    {
        objectName = objName;
        attachmentFileIdList = attachmentIdList;
        changeReqID = crID;
        selectedJuncObj = selJuncObj;
        fieldlist = fieldlst;
        teamId = team;
        teamInstanceId = teamInstance;
        loadTypeName = loadType;
        selectedTeam = team;
        selectedTeamIns = teamInstance;
        selectedLoadType = loadType;
        selectedWSID = workspaceID;
        selectedEventType = eventType;
        wsCountryID = countryID;
        recsProcessed = 0;

        System.debug('fieldlist--'+fieldlist);

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchExecuteRuleEngine'];
        sntNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix+'__' : '';
    }
    
    public list<Attachment> start(Database.BatchableContext BC)
    {
        list<Attachment> att = [Select Id from Attachment where Id in :attachmentFileIdList];
        return att;
    }
    
    public void execute(Database.BatchableContext BC){}
     
    
    public void execute(Database.BatchableContext BC, List<Attachment> attachment)
    {
        String attachId;
        try
        {
            if(attachment!=null && attachment.size()>0)
            {
                attachId = attachment[0].Id;
                list<Attachment> attBodyList = [select Id,Body from Attachment where Id =:attachId];
                temp_Obj__c tempRec;
                recordsToinsert = new list<temp_Obj__c>();
                Map<String,Object> rec = new Map <String,Object> ();

                CustomIterator lineReader = new CustomIterator(attBodyList[0].body.toString());

                while (lineReader.hasNext())
                {
                    lineRow = lineReader.next();
                    rec = new Map<String,Object>();
                    Integer j = 0;

                    /*Added by HT(A0994) on 1st July 2020 for SNT-485*/
                    for(string inputValue : lineRow.split(',(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)'))
                    //for(string inputValue : lineRow.split(','))
                    {
                        if (fieldlist[j].selField != '---select---' && fieldlist[j].selField != null)
                        {
                            if (inputValue != null){
                                inputValue = inputValue.normalizespace();
                            }

                            if (inputValue != null )
                            {
                                if(inputValue.equalsIgnorecase('TRUE') || inputValue.equalsIgnorecase('FALSE'))
                                {
                                    SnTDMLSecurityUtil.printDebugMessage(' input value for visible is  ::' + inputValue);
                                    inputValue = inputValue.tolowercase();
                                }
                            }
                            if (string.IsBlank(inputValue)){
                                inputValue = null;
                            }
                            //Added by HT(A0994) on 1st July 2020 for SNT-485
                            else{
                                inputValue = inputValue.replaceAll('"','');
                            }
                            rec.put(fieldlist[j].selField, inputValue);
                        }
                        j++;
                    }

                    tempRec = (temp_Obj__c)JSON.deserializestrict(JSON.serialize(rec),Type.forname('temp_Obj__c'));
                    SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);
                    tempRec.Change_Request__c = changeReqID;                    
                    tempRec.Status__c = 'New';
                    tempRec.Event__c = 'Insert';
                    tempRec.Object__c = objectName;
                    tempRec.Object_Name__c = objectName;

                    if(changeReqID!=null && changeReqID!='')
                        recordsToinsert.add(tempRec);
                }
                
                System.debug('recordsToinsert--'+recordsToinsert.size());

                if(!recordsToinsert.isEmpty()){
                    SnTDMLSecurityUtil.insertRecords(recordsToinsert, 'CSVFileDataIterator');
                    recsProcessed+=recordsToinsert.size();
                }
                
                if(!attBodyList.isEmpty()){
                    SnTDMLSecurityUtil.deleteRecords(attBodyList,'CSVFileDataIterator');
                }
            }
        }
        catch(Exception e)
        {
            proceedBatch = false;
            errorMessage = e.getMessage();
            SnTDMLSecurityUtil.printDebugMessage('Error in execute() method--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        try
        {
            System.debug('--------------PopulateTempObject : finish() start----------- ');
            /*Commented by HT(A0994) on 18th June 2020 in case number of attachments are >10k
            so makes sense to delete each attachment in the execute() method*/
            /*List<Attachment> listAttach = [select id from Attachment where id in :attachmentFileIdList 
                                            WITH SECURITY_ENFORCED];

            System.debug('listAttach size--'+listAttach.size());
            if(!listAttach.isEmpty()){
                SnTDMLSecurityUtil.deleteRecords(listAttach,'CSVFileDataIterator');
            }*/

            if(proceedBatch && changeReqID!=null && changeReqID!='')
            //if(proceedBatch)
            {
                System.debug('--- inside finish ----');
                System.debug('selectedJuncObj -'+selectedJuncObj);
                Integer batchsize = 500;
                Id batchObj;

                //Tested
                if (selectedJuncObj == 'Populate Employee'){
                    //batchObj = Database.executeBatch(new BatchPopulateEmployee(changeReqID), batchsize);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new BatchPopulateEmployee(changeReqID,true), batchsize);
                }
                else if (selectedJuncObj == 'Zip_Terr'){
                    //batchObj = Database.executeBatch(new UpdateZipTerr(selectedTeam,selectedTeamIns,changeReqID), batchsize);
                    if(!System.Test.isRunningTest()) 
                        batchObj = Database.executeBatch(new UpdateZipTerr(selectedTeam,selectedTeamIns,changeReqID,true), batchsize); //batch to update geography and posiition lookups
                }
                else if (selectedJuncObj == 'Acc_Terr'){
                    //batchObj = Database.executeBatch(new UpdateAccTerr(selectedTeam,selectedTeamIns,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())   
                        batchObj = Database.executeBatch(new UpdateAccTerr(selectedTeam,selectedTeamIns,changeReqID,true), batchsize); //batch to update position and account lookups 
                }
                else if (selectedJuncObj == 'Position_Employee')
                {
                    //CreatePositionDirectLoad newobj =new  CreatePositionDirectLoad(selectedTeam,selectedTeamIns);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new PositionEmployeeDirectLoad(selectedTeam, selectedTeamIns,changeReqID), batchsize); //batch to insert position_employee
                }
                else if(selectedJuncObj == 'Call_Plan')
                {
                    String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
                    //batchObj = Database.executeBatch(new PACP_ErrorFilter_Utility(TIname,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())    
                        batchObj = Database.executeBatch(new PACP_ErrorFilter_Utility(TIname,changeReqID,true), batchsize); //batch to insert Call plan Data
                }
			    else if(selectedJuncObj == 'Accessibility')// Added by chandan SR-4
                {
                    String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
                    //batchObj = Database.executeBatch(new PACP_ErrorFilter_Utility(TIname,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())    
                        batchObj = Database.executeBatch(new Batch_SIQ_Veeva_TSF(TIname,changeReqID,true), batchsize); //batch to insert Accessibility Call plan Data
                }
                else if(selectedJuncObj == 'Product and portfolio')// Added by chandan SR-6
                {
                    String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
                    //batchObj = Database.executeBatch(new PACP_ErrorFilter_Utility(TIname,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new Batch_product_portfolio(TIname,changeReqID), batchsize); //batch to insert Product portfolio Data
                }
                
                else if(selectedJuncObj == 'Survey')
                {
                    String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = : selectedTeamIns].name;
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new BatchPopulateSurveyDirectLoad(TIname,changeReqID,selectedLoadType), batchsize);
                }
                else if (selectedJuncObj == 'Position Product')
                {
                    //batchObj = Database.executeBatch(new PositionProductDirectLoad(selectedTeamIns,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new PositionProductDirectLoad(selectedTeamIns,changeReqID,true), batchsize); //batch to insert position product Data
                }
                else if (selectedJuncObj == 'Assign Product Type')
                {
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new AssignProductType(selectedTeamIns), batchsize); //, crID //batch to insert position product Data
                }
                else if (selectedJuncObj == 'Populate HCP Metrics')
                {
                    //batchObj = Database.executeBatch(new BatchPopulatePAMetrics(selectedTeamIns,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new BatchPopulatePAMetrics(selectedTeamIns,changeReqID,true), batchsize); //batch to insert HCP Metrics
                }
                /* Added by Shivansh(A1450) for SNT-110 (To provide direct load utility for Updating Zip Terr Metrics)*/
                else if (selectedJuncObj == 'Populate Zip-Terr Metrics')
                {
                    //batchObj = Database.executeBatch(new BatchPopulatePosGeoMetrics(selectedTeamIns,changeReqID),batchsize);
                    if(!System.Test.isRunningTest())
                        batchObj = Database.executeBatch(new BatchPopulatePosGeoMetrics(selectedTeamIns,changeReqID,true),batchsize); //batch to insert HCP Metrics   
                }
                //Batch by ayush for SNT-110 (To provide direct load utility for Loading CIM Config) - Tested
                else if (selectedJuncObj == 'Populate CIM Configuration')
                {
                    //batchObj = Database.executeBatch(new BatchPopulateCimConfig(selectedTeamIns,changeReqID), batchsize);
                    if(!System.Test.isRunningTest())    
                        batchObj = Database.executeBatch(new BatchPopulateCimConfig(selectedTeamIns,changeReqID,false), batchsize);  
                }
                else if(selectedJuncObj == 'Channel Preference')
                {
                    //Database.executeBatch(new BatchPopulateChannelPreferenceData(changeReqID), batchsize);
                    if(!System.Test.isRunningTest())   
                        Database.executeBatch(new BatchPopulateChannelPreferenceData(changeReqID,true), batchsize);
                }
                //Added by HT(A0994) on 15th August 2020 for SMCCP-15
                else if(selectedJuncObj == 'MCCP Channel Preference')
                {
                	if(selectedEventType=='Upsert'){
                        if(!System.Test.isRunningTest())
                            Database.executeBatch(new BatchPopulateChannelPreference(changeReqID,wsCountryID),2000);
                	}
                	else{
                        if(!System.Test.isRunningTest())
                            Database.executeBatch(new BatchDeleteMCCPLoadData(changeReqID,'Channel Preference',selectedWSID,wsCountryID,true),2000);
                	}
                }
                //Added by HT(A0994) on 15th August 2020 for SMCCP-15
                else if(selectedJuncObj == 'MCCP Channel Consent')
                {
                	if(selectedEventType=='Upsert'){
                        if(!System.Test.isRunningTest())
                		    Database.executeBatch(new BatchPopulateChannelConsent(changeReqID,wsCountryID),2000);
                	}
                	else{
                        if(!System.Test.isRunningTest())
                            Database.executeBatch(new BatchDeleteMCCPLoadData(changeReqID,'Channel Consent',selectedWSID,wsCountryID,true),2000);
                	}
                }
                //Added by HT(A0994) on 15th August 2020 for SMCCP-15
                else if(selectedJuncObj == 'MCCP Product Segment')
                {
                	if(selectedEventType=='Upsert'){
                        if(!System.Test.isRunningTest())
                            Database.executeBatch(new BatchPopulateProductSegment(changeReqID,wsCountryID,selectedWSID),2000);
                	}
                	else{
                        if(!System.Test.isRunningTest())
                            Database.executeBatch(new BatchDeleteMCCPLoadData(changeReqID,'Product',selectedWSID,wsCountryID,true),2000);
                	}
                }
                //Added by HT(A0994) on 6th July 2020 for SNT-514 - Start
                else if(selectedJuncObj!=null && selectedJuncObj != '')
                {
                    SnTDMLSecurityUtil.printDebugMessage('selectedJuncObj--'+selectedJuncObj);

                    Boolean errorOccured = true;
                    String objectLabel;
                    String methodName;
                    String interfaceName;

                    SalesIQ_Direct_Load__mdt batchRec;

                    String metadataQuery = 'select MasterLabel,Interface_Class_Name__c,Method_Name__c from '+
                                            'SalesIQ_Direct_Load__mdt where Interface_Class_Name__c!=null and '+
                                            'DeveloperName =: selectedJuncObj and Method_Name__c!=null WITH SECURITY_ENFORCED';
                    List<SalesIQ_Direct_Load__mdt> batchNameList = Database.query(metadataQuery);

                    if(batchNameList!=null && batchNameList.size()>0)
                    {
                        batchRec = batchNameList[0];

                        methodName = batchRec.Method_Name__c;
                        objectLabel = batchRec.MasterLabel;
                        interfaceName = batchRec.Interface_Class_Name__c;

                        SnTDMLSecurityUtil.printDebugMessage('interfaceName--'+interfaceName);
                        SnTDMLSecurityUtil.printDebugMessage('methodName--'+methodName);
                        SnTDMLSecurityUtil.printDebugMessage('objectLabel--'+objectLabel);

                        try
                        {
                            //Call Batch dynamically
                            Callable extension = (Callable)Type.forName('',interfaceName).newInstance();
                            Map<String,Object> myObj = new Map<String,Object>();
                            myObj.put('crID',changeReqID);
                            myObj.put('selectedLoadType',selectedLoadType);
                            myObj.put('selectedTeamInst',selectedTeamIns);
                            myObj.put('selectedTeam',selectedTeam);
                            myObj.put('metadataId',batchRec.Id);
                            if(!System.Test.isRunningTest())
                                Extension.call(methodName, myObj);
                        }
                        catch(Exception e)
                        {
                            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(changeReqID,false,e.getMessage(),'Error');
                            Database.executeBatch(batchCall,2000);
                        }
                    }
                }
                //Added by HT(A0994) on 6th July 2020 for SNT-514 - End
            }
            else
            {
                if(changeReqID!=null && changeReqID!='')
                {
                    //Update Status of Temp object recs
                    BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(changeReqID,false,errorMessage,'Error');
                    Database.executeBatch(batchCall,2000);
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in finish() method--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }
    }
}