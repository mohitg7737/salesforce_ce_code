public class PrimaryAffiliationIndicatorHandler {
    
    public static Boolean executeTrigger = true;
    
    public static void insertTrigger(List<AxtriaSalesIQTM__Account_Affiliation__c> insertRec){
      System.debug('====Insert :::::');
      set<string>uniqset = new set<string>();  
      Map<String,String> accountIDparentID = new Map<String, String>();
      List<Account> aList = new List<Account>();
      for(AxtriaSalesIQTM__Account_Affiliation__c accaff : insertRec)
      {
        if(accaff.Primary_Affiliation_Indicator__c == 'Y' && accaff.Affiliation_Status__c  == 'Active')
        {
          accountIDparentID.put(accaff.AxtriaSalesIQTM__Account__c, accaff.AxtriaSalesIQTM__Parent_Account__c);
          System.debug('====accountIDparentID:::::' +accountIDparentID);
      } }
      List<Account> acc = [Select Id, ParentId from Account where Id in :accountIDparentID.keySet()];
      for(Account a : acc)
      {
        if(!uniqset.contains(a.id)){
          a.ParentId = accountIDparentID.get(a.id);
          aList.add(a);
          uniqset.add(a.id);
        }
      }
      System.debug('====aList:::::' +aList);    
      update aList;
      //}

      
    }
    
    public static void updateTrigger(List<AxtriaSalesIQTM__Account_Affiliation__c> updateRec){
        
        System.debug('====Upadte :::::');
        Map<String,String> updatedaccparent = new Map<String, String>();
        List<Account> updatedaList = new List<Account>();
        set<string>uniqset = new set<string>();

        for ( AxtriaSalesIQTM__Account_Affiliation__c updateaccaff : updateRec )
        {
           if(updateaccaff.Primary_Affiliation_Indicator__c == 'Y' && updateaccaff.Affiliation_Status__c  =='Active')
           {
            updatedaccparent.put(updateaccaff.AxtriaSalesIQTM__Account__c, updateaccaff.AxtriaSalesIQTM__Parent_Account__c); 
           }
           else
           {
             if(!updatedaccparent.containsKey(updateaccaff.AxtriaSalesIQTM__Account__c))
             {
               updatedaccparent.put(updateaccaff.AxtriaSalesIQTM__Account__c,'No Parent');
             }
           }
        }
        System.debug('====updatedaccparent:::::' +updatedaccparent);
        List<Account> updatedacc = [Select Id, ParentId from Account where Id in :updatedaccparent.keySet()];
        System.debug('====updatedacc:::::' +updatedacc);
        for(Account updatea : updatedacc)
        {

          if(updatedaccparent.get(updatea.id) == 'No Parent')
          {
            updatea.ParentId = null;
          }  
          else
          {
            updatea.ParentId = updatedaccparent.get(updatea.id);
          }
          if(!uniqset.contains(updatea.id))
          {
            updatedaList.add(updatea);
            uniqset.add(updatea.id);
          }
        }    
        System.debug('====updatedaList:::::' +updatedaList);
        update updatedaList;
       //}
    }
}