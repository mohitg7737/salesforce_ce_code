global class AZ_Integration_MC_Target implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    /*List<MC_Cycle_Plan_Target_vod__c> mcPlanTarget;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpRecs;*/
    
    global AZ_Integration_MC_Target(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

        /*queryString = 'select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,P2_Picklist1_Updated__c,P2_Picklist2_Updated__c,P2_Picklist3_Updated__c,P2_Picklist4_Updated__c,P2_Picklist5_Updated__c,P3_Picklist1_Updated__c,P3_Picklist2_Updated__c,P3_Picklist3_Updated__c,P3_Picklist4_Updated__c,P3_Picklist5_Updated__c,P4_Picklist1_Updated__c,P4_Picklist2_Updated__c,P4_Picklist3_Updated__c,P4_Picklist4_Updated__c,P4_Picklist5_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,  AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist3_Updated__c, AxtriaSalesIQTM__Picklist4_Metric_Updated__c, AxtriaSalesIQTM__Picklist5_Metric_Updated__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and  AxtriaSalesIQTM__isIncludedCallPlan__c = true';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;*/
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    /*public void create_MC_Cycle_Plan_Target_vod(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        pacpRecs = scopePacpProRecs;
        
         List<AxtriaSalesIQTM__User_Access_Permission__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__User__r.FederationIdentifier from AxtriaSalesIQTM__User_Access_Permission__c];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
        
        for(AxtriaSalesIQTM__User_Access_Permission__c u: uap)
        {
            string teamInstancePos = u.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,'Jordon-EXT');
        }
        
        mcPlanTarget = new List<MC_Cycle_Plan_Target_vod__c>();
        
        
        Map<String, Map<String,Integer>> targetCallsMap = new Map<String,  Map<String,Integer>>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRecs)
        {
            string accNumPos = pacp.AxtriaSalesIQTM__Account__r.AccountNumber + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            
            if(targetCallsMap.containsKey(accNumPos))
            {
                integer f2fCalls = targetCallsMap.get(accNumPos).get('f2f');
                
                integer otherCalls = targetCallsMap.get(accNumPos).get('otherCalls');
                
                f2fCalls = Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist1_Updated__c) + Integer.valueOf(pacp.P2_Picklist1_Updated__c)+ Integer.valueOf(pacp.P3_Picklist1_Updated__c)+ Integer.valueOf(pacp.P4_Picklist1_Updated__c);

                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist2_Updated__c) + Integer.valueOf(pacp.P2_Picklist2_Updated__c)+ Integer.valueOf(pacp.P3_Picklist2_Updated__c)+ Integer.valueOf(pacp.P4_Picklist2_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist3_Updated__c) + Integer.valueOf(pacp.P2_Picklist3_Updated__c)+ Integer.valueOf(pacp.P3_Picklist3_Updated__c)+ Integer.valueOf(pacp.P4_Picklist3_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist4_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist4_Updated__c)+ Integer.valueOf(pacp.P3_Picklist4_Updated__c)+ Integer.valueOf(pacp.P4_Picklist4_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist5_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist5_Updated__c)+ Integer.valueOf(pacp.P3_Picklist5_Updated__c)+ Integer.valueOf(pacp.P4_Picklist5_Updated__c);

                
                targetCallsMap.get(accNumPos).put('f2f',f2fCalls);
                targetCallsMap.get(accNumPos).put('otherCalls', otherCalls);
            }
            else
            {
                Map<String, Integer> insideMap = new Map<String,Integer>();
                
                Integer f2fCalls = 0;
                Integer otherCalls = 0;
               
                f2fCalls = Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist1_Updated__c) + Integer.valueOf(pacp.P2_Picklist1_Updated__c)+ Integer.valueOf(pacp.P3_Picklist1_Updated__c)+ Integer.valueOf(pacp.P4_Picklist1_Updated__c);

                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist2_Updated__c) + Integer.valueOf(pacp.P2_Picklist2_Updated__c)+ Integer.valueOf(pacp.P3_Picklist2_Updated__c)+ Integer.valueOf(pacp.P4_Picklist2_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist3_Updated__c) + Integer.valueOf(pacp.P2_Picklist3_Updated__c)+ Integer.valueOf(pacp.P3_Picklist3_Updated__c)+ Integer.valueOf(pacp.P4_Picklist3_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist4_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist4_Updated__c)+ Integer.valueOf(pacp.P3_Picklist4_Updated__c)+ Integer.valueOf(pacp.P4_Picklist4_Updated__c);
                otherCalls = otherCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist5_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist5_Updated__c)+ Integer.valueOf(pacp.P3_Picklist5_Updated__c)+ Integer.valueOf(pacp.P4_Picklist5_Updated__c);


                targetCallsMap.put(accNumPos,insideMap);
                targetCallsMap.get(accNumPos).put('f2f',f2fCalls);
                targetCallsMap.get(accNumPos).put('otherCalls', otherCalls);
                
                insideMap.put('f2f', f2fCalls);
                insideMap.put('otherCalls', otherCalls);
                
                targetCallsMap.put(accNumPos, insideMap);
            }
        }
        
        Set<String> allString = new Set<String>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRecs)
        {

                Integer f2fCalls = 0;
                Integer phoneCalls = 0;
                Integer emailCalls = 0;
                Integer remoteCalls = 0;
                Integer webinarCalls = 0;
                Integer sumF2fCalls = 0;
                
                f2fCalls = Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist1_Updated__c) + Integer.valueOf(pacp.P2_Picklist1_Updated__c)+ Integer.valueOf(pacp.P3_Picklist1_Updated__c)+ Integer.valueOf(pacp.P4_Picklist1_Updated__c);

                phoneCalls = phoneCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist2_Updated__c) + Integer.valueOf(pacp.P2_Picklist2_Updated__c)+ Integer.valueOf(pacp.P3_Picklist2_Updated__c)+ Integer.valueOf(pacp.P4_Picklist2_Updated__c);
                emailCalls = emailCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist3_Updated__c) + Integer.valueOf(pacp.P2_Picklist3_Updated__c)+ Integer.valueOf(pacp.P3_Picklist3_Updated__c)+ Integer.valueOf(pacp.P4_Picklist3_Updated__c);
                remoteCalls = remoteCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist4_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist4_Updated__c)+ Integer.valueOf(pacp.P3_Picklist4_Updated__c)+ Integer.valueOf(pacp.P4_Picklist4_Updated__c);
                webinarCalls = webinarCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist5_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist5_Updated__c)+ Integer.valueOf(pacp.P3_Picklist5_Updated__c)+ Integer.valueOf(pacp.P4_Picklist5_Updated__c);


                sumF2fCalls = f2fCalls;

                String accNumPos = pacp.AxtriaSalesIQTM__Account__r.AccountNumber + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

                if(allString.contains(accNumPos))
                {

                }
                else
                {
                    MC_Cycle_Plan_Target_vod__c mcp = new MC_Cycle_Plan_Target_vod__c();
                
                

                    String teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    String userID = 'Jordon-EXT'; //posTeamToUser.get(teamPos);
                    

                    mcp.Cycle_Plan_vod__c = teamPos +'_'+userID+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c ; //:- Get from Cycle Plan vod External ID 
                    
                    

                    mcp.AZ_External_Id__c  = teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    mcp.Target_vod__c  = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    
                    mcp.Status_vod__c = 'Active_vod';
                    mcp.Channel_Interactions_Goal_vod__c = targetCallsMap.get(accNumPos).get('f2f') + targetCallsMap.get(accNumPos).get('otherCalls');
                    
                    mcp.Product_Interactions_Goal_vod__c    =sumF2fCalls + phoneCalls + emailCalls + remoteCalls + webinarCalls;
                    mcp.Channel_Interactions_Max_vod__c =  mcp.Channel_Interactions_Goal_vod__c;
                    mcp.External_Id_vod__c  =   mcp.AZ_External_Id__c;
                    
                    mcPlanTarget.add(mcp); 

                    allString.add(accNumPos); 
                }
                
                                    
        }
        
        system.debug('++++++++++++++ MC PLan Target');
        system.debug(mcPlanTarget);
        insert mcPlanTarget;
        
        //create_MC_Cycle_Plan_Product_vod();
    }*/
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        /*create_MC_Cycle_Plan_Target_vod(scopePacpProRecs);*/
        
        /*List<MC_Cycle_Plan_Target_vod__c> allMCTarget = new List<MC_Cycle_Plan_Target_vod__c>();
        allMCTarget.addAll(mcPlanTarget);
*/
        //insert mCyclePlanChannel;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}