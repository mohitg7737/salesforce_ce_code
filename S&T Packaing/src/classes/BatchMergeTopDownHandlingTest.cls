/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test BatchDeassignPositionAccounts.
*/
@isTest
public class BatchMergeTopDownHandlingTest {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        string classname = 'BatchMergeTopDownHandlingTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        acc.Merge_Account_Number__c ='BH10461999_US';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        acc1.Type = 'HCA';
        SnTDMLSecurityUtil.insertRecords(acc1,className);
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10462969';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
		acc11.Merge_Account_Number__c ='BH10462969_US';
        acc11.Type = 'HCA';
        SnTDMLSecurityUtil.insertRecords(acc11,className);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        SnTDMLSecurityUtil.insertRecords(affnet,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc1.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;//SHOULD BE HCA
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;//HCP
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc1,affnet);
        accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;//HCA
        accaff1.AxtriaSalesIQTM__Account__c = acc1.Id;//HCA
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff1,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff11.Account_Number__c = 'BH10462969';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff11,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
         AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen2.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S12';
        SnTDMLSecurityUtil.insertRecords(scen2,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen2.id;
        teamins2.Name = 'test2';
        teamins2.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        AxtriaSalesIQTM__Position__c pos2= TestDataFactory.createPosition(team,teamins2);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos2,className);
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        AxtriaSalesIQTM__Position_Account__c posAccount11 = TestDataFactory.createPositionAccount(acc1,pos2,teamins2);
        posAccount11.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount11.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount11,className);
        AxtriaSalesIQTM__Position_Account__c posAccount111 = TestDataFactory.createPositionAccount(acc11,pos,teamins);
        posAccount111.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount111.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount111,className);
        Deassign_Postiton_Account__c daposacc = new Deassign_Postiton_Account__c();
        daposacc.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c;
        daposacc.Account__c = acc.id;
        daposacc.Country_Name__c = countr.Name;
        daposacc.CurrencyIsoCode = 'USD';
        daposacc.Position__c = pos.id;
        daposacc.Rule_Type__c = 'test';
        daposacc.Status__c = 'New';
        daposacc.Team_Instance__c = teamins.Id;
        daposacc.Name ='test';
        SnTDMLSecurityUtil.insertRecords(daposacc,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = acc.AccountNumber;
        zt.Position_Text__c = pos.id;
        zt.country__c = 'USA';
        zt.Team_Instance_Text__c = teamins.id;
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'Bottom Up';
        zt.Object__c ='Deassign_Postiton_Account__c';
        SnTDMLSecurityUtil.insertRecords(zt,className);
        
        AxtriaSalesIQTM__Business_Rules__c b = new AxtriaSalesIQTM__Business_Rules__c();
        b.AxtriaSalesIQTM__Scenario__c = scen.id;
        b.AxtriaSalesIQTM__Rule_Type__c = 'Top Down';
        SnTDMLSecurityUtil.insertRecords(b,className);
        Deassign_Postiton_Account__c de = new Deassign_Postiton_Account__c();
        de.Status__c = 'New Merge';
        de.Account__c = acc.Merge_Account_Number__c;
        de.Position__c =pos.AxtriaSalesIQTM__Client_Position_Code__c;
        de.Team_Instance__c = teamins.Name;
        de.Account_Type__c = 'HCP';
        SnTDMLSecurityUtil.insertRecords(de,className);
        
        Merge_Unmerge__c m = new Merge_Unmerge__c();
        m.Winner_Merge_Account__c = 'BH10462969';
        m.Looser_Merge_Account__c =  'BH10461999'; 
        m.External_ID__c = 'BH10462969_BH10461999'; 
        m.Losing_Source_ID__c = 'test'; 
        m.Market_Code__c = 'US'; 
        m.Country_Code__c = 'US';
        m.MDM_Customer_GUID_1__c = 'BH10462969';
        m.MDM_Customer_GUID_2__c = 'BH10461999';
        m.GUID_1_ID__c = acc.id;
        m.GUID_2_ID__c = acc1.id;
        m.MDM_Customer_ID_1__c = 'BH10462969';
        m.MDM_Customer_ID_2__c = 'BH10461999';
        m.MDM_Previous_Customer_GUID__c = 'test';
        //m.LastModifiedDate = Datetime.now().addMinutes(10);
        SnTDMLSecurityUtil.insertRecords(m,className);    
        
        Set<String> setDeassignID =new Set<String>();
        setDeassignID.add(de.id);
         Set<String> looserRecSet =new Set<String>();
        looserRecSet.add(acc.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            List<String> FIELD_LIST = new List<String>{'AccountNumber__c','Account_Text__c'};
                  //  System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            BatchMergeTopDownHandling obj=new BatchMergeTopDownHandling(setDeassignID,false,looserRecSet);
            Database.executeBatch(obj);            
        }
        Test.stopTest();
    }
	static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        string classname = 'BatchMergeTopDownHandlingTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc.Status__c = 'Active';
        acc.Type = 'HCA';
        acc.Merge_Account_Number__c ='BH10461999';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        acc1.Type = 'HCA';
        SnTDMLSecurityUtil.insertRecords(acc1,className);
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10462969';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
        acc11.Type = 'HCA';
        SnTDMLSecurityUtil.insertRecords(acc11,className);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        SnTDMLSecurityUtil.insertRecords(affnet,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;//SHOULD BE HCA
        accaff.AxtriaSalesIQTM__Account__c = acc1.Id;//HCP
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc1,affnet);
        accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;//HCA
        accaff1.AxtriaSalesIQTM__Account__c = acc1.Id;//HCA
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff1,className);
        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff11.Account_Number__c = 'BH10462969';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        SnTDMLSecurityUtil.insertRecords(accaff11,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
         AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen2.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S12';
        SnTDMLSecurityUtil.insertRecords(scen2,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen2.id;
        teamins2.Name = 'test2';
        teamins2.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        AxtriaSalesIQTM__Position__c pos2= TestDataFactory.createPosition(team,teamins2);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos2,className);
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        AxtriaSalesIQTM__Position_Account__c posAccount11 = TestDataFactory.createPositionAccount(acc1,pos2,teamins2);
        posAccount11.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount11.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount11,className);
        AxtriaSalesIQTM__Position_Account__c posAccount111 = TestDataFactory.createPositionAccount(acc11,pos,teamins);
        posAccount111.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount111.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(posAccount111,className);
        Deassign_Postiton_Account__c daposacc = new Deassign_Postiton_Account__c();
        daposacc.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c;
        daposacc.Account__c = acc.id;
        daposacc.Country_Name__c = countr.Name;
        daposacc.CurrencyIsoCode = 'USD';
        daposacc.Position__c = pos.id;
        daposacc.Rule_Type__c = 'test';
        daposacc.Status__c = 'New';
        daposacc.Team_Instance__c = teamins.Id;
        daposacc.Name ='test';
        SnTDMLSecurityUtil.insertRecords(daposacc,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = acc.AccountNumber;
        zt.Position_Text__c = pos.id;
        zt.country__c = 'USA';
        zt.Team_Instance_Text__c = teamins.Name;
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'Bottom Up';
        zt.Object__c ='Deassign_Postiton_Account__c';
        SnTDMLSecurityUtil.insertRecords(zt,className);
        
        AxtriaSalesIQTM__Business_Rules__c b = new AxtriaSalesIQTM__Business_Rules__c();
        b.AxtriaSalesIQTM__Scenario__c = scen.id;
        b.AxtriaSalesIQTM__Rule_Type__c = 'Top Down';
        SnTDMLSecurityUtil.insertRecords(b,className);
        Deassign_Postiton_Account__c de = new Deassign_Postiton_Account__c();
        de.Status__c = 'New Merge';
        de.Account__c = acc.Merge_Account_Number__c;
        de.Position__c =pos.AxtriaSalesIQTM__Client_Position_Code__c;
        de.Team_Instance__c = teamins.Name;
        de.Account_Type__c = 'HCA';
        SnTDMLSecurityUtil.insertRecords(de,className);
        
        Merge_Unmerge__c m = new Merge_Unmerge__c();
        m.Winner_Merge_Account__c = 'BH10462969';
        m.Looser_Merge_Account__c =  'BH10461999'; 
        m.External_ID__c = 'test'; 
        m.Losing_Source_ID__c = 'test'; 
        m.Market_Code__c = 'test'; 
        m.Country_Code__c = 'test';
        m.MDM_Customer_GUID_1__c = 'test';
        m.MDM_Customer_GUID_2__c = 'test';
        m.GUID_1_ID__c = 'test';
        m.GUID_2_ID__c = 'test';
        m.MDM_Customer_ID_1__c = 'test';
        m.MDM_Customer_ID_2__c = 'test';
        m.MDM_Previous_Customer_GUID__c = 'test';
        //m.LastModifiedDate = Datetime.now().addMinutes(10);
        SnTDMLSecurityUtil.insertRecords(m,className);    
        
        Set<String> setDeassignID =new Set<String>();
        setDeassignID.add(de.id);
         Set<String> looserRecSet =new Set<String>();
        looserRecSet.add(acc.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            List<String> FIELD_LIST = new List<String>{'AccountNumber__c','Account_Text__c'};
                  //  System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            BatchMergeTopDownHandling obj=new BatchMergeTopDownHandling(setDeassignID,false,looserRecSet);
            Database.executeBatch(obj);            
        }
        Test.stopTest();
    }
}