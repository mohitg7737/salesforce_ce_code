global class updatePosGeoAZ implements Database.Batchable<sObject>, Database.Stateful{
    Id team_ins;
    Map<String, Set<String>> mapPosGeo = new Map<String, Set<String>>();
    global updatePosGeoAZ(ID ti) { 
        team_ins = ti;
    }
         
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([select id, AxtriaSalesIQTM__Geography__r.name , AxtriaSalesIQTM__Position__r.name, AxtriaSalesIQTM__SharedWith__c,AxtriaSalesIQTM__isshared__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__c = :team_ins AND (AxtriaSalesIQTM__Assignment_Status__c = 'Future Active' OR AxtriaSalesIQTM__Assignment_Status__c = 'Active')]);
    }
    
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Geography__c> scope) {
     
        for(AxtriaSalesIQTM__Position_Geography__c pg : scope ){
            
            String key = pg.AxtriaSalesIQTM__Geography__r.name;
            if(mapPosGeo.containsKey(key)){
                mapPosGeo.get(key).add(pg.AxtriaSalesIQTM__Position__r.name);
            }
            else{
                mapPosGeo.put(key,new Set<String>{pg.AxtriaSalesIQTM__Position__r.name});
            }   
        }
     
       
    }
        global void finish(Database.BatchableContext BC) {
              Database.executeBatch(new batchUpdatePosGeoAZ(JSON.serialize(mapPosgeo), team_ins));           
        }
    }