@isTest
private class Test_BatchDeactivePosition {
   static testmethod void testdeactivepositionaccount() {

       AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
       OM.Name = 'ABC';
       OM.AxtriaSalesIQTM__Parent_Country_Level__c = True;
       OM.AxtriaSalesIQTM__Org_Level__c = 'Global';
       Insert OM;

       AxtriaSalesIQTM__Country__c Coun = new AxtriaSalesIQTM__Country__c();
       Coun.Name = 'ABC';
       Coun.AxtriaSalesIQTM__Status__c = 'Active';
       Coun.AxtriaSalesIQTM__Parent_Organization__c = OM.id;
       insert Coun;

       AxtriaSalesIQTM__Team__c Team = New AxtriaSalesIQTM__Team__c();
       Team.Name = 'Test';
       Team.AxtriaSalesIQTM__Country__c = Coun.id;
       insert Team;

       AxtriaSalesIQTM__Team_Instance__c Teaminstance = new AxtriaSalesIQTM__Team_Instance__c();
       Teaminstance.Name = 'TestR';
       Teaminstance.AxtriaSalesIQTM__Team__c = Team.id;
       insert Teaminstance;

       AxtriaSalesIQTM__Position__c Position = New AxtriaSalesIQTM__Position__c();
       Position.Name = 'TestRe';
       Position.AxtriaSalesIQTM__Team_iD__c = Team.id;
       insert Position;

       Account acc = new Account();
       acc.Name = 'Testrec';
       acc.Marketing_Code__c = 'IT';
       insert acc;

       AxtriaSalesIQTM__Position_Account__c PA = new AxtriaSalesIQTM__Position_Account__c();
    //PA.Name = 'Testreco';
       PA.AxtriaSalesIQTM__Team_Instance__c = Teaminstance.id;
       PA.AxtriaSalesIQTM__Position__c = Position.id;
       PA.AxtriaSalesIQTM__Account__c = acc.id;
    //PA.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
       insert PA;

       AxtriaSalesIQTM__Position_Account_Call_Plan__c PACP = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
       PACP.Name = 'Testrecods';
       PACP.AxtriaSalesIQTM__Team_Instance__c = Teaminstance.id;
       PACP.AxtriaSalesIQTM__Position__c = Position.id;
       PACP.AxtriaSalesIQTM__Account__c = acc.id;  
       PACP.AxtriaSalesIQTM__lastApprovedTarget__c = True;
       insert PACP;
       ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
       String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
       List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
       System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

       Database.executeBatch(new BatchDeactivePositioncallPlanAccount('ABC'));
     // Database.executeBatch(new BatchDeactivePositioncallPlanAccount());

   }
}