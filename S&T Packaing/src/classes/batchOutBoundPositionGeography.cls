global class batchOutBoundPositionGeography implements Database.Batchable<sObject>, Database.Stateful,schedulable{
        global Integer recordsProcessed;
        global String batchID;
        global DateTime lastjobDate=null;
        global String query;
        public List<String> posCodeList=new List<String>();
        global Date today=Date.today();
        public String teamInstanceId =null;                            
        public set<String> Uniqueset {get;set;}
        public set<String> mkt {get;set;} 
        public map<String,String>mapVeeva2Mktcode {get;set;}
        global List<String> teamInstList =new List<String>(); 
        global List<AxtriaSalesIQTM__Position__c> posList=new List<AxtriaSalesIQTM__Position__c>();
        public list<Custom_Scheduler__c> mktList {get;set;} 
        public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
        static boolean recursive = false;
         public String cycle {get;set;}
 

    global batchOutBoundPositionGeography (String TeamInstance,Integer records,String bID){
            
    }
    
    global batchOutBoundPositionGeography (){
           
            
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
        database.executeBatch(new batchOutBoundUpdPosGeography());
       
    }
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Geography__c> records){
    }    
    global void finish(Database.BatchableContext bc){
    }  
}