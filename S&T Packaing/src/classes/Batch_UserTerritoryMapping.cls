global with sharing class Batch_UserTerritoryMapping implements Database.Batchable<sObject> {
    list<User_Territory__c> userTerr;
    map<string, User_Territory__c> mapUserTerr;
    string jobType = util.getJobType('UserTerritory');
    list<string> allTeamInstance = jobType == 'Full Load'?util.getFullLoadTeamInstancesCallPlan():util.getDeltaLoadTeamInstancesCallPlan();
   // map<string, AxtriaSalesIQTM__Position_Employee__c> mapPosEmp;
   // set<string> setExtId;
    string query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(jobType == 'Full Load'){
            query = 'Select Id, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c From AxtriaSalesIQTM__Position_Employee__c Where AxtriaSalesIQTM__Assignment_Status__c IN (\'Active\') AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED Order by AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Assignment_Status__c';
        }
        else{    
            query = 'Select Id, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c From AxtriaSalesIQTM__Position_Employee__c Where AxtriaSalesIQTM__Assignment_Status__c IN (\'Active\', \'Inactive\') AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED Order by AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Assignment_Status__c';
        }
        return database.getQueryLocator(query);
    }
    
    public void initiateVariables(){
        userTerr = new list<User_Territory__c>();
        mapUserTerr = new map<string, User_Territory__c>();
     //   mapPosEmp = new map<string, AxtriaSalesIQTM__Position_Employee__c>();
     //   setExtId = new set<string>();
    }
    
    public void createMaps(set<string> setExternalId){
        //for(AxtriaSalesIQTM__Position_Employee__c pe : scope){ 
        //    mapPosEmp.put(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,pe) ;
        //}
        for(User_Territory__c ut : [Select Id, Event__c, External_ID__c, Territory__c, User__c from User_Territory__c Where External_ID__c In :setExternalId WITH SECURITY_ENFORCED]){
            mapUserTerr.put(ut.External_ID__c, ut);
        }
    }
        
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Employee__c> scope){
        set<string> setExternalId = new set<string>();
        set<string> setIds = new set<string>();
        for(AxtriaSalesIQTM__Position_Employee__c pe : scope){
            setExternalId.add(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        initiateVariables();
        createMaps(setExternalId);
        for(AxtriaSalesIQTM__Position_Employee__c pe : scope){
            User_Territory__c ut = new User_Territory__c();
            string externalId = pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            if(!setIds.contains(externalId)){
                setIds.add(externalId);
                if(!mapUserTerr.containsKey(externalId) && pe.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){
                    ut.Event__c = 'Inserted';
                    ut.External_ID__c = externalId;
                    ut.Territory__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    ut.User__c = pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                    ut.Country__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c;
                    ut.Record_Status__c = 'Updated';
                    userTerr.add(ut);
                }
                else if((pe.AxtriaSalesIQTM__Assignment_Status__c == 'Active' && mapUserTerr.get(externalId).Event__c == 'Deleted') || (pe.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive' && mapUserTerr.get(externalId).Event__c != 'Deleted')){
                    ut.Event__c = mapUserTerr.get(externalId).Event__c == 'Deleted'?'Inserted':'Deleted';
                    ut.Id = mapUserTerr.get(externalId).Id;
                    ut.Record_Status__c = 'Updated';
                    userTerr.add(ut);
                }   
               /* else if(mapUserTerr.containsKey(externalId) && pe.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive'){
                    ut.AxtriaSalesIQST__Event__c = 'Deleted';
                    ut.Id = mapUserTerr.get(externalId).Id;
                    userTerr.add(ut);
                } */
                else {}
            } 
        }
        //upsert(userTerr);
        SnTDMLSecurityUtil.upsertRecords(userTerr,'Batch_UserTerritoryMapping');
    }
    
    global void finish(Database.BatchableContext bc){
    }
}