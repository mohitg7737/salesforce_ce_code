global class BatchDeltaMCChannelStatus_MultiChannel implements Database.Batchable<sObject>
{
    public String query;
    public Datetime Lmd;
    List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> mCyclePlanChannel;
    List<Parent_PACP__c> pacpRecs;
    List<String> allTeamInstancespro;
    List<String> allTeamInstances;
    List<String> allChannels;
    String teamInstanceSelected;
    
    global BatchDeltaMCChannelStatus_MultiChannel(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        Lmd= lastjobDate;
         teamInstanceSelected = teamInstanceSelectedTemp;
         query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c AxtriaSalesIQTM__Metric5_Approved__c, Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ) from Parent_PACP__c where LastModifiedDate >: Lmd  and Team_Instance__c = :teamInstanceSelected';
          allChannels = allChannelsTemp;
    }

     global BatchDeltaMCChannelStatus_MultiChannel(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        Lmd= lastjobDate;
         query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c AxtriaSalesIQTM__Metric5_Approved__c,Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ) from Parent_PACP__c where LastModifiedDate >: Lmd  and Team_Instance__c in :allTeamInstances';
         allTeamInstances = new List<String>(teamInstanceSelectedTemp);
          allChannels = allChannelsTemp;
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }
     public void create_MC_Cycle_Plan_Channel_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
         system.debug('++query'+query);
        allTeamInstancespro= new List<String>();
        for(Parent_PACP__c pacprecords: scopePacpProRecs)
        {
            allTeamInstancespro.add(pacprecords.Team_Instance__c);
        }
         system.debug('++query'+query);
        //pacpRecs = scopePacpProRecs;
        mCyclePlanChannel = new List<SIQ_MC_Cycle_Plan_Channel_vod_O__c>();
        Set<String> allRecs = new Set<String>();

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstancespro[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;

        List<Veeva_Market_Specific__c>  veevaCriteria = [select Channel_Name__c,Metric_Name__c,Weight__c,MCCP_Target_logic__c, Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        Decimal max = 0;
        Decimal sum = 0;
        decimal sumOtherChannels=0;
        
        map<string,string>allChannels = new map<string,string>();
        Map<string,string> metricAndWeights = new Map<string,string>();
        Map<string,decimal> channelCallMapping = new Map<string,decimal>();     // stores mapping of channels and their respective calls.
        for(integer i=0; i < veevaCriteria[0].Channel_Name__c.split(',').size();i++){
            
            metricAndWeights.put(veevaCriteria[0].Metric_Name__c.split(',')[i],veevaCriteria[0].Weight__c.split(',')[i]);
            allChannels.put(veevaCriteria[0].Metric_Name__c.split(',')[i],veevaCriteria[0].Channel_Name__c.split(',')[i]); 
            channelCallMapping.put(veevaCriteria[0].Metric_Name__c.split(',')[i],0.0);
           
           
        }

        for(Parent_PACP__c ppacp : scopePacpProRecs)
        {
            system.debug('++ppacp'+ppacp);
            max = 0;
            sum = 0;
            Integer f2fCalls = 0;
            sumOtherChannels=0;
            Boolean flag = false;
            Boolean deltacheck=false;
            SIQ_MC_Cycle_Plan_Channel_vod_O__c mcpc = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();

           // String channels = 'F2F';
           channelCallMapping = new Map<string,decimal>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
                if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c==true)
                {
                    system.debug('++hey');
                    deltacheck= true;
                    if(pacp.Final_TCF__c == null)
                        pacp.Final_TCF__c = 0;
                    if(max < pacp.Final_TCF__c)
                    {
                        max = Integer.valueOf(pacp.Final_TCF__c);
                    }

                    flag = true;
                    sum = sum + pacp.Final_TCF__c;
                    
                     /**************** code for sum the other channels**************/ 
                    for(string metric:metricAndWeights.keyset())
                    {
                        if(pacp.get(metric)!=null)
                        {
                               decimal channelcall=channelCallMapping.get(metric);
                               decimal weight=Decimal.valueOf(metricAndWeights.get(metric));
                               decimal metricValue=(decimal)(pacp.get(metric));
                               channelcall=channelcall+(metricValue*weight);
                               channelCallMapping.put(metric,channelcall);
                        }
                        
                    }
                    if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' )
                    {
                        channelCallMapping.put('Final_TCF__c',sum);
                        
                    }
                    else{
                        
                        channelCallMapping.put('Final_TCF__c',max);
                        
                    }
                //********************************* end ************************    
                  
                   
                } 
                
                    
                   
            }
            for(string metric:allChannels.keyset())
            {
                string channels=allChannels.get(metric);
                if(deltacheck==true)
                    {    
                             system.debug('++hey');
    						  string teamPos = ppacp.Team_Instance__r.Name +'_' + ppacp.Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                        mcpc.SIQ_Cycle_Channel_vod__c       =   ppacp.Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';
                        
                        DateTime dtStart                 =   ppacp.Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                        Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());
                        String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
                        mcpc.SIQ_Cycle_Plan_Target_vod__c   =   teamPos + '_' + ppacp.Account__r.AccountNumber;
                        mcpc.SIQ_External_Id_vod__c =teamPos + '-' +channels + '-'+ ppacp.Account__r.AccountNumber;
                            mcpc.Rec_Status__c = 'Updated';
                            mcpc.External_ID_Axtria__c = mcpc.SIQ_External_Id_vod__c;
                            mcpc.Team_Instance__c = ppacp.Team_Instance__c;
                            mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                            
                            if(flag && !allRecs.contains(mcpc.SIQ_External_Id_vod__c))
                            {
                                mcpc.SIQ_RecordTypeId__c  = veevaCriteria[0].MC_Cycle_Plan_Channel_Record_Type__c;
    
                                if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                                {
                                    mcpc.SIQ_Channel_Activity_Goal_vod__c = channelCallMapping.get(metric);//sum
                                    mcpc.SIQ_Channel_Activity_Max_vod__c = channelCallMapping.get(metric);//sum
                                }
                                else
                                {
                                    mcpc.SIQ_Channel_Activity_Goal_vod__c = channelCallMapping.get(metric);//max
                                    mcpc.SIQ_Channel_Activity_Max_vod__c = channelCallMapping.get(metric);//max
                                }
                                mCyclePlanChannel.add(mcpc);
                                allRecs.add(mcpc.SIQ_External_Id_vod__c);
                            }
                        }
                else
                    {
                             system.debug('++hey');
                            mcpc.Rec_Status__c = 'Deleted';
                             mcpc.External_ID_Axtria__c=ppacp.Team_Instance__r.Name +'_' + ppacp.Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '-' +channels + '-'+ ppacp.Account__r.AccountNumber;
                              mCyclePlanChannel.add(mcpc);
                        }
            
                
            }            
            
        }
        //create_MC_Cycle_Plan_Product_vod();
    }
    

    global void execute(Database.BatchableContext BC, List<Sobject> scopePacpProRecs) 
    {
        create_MC_Cycle_Plan_Channel_vod(scopePacpProRecs);
        
        upsert mCyclePlanChannel External_ID_Axtria__c;
    }

    global void finish(Database.BatchableContext BC) {

    }
}