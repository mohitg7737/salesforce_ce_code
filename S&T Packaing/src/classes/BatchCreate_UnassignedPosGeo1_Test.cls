@isTest
public class BatchCreate_UnassignedPosGeo1_Test{
     static testMethod void Test_BatchCreate_UnassignedPosGeo1() {
            
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Unassigned PosGeo Delta';
        sJob.Job_Status__c = 'Successful';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'abc';
        insert geo;   
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.name = 'teste';
        insert team;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'test';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '00000';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        teamInstance.name = 'Hello';
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
       // teamInstance.AxtriaSalesIQTM__Position_ID__c = pos.id;
        insert teamInstance; 
        
        AxtriaSalesIQTM__Position_Team_Instance__c pTeminstance = new AxtriaSalesIQTM__Position_Team_Instance__c();
       // pTeminstance.name = 'TestRU';
        pTeminstance.AxtriaSalesIQTM__Position_ID__c = pos.id;
        pTeminstance.AxtriaSalesIQTM__Effective_End_Date__c = system.Today()+1;
        pTeminstance.AxtriaSalesIQTM__Effective_Start_Date__c= system.Today();
        pTeminstance.AxtriaSalesIQTm__Team_Instance_ID__c =  teamInstance.id;
        insert pTeminstance;
        
        AxtriaSalesIQTM__Position_Geography__c posGeo = new AxtriaSalesIQTM__Position_Geography__c();
        posGeo.AxtriaSalesIQTM__Position__c = pos.id;
        posGeo.AxtriaSalesIQTM__Geography__c = geo.id;
        posGeo.AxtriaSalesIQTM__Effective_End_Date__c = system.Today()+1;
        posGeo.AxtriaSalesIQTM__Effective_Start_Date__c = system.Today();
        posGeo.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;        
        insert posGeo;
        
             
        BatchCreate_UnassignedPosGeo1 BC_UnGEO = new BatchCreate_UnassignedPosGeo1();
        BC_UnGEO.geoDeltaQuery = 'geo';
        
        Database.executeBatch(new BatchCreate_UnassignedPosGeo1());
        
  }
}