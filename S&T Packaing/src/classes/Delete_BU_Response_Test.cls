/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the Delete_BU_Response.
*/

@isTest
private class Delete_BU_Response_Test
{

    static testMethod void testMethod1()
    {
        User loggedInUser = new User(id = UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c = 'yes';
        acc.AxtriaSalesIQTM__Speciality__c = 'testspecs';

        insert acc;

        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c = 'yes';
        acc2.AxtriaSalesIQTM__Speciality__c = 'testspecs';
        acc2.name = 'testacc2';
        insert acc2;

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'test';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;



        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamInstance, countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;


        Measure_Master__c measureMaster = TestDataFactory.createMeasureMaster(pcc, team, teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;

        measureMaster.State__c = 'Executed';
        update measuremaster;
        measureMaster.State__c = 'Executing';
        update measuremaster;
        measureMaster.State__c = 'Complete';
        update measuremaster;
        measureMaster.State__c = 'Publishing';
        update measuremaster;
        measureMaster.State__c = 'Published to Alignment';
        update measuremaster;
        measureMaster.State__c = 'Publishing to Alignment';
        update measuremaster;




        insert  new MetaData_Definition__c (Display_Name__c = 'ACCESSIBILITY', Source_Field__c = 'Accessibility_Range__c', Source_Object__c = 'BU_Response__c', Team_Instance__c = teamInstance.id, Product_Catalog__c = pcc.Id);

        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team, teamInstance);
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc, pos, teamInstance);
        insert posAccount;

        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c = 'testSpecs';
        pp.priority__c = 'P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount, pcc, teamInstance, team, acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        insert bu;
        AxtriaSalesIQTM__Change_Request_Type__c crtype = TestDataFactory.createChangeReqType(SalesIQGlobalConstants.CR_TYPE_CALL_PLAN );
        insert crtype;

        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__RecordTypeID__c = crtype.id;
        insert cr;


        Test.startTest();
        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String> {nameSpace + 'Parameter__c'};
            System.assertEquals(true, SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_BU_Response obj = new Delete_BU_Response(teamInstance.Name, 'File');
            Database.executeBatch(obj);

            obj = new Delete_BU_Response(teamInstance.Name, 'File',(String)cr.id);
            obj.onDemand = false;
            Database.executeBatch(obj);

            obj = new Delete_BU_Response((String)measureMaster.id,(String)measureMaster.id,teamInstance.Name,pcc.Veeva_External_ID__c,'HCO', true);
            Database.executeBatch(obj);

            

        }
        Test.stopTest();
    }
    static testMethod void testMethod3()
    {
        User loggedInUser = new User(id = UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c = 'yes';
        acc.AxtriaSalesIQTM__Speciality__c = 'testspecs';

        insert acc;

        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c = 'yes';
        acc2.AxtriaSalesIQTM__Speciality__c = 'testspecs';
        acc2.name = 'testacc2';
        insert acc2;

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;


        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamInstance, countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;

        Measure_Master__c measureMaster = TestDataFactory.createMeasureMaster(pcc, team, teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;

        insert  new MetaData_Definition__c (Display_Name__c = 'ACCESSIBILITY', Source_Field__c = 'Accessibility_Range__c', Source_Object__c = 'BU_Response__c', Team_Instance__c = teamInstance.id, Product_Catalog__c = pcc.Id);

        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team, teamInstance);
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc, pos, teamInstance);
        insert posAccount;

        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c = 'testSpecs';
        pp.priority__c = 'P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount, pcc, teamInstance, team, acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;

        insert bu;

        Test.startTest();
        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String> {nameSpace + 'Parameter__c'};
            System.assertEquals(true, SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_BU_Response obj = new Delete_BU_Response(teamInstance.Name + ';' + pcc.Veeva_External_ID__c);
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}