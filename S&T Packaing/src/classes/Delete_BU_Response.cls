global with sharing class Delete_BU_Response implements Database.Batchable<sObject>
{
    public String query;
    public String teamInstance;
    public String type;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand = false; //Added by dhiren
    
    //Added by Chirag Ahuja for STIMPS-286
    public boolean HCOFlag = false;
    public String selectedSourceRule;
    public String selectedDestinationRule;

    //Added by HT(A0094) on 17th June 2020 for SNT-428
    public String changeReqID;
    public Boolean proceedBatch = true;

    //Added by Chirag Ahuja for STIMPS-286. Related to HCO Segmentation
    global Delete_BU_Response(String sourceRuleId, String destinationRuleId, String teamInstance, String prod, String type, Boolean flag)
    {
        this.selectedSourceRule = sourceRuleId;
        this.selectedDestinationRule = destinationRuleId;
        this.teamInstance = teamInstance;
        this.type = type;
        this.product = prod;
        this.HCOFlag = flag;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance===?'+teamInstance+'==type===?'+type+'==product===?'+product+'==HCOFlag===?'+HCOFlag);

        query = 'SELECT Id FROM BU_Response__c WHERE Team_Instance__r.Name = \'' + teamInstance + '\' and Type__c = \'' + type + '\' and Product__r.Veeva_External_ID__c = \'' + product + '\' WITH SECURITY_ENFORCED';
    }

    //Added by HT(A0094) on 17th June 2020 for SNT-428
    global Delete_BU_Response(String teamInstance, String type,String crID)
    {
        this.type = type;
        this.teamInstance = teamInstance;

        //Added by HT(A0094) on 17th June 2020 for SNT-428
        changeReqID = crID;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance--'+teamInstance);
        SnTDMLSecurityUtil.printDebugMessage('type--'+type);
        SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);

        query = 'SELECT Id FROM BU_Response__c WHERE Team_Instance__r.Name = \'' +
                teamInstance+'\' WITH SECURITY_ENFORCED';
    }

    //public boolean scheduledjob=true;
    global Delete_BU_Response(String teamInstance, String type)
    {
        this.type = type;
        this.teamInstance = teamInstance;
        query = 'SELECT Id FROM BU_Response__c WHERE Team_Instance__r.Name = \'' + teamInstance + '\' WITH SECURITY_ENFORCED';
    }

    global Delete_BU_Response(String teamInstanceProd)
    {
        teamInstance_Prod = teamInstanceProd;
        SnTDMLSecurityUtil.printDebugMessage('<><><><><><><><>' + teamInstance_Prod);
        teamInst_ProdList = new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList = teamInstance_Prod.split(';');
        }

        SnTDMLSecurityUtil.printDebugMessage('<><>teamInst_ProdList<><<>' + teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        type = 'file';
        ondemand = true;

        SnTDMLSecurityUtil.printDebugMessage('<>>>>>>><>teamInstance<><><><><><><> ' + teamInstance + '  <>>>>>>>>product>>>>>>>>>> ' + product);

        query = 'SELECT Id FROM BU_Response__c WHERE Team_Instance__r.Name = :teamInstance AND Product__r.Veeva_External_ID__c =: product WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        SnTDMLSecurityUtil.printDebugMessage('query-->' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<BU_Response__c> scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('scope size-->' + scope.size());
        if(scope.size() > 0 && scope != null)
        {
            if(BU_Response__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else
            {
                proceedBatch = false;
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete BU_Response__c', 'Delete_BU_Response');
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        String universeOption = [ Select Segmentation_Universe__c from AxtriaSalesIQTM__Team_Instance__c where Name = : teamInstance WITH SECURITY_ENFORCED].Segmentation_Universe__c;
        Boolean sntInput = (universeOption == 'Full S&T Input Customers') ? true : false;

        if(type == 'File' || type == 'Both')
        {
            if(ondemand)
            {
                AxtriaSalesIQTM__Team_Instance__c teamInst = [ Select id, isVeevaSurveyScheduled__c from AxtriaSalesIQTM__Team_Instance__c where Name = : teamInstance WITH SECURITY_ENFORCED];
                Boolean flagteamInst = teamInst.isVeevaSurveyScheduled__c;

                if(flagteamInst)
                {
                    StagingVeevaSurveyData batch = new StagingVeevaSurveyData(teamInstance_Prod, 'Dummy String');
                    Database.executeBatch(batch, 1000);
                }
                else
                {
                    if(sntInput)
                    {
                        BatchCreateAlignedCustomerFlag batch = new BatchCreateAlignedCustomerFlag(teamInstance_Prod, 'Dummy String');
                        Database.executeBatch(batch, 1000);
                    }
                    else
                    {
                        Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(teamInstance_Prod, 'Dummy String');
                        Database.executeBatch(batch, 400);
                    }
                }
            }
            else     //In case of ty
            {
                if(sntInput)
                {
                    //Added by HT(A0994) on 26th October 2020 for SR-299
                    if(changeReqID!=null && changeReqID!='')
                    {
                        if(proceedBatch)
                        {
                            BatchCreateAlignedCustomerFlag batch = new BatchCreateAlignedCustomerFlag(teamInstance,'Dummy String',type,changeReqID);
                            Database.executeBatch(batch, 400);
                        }
                        else{
                            SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
                        }
                    }
                    else
                    {
                        BatchCreateAlignedCustomerFlag batch = new BatchCreateAlignedCustomerFlag(teamInstance, 'Dummy String', type);
                        Database.executeBatch(batch, 400);
                    }
                }
                else
                {
                    //Modified by HT(A0994) on 17th June 2020 for SNT-428
                    if(changeReqID!=null && changeReqID!='')
                    {
                        if(proceedBatch)
                        {
                            Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(teamInstance,Id.valueOf(changeReqID));
                            Database.executeBatch(batch, 400);
                        }
                        else{
                            SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
                        }
                    }
                    else
                    {
                        Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(teamInstance);
                        Database.executeBatch(batch, 400);
                    }
                }
            }
        }
        else     
        {
        //Added by Chirag Ahuja for STIMPS-286
            if(type == 'HCO'){
                if(HCOFlag){
                    //Call batch BatchCreateHCOParameterValueMap
                    Database.executeBatch(new BatchCreateHCOParameterValueMap(selectedSourceRule,selectedDestinationRule,teamInstance,type,HCOFlag),2000);
                }
            }
            else{       //This is for the Scheduled Veeva batch
                StagingVeevaSurveyData batch = new StagingVeevaSurveyData(teamInstance);
                Database.executeBatch(batch, 1000);
            }
        }
    }
}