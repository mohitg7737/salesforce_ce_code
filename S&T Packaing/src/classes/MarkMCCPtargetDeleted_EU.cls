global with sharing class MarkMCCPtargetDeleted_EU implements Database.Batchable<sObject> {
  public String query;
  public List<String> allTeamInstances;
  public String ti;
  public List<String> allChannels;
  Set<String> activityLogIDSet;

  global MarkMCCPtargetDeleted_EU() 
  {
    system.debug('Default Constructor');
    this.query = 'select id from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' ';
  }

  global MarkMCCPtargetDeleted_EU(List<String> allTeamInstances1) 
  {

    allTeamInstances = new List<String>(allTeamInstances1);

    system.debug('++++++ Hey All TO are '+ allTeamInstances);

    this.query = 'select id, Team_Instance__c,Territory__c , SIQ_Status_vod__c, SIQ_Cycle_Plan_vod__c, SIQ_Target_vod__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';
  }

  global MarkMCCPtargetDeleted_EU(List<String> allTeamInstances1, List<String> allChannelsTemp) 
  {
    allChannels = new List<String>(allChannelsTemp);
    allTeamInstances = new List<String>(allTeamInstances1);

    system.debug('++++++ Hey All TO are '+ allTeamInstances);

    this.query = 'select id, Team_Instance__c,Territory__c , SIQ_Status_vod__c, SIQ_Cycle_Plan_vod__c, SIQ_Target_vod__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';
  }

    //Added by Ayushi

  global MarkMCCPtargetDeleted_EU(List<String> allTeamInstances1, List<String> allChannelsTemp,Set<String> activityLogSet) 
  {
    allChannels = new List<String>(allChannelsTemp);
    allTeamInstances = new List<String>(allTeamInstances1);
    activityLogIDSet = new Set<String>();
    activityLogIDSet.addAll(activityLogSet);

    system.debug('++++++ Hey All TO are '+ allTeamInstances);

    this.query = 'select id, Team_Instance__c,Territory__c , SIQ_Status_vod__c, SIQ_Cycle_Plan_vod__c, SIQ_Target_vod__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';

        //Added By Ayushi
        /*List<Error_Object__c> insertErrorObj = new List<Error_Object__c>();
        for(String teamIns : allTeamInstances)
        {
            Error_Object__c rec = new Error_Object__c();
            rec.Object_Name__c = 'MarkMCCPtargetDeleted_EU';
            rec.Team_Instance_Name__c = teamIns;
            insertErrorObj.add(rec);
        }
        insert insertErrorObj;*/
        //till here

      }

    //Till here........

      global MarkMCCPtargetDeleted_EU(String selectedTeamInstance) 
      {
        system.debug('Default Single Team');
        ti = selectedTeamInstance;

        this.query = 'select id, Team_Instance__c,Territory__c , SIQ_Status_vod__c, SIQ_Cycle_Plan_vod__c, SIQ_Target_vod__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c = :ti';
      }

      global Database.QueryLocator start(Database.BatchableContext bc) 
      {
        return Database.getQueryLocator(query);
      }

      global void execute(Database.BatchableContext BC, list<SIQ_MC_Cycle_Plan_Target_vod_O__c> scope) 
      {
        
        Set<String> allTargets = new Set<String>();
        Set<String> allPositions = new Set<String>();
        Set<String> allTeamInstances = new Set<String>();


        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mt : scope)
        {
          allTargets.add(mt.SIQ_Target_vod__c);
            //List<String> tempList = (mt.SIQ_Cycle_Plan_vod__c).split('_');
          allPositions.add(mt.Territory__c);
          allTeamInstances.add(mt.Team_Instance__c);
        }
        system.debug('+++allPositions'+allPositions);
        system.debug('+++allTeamInstances'+allTeamInstances);

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp= [select ID, Segment10__c ,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber IN  :allTargets and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c IN :allPositions and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances];

        Map<String,String> posaccseg = new Map<String,String>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pp : pacp)
        {
          string accpos= pp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_'+ pp.AxtriaSalesIQTM__Team_Instance__c;
          posaccseg.put(accpos,pp.Segment10__c);

          system.debug('+++accpos :::::::::::'+accpos);
          system.debug('+++pp.Segment10__c'+pp.Segment10__c);
        }
        system.debug('+++posaccseg'+posaccseg);


        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mct : scope)
        {  

         String acc = mct.SIQ_Target_vod__c;
           //List<String> tempList = (mct.SIQ_Cycle_Plan_vod__c).split('_');
         String pos = mct.Territory__c;
         String accPos = acc + '_' + pos + '_' + mct.Team_Instance__c;

         system.debug('+++acc:::::::::'+acc);
         system.debug('+++pos:::::::::'+pos);
         system.debug('+++accPos:::::'+accPos);

         if(posaccseg.containsKey(accPos))
         {
          system.debug('+++posaccseg.get(accPos):::::'+posaccseg.get(accPos));            
          if(posaccseg.get(accPos) =='Inactive')
          {
            mct.SIQ_Status_vod__c = 'Inactive';
            mct.Rec_Status__c ='Updated';
          }
          else if(posaccseg.get(accPos) =='Merged')
          {
            mct.SIQ_Status_vod__c = 'Merged';
            mct.Rec_Status__c ='Updated'; 
          }
          else
          {
            mct.SIQ_Status_vod__c = 'Deleted';
            mct.Rec_Status__c ='Deleted';
          }  
        } 
        else
        {
          mct.SIQ_Status_vod__c = 'Deleted';
          mct.Rec_Status__c ='Deleted';
        }  
      }  
    //update scope;

      SnTDMLSecurityUtil.updateRecords(scope, 'MarkMCCPtargetDeleted_EU');
    }

    global void finish(Database.BatchableContext BC) 
    {
      if(activityLogIDSet != null)
      {
        changeMCChannelStatus u1New = new changeMCChannelStatus(allTeamInstances, allChannels,activityLogIDSet);  
        Database.executeBatch(u1New, 2000);
      }
      
      else
      {
        changeMCChannelStatus u1 = new changeMCChannelStatus(allTeamInstances, allChannels);  
        Database.executeBatch(u1, 2000);
      }
      
    }
  }