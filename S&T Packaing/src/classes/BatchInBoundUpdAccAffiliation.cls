global class BatchInBoundUpdAccAffiliation implements Database.Batchable<sObject>, Database.Stateful,schedulable{
    public Integer recordsProcessed=0;
    public Integer successcount =0;
    public String batchID;
    global DateTime lastjobDate=null;
    global String query;
    public map<String, String> mapCountryCode ;
    public map<String, String> mapVeevaCode ;
    public map<String, String> mapMarketingCode ;
    public set<String> AclList {get;set;}
    public map<String,String>Accnoid {get;set;}
    //public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
    public boolean enable ;
    public String afiliationid {get;set;}
    public map<String,String>mapAffiNet {get;set;}
    public list<AxtriaSalesIQTM__Affiliation_Network__c>afflist {get;set;}
    public String cycle {get;set;}
    public list<ClusterCountry__c> clusterCountry{get;set;}

    Set<id> accAffId = new Set<id>();                                   
                                 
    
    global BatchInBoundUpdAccAffiliation (){//set<String> Accountid
      /*AclList.addAll(Accountid);
      for(String S : Accountid){
        String[] arr=s.split('_');
        String Key = arr[0];
        String Value=arr[1];
        if(!Accnoid.containskey(Key)){
          Accnoid.put(key,Value);
        }
      }*/
      clusterCountry = new list<ClusterCountry__c>();
      clusterCountry = ClusterCountry__c.getall().values();



      mapVeevaCode = new map<String,String>();
      mapMarketingCode = new map<String,String>();

      mapAffiNet = new map<String, String>();
      afflist=[Select Id,Name,AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Affiliation_Network__c];
      for(AxtriaSalesIQTM__Affiliation_Network__c a : afflist){
        if(!mapAffiNet.containskey(a.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c )){
          mapAffiNet.put(a.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c ,a.id);
        }
      }
      system.debug('============mapAffiNet::::'+mapAffiNet);
      list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
      for(SIQ_MC_Country_Mapping__c c : countryMap)
      {
        if(!mapVeevaCode.containskey(c.SIQ_Country_Code__c))
        {
          mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
        }
        if(!mapMarketingCode.containskey(c.SIQ_Country_Code__c))
        {
          mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
        }
      }
      //mapCountryCode = new map<String, String>();
      // countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
      //for(AxtriaSalesIQTM__Country__c c : countrylist){
      //  if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
      //    mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
      //  }
      // }
      enable =false;// to insert the affiliation network only once and linking that as it is necessary.
      afiliationid = '';
      List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
      cycleList=[Select Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
      if(cycleList!=null){
        for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
        {
          if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
          cycle = t1.Cycle__r.Name;
        }
      }
      //String cycle=cycleList.get(0).Name;
      // cycle=cycle.substring(cycle.length() - 3);
      
  
      List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
      schLogList=[Select Id,CreatedDate,Created_Date__c,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Affiliation Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
      if(schLogList.size()>0){
          lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
      }
      else{
        lastjobDate=null;       //else we set the lastjobDate to null
      }
      System.debug('last job'+lastjobDate);
        //Last Bacth run ID
      Scheduler_Log__c sJob = new Scheduler_Log__c();    
      sJob.Job_Name__c = 'Affiliation Delta';
      sJob.Job_Status__c = 'Failed';
      sJob.Job_Type__c='Inbound';
      sJob.Created_Date2__c = DateTime.now();
      
      if(cycle!=null && cycle!='')
        sJob.Cycle__c=cycle;
      
      insert sJob;
      batchID = sJob.Id;
      recordsProcessed =0;
      successcount = 0;
      query = 'SELECT CreatedById,CreatedDate,CurrencyIsoCode, ' +
      'Id,Name, ' +
      'OwnerId,SIQ_Account_Id__c,SIQ_Account_Number__c,SIQ_Affiliation_End_Date__c,SIQ_Affiliation_Hierarchy__c, ' +
      'SIQ_Affiliation_Id__c,SIQ_Affiliation_Name__c,SIQ_Affiliation_Start_Date__c,SIQ_Affiliation_Status__c, ' + 
      'SIQ_Affiliation_Sub_Type__c,SIQ_Affiliation_Type__c,SIQ_Country_Code__c,SIQ_External_ID__c, ' +
      'SIQ_Last_Modified_Date__c,SIQ_Marketing_Code__c,SIQ_Parent_Account_Id__c,SIQ_Parent_Account_Number__c, ' +
      'SIQ_Primary_Affiliation_Indicator__c,SIQ_Publish_Date__c,SIQ_Publish_Event__c,SIQ_Role_Name__c,SystemModstamp ' +
      'FROM SIQ_Account_Affiliation__c ' ;
        if(lastjobDate!=null){
          query = query + 'Where LastModifiedDate  >=:  lastjobDate order by AxtriaARSnT__SIQ_Last_Modified_Date__c DESC NULLS LAST '; 
        }
        System.debug('query'+ query);
    //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
    }
    
    
   global Database.QueryLocator  start(Database.BatchableContext bc) {
         enable = true;         
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchInBoundUpdAccAffiliation(),50);
       
    }
     global void execute(Database.BatchableContext bc, List<SIQ_Account_Affiliation__c> records){
      
      AclList = new Set<String>();
      map<String,String>AccId = new Map<String,String>();
      map<String,String>Clusteraccounts = new Map<String,String>();
      list<Temp_Acc_Affliation__c>tempacflist = new list<Temp_Acc_Affliation__c>();
      set<string>uniqueset = new set<string>();
      set<string>child2parent = new set<string>();
      List<AxtriaSalesIQTM__Account_Affiliation__c> accounts = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
      List<AxtriaSalesIQTM__Account_Affiliation__c> clusteracclist = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
      list<Error_Object__c>errorlist = new list<Error_Object__c>();

      map<string,ClusterCountry__c>clustermap = new map<string,ClusterCountry__c>();
      clustermap = ClusterCountry__c.getAll();
      
      for(SIQ_Account_Affiliation__c SAA : records)
      {
        AclList.add(SAA.SIQ_Account_Number__c);
        AclList.add(SAA.SIQ_Parent_Account_Number__c);
      }
      List<Account>AccountList = [select id,AccountNumber,External_Account_Id__c,Country_Code__c,Marketing_Code__c from Account where AccountNumber IN :AclList]; 
      for(Account Acc : AccountList)
      {
          if(!AccId.containsKey(Acc.AccountNumber))
          {
               AccId.put(Acc.AccountNumber, Acc.id);
              //System.debug(Acc.External_Account_Id__c);
          }
          string key=acc.Country_Code__c+'_'+Acc.AccountNumber;
          if(!Clusteraccounts.containskey(key))
          {
            Clusteraccounts.put(key,acc.id);
          }
      }
      system.debug('========AccId::::'+AccId);
      /*Filling affiliationidmap to check whether the affiliation record is already present or not so that affiliation network filed error should not occur*/
      for(AxtriaSalesIQTM__Account_Affiliation__c aff : [select id,Unique_Id__c from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.Accountnumber in:AclList]){
        child2parent.add(aff.Unique_Id__c);
      }
      system.debug('=========child2parent:::'+child2parent.size());

      


      /*=========NON Cluster affiliations================*/
      for(SIQ_Account_Affiliation__c acc : records) 
      {
          string countrycode = mapVeevaCode.get(acc.SIQ_Country_Code__c);
          system.debug('=======countrycode::::'+countrycode);

          if(AccId.containsKey(acc.SIQ_Account_Number__c) && AccId.containsKey(acc.SIQ_Parent_Account_Number__c) && mapAffiNet.get(countrycode) !=null)
          {
              string key= acc.SIQ_Account_Number__c+'_'+acc.SIQ_Parent_Account_Number__c;
              if(!uniqueset.contains(key))
              {
                AxtriaSalesIQTM__Account_Affiliation__c account=new AxtriaSalesIQTM__Account_Affiliation__c();
                //account.AxtriaSalesIQTM__Affiliation_Network__c=afiliationid;// 'a1l5E000000851tQAA';
                
                if(!child2parent.contains(key))
                {
                  system.debug('============inside not contains block=========');
                  account.AxtriaSalesIQTM__Affiliation_Network__c=mapAffiNet.get(countrycode);
                }
                
                /*else if(child2parent == null){
                    system.debug('===========NULL BLOCK-----------');
                    
                  string countrycode = mapVeevaCode.get(acc.SIQ_Country_Code__c);
                  account.AxtriaSalesIQTM__Affiliation_Network__c=mapAffiNet.get(countrycode);
                }*/
                system.debug('========acc.SIQ_Account_Number__c:::'+acc.SIQ_Account_Number__c);
                if(AccId.containsKey(acc.SIQ_Account_Number__c))
                {
                  system.debug('==========acc Present-----');
                  account.AxtriaSalesIQTM__Account__c=AccId.get(acc.SIQ_Account_Number__c);
                }
                  
                account.AxtriaSalesIQTM__Affiliation_End_Date__c=acc.SIQ_Affiliation_End_Date__c;
                account.AxtriaSalesIQTM__Affiliation_Start_Date__c=acc.SIQ_Affiliation_Start_Date__c;
                account.AxtriaSalesIQTM__Affiliation_Type__c=acc.SIQ_Affiliation_Type__c;
                account.CurrencyIsoCode=acc.CurrencyIsoCode;
                account.Account_Number__c=acc.SIQ_Account_Number__c;
                account.Affiliation_Status__c=acc.SIQ_Affiliation_Status__c;
                account.Affiliation_Sub_Type__c=acc.SIQ_Affiliation_Sub_Type__c;
                account.Affiliation_End_Date__c=acc.SIQ_Affiliation_End_Date__c;
                account.Affiliation_Hierarchy__c=acc.SIQ_Affiliation_Hierarchy__c;
                account.Affiliation_Id__c=acc.SIQ_Affiliation_Id__c;
                account.Affiliation_Name__c=acc.SIQ_Affiliation_Name__c;
                account.Affiliation_Start_Date__c=acc.SIQ_Affiliation_Start_Date__c;
                account.Affiliation_Type__c=acc.SIQ_Affiliation_Type__c;
                //account.Country_Code__c=acc.SIQ_Country_Code__c;
                if(mapVeevaCode.get(acc.SIQ_Country_Code__c)!=null)
                {
                  account.Country_Code__c =mapVeevaCode.get(acc.SIQ_Country_Code__c);
                }
                else
                {
                  account.Country_Code__c=null;
                }
                // account.Marketing_Code__c=acc.SIQ_Marketing_Code__c;
                if(mapMarketingCode.get(acc.SIQ_Country_Code__c)!=null)
                {
                  account.Marketing_Code__c=mapMarketingCode.get(acc.SIQ_Country_Code__c);
                }
                account.External_Id__c=acc.SIQ_External_ID__c;
                account.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
                
                if(AccId.containsKey(acc.SIQ_Parent_Account_Number__c))
                {
                   account.Parent_Account_Id__c=AccId.get(acc.SIQ_Parent_Account_Number__c);
                   account.AxtriaSalesIQTM__Parent_Account__c=AccId.get(acc.SIQ_Parent_Account_Number__c);
                   account.AxtriaSalesIQTM__Root_Account__c = AccId.get(acc.SIQ_Parent_Account_Number__c);
                }
                account.Parent_Account_Number__c=acc.SIQ_Parent_Account_Number__c;
                account.Primary_Affiliation_Indicator__c=acc.SIQ_Primary_Affiliation_Indicator__c;
                if(acc.SIQ_Primary_Affiliation_Indicator__c =='Y' || acc.SIQ_Primary_Affiliation_Indicator__c=='y')
                {
                  account.AxtriaSalesIQTM__Is_Primary__c =true;
                }
                else
                {
                  account.AxtriaSalesIQTM__Is_Primary__c =false;
                }

                if(acc.SIQ_Affiliation_Status__c =='Active' || acc.SIQ_Affiliation_Status__c=='active')
                {
                    account.AxtriaSalesIQTM__Active__c=true;
                }
                else
                {
                  account.AxtriaSalesIQTM__Active__c=false;
                }
                //account.AxtriaSalesIQTM__Active__c=
                account.Role_Name__c=acc.SIQ_Role_Name__c;
                account.SIQ_Account_Affiliation_Id__c =acc.Name;
                account.Unique_Id__c = acc.SIQ_Account_Number__c+'_'+acc.SIQ_Parent_Account_Number__c;
                  
                //  if(mapCountryCode.get(acc.SIQ_Country__c)!=null){
                //  account.AxtriaSalesIQTM__Country__c =mapCountryCode.get(acc.SIQ_Country__c);
                //  }
                //  else
                //  {
                //  account.AxtriaSalesIQTM__Country__c=null;
                //  }
                accounts.add(account);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                uniqueset.add(key);

              }
              else
              {
                Error_Object__c err = new Error_Object__c();
                err.Comments__c = 'Duplicate Records in Staging';
                err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                err.Field_Name__c = key;
                err.Record_Id__c = acc.id;
                err.Team_Instance_Name__c = 'Inbound Affiliation';
                errorlist.add(err);
              }
              
              //comments
          }
          else{
              //Error Loge for affiliation records
               //list<AxtriaARSnT__Error_Object__c>errorlist = new list<AxtriaARSnT__Error_Object__c>();
               if(!AccId.containsKey(acc.SIQ_Account_Number__c))
               {
                  Error_Object__c err = new Error_Object__c();
                  err.Comments__c = 'Account Number Notpresent in SalesIQ';
                  err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                  err.Field_Name__c = acc.SIQ_Account_Number__c;
                  err.Record_Id__c = acc.id;
                  err.Team_Instance_Name__c = 'Inbound Affiliation';
                  errorlist.add(err);
              
               }
               else if(!AccId.containsKey(acc.SIQ_Parent_Account_Number__c))
               {
                  Error_Object__c err = new Error_Object__c();
                  err.Comments__c = 'Parent Account Number Notpresent in SalesIQ';
                  err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                  err.Field_Name__c = acc.SIQ_Parent_Account_Number__c;
                  err.Record_Id__c = acc.id;
                  err.Team_Instance_Name__c = 'Inbound Affiliation';
                  errorlist.add(err);
               }
               else if(mapAffiNet.get(countrycode) == null || mapAffiNet.get(countrycode) == '')
               {
                  Error_Object__c err = new Error_Object__c();
                  err.Comments__c = 'Affiliation Network Does not present';
                  err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                  err.Record_Id__c = acc.id;
                  err.Field_Name__c = acc.SIQ_Country_Code__c;
                  err.Team_Instance_Name__c = 'Inbound Affiliation';
                  errorlist.add(err);
               }

          }
        
      }
      upsert accounts Unique_Id__c;
      if(errorlist!=null)
      {
        insert errorlist;
      }
        
      /*Cluster Affiliations=================================== added by siva*/
      uniqueset = new set<string>();
      errorlist = new list<Error_Object__c>();
      for(SIQ_Account_Affiliation__c acc : records) 
      {
          string countrycode = acc.SIQ_Country_Code__c;
          string clustercode='';
          system.debug('=======countrycode::::'+countrycode);
          system.debug('===========clustermap:::::'+clustermap);

          if(clustermap.containskey(countrycode))
          {
              ClusterCountry__c cc = clustermap.get(countrycode);
              clustercode = cc.Country_Code__c;
              system.debug('============Cluster Country codde for :'+countrycode+': is :'+clustercode);
              string childkey = clustercode+'_'+acc.SIQ_Account_Number__c;
              string parentkey = clustercode+'_'+acc.SIQ_Parent_Account_Number__c;
              if(Clusteraccounts.containsKey(childkey) && Clusteraccounts.containskey(parentkey) && mapAffiNet.get(clustercode) !=null)
              {

                string key= childkey+'_'+parentkey;
                if(!uniqueset.contains(key))
                {
                  AxtriaSalesIQTM__Account_Affiliation__c clusteracc=new AxtriaSalesIQTM__Account_Affiliation__c();
                  //if(!child2parent.contains(key))
                  //{
                  system.debug('============Cluster block========='+countrycode);
                  clusteracc.AxtriaSalesIQTM__Affiliation_Network__c=mapAffiNet.get(clustercode);
                  //}
                  system.debug('========Cluster child key::'+childkey);
                  if(Clusteraccounts.containsKey(childkey))
                  {
                    system.debug('==========Cluster parent Present-----');
                    clusteracc.AxtriaSalesIQTM__Account__c=Clusteraccounts.get(childkey);
                  }
                  clusteracc.AxtriaSalesIQTM__Affiliation_End_Date__c=acc.SIQ_Affiliation_End_Date__c;
                  clusteracc.AxtriaSalesIQTM__Affiliation_Start_Date__c=acc.SIQ_Affiliation_Start_Date__c;
                  clusteracc.AxtriaSalesIQTM__Affiliation_Type__c=acc.SIQ_Affiliation_Type__c;
                  clusteracc.CurrencyIsoCode=acc.CurrencyIsoCode;
                  clusteracc.Account_Number__c=acc.SIQ_Account_Number__c;
                  clusteracc.Affiliation_Status__c=acc.SIQ_Affiliation_Status__c;
                  clusteracc.Affiliation_Sub_Type__c=acc.SIQ_Affiliation_Sub_Type__c;
                  clusteracc.Affiliation_End_Date__c=acc.SIQ_Affiliation_End_Date__c;
                  clusteracc.Affiliation_Hierarchy__c=acc.SIQ_Affiliation_Hierarchy__c;
                  clusteracc.Affiliation_Id__c=acc.SIQ_Affiliation_Id__c;
                  clusteracc.Affiliation_Name__c=acc.SIQ_Affiliation_Name__c;
                  clusteracc.Affiliation_Start_Date__c=acc.SIQ_Affiliation_Start_Date__c;
                  clusteracc.Affiliation_Type__c=acc.SIQ_Affiliation_Type__c;
                  clusteracc.Country_Code__c =clustercode;
                  clusteracc.Marketing_Code__c=clustercode;
                  /*if(mapVeevaCode.get(acc.SIQ_Country_Code__c)!=null)
                  {
                    account.Country_Code__c =mapVeevaCode.get(acc.SIQ_Country_Code__c);
                  }
                  else
                  {
                    account.Country_Code__c=null;
                  }
                  if(mapMarketingCode.get(acc.SIQ_Country_Code__c)!=null)
                  {
                    account.Marketing_Code__c=mapMarketingCode.get(acc.SIQ_Country_Code__c);
                  }*/
                  clusteracc.External_Id__c=acc.SIQ_External_ID__c;
                  clusteracc.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
                  if(Clusteraccounts.containskey(parentkey))
                  {
                     clusteracc.Parent_Account_Id__c=Clusteraccounts.get(parentkey);
                     clusteracc.AxtriaSalesIQTM__Parent_Account__c=Clusteraccounts.get(parentkey);
                     clusteracc.AxtriaSalesIQTM__Root_Account__c = Clusteraccounts.get(parentkey);
                  }
                  clusteracc.Parent_Account_Number__c=acc.SIQ_Parent_Account_Number__c;
                  clusteracc.Primary_Affiliation_Indicator__c=acc.SIQ_Primary_Affiliation_Indicator__c;
                  if(acc.SIQ_Primary_Affiliation_Indicator__c =='Y' || acc.SIQ_Primary_Affiliation_Indicator__c=='y')
                  {
                    clusteracc.AxtriaSalesIQTM__Is_Primary__c =true;
                  }
                  else
                  {
                    clusteracc.AxtriaSalesIQTM__Is_Primary__c =false;
                  }
                  if(acc.SIQ_Affiliation_Status__c =='Active' || acc.SIQ_Affiliation_Status__c=='active')
                  {
                    clusteracc.AxtriaSalesIQTM__Active__c=true;
                  }
                  else
                  {
                    clusteracc.AxtriaSalesIQTM__Active__c=false;
                  }
                  clusteracc.Role_Name__c=acc.SIQ_Role_Name__c;
                  clusteracc.SIQ_Account_Affiliation_Id__c =acc.Name;
                  clusteracc.Unique_Id__c = key;
                  clusteracclist.add(clusteracc);
                  system.debug('recordsProcessed+'+recordsProcessed);
                  //recordsProcessed++;
                  uniqueset.add(key);

                }
                else
                {
                  Error_Object__c err = new Error_Object__c();
                  err.Comments__c = 'Cluster -Duplicate Records in Staging';
                  err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                  err.Field_Name__c = key;
                  err.Record_Id__c = acc.id;
                  err.Team_Instance_Name__c = 'Inbound Affiliation';
                  errorlist.add(err);
                }
           
              }
              else
              {
                //Error Loge for affiliation records
                 if(!Clusteraccounts.containsKey(childkey))
                 {
                    Error_Object__c err = new Error_Object__c();
                    err.Comments__c = ' Cluster Account Number Notpresent in SalesIQ';
                    err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                    err.Field_Name__c = acc.SIQ_Account_Number__c;
                    err.Record_Id__c = acc.id;
                    err.Team_Instance_Name__c = 'Inbound Affiliation';
                    errorlist.add(err);
                
                 }
                 else if(!Clusteraccounts.containsKey(parentkey))
                 {
                    Error_Object__c err = new Error_Object__c();
                    err.Comments__c = 'Cluster Parent Account Number Notpresent in SalesIQ';
                    err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                    err.Field_Name__c = acc.SIQ_Parent_Account_Number__c;
                    err.Record_Id__c = acc.id;
                    err.Team_Instance_Name__c = 'Inbound Affiliation';
                    errorlist.add(err);
                 }
                 else if(mapAffiNet.get(clustercode) == null || mapAffiNet.get(clustercode) == '')
                 {
                    Error_Object__c err = new Error_Object__c();
                    err.Comments__c = ' Cluster Affiliation Network Does not present';
                    err.Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
                    err.Record_Id__c = acc.id;
                    err.Field_Name__c = acc.SIQ_Country_Code__c;
                    err.Team_Instance_Name__c = 'Inbound Affiliation';
                    errorlist.add(err);
                 }
              }
          } 
      }
      upsert clusteracclist Unique_Id__c;
      if(errorlist!=null)
      {
        insert errorlist;
      }


      /*END of cluster affiliation by siva*/
      //commented below code on 31-12-2018 as affiliations are not working if batch size is 200
        /*
        Schema.SObjectField f = AxtriaSalesIQTM__Account_Affiliation__c.Fields.Unique_Id__c;
        database.UpsertResult [] results = database.upsert(accounts ,f,false); 
        list<AxtriaARSnT__Error_Object__c>errorlist = new list<AxtriaARSnT__Error_Object__c>();
        for(database.UpsertResult ur : results){
          if(ur.isSuccess()){
            successcount++;
          }else{
            system.debug('===========ERROR Occured===========');
            for(Database.Error error : ur.getErrors()){
              AxtriaARSnT__Error_Object__c err = new AxtriaARSnT__Error_Object__c();
              err.AxtriaARSnT__Comments__c = (String)error.getMessage();
              err.AxtriaARSnT__Object_Name__c = 'AxtriaSalesIQTM__Account_Affiliation__c';
              err.AxtriaARSnT__Record_Id__c = ur.getId();
              string errorfld ='_';
              for(string field : error.getFields()){
                errorfld+=field+';';
              }
              err.AxtriaARSnT__Field_Name__c = errorfld;
              errorlist.add(err); 
            }
              
          }
        }
        if(errorlist !=null && errorlist.size() >0){
          Database.SaveResult[] srList = Database.insert(errorlist, false);
          //insert errorlist;
        }*/
        system.debug('===========size of accounts::'+accounts.size());
        set<string>uniquecheck = new set<string>();
        for(AxtriaSalesIQTM__Account_Affiliation__c accAff : accounts)
        {
           //commented this  by siva 
           /*accAffId.add(accAff.AxtriaSalesIQTM__Account__c);
           System.debug('++'+accAff.AxtriaSalesIQTM__Account__c);
           accAffId.add(accAff.AxtriaSalesIQTM__Parent_Account__c);*/
           if(!uniquecheck.contains(accAff.AxtriaSalesIQTM__Parent_Account__c)){
               Temp_Acc_Affliation__c taf = new Temp_Acc_Affliation__c();
               taf.Account__c = accAff.AxtriaSalesIQTM__Parent_Account__c;
               taf.Type__c = 'HCA';
               tempacflist.add(taf);
               uniquecheck.add(accAff.AxtriaSalesIQTM__Parent_Account__c);
           }
        }
        system.debug('===========tempacflist.size()::'+tempacflist.size());
        if(tempacflist!=null)
          upsert tempacflist Account__c;
        /*  Commented by siva on 23-01-2018
            Schema.SObjectField unq = Temp_Acc_Affliation__c.Fields.Account__c;
            database.upsert(tempacflist,unq, false);
        */
  }    
    global void finish(Database.BatchableContext bc){


        // execute any post-processing operations
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sjob.Object_Name__c = 'Account Affiliation';
        sjob.Changes__c = 'Success Count is:'+successcount;                                               
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;

        Database.ExecuteBatch( new BatchPrimaryAffiliationIndicator(lastjobDate),200);    
                                                           

    }   
}