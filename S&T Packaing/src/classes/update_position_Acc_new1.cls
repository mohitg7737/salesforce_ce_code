global with sharing class update_position_Acc_new1 implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_acc_Terr__c with temp_obj__c due to object purge*/
    global string query;
    global string teamID;
    global string teamInstance;
    String Ids;
    Boolean flag = true;
    
    public integer recordsProcessed;
    
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global Map<Id, Set<Id>> mapTI2Acc = new Map<Id, Set<Id>>();
    global Map<Id, Set<Id>> mapTI2id = new Map<Id, Set<Id>>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global List<TempPosAcc__c> tempPAlist = new List<TempPosAcc__c>();
    public ID BCc {get; set;}
    global String batchStatus {get; set;}
    global string Assignmentstatus ;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue;

    global update_position_Acc_new1(String Team, String TeamIns)
    {
       
    }

    global update_position_Acc_new1(String Team, String TeamIns, String Ids)
    {
       
    }

    //Added by HT(A0994) on 17th June 2020
    global update_position_Acc_new1(String Team, String TeamIns, String Ids,Boolean flag)
    {
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<temp_Obj__c> accterrlist)
    {      

    }

global void finish(Database.BatchableContext BC)
{
}
}