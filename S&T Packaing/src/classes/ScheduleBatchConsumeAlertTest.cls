@isTest 
global class ScheduleBatchConsumeAlertTest {
	static testMethod void testAlert(){
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        //ScheduleBatchConsumeAlert.SendAlerts();
        //	ScheduleBatchConsumeAlert.fakeMethod();
	}
}