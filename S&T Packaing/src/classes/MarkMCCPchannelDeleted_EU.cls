global with sharing class MarkMCCPchannelDeleted_EU implements Database.Batchable<sObject> {
    public String query;
    public List<String> allTeamInstances;
    public String teamInstanceSelected;
    public List<String> allChannels;
    Set<String> activityLogIDSet;
    
    global MarkMCCPchannelDeleted_EU() {
        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' ';
    }

    global MarkMCCPchannelDeleted_EU(List<String> allTeamInstancesList) {

        allTeamInstances = new List<String>(allTeamInstancesList);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';
    }

    global MarkMCCPchannelDeleted_EU(List<String> allTeamInstancesList, List<String> allChannelsTemp) {

        allTeamInstances = new List<String>(allTeamInstancesList);
        allChannels = new List<String>(allChannelsTemp);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';
    }

    //Added by Ayushi
    global MarkMCCPchannelDeleted_EU(List<String> allTeamInstancesList, List<String> allChannelsTemp,Set<String> activityLogSet) {

        allTeamInstances = new List<String>(allTeamInstancesList);
        activityLogIDSet = new Set<String>();
        allChannels = new List<String>(allChannelsTemp);
        activityLogIDSet.addAll(activityLogSet);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances';

        //Added By Ayushi
        /*List<Error_Object__c> insertErrorObj = new List<Error_Object__c>();
        for(String teamIns : allTeamInstances)
        {
            Error_Object__c rec = new Error_Object__c();
            rec.Object_Name__c = 'MarkMCCPchannelDeleted_EU';
            rec.Team_Instance_Name__c = teamIns;
            insertErrorObj.add(rec);
        }
        insert insertErrorObj;*/
        //till here
    }
    //Till here.....

    global MarkMCCPchannelDeleted_EU(String selectedTeamInstance) {

        teamInstanceSelected = selectedTeamInstance;

        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c = :teamInstanceSelected';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scope) {

        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mct : scope)
        {
            mct.Rec_Status__c ='Deleted';
        }   

        //update scope;
        SnTDMLSecurityUtil.updateRecords(scope, 'MarkMCCPchannelDeleted_EU');
        
    }

    global void finish(Database.BatchableContext BC) 
    {
        //START SNT-378 Added by A1942
        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [Select id from AxtriaSalesIQTM__Team_Instance__c where id in: allTeamInstances and Team_Goals__c not in ('Total Objective+Max Calls','Total Objective') WITH SECURITY_ENFORCED];
        allTeamInstances.clear();
        for(AxtriaSalesIQTM__Team_Instance__c obj: teamInsList){
            allTeamInstances.add(obj.Id);
        }
        //END SNT-378
        if(activityLogIDSet != null)
        {
            changeMCProduct u1New = new changeMCProduct(allTeamInstances, allChannels, activityLogIDSet);
            
            Database.executeBatch(u1New, 2000);   
        }
        else
        {
            changeMCProduct u1 = new changeMCProduct(allTeamInstances, allChannels);
            
            Database.executeBatch(u1, 2000);   
        }
    }
}