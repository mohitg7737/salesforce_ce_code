@isTest
private class BatchCreateAlignedCustomerFlagTest { 
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Include_Territory__c = false;
        teamins.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';
        insert teamins;
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.Name = teamins.Name;
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins2.Include_Territory__c = false;
        teamins2.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins2.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';//To store conditions for Position Accounts to be picked while creating BU responses.
        insert teamins2;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.Type__c = 'HCO';
        ssd.Account_Number__c = acc.AccountNumber;
        ssd.Team_Instance__c = teamins.id;
        ssd.Team__c= team.Id;
        ssd.CurrencyIsoCode ='USD';
        insert ssd;
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.PARTY_ID__c = acc.AccountNumber;
        svcd.BRAND_ID__c = 'test';
        svcd.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd;
        
        
        Staging_Veeva_Cust_Data__c svcd1 = new Staging_Veeva_Cust_Data__c();
        svcd1.PARTY_ID__c = acc.AccountNumber;
        svcd1.BRAND_ID__c = 'test';
        svcd1.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd1;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchCreateAlignedCustomerFlag obj=new BatchCreateAlignedCustomerFlag(teamins.Name);
            obj.query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,'+ 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,'+'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
            + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,'+
            'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,'+
            ' Type__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Include_Territory__c = false;
        teamins.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';
        insert teamins;
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.Name = teamins.Name;
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins2.Include_Territory__c = false;
        teamins2.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins2.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';//To store conditions for Position Accounts to be picked while creating BU responses.
        insert teamins2;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.Type__c = 'HCO';
        ssd.Account_Number__c = acc.AccountNumber;
        ssd.Team_Instance__c = teamins.id;
        ssd.Team__c= team.Id;
        ssd.CurrencyIsoCode ='USD';
        ssd.Product_Code__c = product.id;
        insert ssd;
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.PARTY_ID__c = acc.AccountNumber;
        svcd.BRAND_ID__c = product.id;
        svcd.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd;
        
        
        Staging_Veeva_Cust_Data__c svcd1 = new Staging_Veeva_Cust_Data__c();
        svcd1.PARTY_ID__c = acc.AccountNumber;
        svcd1.BRAND_ID__c = 'test';
        svcd1.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd1;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchCreateAlignedCustomerFlag obj=new BatchCreateAlignedCustomerFlag(teamins.Name+';'+product.id, 'diff');
            obj.query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,'+ 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,'+'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
            + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,'+
            'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,'+
            ' Type__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Include_Territory__c = false;
        teamins.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';
        insert teamins;
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.Name = teamins.Name;
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins2.Include_Territory__c = false;
        teamins2.Segmentation_Universe__c ='Full S&T Input Customers';
        teamins2.Condition_For_Position_Account__c = 'AxtriaSalesiQTM__Segment_1__c = 1';//To store conditions for Position Accounts to be picked while creating BU responses.
        insert teamins2;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.Type__c = 'HCO';
        ssd.Account_Number__c = acc.AccountNumber;
        ssd.Team_Instance__c = teamins.id;
        ssd.Team__c= team.Id;
        ssd.CurrencyIsoCode ='USD';
        ssd.Product_Code__c = product.id;
        insert ssd;
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.PARTY_ID__c = acc.AccountNumber;
        svcd.BRAND_ID__c = product.id;
        svcd.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd;
        
        
        Staging_Veeva_Cust_Data__c svcd1 = new Staging_Veeva_Cust_Data__c();
        svcd1.PARTY_ID__c = acc.AccountNumber;
        svcd1.BRAND_ID__c = 'test';
        svcd1.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert svcd1;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchCreateAlignedCustomerFlag obj=new BatchCreateAlignedCustomerFlag(teamins.Name+';'+product.id, 'diff','File');
            obj.query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,'+ 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,'+'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
            + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,'+
            'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,'+
            ' Type__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}