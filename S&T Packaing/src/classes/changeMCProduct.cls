global with sharing class changeMCProduct implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<String> allTeamInstances;
    Set<String> activityLogIDSet;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCProduct(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

    global changeMCProduct(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

    //Added by Ayushi
    global changeMCProduct(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,Set<String> activityLogSet)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
        queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;

        //Added By Ayushi
        // List<Error_Object__c> insertErrorObj = new List<Error_Object__c>();
        // for(String teamIns : allTeamInstances)
        // {
        //     Error_Object__c rec = new Error_Object__c();
        //     rec.Object_Name__c = 'changeMCProduct';
        //     rec.Team_Instance_Name__c = teamIns;
        //     insertErrorObj.add(rec);
        // }
        // insert insertErrorObj;
        //till here
        
    }
    //Till here.......
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Product_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Product_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        //update scopePacpProRecs;
        SnTDMLSecurityUtil.updateRecords(scopePacpProRecs, 'changeMCProduct');

    }

    global void finish(Database.BatchableContext BC)
    {
        if(activityLogIDSet != null)
        {
            AZ_IntegrationUtility_EU u1New = new AZ_IntegrationUtility_EU(allTeamInstances, allChannels, activityLogIDSet);
            Database.executeBatch(u1New, 1000);   
        }
        else
        {
            AZ_IntegrationUtility_EU u1 = new AZ_IntegrationUtility_EU(allTeamInstances, allChannels);
            Database.executeBatch(u1, 1000);   
        }

       
        //Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}