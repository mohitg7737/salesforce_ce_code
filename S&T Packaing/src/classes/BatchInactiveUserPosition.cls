global class BatchInactiveUserPosition implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    public String query;
    public Set<String> uapExists;
    public Set<String> posIDs;
    public Set<String> duplicateKey;
    public string countryname;
    public Map<String,String> mapUSerPosTeamInsKey2Id;
    

    global BatchInactiveUserPosition() {
        
        this.query = 'select id, AxtriaSalesIQTM__Employee__r.Employee_PRID__c , AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Employee__c where  AxtriaSalesIQTM__Assignment_Status__c =\'Inactive\'';
        system.debug('---IIIIIi' + this.query);
        
    }

    @Deprecated
    global BatchInactiveUserPosition(Date changedRec) {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=======Query is:::'+query);
        return Database.getQueryLocator(query);
    }

    public void execute(System.SchedulableContext SC){

        database.executeBatch(new BatchInactiveUserPosition());                                                           
       
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {

        system.debug('=======Query size is:::'+scope.size());
        List<String> allUsers = new List<String>();
        posIDs = new Set<String>();
        duplicateKey = new Set<String>();
        uapExists = new Set<String>();
        mapUSerPosTeamInsKey2Id = new Map<String,String>();

        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            allUsers.add(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
            posIDs.add(posEmp.AxtriaSalesIQTM__Position__c);
        }

        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__c in :posIDs and AxtriaSalesIQTM__is_Active__c = true];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
            uapExists.add(concat);
            mapUSerPosTeamInsKey2Id.put(concat,uap.Id);         
        }
        
        system.debug('+++++++++++++ uap Existing----' + uapExists);
        
        Map<String, ID> fedIdToUserId = new Map<String, ID>();
        
        for(User users : [select FederationIdentifier, id from User where FederationIdentifier in :allUsers])
        {
            if(users.FederationIdentifier != null)
            {
                fedIdToUserId.put(users.FederationIdentifier, users.id);
            }
        }
        
        system.debug('+++++++++++++ fedIdToUserId' + fedIdToUserId);
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPUpdate = new List<AxtriaSalesIQTM__User_Access_Permission__c>();

        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            if(fedIdToUserId.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c))
            {
                String userRec = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                
                String concat = posEmp.AxtriaSalesIQTM__Position__c + '_' + posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c + '_' + userRec;
                if(uapExists.contains(concat))
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uapExist = new AxtriaSalesIQTM__User_Access_Permission__c(id=mapUSerPosTeamInsKey2Id.get(concat));
                    uapExist.isCallPlanEnabled__c = false;
                    uapExist.AxtriaSalesIQTM__is_Active__c = false;
                    if(!duplicateKey.contains(concat))
                    {
                        allUAPUpdate.add(uapExist);
                        duplicateKey.add(concat);
                    }
                }
            }
        }

        if(allUAPUpdate.size() > 0)
            update allUAPUpdate;

    }

    global void finish(Database.BatchableContext BC) {
        
            Database.executeBatch(new Batch_Populate_User_Position());
        
    }
}