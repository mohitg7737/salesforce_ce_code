global class update_position_Acc implements Database.Batchable<sObject>,Database.Stateful{
    
    global string query;
    global string teamID;
    global string teamInstance; 
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global Map<Id,Set<Id>> mapTI2Acc = new Map<Id,Set<Id>>();
    global Map<Id,Set<Id>> mapTI2id = new Map<Id,Set<Id>>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Account__c> accTI=new list<AxtriaSalesIQTM__Team_Instance_Account__c>();
     
    global List<TempPosAcc__c> tempPAlist = new List<TempPosAcc__c>();
    public ID BCc {get;set;}
    global String batchStatus {get;set;}
    global string Assignmentstatus ;
    
    global update_position_Acc(String Team,String TeamIns){
        /*customsetting1 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist1 = new list<AxtriaSalesIQTM__TriggerContol__c>();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist2 = new list<AxtriaSalesIQTM__TriggerContol__c>();
        teamInstance=TeamIns;
        teamID=Team;
        teminslst=[select id,AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c=:teamID and id=:teamInstance];

        // System.debug('=====Custom Setting disable=====');
        // customsetting1 = AxtriaSalesIQTM__TriggerContol__c.getValues('PositionAccountTrigger');
        // if(customsetting1 != null)
        // {
        //   customsetting1.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        //   customsettinglist1.add(customsetting1);
        //   update customsettinglist1;
        // }

        query='SELECT Account__c,Account_Type__c,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,AccountNumber__c,Id,Territory__c,Territory_ID__c,Team__c,Status__c, Territory__r.AxtriaSalesIQTM__inactive__c, Account__r.AxtriaSalesIQTM__Active__c FROM temp_Acc_Terr__c where Team__c=: teamID and status__c = \'New\'';
        Assignmentstatus = teminslst[0].AxtriaSalesIQTM__Alignment_Period__c;*/
    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute(Database.BatchableContext BC, List<temp_Acc_Terr__c> accterrlist){


      
        //set<string> approvedAccountSet= new set<string>();
        //set<string>accountIdSet = new set<string>();
      //Added by Ayushi
       /* Map<String, Set<String>> mapAccNumTypekey2AccTerrID = new Map<String, Set<String>>();
        list<String> accID                 = new list<String>();
        list<String> list1                 = new list<String>();
        list<String> list2                 = new list<String>();
        set<string> setPosAccTeamInstance  = new set<string>();
        set<string> processedID  = new set<string>();
        list<temp_Acc_Terr__c> lsTempRecordToUpdate = new list<temp_Acc_Terr__c>();
        List<TempPosAcc__c> tempPosAccList  = new List<TempPosAcc__c>();
        Map<String,String> mapKey2AccID = new Map<String,String>();
        map<string,Temp_Acc_Terr__c>tempaccmap = new map<string,Temp_Acc_Terr__c>();
        
        system.debug(accterrlist);
        for(Temp_Acc_Terr__c rec:accterrlist)
        {
          system.debug(rec);
          tempaccmap.put(rec.id,rec);
          //accountIdSet.add(rec.AccountNumber__c); 
          System.debug('rec.Account__c:'+rec.Account__c);
          if(rec.Account__c != null)
          {
              accID.add(rec.Account__c);
              mapKey2AccID.put(rec.AccountNumber__c + '_' + rec.Account_Type__c,rec.Account__c);
              System.debug('mapKey2AccID' +mapKey2AccID);
              System.debug('accID:::'+accID);
             if(!mapAccNumTypekey2AccTerrID.containsKey(rec.AccountNumber__c + '_' + rec.Account_Type__c))
             {
                Set<String> accTerrId = new Set<String>();
                accTerrId.add(rec.Id);
                mapAccNumTypekey2AccTerrID.put(rec.AccountNumber__c + '_' + rec.Account_Type__c, accTerrID);
                System.debug('mapAccNumTypekey2AccTerrID inside if:::::'+mapAccNumTypekey2AccTerrID);
             }
             else
             {
                mapAccNumTypekey2AccTerrID.get(rec.AccountNumber__c + '_' + rec.Account_Type__c).add(rec.Id);
                System.debug('mapAccNumTypekey2AccTerrID inside else:::::'+mapAccNumTypekey2AccTerrID);
             }

          }
          if(rec.Territory__c != null )
          {
             list2.add(rec.Territory__c);
             System.debug('list2:::'+list2);
          }
        }


        //Added by Ayushi
        List<Account> accList = [select Id,AccountNumber,Type from Account where Id in :accID];  //AxtriaSalesIQTM__Active__c='Active' and
        Set<String> accNumTypeSet = new Set<String>();
        for(Account acc : accList)
        {
            System.debug('exist::::'+acc.AccountNumber+'_'+acc.Type);
            accNumTypeSet.add(acc.AccountNumber+'_'+acc.Type);
        }
        //     if(mapAccNumTypekey2AccTerrID.containsKey(acc.AccountNumber+'_'+acc.Type))
        //     {
        //         System.debug('If part');
        //         list1.add(acc.Id);
        //         System.debug('list1:::'+list1);
        //     }
        //     else
        //     {
        //         System.debug('Else part');
                
        if(mapAccNumTypekey2AccTerrID != null)  
        {
            System.debug('map not null');
            for(String key : mapAccNumTypekey2AccTerrID.keyset())
            {
                if(accNumTypeSet.contains(key))
                {
                    System.debug('If part');
                    String accountID=mapKey2AccID.get(key);
                    System.debug('accountID::::::' +accountID);
                    list1.add(accountID);
                    System.debug('list1:::'+list1);
                }
                else
                {
                    System.debug('Else part');
                    System.debug('key:::' +key);
                    for(String accTerrID : mapAccNumTypekey2AccTerrID.get(key))
                    {
                        System.debug('accTerrID:::'+accTerrID);
                        Temp_Acc_Terr__c tempAccTerrRec = tempaccmap.get(accTerrID);
                    
                        tempAccTerrRec.Status__c = 'Rejected';
                        tempAccTerrRec.Reason_Code__c = 'Incorrect Account Type in SalesIQ';
                        system.debug('======tempAccTerrRec.Account__c=='+tempAccTerrRec.Account__c);

                        system.debug('======tempAccTerrRec.Territory__c=='+tempAccTerrRec.Territory__c);
                        
                        if(tempAccTerrRec.Account__c != null && tempAccTerrRec.Territory__c != null)
                        {
                          lsTempRecordToUpdate.add(tempAccTerrRec);
                          System.debug('tempAccTerrRec:::'+tempAccTerrRec);
                          processedID.add(accTerrID);
                        }
                        
                    }
                 }
               }
             }
        

        //Alignmnet period modification
        system.debug('========Assignmentstatus====='+Assignmentstatus);
        if(Assignmentstatus =='Current'){
          Assignmentstatus ='Active';
        }
        else if(Assignmentstatus =='Future'){
            Assignmentstatus ='Future Active';
        }
        system.debug('======MOdified==Assignmentstatus====='+Assignmentstatus);
        //query existing position accounts with the key = Position+AccountNumber+TeamInstanceName+Assignment status
        list<AxtriaSalesIQTM__Position_Account__c> lsposAcc = [select id,name,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Account__c in:list1 and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c != true and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Active__c = 'Active']; 
        
        for(AxtriaSalesIQTM__Position_Account__c pa : lsposAcc){
            
            setPosAccTeamInstance.add(string.valueof(pa.AxtriaSalesIQTM__Position__c)+string.valueof(pa.AxtriaSalesIQTM__Account__c)+string.valueof(pa.AxtriaSalesIQTM__Team_Instance__c)+string.valueOf(pa.AxtriaSalesIQTM__Assignment_Status__c));
                        
        }
        //removing check for affiliation on 22-11-2018
        for(AxtriaSalesIQTM__Account_Affiliation__c accAffiliation:[SELECT Id, Name,Account_Number__c, Affiliation_Hierarchy__c, AxtriaSalesIQTM__Affiliation_Type__c, AxtriaSalesIQTM__Active__c, AxtriaSalesIQTM__Is_Primary__c FROM AxtriaSalesIQTM__Account_Affiliation__c where ((AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c= true and AxtriaSalesIQTM__Account__r.type='HCP') OR AxtriaSalesIQTM__Account__r.type!='HCP')  and Account_Number__c in: accountIdSet])
        {
           approvedAccountSet.add(accAffiliation.Account_Number__c);
        }
        system.debug('approvedAccountSet================'+approvedAccountSet); 
        system.debug('====teminslst ::' +teminslst);
        map<string,string> posmap =new map<string,string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI:[select AxtriaSalesIQTM__Position_ID__c,id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstance])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c,posTI.id);
        }
        map<string,string> accmap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Account__c accTI:[select AxtriaSalesIQTM__Account_ID__c,id from AxtriaSalesIQTM__Team_Instance_Account__c where AxtriaSalesIQTM__Account_ID__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            accmap.put(accTI.AxtriaSalesIQTM__Account_ID__c,accTI.id);
        }
        
        list<AxtriaSalesIQTM__Position_Account__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(temp_Acc_Terr__c cprcd : accterrlist){
      if(cprcd.Account__c != null && cprcd.Territory__c != null && !processedID.contains(cprcd.id)){
                if(!setPosAccTeamInstance.contains(string.valueof(cprcd.Territory__c)+string.valueof(cprcd.Account__c)+teamInstance+Assignmentstatus)){
                    if(cprcd.Account__r.AxtriaSalesIQTM__Active__c == 'Active' && cprcd.Territory__r.AxtriaSalesIQTM__inactive__c != True ){    //&& approvedAccountSet.contains(cprcd.AccountNumber__c)
                      AxtriaSalesIQTM__Position_Account__c posAcc=new AxtriaSalesIQTM__Position_Account__c();
                  
                      posAcc.AxtriaSalesIQTM__Account__c                  = cprcd.Account__c;
                      posAcc.AxtriaSalesIQTM__Position__c                 = cprcd.Territory__c;
                      posAcc.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                      posAcc.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(cprcd.Territory__c);
                      posAcc.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                      posAcc.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                      posAcc.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                      posAcc.AxtriaSalesIQTM__Account_Alignment_Type__c   = 'Explicit';
                      //Added on 29-08 by Ayushi
                      posAcc.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
                      posAcc.Batch_Name__c = 'Direct Load Position Account';
                      posAcc.AxtriaSalesIQTM__TempDestinationrgb__c = 'Direct Load';

                      posAccListUpdate.add(posAcc);
                      //Update Temp Record
                      cprcd.status__c = 'Processed';
                        lsTempRecordToUpdate.add(cprcd);
                    }
                    
                    else{
                        cprcd.status__c = 'Rejected';
              cprcd.Reason_Code__c = 'Inactive Position/Account in SalesIQ';
                        lsTempRecordToUpdate.add(cprcd);
                
                    }
                     // commented by Saiba 
                  else if(cprcd.Account__r.AxtriaSalesIQTM__Active__c == 'Inactive')
                  {  
                  system.debug('Inactive Account in SalesIQ'); 
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Inactive Account in SalesIQ';
                    lsTempRecordToUpdate.add(cprcd);          
                  }
                  
                  else if(! approvedAccountSet.contains(cprcd.AccountNumber__c))
                  {   
                    system.debug('Affiliation not available');
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Affiliation not available';
                    lsTempRecordToUpdate.add(cprcd);          
                  }
                  
                  else if(cprcd.Territory__r.AxtriaSalesIQTM__inactive__c == True)
                  {   
                    system.debug('Inactive Position in SalesIQ');
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Inactive Position in SalesIQ';
                    lsTempRecordToUpdate.add(cprcd);          
                  }
                   
                }else{
                    system.debug('Overlapping Assignment');
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Overlapping Assignment';
                    lsTempRecordToUpdate.add(cprcd);
                
                } 
      } */
      
      /* commented by Saiba
      else{
        cprcd.status__c = 'Rejected';
        cprcd.Reason_Code__c = 'Account/Territory/Team Instance does not exist in SalesIQ';
        lsTempRecordToUpdate.add(cprcd);
      }
      */
      
      
     /* else if(cprcd.Account__c == null)
      {
      system.debug('Account does not exist in SalesIQ');
       cprcd.status__c = 'Rejected';
        cprcd.Reason_Code__c = 'Account does not exist in SalesIQ';
        lsTempRecordToUpdate.add(cprcd);
              
      }
      
      else if(cprcd.Territory__c == null)
      {
       system.debug('Territory does not exist in SalesIQ');
       cprcd.status__c = 'Rejected';
        cprcd.Reason_Code__c = 'Territory does not exist in SalesIQ';
        lsTempRecordToUpdate.add(cprcd);
              
      }
      }*/
      
      /*else 
      {
        cprcd.status__c = 'Rejected';
        cprcd.Reason_Code__c = 'Team Instance does not exist in SalesIQ';
        lsTempRecordToUpdate.add(cprcd);
              
      }*/
    
          //Change By Ayushi
        /*  set<string>accountid = new set<string>();
          set<string>teaminstancelist = new set<string>();
          if(posAccListUpdate.size()!=0){
            insert posAccListUpdate;
            if(posAccListUpdate != null && posAccListUpdate.size() > 0)
            {
              for(AxtriaSalesIQTM__Position_Account__c posAccRec : posAccListUpdate){
                TempPosAcc__c tempobj = new TempPosAcc__c();
                tempobj.Team_Instance__c = posAccRec.AxtriaSalesIQTM__Team_Instance__c;
                tempobj.Account__c = posAccRec.AxtriaSalesIQTM__Account__c;
                tempobj.Status__c = 'New';
                tempPosAccList.add(tempobj);
                accountid.add(posAccRec.AxtriaSalesIQTM__Account__c);
                teaminstancelist.add(posAccRec.AxtriaSalesIQTM__Team_Instance__c);
              }
            }
            if(tempPosAccList.size() != 0)
              insert tempPosAccList; 
          }

          
          if(tempPosAccList!=null && tempPosAccList.size()>0){
            List<TempPosAcc__c> templist = [select Id, Team_Instance__c, Account__c, Status__c from TempPosAcc__c where Status__c = 'New' and Account__c IN :accountid and Team_Instance__c IN:teaminstancelist];
            if(templist != null)
            {
              for(TempPosAcc__c temp : templist){
                if(mapTI2Acc.containsKey(temp.Team_Instance__c))
                    {
                      mapTI2Acc.get(temp.Team_Instance__c).add(temp.Account__c);
                      mapTI2id.get(temp.Team_Instance__c).add(temp.Id);
                    }
                    else
                    {
                      Set<Id> tempSet = new Set<Id>();
                      Set<Id> idSet = new Set<Id>();
                      tempSet.add(temp.Account__c);
                      idSet.add(temp.Id);
                      mapTI2Acc.put(temp.Team_Instance__c,tempSet);
                      mapTI2id.put(temp.Team_Instance__c,idSet);
                    }
              }
            }
          }
                    
          //Till here...

          if(lsTempRecordToUpdate.size()!=0){
            update lsTempRecordToUpdate;
          }
          
          
          
          system.debug('============insert done===================');*/
          
    }
    
          global void finish(Database.BatchableContext BC){

             /*string selectedObject = 'temp_Acc_Terr__c';
            Database.executeBatch(new batchdelete3(teamID,selectedObject),1000);*/
           /* list<AsyncApexJob> ajob = new list<AsyncApexJob>();
            if(mapTI2Acc.size() != 0)
            {
              for( Id teaminstn : mapTI2Acc.keySet())
              {
                Set<Id> acc = new Set<Id>();
                system.debug('======teaminstn::::::' +teaminstn);
                acc = mapTI2Acc.get(teaminstn);
                system.debug('======acc::::' +acc);
                //BC = Database.executeBatch(new AxtriaSalesIQTM.ProcessAffiliationRulesBatch(teaminstn,acc),200);
                system.debug('============Calling batch===================');
                ProcessAffiliationRulesBatch obj = new ProcessAffiliationRulesBatch(teaminstn,acc);
                BCc=Database.executeBatch(obj,200);
              }
            } */
            /*for(id teaminst : mapTI2id.keySet()){
                  Database.executeBatch(new update_tempPosAcc(teaminst), 2000);
            }*/
            /*if(BCc!=null)
            {
              ajob = [select TotalJobItems, Status,ExtendedStatus, NumberOfErrors, JobItemsProcessed, CreatedDate, ApexClassId, ApexClass.Name from AsyncApexJob  where id =: BCc]; 
              batchStatus=ajob[0].Status;
              system.debug('======batchStatus::::' +batchStatus);
              if(batchStatus=='Completed'){
                for(id teaminst : mapTI2id.keySet()){
                  system.debug('======teaminst::::' +teaminst);
                  Database.executeBatch(new update_tempPosAcc(teaminst), 2000);
                }
              }

            }*/
            /*for(id teaminst : mapTI2id.keySet())
            {
              for(id tempID : mapTI2id.get(teaminst)){
                  system.debug('======tempID::::' +tempID);
                  TempPosAcc__c tempPA =new TempPosAcc__c(id=tempID);
                  tempPA.Status__c = 'Processed';
                  tempPAlist.add(tempPA);
              }
            }
            if(tempPAlist.size() != 0)
              update tempPAlist;*/
          

          /*customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
          customsettinglist2 = new list<AxtriaSalesIQTM__TriggerContol__c>();
          customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('PositionAccountTrigger');
          if(customsetting2 != null)
          {
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = false ;
            customsettinglist2.add(customsetting2);
            update customsettinglist2;
          }*/
            
       }
}