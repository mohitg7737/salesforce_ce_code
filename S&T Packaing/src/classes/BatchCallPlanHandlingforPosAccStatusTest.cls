@isTest
public class BatchCallPlanHandlingforPosAccStatusTest {
    @istest static void BatchCallPlanDownload_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstobjatt = TestdataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        teaminstobjatt.AxtriaSalesIQTM__isEnabled__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__isRequired__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' ;
        insert teaminstobjatt;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.Share__c = true;
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c o = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        o.cust_Type__c = 'HCP';
        insert o;
        
        Set<String> t1 = new Set<String>();
        t1.add('test');
        Map<String,Set<String>> mapInactiveAcc2PosSet = new Map<String,Set<String>> ();
        mapInactiveAcc2PosSet.put('testkey1',t1);
        Map<String,Set<String>> mapInactiveAccTIkey2PosSet = new Map<String,Set<String>> ();
        mapInactiveAccTIkey2PosSet.put('testkey1',t1);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCallPlanHandlingforPosAccStatus obj = new BatchCallPlanHandlingforPosAccStatus(t1, mapInactiveAcc2PosSet,mapInactiveAccTIkey2PosSet);
            obj.query='select id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__isTarget__c, AxtriaSalesIQTM__isTarget_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}