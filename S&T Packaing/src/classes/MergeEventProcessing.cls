public with sharing class MergeEventProcessing {
  public list<Merge_Unmerge__c>MergeList {get;set;}
  public map<String,String>LoosingCust{get;set;}
  public map<String,String>WinningCust{get;set;}
  public map<String,String>uniqmap {get;set;}
  public map<String,String>winlosuniqmap {get;set;}
  public set<String>loosingset {get;set;}
  public set<string>winningset {get;set;}
  public list<AxtriaSalesIQTM__Position_Account__c>posacc {get;set;}
  public list<String> countryCode {get;set;}
  Public String query {get;set;}

  public MergeEventProcessing(){
    MergeList = new list<Merge_Unmerge__c>();
    LoosingCust = new map<String,String>();
    WinningCust = new map<String,String>();
    loosingset = new set<String>();
    winningset = new set<String>();
    posacc = new list<AxtriaSalesIQTM__Position_Account__c>();
    uniqmap = new map<String,String>();
    winlosuniqmap = new map<String,String>();
    query = '';
    countryCode = new list<String>();
  }
  public void fillvalues( DateTime lastjobDate){
    /*
      Direct Alignment:
      1.Remove all assignments of A2 customer including affiliations
      2.Create new account A1 and assign to positions as appropriate(if Not exists).
                  (OR)
        Update details of A1 and assign to positions as appropriate
      3.Update segmentation and objectives as appropriate


    -->MDM_CUST_GUID_1 will define Surviving (winner) customer
    -->MDM_CUST_GUID_2 will define Losing customer
    -->GUID_1_ID__c   winner Customer SFDC id
    -->GUID_2_ID__c   Looser Customer SFDC id
    **Winner Customer “A” must be present in MDM_CUSTOMER file with ACTIVE status and with CREATED event.
    **If customer “A” is INACTIVE or DELETED, then BOT application will consider this customer “A” as deleted 
      and will not merge any information of customer “B” to customer “A” and BOT application will lose information of both the customers.
    
    */

    /////********/Country Code Fetch
    Map<String,MergeCountry__c> allCodes = MergeCountry__c.getAll();
    if(allCodes.size() != null || allCodes.size() > 0 )
      countryCode.addAll(allCodes.keySet());
    System.debug('====Custom country code=====' +countryCode);
    ///////////////***********  
    set<String>Loosinguniq = new set<String>();
    set<String>winninguniq = new set<String>();
    set<string>lossingaccounts = new set<string>();
    Date todaysdate=System.today();
    System.debug('=======todaysdate======'+todaysdate);
    query ='Select id,External_ID__c,Losing_Source_ID__c,Market_Code__c,MDM_Customer_GUID_1__c,MDM_Customer_GUID_2__c,'+
        'GUID_1_ID__c,GUID_2_ID__c,MDM_Customer_ID_1__c,MDM_Customer_ID_2__c,MDM_Previous_Customer_GUID__c '+
        '  from Merge_Unmerge__c ';

        if(lastjobDate!=null){
         query+=' where LastModifiedDate >=:lastjobDate';
        }

        System.debug('===============Quesy is::'+query);
    list<Merge_Unmerge__c>newMergeList  = Database.query(query);

    if(!newMergeList.isEmpty()){
      for(Merge_Unmerge__c merobj:newMergeList){
        if(merobj.GUID_1_ID__c != null){
            if(!LoosingCust.containsKey(merobj.MDM_Customer_ID_2__c)){
              LoosingCust.put(merobj.MDM_Customer_ID_2__c,merobj.GUID_2_ID__c);
            }
            if(!WinningCust.containsKey(merobj.MDM_Customer_ID_1__c)){
              WinningCust.put(merobj.MDM_Customer_ID_1__c,merobj.GUID_1_ID__c);
            }
            if(!uniqmap.containsKey(merobj.GUID_2_ID__c)){
              uniqmap.put(merobj.GUID_2_ID__c,merobj.GUID_1_ID__c);
            }
            if(!winlosuniqmap.containsKey(merobj.GUID_1_ID__c)){
              winlosuniqmap.put(merobj.GUID_1_ID__c,merobj.GUID_2_ID__c);
            }
            
            loosingset.add(merobj.MDM_Customer_GUID_2__c);
            winningset.add(merobj.MDM_Customer_GUID_1__c);
            lossingaccounts.add(merobj.GUID_2_ID__c); //adding loosing accounts SFDC id to Set
          }
      }
    }
    System.debug('===LoosingCust::'+LoosingCust);
    System.debug('===WinningCust:::'+WinningCust);
    System.debug('===uniqmap:::'+uniqmap);

    System.debug('===loosingset::'+loosingset);
    System.debug('===winningset:::'+winningset);




    List<String> periodList=new List<String>();
    periodList.add('Current');
    periodList.add('Future');
    Set<String> acIdSet=new Set<String>();
    List<Account> acList=new List<Account>();
    map<string,string>winningaccmap = new map<string,string>();
    list<Account>winningaccounts = [select id,External_HCP_No__c,Status__c from account where External_HCP_No__c IN:winningset];
    if(winningaccounts!=null){
      for(Account ac : winningaccounts){
        if(!winningaccmap.containsKey(ac.id) && ac.Status__c=='Active')
            winningaccmap.put(ac.id,ac.Status__c);
      }
    }
    List<AxtriaSalesIQTM__Position_Account__c>LosPositionAccount = [select id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,Acc_Pos__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList   and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :countryCode];
    List<AxtriaSalesIQTM__Position_Account__c>winPositionAccount = [select id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,Acc_Pos__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:winningset and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList   and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :countryCode];
    
    system.debug('++++++++++LosPositionAccount++++++++++' + LosPositionAccount);
    system.debug('++++++++++winPositionAccount++++++++++' + winPositionAccount);

    for(AxtriaSalesIQTM__Position_Account__c pa :LosPositionAccount){
      Loosinguniq.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Position__c);
    }

    for(AxtriaSalesIQTM__Position_Account__c pa :winPositionAccount){
      winninguniq.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Position__c);  
     if(pa.AxtriaSalesIQTM__Account_Alignment_Type__c.equals('Implicit')){
           //acList.add(new Account(id=pa.AxtriaSalesIQTM__Account__c));
           acIdSet.add(pa.AxtriaSalesIQTM__Account__c);
         }

    }
    for(String acId:acIdSet){
       acList.add(new Account(id=acId));
    }
    if(acList.size()>0){
      update acList;
    }
    list<AxtriaSalesIQTM__Position_Account__c>updateposacc = new list<AxtriaSalesIQTM__Position_Account__c>();
    list<AxtriaSalesIQTM__Position_Account__c>insertposacc = new list<AxtriaSalesIQTM__Position_Account__c>();
    //list<AxtriaSalesIQTM__Position_Account__c>deleteposacc = new list<AxtriaSalesIQTM__Position_Account__c>();
    list<Account>updateacc = new list<Account>();
    Map<Id,Set<Id>> mapTI2WinAcc = new Map<Id,Set<Id>>();
    Set<String> setTI = new Set<String>();
    Set<String> setAcc = new Set<String>();

    /* Customer assignment-- Direct assignment Explicit--direct assignment*/
    System.debug('===LosPositionAccount=='+LosPositionAccount);
    for(AxtriaSalesIQTM__Position_Account__c pa :LosPositionAccount){
      System.debug('=======ACCOUNT TYPE IS::'+pa.AxtriaSalesIQTM__Account_Alignment_Type__c);

      if(pa.AxtriaSalesIQTM__Account_Alignment_Type__c.equals('Implicit')){
          String Pos = pa.AxtriaSalesIQTM__Position__c;
          String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
          String Key = winacc+'_'+Pos;
          // if(winninguniq.contains(Key)){
          // deleteposacc.add(pa);
          //  pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
          if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
            pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
          }
          if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future'){
            pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
          }
          updateposacc.add(pa);
      }
       else if(pa.AxtriaSalesIQTM__Account_Alignment_Type__c.contains('Explicit')){
		
		      //Change by Ayushi
          AxtriaSalesIQTM__Position_Account__c paclone = pa.clone();
          String Pos = pa.AxtriaSalesIQTM__Position__c;
          String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
          String Key = winacc+'_'+Pos;
          if(winninguniq.contains(Key)){
                // Account acc1=new Account(id=winacc);
                //acc1.merge__c=true;
                //updateacc.add(acc1);
              if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
                pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
              }
              if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future'){
                pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
              }
              updateposacc.add(pa);
          }
        else{
            if(winningaccmap.containsKey(winacc)){
                paclone.AxtriaSalesIQTM__Account__c = winacc;
                if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                }
                if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future'){
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                }
                updateposacc.add(pa);
                system.debug('============PACLONE  by ayushi IS:::'+paclone);
                insertposacc.add(paclone);
                System.debug('=======Sets and Map for Team Instance and Winner Account=======');
                setTI.add(paclone.AxtriaSalesIQTM__Team_Instance__c);
                setAcc.add(paclone.AxtriaSalesIQTM__Account__c);
                if(mapTI2WinAcc.containsKey(paclone.AxtriaSalesIQTM__Team_Instance__c))
                {

                  mapTI2WinAcc.get(paclone.AxtriaSalesIQTM__Team_Instance__c).add(paclone.AxtriaSalesIQTM__Account__c);
                }
                else
                {
                  Set<Id> tempSet = new Set<Id>();
                  tempSet.add(paclone.AxtriaSalesIQTM__Account__c);
                  mapTI2WinAcc.put(paclone.AxtriaSalesIQTM__Team_Instance__c,tempSet);
                }
                //mapTI2WinAcc.put(paclone.AxtriaSalesIQTM__Team_Instance__c,paclone.AxtriaSalesIQTM__Account__c);
            }
            else{
                  if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                    pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                  if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                    pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                  
                  updateposacc.add(pa);
                }
          }
        }
      
    }
    

  /*  for(Position_Account__c pa :winPositionAccount){
      String acc = pa.Account__c;
      //if(x.containsKey(acc)){
        //pa.Acc_Pos__c=x.get(acc);
        updateposacc.add(pa);
      //}
    }*/

    //system.debug('====deleteposacc==='+deleteposacc);
    system.debug('====updateposacc==='+updateposacc);
   // system.debug('====updateacc==='+updateacc);

    //delete deleteposacc;
       
    if(insertposacc.size()>0){
    //update updateposacc;
    insert insertposacc;
    }

    System.debug('=======Affiliation Assignment for Winner Account========');
    Set<Id> accWin = new Set<Id>();
    //Set<Id> i = new Set<Id>();
    //List<AxtriaSalesIQTM__Position_Account__c> affWin=[Select Id,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN: setAcc and AxtriaSalesIQTM__Team_Instance__c in :setTI];
    if(mapTI2WinAcc.size() != 0){
      for( Id teaminstn : mapTI2WinAcc.keySet()){
        //Set<Id> acc = new Set<Id>();
        //Id ti = winPA.AxtriaSalesIQTM__Team_Instance__c;
        System.debug('=====Team Instance for winner====' + teaminstn);
        accWin = mapTI2WinAcc.get(teaminstn);
        System.debug('=====Account to be processed for winner====' + accWin);
        //i = accWin;
      AxtriaSalesIQTM.ProcessAffiliationRulesBatch obj = new AxtriaSalesIQTM.ProcessAffiliationRulesBatch(teaminstn,accWin);
      Database.executeBatch(obj,200);
    }
  }
                             
   // update updateacc;
       
      //  
    List<AxtriaSalesIQTM__Position_Account__c> winPaList=[Select Id,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:winningset order by createddate asc];
    map<String,String> winPAmap=new map<String,String>();
    for(AxtriaSalesIQTM__Position_Account__c pa:winPaList){
      if(!winPAmap.containsKey(pa.AxtriaSalesIQTM__Account__c)){
      winPAmap.put(pa.AxtriaSalesIQTM__Account__c,pa.Id);
    }
    }

    AffiliationMergeUnmerge affmergeclass = new AffiliationMergeUnmerge(loosingset);
    if(updateposacc.size() != 0)
      update updateposacc;
    System.debug('======Affiliation Handling For looser Account=======');  
     
     
	  System.debug('===========Call Plan Handling============');   
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>LosCallPlan = [SELECT Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Account__r.AccountNumber,Party_ID__c,Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c,Account_HCP_NUmber__c,AxtriaSalesIQTM__Account__c,Acc_Pos__c,Brand_Name__c,Final_TCF__c,Segment__c,P1__c,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__isincludedCallPlan__c,AxtriaSalesIQTM__Position__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Position__c!=null and Party_ID__c!=null and Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c!=null and Party_ID__r.AxtriaSalesIQTM__Position__c!=null and Party_ID__r.AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__isincludedCallPlan__c = TRUE and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :countryCode];
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>winCallPlan = [SELECT Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Account__r.AccountNumber,Party_ID__c,Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c,Account_HCP_NUmber__c,AxtriaSalesIQTM__Account__c,Acc_Pos__c,Brand_Name__c,Final_TCF__c,Segment__c,P1__c,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__isincludedCallPlan__c,AxtriaSalesIQTM__Position__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:winningset and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Position__c!=null and Party_ID__c!=null and Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c!=null and Party_ID__r.AxtriaSalesIQTM__Position__c!=null and Party_ID__r.AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__isincludedCallPlan__c = TRUE and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :countryCode];
    list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>updatecallplan = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>insertcallplan = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    //list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>deletecallplan = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    
    map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c> LosCallPlanMap=new map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    map<String,Decimal> TCFmap=new map<String,Decimal>();
    map<String,String> Segmap=new map<String,String>();
    map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c> winCallMap=new map<String,AxtriaSalesIQTM__Position_Account_Call_Plan__c> ();
    system.debug('==========WINNING CALL PLAN LIST:'+winCallPlan);
    if(winCallPlan != null && winCallPlan.size() >0){
        system.debug('==========WINNING CALL PLAN RECS EXIST+================');
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa :winCallPlan){
            winCallMap.put(pa.AxtriaSalesIQTM__Account__c,pa);
            winninguniq = new set<String>();
            if(!TCFmap.containsKey(pa.AxtriaSalesIQTM__Account__c+'_'+pa.P1__c)){
              TCFmap.put(pa.AxtriaSalesIQTM__Account__c+'_'+pa.P1__c,pa.Final_TCF__c);
            }
            if(!Segmap.containsKey(pa.AxtriaSalesIQTM__Account__c+'_'+pa.P1__c)){
              Segmap.put(pa.AxtriaSalesIQTM__Account__c+'_'+pa.P1__c,pa.Segment__c);
            }
            winninguniq.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Position__c+'_'+pa.P1__c);  
          }
    }
      else{
        system.debug('==================WINNING CALL PLAN RECS DOES NOT EXIST===========');
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa :LosCallPlan){
            system.debug('============ASSIGNMENT TYPE::'+pa.Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c);
           if(pa.Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c.contains('Explicit')){
                system.debug('==========NOT SINGLE WINNING ACCOUNT EXIST CLONNING THE LOSER PACP==========');
                AxtriaSalesIQTM__Position_Account_Call_Plan__c paclone = pa.clone();
                String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
                paclone.AxtriaSalesIQTM__Account__c = winacc;
                paclone.Party_ID__c=winPAmap.get(winacc);                                    
                System.debug('=============CALL PLAN CLONE when no call plan exist for Winner :::'+paclone);
                System.debug('Hey no call plan Clone');
                pa.AxtriaSalesIQTM__isAccountTarget__c=false;
                pa.AxtriaSalesIQTM__isincludedCallPlan__c=false;
                pa.AxtriaSalesIQTM__segment10__c='Inactive';
                pa.AxtriaSalesIQTM__lastApprovedTarget__c=false;
                if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;

                }
                else{
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                }
                updatecallplan.add(pa);
                system.debug('===========0th Loop::'+updatecallplan);
                insertcallplan.add(paclone);
        }
      }
    }
    system.debug(TCFmap);
    system.debug(Segmap);
    System.debug('pacp loosing'+LosCallPlan);
    System.debug('pacp wining'+winCallPlan );
    /* Customer assignment-- Direct assignment Explicit--direct assignment*/
    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa :LosCallPlan){
        if(!LosCallPlanMap.containsKey(pa.AxtriaSalesIQTM__Account__c+'_'+pa.P1__c)){
          LosCallPlanMap.put(pa.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pa.P1__c,pa);
        }
      if(pa.Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c.equals('Implicit')){
        String Pos = pa.AxtriaSalesIQTM__Position__c;
        String Brand=pa.P1__c;
        String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
        String Key = winacc+'_'+Pos+'_'+Brand;
        // if(winninguniq.contains(Key)){
        pa.AxtriaSalesIQTM__isTarget__c=false;
        pa.AxtriaSalesIQTM__isincludedCallPlan__c=false;
        pa.AxtriaSalesIQTM__segment10__c='Inactive';
        pa.AxtriaSalesIQTM__lastApprovedTarget__c=false;
        if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
          pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
        }
        else{
          pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
        }
        updatecallplan.add(pa);
        system.debug('===========1 Loop::'+updatecallplan);
        //deletecallplan.add(pa);
        //  }else{
        /*   pa.AxtriaSalesIQTM__Account__c = winacc;
        if(Segmap.get(winacc+'_'+Brand)!=null){
        pa.Segment__c=Segmap.get(winacc+'_'+Brand);
                                                          

         pa.AxtriaSalesIQTM__segment10__c='Loser Account';
         }                                                
        updatecallplan.add(pa);
        }*/
      }
      else if(pa.Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c.contains('Explicit')){
		 
		
        String Pos = pa.AxtriaSalesIQTM__Position__c;
        String Brand=pa.P1__c;
        String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
        String Key = winacc+'_'+Pos+'_'+Brand;
        System.debug('===== win key======' + key);
        System.debug('===Winning Account=====' + winninguniq);
        System.debug('===call plan key check=====' +Key);

        if(winninguniq.contains(Key)){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c tempWinacc= winCallMap.get(winacc);
            System.debug('Hey no call plan Clone');
            if(tempWinacc.Segment__c==null){
               tempWinacc.Segment__c=pa.Segment__c;
             }
            if(tempWinacc.Final_TCF__c==null){
               tempWinacc.Final_TCF__c=pa.Final_TCF__c;
            }
            pa.AxtriaSalesIQTM__isAccountTarget__c=false;
            pa.AxtriaSalesIQTM__isincludedCallPlan__c=false;
            pa.AxtriaSalesIQTM__segment10__c='Inactive';
            pa.AxtriaSalesIQTM__lastApprovedTarget__c=false;
            if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
            pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
            tempWinacc.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
            tempWinacc.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;

            }
            else{
            pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
            tempWinacc.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
            tempWinacc.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
            }
            updatecallplan.add(pa);
            system.debug('===========2 Loop::'+updatecallplan);
            System.debug('======tempWinacc======' +tempWinacc);
            updatecallplan.add(tempWinacc);
            System.debug('======3 Loop::=========' +updatecallplan);
            //  deletecallplan.add(pa);
        }else{
          
          if(!winningaccmap.containsKey(winacc)){
                system.debug('==========NO WINNING ACCOUNT EXIST CLONNING THE LOSER PACP==========');
                AxtriaSalesIQTM__Position_Account_Call_Plan__c paclone = pa.clone();
                paclone.AxtriaSalesIQTM__Account__c = winacc;
                paclone.Party_ID__c=winPAmap.get(winacc);                                    
                /* if(Segmap.get(winacc+'_'+Brand)!=null){
                   paclone.Segment__c=Segmap.get(winacc+'_'+Brand);
                 }
                 if(TCFmap.get(winacc+'_'+Brand)!=null){
                   paclone.Final_TCF__c=TCFmap.get(winacc+'_'+Brand);
                 }*/
                  //paclone.AxtriaSalesIQTM__segment10__c='Winner Account';
                system.debug('=============CALL PLAN CLONE BY AYUSHI :::'+paclone);
                System.debug('Hey no call plan Clone');
                pa.AxtriaSalesIQTM__isAccountTarget__c=false;
                pa.AxtriaSalesIQTM__isincludedCallPlan__c=false;
                pa.AxtriaSalesIQTM__segment10__c='Inactive';
                pa.AxtriaSalesIQTM__lastApprovedTarget__c=false;
                if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current'){
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                }
                else{
                  pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                  paclone.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                  paclone.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                }
                updatecallplan.add(pa);
                system.debug('===========4 Loop::'+updatecallplan);
                insertcallplan.add(paclone);
            }
            /*else{
                  if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')  
                     pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                  
                  if(pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                     pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                  
                updatecallplan.add(pa);
                system.debug('===========5 Loop::'+updatecallplan);
                }*/
            }
        }
      }
    
  //  delete deletecallplan;
   // update updatecallplan;
      system.debug('==========SIZE OF updatecallplan::'+updatecallplan.size());
      system.debug('==========SIZE OF insertcallplan::'+insertcallplan.size());
        
    
    //Commented thie below code 
     for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa :winCallPlan){
       if(pa.Party_ID__r.AxtriaSalesIQTM__Account_Alignment_Type__c.equals('Implicit')){
         if(pa.Segment__c==null){
            String loserAcc=winlosuniqmap.get(pa.AxtriaSalesIQTM__Account__r.AccountNumber);
            String key=loserAcc+'_'+pa.P1__c;
         if(LosCallPlanMap.containsKey(key)){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c loserCallPlan=LosCallPlanMap.get(key);
            pa.Segment__c=loserCallPlan.Segment__c;
          updatecallplan.add(pa);
          system.debug('===========6 Loop::'+updatecallplan);
          }
        }
       }
    }

    if(updatecallplan.size() > 0){
      /*set<string>uniq = new set<string>();
      list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>newupdatelist = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
      for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa : updatecallplan){
        if(!uniq.contains(pa.id)){
          uniq.add(pa.id);
          newupdatelist.add(pa);

        }
      }
      if(newupdatelist!=null && newupdatelist.size()>0){
        update newupdatelist;
      } */
      update updatecallplan;
    }

    if(insertcallplan.size() > 0) 
       insert insertcallplan;
    //and Position__r.Team_Instance__r.Alignment_Period__c='Current'
     

     /*System.debug('========Account Type HCA or HCP=======');*/

     System.debug('========Account Address Handling========');
     //Change By Ayushi
  
        List <AxtriaSalesIQTM__Account_Address__c> LosAccAdd = new List <AxtriaSalesIQTM__Account_Address__c>();                                                                                                                                                                                                                   
        List<AxtriaSalesIQTM__Account_Address__c> LosAccountAddress = [select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,Status__c from AxtriaSalesIQTM__Account_Address__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset ];                                                                                                                                                                                                                 
        for(AxtriaSalesIQTM__Account_Address__c accAdd : LosAccountAddress){
         accAdd.Status__c = 'Inactive';
         LosAccAdd.add(accAdd);
      }  
      update LosAccAdd;
       
      //till here..




     /*List<AxtriaSalesIQTM__Account_Affiliation__c>LosAccAff = [SELECT AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Status__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,Id,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.External_HCP_No__c IN:loosingset ];
     List<AxtriaSalesIQTM__Account_Affiliation__c>winAccAff = [SELECT AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Status__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,Id,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.External_HCP_No__c IN:winningset ];
     map <String,String> ActiveAcc=new map<String,String>();
      for(AxtriaSalesIQTM__Account_Affiliation__c pa :winAccAff){
        winninguniq = new set<String>();
        winninguniq.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Parent_Account__c);
        ActiveAcc.put(pa.AxtriaSalesIQTM__Account__c,pa.AxtriaSalesIQTM__Account__r.Status__c);  
        }
    list<AxtriaSalesIQTM__Account_Affiliation__c>updateaffil = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
    list<AxtriaSalesIQTM__Account_Affiliation__c>deleteaffil = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
    
    for(AxtriaSalesIQTM__Account_Affiliation__c pa :LosAccAff){
      String parentAcc = pa.AxtriaSalesIQTM__Parent_Account__c;
      String winacc = uniqmap.get(pa.AxtriaSalesIQTM__Account__c);
      String Key = winacc+'_'+parentAcc;
      if(winninguniq.contains(Key)){
        if(ActiveAcc.get(pa.AxtriaSalesIQTM__Account__c).equals('Active')){
          pa.AxtriaSalesIQTM__Parent_Account__c='';
          updateaffil.add(pa);
        }
      }else{
        pa.AxtriaSalesIQTM__Account__c = winacc;
        updateaffil.add(pa);
      }
    }*/
    //update updateaffil;
    list<Account>updateAclist = new list<Account>();
    system.debug('=======lossingaccounts::::'+lossingaccounts);
    if(lossingaccounts!=null &&  lossingaccounts.size()>0){
      for(string sfdcid : lossingaccounts){

        Account acc = new Account(id=sfdcid);
        system.debug('==========DEACTIVATING ACCOUNTS::'+acc);
        acc.Status__c = 'Inactive';
        acc.AxtriaSalesIQTM__Active__c = 'Inactive';
        acc.Active__c = 'Inactive';
        updateAclist.add(acc);
      }

      if(updateAclist!=null && updateAclist.size()>0)
         Update updateAclist;
    }

  }
    
}