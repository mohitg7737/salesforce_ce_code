/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 25th September'2020
Description : Test class for PopulatelookupController
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class PopulatelookupControllerTest 
{
    static testMethod void firstTest() 
    {
        String className = 'PopulatelookupControllerTest';

        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(aom,className);

        /*AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'ChangeRequestTrigger';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(ccd,className); */       
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active',AxtriaSalesIQTM__Country_Code__c='IT');
        SnTDMLSecurityUtil.insertRecords(country,className);
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        SnTDMLSecurityUtil.insertRecords(ti,className);  

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(ti, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);   

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);   
        /*Data creation done*/

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        PopulatelookupController pop = new PopulatelookupController();

        pop.getopenload();
        pop.ShowPopup();
        pop.ClosePopup();
        pop.checkBatchStatus();
        pop.errorFileType();

        pop.columnsHeaderString = 'Metric1__c,Metric2__c';
        pop.showMappingPopup();
        pop.submitFile();
        pop.refresh();

        pop.selectedWSID = 'All';
        pop.teaminsfetch();

        pop.selectedTeamIns = 'All';
        pop.setJunctionList();

        pop.selectedWSID = workspace.Id;
        pop.selectedTeamIns = teamins.Id;
        pop.setJunctionList();

        /*Populate from Temp obj*/
        pop.selectedJuncObj = 'Acc_Terr';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Position_Employee';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Call_Plan';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Accessibility';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Product and portfolio';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Survey';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Position Product';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Assign Product Type';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Populate HCP Metrics';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Populate Zip-Terr Metrics';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Populate CIM Configuration';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Populate Employee';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'Channel Preference';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'MCCP Channel Preference';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'MCCP Channel Consent';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'MCCP Product Segment';
        pop.populateJuncObj();

        pop.selectedJuncObj = 'MCCP Channel Consent 2';
        pop.populateJuncObj();

        pop.selectedJuncObj = '';
        pop.populateJuncObj();

        pop.mapping();

        System.Test.stopTest();
    }

    static testMethod void secondTest() 
    {
        String className = 'PopulatelookupControllerTest';

        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(aom,className);

        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'ChangeRequestTrigger';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(ccd,className);        
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active',AxtriaSalesIQTM__Country_Code__c='IT');
        SnTDMLSecurityUtil.insertRecords(country,className);
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        SnTDMLSecurityUtil.insertRecords(ti,className);  

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(ti, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);   

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);   
        /*Data creation done*/

        //Mock Custom metadata
        SalesIQ_Direct_Load__mdt rec = new SalesIQ_Direct_Load__mdt();
        Map<String,Object> mapFields = (Map<String,Object>)JSON.deserializeUntyped(JSON.serializePretty(rec));
        mapFields.put(SalesIQ_Direct_Load__mdt.Label.getDescribe().getName(),'SourceDest');
        mapFields.put(SalesIQ_Direct_Load__mdt.DeveloperName.getDescribe().getName(),'SourceDest');
        mapFields.put(SalesIQ_Direct_Load__mdt.Interface_Class_Name__c.getDescribe().getName(),'SourceDest');
        mapFields.put(SalesIQ_Direct_Load__mdt.Method_Name__c.getDescribe().getName(),'SourceDest');
        rec = (SalesIQ_Direct_Load__mdt)JSON.deserialize(JSON.serialize(mapFields),SalesIQ_Direct_Load__mdt.class);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        PopulatelookupController pop = new PopulatelookupController();

        pop.columnsHeaderString = 'Metric1__c,Metric2__c,Checkbox1__c';
        pop.showMappingPopup();

        pop.selectedWSID = 'All';
        pop.teaminsfetch();

        pop.selectedWSID = workspace.Id;
        pop.selectedTeamIns = teamins.Id;
        pop.setJunctionList();

        pop.headerList = new List<HeaderFieldMappingWrapper>();

        HeaderFieldMappingWrapper headerMap;
        headerMap = new HeaderFieldMappingWrapper();
        headerMap.selField = 'metric1__c';
        pop.headerList.add(headerMap);

        headerMap = new HeaderFieldMappingWrapper();
        headerMap.selField = 'metric2__c';
        pop.headerList.add(headerMap);

        headerMap = new HeaderFieldMappingWrapper();
        headerMap.selField = 'checkbox1__c';
        pop.headerList.add(headerMap);

        /*Populate from Temp obj file*/
        Attachment att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Zip_Terr';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Acc_Terr';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Position_Employee';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Call_Plan';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Accessibility';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Product and portfolio';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Survey';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0\r\n2,');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Position Product';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Assign Product Type';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Populate HCP Metrics';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Populate Zip-Terr Metrics';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Populate Employee';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Channel Preference';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'MCCP Channel Preference';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'MCCP Channel Consent';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'MCCP Product Segment';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'MCCP Channel Consent 2';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'Populate CIM Configuration';
        pop.insertTempRecs();

        att = new Attachment();
        att.Name = 'Test_attach';
        att.ParentId = aom.Id;
        att.Body = Blob.valueOf('1,0,TRUE\r\n2,0,TRUE');
        insert att;
        pop.listAttachmentString = att.Id;
        pop.selectedJuncObj = 'SourceDest';
        pop.insertTempRecs();

        System.Test.stopTest();
    }

    static testMethod void thirdTest() 
    {
        String className = 'PopulatelookupControllerTest';

        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(aom,className);

        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'ChangeRequestTrigger';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(ccd,className);        
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active',AxtriaSalesIQTM__Country_Code__c='IT');
        SnTDMLSecurityUtil.insertRecords(country,className);
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        SnTDMLSecurityUtil.insertRecords(ti,className);  

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(ti, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);   

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);   
        /*Data creation done*/

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        PopulatelookupController pop = new PopulatelookupController();

        pop.columnsHeaderString = 'Metric1__c,Metric2__c';
        pop.showMappingPopup();

        pop.selectedWSID = 'All';
        pop.teaminsfetch();

        pop.selectedWSID = workspace.Id;
        pop.selectedTeamIns = teamins.Id;
        pop.setJunctionList();

        /*Preview process recs*/
        pop.check = 'true';

        cr.Object_Name__c = 'Populate CIM Configuration';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Geography to Position Mapping';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Account to Position Mapping';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Employee To Position';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Call Plan';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Accessibility';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Product and portfolio';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Position Product';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Populate HCP Metrics';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Populate Zip-Terr Metrics';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Populate Employee';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Channel Preference';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'MCCP Channel Preference';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'MCCP Channel Consent';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'MCCP Product Segment';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = 'Populate CIM Configuration 22';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();

        cr.Object_Name__c = '';
        SnTDMLSecurityUtil.updateRecords(cr,className);
        pop.ruleId = cr.Id;
        pop.downloadReport();
        String headerField= 'Col1';
        String selField = 'value1';
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('value1','value1'));
        HeaderFieldMappingWrapper obj = new HeaderFieldMappingWrapper(headerField,selField,options);

        System.Test.stopTest();
    }
}