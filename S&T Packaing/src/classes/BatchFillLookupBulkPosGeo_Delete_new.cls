global with sharing class BatchFillLookupBulkPosGeo_Delete_new implements Database.Batchable<sObject>, Database.Stateful  {
     String query;
     String CountryName;
     String key;
     Map<String,String> KeyIdMap;
     Set<String> processedrecordSet;

    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/
     global BatchFillLookupBulkPosGeo_Delete_new(String Country){
             }
    
     global Database.Querylocator start(Database.BatchableContext bc){
          return Database.getQueryLocator(query);
        
     } 
    
     global void execute (Database.BatchableContext BC, List<temp_Obj__c>scope){
   
          
     }
    
     global void finish(Database.BatchableContext BC){
     }
}