public class Scenrio_TeamInstance_Past{

    public list<selectoption> clist{get;set;}
    public string counMas{get;set;}
    public string workSpaceLi{get;set;}
    public list<selectoption> options{get;set;}
    public string ScenarioLive{get;set;}
    public list<AxtriaSalesIQTM__Scenario__c > Scen{get;set;}
    public list<AxtriaSalesIQTM__Team_Instance__c> TI{get;set;}
    
    public list<AxtriaSalesIQTM__Scenario__c > LiveScen{get;set;}
    public list<AxtriaSalesIQTM__Team_Instance__c> LiveTI{get;set;}
    
    public Scenrio_TeamInstance_Past(){
        options= new list<selectoption>();
        list<AxtriaSalesIQTM__Country__c> countrylist = [select id,name from AxtriaSalesIQTM__Country__c];
        options.add(new selectoption('—-Select Anyone—-', '—-Select Any Country—-'));
            for(AxtriaSalesIQTM__Country__c country : countrylist){
            options.add(new selectoption(country.id,country.name));
            }
        }
        
     
     public void workspacelistlist(){
      list<AxtriaSalesIQTM__Workspace__c> con = [select id,name from AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__r.id =:counMas];
       clist = new list<selectoption>();
       clist.add(new selectoption('—-Select Anyone—-', '—-Select Any Workspace—-'));
      for(AxtriaSalesIQTM__Workspace__c ca :con){
            clist.add(new selectoption(ca.id,ca.name));
              }
    }
    
       public void workspacelistlist2(){
      list<AxtriaSalesIQTM__Workspace__c> con = [select id,name from AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__r.id =:counMas];
       clist = new list<selectoption>();
       clist.add(new selectoption('—-Select Anyone—-', '—-Select Any Workspace—-'));
      for(AxtriaSalesIQTM__Workspace__c ca :con){
            clist.add(new selectoption(ca.id,ca.name));
              }
    }
    
    
    public void cliston(){
    Scen = new list<AxtriaSalesIQTM__Scenario__c >();
    TI = new list<AxtriaSalesIQTM__Team_Instance__c>();
    
    LiveScen = new list<AxtriaSalesIQTM__Scenario__c >();
    LiveTI = new list<AxtriaSalesIQTM__Team_Instance__c>();
    
    Scen= [SELECT Id,Name,AxtriaSalesIQTM__Scenario_Stage__c FROM AxtriaSalesIQTM__Scenario__c WHERE AxtriaSalesIQTM__Scenario_Stage__c ='Live' AND AxtriaSalesIQTM__Workspace__c = :workSpaceLi];
  
        for(AxtriaSalesIQTM__Scenario__c SS:Scen){
           SS.AxtriaSalesIQTM__Scenario_Stage__c = 'Past';
         }
           
     update Scen;
  
   LiveScen= [SELECT Id,Name,AxtriaSalesIQTM__Scenario_Stage__c FROM AxtriaSalesIQTM__Scenario__c WHERE AxtriaSalesIQTM__Scenario_Stage__c ='Published' AND AxtriaSalesIQTM__Workspace__c = :ScenarioLive];
  
        for(AxtriaSalesIQTM__Scenario__c SS1:LiveScen){
           SS1.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
         }
           
     update LiveScen;
  
  
  TI = [SELECT AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Scenario__c,Id FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' AND AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c = :workSpaceLi];
     
     for(AxtriaSalesIQTM__Team_Instance__c  TT :TI) {
        TT.AxtriaSalesIQTM__Alignment_Period__c='Past';
      }
      
    update TI;  
    
    
     LiveTI = [SELECT AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Scenario__c,Id,AxtriaSalesIQTM__AccountMovement__c,
                    AxtriaSalesIQTM__Allow_Account_Exclusion__c,
                    AxtriaSalesIQTM__Allow_Submission_Pending__c,
                    AxtriaSalesIQTM__Allow_Unassigned_Positions__c,
                    AxtriaSalesIQTM__CIM_Available__c,
                    AxtriaSalesIQTM__doContiguityCheck__c,
                    AxtriaSalesIQTM__doDoughnutCheck__c,
                    AxtriaSalesIQTM__EnableAccountSharing__c,
                    AxtriaSalesIQTM__Enable_Affiliation_Movement__c,
                    AxtriaSalesIQTM__IsAllowedManageAssignment__c ,
                    AxtriaSalesIQTM__IsCascadeDeleteAssignments__c,
                    AxtriaSalesIQTM__ReadOnlyCreatePosition__c,
                    AxtriaSalesIQTM__ReadOnlyDeletePosition__c,
                    AxtriaSalesIQTM__ReadOnlyEditPosition__c,
                    AxtriaSalesIQTM__Restrict_Account_Affiliation__c,
                    AxtriaSalesIQTM__Restrict_Hierarchy_Change__c,
                    AxtriaSalesIQTM__Restrict_Update_Position_Attribute__c,
                    AxtriaSalesIQTM__Restrict_ZIP_Share__c,
                    AxtriaSalesIQTM__Send_Back_to_Revision__c,
                    AxtriaSalesIQTM__Show_Custom_Metric__c,
                    AxtriaSalesIQTM__Show_Map__c,
                    AxtriaSalesIQTM__ZIPMovement__c FROM AxtriaSalesIQTM__Team_Instance__c where  AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c = :ScenarioLive];
     
     for(AxtriaSalesIQTM__Team_Instance__c  TT1 :LiveTI) {
        TT1.AxtriaSalesIQTM__Alignment_Period__c='Current';
        TT1.AxtriaSalesIQTM__AccountMovement__c = false;
        TT1.AxtriaSalesIQTM__Allow_Account_Exclusion__c = true;
        TT1.AxtriaSalesIQTM__Allow_Submission_Pending__c = false;
        TT1.AxtriaSalesIQTM__Allow_Unassigned_Positions__c = true;
        TT1.AxtriaSalesIQTM__CIM_Available__c = false;
        TT1.AxtriaSalesIQTM__doContiguityCheck__c = false;
        TT1.AxtriaSalesIQTM__doDoughnutCheck__c =false;
        TT1.AxtriaSalesIQTM__EnableAccountSharing__c = true;
        TT1.AxtriaSalesIQTM__Enable_Affiliation_Movement__c =true;
        TT1.AxtriaSalesIQTM__IsAllowedManageAssignment__c  = false;
        TT1.AxtriaSalesIQTM__IsCascadeDeleteAssignments__c =true;
        TT1.AxtriaSalesIQTM__ReadOnlyCreatePosition__c = false;
        TT1.AxtriaSalesIQTM__ReadOnlyDeletePosition__c = false;
        TT1.AxtriaSalesIQTM__ReadOnlyEditPosition__c = false;
        TT1.AxtriaSalesIQTM__Restrict_Account_Affiliation__c = false;
        TT1.AxtriaSalesIQTM__Restrict_Hierarchy_Change__c = false;
        TT1.AxtriaSalesIQTM__Restrict_Update_Position_Attribute__c =false;
        TT1.AxtriaSalesIQTM__Restrict_ZIP_Share__c = false;
        TT1.AxtriaSalesIQTM__Send_Back_to_Revision__c = false;
        TT1.AxtriaSalesIQTM__Show_Custom_Metric__c = false;
        TT1.AxtriaSalesIQTM__Show_Map__c = false;
        TT1.AxtriaSalesIQTM__ZIPMovement__c = false;

      }
      
    update LiveTI;
   }
   
}