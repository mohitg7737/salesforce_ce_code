//Changes done By Ayush Rastogi
//Removed occurances of business unit due to object purge activity A1422
@isTest
public class All_BU_Response_Insert_UtilityTest {
    public static List < AxtriaSalesIQTM__Team_Instance__c > teamInstaneList;
    //public static List < Brand_Team_Instance__c > brandTeamInstanceList;
    public static TestMethod void AllBUResponse() {

        Account acc = TestDataFactory.createAccount();
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        /*Business_Unit__c bu = new     Business_Unit__c();
        bu.Name = 'Test';
        bu.CurrencyIsoCode = 'EUR';
        insert bu;*/
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        //teamInstaneList = new List <AxtriaSalesIQTM__Team_Instance__c> ();

        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = c.id;
        teamins.Include_Secondary_Affiliations__c= true;
        insert teamins;
        System.assertEquals(True, teamins.Include_Secondary_Affiliations__c);
        System.assertEquals(True, teamins.Include_Territory__c);

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'abc';
        pos.CurrencyIsoCode = 'USD';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        posAcc.CurrencyIsoCode = 'USD';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = system.today() - 1;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 6;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAcc;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=teamins.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=countr.id);
        insert pc;
        

        //product catlogue
        //
        /*Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        brandTeamInstanceList = new List < Brand_Team_Instance__c > ();
        br.Name = 'fgh';
        br.CurrencyIsoCode = 'USD';
        br.Team_Instance__c = teamins.id;*/

        /*brandTeamInstanceList.add(br);
        insert brandTeamInstanceList;*/
        // commented due to object purge activity A1422
        /*Error_Object__c eo = new Error_Object__c();
            eo.Name = 'error1';
        eo.CurrencyIsoCode = 'EUR';
        insert eo;
*/

        /*Profiling_Response__c pr = new    Profiling_Response__c();
        pr.Name = 'pr12';
        pr.CurrencyIsoCode = 'EUR';
        insert pr;*/

        /*Survey_Master__c sm = new Survey_Master__c();
        sm.Name = 'sr23';
        sm.CurrencyIsoCode = 'EUR';
        sm.Brand_Team_Instance__c = br.id;
        //sm.Profiling_Response__c = pr.id;//object purge
        insert sm;*/


        Staging_BU_Response__c sbu = new Staging_BU_Response__c();
        sbu.CurrencyIsoCode = 'USD';
        sbu.Type__c = 'HCO';
        //sbu.Cycle2__c = c.id;
        sbu.Product_Catalog__c = pc.id;
        //sbu.Business_Unit__c = bu.id;
        sbu.Team_Instance__c = teamins.Id;
        sbu.External_ID__c = 'afgh';
        sbu.Team__c = team.id;
        sbu.Physician__c = acc.id;
        sbu.Position_Account__c = posAcc.id;
        //sbu.Profiling_Response__c = pr.id;//object purge
        //sbu.Survey_Master__c = sm.id;
        sbu.Line__c = team.id;
        sbu.External_ID__c = '2345';
        sbu.Response1__c = '';
        sbu.Response10__c = '';
        sbu.Response2__c = '';
        sbu.Response3__c = '';
        sbu.Response4__c = '';
        sbu.Response5__c = '';
        sbu.Response6__c = '';
        sbu.Response7__c = '';
        sbu.Response8__c = '';
        sbu.Response9__c = '';

        insert sbu;

        Staging_BU_Response__c sbu1 = new Staging_BU_Response__c();
        sbu1.CurrencyIsoCode = 'USD';
        sbu1.Type__c = 'HCP';
        //sbu1.Cycle2__c = c.id;
        sbu1.Product_Catalog__c = pc.id;
        //sbu1.Business_Unit__c = bu.id;
        sbu1.Team_Instance__c = teamins.id;
        sbu1.External_ID__c = 'afgh';
        sbu1.Team__c = team.id;
        sbu1.Physician__c = acc.id;
        sbu1.Position_Account__c = posAcc.id;
        //sbu1.Profiling_Response__c = pr.id;//object purge
        //sbu1.Survey_Master__c = sm.id;
        sbu1.Line__c = team.id;
        sbu1.External_ID__c = 'wert4564';
        sbu1.Response1__c = '1233';
        sbu1.Response10__c = 'egtt1';
        sbu1.Response2__c = 'lop2';
        sbu1.Response3__c = 'plpl3';
        sbu1.Response4__c = 'lkjk1';
        sbu1.Response5__c = 'dfg2';
        sbu1.Response6__c = 'nmnm1';
        sbu1.Response7__c = 'kl2';
        sbu1.Response8__c = 'opjlj2';
        sbu1.Response9__c = 'lkljk2';
        insert sbu1;
        update sbu1;

        Test.startTest();
        All_BU_Response_Insert_Utility Alr = new All_BU_Response_Insert_Utility(teamIns.Name);
        Database.executeBatch(Alr);
        All_BU_Response_Insert_Utility Alr1 = new All_BU_Response_Insert_Utility(teamIns.Name + ';test', 'test');
        Database.executeBatch(Alr1);
        All_BU_Response_Insert_Utility Alr2 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c);
        Database.executeBatch(Alr2);
        
        All_BU_Response_Insert_Utility Alrt3 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c, 'Test');
        Database.executeBatch(Alrt3);
        All_BU_Response_Insert_Utility Alrt4 = new All_BU_Response_Insert_Utility(teamIns.Name, 'HCO', True);
        Database.executeBatch(Alrt4);
        Test.StopTest();

    }


    public static TestMethod void AllBUResponse1() {

        Account acc = TestDataFactory.createAccount();
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        /*Business_Unit__c bu = new     Business_Unit__c();
        bu.Name = 'Test';
        bu.CurrencyIsoCode = 'EUR';
        insert bu;*/
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        //teamInstaneList = new List <AxtriaSalesIQTM__Team_Instance__c> ();

        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = c.id;
        teamins.Include_Secondary_Affiliations__c= false;
        teamins.Include_Territory__c = true;
        insert teamins;
        System.assertEquals(False, teamins.Include_Secondary_Affiliations__c);
        System.assertEquals(True, teamins.Include_Territory__c);

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'abc';
        pos.CurrencyIsoCode = 'USD';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        posAcc.CurrencyIsoCode = 'USD';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = system.today() - 1;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 6;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAcc;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=teamins.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=countr.id);
        insert pc;
        

        //product catlogue
        //
        /*Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        brandTeamInstanceList = new List < Brand_Team_Instance__c > ();
        br.Name = 'fgh';
        br.CurrencyIsoCode = 'USD';
        br.Team_Instance__c = teamins.id;*/

        /*brandTeamInstanceList.add(br);
        insert brandTeamInstanceList;*/
        // commented due to object purge activity A1422
        /*Error_Object__c eo = new Error_Object__c();
            eo.Name = 'error1';
        eo.CurrencyIsoCode = 'EUR';
        insert eo;
*/

        /*Profiling_Response__c pr = new    Profiling_Response__c();
        pr.Name = 'pr12';
        pr.CurrencyIsoCode = 'EUR';
        insert pr;*/

        /*Survey_Master__c sm = new Survey_Master__c();
        sm.Name = 'sr23';
        sm.CurrencyIsoCode = 'EUR';
        sm.Brand_Team_Instance__c = br.id;
        //sm.Profiling_Response__c = pr.id;//object purge
        insert sm;*/


        Staging_BU_Response__c sbu = new Staging_BU_Response__c();
        sbu.CurrencyIsoCode = 'USD';
        sbu.Type__c = 'HCO';
        //sbu.Cycle2__c = c.id;
        sbu.Product_Catalog__c = pc.id;
        //sbu.Business_Unit__c = bu.id;
        sbu.Team_Instance__c = teamins.Id;
        sbu.External_ID__c = 'afgh';
        sbu.Team__c = team.id;
        sbu.Physician__c = acc.id;
        sbu.Position_Account__c = posAcc.id;
        //sbu.Profiling_Response__c = pr.id;//object purge
        //sbu.Survey_Master__c = sm.id;
        sbu.Line__c = team.id;
        sbu.External_ID__c = '2345';
        sbu.Response1__c = '';
        sbu.Response10__c = '';
        sbu.Response2__c = '';
        sbu.Response3__c = '';
        sbu.Response4__c = '';
        sbu.Response5__c = '';
        sbu.Response6__c = '';
        sbu.Response7__c = '';
        sbu.Response8__c = '';
        sbu.Response9__c = '';

        insert sbu;

        Staging_BU_Response__c sbu1 = new Staging_BU_Response__c();
        sbu1.CurrencyIsoCode = 'USD';
        sbu1.Type__c = 'HCP';
        //sbu1.Cycle2__c = c.id;
        sbu1.Product_Catalog__c = pc.id;
        //sbu1.Business_Unit__c = bu.id;
        sbu1.Team_Instance__c = teamins.id;
        sbu1.External_ID__c = 'afgh';
        sbu1.Team__c = team.id;
        sbu1.Physician__c = acc.id;
        sbu1.Position_Account__c = posAcc.id;
        //sbu1.Profiling_Response__c = pr.id;//object purge
        //sbu1.Survey_Master__c = sm.id;
        sbu1.Line__c = team.id;
        sbu1.External_ID__c = 'wert4564';
        sbu1.Response1__c = '1233';
        sbu1.Response10__c = 'egtt1';
        sbu1.Response2__c = 'lop2';
        sbu1.Response3__c = 'plpl3';
        sbu1.Response4__c = 'lkjk1';
        sbu1.Response5__c = 'dfg2';
        sbu1.Response6__c = 'nmnm1';
        sbu1.Response7__c = 'kl2';
        sbu1.Response8__c = 'opjlj2';
        sbu1.Response9__c = 'lkljk2';
        insert sbu1;
        update sbu1;

        Test.startTest();
        All_BU_Response_Insert_Utility Alr = new All_BU_Response_Insert_Utility(teamIns.Name);
        Database.executeBatch(Alr);
        All_BU_Response_Insert_Utility Alr1 = new All_BU_Response_Insert_Utility(teamIns.Name + ';test', 'test');
        Database.executeBatch(Alr1);
        All_BU_Response_Insert_Utility Alr2 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c);
        Database.executeBatch(Alr2);
        
        All_BU_Response_Insert_Utility Alrt3 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c, 'Test');
        Database.executeBatch(Alrt3);
        All_BU_Response_Insert_Utility Alrt4 = new All_BU_Response_Insert_Utility(teamIns.Name, 'HCO', True);
        Database.executeBatch(Alrt4);
        Test.StopTest();

    }
    
    public static TestMethod void AllBUResponse2() {

        Account acc = TestDataFactory.createAccount();
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        /*Business_Unit__c bu = new     Business_Unit__c();
        bu.Name = 'Test';
        bu.CurrencyIsoCode = 'EUR';
        insert bu;*/
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        //teamInstaneList = new List <AxtriaSalesIQTM__Team_Instance__c> ();

        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = c.id;
        teamins.Include_Secondary_Affiliations__c= true;
        teamins.Include_Territory__c = false;
        insert teamins;
        System.assertEquals(True, teamins.Include_Secondary_Affiliations__c);
        System.assertEquals(False, teamins.Include_Territory__c);

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'abc';
        pos.CurrencyIsoCode = 'USD';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        posAcc.CurrencyIsoCode = 'USD';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = system.today() - 1;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 6;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAcc;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=teamins.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=countr.id);
        insert pc;
        

        //product catlogue
        //
        /*Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        brandTeamInstanceList = new List < Brand_Team_Instance__c > ();
        br.Name = 'fgh';
        br.CurrencyIsoCode = 'USD';
        br.Team_Instance__c = teamins.id;*/

        /*brandTeamInstanceList.add(br);
        insert brandTeamInstanceList;*/
        // commented due to object purge activity A1422
        /*Error_Object__c eo = new Error_Object__c();
            eo.Name = 'error1';
        eo.CurrencyIsoCode = 'EUR';
        insert eo;
*/

        /*Profiling_Response__c pr = new    Profiling_Response__c();
        pr.Name = 'pr12';
        pr.CurrencyIsoCode = 'EUR';
        insert pr;*/

        /*Survey_Master__c sm = new Survey_Master__c();
        sm.Name = 'sr23';
        sm.CurrencyIsoCode = 'EUR';
        sm.Brand_Team_Instance__c = br.id;
        //sm.Profiling_Response__c = pr.id;//object purge
        insert sm;*/


        Staging_BU_Response__c sbu = new Staging_BU_Response__c();
        sbu.CurrencyIsoCode = 'USD';
        sbu.Type__c = 'HCO';
        //sbu.Cycle2__c = c.id;
        sbu.Product_Catalog__c = pc.id;
        //sbu.Business_Unit__c = bu.id;
        sbu.Team_Instance__c = teamins.Id;
        sbu.External_ID__c = 'afgh';
        sbu.Team__c = team.id;
        sbu.Physician__c = acc.id;
        sbu.Position_Account__c = posAcc.id;
        //sbu.Profiling_Response__c = pr.id;//object purge
        //sbu.Survey_Master__c = sm.id;
        sbu.Line__c = team.id;
        sbu.External_ID__c = '2345';
        sbu.Response1__c = '';
        sbu.Response10__c = '';
        sbu.Response2__c = '';
        sbu.Response3__c = '';
        sbu.Response4__c = '';
        sbu.Response5__c = '';
        sbu.Response6__c = '';
        sbu.Response7__c = '';
        sbu.Response8__c = '';
        sbu.Response9__c = '';

        insert sbu;

        Staging_BU_Response__c sbu1 = new Staging_BU_Response__c();
        sbu1.CurrencyIsoCode = 'USD';
        sbu1.Type__c = 'HCP';
        //sbu1.Cycle2__c = c.id;
        sbu1.Product_Catalog__c = pc.id;
        //sbu1.Business_Unit__c = bu.id;
        sbu1.Team_Instance__c = teamins.id;
        sbu1.External_ID__c = 'afgh';
        sbu1.Team__c = team.id;
        sbu1.Physician__c = acc.id;
        sbu1.Position_Account__c = posAcc.id;
        //sbu1.Profiling_Response__c = pr.id;//object purge
        //sbu1.Survey_Master__c = sm.id;
        sbu1.Line__c = team.id;
        sbu1.External_ID__c = 'wert4564';
        sbu1.Response1__c = '1233';
        sbu1.Response10__c = 'egtt1';
        sbu1.Response2__c = 'lop2';
        sbu1.Response3__c = 'plpl3';
        sbu1.Response4__c = 'lkjk1';
        sbu1.Response5__c = 'dfg2';
        sbu1.Response6__c = 'nmnm1';
        sbu1.Response7__c = 'kl2';
        sbu1.Response8__c = 'opjlj2';
        sbu1.Response9__c = 'lkljk2';
        insert sbu1;
        update sbu1;

        Test.startTest();
        All_BU_Response_Insert_Utility Alr = new All_BU_Response_Insert_Utility(teamIns.Name);
        Database.executeBatch(Alr);
        All_BU_Response_Insert_Utility Alr1 = new All_BU_Response_Insert_Utility(teamIns.Name + ';test', 'test');
        Database.executeBatch(Alr1);
        All_BU_Response_Insert_Utility Alr2 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c);
        Database.executeBatch(Alr2);
        
        All_BU_Response_Insert_Utility Alrt3 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c, 'Test');
        Database.executeBatch(Alrt3);
        All_BU_Response_Insert_Utility Alrt4 = new All_BU_Response_Insert_Utility(teamIns.Name, 'HCO', True);
        Database.executeBatch(Alrt4);
        Test.StopTest();

    }
    
    public static TestMethod void AllBUResponse3() {

        Account acc = TestDataFactory.createAccount();
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        /*Business_Unit__c bu = new     Business_Unit__c();
        bu.Name = 'Test';
        bu.CurrencyIsoCode = 'EUR';
        insert bu;*/
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        //teamInstaneList = new List <AxtriaSalesIQTM__Team_Instance__c> ();

        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = c.id;
        teamins.Include_Secondary_Affiliations__c= false;
        teamins.Include_Territory__c = false;
        insert teamins;
        System.assertEquals(False, teamins.Include_Secondary_Affiliations__c);
        System.assertEquals(False, teamins.Include_Territory__c);

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'abc';
        pos.CurrencyIsoCode = 'USD';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        posAcc.CurrencyIsoCode = 'USD';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = system.today() - 1;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 6;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAcc;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=teamins.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=countr.id);
        insert pc;
        

        //product catlogue
        //
        /*Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        brandTeamInstanceList = new List < Brand_Team_Instance__c > ();
        br.Name = 'fgh';
        br.CurrencyIsoCode = 'USD';
        br.Team_Instance__c = teamins.id;*/

        /*brandTeamInstanceList.add(br);
        insert brandTeamInstanceList;*/
        // commented due to object purge activity A1422
        /*Error_Object__c eo = new Error_Object__c();
            eo.Name = 'error1';
        eo.CurrencyIsoCode = 'EUR';
        insert eo;
*/

        /*Profiling_Response__c pr = new    Profiling_Response__c();
        pr.Name = 'pr12';
        pr.CurrencyIsoCode = 'EUR';
        insert pr;*/

        /*Survey_Master__c sm = new Survey_Master__c();
        sm.Name = 'sr23';
        sm.CurrencyIsoCode = 'EUR';
        sm.Brand_Team_Instance__c = br.id;
        //sm.Profiling_Response__c = pr.id;//object purge
        insert sm;*/


        Staging_BU_Response__c sbu = new Staging_BU_Response__c();
        sbu.CurrencyIsoCode = 'USD';
        sbu.Type__c = 'HCO';
        //sbu.Cycle2__c = c.id;
        sbu.Product_Catalog__c = pc.id;
        //sbu.Business_Unit__c = bu.id;
        sbu.Team_Instance__c = teamins.Id;
        sbu.External_ID__c = 'afgh';
        sbu.Team__c = team.id;
        sbu.Physician__c = acc.id;
        sbu.Position_Account__c = posAcc.id;
        //sbu.Profiling_Response__c = pr.id;//object purge
        //sbu.Survey_Master__c = sm.id;
        sbu.Line__c = team.id;
        sbu.External_ID__c = '2345';
        sbu.Response1__c = '';
        sbu.Response10__c = '';
        sbu.Response2__c = '';
        sbu.Response3__c = '';
        sbu.Response4__c = '';
        sbu.Response5__c = '';
        sbu.Response6__c = '';
        sbu.Response7__c = '';
        sbu.Response8__c = '';
        sbu.Response9__c = '';

        insert sbu;

        Staging_BU_Response__c sbu1 = new Staging_BU_Response__c();
        sbu1.CurrencyIsoCode = 'USD';
        sbu1.Type__c = 'HCP';
        //sbu1.Cycle2__c = c.id;
        sbu1.Product_Catalog__c = pc.id;
        //sbu1.Business_Unit__c = bu.id;
        sbu1.Team_Instance__c = teamins.id;
        sbu1.External_ID__c = 'afgh';
        sbu1.Team__c = team.id;
        sbu1.Physician__c = acc.id;
        sbu1.Position_Account__c = posAcc.id;
        //sbu1.Profiling_Response__c = pr.id;//object purge
        //sbu1.Survey_Master__c = sm.id;
        sbu1.Line__c = team.id;
        sbu1.External_ID__c = 'wert4564';
        sbu1.Response1__c = '1233';
        sbu1.Response10__c = 'egtt1';
        sbu1.Response2__c = 'lop2';
        sbu1.Response3__c = 'plpl3';
        sbu1.Response4__c = 'lkjk1';
        sbu1.Response5__c = 'dfg2';
        sbu1.Response6__c = 'nmnm1';
        sbu1.Response7__c = 'kl2';
        sbu1.Response8__c = 'opjlj2';
        sbu1.Response9__c = 'lkljk2';
        insert sbu1;
        update sbu1;

        Test.startTest();
        All_BU_Response_Insert_Utility Alr = new All_BU_Response_Insert_Utility(teamIns.Name);
        Database.executeBatch(Alr);
        All_BU_Response_Insert_Utility Alr1 = new All_BU_Response_Insert_Utility(teamIns.Name + ';test', 'test');
        Database.executeBatch(Alr1);
        All_BU_Response_Insert_Utility Alr2 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c);
        Database.executeBatch(Alr2);
        
        All_BU_Response_Insert_Utility Alrt3 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c, 'Test');
        Database.executeBatch(Alrt3);
        All_BU_Response_Insert_Utility Alrt4 = new All_BU_Response_Insert_Utility(teamIns.Name, 'HCO', True);
        Database.executeBatch(Alrt4);
        Test.StopTest();

    }
    
        public static TestMethod void AllBUResponse4() {

        Account acc = TestDataFactory.createAccount();
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        insert countr;
        /*Business_Unit__c bu = new     Business_Unit__c();
        bu.Name = 'Test';
        bu.CurrencyIsoCode = 'EUR';
        insert bu;*/
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        //teamInstaneList = new List <AxtriaSalesIQTM__Team_Instance__c> ();

        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = c.id;
        teamins.Include_Secondary_Affiliations__c= false;
        teamins.Include_Territory__c = false;
        insert teamins;
        System.assertEquals(False, teamins.Include_Secondary_Affiliations__c);
        System.assertEquals(False, teamins.Include_Territory__c);

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'abc';
        pos.CurrencyIsoCode = 'USD';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        insert pos;

        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        posAcc.CurrencyIsoCode = 'USD';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = system.today() - 1;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 6;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAcc;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=teamins.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=countr.id);
        insert pc;
        

        //product catlogue
        //
        /*Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        brandTeamInstanceList = new List < Brand_Team_Instance__c > ();
        br.Name = 'fgh';
        br.CurrencyIsoCode = 'USD';
        br.Team_Instance__c = teamins.id;*/

        /*brandTeamInstanceList.add(br);
        insert brandTeamInstanceList;*/
        // commented due to object purge activity A1422
        /*Error_Object__c eo = new Error_Object__c();
            eo.Name = 'error1';
        eo.CurrencyIsoCode = 'EUR';
        insert eo;
*/

        /*Profiling_Response__c pr = new    Profiling_Response__c();
        pr.Name = 'pr12';
        pr.CurrencyIsoCode = 'EUR';
        insert pr;*/

        /*Survey_Master__c sm = new Survey_Master__c();
        sm.Name = 'sr23';
        sm.CurrencyIsoCode = 'EUR';
        sm.Brand_Team_Instance__c = br.id;
        //sm.Profiling_Response__c = pr.id;//object purge
        insert sm;*/


        Staging_BU_Response__c sbu = new Staging_BU_Response__c();
        sbu.CurrencyIsoCode = 'USD';
        sbu.Type__c = 'HCO';
        //sbu.Cycle2__c = c.id;
        sbu.Product_Catalog__c = pc.id;
        //sbu.Business_Unit__c = bu.id;
        sbu.Team_Instance__c = teamins.Id;
        sbu.External_ID__c = 'afgh';
        sbu.Team__c = team.id;
        //sbu.Physician__c = acc.id;
        sbu.Position_Account__c = posAcc.id;
        //sbu.Profiling_Response__c = pr.id;//object purge
        //sbu.Survey_Master__c = sm.id;
        sbu.Line__c = team.id;
        sbu.External_ID__c = '2345';
        sbu.Response1__c = '';
        sbu.Response10__c = '';
        sbu.Response2__c = '';
        sbu.Response3__c = '';
        sbu.Response4__c = '';
        sbu.Response5__c = '';
        sbu.Response6__c = '';
        sbu.Response7__c = '';
        sbu.Response8__c = '';
        sbu.Response9__c = '';

        insert sbu;

        Staging_BU_Response__c sbu1 = new Staging_BU_Response__c();
        sbu1.CurrencyIsoCode = 'USD';
        sbu1.Type__c = 'HCP';
        //sbu1.Cycle2__c = c.id;
        sbu1.Product_Catalog__c = pc.id;
        //sbu1.Business_Unit__c = bu.id;
        sbu1.Team_Instance__c = teamins.id;
        sbu1.External_ID__c = 'afgh';
        sbu1.Team__c = team.id;
        //sbu1.Physician__c = acc.id;
        sbu1.Position_Account__c = posAcc.id;
        //sbu1.Profiling_Response__c = pr.id;//object purge
        //sbu1.Survey_Master__c = sm.id;
        sbu1.Line__c = team.id;
        sbu1.External_ID__c = 'wert4564';
        sbu1.Response1__c = '1233';
        sbu1.Response10__c = 'egtt1';
        sbu1.Response2__c = 'lop2';
        sbu1.Response3__c = 'plpl3';
        sbu1.Response4__c = 'lkjk1';
        sbu1.Response5__c = 'dfg2';
        sbu1.Response6__c = 'nmnm1';
        sbu1.Response7__c = 'kl2';
        sbu1.Response8__c = 'opjlj2';
        sbu1.Response9__c = 'lkljk2';
        insert sbu1;
        update sbu1;

        Test.startTest();
        All_BU_Response_Insert_Utility Alr = new All_BU_Response_Insert_Utility(teamIns.Name);
        Database.executeBatch(Alr);
        All_BU_Response_Insert_Utility Alr1 = new All_BU_Response_Insert_Utility(teamIns.Name + ';test', 'test');
        Database.executeBatch(Alr1);
        All_BU_Response_Insert_Utility Alr2 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c);
        Database.executeBatch(Alr2);
        
        All_BU_Response_Insert_Utility Alrt3 = new All_BU_Response_Insert_Utility(teamIns.Name, teamins.Include_Secondary_Affiliations__c, 'Test');
        Database.executeBatch(Alrt3);
        All_BU_Response_Insert_Utility Alrt4 = new All_BU_Response_Insert_Utility(teamIns.Name, 'HCO', True);
        Database.executeBatch(Alrt4);
        Test.StopTest();

    }


}