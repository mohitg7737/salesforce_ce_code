@isTest
public class UserReport_Test {
    public static testmethod void sample(){
        Profile p = [select id from Profile where name = 'REP1' or name = 'System Administrator' order by name limit 1];
        //Profile p2 = [select id from Profile where name = 'DM1' or name = 'System Administrator' order by name limit 1];
        
        User tUser = new User(Alias = 'Rep', Email='repuser@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, FederationIdentifier ='1234',
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@astrazeneca.com');
        insert tUser;
        
        User tUser1 = new User(Alias = 'Rep', Email='repuser1@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, FederationIdentifier ='12',
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset1@astrazeneca.com');
        insert tUser1; 
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        User tUser2 = new User(Alias = 'Rep', Email='repuser1@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, FederationIdentifier ='345',
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset2@astrazeneca.com');
        
        insert tUser2; 
        
     //   system.runAs(tUser2){
            Group g = new Group();
            g.Name ='SPAINMARKET';
            insert g;
        	StandardPositions__c sc = new StandardPositions__c();
        	sc.Name='DUMMY';
        	sc.Type__c='Standard';
        	sc.Market_Code__c = 'ES';
        	insert sc;
        
        	StandardPositions__c sc2 = new StandardPositions__c();
        	sc2.Name='REP';
        	sc2.Type__c='CRM';
        	sc2.Market_Code__c = 'ES';
        	insert sc2;
       // } 
       
        system.runAs(loggedInUser){
            Group newgrp = [select id from Group limit 1];
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c	 = true;
        orgmas.Public_Group__c = 'SPAINMARKET';
        orgmas.Public_Group_SFDC_id__c = newgrp.id;
        insert orgmas;
		
        list<AxtriaSalesIQTM__Organization_Master__c> arm = [select id,Public_Group_SFDC_id__c from AxtriaSalesIQTM__Organization_Master__c where name='abcd'];
        system.debug('===========orgmas=========='+orgmas);
       // Group gg = [select id from Group where Name='SPAINMARKET'];
        String grpid = arm[0].Public_Group_SFDC_id__c;
        GroupMember gm = new GroupMember();
        gm.GroupId = grpid;
        gm.UserOrGroupId = loggedInUser.id;
        insert gm;
        
        GroupMember gm1 = new GroupMember();
        gm1.GroupId = grpid;
        gm1.UserOrGroupId = tUser.id;
        insert gm1;
        
        GroupMember gm2 = new GroupMember();
        gm2.GroupId = grpid;
        gm2.UserOrGroupId = tUser1.id;
        insert gm2;
        
        //List<GroupMember>Grouplist = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where GroupId=:grpid.id];
        //system.debug('===============Grouplist.SIZE IS:::::'+Grouplist.size());

        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'UK';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
            
        insert countr; 
        }
        
        test.startTest();
        UserReport ur = new UserReport();
        
    }

}