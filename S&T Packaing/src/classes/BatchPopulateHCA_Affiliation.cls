/*
    Purpose-To generate HCA Position accounts if affiliated position accounts does not exists on the same position
    Author:-Siva Gopi
    Date:30-09-2018
*/
global class BatchPopulateHCA_Affiliation implements Database.Batchable<sObject> {
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacplist;
    public list<AxtriaSalesIQTM__Account_Affiliation__c>affiliationlist;
    public list<AxtriaSalesIQTM__Position_Account__c>posacclist;
    public list<temp_Acc_Terr__c>tempposacc ;
    public String query;
    public string teaminstancename;
    public string teamID ;
    public string teaminstanceID;

    global BatchPopulateHCA_Affiliation(string teaminst) {
        teaminstancename ='';
        teamID = '';
        teaminstanceID ='';
        teaminstancename = teaminst;
        affiliationlist = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
        posacclist = new list<AxtriaSalesIQTM__Position_Account__c>();
        tempposacc = new list<temp_Acc_Terr__c>();
        query='select id,AxtriaSalesIQTM__Account__c,AxtriaARSnT__Account_HCP_NUmber__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        query += ' where AxtriaSalesIQTM__Team_Instance__r.name =:teaminstancename and AxtriaSalesIQTM__Account__r.Type=\'HCP\' and AxtriaSalesIQTM__isincludedCallPlan__c=True and AxtriaSalesIQTM__Account__r.AxtriaARSnt__Status__c=\'active\'';
        this.query = query;
        system.debug('==========Query IS:::'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
        string query2='';
        string query3='';
        teamID = '';
        teaminstanceID='';
        set<string>accountset = new set<string>();
        set<string>positionset = new set<string>();
        affiliationlist = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
        posacclist = new list<AxtriaSalesIQTM__Position_Account__c>();
        set<string>parentaccount = new set<string>();
        set<string>accposteam = new set<string>();
        map<string,string>childparent = new map<string,string>();
        set<string>parentposacc = new set<string>();
        tempposacc = new list<temp_Acc_Terr__c>();
        set<string>uniquetempacc = new set<string>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp:scope){
            accountset.add(pacp.Account_HCP_NUmber__c);
            positionset.add(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            teamID=pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c;
            teaminstanceID = pacp.AxtriaSalesIQTM__Team_Instance__c;
            String key=pacp.Account_HCP_NUmber__c+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__r.name;
            accposteam.add(key);
        }
        query2 = 'select id,AxtriaSalesIQTM__Account__r.AccountNUmber,AxtriaSalesIQTM__Parent_Account__r.AccountNumber from AxtriaSalesIQTM__Account_Affiliation__c ';
        query2 += ' where AxtriaSalesIQTM__Account__r.AccountNUmber IN :accountset and AxtriaARSnT__Primary_Affiliation_Indicator__c=\'Y\' and AxtriaSalesIQTM__Active__c=true and AxtriaSalesIQTM__Is_Primary__c=true ';

        system.debug('=========query2::::::'+query2);
        affiliationlist = Database.query(query2);

        system.debug('=======affiliationlist:::size():::'+affiliationlist.size());
        if(affiliationlist!=null && affiliationlist.size() >0){
            for(AxtriaSalesIQTM__Account_Affiliation__c af : affiliationlist){
                parentaccount.add(af.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
                childparent.put(af.AxtriaSalesIQTM__Account__r.AccountNUmber,af.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
            }
        }
        query3='select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.name from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:parentaccount and AxtriaSalesIQTM__Team_Instance__c=:teaminstanceID';
        system.debug('===========query3:'+query3);
        posacclist = Database.query(query3);
        system.debug('=================posacclist.size()::====='+posacclist.size());
        if(posacclist !=null && posacclist.size()>0){
            for(AxtriaSalesIQTM__Position_Account__c PA: posacclist){
                String key2=PA.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+PA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+PA.AxtriaSalesIQTM__Team_Instance__r.name;
                parentposacc.add(key2);
            }
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: scope){
                if(childparent.get(pacp.Account_HCP_NUmber__c)!=null){
                    String Parent=childparent.get(pacp.Account_HCP_NUmber__c);
                    String unq=Parent+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__r.name;
                    String tempunq = Parent+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                    if(!parentposacc.contains(unq) && !uniquetempacc.contains(tempunq)){
                        temp_Acc_Terr__c temp = new temp_Acc_Terr__c();
                        temp.AccountNumber__c = Parent;
                        temp.Territory_ID__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                        temp.TeamName__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                        temp.Status__c = 'New';
                        tempposacc.add(temp);
                        uniquetempacc.add(tempunq);
                    }
                }
            }

        }
        else{
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: scope){
                if(childparent.get(pacp.Account_HCP_NUmber__c)!=null){
                    String Parent=childparent.get(pacp.Account_HCP_NUmber__c);
                    String tempunq = Parent+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                    if(!uniquetempacc.contains(tempunq)){
                        temp_Acc_Terr__c temp = new temp_Acc_Terr__c();
                        temp.AccountNumber__c = Parent;
                        temp.Territory_ID__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                        temp.TeamName__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                        temp.Status__c = 'New';
                        tempposacc.add(temp);
                        uniquetempacc.add(tempunq);
                    }
                }
            }
        }
        system.debug('=========tempposacc:::'+tempposacc.size());
        if(tempposacc !=null &&  tempposacc.size()>0){
            Database.SaveResult []sr= Database.insert(tempposacc,false);
            //insert tempposacc;
        }
        
    }

    global void finish(Database.BatchableContext BC) {
       //Database.executeBatch(new  update_AccTerr(teamID,teaminstanceID),500); // call direct position account load batch to load the position accounts 

    }
}