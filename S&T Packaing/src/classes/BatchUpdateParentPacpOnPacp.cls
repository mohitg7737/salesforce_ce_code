global with sharing class BatchUpdateParentPacpOnPacp implements Database.Batchable<sObject>,Database.stateful 
{
    public String query;
    public string Ids;
    Boolean flag= true;
    Boolean fourthFlag;
    Boolean fifthFlag = true;
    public string teaminstanceis {get;set;}
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}
    public string ruleidis {get;set;}
    public boolean directFlag=false;
    public boolean directFlag2=false;

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;

    global BatchUpdateParentPacpOnPacp(string selectedteaminstance,string ruleID) {
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        ruleidis = '';
        ruleidis = ruleID;

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c=:teaminstanceis';
        }
        SnTDMLSecurityUtil.printDebugMessage('=======The Query is:'+query);
    }

    //Added by HT(A0944) on 12th June 2020
    global BatchUpdateParentPacpOnPacp(string selectedteaminstance,String loggerID,String alignNmspPrefix) 
    {
        directFlag=true;
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        //ruleidis = ruleID;

        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c '+
                'from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name = :teaminstanceis';
        }
        SnTDMLSecurityUtil.printDebugMessage('=======The Query is:'+query);
    }

    global BatchUpdateParentPacpOnPacp(string selectedteaminstance) {
        directFlag=true;
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        //ruleidis = ruleID;

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name = :teaminstanceis';
        }
        SnTDMLSecurityUtil.printDebugMessage('=======The Query is:'+query);
    }
    
    global BatchUpdateParentPacpOnPacp(string selectedteaminstance,string Ids,Boolean fourthFlag) {
        directFlag=true;
        this.Ids = Ids;
        this.fourthFlag = fourthFlag;
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        //ruleidis = ruleID;

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name = :teaminstanceis';
        }
        SnTDMLSecurityUtil.printDebugMessage('=======The Query is:'+query);
    }
    
    global BatchUpdateParentPacpOnPacp() {
        directFlag2=true;
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
       
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('=======The Query2 is:'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        try
        {
            set<string>teaminstances = new set<string>();
            set<string>positions = new set<string>();
            set<string>accounts = new set<string>();
            map<string,string>parentpacpidmap = new map<string,string>();
            customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
            list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> updatepacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
                teaminstances.add(pacp.AxtriaSalesIQTM__Team_Instance__c);
                positions.add(pacp.AxtriaSalesIQTM__Position__c);
                accounts.add(pacp.AxtriaSalesIQTM__Account__c);
            }
            if(accounts!=null && accounts.size()>0 && positions!=null && positions.size()>0 && teaminstances!=null && teaminstances.size()>0){
                SnTDMLSecurityUtil.printDebugMessage('============Inside the if condition==========');
                list<Parent_PACP__c>ParentList = [select id,Account__c,Position__c,Team_Instance__c from Parent_PACP__c where Account__c IN :accounts and Position__c IN:positions and Team_Instance__c IN:teaminstances];
                if(ParentList!=null && ParentList.size()>0){
                    for(Parent_PACP__c parentpacp : ParentList){
                        string key=parentpacp.Account__c+'_'+parentpacp.Position__c+'_'+parentpacp.Team_Instance__c;
                        if(!parentpacpidmap.containskey(key)){
                            parentpacpidmap.put(key,parentpacp.id);
                        }
                    }
                }
            }
            
            if(parentpacpidmap!=null && parentpacpidmap.size()>0){
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
                    string key2 = pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                    if(parentpacpidmap.containskey(key2)){
                       pacp.Parent_PACP__c =  parentpacpidmap.get(key2);
                       updatepacp.add(pacp);
                    }
                }

            }
            CallPlanSummaryTriggerHandler.execute_trigger = false;
            customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
            customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
            SnTDMLSecurityUtil.printDebugMessage('==========customsetting========'+customsetting);
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            //update customsetting ;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;
            
            //update updatepacp;
            SnTDMLSecurityUtil.updateRecords(updatepacp, 'BatchUpdateParentPacpOnPacp');

            customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
            CallPlanSummaryTriggerHandler.execute_trigger = true;
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
            //update customsetting ;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateParentPacpOnPacp : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,System.Label.CopyCallPlanJobFailed,'Error',alignNmsp);
            }
            //End
        } 
    }

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage('=======FINISHED THE PARENT PACP BATCHES=====');
            List<String> list_ti = new List<String>();  

            for(AxtriaSalesIQTM__Team_Instance__c ti: [Select ID from AxtriaSalesIQTM__Team_Instance__c where id =:teaminstanceis and 
                                                        Enable_Channel_Preference__c = true and MultiChannel__C = true 
                                                        WITH SECURITY_ENFORCED LIMIT 1]){ 
                list_ti.add(ti.id);
            }
            List<String> cimConfigList = new List<String>();
            for(AxtriaSalesIQTM__CIM_Config__c cim :[Select id from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__CR_Type_Name__c = :SalesIQGlobalConstants.CR_TYPE_CALL_PLAN and AxtriaSalesIQTM__Team_Instance__c = :teaminstanceis WITH SECURITY_ENFORCED ]){
              cimConfigList.add(cim.ID);

            }
            String team_ins = teaminstanceis;   
              
            if(list_ti.size()>0){
                if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                    if(proceedNextBatch)
                        Database.executeBatch(new BatchUpdateChannelPreferenceinPACP(list_ti,ruleidis,directFlag,
                                                directFlag2,callPlanLoggerID,alignNmsp),2000);
                }
                else
                    Database.executeBatch(new BatchUpdateChannelPreferenceinPACP(list_ti,ruleidis,directFlag,directFlag2),2000);     
            }
            /*SNT 515 starts*/
            else if(cimConfigList.size()>0){
                if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                    if(proceedNextBatch){
                        Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList,team_ins,ruleidis,directFlag,
                                                directFlag2,callPlanLoggerID,alignNmsp),2000);
                    }
                }
                else{
                    Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList,team_ins,ruleidis,directFlag,directFlag2),2000);  
                }
                            
            }
            /*SNT 515 ends*/
               
            else
            {
                if(directFlag2==true){
                    Database.executeBatch(new BatchCallPlanSummaryReports(), 200);
                }
                else if(directFlag==true)
                {
                    if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                        if(proceedNextBatch)
                            Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis,callPlanLoggerID,alignNmsp), 200);
                    }
                    else
                        Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis), 200);
                }
                else{
                    Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis,ruleidis), 200);   
                }   
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateParentPacpOnPacp : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,System.Label.CopyCallPlanJobFailed,'Error',alignNmsp);
            }
            //End
        }
    }
}