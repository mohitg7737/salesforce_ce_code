@isTest
public  class PrimaryAffiliationIndicatorTest {
    static testMethod void testMethod1() 
    {
    	
    	Account g=new Account();
        g.name='test';
        g.Marketing_Code__c='ES';
        g.Type='HCP';
        g.Status__c='Active';
        g.AccountNumber='test123';
        insert g;

        
        Account parent=new Account();
        parent.name='testParent';
        parent.Marketing_Code__c='ES';
        parent.Type='HCA';
        parent.Status__c='Active';
        parent.AccountNumber='testParentt123';
        insert parent;

        AxtriaSalesIQTM__Affiliation_Network__c net=new AxtriaSalesIQTM__Affiliation_Network__c();
        net.AxtriaSalesIQTM__Hierarchy_Level__c='2';
        insert net;

        AxtriaSalesIQTM__Account_Affiliation__c aff=new AxtriaSalesIQTM__Account_Affiliation__c();
        aff.AxtriaSalesIQTM__Account__c=g.id;
        aff.Account_Number__c=g.AccountNumber;
        aff.AxtriaSalesIQTM__Active__c=true;
        aff.Affiliation_Status__c='Active';
        aff.Primary_Affiliation_Indicator__c='Y';
        aff.AxtriaSalesIQTM__Parent_Account__c=parent.Id;
        aff.Parent_Account_Number__c=parent.AccountNumber;
        aff.AxtriaSalesIQTM__Affiliation_Network__c=net.id;
        insert aff;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    }
}