/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 15th August 2020
@description  : Test class for BatchDeleteMCCPLoadData
@Revision(s)  : v1.0
**********************************************************************************************/
@isTest
private class BatchDeleteMCCPLoadDataTest 
{
	//Test for Product Segment
    public static TestMethod void testMethod1() 
    {
    	String className = 'BatchDeleteMCCPLoadDataTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create MCCP DataLoad rec having RecordType = Product
		MCCP_DataLoad__c mccp = new MCCP_DataLoad__c();
		mccp.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Product').getRecordTypeId();
		mccp.Workspace__c = workspace.Id;
		mccp.Country__c = countr.Id;
        mccp.ExternalID__c = 'Rec1';
		SnTDMLSecurityUtil.insertRecords(mccp,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchDeleteMCCPLoadData for RecordType = Product
        Database.executeBatch(new BatchDeleteMCCPLoadData(cr.Id,'Product',workspace.Id,countr.Id,true),2000);

        System.Test.stopTest();
    }

    //Test for Channel Preference
    public static TestMethod void testMethod2() 
    {
    	String className = 'BatchDeleteMCCPLoadDataTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create MCCP DataLoad rec having RecordType = Channel Preference
		MCCP_DataLoad__c mccp = new MCCP_DataLoad__c();
		mccp.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Channel Preference').getRecordTypeId();
		mccp.Workspace__c = workspace.Id;
		mccp.Country__c = countr.Id;
        mccp.ExternalID__c = 'Rec2';
		SnTDMLSecurityUtil.insertRecords(mccp,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchDeleteMCCPLoadData for RecordType = Channel Preference
        Database.executeBatch(new BatchDeleteMCCPLoadData(cr.Id,'Channel Preference',workspace.Id,countr.Id,true),2000);

        System.Test.stopTest();
    }

    //Test for Channel Consent
    public static TestMethod void testMethod3() 
    {
    	String className = 'BatchDeleteMCCPLoadDataTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create MCCP DataLoad rec having RecordType = Channel Consent
		MCCP_DataLoad__c mccp = new MCCP_DataLoad__c();
		mccp.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Channel Consent').getRecordTypeId();
		mccp.Workspace__c = workspace.Id;
		mccp.Country__c = countr.Id;
        mccp.ExternalID__c = 'Rec3';
		SnTDMLSecurityUtil.insertRecords(mccp,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchDeleteMCCPLoadData for RecordType = Channel Consent
        Database.executeBatch(new BatchDeleteMCCPLoadData(cr.Id,'Channel Consent',workspace.Id,countr.Id,true),2000);

        System.Test.stopTest();
    }
}