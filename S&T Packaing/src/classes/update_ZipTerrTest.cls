@isTest
public class update_ZipTerrTest {
    @isTest
    static void testZipTerr(){
        
        AxtriaSalesIQTM__TriggerContol__c tc = new AxtriaSalesIQTM__TriggerContol__c();
        tc.name = 'PositionGeographyTrigger';
        tc.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert tc;
        
        List<AxtriaSalesIQTM__Geography__c> geo = new List<AxtriaSalesIQTM__Geography__c>();
        for(Integer i = 1; i<10 ; i++){
            AxtriaSalesIQTM__Geography__c zip = new AxtriaSalesIQTM__Geography__c();
            zip.name = '0000'+i;
            geo.add(zip);
        }
        insert geo;
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(name = 'T1');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_instance__c();
        ti.name = 'T1-Q1';
        ti.AxtriaSalesIQTM__Team__c = team.id ; //[Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        ti.AxtriaSalesIQTM__IC_EffEndDate__c= system.today();
        ti.AxtriaSalesIQTM__IC_EffstartDate__c= system.today();
        
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'XYZ';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id; //[Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = ti.id; //[Select id from AxtriaSalesIQTM__Team_Instance__c where name = 'T1-Q1'][0].Id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'X';
        
        insert pos;
        
        List<temp_Zip_Terr__c> zipTerr = new List<temp_Zip_Terr__c >();
        for(Integer i =1; i<10; i++){
            temp_Zip_Terr__c zt = new temp_Zip_Terr__c();
            zt.Territory_ID__c = 'X';
            zt.Zip_Name__c = '0'+i;
            zt.Team_Name__c = 'T1';
            zipTerr.add(zt);
        }        
        insert zipTerr;
        Test.startTest();
        system.debug('team ID and TI' +team.id + '&&&&' +ti.id );
        Database.executeBatch(new UpdateZipTerr(team.id,ti.id));
    
        Test.stopTest();
    }
    
    
}