@isTest
private class SalesIQUtility_Test {
    static testMethod void getUserAccessPermistionTest() {

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas, 'SalesIQUtility_Test');

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr, 'SalesIQUtility_Test');
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        list<AxtriaSalesIQTM__User_Access_Permission__c> uap = SalesIQUtility.getUserAccessPermistion(UserInfo.getUserID());
        uap = SalesIQUtility.getUserAccessPermistion(UserInfo.getUserID(),(String)countr.Id);
    }
    static testMethod void checkCountryAccessTest() {

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas, 'SalesIQUtility_Test');

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr, 'SalesIQUtility_Test');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        Map<string,string> mapString = SalesIQUtility.checkCountryAccess((String)countr.Id);
    }
    static testMethod void GenerateRandomNumberTest() {
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        String rand_number = SalesIQUtility.GenerateRandomNumber();
    }
    static testMethod void getDefaultCountryIDTest(){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        String country = SalesIQUtility.getDefaultCountryID(UserInfo.getUserId());
    }
    static testMethod void getLocaleToDateTimeFmtMapTest(){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        Map<String,String> mapLocale = SalesIQUtility.getLocaleToDateTimeFmtMap();
    }
    static testMethod void updateCRStatusDirectLoadTest(){
    
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = TestDataFactory.createChangeRequest(UserInfo.getUserId(), null, null, null, '');
        SnTDMLSecurityUtil.insertRecords(cr, 'updateCRStatusDirectLoadTest');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        SalesIQUtility.updateCRStatusDirectLoad((String)cr.Id,'',0);
    }
    static testMethod void submitForRejectionTest(){
    
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = TestDataFactory.createChangeRequest(UserInfo.getUserId(), null, null, null, '');
        SnTDMLSecurityUtil.insertRecords(cr, 'updateCRStatusDirectLoadTest');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        Test.startTest();
        SalesIQUtility.submitForRejection(new List<Id>{cr.Id},'Rejected');
        Test.stopTest();
    }
    static testMethod void getJsonStringTest(){


        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas, 'getJsonStringTest');

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr, 'getJsonStringTest');


        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team, 'getJsonStringTest');

        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins, 'getJsonStringTest');

        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos, 'getJsonStringTest');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        Test.startTest();
        String jsonString = SalesIQUtility.getJsonString(new List<String>{(String)pos.id},new List<AxtriaSalesIQTM__Position__c>{pos});
        Test.stopTest();

    }

    static testMethod void getPositionbyRootPositionIdTest(){


        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas, 'getJsonStringTest');

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr, 'getJsonStringTest');


        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team, 'getJsonStringTest');

        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins, 'getJsonStringTest');

        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos, 'getJsonStringTest');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));


        Test.startTest();
        List<AxtriaSalesIQTM__Position__c> listpos= SalesIQUtility.getPositionbyRootPositionId(new List<String>{(String)pos.id},(String)teamins.id,(String)teamins.id,0);
        Test.stopTest();
        

    }

    static testMethod void updateCallPlanLoggerRecordTest() {

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas, 'SalesIQUtility_Test');

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr, 'SalesIQUtility_Test');

        AxtriaSalesIQTM__SalesIQ_Logger__c slog = new AxtriaSalesIQTM__SalesIQ_Logger__c();
        SnTDMLSecurityUtil.insertRecords(slog, 'SalesIQUtility_Test');

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        Test.startTest();
        SalesIQUtility.updateCallPlanLoggerRecord((String)slog.id,'','','AxtriaSalesIQTM__');
        Test.stopTest();
        
    }
}