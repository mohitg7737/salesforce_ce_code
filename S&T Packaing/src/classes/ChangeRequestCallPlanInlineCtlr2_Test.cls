@isTest
public class ChangeRequestCallPlanInlineCtlr2_Test {
    @istest static void ChangeRequestCallPlanInlineCtlr2_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        String classname = 'ChangeRequestCallPlanInlineCtlr2_Test';
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        SnTDMLSecurityUtil.insertRecords(om,className);
        Account acc= TestDataFactory.createAccount();
        SnTDMLSecurityUtil.insertRecords(acc,className);
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
       SnTDMLSecurityUtil.insertRecords(c,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(c);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = c.id;
         SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, c);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
       SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
         SnTDMLSecurityUtil.insertRecords(pPriority,className);
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins, pos, pcc);
        posprod.Calls_Day__c = 1;
        // posprod.Effective_Days_in_Field_Formula__c = 1;
        //insert posprod;
        SnTDMLSecurityUtil.insertRecords(posprod,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pcc.name;
        positionAccountCallPlan.p1_original__c = pcc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp.Final_TCF__c = 10;
        pacp.P1__c = pcc.name;
        pacp.p1_original__c = pcc.name;
        pacp.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id, null, teamins.id);
       SnTDMLSecurityUtil.insertRecords(posteamins,className);
        
        AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
        crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
        crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        SnTDMLSecurityUtil.insertRecords(crType,className);
        
        
        
        AxtriaSalesIQTM__CIM_Config__c cimconfigs = new AxtriaSalesIQTM__CIM_Config__c();
        cimConfigs.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        cimconfigs.isCallCapacity__c = true;
        cimConfigs.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';
        cimConfigs.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Test Display';
        cimConfigs.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
        cimConfigs.AxtriaSalesIQTM__Attribute_API_Name__c = 'AccountNumber';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimConfigs.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c = true and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c=acc.Type';      
        cimConfigs.AxtriaSalesIQTM__Change_Request_Type__c = crType.id;
        cimConfigs.AxtriaSalesIQTM__Enable__c = true;
        cimConfigs.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimConfigs.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimConfigs.AxtriaSalesIQTM__Threshold_Min__c = '-40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Max__c = '40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-20';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Max__c = '20';
        
        SnTDMLSecurityUtil.insertRecords(cimConfigs,className);
        
        
        
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimsummary = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cimSummary.AxtriaSalesIQTM__Approved__c = '100';
        cimSummary.AxtriaSalesIQTM__Original__c = '100';
        cimSummary.AxtriaSalesIQTM__Proposed__c = '100';
        cimSummary.AxtriaSalesIQTM__CIM_Config__c = cimconfigs.id;
        cimSummary.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimSummary.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        SnTDMLSecurityUtil.insertRecords(cimSummary,className);
        
        Grid_Master__c g=new Grid_Master__c();
        g.name='test';
        g.Country__c=c.id;
        g.CurrencyIsoCode='USD';
        SnTDMLSecurityUtil.insertRecords(g,className);
        CallPlanCallCapacity__c cp = new CallPlanCallCapacity__c();
        cp.is_Team_Level__c = false;
        cp.Aggregate_Function__c = 'SUM';
        cp.Name = 'test';
         SnTDMLSecurityUtil.insertRecords(cp,className);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            PageReference pageRef = Page.ChangeRequestInline2;
            Test.setCurrentPage(pageRef);
            AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
            cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
            cr.AxtriaSalesIQTM__Destination_Position__c = pos.id;
            SnTDMLSecurityUtil.insertRecords(cr,className);
            pageRef.getParameters().put('Id', String.valueOf(cr.Id));
            ApexPages.StandardController sc = new ApexPages.standardController(cr);       
            ChangeRequestCallPlanInlineCtlr2 obj=new ChangeRequestCallPlanInlineCtlr2(sc);
            //obj.cimConfig();
        }
        Test.stopTest();
    }
     @istest static void ChangeRequestCallPlanInlineCtlr2_Test1()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'ChangeRequestCallPlanInlineCtlr2_Test';
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        SnTDMLSecurityUtil.insertRecords(om,className);
        Account acc= TestDataFactory.createAccount();
        SnTDMLSecurityUtil.insertRecords(acc,className);
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        c.MCCP_Enabled__c = true;
         SnTDMLSecurityUtil.insertRecords(c,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(c);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
         SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        teamins1.MCCP_Enabled__c = true;
         SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = c.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.MCCP_Enabled__c = true;
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, c);
         SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
         SnTDMLSecurityUtil.insertRecords(mmc,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins, pos, pcc);
        posprod.Calls_Day__c = 1;
        // posprod.Effective_Days_in_Field_Formula__c = 1;
        SnTDMLSecurityUtil.insertRecords(posprod,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pcc.name;
        positionAccountCallPlan.p1_original__c = pcc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
         SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp.Final_TCF__c = 10;
        pacp.P1__c = pcc.name;
        pacp.p1_original__c = pcc.name;
        pacp.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp1.Final_TCF__c = 10;
        pacp1.P1__c = 'Test1';
        pacp1.p1_original__c = 'Test1';
        pacp1.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp1,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp2 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp2.Final_TCF__c = 10;
        pacp2.P1__c = 'Test2';
        pacp2.p1_original__c = 'Test2';
        pacp2.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp2,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp3 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp3.Final_TCF__c = 10;
        pacp3.P1__c = 'Test3';
        pacp3.p1_original__c = 'Test3';
        pacp3.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp3,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp4 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp4.Final_TCF__c = 10;
        pacp4.P1__c = 'Test4';
        pacp4.p1_original__c = 'Test4';
        pacp4.Final_TCF_Original__c = 10;
         SnTDMLSecurityUtil.insertRecords(pacp4,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp5 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp5.Final_TCF__c = 10;
        pacp5.P1__c = 'Test5';
        pacp5.p1_original__c = 'Test5';
        pacp5.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp5,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp6 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp6.Final_TCF__c = 10;
        pacp6.P1__c = 'Test6';
        pacp6.p1_original__c = 'Test6';
        pacp6.Final_TCF_Original__c = 10;
         SnTDMLSecurityUtil.insertRecords(pacp6,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp7 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp7.Final_TCF__c = 10;
        pacp7.P1__c = 'Test7';
        pacp7.p1_original__c = 'Test7';
        pacp7.Final_TCF_Original__c = 10;
         SnTDMLSecurityUtil.insertRecords(pacp7,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp8 = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        pacp8.Final_TCF__c = 10;
        pacp8.P1__c = 'Test8';
        pacp8.p1_original__c = 'Test8';
        pacp8.Final_TCF_Original__c = 10;
        SnTDMLSecurityUtil.insertRecords(pacp8,className);
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id, null, teamins.id);
         SnTDMLSecurityUtil.insertRecords(posteamins,className);
        AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
        crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
        crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        SnTDMLSecurityUtil.insertRecords(crType,className);
        
        
        
        AxtriaSalesIQTM__CIM_Config__c cimconfigs = new AxtriaSalesIQTM__CIM_Config__c();
        cimConfigs.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        cimconfigs.isCallCapacity__c = true;
        cimConfigs.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';
        cimConfigs.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Test Display';
        cimConfigs.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
        cimConfigs.AxtriaSalesIQTM__Attribute_API_Name__c = 'AccountNumber';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimConfigs.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimConfigs.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c = true and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c=acc.Type';      
        cimConfigs.AxtriaSalesIQTM__Change_Request_Type__c = crType.id;
        cimConfigs.AxtriaSalesIQTM__Enable__c = true;
        cimConfigs.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimConfigs.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimConfigs.AxtriaSalesIQTM__Threshold_Min__c = '-40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Max__c = '40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-20';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Max__c = '20';
        
        SnTDMLSecurityUtil.insertRecords(cimConfigs,className);
        
        
        
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimsummary = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cimSummary.AxtriaSalesIQTM__Approved__c = '100';
        cimSummary.AxtriaSalesIQTM__Original__c = '100';
        cimSummary.AxtriaSalesIQTM__Proposed__c = '100';
        cimSummary.AxtriaSalesIQTM__CIM_Config__c = cimconfigs.id;
        cimSummary.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimSummary.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        SnTDMLSecurityUtil.insertRecords(cimSummary,className);
        
        Grid_Master__c g=new Grid_Master__c();
        g.name='test';
        g.Country__c=c.id;
        g.CurrencyIsoCode='USD';
         SnTDMLSecurityUtil.insertRecords(g,className);
        CallPlanCallCapacity__c cp = new CallPlanCallCapacity__c();
        cp.is_Team_Level__c = false;
        cp.Aggregate_Function__c = 'MAX';
        cp.Name = 'test';
         SnTDMLSecurityUtil.insertRecords(cp,className);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            PageReference pageRef = Page.ChangeRequestInline2;
            Test.setCurrentPage(pageRef);
            AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
            cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
            cr.AxtriaSalesIQTM__Destination_Position__c = pos.id;
             SnTDMLSecurityUtil.insertRecords(cr,className);
            pageRef.getParameters().put('Id', String.valueOf(cr.Id));
            ApexPages.StandardController sc = new ApexPages.standardController(cr);       
            ChangeRequestCallPlanInlineCtlr2 obj=new ChangeRequestCallPlanInlineCtlr2(sc);
            obj.selectedTeamInstance = teamins.id;
            //obj.cimConfig();
        }
        Test.stopTest();
    }
}