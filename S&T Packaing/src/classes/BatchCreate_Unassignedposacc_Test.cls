@isTest
private class BatchCreate_Unassignedposacc_Test {
    static testMethod void testMethod1() {
    	Scheduler_Log__c sc = new Scheduler_Log__c();
    	sc.Job_Name__c='Unassigned PosAcc Delta';
    	sc.Job_Status__c='Successful';
    	sc.Created_Date2__c = system.Today()-1;
    	insert sc;

    	Account g=new Account();
        g.name='test';
        g.Marketing_Code__c='ES';
        g.Country_Code__c= 'ES';
        g.AccountNumber='1234';
        insert g;
        
        Account gg=new Account();
        gg.name='test2';
        gg.Marketing_Code__c='ES';
        gg.Country_Code__c= 'ES';
        gg.AccountNumber='12345';
        insert gg;

        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='ES';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;

        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c();
        country.Name = 'Spain';
        country.AxtriaSalesIQTM__Country_Code__c = 'ES';
        country.AxtriaSalesIQTM__Parent_Organization__c= orgId;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;

        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name='HTN';
        team.AxtriaSalesIQTM__Country__c=country.id;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.Name = 'abcd';
        objTeamInstance.AxtriaSalesIQTM__Team__c = team.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c='Current';
        insert objTeamInstance;


        List<AxtriaSalesIQTM__Position__c> poss = new List<AxtriaSalesIQTM__Position__c>();
        for(Integer i=0;i<10;i++)
        {
            AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();    
            pos.name = 'Unassigned';
            pos.AxtriaSalesIQTM__Team_iD__c = team.id;
            pos.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
            poss.add(pos);
        }
        insert poss;
        for(AxtriaSalesIQTM__Position__c p : poss)
        {
            String posid = p.id;
        	AxtriaSalesIQTM__Position_Team_Instance__c pt = new AxtriaSalesIQTM__Position_Team_Instance__c();
        	pt.AxtriaSalesIQTM__Position_ID__c = posid;
        	pt.AxtriaSalesIQTM__Team_Instance_ID__c = p.AxtriaSalesIQTM__Team_Instance__c;
        	pt.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        	pt.AxtriaSalesIQTM__Effective_Start_Date__c = system.today()+5;
        	insert pt;
        }
		
        /*
        UnassignedAccount__mdt mdtobj = new UnassignedAccount__mdt();
        mdtobj.Label ='ES';
        insert mdtobj;
		*/
        
        Database.executeBatch(new BatchCreate_Unassignedposacc());
    }
}