public with sharing class StepCompute implements Step{
    public Compute_Master__c cmpMaster;
    public StepCompute(Step__c step){
        cmpMaster = [SELECT Id, Field_1__r.Parameter_Name__c, Field_2__c, Field_2__r.Parameter_Name__c, Field_2_Type__c, Field_2_val__c, Operator__c FROM Compute_Master__c WHERE Id=:step.Compute_Master__c LIMIT 1];
    }

    

    public String solveStep(Step__c step, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu, List<BU_Response__c> allBuResponses, List<String> allAggregateFunc){
        Decimal val1 = 0;
        Decimal val2 = 0;
        //System.debug('=======acfMap::'+acfMap + ' '+ nameFieldMap);
        if(nameFieldMap.containsKey(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())){
       //     system.debug('---namemap-- ' + (String)bu.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())));
            val1 = Decimal.valueOf((String)bu.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) != null ? (String)bu.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) : '0');
        }else{
       //     System.debug('--Parametrer-- ' + cmpMaster.Field_1__r.Parameter_Name__c);
            if(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()) != null && acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()) != '')
            val1 = Decimal.valueOf(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()));
            else
            val1= 0;
        }

        if(String.isNotBlank(cmpMaster.Field_2__c)){
            if(nameFieldMap.containsKey(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase())){
                val2 = Decimal.valueOf((String)bu.get(nameFieldMap.get(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase())) != null ? (String)bu.get(nameFieldMap.get(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase())) : '0');
            }else{
                
                if(acfMap.get(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase()) != null && acfMap.get(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase()) != '')
                val2 = Decimal.valueOf(acfMap.get(cmpMaster.Field_2__r.Parameter_Name__c.toUpperCase()));
                else 
                val2= 0;
            }
        }else{
            val2 = cmpMaster.Field_2_val__c;
        }

        Decimal output = 0;
        try{
            if(cmpMaster.Operator__c == 'ADD'){
                output = val1 + val2;
            }else if(cmpMaster.Operator__c == 'SUBTRACT'){
                output = val1 - val2;
            }else if (cmpMaster.Operator__c == 'MULTIPLY') {
                output = val1 * val2;
            }else if (cmpMaster.Operator__c == 'DIVIDE') {
                output = val1/val2;
            }
            else if(cmpMaster.Operator__c == 'MAX')
            {
                String maxString = allAggregateFunc[0];
                List<String>allVals = maxString.split('_');
                Integer maxVal = Integer.valueof(allVals[1]);

               
                if(maxVal == 9999)
                {
                    output = -9999;
                  

                    for(BU_Response__c tempBU : allBuResponses)
                    {
                        if(nameFieldMap.containsKey(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()))
                        {
                            
                            val1 = Decimal.valueOf((String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) != null ? (String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) : '0');
                        }
                        else
                        {
                            
                            val1 = Decimal.valueOf(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()));
                        }
                        
                       
                        if(output < val1)
                        {
                            output = val1;
                        }
                    }

                    String newMaxVal = 'MAX_'+String.valueOf(output);
                    allAggregateFunc[0] = newMaxVal;

                }
                else
                {
                    output= maxVal;
                }
                
                
            }
            else if(cmpMaster.Operator__c == 'MIN')
            {
                String minString = allAggregateFunc[1];
                List<String>allVals = minString.split('_');
                Integer minVal = Integer.valueof(allVals[1]);

                if(minVal == 9999)
                {

                    output = 9999;
                    
                    for(BU_Response__c tempBU : allBuResponses)
                    {
                        if(nameFieldMap.containsKey(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()))
                        {
                           
                            val1 = Decimal.valueOf((String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) != null ? (String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) : '0');
                        }
                        else
                        {
                            
                            val1 = Decimal.valueOf(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()));
                        }
                        
                        if(output > val1)
                        {
                            output = val1;
                        }
                    }

                    String newMinVal = 'MIN_'+String.valueOf(output);
                    allAggregateFunc[1] = newMinVal;
                }
                else
                {
                    output = minVal;
                }
                
            }
            else if(cmpMaster.Operator__c == 'AVG')
            {
                String avgString = allAggregateFunc[2];
                List<String>allVals = avgString.split('_');
                Integer avgVal = Integer.valueof(allVals[1]);

                Set<String> allAccounts = new Set<String>();

                if(avgVal == 9999)
                {
                    output = 0;
                    Integer counter = 0;
                    
                    for(BU_Response__c tempBU : allBuResponses)
                    {
                        if(!allAccounts.contains(tempBU.Physician__c))
                        {
                            counter ++;
                            if(nameFieldMap.containsKey(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()))
                            {
                               
                                val1 = Decimal.valueOf((String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) != null ? (String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) : '0');
                            }
                            else
                            {
                                
                                val1 = Decimal.valueOf(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()));
                            }
                            output += val1;                            
                            allAccounts.add(tempBU.Physician__c);
                        }

                    }
                    
                    output = output/counter;

                    String newAvgVal = 'AVG_'+String.valueOf(output);
                    allAggregateFunc[2] = newAvgVal;

                }
                else
                {
                    output = avgVal;
                }
                
            }
            else if(cmpMaster.Operator__c == 'SUM')
            {
                String sumString = allAggregateFunc[3];
                List<String>allVals = sumString.split('_');
                Integer sumVal = Integer.valueof(allVals[1]);

                if(sumVal == 0)
                {
                    output = 0;
                    Integer counter = 0;
                    
                    for(BU_Response__c tempBU : allBuResponses)
                    {
                        if(nameFieldMap.containsKey(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()))
                        {
                           
                            val1 = Decimal.valueOf((String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) != null ? (String)tempBU.get(nameFieldMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase())) : '0');
                        }
                        else
                        {
                           
                            val1 = Decimal.valueOf(acfMap.get(cmpMaster.Field_1__r.Parameter_Name__c.toUpperCase()));
                        }
                        output += val1;
                    }
                    
                    String newSumVal = 'SUM_'+String.valueOf(output);
                    allAggregateFunc[3] = newSumVal;

                }
                else
                {
                    output = sumVal;
                }
                
            }
        }catch (Exception e){
            output = 0;
            
        }
        output = output.setScale(2);
        return String.valueOf(output);
    }

   /* public String solveStep(Step__c step, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu, List<BU_Response__c> allBuResponses, List<String> allAggregateFunc){

        RT_Expression expression = new RT_Expression(step.Compute_Master__r.Expression__c,nameFieldMap, acfMap, bu);
        return String.valueOf(expression.eval());
        
    }*/ // Commented to revert SR-9 Changes
}