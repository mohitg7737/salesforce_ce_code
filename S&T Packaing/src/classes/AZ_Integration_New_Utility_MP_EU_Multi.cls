global class AZ_Integration_New_Utility_MP_EU_Multi implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> mCyclePlanChannel;
    List<Parent_PACP__c> pacpRecs; 
    List<String> allTeamInstances;
    List<String> teamProdAccConcat;
    public Map<String,Decimal> teamSellValues;
    
    global AZ_Integration_New_Utility_MP_EU_Multi(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        teamInstanceSelected = teamInstanceSelectedTemp;
        queryString = 'select id,Account__r.AccountNumber, Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Team_Instance__c,Team_Instance__r.Name,Team_Instance__r.Team_Goals__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c,Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__c,P1__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null LIMIT 50),Position__r.Original_Country_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from Parent_PACP__c where Team_Instance__c = :teamInstanceSelected';       
        
        allChannels = allChannelsTemp;
    }

    global AZ_Integration_New_Utility_MP_EU_Multi(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        queryString = 'select id,Account__r.AccountNumber, Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Team_Instance__c,Team_Instance__r.Name, Team_Instance__r.Team_Goals__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__c,P1__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from Parent_PACP__c where Team_Instance__c in :allTeamInstances';       
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        system.debug('+ HEy '+ queryString);
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Channel_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
        pacpRecs = scopePacpProRecs;
        mCyclePlanChannel = new List<SIQ_MC_Cycle_Plan_Channel_vod_O__c>();
        Set<String> allRecs = new Set<String>();
        teamProdAccConcat = new List<String>();
        teamSellValues= new Map<String,Decimal>();

    String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;

        
        List<Veeva_Market_Specific__c>  veevaCriteria = [select Channel_Name__c,Metric_Name__c,Weight__c,MCCP_Target_logic__c, Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        map<string,string>allChannels = new map<string,string>();
        Map<string,string> metricAndWeights = new Map<string,string>();
        Map<string,decimal> channelCallMapping = new Map<string,decimal>();     // stores mapping of channels and their respective calls.
        for(integer i=0; i < veevaCriteria[0].Channel_Name__c.split(',').size();i++){
            
            metricAndWeights.put(veevaCriteria[0].Metric_Name__c.split(',')[i],veevaCriteria[0].Weight__c.split(',')[i]);
            allChannels.put(veevaCriteria[0].Metric_Name__c.split(',')[i],veevaCriteria[0].Channel_Name__c.split(',')[i]); 
            channelCallMapping.put(veevaCriteria[0].Metric_Name__c.split(',')[i],0.0);
           
           
        }
        for(Parent_PACP__c ppacp : scopePacpProRecs)
        {
            //system.debug('++ppacp'+ppacp);
            //system.debug('++++== ppacp.Call_Plan_Summary__r ' + ppacp.Call_Plan_Summary__r);
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpTeam : ppacp.Call_Plan_Summary__r)
            {
                /*pacpSet.add(pacpTeam.AxtriaSalesIQTM__Team_Instance__r.Cycle__c +'&'+pacpTeam.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpTeam.P1__c);
                parentPACPMap.put(ppacp.id,pacpSet);*/
                teamProdAccConcat.add(pacpTeam.AxtriaSalesIQTM__Team_Instance__r.Cycle__c +'&'+pacpTeam.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpTeam.P1__c);
                system.debug('++teamProdAccConcat'+teamProdAccConcat);
            }
        }   
       if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Individual')
        {
            teamSellValues= Team_Selling.updateIndividualGoals(teamProdAccConcat);
        }
        else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Sum Team')
        {
            teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
        }
        else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Max Team')
        {
            teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
        }
        else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
        {
            teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
            /*teamSellValues1= Team_Selling.updateIndividualGoals(teamProdAccConcat);*/
        }
        else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Max Team+Individual')
        {
            teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
        }

        Decimal maxInd;
        Decimal sumInd;
        Decimal maxTeam;
        Decimal sumTeam;
        Decimal maxSumTeam;
        Decimal value;
        Decimal sumMaxTeam;
        system.debug('pacpRecs==========='+pacpRecs);

        for(Parent_PACP__c ppacp : pacpRecs)
        {
            
            Integer f2fCalls = 0;
            maxInd = 0;
            sumInd = 0;
            maxTeam = 0;
            sumTeam = 0;
            maxSumTeam=0;
            sumMaxTeam=0;

            value = 0;
            
            Boolean flag = false;

            //String channels = 'F2F';
            channelCallMapping = new Map<string,decimal>();
            System.debug('Size of ppacp.Call_Plan_Summary__r -->' + ppacp.Call_Plan_Summary__r.size());
            
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
                
                if(pacp.Final_TCF_Approved__c == null)
                    pacp.Final_TCF_Approved__c = 0;
                    

                if(maxInd < pacp.Final_TCF_Approved__c)
                {
                    maxInd = Integer.valueOf(pacp.Final_TCF_Approved__c);
                }
                sumInd = sumInd + pacp.Final_TCF_Approved__c;
                flag = true;
                
                 /**************** code for sum the other channels**************/ 
                    for(string metric:metricAndWeights.keyset())
                    {
                        if(pacp.get(metric)!=null)
                        {       decimal channelcall=0;
                              if(channelCallMapping.get(metric)!=null)
                               channelcall=channelCallMapping.get(metric);
                               
                               decimal weight=Decimal.valueOf(metricAndWeights.get(metric));
                               decimal metricValue=(decimal)(pacp.get(metric));
                               channelcall=channelcall+(metricValue*weight);
                               channelCallMapping.put(metric,channelcall);
                        }
                        
                    }
                    if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' )
                    {
                        channelCallMapping.put('Final_TCF__c',sumInd);
                        
                    }
                    else{
                        
                        channelCallMapping.put('Final_TCF__c',maxInd);
                        
                    }
                //********************************* end ************************    

               // string teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
               // mcpc.SIQ_Cycle_Channel_vod__c       =   pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';
                
                DateTime dtStart                 =   pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());
                String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
               // mcpc.SIQ_Cycle_Plan_Target_vod__c   =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                //mcpc.SIQ_External_Id_vod__c =teamPos + '-' +channels + '-'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber;   
                if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                {
                    if(teamSellValues.containsKey(pacp.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c))
                    {
                        value=teamSellValues.get(pacp.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c);
                        sumTeam=sumTeam+value;
                        if(maxSumTeam<value)
                           {
                             maxSumTeam=value;
                           }  
                    }
                    system.debug('++sum'+sumTeam);
                    system.debug('++maxSumteam'+maxSumTeam);

                }

                else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                {
                    if(teamSellValues.containsKey(pacp.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c))
                    {
                       value=teamSellValues.get(pacp.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c);
                       sumMaxTeam=sumMaxTeam+value;
                       if(maxTeam<value)
                       {
                         maxTeam=value;
                       }  
                    }
                    system.debug('++max'+maxTeam);
                    system.debug('++sumMaxTeam'+sumMaxTeam);
                }
            }
            system.debug('++sumInd'+sumInd);
            system.debug('++maxInd'+maxInd);
            //mcpc.SIQ_Channel_Activity_Goal_vod__c = f2fCalls;    
          
           // mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
           
          
          for(string metric:allChannels.keyset())
            {
                     SIQ_MC_Cycle_Plan_Channel_vod_O__c mcpc = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();
                     
                       mcpc.Rec_Status__c = 'Updated';
                        mcpc.Team_Instance__c = ppacp.Team_Instance__c;
                        if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                        {
                             mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        }
                        else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                        {
                             mcpc.CountryID__c = ppacp.Position__r.Original_Country_Code__c;
                        }
                        else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                        {
                             mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                        }
                    string channels=allChannels.get(metric);
                    string teamPos = ppacp.Team_Instance__r.Name +'_' + ppacp.Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    if(channels.contains('Email'))
                            mcpc.SIQ_Cycle_Channel_vod__c       =   ppacp.Team_Instance__r.Name + '-' + channels + '-Sent_Email_vod__c';
                    else if(channels.contains('Events'))
                        mcpc.SIQ_Cycle_Channel_vod__c       =   ppacp.Team_Instance__r.Name + '-' + channels + '-Multichannel_Activity_vod__c';
                     else
                        mcpc.SIQ_Cycle_Channel_vod__c       =   ppacp.Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';

                   
                    mcpc.SIQ_External_Id_vod__c =teamPos + '-' +channels + '-'+ ppacp.Account__r.AccountNumber; 
                    mcpc.SIQ_Cycle_Plan_Target_vod__c   =   teamPos + '_' + ppacp.Account__r.AccountNumber;
                     mcpc.External_ID_Axtria__c = mcpc.SIQ_External_Id_vod__c;
                     
                     system.debug('SIQ_External_Id_vod__c======='+mcpc.SIQ_External_Id_vod__c);
                    if(ppacp.Team_Instance__r.Team_Goals__c=='Individual')
                    {
                       if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                          mcpc.SIQ_Channel_Activity_Goal_vod__c  = channelCallMapping.get(metric);//sumInd;
                          mcpc.SIQ_Channel_Activity_Max_vod__c =  channelCallMapping.get(metric);//sumInd;
                          
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                           mcpc.SIQ_Channel_Activity_Goal_vod__c  = channelCallMapping.get(metric);//maxInd;
                           mcpc.SIQ_Channel_Activity_Max_vod__c = channelCallMapping.get(metric);//maxInd;
                       }
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team')
                    {
                         if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=sumTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c =sumTeam;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=maxSumTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c =maxSumTeam;
                        }
                        mcpc.SIQ_Channel_Activity_Goal_vod__c =0;
                        mcpc.SIQ_Channel_Activity_Max_vod__c = 0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=sumTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c =sumTeam;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=maxSumTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c =maxSumTeam;
                        }
                        mcpc.SIQ_Channel_Activity_Goal_vod__c =sumInd;
                        mcpc.SIQ_Channel_Activity_Max_vod__c = sumInd;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                          mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=sumMaxTeam;
                          mcpc.SIQ_Team_Channel_Activity_Max_vod__c=sumMaxTeam;
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=maxTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c=maxTeam;
                       }
                        
                        mcpc.SIQ_Channel_Activity_Goal_vod__c=0;
                        mcpc.SIQ_Channel_Activity_Max_vod__c = 0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                          mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=sumMaxTeam;
                          mcpc.SIQ_Team_Channel_Activity_Max_vod__c=sumMaxTeam;
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                            mcpc.SIQ_Team_Channel_Activity_Goal_vod__c=maxTeam;
                            mcpc.SIQ_Team_Channel_Activity_Max_vod__c=maxTeam;
                       }
                        mcpc.SIQ_Channel_Activity_Goal_vod__c =maxInd;
                        mcpc.SIQ_Channel_Activity_Max_vod__c = maxInd;
                    }
                        
                        system.debug('+++mcp.SIQ_Team_Channel_Interactions_Goal_vod__c'+mcpc.SIQ_Team_Channel_Activity_Goal_vod__c);
                        system.debug('+++mcp.SIQ_Team_Channel_Interactions_Goal_vod__c'+ mcpc.SIQ_Team_Channel_Activity_Max_vod__c);
                        system.debug('+++mcp.SIQ_Channel_Interactions_Max_vod__c'+ mcpc.SIQ_Channel_Activity_Goal_vod__c);
                        system.debug('+++mcp.SIQ_Product_Interactions_Goal_vod__c'+ mcpc.SIQ_Channel_Activity_Max_vod__c);
        
                        if(flag && !allRecs.contains(mcpc.SIQ_External_Id_vod__c))
                    {
                        mcpc.SIQ_RecordTypeId__c  = veevaCriteria[0].MC_Cycle_Plan_Channel_Record_Type__c;
        
                        
                        /*mcpc.SIQ_Channel_Activity_Goal_vod__c = Decimal.valueof(f2fCalls); //Confugurable
                        mcpc.SIQ_Channel_Activity_Max_vod__c = Decimal.valueof(f2fCalls);*/
                       if(allRecs.contains(mcpc.SIQ_External_Id_vod__c)){
                            system.debug(' if contains mcpc.SIQ_External_Id_vod__c===='+mcpc.SIQ_External_Id_vod__c);
                        }
                         mCyclePlanChannel.add(mcpc);
                        allRecs.add(mcpc.SIQ_External_Id_vod__c);
                    }
                    
            }        
        }
        //create_MC_Cycle_Plan_Product_vod();
    }
    
    global void execute(Database.BatchableContext BC, List<Parent_PACP__c> scopePacpProRecs)
    {
        create_MC_Cycle_Plan_Channel_vod(scopePacpProRecs);
        system.debug('List of mCyclePlanChannel ====');
        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mcplanChannel : mCyclePlanChannel){
            System.debug(mcplanChannel);
        }
        system.debug('External_ID_Axtria__c===='+mCyclePlanChannel);
        upsert mCyclePlanChannel External_ID_Axtria__c;
        //upsert mCyclePlanChannel;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //Database.executeBatch(new MarkMCCPchannelDeleted_EU(allTeamInstances, allChannels),2000);
    }
}