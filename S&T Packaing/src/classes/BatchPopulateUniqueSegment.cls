/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 20th August 2020
@description  : Controller for creating MCCP Unique Segment data 
@Revision(s)  : v1.0
**********************************************************************************************/
global with sharing class BatchPopulateUniqueSegment implements Database.Batchable<sObject>,Database.Stateful
{
	public String query;
    public String batchName;
    public String crID;
    public String alignNmsp;
    public String countryID;
    public String workspaceID;
    public String productID;
    public String prodRecordTypeID;
    public String uniqueSegRTID;
    public String errorMessage;
    public String externalID;
    public String securityQuery = 'WITH SECURITY_ENFORCED';

    public Boolean flag = true;

    public List<SObject> crRec;
    public Map<String,Set<String>> mapExtIDToSegments;

    //If user loads data via UI
    global BatchPopulateUniqueSegment(String changeReqID,String wsCountryID,String wsID) 
    {
        batchName = 'BatchPopulateUniqueSegment';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        //Initialise variables
        crRec = new List<SObject>();
        mapExtIDToSegments = new Map<String,Set<String>>();

        //Get ART Namespace
        alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        crID = changeReqID;
        countryID = wsCountryID;
        workspaceID = wsID;
        SnTDMLSecurityUtil.printDebugMessage('crID--'+crID);
        SnTDMLSecurityUtil.printDebugMessage('countryID--'+countryID);
        SnTDMLSecurityUtil.printDebugMessage('workspaceID--'+workspaceID);

        //Get Record Type ID for 'Product' in MCCP DataLoad object
        prodRecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Product').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('prodRecordTypeID--'+prodRecordTypeID);

        //Get Record Type ID for 'Unique Segment' in MCCP DataLoad object
        uniqueSegRTID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Segment').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('uniqueSegRTID--'+uniqueSegRTID);
        
        //Get CR associated with the Direct Load activity
        String crQuery = 'select id,'+alignNmsp+'Request_Type_Change__c,Records_Created__c '+
                         'from '+alignNmsp+'Change_Request__c where id =:crID '+securityQuery;
        crRec = Database.query(crQuery);
        SnTDMLSecurityUtil.printDebugMessage('crRec size--'+crRec.size());

        query = 'select Product__c,Workspace__c,Segment__c from MCCP_DataLoad__c where '+
                'Workspace__c=:workspaceID and RecordTypeId=:prodRecordTypeID '+securityQuery;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<MCCP_DataLoad__c> dataList) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        try
        {
        	Set<String> segmentSet;
        	for(MCCP_DataLoad__c rec : dataList)
        	{
        		if(!mapExtIDToSegments.containsKey(rec.Product__c)){
        			mapExtIDToSegments.put(rec.Product__c,new Set<String>{rec.Segment__c});
        		}

                System.debug('rec.Segment__c--'+rec.Segment__c);
        		if(!mapExtIDToSegments.get(rec.Product__c).contains(rec.Segment__c))
        		{
        			segmentSet = mapExtIDToSegments.get(rec.Product__c);
        			segmentSet.add(rec.Segment__c);
        			mapExtIDToSegments.put(rec.Product__c,segmentSet);
        		}
        	}
        	SnTDMLSecurityUtil.printDebugMessage('mapExtIDToSegments--'+mapExtIDToSegments);
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            flag = false;
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
    	SnTDMLSecurityUtil.printDebugMessage('mapExtIDToSegments--'+mapExtIDToSegments);

    	Savepoint sp = Database.setSavepoint();
    	Boolean noJobErrors;
        String changeReqStatus;
        MCCP_DataLoad__c newRec;
        Set<String> segmentSet;
        List<MCCP_DataLoad__c> insertRecList = new List<MCCP_DataLoad__c>();

    	try
    	{
	        if(!crRec.isEmpty())
	        {
	        	if(flag==true)
	        	{
	        		//Delete previous recs
			        String delQuery = 'select Id from MCCP_DataLoad__c where RecordTypeId=:uniqueSegRTID '+
			        					'and Workspace__c=:workspaceID '+securityQuery;
			        List<SObject> delRecsList = Database.query(delQuery);
			        if(!delRecsList.isEmpty())
			        {
			        	if(Schema.sObjectType.MCCP_CNProd__c.isDeletable()){
			                SnTDMLSecurityUtil.deleteRecords(delRecsList,batchName);
			            }
			        }

			        //create new recs now
			        for(String key : mapExtIDToSegments.keySet())
			        {
			        	segmentSet = mapExtIDToSegments.get(key);
			        	SnTDMLSecurityUtil.printDebugMessage('productID--'+key);
			        	SnTDMLSecurityUtil.printDebugMessage('segmentSet--'+segmentSet);

			        	newRec = new MCCP_DataLoad__c();
			        	newRec.Country__c = countryID;
			        	newRec.Workspace__c = workspaceID;
			        	newRec.Product__c = key;
			        	newRec.ExternalID__c = uniqueSegRTID+'_'+countryID+'_'+workspaceID+'_'+key;
			        	newRec.RecordTypeId = uniqueSegRTID;
			        	newRec.Unique_Segments__c = String.join(new List<String>(segmentSet),',');
			        	insertRecList.add(newRec);
			        }

			        SnTDMLSecurityUtil.printDebugMessage('insertRecList size--'+insertRecList.size());
			        if(!insertRecList.isEmpty()){
			        	SnTDMLSecurityUtil.insertRecords(insertRecList,batchName);
			        }
	        	}

	            //Update temp Status of recs
	            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
	            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

	            //Update temp obj recs as well as CR rec
	            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(crID,true,'Mandatory Fields Missing',changeReqStatus);
	            Database.executeBatch(batchCall,2000);
	        }
    	}
    	catch(Exception e)
    	{
    		SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in finish()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
    		Database.rollback(sp);
    	}
    }
}