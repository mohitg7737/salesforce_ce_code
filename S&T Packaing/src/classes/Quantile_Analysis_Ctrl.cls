global with sharing class Quantile_Analysis_Ctrl 
{

    public String countryID {get;set;}
    Map<string,string> successErrorMap;

    public String selectedCycle {get;set;}
    public String selectedBusinessUnit {get;set;}
    public String selectedMeasureId {get;set;}
    public String selectedStepId {get;set;}

    public List<SelectOption> businessUnits {get;set;}
    public List<SelectOption> quantileMeasures {get;set;}
    public List<SelectOption> quantileSteps {get;set;}
    public List<SelectOption> cycles {get;set;}

    public List<QuantileWrapper> quantileWrapperList {get;set;}
    public Boolean showTable {get;set;}

    public List<SelectOption> allSimulations { get; set;}
    public String selectedSimulation {get;set;}
    public transient List<SLDSPageMessage> PageMessages{get;set;}

    Boolean firstLoad;

    public Quantile_Analysis_Ctrl() 
    {
        try{
          /*  countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
            system.debug('##### countryID ' + countryID);
            successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
            system.debug('############ successErrorMap ' + successErrorMap);
            if(successErrorMap.containsKey('Success'))
            {
               countryID = successErrorMap.get('Success');               
               system.debug('########## countryID from Map ' + countryID);
               //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
               SalesIQUtility.setCookieString('CountryID',countryID);
            }*/
             countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
            SnTDMLSecurityUtil.printDebugMessage('countryID--'+countryID);
            
            allSimulations = new List<SelectOption>();
            createAllSimulationsList();
            selectedCycle = ApexPages.currentPage().getParameters().get('cycleId');
            selectedCycle = selectedCycle == null ? 'None' : selectedCycle;

            fillCycleOptions();


            fillBusinessUnitOptions();

            selectedBusinessUnit = ApexPages.currentPage().getParameters().get('teamInstanceId');
            selectedBusinessUnit = selectedBusinessUnit == null ? 'None' : selectedBusinessUnit;

            fillMeasureOptions();

            selectedMeasureId = ApexPages.currentPage().getParameters().get('ruleId');
            selectedMeasureId = selectedMeasureId == null ? 'None' : selectedMeasureId;

            quantileWrapperList = new List<QuantileWrapper>();
            showTable = false;
            firstLoad = true;
            
            fillStepOptions();   

            //loadParameterTable();
        }
        catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,'Quantile_Analysis_Ctrl',selectedMeasureId);
            PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
        }        
    }

    public void fillCycleOptions()
    {
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        List<AxtriaSalesIQTM__Workspace__c> allCycles = [SELECT Id, Name FROM AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__c = :countryID ];

        if(allCycles != null && allCycles.size() > 0)
        {
            //selectedCycle = allCycles[0].ID;

            for(AxtriaSalesIQTM__Workspace__c cycle: allCycles){
                cycles.add(new SelectOption(cycle.Id, cycle.Name));
            }            
        }
    }

    public void fillBusinessUnitOptions()
    {
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None', '--None--'));
        
        for(AxtriaSalesIQTM__Team_Instance__c bu: [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c = :countryID and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c= :selectedCycle])
        {
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }
        
    }

    public void fillMeasureOptions()
    {
        quantileMeasures = new list<SelectOption>();
        quantileMeasures.add(new SelectOption('None', '--None--'));
        
        for(Measure_Master__c bu: [SELECT Id, Name FROM Measure_Master__c where Team_Instance__c =:selectedBusinessUnit and /*(State__c = 'Executed' or State__c = 'Complete')*/ is_executed__c = true and State__c !='Execution Failed' and is_Deactivated__c = false and Rule_Type__c != 'MCCP'])
        {
            quantileMeasures.add(new SelectOption(bu.Id, bu.Name));
        }
        
    }

    public void fillStepOptions()
    {
        quantileSteps = new list<SelectOption>();
        quantileSteps.add(new SelectOption('None', '--None--'));
        Boolean flag = true;
        selectedStepId = 'None';
        for(Step__c step: [SELECT Id, Name FROM Step__c where Measure_Master__c = :selectedMeasureId and Step_Type__c = 'Quantile'])
        {
            quantileSteps.add(new SelectOption(step.Id, step.Name));
            if(flag)
            {
                selectedStepId = step.Id;
                flag = false;
            }
        }
        
        loadParameterTable();
    }


    public PageReference redirectToPage() //redirects to some other page
    {
        system.debug('selectedSimulation-->'+selectedSimulation);
        try{
            if (selectedSimulation == 'Segment Simulation') {
                /*PageReference pf = new PageReference('/apex/SegmentSimulation');
                pf.setRedirect(true);
                return pf;*/
                //A1422

                List<Measure_Master__c> ruleObject = new List<Measure_Master__c>();
                //publishFlag = true;
                //allSegments = new List<String>();
                System.debug('inside modelling---selectedMeasureId------'+selectedMeasureId);
                try{
                    ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, /*Line_2__c, Line_2__r.Name, Line_Lookup__r.Name,*/ isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, /*Cycle__c, Cycle__r.Name,*/ Team_Instance__r.Name,Team_Instance__c, All_Segments__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM Measure_Master__c WHERE Id=:selectedMeasureId and Country__c = :countryID limit 1];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                }
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
                    if(selectedMeasureId == 'None'){
                        SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,'');
                    }else{
                        SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,selectedMeasureId);
                    }
                    PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
                }
                String cycleId ='',teamInstanceID ='',brandID = '';
                if(ruleObject.size()>0){
                    //cycleId = ruleObject[0].Cycle__c;//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                    cycleId = ruleObject[0].Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                    teamInstanceID = ruleObject[0].Team_Instance__c;
                    brandID = ruleObject[0].Brand_Lookup__c;
                }
                
                System.debug('cycleId=='+cycleId+'==teamInstanceID==='+teamInstanceID+'===brandID=='+brandID);
                PageReference pf = new PageReference('/apex/SegmentSimulation?mode=Edit&rid=' + selectedMeasureId+'&cycleId='+cycleId+'&teamInstanceID='+teamInstanceID+'&brandID='+brandID);
                pf.setRedirect(true);
                return pf;
            } 
            else if (selectedSimulation == 'Workload Simulation') {
                // PageReference pf = new PageReference('/apex/WorkloadSimulation');
                // pf.setRedirect(true);
                // return pf;

                //A1422

                List<Measure_Master__c> ruleObject = new List<Measure_Master__c>();
                //publishFlag = true;
                //allSegments = new List<String>();
                System.debug('inside modelling---selectedMeasureId------'+selectedMeasureId);
                try{
                    ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, /*Line_2__c, Line_2__r.Name, Line_Lookup__r.Name,*/ isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, /*Cycle__c, Cycle__r.Name,*/ Team_Instance__r.Name,Team_Instance__c, All_Segments__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM Measure_Master__c WHERE Id=:selectedMeasureId and Country__c = :countryID];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                }
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
                    SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,selectedMeasureId);
                    PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
                }
                String cycleId ='',teamInstanceID ='',brandID = '';
                if(ruleObject.size()>0){
                     //cycleId = ruleObject[0].Cycle__c;//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                    cycleId = ruleObject[0].Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                     teamInstanceID = ruleObject[0].Team_Instance__c;
                     brandID = ruleObject[0].Brand_Lookup__c;
                }
                System.debug('cycleId=='+cycleId+'==teamInstanceID==='+teamInstanceID+'===brandID=='+brandID);
                PageReference pf = new PageReference('/apex/WorkloadSimulation?mode=Edit&rid=' + selectedMeasureId+'&cycleId='+cycleId+'&teamInstanceID='+teamInstanceID+'&brandID='+brandID);
                pf.setRedirect(true);
                return pf;
               
            }
            else if (selectedSimulation == 'Modelling') {
                /*PageReference pf = new PageReference('/apex/SimulationModelling');
                pf.setRedirect(true);
                return pf;*/
                 List<Measure_Master__c> ruleObject = new List<Measure_Master__c>();
                //publishFlag = true;
                //allSegments = new List<String>();
                System.debug('inside modelling---selectedMeasureId------'+selectedMeasureId);
                try{
                    ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, /*Line_2__c, Line_2__r.Name, Line_Lookup__r.Name,*/ isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, /*Cycle__c, Cycle__r.Name,*/ Team_Instance__r.Name,Team_Instance__c, All_Segments__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM Measure_Master__c WHERE Id=:selectedMeasureId and Country__c = :countryID];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                }
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
                    SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,selectedMeasureId);
                    PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
                }
                String cycleId ='',teamInstanceID ='',brandID = '';
                if(ruleObject.size()>0){
                     //cycleId = ruleObject[0].Cycle__c;//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                    cycleId = ruleObject[0].Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                     teamInstanceID = ruleObject[0].Team_Instance__c;
                     brandID = ruleObject[0].Brand_Lookup__c;
                }
                System.debug('cycleId=='+cycleId+'==teamInstanceID==='+teamInstanceID+'===brandID=='+brandID);
                PageReference pf = new PageReference('/apex/SimulationModelling?mode=Edit&rid=' + selectedMeasureId+'&cycleId='+cycleId+'&teamInstanceID='+teamInstanceID+'&brandID='+brandID);
                pf.setRedirect(true);
                return pf;
            }
            else if(selectedSimulation == 'Quantile Analysis'){
                 Measure_Master__c ruleObject = new Measure_Master__c();
                //publishFlag = true;
                //allSegments = new List<String>();
                System.debug('inside modelling---selectedMeasureId------'+selectedMeasureId);
                try{
                    ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, /*Line_2__c, Line_2__r.Name, Line_Lookup__r.Name,*/ isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, /*Cycle__c, Cycle__r.Name, */Team_Instance__r.Name,Team_Instance__c, All_Segments__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM Measure_Master__c WHERE Id=:selectedMeasureId and Country__c = :countryID];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                }
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
                    SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,selectedMeasureId);
                    PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
                }
                String cycleId = ruleObject.Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                String teamInstanceID = ruleObject.Team_Instance__c;
                String brandID = ruleObject.Brand_Lookup__c;
                System.debug('cycleId=='+cycleId+'==teamInstanceID==='+teamInstanceID+'===brandID=='+brandID);
                PageReference pf = new PageReference('/apex/Quantile_Analysis?mode=Edit&ruleId=' + selectedMeasureId+'&cycleId='+cycleId+'&teamInstanceId='+teamInstanceID+'&brandID='+brandID);
                pf.setRedirect(true);
                return pf;
            }
            else if(selectedSimulation == 'Comparative Analysis'){
                 Measure_Master__c ruleObject = new Measure_Master__c();
                //publishFlag = true;
                //allSegments = new List<String>();
                System.debug('inside modelling---selectedMeasureId------'+selectedMeasureId);
                try{
                    ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, /*Line_2__c, Line_2__r.Name, Line_Lookup__r.Name,*/ isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, /*Cycle__c, Cycle__r.Name, */Team_Instance__r.Name,Team_Instance__c, All_Segments__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM Measure_Master__c WHERE Id=:selectedMeasureId and Country__c = :countryID];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                }            
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
                    SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,selectedMeasureId);
                    PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
                }
                String cycleId = ruleObject.Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                String teamInstanceID = ruleObject.Team_Instance__c;
                String brandID = ruleObject.Brand_Lookup__c;
                System.debug('cycleId=='+cycleId+'==teamInstanceID==='+teamInstanceID+'===brandID=='+brandID);
                PageReference pf = new PageReference('/apex/Compare_Analysis?mode=Edit&ruleId=' + selectedMeasureId+'&cycleId='+cycleId+'&teamInstanceId='+teamInstanceID+'&brandID='+brandID);
                pf.setRedirect(true);
                return pf;
            }
            else {
                return null;
            }
        }
        catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,'Quantile_Analysis_Ctrl',selectedMeasureId);
            PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
            return null;
        }
    }

    //working
    public void createAllSimulationsList(){
        selectedSimulation = 'Quantile Analysis';
        allSimulations.add(new SelectOption('Segment Simulation', 'Segment Simulation'));
        allSimulations.add(new SelectOption('Workload Simulation', 'Workload Simulation'));
        allSimulations.add(new SelectOption('Modelling', 'Modelling'));
        allSimulations.add(new SelectOption('Quantile Analysis', 'Quantile Analysis'));
        allSimulations.add(new SelectOption('Comparative Analysis', 'Comparative Analysis'));
    }

    public void loadParameterTable()
    {
        System.debug('selected cycle ---> ' + selectedCycle);
        System.debug('selectedBusinessUnit ---> ' + selectedBusinessUnit);
        System.debug('selectedMeasureId ---> ' + selectedMeasureId);
        System.debug('selectedStepId ---> ' + selectedStepId);
        try{
            quantileWrapperList = new List<QuantileWrapper>();
            String quantile_grand = 'Grand Total';
            Integer quantile_count_grand = 0;
            Decimal quantile_sum_grand = 0.0;
            Decimal quantile_avg_grand = 0.0;
            Decimal quantile_min_grand = 1000000000000.0;
            Decimal quantile_max_grand = -999999.0;

            List<Segment_Simulation_Copy__c> sscList = [Select Quantile_Average__c,Quantile_Count__c,Quantile_Max__c,Quantile_Min__c,Quantile_Sum__c,Quantile__c from Segment_Simulation_Copy__c where Quantile_Measure_Master__c =:selectedMeasureId and Quantile_Step__c =:selectedStepId order by Quantile__c asc];
            if(sscList.size() > 0)
            {
                for(Segment_Simulation_Copy__c ssc : sscList)
                {
                    quantileWrapperList.add(new QuantileWrapper(String.valueOf(Integer.valueOf(ssc.Quantile__c)),Integer.valueOf(ssc.Quantile_Count__c),ssc.Quantile_Sum__c,ssc.Quantile_Average__c,ssc.Quantile_Min__c,ssc.Quantile_Max__c));
                    quantile_count_grand += Integer.valueOf(ssc.Quantile_Count__c);
                    quantile_sum_grand += ssc.Quantile_Sum__c;
                    if(ssc.Quantile_Min__c < quantile_min_grand)
                        quantile_min_grand = ssc.Quantile_Min__c;
                    if(ssc.Quantile_Max__c > quantile_max_grand)
                        quantile_max_grand = ssc.Quantile_Max__c;
                }
                if(quantile_count_grand > 0)
                {
                    quantile_avg_grand = quantile_sum_grand/quantile_count_grand;
                }
                quantileWrapperList.sort();
                quantileWrapperList.add(new QuantileWrapper(quantile_grand,quantile_count_grand,quantile_sum_grand,quantile_avg_grand,quantile_min_grand,quantile_max_grand));
                showTable = true;
            }
            else if(!firstLoad)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There is no data available for selected Step. Kindly contact the System Administrator.'));
                PageMessages = SLDSPageMessage.add(PageMessages,'There is no data available for selected Step. Kindly contact the System Administrator.','info');
                showTable = false;
            }
        }
        catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.BR_MODULE,'Quantile_Analysis_Ctrl',selectedMeasureId);
            PageMessages = SLDSPageMessage.add(PageMessages,'Some Unexpected Error has occured,please contact your system admin','Error');
        }
    }

    global class QuantileWrapper implements Comparable
    {
        public String quantile                 {get;set;}
        public Integer quantile_count          {get;set;}
        public Decimal quantile_sum            {get;set;}
        public Decimal quantile_avg            {get;set;}
        public Decimal quantile_min            {get;set;}
        public Decimal quantile_max            {get;set;}

        public QuantileWrapper(String quantile,Integer quantile_count,Decimal quantile_sum,Decimal quantile_avg,Decimal quantile_min,Decimal quantile_max)
        {
            this.quantile = quantile;
            this.quantile_count = quantile_count;
            this.quantile_sum = quantile_sum.setScale(2);
            this.quantile_avg = quantile_avg.setScale(2);
            this.quantile_min = quantile_min.setScale(2);
            this.quantile_max = quantile_max.setScale(2);
        }

        global Integer compareTo(Object objToCompare) {
            return Integer.valueOf(quantile) - Integer.valueOf(((QuantileWrapper)objToCompare).quantile);
        }
    }
}