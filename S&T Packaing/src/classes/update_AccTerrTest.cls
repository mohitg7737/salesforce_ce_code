@isTest
public class update_AccTerrTest {

    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'Test ac';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.Type = 'HCP';
        insert acc;
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'OverlappingWorkspace';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        temp_Obj__c zt = new temp_Obj__c();
        zt.status__c = 'New';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c=acc.Type;
        zt.Team_Name__c = team.Name;
        zt.Object__c = 'Acc_Terr';
        insert zt;
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdateAccTerr obj=new UpdateAccTerr(team.Id,teamins.id,cr.Id);
            //obj.query = 'SELECT id,Territory_ID__c,AccountNumber__c,Team_Name__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
       static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'Test ac';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.Type = 'HCP';
        insert acc;
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'OverlappingWorkspace';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        temp_Obj__c zt = new temp_Obj__c();
        zt.status__c = 'New';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c=null;
        zt.Team_Name__c = team.Name;
        zt.Object__c = 'Acc_Terr';
        insert zt;
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdateAccTerr obj=new UpdateAccTerr(team.Id,teamins.id,cr.Id);
            
            //obj.query = 'SELECT id,Territory_ID__c,AccountNumber__c,Team_Name__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
}