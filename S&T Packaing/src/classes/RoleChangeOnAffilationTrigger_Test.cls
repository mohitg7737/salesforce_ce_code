@isTest
public class RoleChangeOnAffilationTrigger_Test {
   static testMethod void RoleChangeOnAffilationTriggerAfterUpdate_Test(){
     
    Test.startTest();
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    AxtriaSalesIQTM__TriggerContol__c aaa=new AxtriaSalesIQTM__TriggerContol__c(Name='RoleChangeOnAffilation',AxtriaSalesIQTM__IsStopTrigger__c=false);
    insert aaa;
    System.debug('fffff'+aaa);
    
    if(!AxtriaSalesIQTM__TriggerContol__c.getValues('RoleChangeOnAffilation').AxtriaSalesIQTM__IsStopTrigger__c){
       Account aa=new Account(Name = 'testAccount',AccountNumber = 'TEST1111',Marketing_Code__c = 'IN',Role_Name__c = 'Doctor');
       insert aa;
       
       System.debug('fffgggff'+aa);
       AxtriaSalesIQTM__Affiliation_Network__c accNt =new AxtriaSalesIQTM__Affiliation_Network__c(name ='xyz',AxtriaSalesIQTM__Hierarchy_Level__c = '6');
       insert accNt;
       
       System.debug('ffffjkjhf'+accNt);
       AxtriaSalesIQTM__Account_Affiliation__c aff = new AxtriaSalesIQTM__Account_Affiliation__c(AxtriaSalesIQTM__Affiliation_Network__c = accNt.id,AxtriaSalesIQTM__Active__c = true,Account_Number__c = 'TEST1111',Role_Name__c = 'Doctor', AxtriaSalesIQTM__Affiliation_Type__c = 'HCP to HCA');
       insert aff;
       system.debug('HHHHH' + aff);
       List<AxtriaSalesIQTM__Account_Affiliation__c> aff1=new List<AxtriaSalesIQTM__Account_Affiliation__c>();
       for(AxtriaSalesIQTM__Account_Affiliation__c aff2 : [Select id from AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c ='TEST1111' and Role_Name__c != null and AxtriaSalesIQTM__Active__c = true AND AxtriaSalesIQTM__Affiliation_Type__c = 'HCP to HCA' AND  (Role_Name__c != null OR Role_Name__c != 'NA')] )
       {
           aff2.AxtriaSalesIQTM__Active__c = false;
           aff1.add(aff2);
           
       }
       System.debug('ffghafff'+aff1);
       
       update aff1;
   }
   
   Test.stopTest();
}

}