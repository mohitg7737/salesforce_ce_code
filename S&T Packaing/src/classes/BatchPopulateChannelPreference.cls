/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 14th August 2020
@description  : Controller for creating MCCP Channel Preference data 
@Revision(s)  : v1.0
**********************************************************************************************/
global with sharing class BatchPopulateChannelPreference implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    public String batchName;
    public String crID;
    public String alignNmsp;
    public String chPrefRecordTypeID;
    public String errorMessage;
    public String processName;
    public String wsCountryID;
    public String newStatus = 'New';
    public String securityQuery = 'WITH SECURITY_ENFORCED';
    public String processedLabel = System.Label.MCCP_Direct_Load_Processed_Label;
    public String rejectedLabel = System.Label.MCCP_Direct_Load_Rejected_Label;

    public Integer recsProcessed = 0;
    public Integer recsTotal = 0;

    public Boolean flag = true;
    public Boolean isRecValid;

    public Set<String> accountSet;
    public Set<String> productSet;
    public Set<String> workspaceSet;
    public Set<String> externalIDSet;

    public List<SObject> crRec;
    public List<MCCP_DataLoad__c> channelPrefList;

    public Transient Map<String,String> mapAccNoToSFID;
    public Transient Map<String,String> mapProdCodeToSFID;
    public Transient Map<String,SObject> mapWSNameToRec;
    public Map<String,Set<String>> mapCountryIDToChannels;

    //If user populates data in Temp object
    global BatchPopulateChannelPreference(String changeReqID,String objectName,String countryID) 
    {
        batchName = 'BatchPopulateChannelPreference';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        //Initialise Lists
        crRec = new List<SObject>();
        channelPrefList = new List<MCCP_DataLoad__c>();
        crID = changeReqID;
        processName = objectName;
        wsCountryID = countryID;
        SnTDMLSecurityUtil.printDebugMessage('crID--'+crID);
        SnTDMLSecurityUtil.printDebugMessage('processName--'+processName);

        //Get ART Namespace
        alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        //Get Record Type ID for 'Channel Preference' in MCCP DataLoad object
        chPrefRecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Channel Preference').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('chPrefRecordTypeID--'+chPrefRecordTypeID);
        
        //Get CR associated with the Direct Load activity
        String crQuery = 'select id,'+alignNmsp+'Request_Type_Change__c,Records_Created__c '+
                         'from '+alignNmsp+'Change_Request__c where id =:crID '+securityQuery;
        crRec = Database.query(crQuery);
        SnTDMLSecurityUtil.printDebugMessage('crRec size--'+crRec.size());

        //Create the batch query
        query = 'select Id,AccountNumber__c,Workspace__c,Product_Code__c,Channel_Name__c,isError__c,'+
                'Channel_Preference__c,status__c,Error_Message__c,SalesIQ_Error_Message__c,Change_Request__c '+
                'from temp_Obj__c where Status__c =:newStatus and Object_Name__c=:processName '+securityQuery+
                ' order by AccountNumber__c,Product_Code__c,Channel_Name__c';

        //Create Country-Channel Map
        createCountryChannelMap();
    }

    //If user loads data via UI
    global BatchPopulateChannelPreference(String changeReqID,String countryID) 
    {
        batchName = 'BatchPopulateChannelPreference';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        //Initialise Lists
        crRec = new List<SObject>();
        channelPrefList = new List<MCCP_DataLoad__c>();
        crID = changeReqID;
        wsCountryID = countryID;
        SnTDMLSecurityUtil.printDebugMessage('crID--'+crID);

        //Get ART Namespace
        alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        //Get Record Type ID for 'Channel Preference' in MCCP DataLoad object
        chPrefRecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Channel Preference').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('chPrefRecordTypeID--'+chPrefRecordTypeID);
        
        //Get CR associated with the Direct Load activity
        String crQuery = 'select id,'+alignNmsp+'Request_Type_Change__c,Records_Created__c '+
                         'from '+alignNmsp+'Change_Request__c where id =:crID '+securityQuery;
        crRec = Database.query(crQuery);
        SnTDMLSecurityUtil.printDebugMessage('crRec size--'+crRec.size());

        //Create the batch query
        query = 'select Id,AccountNumber__c,Workspace__c,Product_Code__c,Channel_Name__c,isError__c,'+
                'Channel_Preference__c,status__c,Error_Message__c,SalesIQ_Error_Message__c,Change_Request__c '+
                'from temp_Obj__c where Change_Request__c=:crID '+securityQuery+
                ' order by AccountNumber__c,Product_Code__c,Channel_Name__c';

        //Create Country-Channel Map
        createCountryChannelMap();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }

    global void createCountryChannelMap()
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : createCountryChannelMap() invoked--');

        mapCountryIDToChannels = new Map<String,Set<String>>();
        List<Channel_Info__c> listChannels = [select Country__c,Channel_Name__c from 
                                                Channel_Info__c WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('listChannels size--'+listChannels.size());
        if(!listChannels.isEmpty())
        {
            for(Channel_Info__c ci : listChannels)
            {
                if(!mapCountryIDToChannels.containsKey(ci.Country__c)){
                    mapCountryIDToChannels.put(ci.Country__c,new Set<String>());
                }
                mapCountryIDToChannels.get(ci.Country__c).add(ci.Channel_Name__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('mapCountryIDToChannels--'+mapCountryIDToChannels);
        }
    }

    global void initialiseVariables()
    {
        channelPrefList = new List<MCCP_DataLoad__c>();

        accountSet = new Set<String>();
        productSet = new Set<String>();
        workspaceSet = new Set<String>();
        externalIDSet = new Set<String>();

        mapAccNoToSFID = new Map<String,String>();
        mapProdCodeToSFID = new Map<String,String>();
        mapWSNameToRec = new Map<String,SObject>();
    }

    public Boolean isNumericString(String str)
    {
        try{
            Decimal d = Decimal.valueOf(str);
        }
        catch(Exception e){
            return false;
        }
        return true;
    }

    global void execute(Database.BatchableContext BC,List<temp_Obj__c> tempObjList) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('mapCountryIDToChannels--'+mapCountryIDToChannels);
        recsTotal+=tempObjList.size();

        initialiseVariables(); //Initialise list vars

        try
        {
            //first get unique Products and Accounts
            for(temp_Obj__c rec1 : tempObjList)
            {
                //This is for aligning all temp obj recs to a CR
                if(processName!=null){
                    rec1.Change_Request__c = crID;
                }

                //Check Channel Name should not be null  
                if(String.isBlank(rec1.Channel_Name__c))
                {
                    rec1.status__c = rejectedLabel;
                    rec1.isError__c = true;
                    rec1.Error_Message__c = System.Label.MCCP_Direct_Load_Channel_Blank;
                    rec1.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Channel_Blank;
                    continue;
                }

                //Check Channel Preference should not be null
                if(String.isBlank(rec1.Channel_Preference__c))
                {
                    rec1.status__c = rejectedLabel;
                    rec1.isError__c = true;
                    rec1.Error_Message__c = System.Label.MCCP_Direct_Load_Preference_Blank;
                    rec1.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Preference_Blank;
                    continue;
                }

                //Check Channel Preference should be numeric  
                if(!isNumericString(rec1.Channel_Preference__c))
                {
                    rec1.status__c = rejectedLabel;
                    rec1.isError__c = true;
                    rec1.Error_Message__c = System.Label.MCCP_Direct_Load_Preference_Non_Numeric;
                    rec1.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Preference_Non_Numeric;
                    continue;
                }

                //Check Accs
                if(rec1.AccountNumber__c!=null && rec1.AccountNumber__c!=''){
                    accountSet.add(rec1.AccountNumber__c);
                }
                else
                {
                    rec1.status__c = rejectedLabel;
                    rec1.isError__c = true;
                    rec1.Error_Message__c = System.Label.MCCP_Direct_Load_Account_Blank;
                    rec1.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Account_Blank;
                    continue;
                }

                //Check Product codes
                if(rec1.Product_Code__c!=null && rec1.Product_Code__c!=''){
                    productSet.add(rec1.Product_Code__c);
                }

                //Check Workspaces
                //Commented since Channel data would be at a country level
                /*if(rec1.Workspace__c!=null && rec1.Workspace__c!=''){
                    workspaceSet.add(rec1.Workspace__c);
                }
                else
                {
                    rec1.status__c = 'Rejected';
                    rec1.isError__c = true;
                    rec1.Error_Message__c = 'Workspace Name is blank/null';
                    rec1.SalesIQ_Error_Message__c = 'Workspace Name is blank/null';
                    continue;
                }*/
            }
            SnTDMLSecurityUtil.printDebugMessage('accountSet--'+accountSet.size());
            SnTDMLSecurityUtil.printDebugMessage('productSet--'+productSet.size());

            //Get Account SFID for the existing Accs
            for(Account acc : [select id,AccountNumber from Account where AccountNumber 
                                 in :accountSet WITH SECURITY_ENFORCED])
            {
                if(!mapAccNoToSFID.containsKey(acc.AccountNumber)){
                    mapAccNoToSFID.put(acc.AccountNumber,acc.Id);
                }
            }
            accountSet.clear();
            SnTDMLSecurityUtil.printDebugMessage('mapAccNoToSFID keySet()--'+mapAccNoToSFID.keySet().size());

            //Get Prod Catalog SFID for the existing Accs
            for(Product_Catalog__c pc : [select id,Product_Code__c from Product_Catalog__c where
                                          Product_Code__c in :productSet and Team_Instance__c = null 
                                          and IsActive__c = true WITH SECURITY_ENFORCED])
            {
                if(!mapProdCodeToSFID.containsKey(pc.Product_Code__c)){
                    mapProdCodeToSFID.put(pc.Product_Code__c,pc.Id);
                }
            }
            productSet.clear();
            SnTDMLSecurityUtil.printDebugMessage('mapProdCodeToSFID keySet()--'+mapProdCodeToSFID.keySet().size());

            //Get Workspace ID and Country ID for the recs
            //Commented since Channel data is at Country level
            /*String wsQuery = 'select Id,Name,'+alignNmsp+'Country__c from '+alignNmsp+
                             'Workspace__c where Name in :workspaceSet and '+alignNmsp+
                             'Country__c!=null '+securityQuery;
            List<SObject> wsList = Database.query(wsQuery);
            workspaceSet.clear();
            SnTDMLSecurityUtil.printDebugMessage('wsList size--'+wsList.size());

            if(!wsList.isEmpty())
            {
                for(SObject ws : wsList){
                    mapWSNameToRec.put((String)ws.get('Name'),ws);
                }
            }
            wsList.clear();
            SnTDMLSecurityUtil.printDebugMessage('mapWSNameToRec keySet()--'+mapWSNameToRec.keySet().size());*/

            //Now loop through the whole dataset
            MCCP_DataLoad__c dataLoadRec;
            Sobject tempWSRec;
            String wsID;
            String countryID;
            Transient Set<String> countryChannels = new Set<String>();

            for(temp_Obj__c tempObj : tempObjList)
            {
                isRecValid = true;
                errorMessage = '';

                if(!tempObj.isError__c)
                {
                    dataLoadRec = new MCCP_DataLoad__c();
                    dataLoadRec.Channel_Preference__c = tempObj.Channel_Preference__c;
                    dataLoadRec.RecordTypeId = chPrefRecordTypeID;
                    dataLoadRec.ExternalID__c = chPrefRecordTypeID+'_';

                    //Check for Account
                    if(mapAccNoToSFID.get(tempObj.AccountNumber__c)!=null){
                        dataLoadRec.HCP__c = mapAccNoToSFID.get(tempObj.AccountNumber__c);
                        dataLoadRec.ExternalID__c += dataLoadRec.HCP__c+'_';
                    }
                    else
                    {
                        tempObj.status__c = rejectedLabel;
                        tempObj.isError__c = true;
                        tempObj.Error_Message__c = System.Label.MCCP_Direct_Load_Account_Unavailable;
                        tempObj.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Account_Unavailable;
                        continue;
                    }

                    //Check for Product
                    if(tempObj.Product_Code__c!=null && tempObj.Product_Code__c!='')
                    {
                        if(mapProdCodeToSFID.get(tempObj.Product_Code__c)!=null){
                            dataLoadRec.Product__c = mapProdCodeToSFID.get(tempObj.Product_Code__c);
                            dataLoadRec.ExternalID__c += dataLoadRec.Product__c+'_';
                        }
                        else
                        {
                            tempObj.status__c = rejectedLabel;
                            tempObj.isError__c = true;
                            tempObj.Error_Message__c = System.Label.MCCP_Direct_Load_Product_Unavailable;
                            tempObj.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Product_Unavailable;
                            continue;
                        }
                    }

                    //Check for Workspace
                    //Commented since Channel Data is at Country Level
                    /*if(mapWSNameToRec.get(tempObj.Workspace__c)!=null)
                    {
                        tempWSRec = mapWSNameToRec.get(tempObj.Workspace__c);

                        dataLoadRec.Workspace__c = (String)tempWSRec.get('Id');
                        dataLoadRec.ExternalID__c += dataLoadRec.Workspace__c+'_';

                        dataLoadRec.Country__c = (String)tempWSRec.get(alignNmsp+'Country__c');
                        dataLoadRec.ExternalID__c += dataLoadRec.Country__c+'_';
                        countryID = dataLoadRec.Country__c;
                    }
                    else
                    {
                        tempObj.status__c = 'Rejected';
                        tempObj.isError__c = true;
                        tempObj.Error_Message__c = 'Workspace not found in the system';
                        tempObj.SalesIQ_Error_Message__c = 'Workspace not found in the system';
                        continue;
                    }*/

                    //Check Channels within a country
                    if(mapCountryIDToChannels.get(wsCountryID)!=null)
                    {
                        countryChannels = mapCountryIDToChannels.get(wsCountryID);
                        if(!countryChannels.contains(tempObj.Channel_Name__c))
                        {
                            tempObj.status__c = rejectedLabel;
                            tempObj.isError__c = true;
                            tempObj.Error_Message__c = System.Label.MCCP_Direct_Load_Channel_Unavailable;
                            tempObj.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Channel_Unavailable;
                            continue;
                        }
                        else
                        {
                            dataLoadRec.Country__c = wsCountryID;
                            dataLoadRec.ExternalID__c += dataLoadRec.Country__c+'_';
                            dataLoadRec.Channel_Name__c = tempObj.Channel_Name__c;
                            dataLoadRec.ExternalID__c += dataLoadRec.Channel_Name__c;
                        }
                    }
                    else
                    {
                        tempObj.status__c = rejectedLabel;
                        tempObj.isError__c = true;
                        tempObj.Error_Message__c = System.Label.MCCP_Direct_Load_Channels_Unavailable;
                        tempObj.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Channels_Unavailable;
                        continue;
                    }

                    if(isRecValid)
                    {
                        if(!externalIDSet.contains(dataLoadRec.ExternalID__c))
                        {
                            tempObj.status__c = processedLabel;
                            tempObj.isError__c = false;
                            tempObj.Error_Message__c = '';
                            tempObj.SalesIQ_Error_Message__c = '';
                            externalIDSet.add(dataLoadRec.ExternalID__c);
                            channelPrefList.add(dataLoadRec);
                        }
                        else
                        {
                            tempObj.status__c = rejectedLabel;
                            tempObj.isError__c = true;
                            tempObj.Error_Message__c = System.Label.MCCP_Direct_Load_Duplicate_Record;
                            tempObj.SalesIQ_Error_Message__c = System.Label.MCCP_Direct_Load_Duplicate_Record;
                        }
                    }
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('channelPrefList size--'+channelPrefList.size());
            if(!channelPrefList.isEmpty())
            {
                Schema.SObjectField f = MCCP_DataLoad__c.Fields.ExternalID__c;
                List<Database.UpsertResult> ds = Database.upsert(channelPrefList,f,false);

                for(Database.UpsertResult d : ds)
                {
                    if(d.isSuccess()){   
                        recsProcessed++;
                    }
                    else
                    {
                        flag = false;
                        for(Database.Error err : d.getErrors()) {
                            SnTDMLSecurityUtil.printDebugMessage('The following error has occurred.');                    
                            SnTDMLSecurityUtil.printDebugMessage(err.getStatusCode() + ': ' + err.getMessage());
                            SnTDMLSecurityUtil.printDebugMessage('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }

            if(!tempObjList.isEmpty()){
                SnTDMLSecurityUtil.updateRecords(tempObjList,batchName);
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            flag = false;
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');

        Boolean noJobErrors;
        String changeReqStatus;

        if(!crRec.isEmpty())
        {
            SObject crRec = crRec[0];
            crRec.put('Records_Updated__c',recsProcessed);

            if((String)crRec.get(alignNmsp+'Request_Type_Change__c')=='Data Load Backend'){
                crRec.put('Records_Created__c',recsTotal);
            }
            SnTDMLSecurityUtil.updateRecords(crRec,batchName);

            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

            if(changeReqStatus=='Done')
            {
                //Get the unique Product-Channel combo in a particular country
                BatchPopulateUniqueChannelPref batchCall = new BatchPopulateUniqueChannelPref(crID,wsCountryID);
                Database.executeBatch(batchCall,2000);
            }
            else
            {
                //Update temp obj recs as well as CR rec
                BatchUpdateTempObjRecsCR batchCall2 = new BatchUpdateTempObjRecsCR(crID,true,System.Label.MCCP_Direct_Load_Mandatory_Field_Missing,changeReqStatus);
                Database.executeBatch(batchCall2,2000);
            }
        }
    }
}