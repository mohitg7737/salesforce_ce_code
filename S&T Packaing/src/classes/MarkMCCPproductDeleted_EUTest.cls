/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test MarkMCCPproductDeleted_EU.
*/
@isTest
private class MarkMCCPproductDeleted_EUTest {
    static testMethod void testMethod1() {
       User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
         AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        SIQ_MC_Cycle_Plan_Channel_vod_O__c ss = new SIQ_MC_Cycle_Plan_Channel_vod_O__c ();
        insert ss;
        SIQ_MC_Cycle_Plan_Product_vod_O__c v = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
        v.Team_Instance__c = teamins.Id;
        v.Rec_Status__c = 'Deleted';
        v.Position__c = pos.id;
        v.Account__c = acc.id;
        v.SIQ_Cycle_Plan_Channel_vod__c = ss.id;
        insert v;
        Scheduler_Log__c s = new Scheduler_Log__c();
        s.Job_Type__c = 'Survey Data Load';
        s.Job_Status__c = 'Failed';
        insert s;
        
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        etl.Name = 'Survey Data Load';
        insert etl;
        
        list<String> teamInstanceList = new list<String>();
        teamInstanceList.add(teamins.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
                    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            MarkMCCPproductDeleted_EU obj=new MarkMCCPproductDeleted_EU(teamInstanceList);
            obj.query = 'select id, Team_Instance__c, Account__c, Position__c, SIQ_Cycle_Plan_Channel_vod__c from SIQ_MC_Cycle_Plan_Product_vod_O__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
       User loggedInUser = new User(id=UserInfo.getUserId());
                insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
         AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Product_Name__c ='GIST';
        pPriority.Speciality_Name__c ='Cardiology';
        pPriority.CycleName__c = 'Oncology';
        
        pPriority.Product_Type__c ='Speciality';
        
        pPriority.Product__c = pcc.id;
        pPriority.TeamInstance__c =teamins.id;
        pPriority.Type__c ='Territory';
        pPriority.Position_Name__c =pos.Name;
        pPriority.TeamInstance__c =teamins.id;
        insert pPriority;
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        SIQ_MC_Cycle_Plan_Channel_vod_O__c ss = new SIQ_MC_Cycle_Plan_Channel_vod_O__c ();
        insert ss;
        SIQ_MC_Cycle_Plan_Product_vod_O__c v = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
        v.Team_Instance__c = teamins.Id;
        v.Rec_Status__c = 'Deleted';
        v.Position__c = pos.id;
        v.Account__c = acc.id;
        v.SIQ_Cycle_Plan_Channel_vod__c = ss.id;
        insert v;
        
        Scheduler_Log__c s = new Scheduler_Log__c();
        s.Job_Type__c = 'Veeva Full Load';
        s.Job_Status__c = 'Failed';
        s.Team_Instance__c = teamins.id;
        insert s;
        
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        etl.Name = 'Veeva Full Load';
        insert etl;
        
        list<String> teamInstanceList = new list<String>();
        teamInstanceList.add(teamins.id);
        Set<String> activityLogSet = new Set<String>();
        activityLogSet.add(s.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
                    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            MarkMCCPproductDeleted_EU obj=new MarkMCCPproductDeleted_EU(teamInstanceList,activityLogSet);
            obj.query = 'select id, Team_Instance__c, Account__c, Position__c, SIQ_Cycle_Plan_Channel_vod__c from SIQ_MC_Cycle_Plan_Product_vod_O__c ';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}