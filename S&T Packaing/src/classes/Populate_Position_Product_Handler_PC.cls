public with sharing class Populate_Position_Product_Handler_PC 
{
    public Populate_Position_Product_Handler_PC()
    {
        
    }
    
    public static void populatePosition(List<Product_Catalog__c> allPosProducts)
    {
        set<string>teaminstanceset = new set<string>();
        set<string>productset = new set<string>();
        
        //Purge begins
        //List<Brand_Team_Instance__c>brandteamlist = [Select id,Name,Brand__c,Brand__r.Name,Team_Instance__c from Brand_Team_Instance__c ];
        //Set<String>Brand_TeamInstance = new set<String>();
        
        //map<string,Brand_Team_Instance__c>brandteammap =new map<string,Brand_Team_Instance__c>();

        /*for(Brand_Team_Instance__c br : brandteamlist)
        {
            Brand_TeamInstance.add(br.Brand__c+'_'+br.Team_Instance__c);
            brandteammap.put(br.Brand__c+'_'+br.Team_Instance__c,br);
        }*/

        //Purge ends


        /* Added by siva to ristrict duplicate position products */
        for(Product_Catalog__c prod : allPosProducts)
        {
            if(prod.Team_Instance__c!=null)
             teaminstanceset.add(prod.Team_Instance__c);
             productset.add(prod.Id);
        }
        SnTDMLSecurityUtil.printDebugMessage('productset++++'+productset);
        set<string>uniqset =new set<string>();

        List<Team_Instance_Product_AZ__c> ExistingTeamInstProdAZ =[select id, Product_Catalogue__c, Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c in :teaminstanceset and Product_Catalogue__c in :productset WITH SECURITY_ENFORCED];

        if(ExistingTeamInstProdAZ!=null && ExistingTeamInstProdAZ.size()>0)
        {
            for(Team_Instance_Product_AZ__c az:ExistingTeamInstProdAZ)
            {
                string key=az.Team_Instance__c+'_'+az.Product_Catalogue__c;
                uniqset.add(key);
            }
            SnTDMLSecurityUtil.printDebugMessage('uniqset++++'+uniqset);
        }

        //purge begins
        /*List<Brand_Team_Instance__c>insertbrdteam = new list<Brand_Team_Instance__c>();
        for(Product_Catalog__c prod : allPosProducts)
        {
            if(!Brand_TeamInstance.contains(Prod.Id+'_'+prod.Team_Instance__c))
            {
                Brand_Team_Instance__c newBt = new Brand_Team_Instance__c();
                newbt.Name = prod.Name;
                newbt.Brand__c = prod.Id;
                newbt.Team_Instance__c = prod.Team_Instance__c;
                insertbrdteam.add(newbt);
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('insertbrdteam++++'+insertbrdteam);
        insert insertbrdteam;
        */
        //purge ends
        
        List<Team_Instance_Product_AZ__c> TeamInstProdAZToInsert = new List<Team_Instance_Product_AZ__c>();
        Team_Instance_Product_AZ__c TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
        for(Product_Catalog__c tip : allPosProducts)
        {
            if(tip.Product_Type__c == 'Product')
            {
                String key2 =tip.Team_Instance__c+'_'+tip.id;
                SnTDMLSecurityUtil.printDebugMessage('key2++++');

                if(!uniqset.contains(key2) && tip.Team_Instance__c!=null)
                {
                    SnTDMLSecurityUtil.printDebugMessage('hey++++');
                    TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
                    TeamInstProdAZRec.Product_Catalogue__c=tip.id;
                    TeamInstProdAZRec.Team_Instance__c=tip.Team_Instance__c;
                    TeamInstProdAZRec.Vacation_Days__c = 0;
                    TeamInstProdAZRec.Holidays__c = 0;
                    TeamInstProdAZRec.Calls_Day__c = 0;
                    TeamInstProdAZRec.Other_Days_Off__c = 0;   
                    TeamInstProdAZRec.Full_Load__c = tip.Full_Load__c;
                    TeamInstProdAZToInsert.add(TeamInstProdAZRec);                

                }
                SnTDMLSecurityUtil.printDebugMessage('TeamInstProdAZToInsert++++'+TeamInstProdAZToInsert);
            }

        }

        //insert TeamInstProdAZToInsert;
        SnTDMLSecurityUtil.insertRecords(TeamInstProdAZToInsert, 'Populate_Position_Product_Handler_PC');
    }

    public static void deletePosition(List<Product_Catalog__c> allPosProducts)
    {
        SnTDMLSecurityUtil.printDebugMessage('allPosProducts++++++'+allPosProducts);
        Set<String> allTeamInstances = new Set<String>();
        Set<String> allProducts = new Set<String>();
        Set<String> posProdCombo = new Set<String>();

        for(Product_Catalog__c ti :allPosProducts)
        {
            allTeamInstances.add(ti.Team_Instance__c);
            allProducts.add(ti.ID);
            posProdCombo.add(ti.Team_Instance__c + '_' + ti.ID);
        }
        SnTDMLSecurityUtil.printDebugMessage('allTeamInstances++++++'+allTeamInstances);
        SnTDMLSecurityUtil.printDebugMessage('allProducts++++++'+allProducts);
        SnTDMLSecurityUtil.printDebugMessage('posProdCombo++++++'+posProdCombo);

        List<Team_Instance_Product_AZ__c> TeamInstProdAZToDel = new List<Team_Instance_Product_AZ__c>();
        List<Team_Instance_Product_AZ__c> TeamInstProdAZList = new List<Team_Instance_Product_AZ__c>();
        TeamInstProdAZList = [select id, Product_Catalogue__c, Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c in :allTeamInstances and Product_Catalogue__c in :allProducts WITH SECURITY_ENFORCED];
        
        //Team_Instance_Product_AZ__c TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
        SnTDMLSecurityUtil.printDebugMessage('TeamInstProdAZList++++++'+TeamInstProdAZList);
        for(Team_Instance_Product_AZ__c pp : TeamInstProdAZList)
        {
            if(posProdCombo.contains(pp.Team_Instance__c + '_' + pp.Product_Catalogue__c))
            {
                TeamInstProdAZToDel.add(pp);
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('TeamInstProdAZToDel++++++'+TeamInstProdAZToDel);
        //delete TeamInstProdAZToDel;
        SnTDMLSecurityUtil.deleteRecords(TeamInstProdAZToDel, 'TeamInstanceProductTriggerHandler');

    }

    /*public static void populatePosition(List<Product_Catalog__c> allPosProducts)
    {
        Set<String> allTeamInstances = new Set<String>();
        Map<String, Set<ID>> allProductsInTeam = new Map<String, Set<ID>>();

        for(Product_Catalog__c tip : allPosProducts)
        {
            allTeamInstances.add(tip.Team_Instance__c);
            String teamInstance = String.valueOf(tip.Team_Instance__c);

            if(allProductsInTeam.containsKey(teamInstance))
            {
                allProductsInTeam.get(teamInstance).add(tip.id);
            }
            else
            {
                Set<ID> tempSet = new Set<ID>();
                tempSet.add(tip.id);
                allProductsInTeam.put(teamInstance, tempSet);
            }
        }

        List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances];

        List<AxtriaSalesIQTM__Position_Product__c> positionProductsToInsert = new List<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position__c pos : allPositions)
        {
            String teamInstance = pos.AxtriaSalesIQTM__Team_Instance__c;

            for(ID product : allProductsInTeam.get(teamInstance))
            {
                AxtriaSalesIQTM__Position_Product__c posPro = new AxtriaSalesIQTM__Position_Product__c();
                posPro.AxtriaSalesIQTM__Position__c = pos.id;
                //obj.Product_Master__c = (Id) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Product_Master__c');
                posPro.AxtriaSalesIQTM__Team_Instance__c =teamInstance;
                posPro.Product_Catalog__c = product;
                posPro.Vacation_Days__c = 0;
                posPro.Holidays__c = 0;
                posPro.Calls_Day__c = 0;
                posPro.Business_Days_in_Cycle__c = 0;
                posPro.AxtriaSalesIQTM__Product_Weight__c = 0;    
                posPro.AxtriaSalesIQTM__isActive__c = true;
                posPro.AxtriaSalesIQTM__Effective_Start_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                posPro.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c;

                positionProductsToInsert.add(posPro);
            
            }
        }
        insert positionProductsToInsert;
    }

    public static void deletePosition(List<Product_Catalog__c> allPosProducts)
    {
        Set<String> allTeamInstances = new Set<String>();
        Set<String> allProducts = new Set<String>();
        Set<String> posProdCombo = new Set<String>();

        for(Product_Catalog__c ti :allPosProducts)
        {
            allTeamInstances.add(ti.Team_Instance__c);
            allProducts.add(ti.ID);
            posProdCombo.add(ti.Team_Instance__c + '_' + ti.ID);
        }

        List<AxtriaSalesIQTM__Position_Product__c> allPosProductsDel = [select id, AxtriaSalesIQTM__Position__c, Product_Catalog__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and Product_Catalog__c in :allProducts];
        
        List<AxtriaSalesIQTM__Position_Product__c> deletePosProduct = new List<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position_Product__c pp : allPosProductsDel)
        {
            if(posProdCombo.contains(pp.AxtriaSalesIQTM__Team_Instance__c + '_' + pp.Product_Catalog__c))
            {
                deletePosProduct.add(pp);
            }
        }

        delete deletePosProduct;
    }*/
}