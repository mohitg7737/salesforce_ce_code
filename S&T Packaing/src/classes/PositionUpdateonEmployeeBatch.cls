global class PositionUpdateonEmployeeBatch implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public Map<String,String> mapPos2Emp;
    public Map<String,String> mapPos2ParentPos;
    public Map<String,String> newMapEmp2Pos;
    public Map<String,String> oldMapEmp2Manager;
    public Map<String,String> oldMapEmp2Pos;
    public Map<String,String> oldMapEmp2PRID;
    public Map<String,String> oldMapEmp2Name;
    public Map<String,Map<String,String>> emp2MapType2Pos;
    public Set<ID> allAssignedEmployees;
    public List<AxtriaSalesIQTM__Employee__c> updateEmpList;
    public List<AxtriaSalesIQTM__Employee__c> updateUnassignEmpList;
   
    
    public Set<ID> duplicateRecs;

    global PositionUpdateonEmployeeBatch() 
    {
        query = '';
        duplicateRecs = new Set<ID>();
        mapPos2Emp=new Map<String,String>();
        mapPos2ParentPos=new Map<String,String>();
        newMapEmp2Pos=new Map<String,String>();
        oldMapEmp2Manager=new Map<String,String>();
        oldMapEmp2Pos=new Map<String,String>();
        oldMapEmp2PRID=new Map<String,String>();
        oldMapEmp2Name=new Map<String,String>();
        emp2MapType2Pos=new Map<String,Map<String,String>>();
        allAssignedEmployees = new Set<ID>();
        updateUnassignEmpList=new List<AxtriaSalesIQTM__Employee__c>();
        updateEmpList= new List<AxtriaSalesIQTM__Employee__c>();
        

        query='select Id,AxtriaSalesIQTM__Current_Territory__c,Employee_PRID__c,Name,AxtriaSalesIQTM__Field_Status__c,Current_Territory1__c,AxtriaSalesIQTM__Current_Territory__r.Employee1__c,AxtriaSalesIQTM__Manager__c,ReportingToWorkerName__c,ReportsToAssociateOID__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__HR_Status__c=\'Active\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> scope) 
    {
        /*try
        {*/
            System.debug('======Query::::::' +scope);
            for(AxtriaSalesIQTM__Employee__c eRec : scope)
            {
                oldMapEmp2Pos.put(eRec.Id,eRec.Current_Territory1__c);
                oldMapEmp2Manager.put(eRec.Id,eRec.AxtriaSalesIQTM__Manager__c);
                oldMapEmp2PRID.put(eRec.Id,eRec.Employee_PRID__c);
                oldMapEmp2Name.put(eRec.Id,eRec.Name);
            }
            System.debug('======oldMapEmp2Pos::::::'+oldMapEmp2Pos);
            System.debug('======oldMapEmp2Pos::::::'+oldMapEmp2Pos);


            List<AxtriaSalesIQTM__Position__c> posList =[select Id,Employee1__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Parent_Position__c,Employee1_Assignment_Type__c,AxtriaSalesIQTM__Parent_Position__r.Employee1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and Employee1__c in :oldMapEmp2Pos.keySet() and AxtriaSalesIQTM__IsMaster__c=true];   //added master check
            
            for(AxtriaSalesIQTM__Position__c posRec : posList)
            {
                if(!emp2MapType2Pos.containsKey(posRec.Employee1__c)){
                    Map<String,String> tempType2Pos = new Map<String,String>();
                    tempType2Pos.put(posRec.Employee1_Assignment_Type__c,posRec.Id);
                    emp2MapType2Pos.put(posRec.Employee1__c,tempType2Pos);
                }
                else{
                    emp2MapType2Pos.get(posRec.Employee1__c).put(posRec.Employee1_Assignment_Type__c,posRec.Id);
                }
                            
                mapPos2Emp.put(posRec.Id,posRec.Employee1__c);
                mapPos2ParentPos.put(posRec.Id,posRec.AxtriaSalesIQTM__Parent_Position__c);
                
            }

            System.debug('======emp2MapType2Pos::::::'+emp2MapType2Pos);
            System.debug('======mapPos2Emp::::::'+mapPos2Emp);
            System.debug('======mapPos2ParentPos::::::'+mapPos2ParentPos);
                

            System.debug('============Fill new maps for Emp2Pos==============');
            for(AxtriaSalesIQTM__Position__c posRec : posList)
            {
                if(emp2MapType2Pos.containsKey(posRec.Employee1__c)){
                    if(emp2MapType2Pos.get(posRec.Employee1__c).containsKey('Primary')){
                        newMapEmp2Pos.put(posRec.Employee1__c,posRec.Id);
                    }
                    else{
                        newMapEmp2Pos.put(posRec.Employee1__c,posRec.Id);
                    }
                }
            }
            System.debug('======newMapEmp2Pos::::::'+newMapEmp2Pos);
            
            List<AxtriaSalesIQTM__Employee__c> empList = [select Id,Employee_PRID__c,Name,AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__Current_Territory__c,Current_Territory1__c,AxtriaSalesIQTM__Current_Territory__r.Employee1__c,AxtriaSalesIQTM__Manager__c,ReportingToWorkerName__c,ReportsToAssociateOID__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__HR_Status__c='Active' and Id in:oldMapEmp2Pos.keySet()];

            for(AxtriaSalesIQTM__Employee__c empRec : empList)
            {
                Boolean flag = false;
                //System.debug('======newMapEmp2Pos.containsKey(empRec.Id)::::::'+newMapEmp2Pos.containsKey(empRec.Id));
                if(newMapEmp2Pos.containsKey(empRec.Id))
                {
                    System.debug('Inside New mapE2P contains the employee' +empRec.Id);
                    if(oldMapEmp2Pos.get(empRec.Id) != newMapEmp2Pos.get(empRec.Id)){
                        flag=true;
                        empRec.Current_Territory1__c=newMapEmp2Pos.get(empRec.Id);
                        System.debug('====oldMapEmp2Pos.get(empRec.Id):::' +oldMapEmp2Pos.get(empRec.Id));
                    }
                    String pos =newMapEmp2Pos.get(empRec.Id);
                    String parentPos=mapPos2ParentPos.get(pos);
                    String emp =mapPos2Emp.get(parentPos);
                    if(oldMapEmp2Manager.get(empRec.Id) != emp)
                    {
                        //empRec.AxtriaSalesIQTM__Manager__c = emp;
                        if(emp != null && emp != empRec.Id){


                            empRec.AxtriaSalesIQTM__Manager__c = emp;
                            empRec.ReportingToWorkerName__c = oldMapEmp2Name.get(emp);
                            empRec.ReportsToAssociateOID__c = oldMapEmp2PRID.get(emp);
                        }
                        else{
                            empRec.AxtriaSalesIQTM__Manager__c = null;
                            empRec.ReportingToWorkerName__c = null;
                            empRec.ReportsToAssociateOID__c = null;
                        }

                        flag=true;
                        System.debug('====oldMapEmp2Manager.get(empRec.Id):::' +oldMapEmp2Manager.get(empRec.Id));
                        System.debug('====pos:::' +pos);
                        System.debug('====parentPos:::' +parentPos);
                        System.debug('====emp:::' +emp);
                    }
                    system.debug('==============emp::::'+emp);
                    system.debug('==============oldMapEmp2Manager.get(empRec.Id::::'+oldMapEmp2Manager.get(empRec.Id));
                   /* if(oldMapEmp2Manager.get(empRec.Id)==null && emp==null){
                        system.debug('=================NULL NULL BLOCK+++++++++++++');
                        empRec.AxtriaSalesIQTM__Manager__c = null;
                        empRec.ReportingToWorkerName__c = null;
                        empRec.ReportsToAssociateOID__c = null;
                        flag=true;
                    }*/
                    if(flag)
                    {
                        if(duplicateRecs.contains(empRec.ID))
                        {
                            system.debug('+++++++++++++++++ Duplicate Recs ' + updateEmpList);
                        }
                        else
                        {
                            updateEmpList.add(empRec);
                            duplicateRecs.add(empRec.ID);       
                        }
                        
                    }
                        

                    //allAssignedEmployees.add(empRec.Id);
                }
                else
                {
                    System.debug('Inside else part which does not contains the employee' +empRec.Id);
                    if(oldMapEmp2Pos.get(empRec.Id) != null || oldMapEmp2Manager.get(empRec.Id) !=null){
                        empRec.AxtriaSalesIQTM__Manager__c = null;
                        empRec.ReportingToWorkerName__c = null;
                        empRec.ReportsToAssociateOID__c = null;
                        empRec.Current_Territory1__c=null;
                        //updateEmpList.add(empRec);
                        
                        if(duplicateRecs.contains(empRec.ID))
                        {
                            system.debug('+++++++++++++++++ Duplicate Recs ' + updateEmpList);
                        }
                        else
                        {
                            updateEmpList.add(empRec);
                            duplicateRecs.add(empRec.ID);       
                        }
                        system.debug('Employee Record======' +empRec);
                    }

                }
            }

            System.debug('=======updateEmpList.size()=====' +updateEmpList.size());
                if(updateEmpList.size()>0)
                   update updateEmpList;
                /*if(oldMapEmp2Pos.get(empRec.Id) != newMapEmp2Pos.get(empRec.Id)){
                    flag=true;
                    empRec.Current_Territory1__c=newMapEmp2Pos.get(empRec.Id);
                }
                String pos =newMapEmp2Pos.get(empRec.Id);
                String parentPos=mapPos2ParentPos.get(pos);
                String emp =mapPos2Emp.get(parentPos);
                if(oldMapEmp2Manager.get(empRec.Id) != emp){
                    empRec.AxtriaSalesIQTM__Manager__c = emp;
                    flag=true;
                }
                if(flag)
                    updateEmpList.add(empRec);

                allAssignedEmployees.add(empRec.Id);
            }
            if(updateEmpList.size()>0)
                update updateEmpList;

            List<AxtriaSalesIQTM__Employee__c> unassignEmpList = [select Id,Employee_PRID__c,Name,AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__Current_Territory__c,Current_Territory1__c,AxtriaSalesIQTM__Current_Territory__r.Employee1__c,AxtriaSalesIQTM__Manager__c,ReportingToWorkerName__c,ReportsToAssociateOID__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__HR_Status__c='Active' and Id not in:allAssignedEmployees and AxtriaSalesIQTM__Manager__c!=null];
            for(AxtriaSalesIQTM__Employee__c empUnassign :unassignEmpList)
            {
                empUnassign.AxtriaSalesIQTM__Manager__c = null;
                empUnassign.ReportingToWorkerName__c = null;
                empUnassign.ReportsToAssociateOID__c = null;
                updateUnassignEmpList.add(empUnassign);
                system.debug('Employee Record======' +empUnassign);
            }

            if(updateUnassignEmpList.size() > 0)
                update updateUnassignEmpList;*/
       /* } 
        catch(Exception e)
       {
           
            system.debug('++inside catch');
            String Header='Position update on Employee batch is failed, so delta has been haulted of this org for Today';
            tryCatchEmailAlert.veevaLoad(Header);
       }*/
    }

    global void finish(Database.BatchableContext BC) 
    {

    }
}