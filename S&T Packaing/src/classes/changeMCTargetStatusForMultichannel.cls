global class changeMCTargetStatusForMultichannel implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    List<String> allTeamInstances;
    String queryString;
    public Boolean flag = true;
    public Date lmd;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCTargetStatusForMultichannel(string teamInstanceSelectedTemp, List<String> allChannelsTemp,Boolean check)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c =   :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        flag=check;
    }



  global changeMCTargetStatusForMultichannel(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in   :allTeamInstances';
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        
    }
     global changeMCTargetStatusForMultichannel(boolean check)
    { 
        //allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c'; //where Team_Instance__c in   :allTeamInstances';
        //teamInstanceSelected = teamInstanceSelectedTemp;
        //allChannels = allChannelsTemp;
        
    }
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Target_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        
        AZ_Integration_MC_Target_MP_EU_MultiChan u1 = new AZ_Integration_MC_Target_MP_EU_MultiChan(allTeamInstances, allChannels);
        Database.executeBatch(u1, 2000);             
    }
}