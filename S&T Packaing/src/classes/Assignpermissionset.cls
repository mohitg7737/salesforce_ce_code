global class Assignpermissionset implements Database.Batchable<sObject>,Schedulable,Database.stateful
 {
    public String query;
    Set<string> prid;
    Map<String,Id> userpridtoidmap;
    Map<String,String> pridtohierchymap;
    Map<String,String> pridtocountrymap;

    public string profileid;
    Set<string> sfid;
    List<Profile> pro;
    public Set<string> exProfileNameSet;
    public Set<String> exProfileIdSet;
    List<ExcludeProfile__c> exProfile;

    global Assignpermissionset() 
    {
        exProfileIdSet = new Set<String>();
        exProfileNameSet = new Set<String>();
        exProfile = [Select Id, Name From ExcludeProfile__c];
        if(exProfile!=null && exProfile.size()>0){
            for(ExcludeProfile__c exPro:exProfile){
                exProfileNameSet.add(exPro.Name);
            }
        }else{
            exProfileNameSet.addAll(new List<String>{'HO','HO1','Service Admin','Ops Admin','Data Admin','System Administrator','Market_Admin_ReadOnly'});         
        }

        query='SELECT AxtriaSalesIQTM__Employee__r.Employee_PRID__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c,id,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country__c FROM AxtriaSalesIQTM__Position_Employee__c where LastModifiedDate = TODAY and AxtriaSalesIQTM__Employee__r.Employee_PRID__c!=null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Employee__c> scope) 
    {
        system.debug('=============query=======:'+query);
        prid=new Set<String>();
        userpridtoidmap= new Map<string,id>();
        pridtohierchymap= new Map<String,String>();
        pridtocountrymap= new Map<String,String>();
        profileid='';
        sfid = new Set<String>();
        Map<String,Temp_user__c> levelmap = new Map<String,Temp_user__c>();
        Map<String,Temp_user__c> countrymap = new Map<String,Temp_user__c>();
        Map<String,Temp_user__c> hierarchycountrymap= new Map<String,Temp_user__c>();
        List<PermissionSetAssignment> permissionList = new List<PermissionSetAssignment>();

        Map<string,Temp_user__c> usermap = new Map<String,Temp_user__c>();
        List<user> userlist = new List<user>();
        List<PermissionSetAssignment> updateperm = new List<PermissionSetAssignment>();
        set<string>unq = new set<string>();
        List<user> rolelist = new List<user>();


        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            prid.add(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
            pridtohierchymap.put(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c);
            pridtocountrymap.put(posEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country__c);
        }
        system.debug('+++prid++'+prid);
        system.debug('+++pridtohierchymap++'+pridtohierchymap);
         //user list
        pro = new List<Profile>();
        pro= [SELECT Id,Name FROM Profile WHERE Name IN :exProfileNameSet];
        system.debug('+++pro++'+pro);
        for(Profile p:pro)
        {
            exProfileIdSet.add(p.id);
        }
        //profileHO = string.valueof(pro[0].id);
        system.debug('+++exProfileIdSet +'+exProfileIdSet);

        List<User> us=[SELECT FederationIdentifier,id,country FROM User where ProfileID NOT IN :exProfileIdSet];
        for(User u: us)
        {
            if(!String.isBlank(u.FederationIdentifier)){
                userpridtoidmap.put(u.FederationIdentifier,u.Id);
                sfid.add(u.id);
            }  
        }

        system.debug('+++userpridtoidmap++'+userpridtoidmap);

        List<Temp_user__c> tempUser =[SELECT id, Hierarchy_Level__c,Permission_Set__c,Profile__c,Role__c,Country__c FROM Temp_user__c];
        for(Temp_user__c t :tempUser)
        {
            if(!String.isBlank(t.Hierarchy_Level__c) && !String.isBlank(t.Country__c)){
                levelmap.put(t.Hierarchy_Level__c,t);
                countrymap.put(t.Country__c,t);
                hierarchycountrymap.put(t.Country__c+'_'+t.Hierarchy_Level__c,t);
            }
        }
        system.debug('+++levelmap++'+levelmap);
        system.debug('+++countrymap++'+countrymap);
        system.debug('+++hierarchycountrymap++'+hierarchycountrymap);


        //Assigning PROFILE
        for(String prid:pridtohierchymap.keySet())
        {
            if(userpridtoidmap.containskey(prid))
            {
                string hierarchy = pridtohierchymap.get(prid);
                string country= pridtocountrymap.get(prid);
                string key= country+'_'+hierarchy;
                system.debug('+++hierarchy++'+hierarchy);
                system.debug('+++key++'+key);
                if(!String.isBlank(hierarchy))
                {
                    system.debug('++Assigning profile++');
                    Temp_user__c tem = hierarchycountrymap.get(key);
                    if(tem!=null && !String.isBlank(tem.Profile__c)){
                        user u = new user(id=userpridtoidmap.get(prid));
                        u.ProfileId=tem.Profile__c;
                        userlist.add(u);
                    }
                }
                system.debug('+++userlist++'+userlist);
            }
        }  
        update userlist;


        //Assigning role
        for(String prid:pridtohierchymap.keySet())
        {
            string country= pridtocountrymap.get(prid);
            system.debug('============Country is ::'+prid+'================'+country);
            if(userpridtoidmap.containskey(prid))
            { 
                if(countrymap.containsKey(country))
                {
                    string hierarchy = pridtohierchymap.get(prid);
                    String key= country+'_'+hierarchy;
                    system.debug('+++hierarchy++'+hierarchy);
                    system.debug('+++key++'+key);
                    if(!String.isBlank(hierarchy))
                    {
                         system.debug('++Assigning role++');
                        Temp_user__c tem = hierarchycountrymap.get(key);
                        if(tem!=null && !String.isBlank(tem.Role__c)){
                            user user2 = new user(id=userpridtoidmap.get(prid));
                            user2.UserRoleId=tem.Role__c;
                            rolelist.add(user2);
                            system.debug('===========User For role update:::'+user2);
                        }
                    }
                    system.debug('+++rolelist++'+rolelist);
                }
            }
        }  
        update rolelist;

        //Assigning Permission Set
        List<PermissionSetAssignment>  permSet = [Select AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId in :sfid and PermissionSetId IN (SELECT Id 
                                                      FROM PermissionSet
                                                     WHERE IsOwnedByProfile =false)];

        List<PermissionSetAssignment> permList= new List<PermissionSetAssignment>();
        Set<String> permisionSet = new Set<String>();

        system.debug('+++permSet+++'+permSet);
        Set<String> userpermissionset = new Set<String>();
      
        if(permSet!=null && permSet.size()>0)
        {
            for(PermissionSetAssignment perm : permSet)
            {
                unq.add(perm.AssigneeId);
                userpermissionset.add(perm.AssigneeId+'_'+perm.PermissionSetId);
            }
            system.debug('+++unq Size+++'+unq.size());
        }
        for(String prid:pridtohierchymap.keySet())
        { 
            if(userpridtoidmap.containskey(prid))
            {
                string hierarchy1 = pridtohierchymap.get(prid);
                string country= pridtocountrymap.get(prid);
                String key= country+'_'+hierarchy1;
                system.debug('+++hierarchy1+++'+hierarchy1);
                system.debug('+++key+++'+key);

                if(!String.isBlank(hierarchy1))
                {  
                    string userid = userpridtoidmap.get(prid);
                    system.debug('+++userid+++'+userid);
                    if(!unq.contains(userid))
                    {
                        Temp_user__c temhier = hierarchycountrymap.get(key);
                        if(temhier!=null && !String.isBlank(temhier.Permission_Set__c)){
                            user user1 = new user(id=userpridtoidmap.get(prid));
                            system.debug('+++user1+++'+user1);
                            PermissionSetAssignment psa= new PermissionSetAssignment();
                            psa.PermissionSetId = temhier.Permission_Set__c;
                            psa.AssigneeId = user1.Id;
                            system.debug('+++ psa.PermissionSetId+++'+ psa.PermissionSetId);
                            updateperm.add(psa);
                        }
                    }
                    else
                    {
                        Temp_user__c temHier = hierarchycountrymap.get(key);
                        if(temHier!=null && !String.isBlank(temHier.Permission_Set__c)){
                            system.debug('temHier'+temHier);
                            user user1 = new user(id=userpridtoidmap.get(prid));
                            system.debug('+++user1+++'+user1);
                            string tempkey = user1.id+'_'+temHier.Permission_Set__c;

                            for(string usperm: userpermissionset)
                            {
                                //system.debug('usperm'+usperm);
                                if(usperm.contains(user1.id)){
                                    permisionSet.add(usperm);
                                }
                            }
                            system.debug('+++permisionSet to delete Size+++'+permisionSet.size());
                            PermissionSetAssignment psa= new PermissionSetAssignment();
                            psa.PermissionSetId = temHier.Permission_Set__c;
                            psa.AssigneeId = user1.Id;
                            system.debug('+++ psa.PermissionSetId+++'+ psa.PermissionSetId);
                            updateperm.add(psa);
                        }                                             
                    }
                }
            }
            system.debug('+++updateperm++'+updateperm);
        }
       
        for(PermissionSetAssignment perm :permSet)
        {
            string key= perm.AssigneeId+'_'+perm.PermissionSetId;
            if(permisionSet.contains(key))
            {
               permList.add(perm);
            }
        }

        system.debug('+++permList++'+permList);
        if(permList !=null && permList.size()>0)
        {
             delete permList;
        }          
        system.debug('+++permissionToInsert++'+updateperm);
        if(updateperm !=null && updateperm.size()>0 )
        {
            database.insert(updateperm,false);
        }               
    }

    global void finish(Database.BatchableContext BC)
    {
       
    }
    global void execute(SchedulableContext sc)
    {
        database.executeBatch(new Assignpermissionset(),2000);
    }
}