global with sharing class StorageNotification implements Schedulable {
    
   global void execute(SchedulableContext SC) {
      SendAlerts();      
   }
   
    @future (callout=true)  
    public static void SendAlerts(){
        
        String[] recipientsMail =   new String[] {};
        String[] adminsMail     =   new String[] {}; //Sent when error occurs 
        
        //commented due to object purge activity A1450
        // List<StorageNotificationEmail__c> recipientsMailList = [Select Id,Name from StorageNotificationEmail__c];
        // if(recipientsMailList !=null & recipientsMailList.size()>0){
        //     for(StorageNotificationEmail__c obj:recipientsMailList){
        //         recipientsMail.add(obj.Name);
        //         adminsMail.add(obj.Name);
        //     }
        // }else {
        //     recipientsMail.addAll(new List<String>{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'});
        //     adminsMail.addAll(new List<String>{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'});
        // }

        //Replacing StorageNotificationEmail__c Custom Setting with Logger_Email__c
        List<Logger_Email__c> recipientsMailList = [Select Id,Name from Logger_Email__c where Type__c = 'StorageNotification'];
        if(recipientsMailList !=null & recipientsMailList.size()>0){
            for(Logger_Email__c obj:recipientsMailList){
                recipientsMail.add(obj.Name);
                adminsMail.add(obj.Name);
            }
        }else {
            recipientsMail.addAll(new List<String>{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'});
            adminsMail.addAll(new List<String>{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'});
        }

        Integer limitDataStorageInfo   = 60; // Send information mail when you reach this%
        Integer limitDataStorageAlert = 75; // Send information mail when you reach this%
        Integer limitFileStorageInfo   = 60; // Send information mail when you reach this%
        Integer limitFileStorageAlert = 70; // Send information mail when you reach this%
        
        String requestUrl = '/setup/org/orgstorageusage.jsp?id=' + UserInfo.getOrganizationId() + '&setupid=CompanyResourceDisk';
        System.debug('RequestUrl is '+requestUrl);
        //Get the Storage Page, prepare to scrape
        PageReference pg = new PageReference( requestUrl );
        String htmlCode = '';
        if(!Test.isRunningTest()){
            htmlCode = pg.getContent().toString();
        }else{
            htmlCode = '<div>testet</div><table><tr><td> test\n68%'+
                                                 +'</td></tr><tr><td> test\n68%</td></tr><tr><td> test\n68%</td></tr>';
        }
        System.debug('HTML code is'  + htmlCode);
        
        //Find the pattern 
        Pattern patternToSearch = Pattern.compile('\\d+%</td></tr>'); 
        Matcher matcherPattern = patternToSearch.matcher(htmlCode);

        String  dataStorageString, fileStorageString, dataStorageUsedPercentage, fileStorageUsedPercentage;
                
        //Find the first Occurrence which is Data Storage       
        if ( matcherPattern.find() ) {
            
            dataStorageString = htmlCode.substring(matcherPattern.start(), matcherPattern.end());
            //System.debug('**************************** dataStorageString: ' + dataStorageString);
            
            //Find the Subpattern
            Pattern subpatternToSearch = Pattern.compile('\\d+'); //Indicates values â€‹â€‹of the table of percentages
            Matcher matcherPatternPercentage = subpatternToSearch.matcher(dataStorageString);
            
            if ( matcherPatternPercentage.find() ) { 
                
                dataStorageUsedPercentage = dataStorageString.substring(matcherPatternPercentage.start(), matcherPatternPercentage.end());
                
            }else{
                dataStorageString =  null;
            }
        }
         
        //Find the first Occurrence; which is File Storage
        if ( matcherPattern.find() ) {
            
            fileStorageString = htmlCode.substring(matcherPattern.start(), matcherPattern.end());
            
            Pattern subpatternToSearch = Pattern.compile('\\d+'); // Indicates values â€‹â€‹of the percentages table
            Matcher matcherPatternPercentage = subpatternToSearch.matcher(fileStorageString);            
            
            if ( matcherPatternPercentage.find() ) { 
                fileStorageUsedPercentage = fileStorageString.substring(matcherPatternPercentage.start(), matcherPatternPercentage.end());
            }else{
                fileStorageString = null;
            }
        }        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String bodyMessage = '';
        Boolean sendMessage = false;
        
        //Check we've got values, otherwise notify the Admins
        if ( (!String.isBlank(dataStorageUsedPercentage)) && (!String.isBlank(fileStorageUsedPercentage) ) ){
            
            if ( ( limitDataStorageAlert <= integer.valueof(dataStorageUsedPercentage) ) || ( limitFileStorageAlert <= integer.valueof(fileStorageUsedPercentage) ) ) {
                
                sendMessage = true;
                mail.setSubject('ALERT warning storage: ' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());
                    
            }else if ( ( limitDataStorageInfo <= integer.valueof(dataStorageUsedPercentage) ) || ( limitFileStorageInfo <= integer.valueof(fileStorageUsedPercentage) ) ) {
                
                sendMessage = true;
                mail.setSubject('Storage INFO notice:' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());
                
            }            
            
            if ( sendMessage ){
                //Send Mail Notifications
                mail.setToAddresses(recipientsMail);
                mail.setSenderDisplayName('Storage notice');
                bodyMessage  = 'A storage alarm has been generated because one of the following values â€‹â€‹has exceeded the limit:';
                bodyMessage += '<ul><li>Data Storage: <b>' +  dataStorageUsedPercentage + '%</b>';
                bodyMessage += '<li>Storage Files: <b>' + fileStorageUsedPercentage + '%</b></ul>';
                bodyMessage += 'The established limits are:';
                bodyMessage += '<ul><li>Info and Alert for Data Storage: ' + limitDataStorageInfo + '% y ' + limitDataStorageAlert + '%';
                bodyMessage += '<li>Info and Alert for File Storage: ' + limitFileStorageInfo + '% y ' + limitFileStorageAlert + '%';
                bodyMessage += '<p><a href="' + System.URL.getSalesforceBaseUrl().toExternalForm() + requestUrl + '" target="_blank">Access the storage page</a>';
                mail.setHtmlBody(bodyMessage);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            //else No Alerts must be sent, storage usages are between limits
            
            
            
        }else{
            //Things went Wrong, notify the admins
            
            //System.debug('Pattern finding failed then - Vikas's Fault');
            
            //Send Mail Notifications
            mail.setToAddresses(adminsMail);
            mail.setSenderDisplayName('Error in Alerts generation');
            mail.setSubject('Error Generation Alerts Storage in: ' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());
            
            bodyMessage  = 'EError generating alerts, the values â€‹â€‹are dataStorageUsedPercentage:' +  dataStorageUsedPercentage + ' fileStorageUsedPercentage:' + fileStorageUsedPercentage;
            
            mail.setHtmlBody(bodyMessage);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}