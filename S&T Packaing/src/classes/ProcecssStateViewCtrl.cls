public with sharing class ProcecssStateViewCtrl {
	public string pathvalue {get;set;}

	public pagereference redirecttopage(){
		System.debug('=============pathvalue='+pathvalue);
		
		if(pathvalue != null && pathvalue != '')
		{
			PageReference	pg = new pagereference('/apex/'+pathvalue);
			pg.setRedirect(true);
			return pg; 
		}
		else
		{
			system.debug('++++++++++++ IN 2nd ');
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Cannot directly jump to other steps, Click Save/Save & Next to proceed'));
			PageReference	pg = new pagereference(ApexPages.currentPage().getURL());
			pg.setRedirect(true);
			return pg;
		}

		return null;
	}
    
}