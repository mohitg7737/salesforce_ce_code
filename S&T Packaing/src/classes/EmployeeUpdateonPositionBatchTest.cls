@isTest
public class EmployeeUpdateonPositionBatchTest {
    @istest 
    static void EmployeeUpdateonPositionBatchTest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        

        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins2;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins2, countr);
        insert pcc1;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos;
        

        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
        emp.AxtriaSalesIQTM__Country__c = countr.id;
        insert emp;
        
        AxtriaSalesIQTM__Position_Employee__c posemp = TestDataFactory.createPositionEmployee(pos, emp.id, 'Primary', date.today(), date.today()+1);
        insert posemp;
        Set<String> country = new Set<String>();
        country.add(countr.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            EmployeeUpdateonPositionBatch obj = new EmployeeUpdateonPositionBatch();
            obj.query='select id,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c  from AxtriaSalesIQTM__Position_Employee__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}