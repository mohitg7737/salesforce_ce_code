global with sharing class changeMCChannelStatus implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<String> allTeamInstances;
    Set<String> activityLogIDSet;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCChannelStatus(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global changeMCChannelStatus(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
       allTeamInstances = new List<String>(teamInstanceSelectedTemp);

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

    //Added by Ayushi

    global changeMCChannelStatus(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp, Set<String> activityLogSet)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
       allTeamInstances = new List<String>(teamInstanceSelectedTemp);
       activityLogIDSet = new Set<String>();
       activityLogIDSet.addAll(activityLogSet);

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;

        //Added By Ayushi
        /*List<Error_Object__c> insertErrorObj = new List<Error_Object__c>();
        for(String teamIns : allTeamInstances)
        {
            Error_Object__c rec = new Error_Object__c();
            rec.Object_Name__c = 'changeMCChannelStatus';
            rec.Team_Instance_Name__c = teamIns;
            insertErrorObj.add(rec);
        }
        insert insertErrorObj;*/
        //till here
    }

    //Till here.......

    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        //update scopePacpProRecs;
        SnTDMLSecurityUtil.updateRecords(scopePacpProRecs, 'changeMCChannelStatus');

    }

    global void finish(Database.BatchableContext BC)
    {
        if(activityLogIDSet != null)
        {
            AZ_Integration_New_Utility_MP_EU u1New = new AZ_Integration_New_Utility_MP_EU(allTeamInstances, allChannels,activityLogIDSet);
        
            Database.executeBatch(u1New, 2000);

        }
        else
        {
            AZ_Integration_New_Utility_MP_EU u1 = new AZ_Integration_New_Utility_MP_EU(allTeamInstances, allChannels);
        
            Database.executeBatch(u1, 2000);   
        }
        
        //Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}