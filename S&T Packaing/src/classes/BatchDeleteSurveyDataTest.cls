/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test BatchDeleteSurveyData.
*/
@isTest
private class BatchDeleteSurveyDataTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'TESTTEAMINS';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;        
        Staging_Veeva_Cust_Data__c v = new Staging_Veeva_Cust_Data__c();
        v.Team_Instance__c = teamins.Id;
        insert v;
        Scheduler_Log__c s = new Scheduler_Log__c();
        s.Job_Type__c = 'Survey Data Load';
        s.Job_Status__c = 'Failed';
        insert s;
        
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        etl.Name = 'Survey Data Load';
        insert etl;
        
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> STAGINGVEEVA_READ_FIELD = new List<String>{'Id'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Staging_Veeva_Cust_Data__c.SObjectType, STAGINGVEEVA_READ_FIELD, false));
            BatchDeleteSurveyData obj=new BatchDeleteSurveyData();
            obj.query = 'SELECT Id FROM Staging_Veeva_Cust_Data__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}