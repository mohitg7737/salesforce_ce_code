global with sharing class BatchdeleteRecords implements database.batchable<sobject> {
                   
    string query ;
    string objName;
    string TeamIns;
    string TeamInsid;
    
    global BatchdeleteRecords(String ObjectName,String TeamInstanceAPI,String TeamInstanceID){
        objName = ObjectName;
        TeamIns = TeamInstanceAPI ;
        TeamInsid = TeamInstanceID;
        SnTDMLSecurityUtil.printDebugMessage('=====ObjectName:'+ObjectName+'=======teamInstanceAPI:'+teamInstanceAPI+'=============TeamInstanceID:'+TeamInstanceID);
       query = 'SELECT Id FROM '+ObjectName+' where '+teamInstanceAPI +'=\''+TeamInsid+'\' WITH SECURITY_ENFORCED';
        SnTDMLSecurityUtil.printDebugMessage('====query:' + query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){         
        return Database.getQueryLocator(query);
    }      
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        //delete scope; 
        SnTDMLSecurityUtil.deleteRecords(scope, 'BatchdeleteRecords');
    }
    
    global void finish(Database.BatchableContext BC){
        if(objName.contains('AxtriaSalesIQTM__Position_Geography__c')){
           database.executeBatch(new BatchdeleteRecords ('Account_Compute_Final__c','Measure_Master__r.Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('Account_Compute_Final__c')){
                database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Position_Account__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid)); 
        }
        else if(objName.contains('AxtriaSalesIQTM__Position_Account__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Position_Employee__c','AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Position_Employee__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Position__c','AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Position__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Geography__c','AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Geography__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Account__c','AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Account__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CIM_Change_Request_Impact__c','AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CIM_Change_Request_Impact__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Employee_Assignment__c','AxtriaSalesIQTM__Change_Request_ID__r.AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Employee_Assignment__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Change_Request__c','AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Change_Request__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Position_Team_Instance__c','AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Position_Team_Instance__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Position__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Position__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Team_Instance_Object_Attribute__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Team_Instance_Object_Attribute__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Team_Instance_Config__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Team_Instance_Config__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CR_Approval_Config__c','AxtriaSalesIQTM__Team_Instance_ID__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CR_Approval_Config__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CIM_Position_Metric_Summary__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CIM_Position_Metric_Summary__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__CIM_Config__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__CIM_Config__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__BR_Join_Rule_Condition__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__BR_Join_Rule_Condition__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__BRJoinRuleSelectCondition__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__BRJoinRuleSelectCondition__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__BR_JoinRule__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__BR_JoinRule__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Business_Rules_Expression__c','AxtriaSalesIQTM__Business_Rules__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }   
        else if(objName.contains('AxtriaSalesIQTM__Business_Rules_Expression__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Aggregate_Rule_Detail__c','AxtriaSalesIQTM__Business_Rules__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }   
          else if(objName.contains('AxtriaSalesIQTM__Aggregate_Rule_Detail__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Aggregate_Rule_Level__c','AxtriaSalesIQTM__Business_Rules__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }   
        else if(objName.contains('AxtriaSalesIQTM__Aggregate_Rule_Level__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Business_Rules__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
         else if(objName.contains('AxtriaSalesIQTM__Business_Rules__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Data_Set__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
         else if(objName.contains('AxtriaSalesIQTM__Data_Set__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Scenario_Rule_Instance_Details__c','AxtriaSalesIQTM__Scenario_Id__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
          else if(objName.contains('AxtriaSalesIQTM__Scenario_Rule_Instance_Details__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Scenario_Data_Object_Map__c','AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Scenario_Data_Object_Map__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Scenario__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__Scenario__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__User_Access_Permission__c','AxtriaSalesIQTM__Team_Instance__c',TeamInsid));
        }
        else if(objName.contains('AxtriaSalesIQTM__User_Access_Permission__c')){
            database.executeBatch(new BatchdeleteRecords ('AxtriaSalesIQTM__Team_Instance__c','Id',TeamInsid));
        }
        
        
        
    }

}