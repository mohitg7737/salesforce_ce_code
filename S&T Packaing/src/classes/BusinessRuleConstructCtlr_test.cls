/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test BusinessRuleConstructCtlr Controller.
*/

@isTest
private class BusinessRuleConstructCtlr_test {       
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c=true;
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.IsHCOSegmentationEnabled__c=true;
        teamins.MCCP_Enabled__c=true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        insert s;
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;

            BusinessRuleConstructCtlr obj=new BusinessRuleConstructCtlr();
            obj.selectedBuisnessUnit = 'None';
            obj.selectedCustType = '';
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            System.assert(!obj.brands.isEmpty(),'Assertion Error');
            obj.save();
            obj.saveAndNext();
            obj.isRuleValid();
            obj.fillLineOptions();
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c =true;
        countr.Channels__c = 'F2F';
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.IsHCOSegmentationEnabled__c=true;
        teamins.MCCP_Enabled__c=true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        insert s;
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;

            BusinessRuleConstructCtlr obj=new BusinessRuleConstructCtlr();
            obj.selectedBuisnessUnit = teamins.id;
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            System.assert(!obj.brands.isEmpty(),'Assertion Error');
            obj.save();
            obj.saveAndNext();
            obj.isRuleValid();
            obj.fillLineOptions();
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c =true;
        countr.Channels__c = 'F2F';
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.IsHCOSegmentationEnabled__c=true;
        teamins.MCCP_Enabled__c=true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        insert s;
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;

            BusinessRuleConstructCtlr obj=new BusinessRuleConstructCtlr();
            //obj.showCustomerType = true;
            //obj.showChannelList =true;
            obj.ruleObject=mmc;
            obj.Buset = null;
            obj.selectedBuisnessUnit = teamins.Id;
            obj.selectedCycle = workspace.id;
            obj.selectedBrand =pcc.Id;
            //obj.selectChannelList = null;
            //obj.selectedCustType = null;            
            obj.mode='new';
            obj.isRuleValid();
            obj.save();
            obj.saveAndNext();
            
            //obj.fillLineOptions();
        }
        Test.stopTest();
    }
}