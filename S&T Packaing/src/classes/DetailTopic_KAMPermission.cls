global class DetailTopic_KAMPermission implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public Boolean chaining = false;

    
    global DetailTopic_KAMPermission() 
    {

       query = 'SELECT SIQ_Employee_PRID__c,External_ID__c,SIQ_Product_vod__c, Id,Type__c FROM SIQ_My_Setup_Products_vod_O__c where Type__c= \'Product\'';

    }

    global DetailTopic_KAMPermission(Boolean chain) 
    {
       chaining = chain;
       query = 'SELECT SIQ_Employee_PRID__c,External_ID__c,SIQ_Product_vod__c, Id,Type__c FROM SIQ_My_Setup_Products_vod_O__c where Type__c= \'Product\'';

    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_My_Setup_Products_vod_O__c> scope)
    {
        Set<String> detailTopics = new Set<String>();
        Set<String> alreadyExists = new Set<String>();

        Map<String,Set<String>> kamMap= new Map<String,Set<String>>();

        for(SIQ_My_Setup_Products_vod_O__c prodT: scope)
        {
            detailTopics.add(prodT.SIQ_Product_vod__c);
        }

        List<DetailTopicToKAM__c> setupPermission = [SELECT Active__c,Id,Detail_Topic__c,KAM_Permission_Set__c,Name FROM DetailTopicToKAM__c where Detail_Topic__c in : detailTopics and Active__c=true ];

        for(DetailTopicToKAM__c detailKAM : setupPermission)
        {
            Set<String> tempString = kamMap.get(detailKAM.Detail_Topic__c);

            if(tempString == null)
            {
                tempString = new Set<String>();
            }

            tempString.add(detailKAM.KAM_Permission_Set__c);
            kamMap.put(detailKAM.Detail_Topic__c, tempString);
        }
        
        List<SIQ_My_Setup_Products_vod_O__c> userDetail= new  List<SIQ_My_Setup_Products_vod_O__c>();
        List<User_KAMPermission__c> KamsDetails = new List<User_KAMPermission__c>();

        for(SIQ_My_Setup_Products_vod_O__c prodT: scope)
        {
            String dTopic = prodT.SIQ_Product_vod__c;

            if(kamMap.containsKey(dTopic))
            {
                Set<String> kamPermissions = kamMap.get(dTopic);

                for(String str : kamPermissions)
                {
                    User_KAMPermission__c userKam = new User_KAMPermission__c();

                    userKam.SIQ_Employee_PRID__c = prodT.SIQ_Employee_PRID__c;
                    userKam.KAM_Permission_Set__c =  str ;

                    
                    userKam.External_ID__c= userKam.SIQ_Employee_PRID__c+'_'+ userKam.KAM_Permission_Set__c;
                    
                    if(!alreadyExists.contains(userKam.External_ID__c))
                    {
                        KamsDetails.add(userKam);                        
                        alreadyExists.add(userKam.External_ID__c);
                    }
                    
                }
            }
        }
       upsert KamsDetails External_ID__c;
    }

    global void finish(Database.BatchableContext BC)
    {
        /*if(chaining)
        {
            list<string> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
            //database.executeBatch(new changeTSFStatus(teaminstancelistis));
            AZ_Integration_Pack aip = new AZ_Integration_Pack(teaminstancelistis);            
        }*/

    }

}