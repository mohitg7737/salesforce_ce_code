global class update_TempPosAcc implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String teamInstance;

    global update_TempPosAcc(String teamIns) {
        query = ' ';
        teamInstance=teamIns;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        query= 'select id,Account__c,Status__c,Team_Instance__c from TempPosAcc__c where Team_Instance__c=:teamInstance and Status__c=\'New\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<TempPosAcc__c> scope) {
        System.debug('========query::::'+scope);
        for(TempPosAcc__c temp : scope){
            temp.Status__c='Processed';
        }
        update scope;
    }

    global void finish(Database.BatchableContext BC) {

    }
}