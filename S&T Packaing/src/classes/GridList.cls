public with sharing class GridList{
    public String selectedGridId {get;set;}
    public string allGrids {get;set;}

    public String countryID {get;set;}

    Map<string,string> successErrorMap;
    Public AxtriaSalesIQTM__Country__c Country {get;set;}

    public String gridID {get;set;}


    public GridList(){
        try{
           /* countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
            system.debug('##### countryID ' + countryID);
            successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
            system.debug('############ successErrorMap ' + successErrorMap);
            if(successErrorMap.containsKey('Success')){
               countryID = successErrorMap.get('Success');               
               system.debug('########## countryID from Map ' + countryID);
               //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
               SalesIQUtility.setCookieString('CountryID',countryID);
                Country = new AxtriaSalesIQTM__Country__c();
                Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
            }*/
            countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];

            list<GridListWrapper> GridWrapperList = new list<GridListWrapper>();
            Map<String, String> gridRuleMap = new Map<String, String>();
            Map<String, Grid_Master__c> gmMap = new Map<String, Grid_Master__c>([SELECT id,Name,Brand__r.Name,isGlobal__c, Dimension_1_Name__c,Dimension_2_Name__c,Grid_Type__c,Output_Name__c,Output_Type__c,Brand__r.Team_Instance__r.Name,CreatedDate,LastModifiedBy.Name from Grid_Master__c where Country__c = :countryID Order By CreatedDate DESC]);
            for(Step__c rule : [SELECT Id, Matrix__c, Measure_Master__r.Name FROM Step__c WHERE Matrix__c IN : gmMap.keySet()]){
                String tempString;
                if(gridRuleMap.containsKey(rule.Matrix__c)){
                    tempString = gridRuleMap.get(rule.Matrix__c);
                }else{
                    tempString = '';
                }
                tempString += rule.Measure_Master__r.Name;
                gridRuleMap.put(rule.Matrix__c, rule.Measure_Master__r.Name);
            }

            for(Grid_Master__c gm: [SELECT id,Name,Brand__r.Name,Dimension_1_Name__c,Dimension_2_Name__c,isGlobal__c, Grid_Type__c,Output_Name__c,Output_Type__c,Brand__r.Team_Instance__r.Name,CreatedDate,LastModifiedBy.Name from Grid_Master__c where Country__c = :countryID Order By CreatedDate DESC]){
                GridWrapperList.add(new GridListWrapper(gm, gridRuleMap.get(gm.Id)));
            }

            if(GridWrapperList != null && GridWrapperList.size() > 0){
                allGrids = JSON.serialize(GridWrapperList);
            }
        }
        catch(Exception e){
            SalesIQSnTLogger.createUnHandledErrorLogsforMatrix(e,SalesIQSnTLogger.BR_MODULE,'GridList','');
        }
    }

    public PageReference editMatrix()
    {
        pageReference pr = new PageReference('/apex/Matrices_Clone_Component?mode=Edit&msg='+gridID+'');
        pr.setRedirect(true);
        return pr;
    }

    public PageReference cloneMatrix()
    {
        pageReference pr = new PageReference('/apex/Matrices_Clone_Component?mode=Clone&msg='+gridID+'');
        pr.setRedirect(true);
        return pr;
    }

    public PageReference viewMatrix()
    {
        pageReference pr = new PageReference('/apex/Matrices_Clone_Component?mode=View&msg='+gridID+'');
        pr.setRedirect(true);
        return pr;
    }


    public class GridListWrapper{
        public String Id{get;set;}
        public String action{get;set;}
        public String name{get;set;}
        public String dimension{get;set;} 
        public String product{get;set;}
        public String cycle{get;set;}
        public DateTime createdOn{get;set;}
        public String lastUpdatedBy{get;set;}
        public String usedAt{get;set;}
        public Boolean isGlobal {get;set;}
       // public DateTime dateis {get;set;}
        public GridListWrapper(Grid_Master__c gridMaster, String rules){
            this.Id = gridMaster.Id;
            this.Name = gridMaster.Name;
            this.action = '';
            this.dimension = gridMaster.Grid_Type__c;
            this.product = gridMaster.Brand__r.Name;
            this.cycle = gridMaster.Brand__r.Team_Instance__r.Name;
            this.createdOn = gridMaster.CreatedDate;
            this.lastUpdatedBy = gridMaster.LastModifiedBy.Name;
            this.usedAt = rules;
            isGlobal = gridMaster.isGlobal__c;
        }
    }
}