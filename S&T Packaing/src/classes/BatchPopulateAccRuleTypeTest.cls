/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test BatchPopulateAccRuleType.
*/
@isTest
private class BatchPopulateAccRuleTypeTest { 
     static testMethod void testMethod5() {
    
        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'abcde';
        teamins1.IsHCOSegmentationEnabled__c = true;
        teamIns1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
        teamins1.Include_Secondary_Affiliations__c = true;
        teamins1.Include_Territory__c = true;
        teamins1.Product_Priority__c= true;
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
        insert scenario;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        //teamins.Cycle__c = cyc.id;
        insert teamins;
    
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //insert pos;
        
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //pos1.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        //insert pos1;
        
        AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
        uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
        uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        uAccessPerm.AxtriaSalesIQTM__User__c = loggedInUser.id;
        insert uAccessPerm;
        
        AxtriaSalesIQTM__Organization_Master__c org = TestDataFactory.createOrganizationMaster();
        insert org;
        
      
        Product2 pp2= TestDataFactory.newprod();
        pp2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert pp2;
         
        Product_Catalog__c prodCat = TestDataFactory.productCatalog(team,teamins,countr);
        prodCat.Team__c= team.id;
        prodCat.Team_Instance__c = teamins.id;
        prodCat.Country_Lookup__c = countr.id;
        insert prodCat;
        
        AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
        prod.Team__c = team.id;
        prod.Team_Instance__c = teamins.id; 
        insert prod;
        
        AxtriaSalesIQTM__Position_Product__c ppc = TestDataFactory.createPositionProduct(teamins,pos1,prodCat);
        //ppc.AxtriaSalesIQTM__Position__c = pos1.id;
        ppc.Product_Catalog__c = prodCat.id;
        ppc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        ppc.AxtriaSalesIQTM__Product_Master__c = prod.id;
        insert ppc;
        
        AxtriaSalesIQTM__Team_Instance_Product__c teaminsproduct = TestDataFactory.teamInstanceProduct(team,teamins,prod);
        teaminsproduct.AxtriaSalesIQTM__Product__c = pp2.id;
        teaminsproduct.AxtriaSalesIQTM__Product_Master__c = prod.id;
        teaminsproduct.AxtriaSalesIQTM__Team__c = team.id;
        teaminsproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert teaminsproduct;
        
        Team_Instance_Product_AZ__c teaminsprod = TestDataFactory.teamInstanceProductAZ(prodCat,teamins);
        teaminsprod.Team_Instance__c = teamins.id;
        teaminsprod.Product_Catalogue__c = prodCat.id;
        insert teaminsprod;
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'FillExternalIDonPosProduct';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert ccd;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'TeamInstanceProductTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        //ccd.Value__c = 'FillExternalIDonPosProduct';
        insert ccd1;
        
         temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';//
        zt.Country__c = countr.Id;//
        zt.Event__c = 'Delete';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;//
        zt.Country_Name__c = countr.Name;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;//
        zt.Team_Instance_Name__c = teamins.Name;//
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Object__c ='Deassign_Postiton_Account__c';//
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        zt.Product_Code__c = 'GIST';//
        zt.Product_Name__c = prodCat.id;
        zt.Holidays__c = 1;
        zt.Other_Days_Off__c = 1;
        zt.Vacation_Days__c = 1;
        zt.Calls_Per_Days__c  = 1;
        zt.Rule_Type__c ='test';//
        insert zt;
       Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
                    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            Populate_RuleType obj=new Populate_RuleType(true,countr.Id);
            Database.executeBatch(obj);            
        }
        Test.stopTest();
    }
}