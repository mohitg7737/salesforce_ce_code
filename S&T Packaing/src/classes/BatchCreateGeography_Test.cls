@isTest
private class BatchCreateGeography_Test {    
        
    static testMethod void Test_BatchCreateGeography() {
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Create Geography Delta';
        sJob.Job_Status__c = 'Successful';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;

        AxtriaSalesIQTM__Organization_Master__c orgMstr = new AxtriaSalesIQTM__Organization_Master__c();
        orgMstr.Name = 'Test Org';
        orgMstr.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgMstr.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        System.debug('orgMstr ID ' + orgMstr.Id);
        insert orgMstr;
        System.debug('orgMstr ID ' + orgMstr.Id);
        
        AxtriaSalesIQTM__Country__c contry = new AxtriaSalesIQTM__Country__c();
        //contry.AxtriaSalesIQTM__Country_Code__c = 'CH';
        contry.AxtriaSalesIQTM__Status__c = 'Active';       
        contry.AxtriaSalesIQTM__Parent_Organization__c = orgMstr.Id;
        System.debug('contry ID ' + contry.Id);
        insert contry;
        System.debug('contry ID ' + contry.Id);
        
        AxtriaSalesIQTM__Geography_Type__c geoType = new AxtriaSalesIQTM__Geography_Type__c();
        geoType.AxtriaSalesIQTM__Country__c = contry.Id;
        System.debug('geoType ID ' + geoType.Id);
        insert geoType;
        System.debug('geoType ID ' + geoType.Id);
        
        account acctss = new account();
        acctss.name = 'test';
        acctss.Marketing_Code__c = 'PL';
        System.debug('acctss ID  ' + acctss.Id);
        insert acctss;
        System.debug('acctss ID  ' + acctss.Id);
        
        AxtriaSalesIQTM__Account_Address__c accAdd = new AxtriaSalesIQTM__Account_Address__c();
        accAdd.AxtriaSalesIQTM__Account__c = acctss.id;
        accAdd.AxtriaSalesIQTM__Country__c = contry.Id;
        accAdd.AxtriaSalesIQTM__Pincode__c = 'TestPincode1';
        System.debug('accAdd ID ' + accAdd.Id);
        insert accAdd ;
        System.debug('accAdd ID ' + accAdd.Id);
        
        Database.executeBatch(new BatchCreateGeography());        
        //String jobvId = System.schedule('BatchCreateGeography',CRON_EXP1,new BatchCreateGeography());        
        }

}