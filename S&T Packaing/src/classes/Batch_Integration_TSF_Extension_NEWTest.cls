/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the Batch_Integration_TSF_Extension_NEW.
*/

@isTest
private class Batch_Integration_TSF_Extension_NEWTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.AxtriaSalesIQTM__Country__c = countr.id;
        
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        insert positionAccountCallPlan;
        List<String> allTeamInstances = new List<String>();
        allTeamInstances.add(teamins.id);
        
        SIQ_TSF_vod_O__c ss = new SIQ_TSF_vod_O__c();
        //ss.Country__c = countr.id;//CASESAFEID( Team_Instance_Lookup__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c )
        ss.Status__c = 'Updated';
        ss.Position_Account_Call_Plan__c = positionAccountCallPlan.id;
        ss.Team_Instance_Lookup__c= teamins.id;
        insert ss;
        //where Country__c in :allCountries and Status__c != \'Updated\' 
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
                Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Batch_Integration_TSF_Extension_NEW o3=new Batch_Integration_TSF_Extension_NEW(allTeamInstances);
            Database.executeBatch(o3);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        insert positionAccountCallPlan;
        
        SIQ_TSF_vod_O__c ss = new SIQ_TSF_vod_O__c();
        //ss.Country__c = countr.id;//CASESAFEID( Team_Instance_Lookup__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c )
        ss.Status__c = 'Updated';
        ss.Position_Account_Call_Plan__c = positionAccountCallPlan.id;
        ss.Team_Instance_Lookup__c= teamins.id;
        insert ss;
        List<String> allTeamInstances = new List<String>();
        allTeamInstances.add(teamins.id);
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
                Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Batch_Integration_TSF_Extension_NEW o3=new Batch_Integration_TSF_Extension_NEW(allTeamInstances,false);
            Database.executeBatch(o3);
        }
        Test.stopTest();
    }
}