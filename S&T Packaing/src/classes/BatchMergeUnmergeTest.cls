@isTest
public class BatchMergeUnmergeTest {
    
    static  testmethod void mergetest(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';

        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461998_US';
         SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455998_US';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
       SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
         SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH10455998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH10455998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH10461998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH10461998';
        merge1.Status__c = 'New';
       SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void merge2(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461998_US';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Inactive';
        acc.Merge_Account_Number__c =  'BH10455998_US';
       SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
       SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
         SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
       SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
         SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
         SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH10455998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH10455998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH10461998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH10461998';
        merge1.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void merge3(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Inactive';
        a1.Merge_Account_Number__c = 'BH10461998_US';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Inactive';
        acc.Merge_Account_Number__c =  'BH10455998_US';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
       SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
       SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH10455998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH10455998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH10461998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH10461998';
        merge1.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void merge4(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
       SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
         SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Inactive';
        a1.Merge_Account_Number__c = 'BH10461998_US';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Inactive';
        acc.Merge_Account_Number__c =  'BH10455998_US';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
       SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
         SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
       SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
       SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH104587998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH104587998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH10461998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH10461998';
        merge1.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void merge5(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Inactive';
        a1.Merge_Account_Number__c = 'BH10461998_US';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Inactive';
        acc.Merge_Account_Number__c =  'BH10455998_US';
       SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
       SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
       SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
         SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
         SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH10455998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH10455998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH1042361998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH1046231998';
        merge1.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void merge6(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        String classname = 'BatchMergeUnmergeTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
       
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10455998';
        a1.Status__c = 'Inactive';
        a1.Merge_Account_Number__c = 'BH10461998_US';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10461998';
        acc.Status__c = 'Inactive';
        acc.Merge_Account_Number__c =  'BH10455998_US';
       SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
           AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
         SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        SIQ_MC_Country_Mapping__c countmapp = new SIQ_MC_Country_Mapping__c();
        countmapp.SIQ_Country_Code__c = 'US';
        countmapp.SIQ_Veeva_Country_Code__c = 'US';
        SnTDMLSecurityUtil.insertRecords(countmapp,className);
        
        SIQ_CM_Merge_Unmerge__c merge1 = new SIQ_CM_Merge_Unmerge__c();
        merge1.SIQ_Country_Code__c = 'US';
        merge1.SIQ_MDM_Customer_GUID_1__c = 'BH1045125998';
        merge1.SIQ_MDM_Customer_ID_1__c = 'BH1045521998';
        merge1.SIQ_Market_Code__c = 'US';
        merge1.GUID1_GUID2_Key__c = 'BH10455998_BH10461998';
        merge1.SIQ_MDM_Customer_GUID_2__c = 'BH1042361998';
        merge1.SIQ_MDM_Customer_ID_2__c = 'BH1046231998';
        merge1.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(merge1,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'SIQ_Country_Code__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(SIQ_CM_Merge_Unmerge__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchMergeUnmerge obj=new BatchMergeUnmerge();
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    
  }