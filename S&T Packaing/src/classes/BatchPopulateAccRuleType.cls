global class BatchPopulateAccRuleType implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> setTDDeassignID;
    public Set<String> setBUDeassignID;
    public Boolean flag{get;set;}
    public String countryID;
    public String countryName;

    global BatchPopulateAccRuleType(Boolean flagg, String country) {
      /*  query = '';
        countryID='';
        countryName='';
        flag=flagg;
        countryID=country;
        System.debug('====countryID:::::::' +countryID);
        System.debug('====flag:::::::' +flag);
        setTDDeassignID=new Set<String>();
        setBUDeassignID=new Set<String>();
        query='select id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c,Country_Name__c from Deassign_Postiton_Account__c where Status__c=\'New\'';*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Deassign_Postiton_Account__c> scope) {
        
      /*  System.debug('====Query:::::::' +scope);

        Set<String> setAccount=new Set<String>();
        Set<String> setTeamIns=new Set<String>();
        Set<String> setScenario=new Set<String>();
        Map<String,String> mapAccNum2AccType=new Map<String,String>();
        Map<String,String> mapTeamIns2Scenario=new Map<String,String>();
        Map<String,String> mapScenario2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2Country=new Map<String,String>();
        List<Deassign_Postiton_Account__c> deassignList=new List<Deassign_Postiton_Account__c>();

        List<AxtriaSalesIQTM__Country__c> countryList=[Select Id,Name from AxtriaSalesIQTM__Country__c where id = :countryID];
        countryName=countryList[0].Name;
        System.debug('====countryName:::::::' +countryName);

        for(Deassign_Postiton_Account__c deassignRec : scope)
        {
            setTeamIns.add(deassignRec.Team_Instance__c);
            setAccount.add(deassignRec.Account__c);
        }

        System.debug('====setTeamIns:::::::' +setTeamIns);
        System.debug('====setAccount:::::::' +setAccount);

        List<Account> accList = [select Id,AccountNumber,Type from Account where AccountNumber in :setAccount and AccountNumber != null and AxtriaSalesIQTM__Active__c='Active' and Type!=null];

        for(Account acc : accList)
        {
            mapAccNum2AccType.put(acc.AccountNumber,acc.Type);
        }

        System.debug('====mapAccNum2AccType:::::::' +mapAccNum2AccType);

        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id,Name,AxtriaSalesIQTM__Scenario__c,Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where Name in :setTeamIns and AxtriaSalesIQTM__Scenario__c != null];

        for(AxtriaSalesIQTM__Team_Instance__c tiRec : teamInsList)
        {
            mapTeamIns2Scenario.put(tiRec.Name,tiRec.AxtriaSalesIQTM__Scenario__c);
            mapTeamIns2Country.put(tiRec.Name,tiRec.Country_Name__c);
            setScenario.add(tiRec.AxtriaSalesIQTM__Scenario__c);
        }

        System.debug('====mapTeamIns2Scenario:::::::' +mapTeamIns2Scenario);
        System.debug('====setScenario:::::::' +setScenario);

        List<AxtriaSalesIQTM__Business_Rules__c> bussRuleList = [select Id, AxtriaSalesIQTM__Rule_Type__c,AxtriaSalesIQTM__Scenario__c from AxtriaSalesIQTM__Business_Rules__c where AxtriaSalesIQTM__Scenario__c in :setScenario and AxtriaSalesIQTM__Rule_Type__c!=null];

         for(AxtriaSalesIQTM__Business_Rules__c busRule : bussRuleList)
         {
            mapScenario2RuleType.put(busRule.AxtriaSalesIQTM__Scenario__c,busRule.AxtriaSalesIQTM__Rule_Type__c);
         }

         System.debug('====mapScenario2RuleType:::::::' +mapScenario2RuleType);

         for(String teamIns : mapTeamIns2Scenario.keySet())
         {
            if(mapTeamIns2Scenario.get(teamIns) != null)
            {
                String scenario = mapTeamIns2Scenario.get(teamIns);
                System.debug('====scenario:::::::' +scenario);
                String ruleType=mapScenario2RuleType.get(scenario);
                System.debug('====ruleType:::::::' +ruleType);
                mapTeamIns2RuleType.put(teamIns,ruleType);
            }
         }
         System.debug('====mapTeamIns2RuleType:::::::' +mapTeamIns2RuleType);

         for(Deassign_Postiton_Account__c deassignRec : scope)
         {
            deassignRec.Account_Type__c=mapAccNum2AccType.get(deassignRec.Account__c);
            deassignRec.Rule_Type__c=mapTeamIns2RuleType.get(deassignRec.Team_Instance__c);
            deassignRec.Country_Name__c=mapTeamIns2Country.get(deassignRec.Team_Instance__c);
            //setDeassignID.add(deassignRec.Id);
            deassignList.add(deassignRec);
            System.debug('====deassignRec:::::::' +deassignRec);
         }

         System.debug('====deassignList.size():::::::' +deassignList.size());

         if(deassignList.size() > 0)
            update deassignList;

        for(Deassign_Postiton_Account__c deassignRec : deassignList)
        {
            if(deassignRec.Rule_Type__c == 'Top Down')
            {
                setTDDeassignID.add(deassignRec.Id);
            }
            if(deassignRec.Rule_Type__c == 'Bottom Up')
            {
                setBUDeassignID.add(deassignRec.Id);
            }
        }
        System.debug('====setTDDeassignID:::::::' +setTDDeassignID);
        System.debug('====setBUDeassignID:::::::' +setBUDeassignID);*/
    }

    global void finish(Database.BatchableContext BC) {

        
       /* if(setTDDeassignID != null)
        {
            System.debug('=========Calling Deassignment Position Account Batch for Top Down Rule Type:::::::::::::::::::::::::::');
            BatchDeassignPositionAccounts_New posAcc = new BatchDeassignPositionAccounts_New(setTDDeassignID,flag,countryName);
            Database.executeBatch(posAcc,10);
        }

        if(setBUDeassignID != null)
        {
            System.debug('=========Calling Deassignment Position Account Batch for Bottom Up Rule Type:::::::::::::::::::::::::::');
            BatchDeassignBottomUpPosition_New buPosAcc = new BatchDeassignBottomUpPosition_New(setBUDeassignID,flag,countryName);
            Database.executeBatch(buPosAcc,10);
        } */
    }

}