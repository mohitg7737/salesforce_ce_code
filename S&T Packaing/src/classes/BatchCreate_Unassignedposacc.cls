global class BatchCreate_Unassignedposacc implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    public String query;
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;

    global BatchCreate_Unassignedposacc() {
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Unassigned PosAcc Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0)
        {
          lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
          lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Unassigned PosAcc Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;

        query = 'select id,AccountNumber,BillingCountry,Marketing_Code__c,AxtriaSalesIQTM__Active__c,Country_Code__c from Account';
        if(lastjobDate!=null){
          query = query + ' Where CreatedDate  >=:  lastjobDate '; 
        }
        this.query = query;

        system.debug('===============The query is:::'+query);

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {
        map<string,list<Account>> country2accmap = new map<string,list<Account>>();
        map<string,list<AxtriaSalesIQTM__Position_Team_Instance__c>> country2posteaminst = new map<string,list<AxtriaSalesIQTM__Position_Team_Instance__c>>();
        list<UnassignedAccount__mdt>cmdata = new list<UnassignedAccount__mdt>();
        set<string>Countrycode = new set<string>();
        cmdata = [select id,Label from UnassignedAccount__mdt];
        if(cmdata!=null)
        {
            for(UnassignedAccount__mdt mdt : cmdata)
            {
                Countrycode.add(mdt.Label);
            }

        }

        //filling country2accmap county wise from scope
        for(Account a: scope)
        {
            if(a.Country_Code__c !=null)
            {
                if(!country2accmap.containsKey(a.Country_Code__c))
                {

                    country2accmap.put(a.Country_Code__c,new list<account>{a});
                }
                else
                {
                    country2accmap.get(a.Country_Code__c).add(a);   
                }
            }
        }
        system.debug('===============country2accmap.Keyset():::'+country2accmap.keySet());
        system.debug('===============country2accmap:::'+country2accmap);
        set<string>period= new set<string>();
        period.add('Current');
        period.add('Future');

        list<AxtriaSalesIQTM__Position_Team_Instance__c>postlist = [select id,name,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN : country2accmap.keyset() and AxtriaSalesIQTM__Position_ID__r.name='Unassigned' and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c IN : period
        ];

        for(AxtriaSalesIQTM__Position_Team_Instance__c post : postlist){
            string key=post.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            if(!country2posteaminst.containsKey(key))
            {
                country2posteaminst.put(key,new list<AxtriaSalesIQTM__Position_Team_Instance__c>{post});
            }
            else
            {
                country2posteaminst.get(key).add(post);
            }

        }
        system.debug('===================country2posteaminst::::'+country2posteaminst.size());

        //creating position account records by itteration on country2accmap
        list<AxtriaSalesIQTM__Position_Account__c>newposacclist = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(String country : country2accmap.keySet())
        {
            if(Countrycode.contains(country))
            {
                list<Account>acclist = country2accmap.get(country);
                
                list<AxtriaSalesIQTM__Position_Team_Instance__c>posteam = country2posteaminst.get(country);
                if(acclist !=null && posteam!=null)
                {
                    for(Account acc : acclist)
                    {
                        for(AxtriaSalesIQTM__Position_Team_Instance__c pst : posteam)
                        {
                            AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c();
                            pa.AxtriaSalesIQTM__Account__c= acc.id;
                            pa.AxtriaSalesIQTM__Position__c = pst.AxtriaSalesIQTM__Position_ID__c;
                            pa.AxtriaSalesIQTM__Team_Instance__c = pst.AxtriaSalesIQTM__Team_Instance_ID__c;
                            pa.AxtriaSalesIQTM__Position_Team_Instance__c = pst.id;
                            pa.AxtriaSalesIQTM__Proposed_Position__c = pst.AxtriaSalesIQTM__Position_ID__c;
                            pa.AxtriaSalesIQTM__Effective_Start_Date__c = pst.AxtriaSalesIQTM__Effective_Start_Date__c;
                            pa.AxtriaSalesIQTM__Effective_End_Date__c = pst.AxtriaSalesIQTM__Effective_End_Date__c;
                            pa.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';

                            newposacclist.add(pa);
                        }
                        recordsProcessed++;
                    } 
                }
                
            }
        }
        //recordsProcessed +=newposacclist.size();
        if(newposacclist !=null && newposacclist.size()>0){
            insert newposacclist;
        }
        
    }

    global void execute(SchedulableContext sc){
        Database.executeBatch(new BatchCreate_Unassignedposacc(), 1000);

    }
    global void finish(Database.BatchableContext BC) {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        update sJob;
      

    }
}