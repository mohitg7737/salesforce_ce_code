global class CreateNewUser implements Schedulable
{
	List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
	public DateTime lastjobDate=null;
	public String query;
	public set<string> setprid ;
	public set<string> setuserid;
	public set<string> setnewuser;
	map<string,AxtriaSalesIQTM__Employee__c> mappridtoemp;
	list<user> newuserinsert;
	map<string,Time_Zone__c> timeZOne;
	map<string,string> mappridtocountry;
	public string profileid;
	public List<sObject> lsPermissionset = new List<sObject>();
	public List<sObject> permissionToInsert = new List<sObject>();
	//List<Time_Zone__c> time;
	List<Profile> pro;
	List<UserRole> role;
	list<string>listOfIds;
    public string newprid;
    Set<String> employeePRID;
    set<string> employeeEmpID;
    set<string> employeeStagingPrid;
    map<string,string> pridtocountrycodemap;
    Map<String,String> pridtocountryveevacodemap;
    Map<String,string> mapVeevaCode;
    set<string> setempprid;
    List<user> newUserStagInsert;
     List<user> newUsersiqinsert;
    Set<String> userfedidset;
    global list<ErrorLogEmail__c> emailIds{get;set;}
    public string header;

    global CreateNewUser()
    {
        
    }
    global CreateNewUser(Set<String> empPrid,String delta)
    {
       employeeStagingPrid= new Set<String>();
       employeeStagingPrid.addAll(empPrid);
       setempprid= new Set<String>();
       pridtocountrycodemap= new Map<string,string>();
       pridtocountryveevacodemap= new Map<string,string>();
       mapVeevaCode= new Map<string,string>();
       newUserStagInsert= new List<User>();
       userfedidset= new Set<String>();
       newUsersiqinsert= new List<User>();
       timeZOne = new map<string,Time_Zone__c>();
       listOfIds = new list<string>();

       list<string>emaillist = new list<string>();
       String finalString;
       List<SIQ_Employee_Master__c> empmaster=[Select Id,SIQ_Employee_ID__c,Dummy_Prid__c,SIQ_Last_Name__c,SIQ_First_Name__c, SIQ_Email__c,SIQ_PRID__c,SIQ_Country__c,SIQ_Country_Code__c from SIQ_Employee_Master__c where SIQ_PRID__c in :employeeStagingPrid];
       system.debug('+++empmaster++'+empmaster);
       
       for(SIQ_Employee_Master__c employees: empmaster)
       {
        setempprid.add(employees.SIQ_PRID__c);
        pridtocountrycodemap.put(employees.SIQ_PRID__c,employees.SIQ_Country_Code__c);
       }
       system.debug('+++setempprid++'+setempprid);
       system.debug('+++pridtocountrycodemap++'+pridtocountrycodemap);

        list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
        for(SIQ_MC_Country_Mapping__c c : countryMap)
        {
          if(c.SIQ_Country_Code__c!=null && !mapVeevaCode.containskey(c.SIQ_Country_Code__c))
            {
                mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
            }
        }
        system.debug('+++mapVeevaCode'+mapVeevaCode);
        for(string prid: pridtocountrycodemap.keyset())
        {
            system.debug('++prid'+prid);
            pridtocountryveevacodemap.put(prid,mapVeevaCode.get(pridtocountrycodemap.get(prid)));
        }
        system.debug('+++pridtocountryveevacodemap'+pridtocountryveevacodemap);

       List<Time_Zone__c> tz = [Select Country_Name__c,Language__c,Locale__c,SIQ_Country_Code__c,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,Zone_Time__c from Time_Zone__c];
        for(Time_Zone__c zone : tz)
        {   
            system.debug('====================Countrycode::'+zone.SIQ_Country_Code__c);
            system.debug('==============ZONE iS:'+zone);
            timeZOne.put(zone.SIQ_Country_Code__c,zone);
        }
        system.debug('+++timeZOne+++'+timeZOne);
       List<User> user1= [select id, FederationIdentifier from User where FederationIdentifier in: employeeStagingPrid];
       system.debug('+++user1++'+user1.size());
        pro= [SELECT Id,Name FROM Profile WHERE Name = 'Rep1' limit 1];
        system.debug('=================PROFILE :::'+pro);
        role= [SELECT Id,Name  FROM UserRole WHERE Name = 'Rep' limit 1];
        profileid = string.valueof(pro[0].id);

       if(user1==null||user1.size()==0)
       {
        system.debug('=====hey');
        for(SIQ_Employee_Master__c masteremployee : empmaster)
           {
             string countryemployee= pridtocountryveevacodemap.get(masteremployee.SIQ_PRID__c);         
             system.debug('============Country is ::'+masteremployee.SIQ_PRID__c+'================'+countryemployee);
             if(timeZOne.containsKey(countryemployee)) 
                {
                    if(masteremployee.Dummy_Prid__c==false)
                      {
                        system.debug('==========================About to create a new user:');
                         Time_Zone__c tzone= timeZOne.get(countryemployee);
                         user u= new user();
                         u.email= masteremployee.SIQ_Email__c;
                         u.IsActive=TRUE;
                         u.FirstName=masteremployee.SIQ_First_Name__c;
                         u.LastName=masteremployee.SIQ_Last_Name__c;
                         u.username= masteremployee.SIQ_Email__c;
                         u.FederationIdentifier= (masteremployee.SIQ_PRID__c).toLowerCase();
                         u.ProfileID=profileid;
                         u.UserRole=role[0];
                         u.Country=countryemployee;
                         if(masteremployee.SIQ_PRID__c.length()>=7)
                            {
                                u.Alias= masteremployee.SIQ_PRID__c.substring(0,7);
                            }
                          else
                          {
                           u.Alias= masteremployee.SIQ_PRID__c; 
                          }
                         u.CommunityNickname=masteremployee.SIQ_First_Name__c+masteremployee.SIQ_Last_Name__c;
                         u.EmailEncodingKey='unicode(UTF-8)';
                         u.EmailEncodingKey='ISO-8859-1';
                         u.TimeZoneSidKey= tzone.Zone_Time__c;  //'GMT'
                         u.LocaleSidKey= tzone.Locale__c;    //'en_US'
                         u.LanguageLocaleKey=tzone.Language__c;
                         newUsersiqinsert.add(u);
                      }
                    else
                      {
                         system.debug('==========================About to create a dummy user:');
                         Time_Zone__c tzone= timeZOne.get(countryemployee);
                         user u= new user();
                         u.email= masteremployee.SIQ_Email__c;
                         u.IsActive=TRUE;
                         u.FirstName=masteremployee.SIQ_First_Name__c;
                         u.LastName=masteremployee.SIQ_Last_Name__c; 
                         u.username= masteremployee.SIQ_Email__c+'_'+countryemployee;
                         u.FederationIdentifier= (masteremployee.SIQ_PRID__c).toLowerCase();
                         u.ProfileID=profileid;
                         u.UserRole=role[0];
                         u.Country=countryemployee;
                         if(masteremployee.SIQ_PRID__c.length()>=7)
                          {
                            u.Alias= masteremployee.SIQ_PRID__c.substring(0,7);
                          }
                          else
                          {
                            u.Alias= masteremployee.SIQ_PRID__c; 
                          }
                          u.CommunityNickname=masteremployee.SIQ_First_Name__c+masteremployee.SIQ_Last_Name__c;
                          u.EmailEncodingKey='unicode(UTF-8)';
                          u.EmailEncodingKey='ISO-8859-1';
                          u.TimeZoneSidKey= tzone.Zone_Time__c;  //'GMT'
                          u.LocaleSidKey= tzone.Locale__c;    //'en_US'
                          u.LanguageLocaleKey=tzone.Language__c;
                          newUsersiqinsert.add(u);
                      }
                }
            }

              system.debug('+++newuserinsert+++'+newUsersiqinsert.size());
              if(newUsersiqinsert!=null && newUsersiqinsert.size() >0)
              {
                database.SaveResult[] insertResults=  database.insert(newUsersiqinsert,false);
                for (Database.SaveResult sr : insertResults) 
                  {
                      if (sr.isSuccess())
                      {
                          listOfIds.add((string)sr.getId());
                      }
                      else
                      {
                          //system.debug('Error Message'+sr.getErrors());
                          for(Database.Error err : sr.getErrors())
                          {
                             system.debug('Error Message'+sr.getErrors()); 
                             System.debug(err.getStatusCode() + ': ' + err.getMessage());
                             finalString= err.getMessage();
                             system.debug('finalString'+finalString);
                          } 
                          header= finalString;
                          system.debug('header'+header); 
                      }
                  }
                system.debug('==================new inserted users count is :'+listOfIds);
              }
              if(header!=null)
              {
                Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                attach.setFileName('NewUserError.xlsx');
                String subject = 'New User error';
                emailIds = ErrorLogEmail__c.getall().values();
                if(emailIds!=null)
                {
                    for(ErrorLogEmail__c LEM:emailIds)
                    {
                        emaillist.add(LEM.Name);
                    }
                }
              
                Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage(); 
                emailwithattch.setSubject(subject);   
                emailwithattch.setToAddresses(emaillist);
                emailwithattch.setPlainTextBody(header);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
              }
             

              List<User> userid= [Select id, FederationIdentifier From User where id in:listOfIds];
              set<string> userfed = new set<string>();
              for(user userstaginsert : userid)
              {
                   userfed.add(userstaginsert.FederationIdentifier);
              }  
              system.debug('+++userfed+++'+userfed);
              BatchUpdateUserLookupinEmp lookupemp = new BatchUpdateUserLookupinEmp(userfed);
              database.executebatch(lookupemp,2000);                      
        }

       for(User newuser: user1)
       {
           userfedidset.add(newuser.FederationIdentifier);
       }
        if(user1!=null && user1.size()>0)
        {    
          for(SIQ_Employee_Master__c masteremp : empmaster)
            {
                if(userfedidset.contains(masteremp.SIQ_PRID__c))
                {
                    system.debug('user is there'+masteremp.SIQ_PRID__c);
                }
                else
                {
                    
                     string countryemp= pridtocountryveevacodemap.get(masteremp.SIQ_PRID__c);
                     system.debug('============Country is ::'+masteremp.SIQ_PRID__c+'================'+countryemp);
                     if(timeZOne.containsKey(countryemp)) 
                        {
                            if(masteremp.dummy_prid__c==false)
                            {
                                system.debug('==========================About to create a new user:');
                                 Time_Zone__c tzone= timeZOne.get(countryemp);
                                 user u= new user();
                                 u.email= masteremp.SIQ_Email__c;
                                 u.IsActive=TRUE;
                                 u.FirstName=masteremp.SIQ_First_Name__c;
                                 u.LastName=masteremp.SIQ_Last_Name__c;
                                 u.username= masteremp.SIQ_Email__c;
                                 u.FederationIdentifier= (masteremp.SIQ_PRID__c).toLowerCase();
                                 u.ProfileID=profileid;
                                 u.UserRole=role[0];
                                 u.Country=countryemp;
                                if(masteremp.SIQ_PRID__c.length()>=7)
                                  {
                                      u.Alias= masteremp.SIQ_PRID__c.substring(0,7);
                                  }
                                else
                                  {
                                      u.Alias= masteremp.SIQ_PRID__c; 
                                  }
                                u.CommunityNickname=masteremp.SIQ_First_Name__c+masteremp.SIQ_Last_Name__c;
                                u.EmailEncodingKey='unicode(UTF-8)';
                                u.EmailEncodingKey='ISO-8859-1';
                                u.TimeZoneSidKey= tzone.Zone_Time__c;  //'GMT'
                                u.LocaleSidKey= tzone.Locale__c;    //'en_US'
                                u.LanguageLocaleKey=tzone.Language__c;
                                newUserStagInsert.add(u);
                            }
                            else
                            {
                                system.debug('==========================About to create a dummy user:');
                                 Time_Zone__c tzone= timeZOne.get(countryemp);
                                 user u= new user();
                                 u.email= masteremp.SIQ_Email__c;
                                 u.IsActive=TRUE;
                                 u.FirstName=masteremp.SIQ_First_Name__c;
                                 u.LastName=masteremp.SIQ_Last_Name__c;
                                 u.username= masteremp.SIQ_Email__c+'_'+countryemp;
                                 u.FederationIdentifier= (masteremp.SIQ_PRID__c).toLowerCase();
                                 u.ProfileID=profileid;
                                 u.UserRole=role[0];
                                 u.Country=countryemp;
                                 if(masteremp.SIQ_PRID__c.length()>=7)
                                  {
                                    u.Alias= masteremp.SIQ_PRID__c.substring(0,7);
                                  }
                                  else
                                  {
                                    u.Alias= masteremp.SIQ_PRID__c; 
                                  }
                                u.CommunityNickname=masteremp.SIQ_First_Name__c+masteremp.SIQ_Last_Name__c;
                                u.EmailEncodingKey='unicode(UTF-8)';
                                u.EmailEncodingKey='ISO-8859-1';
                                u.TimeZoneSidKey= tzone.Zone_Time__c;  //'GMT'
                                u.LocaleSidKey= tzone.Locale__c;    //'en_US'
                                u.LanguageLocaleKey=tzone.Language__c;
                                newUserStagInsert.add(u);
                            }
                        }
                }    
            }

             system.debug('+++newuserinsert+++'+newUserStagInsert.size());
                         if(newUserStagInsert!=null && newUserStagInsert.size() >0)
                          {
                            database.SaveResult[] insertResults=  database.insert(newUserStagInsert,false);
                            for (Database.SaveResult sr : insertResults) 
                            {
                                if (sr.isSuccess())
                                {
                                    listOfIds.add((string)sr.getId());
                                }
                                else
                                {
                                    //system.debug('Error Message'+sr.getErrors());
                                    for(Database.Error err : sr.getErrors())
                                    {
                                       system.debug('Error Message'+sr.getErrors()); 
                                       System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                       finalString= err.getMessage();
                                       system.debug('finalString'+finalString);
                                    }
                                    header= finalString;
                                    system.debug('header'+header);
                                    
                                }     
                            }
                            system.debug('==================new inserted users count is :'+listOfIds);
                          }
                       
                        if(header!=null)
                        {
                          Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                          attach.setFileName('NewUserError.xlsx');
                          String subject = 'New User error';
                          emailIds = ErrorLogEmail__c.getall().values();
                          if(emailIds!=null)
                          {
                              for(ErrorLogEmail__c LEM:emailIds)
                              {
                                  emaillist.add(LEM.Name);
                              }
                          }
                        
                          Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage(); 
                          emailwithattch.setSubject(subject);   
                          emailwithattch.setToAddresses(emaillist);
                          emailwithattch.setPlainTextBody(header);
                          Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
                        }

                        List<User> userid= [Select id, FederationIdentifier From User where id in:listOfIds];
                            set<string> userfed = new set<string>();
                            for(user userstag : userid)
                            {
                                 userfed.add(userstag.FederationIdentifier);
                            }  
                            system.debug('+++userfed+++'+userfed);
                            BatchUpdateUserLookupinEmp lookupemp = new BatchUpdateUserLookupinEmp(userfed);
                            database.executebatch(lookupemp,2000);                               
         }   
     
    }
    global CreateNewUser(Set<string> empPrid )
    {
    	//newprid= '';
    	//newprid= prid;
    	employeePRID= new Set<String>();
        employeePRID.addAll(empPrid);
		setprid= new set<String>();
		setuserid= new set<String>();
		setnewuser= new set<String>();
		mappridtoemp= new map<string,AxtriaSalesIQTM__Employee__c>();
		newuserinsert=new list<user>();
		pro= new List<Profile>();
		role= new List<UserRole>();
		mappridtocountry= new map<string,string>();
		timeZOne= new map<string,Time_Zone__c>();

		profileid='';
		
		query = 'SELECT id,AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Last_Name__c,AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Email__c,Employee_PRID__c,AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c FROM AxtriaSalesIQTM__Employee__c '  + 'where Employee_PRID__c in :employeePRID';
   		createuser();
		
    }
    
    global void createuser()
    {
    	system.debug('=============query=======:'+query);
    	list<AxtriaSalesIQTM__Employee__c>scope = Database.query(query);
    	listOfIds = new list<string>();
    	List<Time_Zone__c> tz = [Select Country_Name__c,Language__c,Locale__c,SIQ_Country_Code__c,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,Zone_Time__c from Time_Zone__c];
		for(Time_Zone__c zone : tz)
		{
			timeZOne.put(zone.SIQ_Country_Code__c,zone);
		}
		system.debug('+++timeZOne+++'+timeZOne);
	for(AxtriaSalesIQTM__Employee__c employee:scope)
     {
         setprid.add(employee.Employee_PRID__c);
         mappridtoemp.put(employee.Employee_PRID__c,employee);
         mappridtocountry.put(employee.Employee_PRID__c,employee.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c);
     }
     system.debug('+++setprid+++'+setprid);
     system.debug('+++mappridtoemp+++'+mappridtoemp);
     system.debug('+++mappridtocountry+++'+mappridtocountry);
     List<user> uss= [select id, FederationIdentifier from User];// where FederationIdentifier in:setprid
     for(User u: uss)
     {
         setuserid.add(u.FederationIdentifier);
     }
      system.debug('+++setuserid+++'+setuserid);
     for(string user : setprid)
     {
         if(!setuserid.contains(user))
         {
             setnewuser.add(user);
         }
         
     }
	system.debug('+++setnewuser+++'+setnewuser.size());
	pro= [SELECT Id,Name FROM Profile WHERE Name = 'Rep1' limit 1];
	system.debug('=================PROFILE :::'+pro);
	role=  [SELECT Id,Name FROM UserRole WHERE Name = 'Rep' OR Name = 'Rep1' limit 1];
	profileid = string.valueof(pro[0].id);
	system.debug('================profileid:::::::::::'+profileid);
 
    for(string newprid: setnewuser)
    {    
        string country= mappridtocountry.get(newprid);
         system.debug('============Country is ::'+newprid+'================'+country);
         if(timeZOne.containsKey(country)) 
        {
            system.debug('==========================About to create a new user:');
         Time_Zone__c tzone= timeZOne.get(country);
         user u= new user();
         AxtriaSalesIQTM__Employee__c emp= new AxtriaSalesIQTM__Employee__c();
         emp= mappridtoemp.get(newprid);
         u.email= emp.AxtriaSalesIQTM__Email__c;
         u.IsActive=TRUE;
         //u.Name = emp.Employee_First_Name__c;
         u.username= emp.AxtriaSalesIQTM__Email__c;
         u.FederationIdentifier= (emp.Employee_PRID__c).toLowerCase();
         u.ProfileID=profileid;
         u.UserRole=role[0];
         u.FirstName=emp.AxtriaSalesIQTM__FirstName__c;
         u.LastName=emp.AxtriaSalesIQTM__Last_Name__c;
         u.Alias= emp.Employee_PRID__c.substring(0,7);
         u.CommunityNickname=emp.AxtriaSalesIQTM__FirstName__c;
         u.EmailEncodingKey='unicode(UTF-8)';
         //u.DefaultCurrencyIsoCode='EUR-Euro';
         u.TimeZoneSidKey= tzone.Zone_Time__c;//'GMT'
         u.LocaleSidKey= tzone.Locale__c;//'en_US'
         u.LanguageLocaleKey= tzone.Language__c;//'en_US';
         u.EmailEncodingKey='ISO-8859-1';
         //u.CurrencyIsoCode='USD';
         newuserinsert.add(u);
        }
     }
     system.debug('+++newuserinsert+++'+newuserinsert.size());
     if(newuserinsert!=null && newuserinsert.size() >0)
     {
        database.SaveResult[] insertResults=  database.insert(newuserinsert,false);
        for (Database.SaveResult sr : insertResults) 
        {
		    if (sr.isSuccess())
		    {
		        listOfIds.add((string)sr.getId());
		    }
		    else
		    {
		    	system.debug('Error Message'+sr.getErrors());
		    }
		}
		system.debug('==================new inserted users count is :'+listOfIds);
        
        // string permission = lsPermissionset[0].id;
         //assignpermissionset(listOfIds,permission);
    }
  
        List<User> userid= [select id, FederationIdentifier From User where id in:listOfIds];
        set<string> userfed = new set<string>();
        for(user user1 : userid)
        {
             userfed.add(user1.FederationIdentifier);
        }  
        system.debug('+++userfed+++'+userfed);
        BatchUpdateUserLookupinEmp lookupemp = new BatchUpdateUserLookupinEmp(userfed);
        database.executebatch(lookupemp,2000);
    
    }
      
    global static void assignpermissionset(list<string>users,string permissionid)
    {
       /*	system.debug('==========================Hey Future method called=================');
    	system.debug('===========================Users list:::'+users);
    	list<PermissionSetAssignment>insertperm = new list<PermissionSetAssignment>();
    	for(string us : users)
         {
           PermissionSetAssignment psa= new PermissionSetAssignment();
           psa.PermissionSetId = permissionid;
           psa.AssigneeId = us;
           insertperm.add(psa);
         }
         system.debug('+++permissionToInsert++'+insertperm);
         if(insertperm !=null && insertperm.size()>0 )
         {
         	 insert insertperm;
         }*/
    }
    global void execute(SchedulableContext sc)
         {
              //employeePRID= new Set<String>();
             //CreateNewUser obj1 = new CreateNewUser(employeePRID);
         }

}