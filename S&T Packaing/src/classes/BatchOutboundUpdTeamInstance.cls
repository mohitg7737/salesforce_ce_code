global class BatchOutboundUpdTeamInstance implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     global Date today=Date.today();              
     public  List<String> DeltaCountry ;
                  
     
                                              
    public map<String,String>Countrymap {get;set;}
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public set<String> Uniqueset {get;set;} 
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
    public String cycle {get;set;}
    global Boolean flag=true;
    global String Country_1;
    public list<String> CountryList;


    global BatchOutboundUpdTeamInstance (String Country1){//set<String> Accountid

        flag=false;
        Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        
        
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode = new map<String,String>();
        Uniqueset = new set<String>(); 
        


        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
                                                                                                                                     
        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

       
       query = 'Select id,AxtriaSalesIQTM__Country__c,Country_Name__c,Name,AxtriaSalesIQTM__Base_Team__c,AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaARSnT__Cycle__r.Name, ' +
        'AxtriaSalesIQTM__IC_EffEndDate__c,CreatedDate,AxtriaSalesIQTM__Team_Instance_Code__c,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Team__r.Marketing_Code__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c ' +
        'FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaARSnT__Country_Name__c IN :CountryList ' ;

        System.debug('query'+ query);

    }


     global BatchOutboundUpdTeamInstance (){//set<String> Accountid
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        
        for(Custom_Scheduler__c cs:mktList){
            if(cs.Status__c==true && cs.Schedule_date__c!=null){
                if(cs.Schedule_date__c>today.addDays(1)){
                    mkt.add(cs.Marketing_Code__c);
                }else{
                    //update Custom scheduler record
                   cs.Status__c = False;
                   lsCustomSchedulerUpdate.add(cs);
                }
            }
        }
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode = new map<String,String>();
        Uniqueset = new set<String>(); 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
         if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                    cycle = t1.Cycle__r.Name;
            }
            
        }
         //String cycle=cycleList.get(0).Name;
         //cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Team Instance' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);


        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
                                                                                                                                     
        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'Team Instance';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
         if(cycle!=null && cycle!='')
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
       
        recordsProcessed =0;

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceList.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>'+DeltaCountry);
        
       query = 'Select id,AxtriaSalesIQTM__Country__c,Country_Name__c,Name,AxtriaSalesIQTM__Base_Team__c,AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaARSnT__Cycle__r.Name, ' +
        'AxtriaSalesIQTM__IC_EffEndDate__c,CreatedDate,AxtriaSalesIQTM__Team_Instance_Code__c,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Team__r.Marketing_Code__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c ' +
        'FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaARSnT__Country_Name__c IN :DeltaCountry ' ;
      
        
        if(lastjobDate!=null){
            query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
         if(mkt.size()>0){
        query=query + ' and AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
                         
                System.debug('query'+ query);
         //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }
 
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchOutboundUpdTeamInstance());                                                            
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Team_Instance__c> records){
        // process each batch of records
        //-------------------------------- Commented due to Purging activity------------------------------------------------------

       
        String code = ' ';
        /*List<SIQ_Sales_Cycle_O__c> salesCycles = new List<SIQ_Sales_Cycle_O__c>();
        for (AxtriaSalesIQTM__Team_Instance__c team : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
             //String key=Countrymap.get(team.Country_Name__c)+'_'+team.AxtriaSalesIQTM__Team_Instance_Code__c ;
             String key=team.Name+'_'+team.AxtriaARSnT__Cycle__r.Name;
           if(!Uniqueset.contains(Key)){
               SIQ_Sales_Cycle_O__c salesCycle=new SIQ_Sales_Cycle_O__c();
                  // String key=Countrymap.get(team.Country_Name__c)+'_'+team.Name;
                   //salesCycle.SIQ_Country_Code__c=Countrymap.get(team.Country_Name__c);
                    salesCycle.SIQ_Country_Code__c=team.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    code = team.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    salesCycle.SIQ_External_Team_Id__c=team.id;
                    salesCycle.SIQ_Team_Instance__c=team.Name;
                //  salesCycle.SIQ_Event__c=
                 // salesCycle.SIQ_Marketing_Code__c=Countrymap.get(team.Country_Name__c);
                   // salesCycle.SIQ_Marketing_Code__c= team.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
                    //Added by Ayushi
                    if(mapVeeva2Mktcode.get(code) != null){
                        salesCycle.SIQ_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                    }
                    else{
                        salesCycle.SIQ_Marketing_Code__c = 'MC code does not exist';
                    }
                    //Till here..
                    salesCycle.SIQ_Parent_Team__c=team.AxtriaSalesIQTM__Base_Team__c;
                    salesCycle.Name=team.Name;
                    salesCycle.SIQ_Sales_Cycle__c=team.AxtriaARSnT__Cycle__r.Name;
                    salesCycle.SIQ_Team_Name__c=team.AxtriaSalesIQTM__Team__r.name;
                    salesCycle.SIQ_Effective_Start_Date__c=team.AxtriaSalesIQTM__IC_EffstartDate__c;
                    salesCycle.SIQ_Effective_End_Date__c=team.AxtriaSalesIQTM__IC_EffEndDate__c;
                    salesCycle.SIQ_Created_Date__c=team.CreatedDate;
                    salesCycle.SIQ_Updated_Date__c=team.LastModifiedDate;
                    salesCycle.SIQ_Team_Type__c=team.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c;
                    salesCycle.Unique_Id__c=key;                            
                salesCycles.add(salesCycle);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                   Uniqueset.add(key);
                //comments
            }*/
            
        //}
     
        //upsert salesCycles Unique_Id__c;
       

      
        
    }    
    global void finish(Database.BatchableContext bc){

        if(flag)
        {
          // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                 sjob.Object_Name__c = 'Team Instance';
                //sjob.Changes__c        
                                         
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
         //Database.ExecuteBatch(new BatchOutBoundEmployee(),200); 
         Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
              }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Team_Instance__c> teamInsList=[Select Id FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update teamInsList;
          }
        }

    }   
}