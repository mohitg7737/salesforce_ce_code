public class TestDataFactoryAZ{
    
    public static AxtriaSalesIQTM__Business_Unit__c bu ;
    public static List<AxtriaSalesIQTM__Team__c> teamList ;
    public static List<AxtriaSalesIQTM__Team_Instance__c> teamIns ;
    public TestDataFactoryAZ(){
    
    }
    
    
    public static List<AxtriaSalesIQTM__Team__c> createTeam(){
        teamList = new List<AxtriaSalesIQTM__Team__c>();
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.name = 'CEOS';
        team.AxtriaSalesIQTM__Type__c = 'Base';
        team.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 1, 1);
        team.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
        teamList.add(team);
        
        AxtriaSalesIQTM__Team__c team2 = new AxtriaSalesIQTM__Team__c();
        team2.name = 'KAOM';
        team2.AxtriaSalesIQTM__Type__c = 'Base';
        team.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 1, 1);
        team.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
        teamList.add(team2);
        
        AxtriaSalesIQTM__Team__c team3 = new AxtriaSalesIQTM__Team__c();
        team3.name = 'CAEOS';
        team3.AxtriaSalesIQTM__Type__c = 'Base';
        team.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 1, 1);
        team.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
        teamList.add(team3);
        insert teamList ;
        return teamList ;
    }
    
    public static List<AxtriaSalesIQTM__Team_Instance__c> createTeamInstance(List<AxtriaSalesIQTM__Team__c> teams){
        teamIns = new List<AxtriaSalesIQTM__Team_Instance__c>();
        AxtriaSalesIQTM__Team_Instance__c ins1 = new AxtriaSalesIQTM__Team_Instance__c();
        ins1.name = 'CEO-Q4-2017' ;
        ins1.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        ins1.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
        ins1.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        ins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.newinstance(2017, 1, 1);
        ins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.newinstance(4000, 12, 31);
        ins1.AxtriaSalesIQTM__Team__c = teams[0].id;
        teamIns.add(ins1);
        
        AxtriaSalesIQTM__Team_Instance__c ins2 = new AxtriaSalesIQTM__Team_Instance__c();
        ins2.name = 'KAOM-Q4-2017' ;
        ins2.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        ins2.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
        ins2.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        ins2.AxtriaSalesIQTM__IC_EffstartDate__c = date.newinstance(2017, 1, 1);
        ins2.AxtriaSalesIQTM__IC_EffEndDate__c = date.newinstance(4000, 12, 31);
        ins2.AxtriaSalesIQTM__Team__c = teams[1].id;
        teamIns.add(ins2);
        
        AxtriaSalesIQTM__Team_Instance__c ins3 = new AxtriaSalesIQTM__Team_Instance__c();
        ins3.name = 'CAEO-Q4-2017' ;
        ins3.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        ins3.AxtriaSalesIQTM__Alignment_Type__c = 'ZIP';
        ins3.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        ins3.AxtriaSalesIQTM__IC_EffstartDate__c = date.newinstance(2017, 1, 1);
        ins3.AxtriaSalesIQTM__IC_EffEndDate__c = date.newinstance(4000, 12, 31);
        ins3.AxtriaSalesIQTM__Team__c = teams[2].id;
        teamIns.add(ins3);
        
        insert teamIns ;
        return teamIns ;
    }
    
    public static List<AxtriaSalesIQTM__Employee__c> createEmployees(String FirstName,String LastName,String TerrAlign,String empStatus,string empcode,Date HireDate, Integer recordNo){
        List<AxtriaSalesIQTM__Employee__c> empList = new List<AxtriaSalesIQTM__Employee__c>();
        For(integer i=0;i<recordNo;i++){
            AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
            emp.AxtriaSalesIQTM__FirstName__c = FirstName + i ;
            emp.AxtriaSalesIQTM__Last_Name__c = LastName + i ;
            emp.Employee_Status__c = empStatus;
            emp.Employment_Status__c=empStatus;
            emp.AxtriaSalesIQTM__HR_Status__c = 'Active';
            //emp.Territory_Alignment__c = TerrAlign;
            emp.AxtriaSalesIQTM__Original_Hire_Date__c = HireDate ;
            emp.AxtriaSalesIQTM__Employee_ID__c = empcode +i;
            /*if(i==3){
                emp.AxtriaSalesIQTM__HR_Status__c = 'Inactive';
                emp.AxtriaSalesIQTM__HR_Termination_Date__c = system.Today();
            }
            emp.AxtriaSalesIQTM__Employee_ID__c = empcode +i;
            if(i==4){
                emp.AxtriaSalesIQTM__HR_Status__c = 'Terminated';
                emp.AxtriaSalesIQTM__HR_Termination_Date__c = system.Today();
            }*/
            empList.add(emp);
        }
        return empList ;
    }
    
    public static List<AxtriaSalesIQTM__Position__c> createPositions(String name,integer RecordNo,string teamname,List<AxtriaSalesIQTM__Team_Instance__c> teamInst){
        List<AxtriaSalesIQTM__Position__c> posList = new List<AxtriaSalesIQTM__Position__c>();
        For(integer i=0;i<recordNo;i++){
            AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
            pos.name = name + i;
            if(teamname == 'CAEOS'){
                pos.AxtriaSalesIQTM__Team_iD__c = teamInst[2].AxtriaSalesIQTM__Team__c ;
                pos.AxtriaSalesIQTM__Team_Instance__c = teamInst[2].id;
            }
            if(teamname == 'KAOM'){
                pos.AxtriaSalesIQTM__Team_iD__c = teamInst[1].AxtriaSalesIQTM__Team__c ;
                pos.AxtriaSalesIQTM__Team_Instance__c = teamInst[1].id;
            }
            if(teamname == 'CEOS'){
                pos.AxtriaSalesIQTM__Team_iD__c = teamInst[0].AxtriaSalesIQTM__Team__c ;
                pos.AxtriaSalesIQTM__Team_Instance__c = teamInst[0].id;
            }
            posList.add(pos);
        }
        return posList ;
    }    
    
    
    public static List<AxtriaSalesIQTM__Position_Employee__c> createPositionEmployees(List<AxtriaSalesIQTM__Employee__c> empList,List<AxtriaSalesIQTM__Position__c> posList){
        List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = new List<AxtriaSalesIQTM__Position_Employee__c> ();
        for(integer i=0;i<empList.size();i++){
            AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
            posEmp.name = 'testPosEmp' + i ;
            posEmp.AxtriaSalesIQTM__Employee__c = empList[i].id;
            posEmp.AxtriaSalesIQTM__Position__c = posList[i].id;
            posEmp.AxtriaSalesIQTM__Status__c = 'Approved';
            posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 4, 15) ;
            posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
            If(empList[i].Employment_Status__c  == 'New Hire'){
                posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 1, 1) ;
                posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(2017, 4, 20);
            }
            else{
                posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Secondary';
            }
            
            
            posEmpList.add(posEmp);
        }
        return posEmpList;
    }
    //Used for Alignment Module
      public static List<AxtriaSalesIQTM__Change_Request_Type__c> createChangeRequesttype(String TypeName, Integer recordNo){      
       
        List<AxtriaSalesIQTM__Change_Request_Type__c> CR_TypeList= new List<AxtriaSalesIQTM__Change_Request_Type__c>() ;
        For(integer i=0;i<recordNo;i++){
        AxtriaSalesIQTM__Change_Request_Type__c crtype= new AxtriaSalesIQTM__Change_Request_Type__c();
            crtype.AxtriaSalesIQTM__CR_Type_Name__c= TypeName;
            CR_TypeList.add(crtype);
        }
        
        return CR_TypeList;
        
   }
    
    public static List<AxtriaSalesIQTM__Change_Request__c> createChangeRequest(String RequestTypeChange,List<AxtriaSalesIQTM__Position__c> Source, Integer recordNo){      
        List<AxtriaSalesIQTM__Change_Request__c> CR_List= new List<AxtriaSalesIQTM__Change_Request__c>() ;
        
        For(integer i=0; i < recordNo; i++){
            AxtriaSalesIQTM__Change_Request__c cr= new AxtriaSalesIQTM__Change_Request__c();
            cr.AxtriaSalesIQTM__Request_Type_Change__c = RequestTypeChange;
            cr.AxtriaSalesIQTM__Source_Position__c = Source[i].id;
            cr.AxtriaSalesIQTM__Contiguous__c=False;
            cr.AxtriaSalesIQTM__hasChild__c=False;
            cr.AxtriaSalesIQTM__isExternal__c=False;
            cr.AxtriaSalesIQTM__is_Active__c= true;
            cr.AxtriaSalesIQTM__Is_Auto_Approved__c= true;
            cr.AxtriaSalesIQTM__Is_Overlay_Impacted__c=False;
            cr.AxtriaSalesIQTM__OutsideDistrict__c=False;
            cr.AxtriaSalesIQTM__Status__c='Pending Approval';
            cr.AxtriaSalesIQTM__Approver1__c=Userinfo.getUserid();
            cr.AxtriaSalesIQTM__Change_Effective_Date__c=Date.today();
            CR_List.add(cr);
        }
        
        return CR_List;        
   }
        
    public static List<AxtriaSalesIQTM__CR_Employee_Assignment__c>  createEmpAssignment(List<AxtriaSalesIQTM__Change_Request__c> crList,List<AxtriaSalesIQTM__Employee__c> empList,List<AxtriaSalesIQTM__Position__c> posList, integer recordNo){
       List<AxtriaSalesIQTM__CR_Employee_Assignment__c> empAssgnList = new List<AxtriaSalesIQTM__CR_Employee_Assignment__c>() ;
       For(integer i=0;i<recordNo;i++){
           AxtriaSalesIQTM__CR_Employee_Assignment__c empassgn = new AxtriaSalesIQTM__CR_Employee_Assignment__c();
           empassgn.AxtriaSalesIQTM__Employee_ID__c = empList[i].id;
           empassgn.AxtriaSalesIQTM__Position_ID__c = posList[i].id;
           empassgn.AxtriaSalesIQTM__Change_Request_ID__c = crList[i].id;
           empassgn.AxtriaSalesIQTM__After_Effective_Start_Date__c = date.today();
           empassgn.AxtriaSalesIQTM__After_Effective_End_Date__c = date.today() + 10 ;
           empAssgnList.add(empassgn);
       } 
       return  empAssgnList ;
    }
    
    
    public static List<CR_Employee_Feed__c> createEmpFeed(string name,string eventName, List<AxtriaSalesIQTM__Employee__c> emplist, List<AxtriaSalesIQTM__Position__c> poslist, integer recordno){
        List<CR_Employee_Feed__c> empFeedList = new List<CR_Employee_Feed__c>();
        for(integer i=0; i< recordno; i++){
            CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
            cr.name = name + i;
            cr.Employee__c = emplist[i].id ;
            cr.Position__c = poslist[i].id ;
            cr.Event_Name__c = eventName ;
            empFeedList.add(cr);
        }
        
        return empFeedList ;        
    }
    
    public static list<SIQ_Employee_Master__c> createEmpStaging(String FirstName,String LastName,String TerrAlign,String empStatus,string empcode,Date HireDate, Integer recordNo){
        list<SIQ_Employee_Master__c> empStagingList = new list<SIQ_Employee_Master__c>();
        for(integer i=0;i<recordNo;i++){
            SIQ_Employee_Master__c emp = new SIQ_Employee_Master__c();
            emp.SIQ_First_Name__c = FirstName + i ;
            emp.SIQ_Last_Name__c = LastName + i ;
            emp.SIQ_HR_Status__c = empStatus;
            emp.SIQ_Hire_Date__c = HireDate ;
            emp.SIQ_PRID__c = empcode +i;
            emp.SIQ_External_Employee_Id__c = empcode +i;
            emp.SIQ_Marketing_Code__c = 'ES';
            emp.SIQ_Country_Code__c = 'ES';
            emp.SIQ_Address__c = 'Test';
            emp.SIQ_Country__c='ES';
            emp.SIQ_Job_Title__c='emp';
            emp.SIQ_Status__c='A';
            
            
            empStagingList.add(emp); 
        }
        return empStagingList;
    } 
    
    public static list<AxtriaSalesIQTM__Employee_Status__c> createEmpStatus(list<AxtriaSalesIQTM__Employee__c> employeeList, string status){
        list<AxtriaSalesIQTM__Employee_Status__c> empStatusList = new list<AxtriaSalesIQTM__Employee_Status__c>();
        for(integer i=0;i<employeeList.size();i++){
            AxtriaSalesIQTM__Employee_Status__c empStatus = new AxtriaSalesIQTM__Employee_Status__c();
            empStatus.AxtriaSalesIQTM__Employee__c = employeeList[i].id;
            empStatus.AxtriaSalesIQTM__Employee_Status__c = status;
            empStatus.AxtriaSalesIQTM__Effective_Start_Date__c = date.newInstance(2017, 1,1);
            empStatus.AxtriaSalesIQTM__effective_end_date__c = date.newInstance(4000, 12, 31);
            empStatusList.add(empStatus);
        }
        
        return empStatusList;
    }

  
    public static List<AxtriaSalesIQTM__Position_Employee__c> createEmployeeAssignmentStatus(list<AxtriaSalesIQTM__Employee__c> employeeList){
        List<AxtriaSalesIQTM__Position_Employee__c> empAssignmentList = new List<AxtriaSalesIQTM__Position_Employee__c>();
        For(integer i=0;i<employeeList.size();i++){
            AxtriaSalesIQTM__Position_Employee__c emp = new AxtriaSalesIQTM__Position_Employee__c();
        //    emp.Employee__c = employeeList[i].id;
            emp.AxtriaSalesIQTM__Effective_End_Date__c = date.newInstance(4000, 12, 31);
            emp.AxtriaSalesIQTM__Effective_Start_Date__c = date.newInstance(2000, 1,1);
            /*emp.Assignment_Status__c = 'active';
            emp.Assignment_Type__c = 'primary';
            emp.Global_Employee_ID__c = '123';
            emp.RMS_Territory_Number__c = '456';
            emp.RMS_Territory_Name__c = 'abc';*/
            empAssignmentList.add(emp);
        }
        return empAssignmentList ;
    }
    
}