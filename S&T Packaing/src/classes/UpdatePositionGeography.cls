global class UpdatePositionGeography implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
    public map<String,String>Countrymap {get;set;}
    
    global UpdatePositionGeography(){
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Geography__c> records){
    }    
    global void finish(Database.BatchableContext bc){
        
    }  
}