@isTest
public class update_PositionGeographyTest {
    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'PositionGeographyTrigger')};
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'OverlappingWorkspace';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = countr.Id;
        insert type;
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos;
        AxtriaSalesIQTM__Position_Geography__c posgeo = new AxtriaSalesIQTM__Position_Geography__c();
        posgeo.name = 'X';
        //posgeo.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
        posgeo.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posgeo.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posgeo.AxtriaSalesIQTM__Team_Instance__c=teamins.id;
        posgeo.AxtriaSalesIQTM__Geography__c = geo.Id;
        posgeo.AxtriaSalesIQTM__Position__c = pos.id;
        insert posgeo;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = true;
        insert pos1;
        AxtriaSalesIQTM__Position_Geography__c posgeo1 = new AxtriaSalesIQTM__Position_Geography__c();
        posgeo1.name = 'Y';
        //posgeo.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
        posgeo1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posgeo1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posgeo1.AxtriaSalesIQTM__Team_Instance__c=teamins.id;
        posgeo1.AxtriaSalesIQTM__Geography__c = geo.Id;
        posgeo1.AxtriaSalesIQTM__Position__c = pos1.id;
        insert posgeo1;
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posteamins.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posteamins.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        posteamins.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posteamins.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posteamins;
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins1 = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posteamins1.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posteamins1.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        posteamins1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posteamins1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posteamins1;

        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        temp_Obj__c zt = new temp_Obj__c();
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Position__c = pos.id;
        zt.Geography__c=geo.id;
        zt.Zip_Name__c = 'X';
        zt.Team_Name__c = team.Name;
        zt.Status__c ='New';
        zt.Team_Instance__c = teamins.id;
        zt.Team__c = team.id;
        zt.Event__c = 'Insert';
        zt.Object__c ='Zip_Terr';
        zt.Change_Request__c=cr.Id;
        zt.isError__c = false;
        zt.SalesIQ_Error_Message__c= 'test';
        zt.Error_message__c ='test' ;
        insert zt;
         temp_Obj__c z = new temp_Obj__c();
        z.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        z.Position__c = pos1.id;
        z.Geography__c=geo.id;
        z.Zip_Name__c = 'X';
        z.Team_Name__c = team.Name;
        z.Status__c ='New';
        z.Team_Instance__c = teamins.id;
        z.Team__c = team.id;
        z.Event__c = 'Insert';
        z.Object__c ='Zip_Terr';
        z.Change_Request__c=cr.Id;
        z.isError__c = false;
        z.SalesIQ_Error_Message__c= 'test';
        z.Error_message__c ='test' ;
        insert z;

        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdatePosGeography obj=new UpdatePosGeography(team.Id,teamins.id,cr.Id);
            //obj.query='SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c, Position__r.AxtriaSalesIQTM__inactive__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}