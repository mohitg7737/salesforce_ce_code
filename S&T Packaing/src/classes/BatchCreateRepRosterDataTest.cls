/**********************************************************************************************
@author     : Himanshu Tariyal (A0994)
@date       : 2nd July'2020
@description: Test Class for BatchCreateRepRosterData class
Revison(s)  : v1.0
**********************************************************************************************/
@isTest
public class BatchCreateRepRosterDataTest 
{
    static TestMethod void firstTest() 
    {
    	String className = 'BatchCreateRepRosterDataTest';
        User loggedInUser = new User(id=UserInfo.getUserId());

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgmas);
        country.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(country,className);

    	AxtriaSalesIQTM__Employee__c emp1 = TestDataFactory.createEmployee('FirstName','LastName');
    	emp1.AddressLine1__c = 'Address Line1';
    	emp1.AddressLine2__c = 'Address Line2';
    	emp1.AddressCity__c = 'Address City';
    	emp1.AxtriaSalesIQTM__Country_Name__c = country.Id;
    	emp1.AxtriaSalesIQTM__Fax_Number__c = '1111111111';
    	emp1.HomePhone__c = '1111111111';
    	emp1.PersonalCell__c = '1111111111';
    	emp1.AddressStateCode__c = 'CO';
    	emp1.AddressPostalCode__c = '99711';
    	SnTDMLSecurityUtil.insertRecords(emp1,className);

    	AxtriaSalesIQTM__Employee__c emp2 = TestDataFactory.createEmployee('FirstName2','LastName2');
    	emp2.AddressLine1__c = 'Address Line1';
    	emp2.AddressLine2__c = 'Address Line2';
    	emp2.AddressCity__c = 'Address City';
    	emp2.AxtriaSalesIQTM__Country_Name__c = country.Id;
    	emp2.AxtriaSalesIQTM__Fax_Number__c = '1111111111';
    	emp2.HomePhone__c = '1111111111';
    	emp2.PersonalCell__c = '1111111111';
    	emp2.AddressStateCode__c = 'CO';
    	emp2.AddressPostalCode__c = '99711';
    	SnTDMLSecurityUtil.insertRecords(emp2,className);

    	Staging_Rep_Roster__c repRoster = new Staging_Rep_Roster__c();
    	repRoster.External_ID__c = emp1.AxtriaSalesIQTM__Employee_ID__c;
    	SnTDMLSecurityUtil.insertRecords(repRoster,className);

    	Test.startTest();

        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

            List<String> RULEPARAMETER_READ_FIELD = new List<String>{namespace+'External_ID__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Staging_Rep_Roster__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            BatchCreateRepRosterData batchCall = new BatchCreateRepRosterData();
            Database.executeBatch(batchCall);
        }

        Test.stopTest();
    }
}