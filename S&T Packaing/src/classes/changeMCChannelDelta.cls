global class changeMCChannelDelta implements Database.Batchable<sObject> 
{
     public String query;
     public Boolean flag = true;
     public Datetime lmd;
     List<String> allChannels;
    String teamInstanceSelected;
    List<String> allTeamInstances;
    public Boolean chaining = false;
    Set<String> activityLogIDSet;
    
    global changeMCChannelDelta(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        Lmd=lastjobDate;
        query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c = :teamInstanceSelected';
         teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    global changeMCChannelDelta(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        Lmd=lastjobDate;
        query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        allChannels = allChannelsTemp;
    }
    global changeMCChannelDelta(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp,Set<String> activityLogSet) 
    {
        Lmd=lastjobDate;
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        allChannels = allChannelsTemp;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

     global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scopePacpProRecs)
    {
        system.debug('++query'+query);
        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }       
        update scopePacpProRecs;
    }

    global void finish(Database.BatchableContext BC)
    {        
        //lmd=Date.Today();
        system.debug('++lmd'+lmd);
        if(activityLogIDSet != null)
        {
          //BatchDeltaMCChannelStatus u2 = new BatchDeltaMCChannelStatus(lmd,allTeamInstances,allChannels,activityLogIDSet);
          //database.executeBatch(u2,2000);
        }
        else

        {
            BatchDeltaMCChannelStatus u2 = new BatchDeltaMCChannelStatus(lmd,allTeamInstances,allChannels);
            database.executeBatch(u2,2000);
        }
    }
}