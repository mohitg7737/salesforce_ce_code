global with sharing class ATL_Delete_Recs implements Database.Batchable<sObject>,Schedulable {
    
    public String query;
    public List<String> allCountries;
    public List<String> teaminstancelistis;
    List<Staging_ATL__c> stagList;
    
    public Set<String> activityLogIDSet;
    public List<Scheduler_Log__c> activityLogList = new List<Scheduler_Log__c>();

    public Boolean chaining = false;
    global ATL_Delete_Recs(List<String> allCountries1) {

        allCountries = new List<String>(allCountries1);
        SnTDMLSecurityUtil.printDebugMessage('+++allCountries'+allCountries);
        this.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c where Status__c = \'Updated\' WITH SECURITY_ENFORCED';
    
    }

    global ATL_Delete_Recs(List<String> allCountries1, Boolean chain) {

        chaining = chain;
        allCountries = new List<String>(allCountries1);
        SnTDMLSecurityUtil.printDebugMessage('+++allCountries'+allCountries);
        this.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c where Status__c = \'Updated\' WITH SECURITY_ENFORCED';
    
    }

    global ATL_Delete_Recs(List<String> allCountries1, Boolean chain, Set<String> activitylogSet) {}

    global ATL_Delete_Recs(List<String> allCountries1, Integer specificCountry) 
    {

        allCountries = new List<String>(allCountries1);
        SnTDMLSecurityUtil.printDebugMessage('+++allCountries'+allCountries);
        this.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c where Status__c = \'Updated\' and Country_Lookup__c in :allCountries WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) 
    {
        stagList= new List<Staging_ATL__c>();
    
        for(Staging_ATL__c sta : scope)
        {
            sta.Status__c = '';
            SnTDMLSecurityUtil.printDebugMessage('sta'+sta.Territory__c);
            
            if(sta.Territory_Old__c!= null)
            {
                SnTDMLSecurityUtil.printDebugMessage('inside if');
                sta.Territory_Old__c=null;
            }

            sta.Territory_Old__c=  sta.Territory__c;
            SnTDMLSecurityUtil.printDebugMessage('sta'+sta.Territory_Old__c);
        }
        
        //update scope;
        SnTDMLSecurityUtil.updateRecords(scope, 'ATL_Delete_Recs');

    }

    global void finish(Database.BatchableContext BC) 
    {
        list<string> teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getDeltaTeamInstances();
        //commented since BatchPositionAccountStatus removed
        /*if(teaminstancelistis.size() > 0 )
        {
            Set<String> allTI = new Set<String>(teaminstancelistis);
            Database.executeBatch(new BatchPositionAccountStatus(allTI, chaining),2000);    
        }*/


        //Added by Ayushi
        list<string>countryList = new list<string>();
        set<string>countrySet = new set<string>();

        set<string>smallCountrySet = new set<string>();
        set<string>bigCountrySet = new set<string>();

        list<string>smallCountryList = new list<string>();
        list<string>bigCountryList = new list<string>();   
        
        list<AxtriaSalesIQTM__Team_Instance__c>teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Small_Country__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and  (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published' ) ];

        if(teamlist!=null && teamlist.size()>0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti:teamlist){
                countrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);

                if(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Small_Country__c == true)
                {
                    smallCountrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
                }
                else
                {
                    bigCountrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
                }
            }
            countryList.addall(countrySet);
            smallCountryList.addall(smallCountrySet);
            bigCountryList.addall(bigCountrySet);
        }

        SnTDMLSecurityUtil.printDebugMessage('smallCountryList' +smallCountryList );
        SnTDMLSecurityUtil.printDebugMessage('bigCountryList' +bigCountryList );



        AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
        exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('ATLDeltaJob')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('ATLDeltaJob'):null;

        SnTDMLSecurityUtil.printDebugMessage('execute trigger' +exeTrigger );
        //public static Boolean executeTrigger; 
        if(exeTrigger != null)
        {
            if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
            {
                activityLogIDSet = new Set<String>();

                for(String country : countryList)
                {
                    Scheduler_Log__c activityLogrec = new Scheduler_Log__c();
                    activityLogrec.Job_Status__c = 'Failed';
                    activityLogrec.Job_Type__c = 'ATL Delta Job';
                    activityLogrec.Country__c = country;
                    activityLogrec.Job_Name__c = 'ATL Delta Job';
                    activityLogList.add(activityLogrec);

                }

                SnTDMLSecurityUtil.printDebugMessage('activityLogList.size() :::::::::' +activityLogList.size());
                SnTDMLSecurityUtil.printDebugMessage('countryList.size() :::::::::' +countryList.size());

                if(activityLogList.size() > 0){
                    //insert activityLogList;
                    SnTDMLSecurityUtil.insertRecords(activityLogList, 'ATL_Delete_Recs');
                }

                SnTDMLSecurityUtil.printDebugMessage('activityLogList after insert :::::::::' +activityLogList);

                for(Scheduler_Log__c activityID : activityLogList)
                {
                    activityLogIDSet.add(activityID.Id);
                }
                SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet.size() :::::::::' +activityLogIDSet.size());
                SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet :::::::::' +activityLogIDSet);
            }
        }
        //commented since ATL_TransformationBatch removed
        /*if(activityLogIDSet != null)
        {
            if(smallCountryList.size() > 0)
            {
                SnTDMLSecurityUtil.printDebugMessage('smallCountryList.size() :::::::::' +smallCountryList.size());
                SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet.size() :::::::::' +activityLogIDSet.size());
                database.executeBatch(new ATL_TransformationBatch(smallCountryList, true, activityLogIDSet),2000);
            }

            if(bigCountryList.size() > 0)
            {
                for(String countryrec : bigCountryList)
                {
                    SnTDMLSecurityUtil.printDebugMessage('bigCountryList.size() :::::::::' +bigCountryList.size());
                    SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet.size() in bigCountryList :::::::::' +activityLogIDSet.size());
                    List<String> tempCountry = new List<String>();
                    tempCountry.add(countryrec);
                    SnTDMLSecurityUtil.printDebugMessage('tempCountry :::::::::' +tempCountry);
                    database.executeBatch(new ATL_TransformationBatch(tempCountry, true, activityLogIDSet),2000);
                }
            }

        }
        else
        {
            if(smallCountryList.size() > 0)
                database.executeBatch(new ATL_TransformationBatch(smallCountryList, true),2000);

            if(bigCountryList.size() > 0)
            {
                for(String countryrec : bigCountryList)
                {
                    List<String> tempCountry = new List<String>();
                    tempCountry.add(countryrec);
                    database.executeBatch(new ATL_TransformationBatch(tempCountry, true),2000);
                }
            }
        }
        */
        //comment ends ATL_TransformationBatch
        /*if(activityLogIDSet != null)
        {
            Database.executeBatch(new ATL_TransformationBatch(allCountries, true,activityLogIDSet),2000);
        }
        else
        {
            Database.executeBatch(new ATL_TransformationBatch(allCountries, true),2000);
        }*/

        /*teaminstancelistis = StaticTeaminstanceList.getFulloadCountries();
        SnTDMLSecurityUtil.printDebugMessage('+++teaminstancelistis'+teaminstancelistis);

        if(teaminstancelistis.size() > 0)
        {
            SnTDMLSecurityUtil.printDebugMessage('+++allCountries'+allCountries);
            Database.executeBatch(new stagingATLCopyTerritory(allCountries),2000);    
        }        */
    }
    global void execute(System.SchedulableContext SC)
    {

        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getAllCountries();
        database.executeBatch(new ATL_Delete_Recs(teaminstancelistis));
    } 
}