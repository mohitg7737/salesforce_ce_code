@isTest
global with sharing class MockHttpResponseGenerate implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {

        // Optionally, only send a mock response for a specific endpoint

        // and method.
        //System.assertEquals('http://13.66.200.148/ComparativeAnalysis/initParams', req.getEndpoint());

        //System.assertEquals('POST', req.getMethod());

         

        // Create a fake response
        String myobj = '{"Data" :{"key1":"1","key2":"2"}}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(myobj);
        res.setStatusCode(200);
        return res;

    }
}