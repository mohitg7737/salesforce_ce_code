global class Integration_Batch_GAS_History_Data implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public List<String> teamInstances;

    public Integer totalRecs;
    public Integer errorRecsCount;

     global Integration_Batch_GAS_History_Data() {

        this.teamInstances = teamInstances;
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c where (Deployment_Status__c = \'New\' OR Deployment_Status__c = null) OR (Deployment_Status__c = \'Rejected\' AND Run_Count__c <2)';

        totalRecs = 0;
        errorRecsCount = 0;

    }

    global Integration_Batch_GAS_History_Data(List<String> teamInstances) {

        this.teamInstances = teamInstances;
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c where (Deployment_Status__c = \'New\' OR Deployment_Status__c = null) OR (Deployment_Status__c = \'Rejected\' AND Run_Count__c <2)';

        totalRecs = 0;
        errorRecsCount = 0;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_GAS_History__c> scope) {
        
        
        Set<String> allAccounts = new Set<String>();
        Set<String> allPositionsSet = new Set<String>();
        Map<String, String> atlMap = new Map<String,String>();
        Map<String,String> veevaToSalesIQID = new Map<String,String>();
        Map<String,String> posIDMapping = new Map<String,String>();
        Map<String,String> posToTeaInstanceMap = new Map<String,String>();

        List<AxtriaSalesIQTM__Position_Account__c> newPositionAccounts = new List<AxtriaSalesIQTM__Position_Account__c>();


        for(SIQ_GAS_History__c sgh : scope)
        {
            allAccounts.add(sgh.SIQ_Account__c);
            allPositionsSet.add(sgh.SIQ_Added_Territory__c);
            totalRecs ++;
        }  

        List<Staging_ATL__c> allStagingRecs = [select Axtria_Account_ID__c, Account__c, Territory__c from Staging_ATL__c where Account__c in :allAccounts];
        List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :allPositionsSet and AxtriaSalesIQTM__Team_Instance__c in :teamInstances];

        for(AxtriaSalesIQTM__Position__c pos : allPositions)
        {
            posIDMapping.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.ID);
            posToTeaInstanceMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.AxtriaSalesIQTM__Team_Instance__c);
        }

        for(Staging_ATL__c sa : allStagingRecs)
        {
            veevaToSalesIQID.put(sa.Account__c, sa.Axtria_Account_ID__c);
            atlMap.put(sa.Account__c, sa.Territory__c);
        }

        //NEED TO ADD RECORDS TO CP AFTER Global exclusion rule
        List<Gas_History_Error_Records__c> errorRecs = new List<Gas_History_Error_Records__c>();

        for(SIQ_GAS_History__c sgh : scope)
        {
            //if(atlMap.containsKey(sgh.SIQ_Account__c))
            //{
                List<String> territoriesList = new List<String>();

                if(atlMap.containsKey(sgh.SIQ_Account__c))
                {
                    String terrs = atlMAp.get(sgh.SIQ_Account__c);
                
                    if(terrs != null)
                    {
                        territoriesList = terrs.split(';');
                    }                    
                }

               
                if(!territoriesList.contains(sgh.SIQ_Added_Territory__c))
                {
                    AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
                    posAcc.AxtriaSalesIQTM__Team_Instance__c = posToTeaInstanceMap.get(sgh.SIQ_Added_Territory__c);
                    sgh.Run_Count__c = sgh.Run_Count__c + 1;

                    

                    if(veevaToSalesIQID.containsKey(sgh.SIQ_Account__c))
                    {
                        posAcc.AxtriaSalesIQTM__Account__c = veevaToSalesIQID.get(sgh.SIQ_Account__c);    
                    }
                    else
                    {
                        errorRecs.add(new Gas_History_Error_Records__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account Not Found'));
                        //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                        sgh.Deployment_Status__c = 'Rejected';
                        errorRecsCount++;
                        continue;
                    }

                    if(posIDMapping.containsKey(sgh.SIQ_Added_Territory__c))
                        posAcc.AxtriaSalesIQTM__Position__c = posIDMapping.get(sgh.SIQ_Added_Territory__c);
                    else
                    {
                        errorRecs.add(new Gas_History_Error_Records__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Territory Not Found'));
                        //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                        sgh.Deployment_Status__c = 'Rejected';
                        errorRecsCount++;
                        continue;
                    }

                    posAcc.AxtriaSalesIQTM__Effective_Start_Date__c =  System.today();
                    posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2099,12,31);
                    newPositionAccounts.add(posAcc);
                    sgh.Deployment_Status__c = 'Processed';
                }
                else
                {
                    errorRecs.add(new Gas_History_Error_Records__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account already assigned to territory'));
                    errorRecsCount++;
                    //sgh.Deployment_Status__c = 'Rejected';
                    //sgh.Deployment_Status__c = 'Not Required';
                }

           /* }
            else
            {
                errorRecs.add(new Gas_History_Error_Records__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account Not Found'));
                //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                sgh.Run_Count__c = sgh.Run_Count__c + 1;
                errorRecsCount++;
                continue;
            }*/

        }



        if(errorRecs.size()>0)
            insert errorRecs;

        insert newPositionAccounts;

        update scope;
    }

    global void finish(Database.BatchableContext BC) {
         Scheduler_Log__c slog = new Scheduler_Log__c();


        slog.Changes__c = 'GAS Assignment';
        //slog.Created_Date__c = DateTime.now();
        slog.Created_Date2__c = DateTime.now();
        //s1.Cycle__c = 
        slog.Job_Type__c = 'Inbound';
        slog.Job_Name__c = 'GAS Assignment';
        slog.Job_Status__c = 'Executed';
        slog.No_of_Records_Processed__c = totalRecs;
        slog.Total_Errors__c = errorRecsCount;

        insert slog;
    }
}