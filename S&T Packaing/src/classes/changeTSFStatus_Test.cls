@isTest
private class changeTSFStatus_Test {

    static testMethod void testMethod2() {
       User loggedInUser = new User(id=UserInfo.getUserId());
       
       Account acc= TestDataFactory.createAccount();
       insert acc;
       AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
       insert orgmas;
       AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
       countr.Cluster_Information__c = 'No Cluster';
       countr.Small_Country__c =true;
       insert countr;
       
       AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
       insert team;
       AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
       insert teamins1;
       
       AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
       workspace.AxtriaSalesIQTM__Country__c = countr.id;
       insert workspace;
       
       
       AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
       scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
       insert scen;
       
       AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
       teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
       teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
       teamins.Team_Selling__c =true;
       teamins.Team_Goals__c='Individual';
       teamins.multichannel__c = true;
       insert teamins;
       
       
       Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
       insert pcc;
       
       Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
       mmc.Team_Instance__c = teamins.id;
       insert mmc;
       
       AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
       pos.AxtriaSalesIQTM__IsMaster__c = true;
       pos.AxtriaSalesIQTM__Hierarchy_Level__c ='1';
       pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
       insert pos;
       
       AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
       insert posAccount;
       Product_Priority__c pPriority = TestDataFactory.productPriority();
       insert pPriority;
       Parent_PACP__c p = TestDataFactory.createParentPACP(teamins,acc,pos);
       insert p;
       SIQ_MC_Cycle_Plan_Target_vod_O__c calsum = TestDataFactory.createSIQ_MC_Cycle_Plan_Target_vod(teamins);
       insert calsum;
       SIQ_MC_Cycle_Plan_Channel_vod_O__c ch = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();
       ch.Team_Instance__c = teamins.id;
       insert ch;
       AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
       positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
       positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
       positionAccountCallPlan.Final_TCF__c = 3.0;
       positionAccountCallPlan.P1__c = 'GIST';
       positionAccountCallPlan.Parent_Pacp__c = p.id;
       insert positionAccountCallPlan;
       Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
       insert v;
       AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
       emp.AxtriaSalesIQTM__Employee_ID__c ='test';
       insert emp;
       AxtriaSalesIQTM__Position_Employee__c posemp = TestDatafactory.createPositionEmployee(pos, emp.id, 'HCO', date.today(), date.today()+1);
       pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
       insert posemp;
       
       
       List<String> teamInstanceSelectedTemp = new List<String> ();
       teamInstanceSelectedTemp.add(teamins.id);        
       SIQ_TSF_vod_O__c sss = new SIQ_TSF_vod_O__c();
       sss.Team_Instance_Lookup__c = teamins.id;
       sss.Status__c ='Updated';
       insert sss;

       
       
       
       
       
       List<String> allTeamInstances = new List<String>();
       allTeamInstances.add(teamins.id);
       insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
       new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
       Test.startTest();
       System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        changeTSFStatus o3=new changeTSFStatus(allTeamInstances);
            //changeTSFStatus.queryString = 'select id from SIQ_TSF_vod_O__c ';
        
        Database.executeBatch(o3);
        changeTSFStatus o32=new changeTSFStatus(allTeamInstances,false);
            //changeTSFStatus.queryString = 'select id from SIQ_TSF_vod_O__c ';
        
        Database.executeBatch(o32);
    }
    Test.stopTest();
}
}