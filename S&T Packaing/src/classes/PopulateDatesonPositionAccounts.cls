global class PopulateDatesonPositionAccounts implements Database.Batchable<sObject> {
    public String query;

    global PopulateDatesonPositionAccounts() {
        query = '';
        query = 'SELECT AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Id,AxtriaSalesIQTM__Account_Alignment_Type__c,CreatedDate FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Assignment_Status__c = null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Active__c = \'Active\' and AxtriaSalesIQTM__Account_Alignment_Type__c != null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {

        System.debug('Query size :::::::::' +scope.size());
        List<AxtriaSalesIQTM__Position_Account__c> posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

        for(AxtriaSalesIQTM__Position_Account__c paRec : scope)
        {
            if(paRec.AxtriaSalesIQTM__Account_Alignment_Type__c == 'Explicit')
            {
                paRec.AxtriaSalesIQTM__Effective_End_Date__c=paRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                paRec.AxtriaSalesIQTM__Effective_Start_Date__c=paRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                posAccList.add(paRec);
            }
            else if(paRec.AxtriaSalesIQTM__Account_Alignment_Type__c == 'Implicit')
            {
               paRec.AxtriaSalesIQTM__Effective_End_Date__c=paRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
               paRec.AxtriaSalesIQTM__Effective_Start_Date__c=Date.valueOf(paRec.CreatedDate);
               posAccList.add(paRec); 
            }
        }

        System.debug('posAccList size :::::::::' +posAccList.size());
        
        if(posAccList.size() > 0)
            update posAccList;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}