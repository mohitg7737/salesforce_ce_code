/*Deprecated due to Object purge activity on Jan 13th, 2020*/
@deprecated
global class BatchOutBoundPosEmp implements Database.Batchable<sObject>,Database.stateful,Schedulable  {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    public String UnAsgnPos='00000';
    public String UnAsgnPos1='0';
    public List<String> posCodeList=new List<String>();
    global DateTime lastjobDate=null;
    public map<String,String>Countrymap {get;set;}
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public set<String> Uniqueset {get;set;} 
    public String cycle {get;set;}
    public Set<String> allMksCode;
    global String Country_1;
    public list<String> CountryList;
    global boolean flag=true;
    public Date InactiveDate;
    public Date TodaysDate;
    public  List<String> DeltaCountry ;

        
    
     
    global BatchOutBoundPosEmp(String Country1) {/*
        Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        flag=false;
        allMksCode = new Set<String>();
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode= new map<String,String>();
        Uniqueset = new set<String>();
        
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        
        query = 'Select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, '+
                'Country_Code__c,CreatedDate ,Employee_ID__c,Territory_ID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.name,LastModifiedDate,AxtriaSalesIQTM__Position__r.Team_Name__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c  from AxtriaSalesIQTM__Position_Employee__c '+
                'where AxtriaSalesIQTM__Position__c!=null AND AxtriaSalesIQTM__Employee__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :CountryList ';
                //Event,SIQ_isActive__c,SIQ_Marketing_Code__c,SIQ_Permanent_Position_Flag__c,SIQ_Salesforce_Name__c,
         
         System.debug('====================query'+ query);*/
    }


    global BatchOutBoundPosEmp() {/*
        
        allMksCode = new Set<String>();
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode= new map<String,String>();
        Uniqueset = new set<String>();
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name,Cycle__r.Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
         if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                allMksCode.add(t1.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                    cycle = t1.Cycle__r.Name;
            }
            
        }
        // String cycle=cycleList.get(0).Name;
         //cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound PosEmp Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);

        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        //Till here..
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound PosEmp Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceList.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>'+DeltaCountry);

        InactiveDate= Date.today().addDays(-30);
        TodaysDate = Date.today();
        
        recordsProcessed =0;
        query = 'Select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, '+
                'Country_Code__c,CreatedDate ,Employee_ID__c,Territory_ID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.name,LastModifiedDate,AxtriaSalesIQTM__Position__r.Team_Name__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c  from AxtriaSalesIQTM__Position_Employee__c '+
                'where AxtriaSalesIQTM__Position__c!=null AND AxtriaSalesIQTM__Employee__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Effective_End_Date__c >: InactiveDate and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :DeltaCountry ';
                //Event,SIQ_isActive__c,SIQ_Marketing_Code__c,SIQ_Permanent_Position_Flag__c,SIQ_Salesforce_Name__c,
         if(lastjobDate!=null){
            query = query + ' and ( LastModifiedDate  >=:  lastjobDate or AxtriaSalesIQTM__Effective_Start_Date__c =: TodaysDate ) '; 
        }
         else{
                query = query + ' and AxtriaSalesIQTM__Effective_Start_Date__c =: TodaysDate ';

         }
                System.debug('query'+ query);*/

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundPosEmp());
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {/*
        List<SIQ_Position_Employee_O__c>PosEmpList = new List<SIQ_Position_Employee_O__c>();
        String code = ' ';
        for(AxtriaSalesIQTM__Position_Employee__c obj : scope){
        // String key=obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+obj.AxtriaSalesIQTM__Employee__r.Name+'_'+obj.AxtriaSalesIQTM__Assignment_Status__c ;
         String key=obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+obj.Employee_ID__c+'_'+obj.AxtriaSalesIQTM__Assignment_Type__c+'_'+obj.AxtriaSalesIQTM__Position__r.Team_Name__c;
          //position employee status
           if(!Uniqueset.contains(Key)){
            SIQ_Position_Employee_O__c pe = new SIQ_Position_Employee_O__c();
            pe.SIQ_ASSIGNMENT_END_DATE__c = obj.AxtriaSalesIQTM__Effective_End_Date__c;
            pe.SIQ_ASSIGNMENT_START_DATE__c = obj.AxtriaSalesIQTM__Effective_Start_Date__c;
            pe.SIQ_ASSIGNMENT_TYPE__c = obj.AxtriaSalesIQTM__Assignment_Type__c;
            //pe.SIQ_Country_Code__c = Countrymap.get(obj.Country_Code__c);
            pe.SIQ_Country_Code__c = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            code = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            pe.SIQ_Created_Date__c   = obj.CreatedDate;
            pe.SIQ_EMP_ID__c = obj.Employee_ID__c;
            //pe.SIQ_POSITION_CODE__c=obj.Territory_ID__c;
            pe.SIQ_POSITION_CODE__c=obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            //pe.SIQ_Team_Instance__c = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.name;
            pe.SIQ_Updated_Date__c= obj.LastModifiedDate;
            pe.SIQ_isActive__c= obj.AxtriaSalesIQTM__Assignment_Status__c;
            pe.SIQ_Position_Type__c = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
            //pe.SIQ_Event__c = 
            pe.Unique_Id__c = key;
            //pe.SIQ_Marketing_Code__c=Countrymap.get(obj.Country_Code__c);
            //pe.SIQ_Marketing_Code__c= obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
            //Added by Ayushi
            if(mapVeeva2Mktcode.get(code) != null){
                pe.SIQ_Marketing_Code__c = mapVeeva2Mktcode.get(code);
            }
            else{
                pe.SIQ_Marketing_Code__c = 'MC code does not exist';
            }
            //Till here..
            pe.SIQ_Permanent_Position_Flag__c=obj.AxtriaSalesIQTM__Assignment_Type__c;
            pe.SIQ_Salesforce_Name__c = obj.AxtriaSalesIQTM__Position__r.Team_Name__c;
            PosEmpList.add(pe);
            Uniqueset.add(Key);    
            recordsProcessed++;
        }
        }
        upsert PosEmpList Unique_Id__c ;//SIQ_POSITION_CODE__c*/
        
    }

    global void finish(Database.BatchableContext BC) {/*
        if(flag)
        {
            //*******************Added by Ritu on 7th August 2018 for including Organisation Master records in outbound *****************************
        //Fetch delta Organisation master to User records and add them to Outbound Position employee
        map<string, list<AxtriaSalesIQTM__Organization_Master__c>> mapCountry2OrgMRecords = new map<string, list<AxtriaSalesIQTM__Organization_Master__c>>();
        List<SIQ_Position_Employee_O__c> PosEmpList1 = new List<SIQ_Position_Employee_O__c>();
        List<SIQ_Position_Employee_O__c> PosEmpList2 = new List<SIQ_Position_Employee_O__c>();
        set<string>uniqueprid = new set<string>();
        mapCountry2OrgMRecords = SalesIQUtility.fetchOrganizationMasterRecords(null,allMksCode); //Passing null as parameter becuase in organisation master, we need to send mapping of 
                                                                                      // org master to users in emp master whcih is independent of whent he organisation master record was updated.
        PosEmpList1.addAll(SalesIQUtility.fetchOrgMasterPositionEmployee(mapCountry2OrgMRecords,allMksCode));
        system.debug('PosEmpList1+'+PosEmpList1);
        system.debug('PosEmpList1 size+'+PosEmpList1.size());
        if(PosEmpList1 !=null && PosEmpList1.size()>0){
            for(SIQ_Position_Employee_O__c pe : PosEmpList1){
                if(!uniqueprid.contains(pe.Unique_Id__c)){
                    PosEmpList2.add(pe);
                    uniqueprid.add(pe.Unique_Id__c);
                    recordsProcessed++;
                }
            }
        }
        //recordsProcessed = recordsProcessed + PosEmpList1.size();
        if(PosEmpList2 !=null && PosEmpList2.size()>0)
            upsert PosEmpList2 Unique_Id__c ;//SIQ_POSITION_CODE__c
        
        //***********************End of changes by Ritu******************************************************************************************
        
        
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
        // Database.ExecuteBatch(new BatchOutBounUpdPositionAccount(),200);
        }




        AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
        exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('PositionEmployeeDeleteEvent')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('PositionEmployeeDeleteEvent'):null;
    
        system.debug('execute trigger' +exeTrigger );
        if(exeTrigger != null){
            if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != True){
                database.executeBatch(new BatchOutboundPosEmp_OB_DeleteEvent ());
            }
        }*/
    }
}