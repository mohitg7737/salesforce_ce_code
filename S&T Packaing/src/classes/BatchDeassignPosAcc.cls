global class BatchDeassignPosAcc implements Database.Batchable<sObject> { //, Database.Stateful
    public String query;
    public Set<String> deassignPosSet; 
    public Set<String> deassignAccSet; 
    public Set<String> deassignTeamInsSet;
    public Set<String> deassignKey;
    public Set<String> deassignHCP;
    public Set<String> deassignHCA;

    global BatchDeassignPosAcc() {
       /* deassignPosSet=new Set<String>();
        deassignAccSet=new Set<String>();
        deassignTeamInsSet=new Set<String>();
        deassignKey=new Set<String>();
        
        query = '';
        query='select id,Account__c,Position__c,Team_Instance__c,Status__c from Deassign_Postiton_Account__c where Status__c=\'New\'';*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Deassign_Postiton_Account__c> scope) {

     /* System.debug('======Query::::' +scope);
      deassignHCP=new Set<String>();
      deassignHCA=new Set<String>();
      
      for(Deassign_Postiton_Account__c deassignRec : scope)
      {
        deassignPosSet.add(deassignRec.Position__c);
        deassignAccSet.add(deassignRec.Account__c);
        deassignTeamInsSet.add(deassignRec.Team_Instance__c);
        deassignKey.add(deassignRec.Account__c + '_' + deassignRec.Position__c + '_' + deassignRec.Team_Instance__c);
      }
      
      System.debug('======deassignPosSet::::' +deassignPosSet);
      System.debug('======deassignAccSet::::' +deassignAccSet);
      System.debug('======deassignTeamInsSet::::' +deassignTeamInsSet);
      System.debug('======deassignKey::::' +deassignKey);


      System.debug('===========Considering HCPs only from Account Object===========');
      List<Account> deassignAccList = [select Id,AccountNumber,Type FROM Account where AccountNumber != null and AccountNumber IN :deassignAccSet];

      if(deassignAccList!= null)
      {
        for(Account acc : deassignAccList)
        {
            if(acc.Type == 'HCP')
            {
                deassignHCP.add(acc.AccountNumber);
            }
            if(acc.Type == 'HCA')
            {
                deassignHCA.add(acc.AccountNumber);
            }
        }
      }

      System.debug('=====deassign HCP Set=====' +deassignHCP);

      System.debug('===========Querying Position Account for Deassign Account-Position Set===========');
      List<AxtriaSalesIQTM__Position_Account__c> deassignPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :deassignHCP and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignTeamInsSet and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false];
      
      System.debug('=====deassign HCP Position Account=====' +deassignPosAccList.size());

      Map<String,Set<String>> mapHCP2key=new Map<String,Set<String>>();
      List<AxtriaSalesIQTM__Position_Account__c> hcpPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

      if(deassignPosAccList != null)
      {
        for(AxtriaSalesIQTM__Position_Account__c deassignPA : deassignPosAccList)
        {
            String key = deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber + '_' +deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
            if(deassignKey.contains(key))
            {
                if(!mapHCP2key.containsKey(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    Set<String> tempKeySet= new Set<String>();
                    tempKeySet.add(tempKey);
                    mapHCP2key.put(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                }
                else
                {
                    String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapHCP2key.get(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                }
                if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);

                hcpPosAccList.add(deassignPA);

            }
        }
      }
      System.debug('=====mapHCP2key::::::' +mapHCP2key);

      System.debug('==========Affiliation Handling for HCPs===============');
        List<AxtriaSalesIQTM__Account_Affiliation__c> affHCP2HCAList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.AccountNumber,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c='Active' and Account_Number__c in :deassignHCP and AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c=true];

        System.debug('=====deassign HCP and HCA Affiliation=====' +affHCP2HCAList);
        Map<String,String> mapdeassignHCP2HCA=new Map<String,String>();

        if(affHCP2HCAList != null)
        {
            for(AxtriaSalesIQTM__Account_Affiliation__c affHCP2HCARec : affHCP2HCAList)
            {
                mapdeassignHCP2HCA.put(affHCP2HCARec.AxtriaSalesIQTM__Account__r.AccountNumber,affHCP2HCARec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
            }
        }
        System.debug('=====mapdeassignHCP2HCA:::::::::' +mapdeassignHCP2HCA);
        
        System.debug('===========Querying Position Account for affiliated HCA (HCP-->HCA) First level===========');
        List<AxtriaSalesIQTM__Position_Account__c> affHCAPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :mapdeassignHCP2HCA.values() and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignTeamInsSet and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false];

        System.debug('=====HCA Position Account=====' +affHCAPosAccList.size());
        Map<String,Set<String>> mapHCA2key=new Map<String,Set<String>>();

        if(affHCAPosAccList != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c affHCARec : affHCAPosAccList)
            {
                if(!mapHCA2key.containsKey(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    String tempKey = affHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    Set<String> tempKeySet= new Set<String>();
                    tempKeySet.add(tempKey);
                    mapHCA2key.put(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                }
                else
                {
                    String tempKey = affHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapHCA2key.get(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                }
            }
        }

        System.debug('===========Check for common Positions in HCP ----> HCA===========');
        Map<String,Set<String>> mapfurthercheckHCA=new Map<String,Set<String>>();

        if(mapHCP2key != null)
        {
            for(String hcp : mapHCP2key.keySet())
            {
                Set<String> hcpPosTIKey = mapHCP2key.get(hcp);
                String hca = mapdeassignHCP2HCA.get(hcp);
                if(mapHCA2key.get(hca) != null)
                {
                    for(String hcaPosTIKey : mapHCA2key.get(hca))
                    {
                        if(hcpPosTIKey.contains(hcaPosTIKey))
                        {
                            if(!mapfurthercheckHCA.containsKey(hca))
                            {
                                Set<String> commonKeySET = new Set<String>();
                                commonKeySET.add(hcaPosTIKey);
                                mapfurthercheckHCA.put(hca,commonKeySET);
                            }
                            else
                            {
                                mapfurthercheckHCA.get(hca).add(hcaPosTIKey);   
                            }
                        }
                    }
                }
            }
        }
        System.debug('=====mapfurthercheckHCA:::::::::' +mapfurthercheckHCA);

        System.debug('==========Further Affiliation Handling for HCAs===============');
        List<AxtriaSalesIQTM__Account_Affiliation__c> furtherAffHCA2HCPList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.AccountNumber,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c='Active' and Parent_Account_Number__c in :mapfurthercheckHCA.keySet() and AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c=true];

        System.debug('=====Affiliated HCAs to further HCPs list size=====' +furtherAffHCA2HCPList.size());
        System.debug('=====Affiliated HCAs to further HCPs=====' +furtherAffHCA2HCPList);
        Map<String,String> mapFurtherHCP2HCA=new Map<String,String>();

        if(furtherAffHCA2HCPList != null)
        {
            for(AxtriaSalesIQTM__Account_Affiliation__c furtherAffHCPRec : furtherAffHCA2HCPList)
            {
                mapFurtherHCP2HCA.put(furtherAffHCPRec.AxtriaSalesIQTM__Account__r.AccountNumber,furtherAffHCPRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
            }
        }

        System.debug('=====mapFurtherHCP2HCA:::::::::' +mapFurtherHCP2HCA);
        
        System.debug('===========Querying Position Account for further affiliated HCPs (HCA---->HCPs)===========');
        List<AxtriaSalesIQTM__Position_Account__c> furtherAffHCPPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :mapFurtherHCP2HCA.keySet() and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignTeamInsSet and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false];

        System.debug('=====further HCPs Position Account=====' +furtherAffHCPPosAccList.size());
        Map<String,Set<String>> mapFurtherHCP2key=new Map<String,Set<String>>();

        if(furtherAffHCPPosAccList != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c furtherHCP2key : furtherAffHCPPosAccList)
            {
                if(!mapFurtherHCP2key.containsKey(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    String tempKey = furtherHCP2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCP2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                    Set<String> tempKeySet= new Set<String>();
                    tempKeySet.add(tempKey);
                    mapFurtherHCP2key.put(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                }
                else
                {
                    String tempKey = furtherHCP2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCP2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapFurtherHCP2key.get(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                }
            }
        }
        System.debug('=====mapFurtherHCP2key:::::::::' +mapFurtherHCP2key);

        System.debug('=====Maintaining Key for HCA and further set of HCPs (Format- Acc_P_TI):::::::::');
        Map<String,Set<String>> mapHCAkey2HCPkey=new Map<String,Set<String>>();

        if(mapFurtherHCP2key != null)
        {
            System.debug('=====mapFurtherHCP2key.keySet():::::::::' +mapFurtherHCP2key.keySet());
            for(String frtherHCP : mapFurtherHCP2key.keySet())
            {
                System.debug('=====frtherHCP:::::::::' +frtherHCP);
                String parentHCA = mapFurtherHCP2HCA.get(frtherHCP);
                System.debug('=====parentHCA:::::::::' +parentHCA);
                Set<String> hcaPosTICheck = mapfurthercheckHCA.get(parentHCA);
                for(String hcaPosTIkeyRec : mapfurthercheckHCA.get(parentHCA))
                {
                    System.debug('=====hcaPosTIkeyRec:::::::::' +hcaPosTIkeyRec);
                    String hcaKey = parentHCA + '_' + hcaPosTIkeyRec;
                    System.debug('=====hcaKey:::::::::' +hcaKey);
                    for(String hcpPosTIKeyRec : mapFurtherHCP2key.get(frtherHCP))
                    {
                        System.debug('=====hcpPosTIKeyRec:::::::::' +hcpPosTIKeyRec);
                        if(hcaPosTICheck.contains(hcpPosTIKeyRec))
                        {
                            String hcpKey = frtherHCP + '_' + hcpPosTIKeyRec;
                            System.debug('=====hcpKey:::::::::' +hcpKey);
                            if(!mapHCAkey2HCPkey.containsKey(hcakey))
                            {
                                Set<String> setkeyHCP = new Set<String>();
                                setkeyHCP.add(hcpKey);
                                mapHCAkey2HCPkey.put(hcakey,setkeyHCP);
                                System.debug('=====mapHCAkey2HCPkey inside if:::::::::' +mapHCAkey2HCPkey);
                            }
                            else
                            {
                                mapHCAkey2HCPkey.get(hcakey).add(hcpKey);
                                System.debug('=====mapHCAkey2HCPkey inside else:::::::::' +mapHCAkey2HCPkey.get(hcakey));
                            }
                        }
                    }
                }
            }
        }
        System.debug('=====mapHCAkey2HCPkey:::::::::' +mapHCAkey2HCPkey);
        System.debug('=====mapHCAkey2HCPkey.size():::::::::' +mapHCAkey2HCPkey.size());

        for(String keyMap : mapHCAkey2HCPkey.keySet())
        {
            Set<String> hcpKeyCheck = mapHCAkey2HCPkey.get(keyMap);
            for(String hcpkeycheckRec : hcpKeyCheck)
            {
                if(deassignKey.contains(hcpkeycheckRec))
                {
                    mapHCAkey2HCPkey.get(keyMap).remove(hcpkeycheckRec);
                }
            }
        }

        System.debug('=====mapHCAkey2HCPkey after removing deassignHCP Key:::::::::' +mapHCAkey2HCPkey);

        Set<String> inactiveHCA=new Set<String>();
        Set<String> furtherHCAkeySet=new Set<String>();

        for(String keyMap : mapHCAkey2HCPkey.keySet())
        {
            if(mapHCAkey2HCPkey.get(keyMap).size() == 0)
            {
                inactiveHCA.add(keymap);
            }
        }
        System.debug('=====inactiveHCA:::::::::' +inactiveHCA);

        for(String hcaRec : mapfurthercheckHCA.keySet())
        {
            for(String posTI : mapfurthercheckHCA.get(hcaRec))
            {
                furtherHCAkeySet.add(hcaRec+'_'+posTI);
            }
            
        }
        System.debug('=====furtherHCAkeySet:::::::::' +furtherHCAkeySet);

        for(String inactiveHCArec : inactiveHCA)
        {
            if(!furtherHCAkeySet.contains(inactiveHCArec))
            {
                inactiveHCA.remove(inactiveHCArec);
            }
        }
        System.debug('=====inactiveHCA final:::::::::' +inactiveHCA);

        List<AxtriaSalesIQTM__Position_Account__c> hcaPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

        if(affHCAPosAccList != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c inactiveAffHCARec : affHCAPosAccList)
            {
                String tempKey = inactiveAffHCARec.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + inactiveAffHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +inactiveAffHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                if(inactiveHCA.contains(tempKey))
                {
                    if(inactiveAffHCARec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        inactiveAffHCARec.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                    if(inactiveAffHCARec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        inactiveAffHCARec.AxtriaSalesIQTM__Effective_End_Date__c=inactiveAffHCARec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);

                    hcaPosAccList.add(inactiveAffHCARec);
                }
            }
        }
        System.debug('=====hcaPosAccList.size():::::::::' +hcaPosAccList.size());
        System.debug('=====hcaPosAccList:::::::::' +hcaPosAccList);

        System.debug('=====hcpPosAccList.size():::::::::' +hcpPosAccList.size());
        System.debug('=====hcpPosAccList:::::::::' +hcpPosAccList);

        if(hcaPosAccList.size() > 0)
            update hcaPosAccList;

        if(hcpPosAccList.size() > 0)
            update hcpPosAccList;

        System.debug('************************************************* HCA De-assignment Handling************************************************************');
        System.debug('=====deassign HCA Set=====' +deassignHCA);

        System.debug('===========Querying Position Account for Deassign Account-Position Set for HCA===========');
        List<AxtriaSalesIQTM__Position_Account__c> deassignHCAPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :deassignHCA and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignTeamInsSet and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false];
        
      
        System.debug('=====deassign HCA Position Account=====' +deassignHCAPosAccList.size());

        Map<String,Set<String>> mapInputHCA2Key=new Map<String,Set<String>>();
        List<AxtriaSalesIQTM__Position_Account__c> inputHcaPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

       if(deassignHCAPosAccList != null)
       {
         for(AxtriaSalesIQTM__Position_Account__c deassignPA : deassignHCAPosAccList)
         {
            String key = deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber + '_' +deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
            if(deassignKey.contains(key))
            {
                if(!mapInputHCA2Key.containsKey(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    Set<String> tempKeySet= new Set<String>();
                    tempKeySet.add(tempKey);
                    mapInputHCA2Key.put(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                }
                else
                {
                    String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapInputHCA2Key.get(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                }
                if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);

                inputHcaPosAccList.add(deassignPA);

            }
         }
       }

        System.debug('=====mapInputHCA2Key::::::' +mapInputHCA2Key);

        System.debug('==========Affiliation Handling for HCAs===============');
        List<AxtriaSalesIQTM__Account_Affiliation__c> affInputHCA2HCPList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.AccountNumber,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c='Active' and Parent_Account_Number__c in :deassignHCA and AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c=true];

        System.debug('=====deassign HCA and HCP Affiliation=====' +affInputHCA2HCPList);
        Map<String,Set<String>> mapdeassignHCA2HCPset=new Map<String,Set<String>>();

        if(affInputHCA2HCPList != null)
        {
            for(AxtriaSalesIQTM__Account_Affiliation__c affHCA2HCPRec : affInputHCA2HCPList)
            {
                if(!mapdeassignHCA2HCPset.containsKey(affHCA2HCPRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber))
                {
                    Set<String> hcpsSet = new Set<String>();
                    hcpsSet.add(affHCA2HCPRec.AxtriaSalesIQTM__Account__r.AccountNumber);
                    mapdeassignHCA2HCPset.put(affHCA2HCPRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber,hcpsSet);
                }
                else
                {
                    mapdeassignHCA2HCPset.get(affHCA2HCPRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber).add(affHCA2HCPRec.AxtriaSalesIQTM__Account__r.AccountNumber);
                }
            }
        }
        System.debug('=====mapdeassignHCA2HCPset:::::::::' +mapdeassignHCA2HCPset);
        Set<String> hcpSetToCheckPosAcc = new Set<String>();

        for(String hca : mapdeassignHCA2HCPset.keySet())
        {
            for(String affhcp : mapdeassignHCA2HCPset.get(hca))
            {
                hcpSetToCheckPosAcc.add(affhcp);
            }
        }
        
        System.debug('===========Querying Position Account for affiliated HCA (HCA-->HCP) First level===========');
        List<AxtriaSalesIQTM__Position_Account__c> affHCPSetPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :hcpSetToCheckPosAcc and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignTeamInsSet and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future') and AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false];

        System.debug('=====HCps Position Account=====' +affHCPSetPosAccList.size());
        Map<String,Set<String>> mapaffiliationHCP2Key=new Map<String,Set<String>>();

        if(affHCPSetPosAccList != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c affHCPRec : affHCPSetPosAccList)
            {
                if(!mapaffiliationHCP2Key.containsKey(affHCPRec.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    String tempKey = affHCPRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCPRec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    Set<String> tempKeySet= new Set<String>();
                    tempKeySet.add(tempKey);
                    mapaffiliationHCP2Key.put(affHCPRec.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                }
                else
                {
                    String tempKey = affHCPRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCPRec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapaffiliationHCP2Key.get(affHCPRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                }
            }
        }

        System.debug('===========mapaffiliationHCP2Key:::::' +mapaffiliationHCP2Key);
        System.debug('===========Check for common Positions in HCA ----> HCP===========');
        Set<String> setInactiveHCPkey = new Set<String>();
        
        if(mapInputHCA2Key != null)
        {
            for(String hca : mapInputHCA2Key.keySet())
            {
                Set<String> setPosTeamInsHCA = mapInputHCA2Key.get(hca);
                System.debug('===========setPosTeamInsHCA:::::' +setPosTeamInsHCA);
                if(mapdeassignHCA2HCPset.get(hca) != null)
                {
                    for(String hcp : mapdeassignHCA2HCPset.get(hca))
                    {
                        System.debug('===========hcp:::::' +hcp);
                        if(mapaffiliationHCP2Key.get(hcp) != null)
                        {    
                            for(String posTeamInsCheckHCP : mapaffiliationHCP2Key.get(hcp))
                            {
                                System.debug('===========posTeamInsCheckHCP:::::' +posTeamInsCheckHCP);
                                if(setPosTeamInsHCA.contains(posTeamInsCheckHCP))
                                {
                                    System.debug('===common Position');
                                    setInactiveHCPkey.add(hcp+'_'+posTeamInsCheckHCP);
                                }
                            }
                        }
                    }
                }
            }
        }
        System.debug('===========setInactiveHCPkey:::::' +setInactiveHCPkey);
        List<AxtriaSalesIQTM__Position_Account__c> inactiveHCPposAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

        if(affHCPSetPosAccList != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c inactiveHCPrec : affHCPSetPosAccList)
            {
                String key = inactiveHCPrec.AxtriaSalesIQTM__Account__r.AccountNumber + '_' +inactiveHCPrec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + inactiveHCPrec.AxtriaSalesIQTM__Team_Instance__r.Name;
                System.debug('===========key:::::' +key);
                if(setInactiveHCPkey.contains(key))
                {
                    if(inactiveHCPrec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        inactiveHCPrec.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                    if(inactiveHCPrec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        inactiveHCPrec.AxtriaSalesIQTM__Effective_End_Date__c=inactiveHCPrec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);

                    inactiveHCPposAccList.add(inactiveHCPrec);
                }
            }
        }

        System.debug('===========inputHcaPosAccList.size():::::' +inputHcaPosAccList.size());
        System.debug('===========inputHcaPosAccList:::::' +inputHcaPosAccList);
        if(inputHcaPosAccList.size() > 0)
            update inputHcaPosAccList;

        System.debug('===========inactiveHCPposAccList.size():::::' +inactiveHCPposAccList.size());
        System.debug('===========inactiveHCPposAccList:::::' +inactiveHCPposAccList);
        if(inactiveHCPposAccList.size() > 0)
            update inactiveHCPposAccList;

        List<Deassign_Postiton_Account__c> deassignList=new List<Deassign_Postiton_Account__c>();

        for(Deassign_Postiton_Account__c deassignProcessRec : scope)
        {
          if(deassignHCP.contains(deassignProcessRec.Account__c))
          {
              deassignProcessRec.Status__c='Processed';
              deassignList.add(deassignProcessRec);    
          }
          if(deassignHCA.contains(deassignProcessRec.Account__c))
          {
              deassignProcessRec.Status__c='Processed';
              deassignList.add(deassignProcessRec);    
          }
        }
          
        if(deassignList.size() > 0)
            update deassignList;*/

    }

    global void finish(Database.BatchableContext BC) {

    }
}