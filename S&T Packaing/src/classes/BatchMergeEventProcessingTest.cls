@isTest
public class BatchMergeEventProcessingTest {
    
    static  testmethod void TestMovement(){
        User loggedInUser = new User(id=UserInfo.getUserId());
		string classname = 'BatchMergeEventProcessingTest';
       
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        
         Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
		AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
       
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        
        Merge_Unmerge__c m = new Merge_Unmerge__c();
        m.Winner_Merge_Account__c = 'BH10461999';
        m.Looser_Merge_Account__c =  'BH10455999'; 
        m.External_ID__c = 'BH10461999_BH10455999'; 
        m.Losing_Source_ID__c = 'test'; 
        m.Market_Code__c = 'test'; 
        m.Country_Code__c = 'test';
        m.MDM_Customer_GUID_1__c = 'BH10461999';
        m.MDM_Customer_GUID_2__c = 'BH10455999';
        m.GUID_1_ID__c = a1.id;
        m.GUID_2_ID__c = acc.id;
        m.MDM_Customer_ID_1__c = 'BH10461999';
        m.MDM_Customer_ID_2__c = 'BH10455999';
        m.MDM_Previous_Customer_GUID__c = 'test';
        //m.LastModifiedDate = Datetime.now().addMinutes(10);
        SnTDMLSecurityUtil.insertRecords(m,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
            BatchMergeEventProcessing obj=new BatchMergeEventProcessing(null);
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
    static  testmethod void TestMovement2(){
        User loggedInUser = new User(id=UserInfo.getUserId());
		string classname = 'BatchMergeEventProcessingTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        
         Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        teamins1.Name = 'testbbb';
        teamins1.AxtriaSalesIQTM__Alignment_Period__c ='Future';
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+2;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Future';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+2;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
      
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()+1;
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+2;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()+1;
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+2;
        posAccount1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        pacp.AxtriaSalesIQTM__Account__c = acc.id;
        pacp.Name = 'test';
       // pacp.Party_ID__c=winPAmap.get(winacc);
        pacp.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp.AxtriaSalesIQTM__Position__c = pos.id;
        pacp.AxtriaSalesIQTM__lastApprovedTarget__c = true;
		pacp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
		pacp.AxtriaSalesIQTM__Effective_End_Date__c = system.today()+1;
        SnTDMLSecurityUtil.insertRecords(pacp,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        pacp1.AxtriaSalesIQTM__Account__c = a1.id;
        pacp1.Name = 'test1';
       // pacp.Party_ID__c=winPAmap.get(winacc);
        pacp1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp1.AxtriaSalesIQTM__Position__c = pos1.id;
        pacp1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
		pacp1.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
		pacp1.AxtriaSalesIQTM__Effective_End_Date__c = system.today()+1;
        SnTDMLSecurityUtil.insertRecords(pacp1,className);
        
        Merge_Unmerge__c m = new Merge_Unmerge__c();
        m.Winner_Merge_Account__c = 'BH10461999';
        m.Looser_Merge_Account__c =  'BH10455999'; 
        m.External_ID__c = 'BH10461999_BH10455999'; 
        m.Losing_Source_ID__c = 'test'; 
        m.Market_Code__c = 'test'; 
        m.Country_Code__c = 'test';
        m.MDM_Customer_GUID_2__c = 'BH10455999';
        m.MDM_Customer_GUID_1__c = 'BH10461999';
        m.GUID_2_ID__c = acc.id;
        m.GUID_1_ID__c = a1.id;
        m.MDM_Customer_ID_2__c = 'BH10455999';
        m.MDM_Customer_ID_1__c = 'BH10461999';
        m.MDM_Previous_Customer_GUID__c = 'test';
        //m.LastModifiedDate = Datetime.now().addMinutes(10);
        SnTDMLSecurityUtil.insertRecords(m,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
            BatchMergeEventProcessing obj=new BatchMergeEventProcessing(null);
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
	static  testmethod void TestMovement3(){
        User loggedInUser = new User(id=UserInfo.getUserId());
		string classname = 'BatchMergeEventProcessingTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();

        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        SnTDMLSecurityUtil.insertRecords(a1,className);
        
         Account acc= TestDataFactory.createAccount();

        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        teamins1.Name = 'test121';
        teamins1.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos,className);
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = false;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
      
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos1,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+2;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
		 // -SR-357 changes by RT on 5thNov --
         posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = Date.Today().addDays(1);
         posAccount.AxtriaSalesIQTM__Effective_End_Date__c = posAccount.AxtriaSalesIQTM__Effective_Start_Date__c ;
         SnTDMLSecurityUtil.updateRecords(posAccount,className);
         // -SR-357 ends here --
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+2;
        posAccount1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        pacp.AxtriaSalesIQTM__Account__c = acc.id;
        pacp.Name = 'test';
       // pacp.Party_ID__c=winPAmap.get(winacc);
        pacp.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp.AxtriaSalesIQTM__Position__c = pos1.id;
        pacp.AxtriaSalesIQTM__lastApprovedTarget__c = true;
		pacp.AxtriaSalesIQTM__Effective_End_Date__c = system.today()+1;
		pacp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        SnTDMLSecurityUtil.insertRecords(pacp,className);
        
		system.debug('check pacp loosing'+pacp);
		system.debug('check pacp loosing1'+pacp.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c);
		system.debug('check pacp loosing2'+pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c);
		
		
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        pacp1.AxtriaSalesIQTM__Account__c = a1.id;
        pacp1.Name = 'test1';
       // pacp.Party_ID__c=winPAmap.get(winacc);
        pacp1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp1.AxtriaSalesIQTM__Position__c = pos1.id;
        pacp1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
		pacp1.AxtriaSalesIQTM__Effective_End_Date__c = system.today()+1;
		pacp1.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        SnTDMLSecurityUtil.insertRecords(pacp1,className);
        
		
        Merge_Unmerge__c m = new Merge_Unmerge__c();
        m.Winner_Merge_Account__c = 'BH10461999';
        m.Looser_Merge_Account__c =  'BH10455999'; 
        m.External_ID__c = 'BH10461999_BH10455999'; 
        m.Losing_Source_ID__c = 'test'; 
        m.Market_Code__c = 'US'; 
        m.Country_Code__c = 'US';
        m.MDM_Customer_GUID_2__c = 'BH10455999';
        m.MDM_Customer_GUID_1__c = 'BH10461999';
        m.GUID_2_ID__c = acc.id;
        m.GUID_1_ID__c = a1.id;
        m.MDM_Customer_ID_2__c = 'BH10455999';
        m.MDM_Customer_ID_1__c = 'BH10461999';
        m.MDM_Previous_Customer_GUID__c = 'test';
        //m.LastModifiedDate = Datetime.now().addMinutes(10);
        SnTDMLSecurityUtil.insertRecords(m,className);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
            BatchMergeEventProcessing obj=new BatchMergeEventProcessing(null);
            Database.executeBatch(obj);
        }
        Test.stopTest();
        
    }
}