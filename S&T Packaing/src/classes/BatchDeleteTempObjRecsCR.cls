/**********************************************************************************************
@author   	  : Himanshu Tariyal (A0994)
@modifiedDate : 17th June'2020
@description  : Batch class for deleting errored Temp recs when Technical issues arise(Heap issues etc)
@Revison(s)   : v1.0
**********************************************************************************************/
global  with sharing class BatchDeleteTempObjRecsCR implements Database.Batchable<sObject>,Database.Stateful
{
    public List<temp_Obj__c> recordsToinsert;

    public String changeReqID;
    public String batchName;
    public String query;
     
    public BatchDeleteTempObjRecsCR(String crID)
    {
    	batchName = 'BatchDeleteTempObjRecsCR';
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        changeReqID = crID;
        SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);
 
 		if(changeReqID!=null && changeReqID!='')
 			query = 'select id from temp_obj__c where Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
 		else
			query = '';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');
    	SnTDMLSecurityUtil.printDebugMessage('query--'+query);
    	return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> tempRecsList)
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');
    	SnTDMLSecurityUtil.printDebugMessage('tempRecsList size--'+tempRecsList.size());

        try
        {
        	if(!tempRecsList.isEmpty())
        		SnTDMLSecurityUtil.deleteRecords(tempRecsList,batchName);
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in execute() method--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
    }
}