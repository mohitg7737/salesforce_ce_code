global class BatchCheckOrphanHCP implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String teamInsset;

    @Deprecated
     global BatchCheckOrphanHCP(Set<String> teamIns) {
     }

    global BatchCheckOrphanHCP(String teamIns) {
        query = '';
        teamInsset='';
        teamInsSet = teamIns;

        query = 'select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\') and AxtriaSalesIQTM__Team_Instance__c = :teamInsSet and AxtriaSalesIQTM__Account__r.Type = \'HCP\' and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Team_Instance__c != null order by AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {

        System.debug('======================================== Maintain HCP 2 PosTeamIns KeySet==========================================================');

        System.debug('====Query Size:::::::::::' +scope.size());
        System.debug('====Query:::::::::::' +scope);

        Set<String> setHCP = new Set<String>();
        Set<String> setPos = new Set<String>();
        Set<String> setTeamIns = new Set<String>();
        Map<String,Set<String>> mapHCP2PosTeamInsKey = new Map<String,Set<String>>();

        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {
            setHCP.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            setPos.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            setTeamIns.add(posAcc.AxtriaSalesIQTM__Team_Instance__r.Name);

            if(!mapHCP2PosTeamInsKey.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
            {
                Set<String> tempKey = new Set<String>();
                tempKey.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name);
                mapHCP2PosTeamInsKey.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,tempKey);
            }
            else
            {
                String key = posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name;
                mapHCP2PosTeamInsKey.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber).add(key);
            }
            System.debug('====HCP Account Number:::::::::::' +posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            System.debug('====key:::::::::::' +posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name);
        }
        System.debug('====mapHCP2PosTeamInsKey:::::::::::' +mapHCP2PosTeamInsKey);
        System.debug('====setHCP:::::::::::' +setHCP);
        System.debug('====setPos:::::::::::' +setPos);
        System.debug('====setTeamIns:::::::::::' +setTeamIns);
        

        System.debug('===================================Check Staging Object HCP to Primary HCA==========================================================');

        List<SIQ_Account_Affiliation__c> stagingAccAffListHCP = [select Id,SIQ_Account_Number__c,SIQ_Affiliation_Status__c,SIQ_Parent_Account_Number__c from SIQ_Account_Affiliation__c where SIQ_Affiliation_Status__c='Active' and SIQ_Primary_Affiliation_Indicator__c = 'Y' and SIQ_Account_Number__c in :setHCP and SIQ_Account_Number__c != null and SIQ_Parent_Account_Number__c != null];

        Set<String> setHCA = new Set<String>();
        Map<String,String> mapHCP2HCA = new Map<String,String>();

        for(SIQ_Account_Affiliation__c affRec : stagingAccAffListHCP)
        {
            setHCA.add(affRec.SIQ_Parent_Account_Number__c);
            System.debug('====HCP:::::::::::' +affRec.SIQ_Account_Number__c);
            System.debug('====erespective HCA:::::::::::' +affRec.SIQ_Parent_Account_Number__c);

            mapHCP2HCA.put(affRec.SIQ_Account_Number__c,affRec.SIQ_Parent_Account_Number__c);

            /*if(!mapHCP2HCA.containsKey(affRec.SIQ_Account_Number__c))
            {
                Set<String> tempAcc = new Set<String>();
                tempAcc.add(affRec.SIQ_Parent_Account_Number__c);
                mapHCP2HCA.put(affRec.SIQ_Account_Number__c,tempAcc);
            }
            else
            {
                mapHCP2HCA.get(affRec.SIQ_Account_Number__c).add(affRec.SIQ_Parent_Account_Number__c);
            }*/
        }
        System.debug('====mapHCP2HCA:::::::::::' +mapHCP2HCA);

        System.debug('==========================================Position Account check for HCA================================================================');
        
        List<AxtriaSalesIQTM__Position_Account__c> posAccListHCA = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Team_Instance__c in :setTeamIns and AxtriaSalesIQTM__Account__r.Type = 'HCA' and AxtriaSalesIQTM__Account__r.AccountNumber in :setHCA and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Team_Instance__c != null];

        Map<String,Set<String>> mapHCA2PosTeamInsKey = new Map<String,Set<String>>();

        if(posAccListHCA != null)
        {
            for(AxtriaSalesIQTM__Position_Account__c posAcc : posAccListHCA)
            {
                if(!mapHCA2PosTeamInsKey.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    Set<String> tempKey = new Set<String>();
                    tempKey.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name);
                    mapHCP2PosTeamInsKey.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,tempKey);
                }
                else
                {
                    String key = posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name;
                    mapHCP2PosTeamInsKey.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber).add(key);
                }
                System.debug('====HCA Account Number:::::::::::' +posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
                System.debug('====key:::::::::::' +posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '&' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name);
            }
        }

        System.debug('===================================check for missing affiliation and Orphan Child======================================================');

        // System.debug('1 =======================>>>>>>>>>>>>  Orphan HCPs::::::::::::::::');
        // Set<String> orphanHCPSet = new Set<String>();
        
        // for(String hcp : setHCP)
        // {
        //     if(!mapHCP2HCA.containsKey(hcp))
        //     {
        //         orphanHCPSet.add(hcp);
        //     }
        // }

        List<Check_Missing_Affiliation__c> insertList = new List<Check_Missing_Affiliation__c>();
        //List<Check_Missing_Affiliation__c> finalInsertList = new List<Check_Missing_Affiliation__c>();
        //Set<String> exist = new Set<String>();

        if(mapHCP2PosTeamInsKey != null)
        {
            for(String hcp : mapHCP2PosTeamInsKey.keySet())
            {
                System.debug('hcp:::::::::: ' +hcp);
                Set<String> hcpPosTeamInsSet = new Set<String>();
                hcpPosTeamInsSet = mapHCP2PosTeamInsKey.get(hcp);
                System.debug('hcpPosTeamInsSet:::::::::: ' +hcpPosTeamInsSet);
                if(!mapHCP2HCA.containsKey(hcp))
                {
                    System.debug('::::::::::Orphan HCPs::::::::::::::::');
                    
                    for(String posTeamIns : hcpPosTeamInsSet)
                    {
                        Check_Missing_Affiliation__c insertRec = new Check_Missing_Affiliation__c();
                        insertRec.HCAs__c= null;
                        insertRec.HCPs__c= hcp;
                        System.debug('posTeamIns:::::::::: ' +posTeamIns);
                        List<String> strings = new List<String>();
                        strings = posTeamIns.split('&');
                        insertRec.Position_Code__c = strings[0];
                        System.debug('strings[0]:::::::::: ' +strings[0]);
                        insertRec.Team_Instance__c = strings[1];
                        System.debug('strings[1]:::::::::: ' +strings[1]);
                        insertRec.ConcateKey__c = hcp + strings[0] + strings[1] + null;
                        insertList.add(insertRec);
                        System.debug('insertRec in if' +insertRec);
                    }
                }
                else
                {
                    System.debug('::::::::Check for missing Affiliations::::::::::::::::');
                    if(mapHCP2HCA.get(hcp) != null)
                    {
                        String hca = mapHCP2HCA.get(hcp);
                        System.debug('hca::::::::::: ' +hca);
                        if(mapHCA2PosTeamInsKey.get(hca) != null)
                        {
                            Set<String> hcaPosTeamInsSet = new Set<String>();
                            hcaPosTeamInsSet = mapHCA2PosTeamInsKey.get(hca);
                            System.debug('hcaPosTeamInsSet::::::::::: ' +hcaPosTeamInsSet);

                            for(String hcpKey : hcpPosTeamInsSet)
                            {
                                System.debug('hcpKey::::::::::: ' +hcpKey);
                                if(!hcaPosTeamInsSet.contains(hcpKey))
                                {
                                    System.debug('Missing key is :::::::::: ' +hcpKey);
                                    Check_Missing_Affiliation__c insertRec = new Check_Missing_Affiliation__c();
                                    insertRec.HCAs__c= hca;
                                    insertRec.HCPs__c= hcp;
                                    List<String> strings = new List<String>();
                                    strings = hcpKey.split('&');
                                    insertRec.Position_Code__c = strings[0];
                                    insertRec.Team_Instance__c = strings[1];
                                    insertRec.ConcateKey__c = hcp + strings[0] + strings[1] + hca;
                                    System.debug('Missing hca::::::::::: ' +hca);
                                    System.debug('Missing hcp::::::::::: ' +hcp);
                                    System.debug('strings[0]:::::::::: ' +strings[0]);
                                    System.debug('strings[1]:::::::::: ' +strings[1]);
                                    insertList.add(insertRec);
                                    System.debug('insertRec in else' +insertRec);   
                                }
                            }
                        }
                        else
                        {
                            for(String posTeamIns : hcpPosTeamInsSet)
                            {
                                Check_Missing_Affiliation__c insertRec = new Check_Missing_Affiliation__c();
                                insertRec.HCAs__c= hca;
                                insertRec.HCPs__c= hcp;
                                System.debug('posTeamIns:::::::::: ' +posTeamIns);
                                List<String> strings = new List<String>();
                                strings = posTeamIns.split('&');
                                insertRec.Position_Code__c = strings[0];
                                System.debug('strings[0]:::::::::: ' +strings[0]);
                                insertRec.Team_Instance__c = strings[1];
                                System.debug('strings[1]:::::::::: ' +strings[1]);
                                insertRec.ConcateKey__c = hcp + strings[0] + strings[1] + null;
                                insertList.add(insertRec);
                                System.debug('insertRec when no Position Account exist for HCA' +insertRec);
                            } 
                        }
                        
                    }
                }

            }
        }

        System.debug('insertList size' +insertList.size());
         System.debug('insertList size' +insertList);
        if(insertList.size() > 0)
            insert insertList;

    }

    global void finish(Database.BatchableContext BC) {

        System.debug('Email Notification');

        BatchCheckOrphanHCPFilesEmail obj = new BatchCheckOrphanHCPFilesEmail();
        Database.executeBatch(obj,2000);

    }
}