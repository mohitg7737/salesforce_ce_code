/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 23rd September'2020
Description : Test class for Batch_DeletePositionProduct
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class Batch_DeletePositionProduct_Test 
{
    public static testMethod void testMethod1() 
    {
    	String className = 'Batch_DeletePositionProduct_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);

    	Scheduler_Log__c schLog = TestDataFactory.createSchLog('Full Load','PositionProduct');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        SIQ_My_Setup_Products_vod_O__c spa = new SIQ_My_Setup_Products_vod_O__c();
        spa.SIQ_Country__c = countr.Id;
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new Batch_DeletePositionProduct());

        System.Test.stopTest();
    }

    public static testMethod void testMethod2() 
    {
    	String className = 'Batch_DeleteObjectTerritory_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Delta';
        SnTDMLSecurityUtil.insertRecords(countr,className);

		Scheduler_Log__c schLog = TestDataFactory.createSchLog('Delta','PositionProduct');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        SIQ_My_Setup_Products_vod_O__c spa = new SIQ_My_Setup_Products_vod_O__c();
        spa.Record_Status__c = 'Updated';
        spa.SIQ_Country__c = countr.Id;
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new Batch_DeletePositionProduct());

        System.Test.stopTest();
    }

    public static testMethod void testMethod3() 
    {
    	String className = 'Batch_DeleteObjectTerritory_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);

    	Scheduler_Log__c schLog = TestDataFactory.createSchLog('Full Load','PositionProduct');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        SIQ_My_Setup_Products_vod_O__c spa = new SIQ_My_Setup_Products_vod_O__c();
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new Batch_DeletePositionProduct());

        System.Test.stopTest();
    }
}