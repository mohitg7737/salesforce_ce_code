global class PACP_lookups_Utility1 implements Database.Batchable<sObject> {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    public string country;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    map<string,string>alignmentperiodmap ;
    map<string,AxtriaARSnT__Call_Plan__c>Cycle_HCP_Productmap ;
    map<string,AxtriaARSnT__Call_Plan__c>Cycle_HCP_Product_Positionmap ;
    String alignmnetperiod;
    public list<AxtriaSalesIQTM__Team_Instance__c> countrylst=new list<AxtriaSalesIQTM__Team_Instance__c>();

  
   global PACP_lookups_Utility1(String teamInstance) {
       /* teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        Cycle_HCP_Productmap=new Map<String,AxtriaARSnT__Call_Plan__c>();
        Cycle_HCP_Product_Positionmap =new Map<String,AxtriaARSnT__Call_Plan__c>();
        selectedTeamInstance = teamInstance;
        alignmentperiodmap = new map<string,string>();
        alignmnetperiod = '';
        
        countrylst =[select id,name,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where name=:teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;

        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Alignment_Period__c From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            alignmentperiodmap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Alignment_Period__c);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        alignmnetperiod = alignmentperiodmap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id,Name,AxtriaARSnT__Cycle__c, SalesIQ_Error_Message__c,AxtriaARSnT__Account_ID__c, AxtriaARSnT__Team_ID__c ,AxtriaARSnT__Product_Name__c, AxtriaARSnT__Territory_ID__c, AxtriaARSnT__Segment__c, AxtriaARSnT__Objective__c, AxtriaARSnT__Product_ID__c, AxtriaARSnT__Adoption__c, AxtriaARSnT__Potential__c, AxtriaARSnT__Target__c, AxtriaARSnT__Previous_Cycle_Calls__c, AxtriaARSnT__Account__c, AxtriaARSnT__Position__c, AxtriaARSnT__Position_Team_Instance__c, AxtriaARSnT__Team_Instance__c, AxtriaARSnT__isError__c FROM AxtriaARSnT__Call_Plan__c  Where AxtriaARSnT__Team_ID__c   = \'' + teamInstance + '\' and AxtriaARSnT__isError__c = false'; */
    } 

    global Database.QueryLocator start(Database.BatchableContext bc) {
       // system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaARSnT__Call_Plan__c> scopeRecs) {
     /* system.debug('Hello++++++++++++++++++++++++++++');
        Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Set<string>productid = new set<string>();
        Map<string,id> posCodeIdMap = new Map<string,id>();
        Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        // gnan
        Map<string,Account> accountNumberIdMap = new Map<string,Account>();
        
        //gnan
        Map<string,id> accountNumberPosIDtoPosAccIdMap = new Map<string,id>();
        Map<string,id> posTeamInstMap = new Map<string,id>();
        map<string,AxtriaARSnT__Product_Catalog__c>Prodcodetonamemap = new map<string,AxtriaARSnT__Product_Catalog__c>();
        set<string> positionset= new set<string>();
        Map<String,Set<String>> postoproductmap = new Map<String,Set<String>>();
        set<String> productset = new set<string>();
        
        
        List<AxtriaARSnT__Error_Object__c> errorList = new List<AxtriaARSnT__Error_Object__c>();
        Map<id,AxtriaARSnT__Call_Plan__c>duplicatecallplanMap= new map<id,AxtriaARSnT__Call_Plan__c>();
        AxtriaARSnT__Error_Object__c tempErrorObj;

        for(AxtriaARSnT__Call_Plan__c cp : scopeRecs)
        {      //Positions Set
            if(!string.IsBlank(cp.AxtriaARSnT__Territory_ID__c))
            {
                posCodeSet.add(cp.AxtriaARSnT__Territory_ID__c);
            }

            if(!string.IsBlank(cp.AxtriaARSnT__Account_ID__c))
            {            //Accounts Set
                accountNumberSet.add(cp.AxtriaARSnT__Account_ID__c);
            }
            if(!string.isBlank(cp.AxtriaARSnT__Product_ID__c))
            {
                productid.add(cp.AxtriaARSnT__Product_ID__c);
            }
        }
        String key;
        for(AxtriaSalesIQTM__Position__c  pos : [Select id,  AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position__c 
                                                where AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet]){
            if(pos.AxtriaSalesIQTM__inactive__c == false){
                key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                posCodeIdMap.put(key,pos.id);
            }
            else{
              key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                inactivePosCodeIdMap.put(key,pos.id);
            }
        } 
        //-------- Position and PTI Map
       for(AxtriaSalesIQTM__Position_Team_Instance__c  posTeamInst : [Select Id,AxtriaSalesIQTM__Team_Instance_ID__r.Name,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c=:alignmnetperiod])
        {
            key = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c + (string)posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            if(posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c == false)
            {
                
                posCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);        //Map of Active Positions and their lookups
            }
            else
            {
                inactivePosCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);    //Map of InActive Positions and their lookups
            }
            String position = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c;
          String teamInstance = posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            posTeamInstMap.put(posTeamInst.AxtriaSalesIQTM__Position_ID__c,posTeamInst.Id);
            
        }
        
        Set<String>SegmentListValues=new set<String>();
         for(SalesIQ_Direct_Load__mdt DirectLoad:[SELECT MasterLabel,DeveloperName,Segment_Values__c FROM SalesIQ_Direct_Load__mdt WHERE MasterLabel='Segment_Values'])
        {
            SegmentListValues.add(DirectLoad.Segment_Values__c.trim().tolowercase());
        }
        system.debug('============accountNumberSet::::'+accountNumberSet);
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber,AxtriaSalesIQTM__Active__c,AxtriaARSnT__Active__c from Account where AccountNumber IN:accountNumberSet and AxtriaARSnT__Country_ID__c=:country])
        { //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc);                  //Map of accounts and their lookups     
        }
        system.debug('=========accountNumberIdMap==========='+accountNumberIdMap);
        //-------- Position Account Map
         for(AxtriaSalesIQTM__Position_Account__c acc : [Select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:accountNumberSet and AxtriaSalesIQTM__Team_Instance__c = :teamInstanceNametoIDMap.get(selectedTeamInstance) and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=:alignmnetperiod and AxtriaARSnT__Country__c=:country and AxtriaSalesIQTM__Assignment_Status__c!='Inactive'])
        { //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberPosIDtoPosAccIdMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber+acc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,acc.id);                  //Map of accounts and their lookups  
        }
        
        //------- Product ID to Name Map
        system.debug('==============selectedTeamInstance::'+selectedTeamInstance);
        system.debug('==============productid::'+productid);
        
        for(AxtriaARSnT__Product_Catalog__c prod :[select id,Name,AxtriaARSnT__Veeva_External_ID__c,AxtriaARSnT__IsActive__c from AxtriaARSnT__Product_Catalog__c where AxtriaARSnT__Veeva_External_ID__c IN:productid and AxtriaARSnT__Team_Instance__r.Name =: selectedTeamInstance and AxtriaARSnT__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c =:country and AxtriaARSnT__IsActive__c=true] )
        {
            Prodcodetonamemap.put(prod.AxtriaARSnT__Veeva_External_ID__c,prod);
        }

        //------ Position product
        map<string,string>prodidtoname = new map<string,string>();
        map<string,set<string>>postoprodid = new map<string,set<string>>();
        
        for(AxtriaSalesIQTM__Position_Product__c posProduct :[SELECT AxtriaARSnT__Product_Catalog__c,AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c,AxtriaARSnT__Product_Catalog__r.Name,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Product_Weight__c,AxtriaSalesIQTM__Team_Instance__c,Id,Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : posCodeSet and AxtriaSalesIQTM__Team_Instance__r.name =:selectedTeamInstance and AxtriaSalesIQTM__isActive__c=true])
        {
            
            if(!postoproductmap.containsKey(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
            {   
                 productset.add(posProduct.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c);
                 postoproductmap.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,productset);
                 system.debug('++++productset+++'+productset);
                 system.debug('++++postoproductmap+++'+postoproductmap);

             }
             else{
                set<string>productset1 = new set<string>();
                productset1.addall(postoproductmap.get(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                productset1.add(posProduct.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c);  
                postoproductmap.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,productset1);
                system.debug('++++productset1+++'+productset1);
                system.debug('++++postoproductmap+++'+postoproductmap);
             }
             prodidtoname.put(posProduct.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c,posProduct.AxtriaARSnT__Product_Catalog__r.Name);*/
              
             /*if(postoprodid.containsKey)
             prodidtoname.put(posProduct.Product_Catalog__r.Veeva_External_ID__c,posProduct.Product_Catalog__r.Name);
             postoprodid.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,)
            
        }
        system.debug('++++postoproductmap OUT SIDE+++'+postoproductmap);
        system.debug('++++prodidtoname+++'+prodidtoname);
        
        for(AxtriaARSnT__Call_Plan__c cp : scopeRecs)
        {
        cp.SalesIQ_Error_Message__c='';
            //---------- Objectives checking
            if(!string.isblank(cp.AxtriaARSnT__Objective__c)){
             if(!cp.AxtriaARSnT__Objective__c.isnumeric()){
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Objectives should be numeric. ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Objective__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Objectives should be numeric.';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c  ;
                    errorList.add(tempErrorObj);
             }
                                     
            }else{
            
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Objective is empty. ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Objective__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Objective is empty';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c  ;
                    errorList.add(tempErrorObj);
            }
            
            //---------- Segment checking 
            if(!string.isblank(cp.AxtriaARSnT__Segment__c)){
             if(!SegmentListValues.contains(cp.AxtriaARSnT__Segment__c.tolowercase())){
                     cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Segment value is incorrect. ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Segment__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Segment value is incorrect.';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c  ;
                    errorList.add(tempErrorObj);
             }
                                     
            }else{
            
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Segment is empty. ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Segment__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Segment is empty';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c  ;
                    errorList.add(tempErrorObj);
            }
           
          
            //---------- Team Instance lookups
            if(!string.IsBlank(cp.AxtriaARSnT__Team_ID__c  )){
                if(teamInstanceNametoIDMap.containsKey(cp.AxtriaARSnT__Team_ID__c )){
                    cp.AxtriaARSnT__Team_Instance__c = teamInstanceNametoIDMap.get(cp.AxtriaARSnT__Team_ID__c  );    //Filling lookups of Team Instance
                }
                else{
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Problem in Name of Team Instance: Team Instance name not found in map. ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Team_Instance__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Problem in Name of Team Instance: Team Instance name not found in map';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c  ;
                    errorList.add(tempErrorObj);
                }
            }
            else{
              
                cp.AxtriaARSnT__isError__c = true;
                 cp.SalesIQ_Error_Message__c+='Team Instance Name is Blank. ';
                    
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Team_Instance__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Team Instance Name is Blank';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
            }
            //---------- Account lookups
            if(!string.isBlank(cp.AxtriaARSnT__Account_ID__c)){
              if(accountNumberIdMap.containsKey(cp.AxtriaARSnT__Account_ID__c))
              {
              //gnan
                if(accountNumberIdMap.get(cp.AxtriaARSnT__Account_ID__c).AxtriaARSnT__Active__c!='Inactive')
                {
                    cp.AxtriaARSnT__Account__c = accountNumberIdMap.get(cp.AxtriaARSnT__Account_ID__c).id;        //Filling lookups of Accounts
                }else
                {                
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='HCP is not active. ';                     
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'HCP is not active.';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
                }
              }
              else{
                cp.AxtriaARSnT__isError__c = true;
                cp.SalesIQ_Error_Message__c+='Account Number not present in SalesIQ. ';
                 
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Account Number not present in SalesIQ.';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.AxtriaARSnT__isError__c = true;
               cp.SalesIQ_Error_Message__c+='Account Number is Blank. ';
                 
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Account Number is Blank';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
              
            }

            //---------Product lookup
            system.debug('=========cp.AxtriaARSnT__Product_ID__c:::'+cp.AxtriaARSnT__Product_ID__c);
            system.debug('====postoproduct-------:'+postoproductmap);
            if(!string.isBlank(cp.AxtriaARSnT__Product_ID__c))
            {
              if(Prodcodetonamemap.containsKey(cp.AxtriaARSnT__Product_ID__c)){
                 if(Prodcodetonamemap.get(cp.AxtriaARSnT__Product_ID__c).AxtriaARSnT__IsActive__c==false)
                 {
                 cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Product is not enabled for the corresponding alignment';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'AxtriaARSnT__Product_ID__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Product is not enabled for the corresponding alignment';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
                }else{
               // String prodkey = cp.AxtriaARSnT__Product_ID__c;
                  String prodkey = cp.AxtriaARSnT__Territory_ID__c;
                  system.debug('====prodkey-------:'+prodkey);
                  set<string>products = new set<string>();
                  boolean prodexist=false;
                  string prodname ='';
                if(postoproductmap.containsKey(prodkey))
                {
                    products = postoproductmap.get(prodkey);
                }
                system.debug('++products++'+products);
                if(products.contains(cp.AxtriaARSnT__Product_ID__c))
                {
                    system.debug('++++++++++++  prod Exists'+prodexist);
                    prodexist=true;
                    prodname=prodidtoname.get(cp.AxtriaARSnT__Product_ID__c);
                    cp.AxtriaARSnT__Product_Name__c = prodname;
                }
                
                system.debug('++++++++++++  prod Exists' + prodexist);
                if(prodexist == false)
                {
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Product to position mapping should be present';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'AxtriaARSnT__Product_ID__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Product to position mapping should be present' ;
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
                }
               } 
              }else{
              cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Product is not available in SalesIQ';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'AxtriaARSnT__Product_ID__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Product is not available in SalesIQ';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
              }
            }else{
                    cp.AxtriaARSnT__isError__c = true;
                    cp.SalesIQ_Error_Message__c+='Product ID is Blank.';
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'AxtriaARSnT__Product_ID__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Product ID is Blank';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
            }
            
            //------------  Position and PTI lookups
            String keyClientCodeTeamName;
            keyClientCodeTeamName = (string)cp.AxtriaARSnT__Territory_ID__c + (string)cp.AxtriaARSnT__Team_ID__c   ;
            
            if(string.IsBlank(cp.AxtriaARSnT__Territory_ID__c)){
                cp.AxtriaARSnT__isError__c = true;
                cp.SalesIQ_Error_Message__c+='Position Code is blank.';
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Position__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Position Code is blank';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
            }
            
            else if(!string.IsBlank(cp.AxtriaARSnT__Territory_ID__c) && posCodeIdMap.containsKey(keyClientCodeTeamName)){
              String activePosID = posCodeIdMap.get(keyClientCodeTeamName);          
                cp.AxtriaARSnT__Position__c = activePosID;                          //Filling lookups of Active Positions
                cp.AxtriaARSnT__Position_Team_Instance__c = posTeamInstMap.get(activePosID);        //Filling lookups of PTI
                
            }
            else if(!string.IsBlank(cp.AxtriaARSnT__Territory_ID__c) && inactivePosCodeIdMap.containsKey(keyClientCodeTeamName))
            {
              String inActivePosID = inactivePosCodeIdMap.get(keyClientCodeTeamName);      
              cp.AxtriaARSnT__Position__c = inActivePosID;                        //Filling lookups of InActive Positions
              cp.AxtriaARSnT__Position_Team_Instance__c = posTeamInstMap.get(inActivePosID);        //Filling lookups of PTI
              
              
                cp.AxtriaARSnT__isError__c = true;
                cp.SalesIQ_Error_Message__c+='Warning: Position is Inactive in system, but its lookup is filled.';
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Position__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Warning: Position is Inactive in system, but its lookup is filled';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
            }
            else if((!posCodeIdMap.containsKey(keyClientCodeTeamName)) && (!inactivePosCodeIdMap.containsKey(keyClientCodeTeamName))){
              cp.AxtriaARSnT__isError__c = true;
               cp.SalesIQ_Error_Message__c+='Either Position or its Position Team Instance does not exist.';
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Position__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Either Position or its Position Team Instance does not exist';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
              
            }
            
            
            if(!string.isBlank(cp.AxtriaARSnT__Account_ID__c) && !string.isBlank(cp.AxtriaARSnT__Territory_ID__c))
            {
              if(accountNumberPosIDtoPosAccIdMap.containsKey(cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Territory_ID__c))
              {
                cp.AxtriaARSnT__Position_Account__c = accountNumberPosIDtoPosAccIdMap.get(cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Territory_ID__c);        //Filling lookups of Accounts
              }
              else{
                cp.AxtriaARSnT__isError__c = true;
                 cp.SalesIQ_Error_Message__c+='Position Account is not present. ';
               
                    tempErrorObj = new AxtriaARSnT__Error_Object__c();
                    tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                    tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                    tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                    tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                    tempErrorObj.AxtriaARSnT__Comments__c = 'Position Account is not present.';
                    tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.AxtriaARSnT__isError__c = true;
               cp.SalesIQ_Error_Message__c+='Position or Account is Blank.';
               
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'Position or Account is Blank';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
              
            }
            if(!Test.isRunningTest()){
            if(Cycle_HCP_Productmap.containsKey(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c)){
            AxtriaARSnT__Call_Plan__c errorCallPlan= new AxtriaARSnT__Call_Plan__c();
            errorCallPlan= Cycle_HCP_Productmap.get(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c);
            if(errorCallPlan.AxtriaARSnT__Segment__c!=cp.AxtriaARSnT__Segment__c){    
                cp.AxtriaARSnT__isError__c = true;
                cp.SalesIQ_Error_Message__c+='customer is having different segment value for same product ';               
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'customer is having different segment value for same product ';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
                
                
                errorCallPlan.AxtriaARSnT__isError__c = true;
                errorCallPlan.SalesIQ_Error_Message__c+='customer is having different segment value for same product ';      
                duplicatecallplanMap.put(errorCallPlan.id,errorCallPlan);      
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = errorCallPlan.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = errorCallPlan.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'customer is having different segment value for same product ';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = errorCallPlan.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
             }
            }
            else{
            Cycle_HCP_Productmap.put(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c,cp);
            }
            
            
            if(Cycle_HCP_Product_Positionmap.containsKey(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c+cp.AxtriaARSnT__Territory_ID__c)){
            AxtriaARSnT__Call_Plan__c errorCallPlan= new AxtriaARSnT__Call_Plan__c();
            errorCallPlan= Cycle_HCP_Product_Positionmap.get(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c+cp.AxtriaARSnT__Territory_ID__c);
            if(errorCallPlan.AxtriaARSnT__Objective__c!=cp.AxtriaARSnT__Objective__c){    
                cp.AxtriaARSnT__isError__c = true;
                cp.SalesIQ_Error_Message__c+='customer is having different objective value for same product and position.';               
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = cp.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = cp.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'customer is having different objective value for same product and position.';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = cp.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
                
                
                errorCallPlan.AxtriaARSnT__isError__c = true;
                errorCallPlan.SalesIQ_Error_Message__c+='customer is having different objective value for same product and position.';      
                duplicatecallplanMap.put(errorCallPlan.id,errorCallPlan);      
                tempErrorObj = new AxtriaARSnT__Error_Object__c();
                tempErrorObj.AxtriaARSnT__Record_Id__c  = errorCallPlan.Id;
                tempErrorObj.SalesIQ_Call_Plan_Record_id__c = errorCallPlan.Id;
                tempErrorObj.AxtriaARSnT__Object_Name__c  = 'AxtriaARSnT__Call_Plan__c';
                tempErrorObj.AxtriaARSnT__Field_Name__c = 'Account__c';
                tempErrorObj.AxtriaARSnT__Comments__c = 'customer is having different objective value for same product and position.';
                tempErrorObj.AxtriaARSnT__Team_Instance_Name__c = errorCallPlan.AxtriaARSnT__Team_ID__c ;
                errorList.add(tempErrorObj);
             }
            }
            else{
            Cycle_HCP_Product_Positionmap.put(cp.AxtriaARSnT__Cycle__c+cp.AxtriaARSnT__Account_ID__c+cp.AxtriaARSnT__Product_ID__c+cp.AxtriaARSnT__Territory_ID__c,cp);
            }
            }
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        if(duplicatecallplanmap.size() >0){
         update duplicatecallplanmap.values();
        }
        update scopeRecs ;*/

    }

    global void finish(Database.BatchableContext BC) {
       Database.executeBatch(new  Call_Plan_Data_to_PACP1(selectedTeamInstance),2000);  
    }
}