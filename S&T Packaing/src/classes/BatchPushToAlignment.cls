global with sharing class BatchPushToAlignment implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public String teamInstance;
    public Map<String, List<String>> mapSourceFieldToDestField;
    public Measure_Master__c rule;
    public String destFields;
    public String ruleId;
    public Boolean isException;

    global BatchPushToAlignment(String teamIns, String rId, String product)
    {
        List<String> fieldList =  new List<String>();
        List<String> sourceFieldList =  new List<String>();
        List<String> destFieldList =  new List<String>();
        List<String> tempList =  new List<String>();
        mapSourceFieldToDestField = new Map<String, List<String>>();

        String source_field, dest_field;
        destFields = '';
        teamInstance = teamIns;
        ruleId = rId;
        isException = false;
        rule = [Select Id, State__c, EndUserErrorMsg__c, ErrorDetail__c from Measure_Master__c where id = :ruleId WITH SECURITY_ENFORCED];
        query = 'Select Id, Physician__c, Physician_2__c, ';
        for(Source_to_Destination_Mapping__c fieldMapping : [Select Source_Object_Field__c, Destination_Object_Field__c, Load_Type__c from Source_to_Destination_Mapping__c where Team_Instance__c = :teamInstance and Product__c = :product and Destination_Object_Field__c != '' and Source_Object_Field__c != '' and (Load_Type__c = '' or Load_Type__c = null) WITH SECURITY_ENFORCED])
        {
            source_field = fieldMapping.Source_Object_Field__c;
            dest_field = fieldMapping.Destination_Object_Field__c;
            if(!mapSourceFieldToDestField.containsKey(source_field))
            {
                tempList = new List<String>();
            }
            else
            {
                tempList = mapSourceFieldToDestField.get(source_field);
            }
            tempList.add(dest_field);
            mapSourceFieldToDestField.put(source_field, tempList);
            if(!destFieldList.contains(dest_field))
            {
                destFields += (dest_field + ', ') ;
                destFieldList.add(dest_field);
            }
            if(!sourceFieldList.contains(source_field))
            {
                sourceFieldList.add(source_field);
                query += source_field + ', ';
            }

        }


        query = query.removeEnd(', ');
        query += ' from Account_Compute_Final__c';
        query += ' where Measure_Master__c =:ruleId and Physician__c!= null and Physician_2__c != null WITH SECURITY_ENFORCED';
        SnTDMLSecurityUtil.printDebugMessage('query===' + query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account_Compute_Final__c> scope)
    {
        Set<ID> setAccountACF = new Set<ID>();
        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
        Map<Id, AxtriaSalesIQTM__Position_Account__c> mapPosAcc = new Map<Id, AxtriaSalesIQTM__Position_Account__c>();
        List<AxtriaSalesIQTM__Position_Account__c> posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
        for(Account_Compute_Final__c acf : scope)
        {
            setAccountACF.add(acf.Physician_2__c);
        }
        String queryPA = 'Select ID, ';
        queryPA += destFields;
        queryPA = queryPA.removeEnd(', ');
        queryPA += ' from AxtriaSalesIQTM__Position_Account__c';
        queryPA += ' where AxtriaSalesIQTM__Team_Instance__c =:teamInstance';
        queryPA += ' and AxtriaSalesIQTM__Account__c in :setAccountACF WITH SECURITY_ENFORCED';

        try
        {
            posAccList = Database.query(queryPA);

            for(AxtriaSalesIQTM__Position_Account__c pa : posAccList)
            {
                mapPosAcc.put(pa.ID, pa);
            }
            posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
            for(Account_Compute_Final__c acf : scope)
            {
                posAcc = mapPosAcc.get(acf.Physician__c);
                for(String source_field : mapSourceFieldToDestField.keySet())
                {
                    for(String dest_field : mapSourceFieldToDestField.get(source_field))
                    {
                        posAcc.put(dest_field, acf.get(source_field));
                    }
                }
                posAccList.add(posAcc);
            }
            if(posAccList.size() > 0)
            {
                //update posAccList;
                SnTDMLSecurityUtil.updateRecords(posAccList, 'BatchPushToAlignment');
            }

        }
        catch(Exception e)
        {
            isException = true;
            SnTDMLSecurityUtil.printDebugMessage('inside exception----');
            rule.State__c = 'Error: Publish to Alignment';
            rule.is_pushed_to_alignment__c = false;
            rule.EndUserErrorMsg__c = 'Please contact Service admin.';
            String errMsg = e.getTypeName() + ' / ErrorMessage :: ' + e.getMessage() + ' / Cause:: ' + e.getCause() + ' / at  ' + e.getLineNumber();
            SnTDMLSecurityUtil.printDebugMessage('ErrorMessage ' + errMsg);
            if(errMsg.length() > 130000)
            {
                errMsg = errMsg.substring(0, 129998);
            }
            rule.ErrorDetail__c = errMsg;
            //update rule;
            SnTDMLSecurityUtil.updateRecords(rule, 'BatchPushToAlignment');
        }



    }

    global void finish(Database.BatchableContext BC)
    {


        SnTDMLSecurityUtil.printDebugMessage('===========IS Exception ==========:' + isException);
        if(!isException)
        {
            Measure_Master__c rule = [SELECT Id, is_pushed_to_alignment__c, is_push_to_alignment_allowed__c, State__c, Team__r.Name, Brand_Lookup__c, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__r.Name, Measure_Type__c FROM Measure_Master__c WHERE Id = :ruleId WITH SECURITY_ENFORCED LIMIT 1];

            rule.is_pushed_to_alignment__c = true;
            rule.is_push_to_alignment_allowed__c = true;
            rule.State__c = 'Published to Alignment';
            rule.EndUserErrorMsg__c = '';
            rule.ErrorDetail__c = '';
            //update rule;
            SnTDMLSecurityUtil.updateRecords(rule, 'BatchPushToAlignment');

            list<Measure_Master__c> rules = [SELECT Id, is_push_to_alignment_allowed__c from Measure_Master__c where id != :ruleId AND Team_Instance__c = :rule.Team_Instance__c AND Brand_Lookup__r.Name = : rule.Brand_Lookup__r.Name AND Measure_Type__c = :rule.Measure_Type__c WITH SECURITY_ENFORCED];
            for(Measure_Master__c r : rules)
            {
                r.is_push_to_alignment_allowed__c = false;
            }
            //update rules;
            SnTDMLSecurityUtil.updateRecords(rules, 'BatchPushToAlignment');
        }

    }
}