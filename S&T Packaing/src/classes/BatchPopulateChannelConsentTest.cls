/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 15th August 2020
@description  : Test class for BatchPopulateChannelConsent
@Revision(s)  : v1.0
**********************************************************************************************/
@isTest
private class BatchPopulateChannelConsentTest 
{
	//Data added via UI based test
    public static TestMethod void testMethod1() 
    {
    	String className = 'BatchPopulateChannelConsentTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Team_Instance__c = null;
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Channel Info record
		Channel_Info__c ci = new Channel_Info__c();
		ci.Country__c = countr.Id;
		ci.Channel_Name__c = 'Email';
		SnTDMLSecurityUtil.insertRecords(ci,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Channel_Name__c = '';
		rec1.Change_Request__c = cr.Id;
		tempObjList.add(rec1);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Channel_Name__c = 'Email';
		rec2.AccountNumber__c = '';
		rec2.Channel_Consent__c = 'YES';
		rec2.Change_Request__c = cr.Id;
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Channel_Name__c = 'Email';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Change_Request__c = cr.Id;
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Channel_Name__c = 'Email';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc1';
		rec4.Channel_Consent__c = '';
		rec4.Change_Request__c = cr.Id;
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Channel_Name__c = 'Email';
		rec5.Product_Code__c = 'PROD1';
		rec5.AccountNumber__c = 'Acc2';
		rec5.Change_Request__c = cr.Id;
		rec5.Channel_Consent__c = 'YES';
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Channel_Name__c = 'Email';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD2';
		rec6.Change_Request__c = cr.Id;
		rec6.Channel_Consent__c = 'YES';
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Channel_Name__c = 'Email2';
		rec7.Workspace__c = 'WS';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Channel_Consent__c = 'YES';
		rec7.Change_Request__c = cr.Id;
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Channel_Name__c = 'Email';
		rec8.Workspace__c = 'WS';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Channel_Consent__c = 'YES';
		rec8.Change_Request__c = cr.Id;
		tempObjList.add(rec8);

		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

		//Create MCCP Unique Channel Preference data
		MCCP_DataLoad__c data1 = new MCCP_DataLoad__c();
		data1.RecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Channel Consent').getRecordTypeId();
		data1.Product__c = pcc.Id;
		data1.Country__c = countr.Id;
		data1.Channel_Name__c = 'Email';
		data1.ExternalID__c = data1.RecordTypeID+'_'+countr.Id+'_'+data1.Channel_Name__c+'_'+pcc.Id;
		SnTDMLSecurityUtil.insertRecords(data1,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateChannelConsent(cr.Id,countr.Id),2000);

        System.Test.stopTest();
    }

    //Data added directly in Temp obj based test
    public static TestMethod void testMethod2() 
    {
    	String className = 'BatchPopulateChannelConsentTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        pcc.Team_Instance__c = null;
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Channel Info record
		Channel_Info__c ci = new Channel_Info__c();
		ci.Country__c = countr.Id;
		ci.Channel_Name__c = 'Email';
		SnTDMLSecurityUtil.insertRecords(ci,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Channel_Name__c = '';
		rec1.Change_Request__c = cr.Id;
		rec1.Status__c = 'New';
		rec1.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec1);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Channel_Name__c = 'Email';
		rec2.AccountNumber__c = '';
		rec2.Channel_Consent__c = 'YES';
		rec2.Change_Request__c = cr.Id;
		rec2.Status__c = 'New';
		rec2.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Channel_Name__c = 'Email';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Channel_Consent__c = '';
		rec3.Change_Request__c = cr.Id;
		rec3.Status__c = 'New';
		rec3.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Channel_Name__c = 'Email';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc1';
		rec4.Channel_Consent__c = 'YES';
		rec4.Change_Request__c = cr.Id;
		rec4.Status__c = 'New';
		rec4.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Channel_Name__c = 'Email';
		rec5.Product_Code__c = 'PROD1';
		rec5.AccountNumber__c = 'Acc2';
		rec5.Change_Request__c = cr.Id;
		rec5.Channel_Consent__c = 'YES';
		rec5.Status__c = 'New';
		rec5.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Channel_Name__c = 'Email';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD2';
		rec6.Change_Request__c = cr.Id;
		rec6.Channel_Consent__c = 'YES';
		rec6.Status__c = 'New';
		rec6.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Channel_Name__c = 'Email2';
		rec7.Workspace__c = 'WS';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Channel_Consent__c = 'YES';
		rec7.Change_Request__c = cr.Id;
		rec7.Status__c = 'New';
		rec7.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Channel_Name__c = 'Email';
		rec8.Workspace__c = 'WS';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Channel_Consent__c = 'YES';
		rec8.Change_Request__c = cr.Id;
		rec8.Status__c = 'New';
		rec8.Object_Name__c = 'MCCP Channel Consent';
		tempObjList.add(rec8);
		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateChannelConsent(cr.Id,'MCCP Channel Consent',countr.Id),2000);

        System.Test.stopTest();
    }
}