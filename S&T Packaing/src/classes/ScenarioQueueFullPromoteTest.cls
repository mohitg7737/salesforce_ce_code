@isTest
public class ScenarioQueueFullPromoteTest{
   static testmethod void testScenarioQueueFullPromote(){
       ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
       String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
       List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
       System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
       
       AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
       OM.Name='Dummy1';
       OM.AxtriaSalesIQTM__Org_Level__c='Global';
       OM.Marketing_Code__c='ES';
       OM.AxtriaSalesIQTM__Parent_Country_Level__c=TRUE;
       insert OM;
       
       AxtriaSalesIQTM__Country__c CON = new AxtriaSalesIQTM__Country__c();
       CON.AxtriaSalesIQTM__Country_Code__c='ES';
       CON.AxtriaSalesIQTM__Status__c='ACTIVE';
       CON.AxtriaSalesIQTM__Parent_Organization__c=OM.id;
       insert CON;

       AxtriaSalesIQTM__Workspace__c WK = new AxtriaSalesIQTM__Workspace__c();
       WK.Name='W1';
       WK.AxtriaSalesIQTM__Workspace_Start_Date__c=system.today().adddays(-10);
       
       WK.AxtriaSalesIQTM__Workspace_Description__c='test class for Scenario Queue Trigger';
       insert WK;
       
       AxtriaSalesIQTM__Team__c TN = new AxtriaSalesIQTM__Team__c();
       TN.Name='ABC';
       TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today()-1;
       TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today();
       TN.AxtriaSalesIQTM__Alignment_Type__c='Account';
       TN.AxtriaSalesIQTM__Team_Code__c='TE01111';
       TN.AxtriaSalesIQTM__Country__c=CON.id;
       TN.AxtriaSalesIQTM__Type__c='BASE';
       insert TN;
       
       AxtriaSalesIQTM__Scenario__c SS = new AxtriaSalesIQTM__Scenario__c();
       SS.AxtriaSalesIQTM__Workspace__c=WK.id;
       insert SS;
       
       AxtriaSalesIQTM__Team_Instance__c  TI = new AxtriaSalesIQTM__Team_Instance__c ();
       TI.Name='ESABCD';
       TI.AxtriaSalesIQTM__Team__c=TN.id;
       
       insert TI;
       
       set<Id> teamInstIds = new set<Id>();     
       teamInstIds.add(TN.id);
       
       
       Schema.DescribeFieldResult Pickvalue= AxtriaSalesIQTM__Position__c.Sales_Team_Attribute_MS__c.getDescribe();
       List<Schema.PicklistEntry> PickListValue = Pickvalue.getPicklistValues();

       
       AxtriaSalesIQTM__Position__c POS = new AxtriaSalesIQTM__Position__c();
       POS.Name='Position1';
       POS.AxtriaSalesIQTM__Team_iD__c=TN.id;
       POS.Channel_AZ__c='test1';
       POS.Position_Description__c='heyyyy';
     //POS.Sales_Team_Attribute_MS__c='BG_Other';
       POS.Sales_Team_Attribute__c=String.valueof(PickListValue[0].getValue());
       insert POS;
       
       AxtriaSalesIQTM__Geography__c G = new AxtriaSalesIQTM__Geography__c();
       G.Geo_Level__c='1';
       G.Country_Code__c='ES';
       G.AxtriaSalesIQTM__Zip_Name__c='Minibrick';
       G.Geo_Code__c='AB';
       
       insert G;
       
       AxtriaSalesIQTM__Position_Geography__c PG = new AxtriaSalesIQTM__Position_Geography__c();
       PG.AxtriaSalesIQTM__Geography__c = G.id;
       PG.AxtriaSalesIQTM__Position__c = POS.id;
       PG.AxtriaSalesIQTM__Effective_End_Date__c=system.today().adddays(+2);
       PG.AxtriaSalesIQTM__Effective_Start_Date__c=system.today().adddays(-1);
       PG.AxtriaSalesIQTM__Team_Instance__c=TI.Id;
       insert PG;
       

       

       
       list<AxtriaSalesIQTM__Scenario__c> selectedSceanrio = new list<AxtriaSalesIQTM__Scenario__c>();
       selectedSceanrio = [Select Id,AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Scenario_Name__c,AxtriaSalesIQTM__Rule_Execution_Status__c, AxtriaSalesIQTM__Promote_Mode__c 
       FROM AxtriaSalesIQTM__Scenario__c where AxtriaSalesIQTM__Team_Instance__c =:teamInstIds];
       
       for(integer i=0;i<selectedSceanrio.size();i++)
       {                                                
         If(selectedSceanrio[i].AxtriaSalesIQTM__Scenario_Stage__c == 'Live' && selectedSceanrio[i].AxtriaSalesIQTM__Rule_Execution_Status__c =='Success')
         {
             /* AxtriaSalesIQTM__Activity_Log__c sobj = new AxtriaSalesIQTM__Activity_Log__c();
            sobj.Name =selectedSceanrio[i].AxtriaSalesIQTM__Scenario_Name__c;                     
            sobj.AxtriaSalesIQTM__Execution_Mode__c = 'Run Full and Promote';                           
            sobj.AxtriaSalesIQTM__Priority__c = 1;                                                 
            sobj.RecordTypeId ='0121r000000VixLAAS';                                                                                               
            sobj.AxtriaSalesIQTM__Job_Status__c = 'Queued';                                          
            sobj.AxtriaSalesIQTM__Scenario__c = selectedSceanrio[i].Id;                                   
          
            insert sobj;*/
            
            AxtriaSalesIQTM__Scenario__c scenario = new AxtriaSalesIQTM__Scenario__c(
                id = selectedSceanrio[i].Id,
                AxtriaSalesIQTM__Rule_Execution_Status__c = 'Queued',
                AxtriaSalesIQTM__Promote_Mode__c = 'Run Full and Promote'       
                );
            update scenario;
        }
    } 
    
    
    
    
}



}