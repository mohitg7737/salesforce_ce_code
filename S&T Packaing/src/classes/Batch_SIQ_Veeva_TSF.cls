global with sharing class Batch_SIQ_Veeva_TSF implements Database.Batchable < sObject > , Database.Stateful 
{
    public Integer recsTotal = 0;
    public List < temp_Obj__c > processedTempObjList;
    public Boolean flag = true;
    public Integer recsProcessed = 0;
    public String batchName;
    public String changeReqStatus;
    public List < AxtriaSalesIQTM__Change_Request__c > changeRequestCreation;
    public string query;
    public string Account_Territory;
    public string teamInstance;
    public string qname;
    public String changeReqID;
    
    public Boolean flagValue = false;
    public list < AxtriaSalesIQTM__Team_Instance__c > teaminst = new list < AxtriaSalesIQTM__Team_Instance__c > ();
    list < SIQ_Veeva_TSF_vod_Inbound__c > Veeva_TSF_ListUpdate = new list < SIQ_Veeva_TSF_vod_Inbound__c > ();
    
    public Boolean teamInstExists;
    
    global Batch_SIQ_Veeva_TSF(String TIname, String ids, Boolean flag) 
    {
    
        teamInstance = TIname;
        changeReqID = ids;
        flagValue = flag;
        System.debug('in side constructor TIname ----->'+teamInstance);
        System.debug('in side constructor TIname ----->'+changeReqID);
        System.debug('in side constructor ----->'+flagValue);
    
        teaminst = [select id from AxtriaSalesIQTM__Team_Instance__c where Name=: teamInstance];
        teamInstExists = teaminst.size()>0 ? true : false;
        changeRequestCreation = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = :ids];
        qname ='';
        System.debug('------------teamInstExists---->'+teamInstExists);
        
        query = 'Select Id, AccountNumber__c,Team_Instance_Text__c,Position_Code__c,Accessibility_Range__c,  Team_Instance_Name__c, isError__c,Status__c FROM temp_Obj__c  where Status__c = \'New\'  and Object__c = \'Accessibility\' and Change_Request__c=:changeReqID  WITH SECURITY_ENFORCED';
    }
    
     global Batch_SIQ_Veeva_TSF(String TIname, String ids) 
    {
    
        teamInstance = TIname;
        changeReqID = ids;
        
        System.debug('in side constructor TIname ----->'+teamInstance);
        System.debug('in side constructor TIname ----->'+changeReqID);
        System.debug('in side constructor ----->'+flagValue);
    
        teaminst = [select id from AxtriaSalesIQTM__Team_Instance__c where Name=: teamInstance];
        teamInstExists = teaminst.size()>0 ? true : false;
        changeRequestCreation = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = :ids];
        qname ='';
        System.debug('------------teamInstExists---->'+teamInstExists);
        
        query = 'Select Id, AccountNumber__c,Team_Instance_Text__c,Position_Code__c,Accessibility_Range__c,  Team_Instance_Name__c, isError__c,Status__c FROM temp_Obj__c  where Status__c = \'New\'  and Object__c = \'Accessibility\' and Change_Request__c=:changeReqID and Team_Instance_Text__c =:teamInstance  WITH SECURITY_ENFORCED';
   
   
     
    }

    global Database.Querylocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List < temp_Obj__c > Scope) 
    {
        Veeva_TSF_ListUpdate = new List < SIQ_Veeva_TSF_vod_Inbound__c > ();
        processedTempObjList = new List < temp_Obj__c > ();
        recsTotal += scope.size();
        set <string>  uniqueids = new set<string>();
        System.debug('------------recsTotal line no 44---->'+recsTotal);
        try 
        {
            SnTDMLSecurityUtil.printDebugMessage('teaminst.size()--'+teaminst.size());
            
            Set<String> accSet = new Set<String>();
            Set<String> posCodeSet = new Set<String>();

            for(temp_Obj__c tempObj : scope)
            {
                if(!teamInstExists)
                {   
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Incorrect Team Instance present';
                    tempObj.SalesIQ_Error_Message__c = 'Incorrect Team Instance present';
                    processedTempObjList.add(tempObj);
                }
                else if(String.isBlank(tempObj.AccountNumber__c) || String.isBlank(tempObj.Position_Code__c))
                {   
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Mandatory field missing or Incorrect';
                    tempObj.SalesIQ_Error_Message__c = 'Mandatory field missing or Incorrect';
                    processedTempObjList.add(tempObj);
                   
                }
                else
                {   
                    
                    accSet.add(tempObj.AccountNumber__c);
                    posCodeSet.add(tempObj.Position_Code__c);
                    
                }
                
            }
            
            if(teamInstExists)
            {  
                List<Account> listAccs = [select id,AccountNumber from Account where AccountNumber in :accSet];
                accSet.clear();
                if(!listAccs.isEmpty())
                {
                    for(Account acc : listAccs){
                        accSet.add(acc.AccountNumber);
                    }
                }
                
                List<AxtriaSalesIQTM__Position__c> listPos = [select AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :posCodeSet and AxtriaSalesIQTM__Team_Instance__r.Name = :teamInstance];
                posCodeSet.clear();
                if(!listAccs.isEmpty())
                {
                    for(AxtriaSalesIQTM__Position__c pos : listPos){
                        posCodeSet.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
                    }
                }
                
                    for(temp_Obj__c tempObj : Scope)
                    {
                        if(!String.isBlank(tempObj.AccountNumber__c) && !String.isBlank(tempObj.Position_Code__c))
                        {
                            uniqueids.add(tempObj.AccountNumber__c + '_' + tempObj.Position_Code__c);  //01
                            system.debug('-----uniquePrids------->'+uniqueids);
                        }
                    }
                
                    Set<string> Strid = new Set<String>();
                      Set<string> uniqueTempObjSet = new Set<String>();
                      
                List<SIQ_Veeva_TSF_vod_Inbound__c> Veevauniqlist = [Select id, SIQ_Veeva_External_ID__c From SIQ_Veeva_TSF_vod_Inbound__c where SIQ_Veeva_External_ID__c in: uniqueids];   //01
                 for(SIQ_Veeva_TSF_vod_Inbound__c TSF :Veevauniqlist){
                     Strid.add(TSF.SIQ_Veeva_External_ID__c);
                 }
                 
                for (temp_Obj__c tempObj : scope) 
                {   
                    System.debug('tempObj' +tempObj);
                    if(tempObj.Status__c!='Rejected') 
                    {   
                        Account_Territory = tempObj.AccountNumber__c + '_' + tempObj.Position_Code__c;
                        if(accSet.size()>0 && posCodeSet.size()>0 && accSet.contains(tempObj.AccountNumber__c) && posCodeSet.contains(tempObj.Position_Code__c))
                        {
                            
                            if(!Strid.contains(Account_Territory) && !uniqueTempObjSet.contains(Account_Territory)){  
                             
                                SIQ_Veeva_TSF_vod_Inbound__c TFC = new SIQ_Veeva_TSF_vod_Inbound__c();
                                TFC.SIQ_Veeva_Account_vod__c = tempObj.AccountNumber__c;
                                TFC.SIQ_Veeva_Territory_vod__c = tempObj.Position_Code__c;
                                TFC.SIQ_Veeva_External_ID__c = Account_Territory;
                                TFC.SIQ_Veeva_Accessibility_AZ_EU__c = tempObj.Accessibility_Range__c;

                                tempObj.status__c = 'Processed';
                                tempObj.isError__c = false;
                                processedTempObjList.add(tempObj);
                                Veeva_TSF_ListUpdate.add(TFC);
                                uniqueTempObjSet.add(Account_Territory);
                            }
                            else 
                            {
                                tempObj.status__c = 'Rejected';
                                tempObj.isError__c = true;
                                tempObj.Error_Message__c = 'Duplicate record already exists';
                                tempObj.SalesIQ_Error_Message__c = 'Duplicate record already exists';
                                processedTempObjList.add(tempObj);
                            }
                        }  
                            
                        else 
                        {
                            tempObj.status__c = 'Rejected';
                            tempObj.isError__c = true;
                            tempObj.Error_Message__c = 'Mandatory field missing or Incorrect';
                            tempObj.SalesIQ_Error_Message__c = 'Mandatory field missing or Incorrect';
                            processedTempObjList.add(tempObj);
                        }   
                    } 
                }
            }
            
            if (!Veeva_TSF_ListUpdate.isEmpty()) {
                
                SnTDMLSecurityUtil.printDebugMessage('Veeva_TSF_ListUpdate size--' + Veeva_TSF_ListUpdate.size());
                System.debug('final record size ---------->'+ Veeva_TSF_ListUpdate.size());
                
                SnTDMLSecurityUtil.insertRecords(Veeva_TSF_ListUpdate, 'Batch_SIQ_Veeva_TSF');
                
                
           /*     Database.SaveResult[] ds = Database.insert(Veeva_TSF_ListUpdate, false);
                for (Database.SaveResult d: ds) {
                    if (d.isSuccess()) {
                        recsProcessed++;
                    } else {
                        flag = false;
                        for (Database.Error err: d.getErrors()) {
                            SnTDMLSecurityUtil.printDebugMessage('The following error has occurred.');
                            SnTDMLSecurityUtil.printDebugMessage(err.getStatusCode() + ': ' + err.getMessage());
                            SnTDMLSecurityUtil.printDebugMessage('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }*/
            }
            if (!processedTempObjList.isEmpty()) {
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList--' + processedTempObjList);
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList size--' + processedTempObjList.size());

                SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, processedTempObjList);
                List < temp_Obj__c > tempList = securityDecision.getRecords();
                SnTDMLSecurityUtil.updateRecords(tempList, batchName);

                if (!securityDecision.getRemovedFields().isEmpty()) {
                    throw new SnTException(securityDecision, 'temp_Obj__c', SnTException.OperationType.U);
                }
            }
        } catch (Exception e) {
            SnTDMLSecurityUtil.printDebugMessage(batchName + ' : Error in execute()--' + e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--' + e.getStackTraceString());
        }
    }

    global void finish(Database.BatchableContext BC) {
        SnTDMLSecurityUtil.printDebugMessage(batchName + ' : constructor invoked--');

        Boolean noJobErrors;
        if (changeReqID != null && changeReqID != '') {
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.Id = changeReqID;
            changeRequest.Records_Updated__c = recsProcessed;
            if (changeRequestCreation.size() > 0 && changeRequestCreation[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend') {
                changeRequest.Records_Created__c = recsTotal;
            }
            SnTDMLSecurityUtil.updateRecords(changerequest, batchName);

            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

             if(flag && noJobErrors)
              {
              //BU_Response_TSF_Update_Utility_New  // Added this batch
                  BU_Response_TSF_Update_Utility_New batchCall = new BU_Response_TSF_Update_Utility_New(teamInstance,qname,changeReqID);
                  Database.executeBatch(batchCall,500);
              }
              
        }
        
    }

}