global class BatchPosAccAddedDroppedCount implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    
     public String Country;
     public String query;
     public String batchID;
     global DateTime lastjobDate=null;
     public Integer recordsProcessed=0;


     global BatchPosAccAddedDroppedCount (String Country){
          this.Country = Country;
          System.debug('Country is ' + Country);

          List<AxtriaARSnT__Scheduler_Log__c> schLog = new List<AxtriaARSnT__Scheduler_Log__c>();
          schLog = [Select Id,AxtriaARSnT__Created_Date__c,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c= 'Create Position Account Records Count' and AxtriaARSnT__Job_Status__c='Successful'  Order By AxtriaARSnT__Created_Date2__c desc];
          if(schLog.size() > 0)
          {
          lastjobDate=schLog[0].AxtriaARSnT__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
          }
          System.debug('last job'+lastjobDate);

          AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
          sJob.AxtriaARSnT__Job_Name__c = 'Create Position Account Records Count';
          sJob.AxtriaARSnT__Job_Status__c = 'Failed';
          sJob.AxtriaARSnT__Job_Type__c='Inbound';
          sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
          insert sJob;
          batchID = sJob.Id;

          query = 'SELECT Id, Name,PA_Active__c,PA_Inactive__c  FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\'and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country';
          
     }

     global Database.QueryLocator start(Database.BatchableContext BC) {
          system.debug('Position Account Records:::::::::::::' + query);        
          return Database.getQueryLocator(query);
     }

     global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position__c> posList){     

          System.debug('posList size is ' + posList.size());
          Set<String> posIdSet = new Set<String>();

          for(AxtriaSalesIQTM__Position__c position: posList){
               posIdSet.add(position.Id);
          }
          System.debug('posIdSet size is ' + posIdSet.size());

          List<AggregateResult> activePosCount = new List<AggregateResult>();
          List<AggregateResult> inActivePosCount = new List<AggregateResult>();
          if(lastjobDate != null){
               activePosCount = [SELECT AxtriaSalesIQTM__Position__c, Count(Id) con from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and AxtriaSalesIQTM__Assignment_Status__c = 'Active' and LastModifiedDate >:lastjobDate group by AxtriaSalesIQTM__Position__c];
               
               inActivePosCount = [SELECT AxtriaSalesIQTM__Position__c, Count(Id) con from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and AxtriaSalesIQTM__Assignment_Status__c = 'InActive' and LastModifiedDate >:lastjobDate group by AxtriaSalesIQTM__Position__c];
          } else{
               activePosCount = [SELECT AxtriaSalesIQTM__Position__c, Count(Id) con from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and AxtriaSalesIQTM__Assignment_Status__c = 'Active' group by AxtriaSalesIQTM__Position__c];
               
               inActivePosCount = [SELECT AxtriaSalesIQTM__Position__c, Count(Id) con from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and AxtriaSalesIQTM__Assignment_Status__c = 'InActive' group by AxtriaSalesIQTM__Position__c];
          }
          System.debug('ActivePosCount size is ' + activePosCount.size());
          System.debug('InActivePosCount size is ' + inActivePosCount.size());

          map<String,Integer> Active_Map = New map<String,Integer>();
          map<String,Integer> InActive_Map = New map<String,Integer>();   
     
          for(AggregateResult aggRes : activePosCount){
               Active_Map.put((String)aggRes.get('AxtriaSalesIQTM__Position__c'), (Integer)aggRes.get('con'));
          }
          System.debug('Active_Map size is ' + Active_Map.size());
          for(AggregateResult aggRes : inActivePosCount){
               InActive_Map.put((String)aggRes.get('AxtriaSalesIQTM__Position__c'), (Integer)aggRes.get('con'));
          }
          System.debug('Inactive_Map size is ' + InActive_Map.size());

          for(AxtriaSalesIQTM__Position__c pos : posList){
               if(Active_Map.get(pos.ID) != null){
                    pos.PA_Active__c = Active_Map.get(pos.ID); 
               }
               if(InActive_Map.get(pos.ID) != null){
                    pos.PA_Inactive__c  = InActive_Map.get(pos.ID); 
               }
          }
          System.debug('PosList size is ' + posList.size());
          update posList;  
            
          recordsProcessed +=posList.size();
          System.debug('recordsProcessed value is' + recordsProcessed);
     }
    
     global void execute(SchedulableContext sc){
          Database.executeBatch(new BatchPositionAccountCount1(Country), 200);
     }

     global void finish(Database.BatchableContext BC) {
          AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
          sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
          sJob.AxtriaARSnT__Job_Status__c='Successful';
          update sJob;
     }    
   }