@isTest
public class CallPlanSummaryTriggerHandler_Test {
    @istest static void CallPlanSummaryTriggerHandler_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='No';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        insert acc;
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'CallPlanSummaryTrigger')};
                
                
                
                AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.Segment_Values__c = 'A,B,C,D';
        insert teamins ;
        
        AxtriaSalesIQTM__Organization_Master__c omc = TestDataFactory.createOrganizationMaster();
        insert omc;
        
        AxtriaSalesIQTM__Country__c cc = TestDataFactory.createCountry(omc);
        cc.AxtriaSalesIQTM__Parent_Organization__c = omc.id;
        insert cc;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,countr);
        pcc.Country_Lookup__c = cc.id;
        pcc.team_instance__c = teamins.id;
        insert pcc;
        
        
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamins);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamins.id;
        insert measureMaster;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        AxtriaSalesIQTM__Position__c papos1 = TestDataFactory.createPosition(team,teamins);
        //papos1.AxtriaSalesIQTM__Parent_Position__c = papos1.id;
        insert papos1;
        AxtriaSalesIQTM__Position__c papos2 = TestDataFactory.createPosition(team,teamins);
        papos2.AxtriaSalesIQTM__Parent_Position__c = papos1.id;
        insert papos2;
        AxtriaSalesIQTM__Position__c papos3 = TestDataFactory.createPosition(team,teamins);
        papos3.AxtriaSalesIQTM__Parent_Position__c = papos2.id;
        insert papos3;
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = papos3.id;
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pp1 = new Product_Priority__c();
        pp1.CurrencyIsoCode = 'USD';
        pp1.Product__c = pcc.id;
        pp1.Speciality_ID__c='testSpecs';
        pp1.priority__c='P2';
        insert pp1;
        AxtriaSalesIQTM__Position_Product__c posprod1 = TestDataFactory.createPositionProduct(teamins, papos1, pcc);
        insert posprod1;
        AxtriaSalesIQTM__Position_Product__c posprod2 = TestDataFactory.createPositionProduct(teamins, papos2, pcc);
        insert posprod2;
        AxtriaSalesIQTM__Position_Product__c posprod3 = TestDataFactory.createPositionProduct(teamins, papos3, pcc);
        insert posprod3;
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins, pos, pcc);
        insert posprod;
           /*   Area_Line_Junction__c parentarea1 = new Area_Line_Junction__c();
        parentarea1.Speciality__c = '-';
        parentarea1.Is_Rep_Refinement_Report__c = false;
        parentarea1.Area__c = papos1.id;
        parentarea1.Team_Instance__c = teamins.id;
        parentarea1.Brand_Name__c = pcc.Name;
        parentarea1.Number_of_HCP_Post_fine_tuning__c = 0;
        parentarea1.Number_of_calls_post_fine_tuning__c     = 0;
        parentarea1.Number_of_HCP_Targeted__c = 0;
        parentarea1.A__c = 0;
        parentarea1.A1__c = 0;
        parentarea1.B__c = 0;
        parentarea1.C__c = 0;
        parentarea1.D__c = 0;
        parentarea1.ND__c = 0;
        parentarea1.CL__c = 0;
        parentarea1.MDT__c = 0;
        parentarea1.Calls_A__c = 0;
        parentarea1.Calls_A1__c = 0;
        parentarea1.Calls_B__c = 0;
        parentarea1.Calls_C__c = 0;
        parentarea1.Calls_D__c = 0;
        parentarea1.Calls_CL__c = 0;
        parentarea1.Calls_ND__c = 0;
        parentarea1.Calls_MDT__c = 0;
        parentarea1.Total_Calls__c = 0;
        
        parentarea1.Proposed_TCF_A__c = 0;
        parentarea1.Proposed_TCF_A1__c=0;
        parentarea1.Proposed_TCF_B__c=0;
        parentarea1.Proposed_TCF_C__c=0;
        parentarea1.Proposed_TCF_CL__c=0;
        parentarea1.Proposed_TCF_D__c=0;
        parentarea1.Proposed_TCF_MDT__c=0;
        parentarea1.Proposed_TCF_ND__c=0;
        parentarea1.TCF_A_Total__c=0;
        parentarea1.TCF_B_Total__c=0;
        parentarea1.TCF_C_Total__c=0;
        parentarea1.TCF_CL_Total__c=0;
        parentarea1.TCF_D_Total__c=0;
        parentarea1.TCF_MDT_Total__c=0;
        parentarea1.TCF_ND_Total__c=0;
        parentarea1.TCF_A1_Total__c=0;
        parentarea1.V__c = 0;
        parentarea1.Calls_V__c = 0;
        parentarea1.Proposed_TCF_V__c=0;
        parentarea1.TCF_V_Total__c=0;
        parentarea1.Original_Adoption_val__c = 0;
        parentarea1.Original_Potential_Val__c = 0;
        parentarea1.Updated_Adoption_Val__c = 0;
        parentarea1.Updated_Potential_Val__c = 0;
        parentarea1.Add_HCP__c = 0;
        parentarea1.Drop_HCP__c = 0;
        insert parentarea1;
              Area_Line_Junction__c parentarea2 = new Area_Line_Junction__c();
        parentarea2.Speciality__c = '-';
        parentarea2.Is_Rep_Refinement_Report__c = false;
        parentarea2.Area__c = papos2.id;
        parentarea2.Team_Instance__c = teamins.id;
        parentarea2.Brand_Name__c = pcc.Name;
        parentarea2.Number_of_HCP_Post_fine_tuning__c = 0;
        parentarea2.Number_of_calls_post_fine_tuning__c     = 0;
        parentarea2.Number_of_HCP_Targeted__c = 0;
        parentarea2.A__c = 0;
        parentarea2.A1__c = 0;
        parentarea2.B__c = 0;
        parentarea2.C__c = 0;
        parentarea2.D__c = 0;
        parentarea2.ND__c = 0;
        parentarea2.CL__c = 0;
        parentarea2.MDT__c = 0;
        parentarea2.Calls_A__c = 0;
        parentarea2.Calls_A1__c = 0;
        parentarea2.Calls_B__c = 0;
        parentarea2.Calls_C__c = 0;
        parentarea2.Calls_D__c = 0;
        parentarea2.Calls_CL__c = 0;
        parentarea2.Calls_ND__c = 0;
        parentarea2.Calls_MDT__c = 0;
        parentarea2.Total_Calls__c = 0;
        
        parentarea2.Proposed_TCF_A__c = 0;
        parentarea2.Proposed_TCF_A1__c=0;
        parentarea2.Proposed_TCF_B__c=0;
        parentarea2.Proposed_TCF_C__c=0;
        parentarea2.Proposed_TCF_CL__c=0;
        parentarea2.Proposed_TCF_D__c=0;
        parentarea2.Proposed_TCF_MDT__c=0;
        parentarea2.Proposed_TCF_ND__c=0;
        parentarea2.TCF_A_Total__c=0;
        parentarea2.TCF_B_Total__c=0;
        parentarea2.TCF_C_Total__c=0;
        parentarea2.TCF_CL_Total__c=0;
        parentarea2.TCF_D_Total__c=0;
        parentarea2.TCF_MDT_Total__c=0;
        parentarea2.TCF_ND_Total__c=0;
        parentarea2.TCF_A1_Total__c=0;
        parentarea2.V__c = 0;
        parentarea2.Calls_V__c = 0;
        parentarea2.Proposed_TCF_V__c=0;
        parentarea2.TCF_V_Total__c=0;
        parentarea2.Original_Adoption_val__c = 0;
        parentarea2.Original_Potential_Val__c = 0;
        parentarea2.Updated_Adoption_Val__c = 0;
        parentarea2.Updated_Potential_Val__c = 0;
        parentarea2.Add_HCP__c = 0;
        parentarea2.Drop_HCP__c = 0;
        insert parentarea2;*/
        Area_Line_Junction__c parentarea = new Area_Line_Junction__c();
        parentarea.Speciality__c = '-';
        parentarea.Is_Rep_Refinement_Report__c = false;
        parentarea.Area__c = papos3.id;
        parentarea.Team_Instance__c = teamins.id;
        parentarea.Brand_Name__c = pcc.Name;
        parentarea.Number_of_HCP_Post_fine_tuning__c = 0;
        parentarea.Number_of_calls_post_fine_tuning__c     = 0;
        parentarea.Number_of_HCP_Targeted__c = 0;
        parentarea.A__c = 0;
        parentarea.A1__c = 0;
        parentarea.B__c = 0;
        parentarea.C__c = 0;
        parentarea.D__c = 0;
        parentarea.ND__c = 0;
        parentarea.CL__c = 0;
        parentarea.MDT__c = 0;
        parentarea.Calls_A__c = 0;
        parentarea.Calls_A1__c = 0;
        parentarea.Calls_B__c = 0;
        parentarea.Calls_C__c = 0;
        parentarea.Calls_D__c = 0;
        parentarea.Calls_CL__c = 0;
        parentarea.Calls_ND__c = 0;
        parentarea.Calls_MDT__c = 0;
        parentarea.Total_Calls__c = 0;
        
        parentarea.Proposed_TCF_A__c = 0;
        parentarea.Proposed_TCF_A1__c=0;
        parentarea.Proposed_TCF_B__c=0;
        parentarea.Proposed_TCF_C__c=0;
        parentarea.Proposed_TCF_CL__c=0;
        parentarea.Proposed_TCF_D__c=0;
        parentarea.Proposed_TCF_MDT__c=0;
        parentarea.Proposed_TCF_ND__c=0;
        parentarea.TCF_A_Total__c=0;
        parentarea.TCF_B_Total__c=0;
        parentarea.TCF_C_Total__c=0;
        parentarea.TCF_CL_Total__c=0;
        parentarea.TCF_D_Total__c=0;
        parentarea.TCF_MDT_Total__c=0;
        parentarea.TCF_ND_Total__c=0;
        parentarea.TCF_A1_Total__c=0;
        parentarea.V__c = 0;
        parentarea.Calls_V__c = 0;
        parentarea.Proposed_TCF_V__c=0;
        parentarea.TCF_V_Total__c=0;
        parentarea.Original_Adoption_val__c = 0;
        parentarea.Original_Potential_Val__c = 0;
        parentarea.Updated_Adoption_Val__c = 0;
        parentarea.Updated_Potential_Val__c = 0;
        parentarea.Add_HCP__c = 0;
        parentarea.Drop_HCP__c = 0;
        insert parentarea;
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Speciality__c = '-';
        area.Is_Rep_Refinement_Report__c = false;
        area.Area__c = pos.id;
        area.Team_Instance__c = teamins.id;
        area.Brand_Name__c = pcc.Name;
        area.Number_of_HCP_Post_fine_tuning__c = 0;
        area.Number_of_calls_post_fine_tuning__c     = 0;
        area.Number_of_HCP_Targeted__c = 0;
        area.A__c = 0;
        area.A1__c = 0;
        area.B__c = 0;
        area.C__c = 0;
        area.D__c = 0;
        area.ND__c = 0;
        area.CL__c = 0;
        area.MDT__c = 0;
        area.Calls_A__c = 0;
        area.Calls_A1__c = 0;
        area.Calls_B__c = 0;
        area.Calls_C__c = 0;
        area.Calls_D__c = 0;
        area.Calls_CL__c = 0;
        area.Calls_ND__c = 0;
        area.Calls_MDT__c = 0;
        area.Total_Calls__c = 0;
        
        area.Proposed_TCF_A__c = 0;
        area.Proposed_TCF_A1__c=0;
        area.Proposed_TCF_B__c=0;
        area.Proposed_TCF_C__c=0;
        area.Proposed_TCF_CL__c=0;
        area.Proposed_TCF_D__c=0;
        area.Proposed_TCF_MDT__c=0;
        area.Proposed_TCF_ND__c=0;
        area.TCF_A_Total__c=0;
        area.TCF_B_Total__c=0;
        area.TCF_C_Total__c=0;
        area.TCF_CL_Total__c=0;
        area.TCF_D_Total__c=0;
        area.TCF_MDT_Total__c=0;
        area.TCF_ND_Total__c=0;
        area.TCF_A1_Total__c=0;
        area.V__c = 0;
        area.Calls_V__c = 0;
        area.Proposed_TCF_V__c=0;
        area.TCF_V_Total__c=0;
        area.Original_Adoption_val__c = 0;
        area.Original_Potential_Val__c = 0;
        area.Updated_Adoption_Val__c = 0;
        area.Updated_Potential_Val__c = 0;
        area.Add_HCP__c = 0;
        area.Drop_HCP__c = 0;
        insert area;
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> listpositionaccountcallplan = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamins,posAccount,pp,pos);
        positionacc.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc.AxtriaSalesIQTM__Team_Instance__c =teamins.id;
        
        insert positionacc;
        listpositionaccountcallplan.add(positionacc);
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> listpositionaccountcallplan1 = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc1 = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamins,posAccount,pp,papos3);
        positionacc1.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc1.AxtriaSalesIQTM__Team_Instance__c =teamins.id;
        positionacc1.AxtriaSalesIQTM__Change_Status__c = 'Approved';
        positionacc1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        
        insert positionacc1;
        
        listpositionaccountcallplan1.add(positionacc1);
        /*
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc2 = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamins,posAccount,pp,papos2);
        positionacc2.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc2.AxtriaSalesIQTM__Team_Instance__c =teamins.id;
        
        insert positionacc2;
        
        listpositionaccountcallplan.add(positionacc2);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc3 = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamins,posAccount,pp,papos1);
        positionacc3.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc3.AxtriaSalesIQTM__Team_Instance__c =teamins.id;
        positionacc3.AxtriaSalesIQTM__Change_Status__c = 'Approved';
        positionacc3.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        
        insert positionacc3;
        listpositionaccountcallplan1.add(positionacc3);
        */
        map<Id, AxtriaSalesIQTM__Position_Account_Call_Plan__c> triggerOldMap = new map<Id, AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        triggerOldMap.put(positionacc.id,positionacc1);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            CallPlanSummaryTriggerHandler obj=new CallPlanSummaryTriggerHandler();
            CallPlanSummaryTriggerHandler.afterUpdate(listpositionaccountcallplan1,listpositionaccountcallplan,triggerOldMap);
            
        }
        Test.stopTest();
    }
}