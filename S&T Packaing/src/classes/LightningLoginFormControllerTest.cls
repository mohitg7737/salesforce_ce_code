@IsTest(SeeAllData = true)
public with sharing class LightningLoginFormControllerTest {

	@IsTest
	static void testLoginWithInvalidCredentials() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //System.assertEquals('Argument 1 cannot be null', LightningLoginFormController.login('testUser', 'fakepwd', null));
	}

	@IsTest
	static void LightningLoginFormControllerInstantiation() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //LightningLoginFormController controller = new LightningLoginFormController();
  //System.assertNotEquals(controller, null);
	}

	@IsTest
	static void testIsUsernamePasswordEnabled() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //System.assertEquals(true, LightningLoginFormController.getIsUsernamePasswordEnabled());
	}

	@IsTest
	static void testIsSelfRegistrationEnabled() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //System.assertEquals(false, LightningLoginFormController.getIsSelfRegistrationEnabled());
	}

	@IsTest
	static void testGetSelfRegistrationURL() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //System.assertEquals(null, LightningLoginFormController.getSelfRegistrationUrl());
	}

	@IsTest
	static void testAuthConfig() {
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
  //Auth.AuthConfiguration authConfig = LightningLoginFormController.getAuthConfig();
  //System.assertNotEquals(null, authConfig);
	}
}