@isTest
    public class BatchDeassignmentPositionAccountTest {
        static testMethod void testMethod1() {
            User loggedInUser = new User(id=UserInfo.getUserId());
            
            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;
            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            countr.Name ='USA';
            insert countr;
            Account a1= TestDataFactory.createAccount();
            a1.AxtriaSalesIQTM__Country__c = countr.id;
            a1.AccountNumber = 'BH10477999';
            a1.Type = 'HCO';
            a1.Status__c ='test';
            insert a1;
            Account a= TestDataFactory.createAccount();
            a.AxtriaSalesIQTM__Country__c = countr.id;
            a.AccountNumber = 'BH10117999';
            a.Type = 'HCO';
            a.Status__c ='test';
            insert a;
            Account acc= TestDataFactory.createAccount();
            acc.AxtriaSalesIQTM__Country__c = countr.id;
            acc.AccountNumber = 'BH10461999';
            acc.Type = 'HCP';
            acc.Status__c ='test';
            insert acc;
            
            
            AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
            affnet.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
            insert affnet;
            AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
            accaff.AxtriaSalesIQTM__Root_Account__c = a1.id;
            accaff.AxtriaSalesIQTM__Parent_Account__c = a1.Id;
            accaff.AxtriaSalesIQTM__Account__c = acc.Id;
            accaff.Affiliation_Status__c ='Active';
            accaff.AxtriaSalesIQTM__Is_Primary__c = true;
            accaff.AxtriaSalesIQTM__Active__c = true;
            accaff.Parent_Account_Number__c  = a1.AccountNumber;
            accaff.Account_Number__c = acc.AccountNumber;
            insert accaff;
            AxtriaSalesIQTM__Account_Affiliation__c aa = TestDataFactory.createAcctAffli(acc,affnet);
            aa.AxtriaSalesIQTM__Root_Account__c = a.id;
            aa.AxtriaSalesIQTM__Parent_Account__c = a.Id;
            aa.AxtriaSalesIQTM__Account__c = acc.Id;
            aa.Affiliation_Status__c ='Active';
            aa.AxtriaSalesIQTM__Is_Primary__c = true;
            aa.AxtriaSalesIQTM__Active__c = true;
            aa.Parent_Account_Number__c  = a.AccountNumber;
            aa.Account_Number__c = acc.AccountNumber;
            insert aa;
            
            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            team.Name = 'Oncology';
            team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
            insert team;
            AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
            insert teamins1;
            
            AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
            workspace.AxtriaSalesIQTM__Country__c = countr.id;
            insert workspace;
            
            
            AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
            scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
            insert scen;
            
            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
            teamins.Name = 'test';
            teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
            insert teamins;
            AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
            insert pos;
            
            AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
            posAccount.AxtriaSalesIQTM__Account__c = acc.id;
            posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
            posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
            insert posAccount;
            AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos,teamins);
            posAccount1.AxtriaSalesIQTM__Account__c = a1.id;
            posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
            posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
            insert posAccount1;
            AxtriaSalesIQTM__Business_Rules__c b1 = new AxtriaSalesIQTM__Business_Rules__c();
            b1.AxtriaSalesIQTM__Scenario__c = scen.id;
            b1.AxtriaSalesIQTM__Rule_Type__c = 'Bottom Up';
            insert b1;
            
            temp_Obj__c zt = new temp_Obj__c();
            zt.Status__c ='New';
            zt.Account_Text__c = acc.AccountNumber;
            zt.Team_Instance_Text__c = teamins.Name;
            zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
            zt.Object__c ='Deassign_Postiton_Account__c';
            insert zt;
            /*temp_Obj__c z = new temp_Obj__c();
    z.Status__c ='New';
    z.Account_Text__c = a1.AccountNumber;
    z.Team_Instance_Text__c = teamins.Name;
    z.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
    z.Object__c ='Deassign_Postiton_Account__c';
    insert z;*/

    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
                //BatchDeassignmentPositionAccount obj=new BatchDeassignmentPositionAccount();
                // obj.query='select Id,AccountNumber__c,Position_Code__c,Team_Instance_Name__c,Status__c,Account_Type__c,Rule_Type__c from temp_Obj__c ';
                //Database.executeBatch(obj);
        
    }
    Test.stopTest();
}
static testMethod void testMethod2() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    countr.Name ='USA';
    insert countr;
    Account acc= TestDataFactory.createAccount();
    acc.AxtriaSalesIQTM__Country__c = countr.id;
    acc.AccountNumber = 'BH10461999';
    
    acc.Type = 'HCP';
    insert acc;
    Account acc1= TestDataFactory.createAccount();
    acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
    acc1.AxtriaSalesIQTM__Country__c = countr.id;
    acc1.AccountNumber = 'BH10461969';
    acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
    acc1.Status__c = 'Active';
    acc1.Type = 'HCA';
    insert acc1;
    
    AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
    insert affnet;
    AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
    accaff.Account_Number__c = 'BH10461999';
    accaff.AxtriaSalesIQTM__Root_Account__c = acc1.id;
    accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
    accaff.AxtriaSalesIQTM__Account__c = acc.Id;
    accaff.Affiliation_Status__c ='Active';
    accaff.AxtriaSalesIQTM__Is_Primary__c = true;
    accaff.AxtriaSalesIQTM__Active__c = true;
    insert accaff;
    AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc1,affnet);
    accaff1.Account_Number__c = 'BH10461969';
    accaff1.AxtriaSalesIQTM__Root_Account__c = acc1.id;
    accaff1.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
    accaff1.AxtriaSalesIQTM__Account__c = acc1.Id;
    accaff1.Affiliation_Status__c ='Active';
    accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
    accaff1.AxtriaSalesIQTM__Active__c = true;
    insert accaff1;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    team.Name = 'Oncology';
    team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
            //team.hasCallPlan__c = true;
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
    insert scen;
    
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
    teamins.Name = 'test';
    teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
            //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
            //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
    insert teamins;
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    insert pos;
    AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen2.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S12';
    insert scen2;
    
    AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
    teamins2.AxtriaSalesIQTM__Scenario__c = scen2.id;
    teamins2.Name = 'test2';
    teamins2.AxtriaSalesIQTM__Alignment_Period__c ='Current';
            //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
            //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
    insert teamins2;
    AxtriaSalesIQTM__Position__c pos2= TestDataFactory.createPosition(team,teamins2);
    insert pos2;
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    posAccount.AxtriaSalesIQTM__Account__c = acc.id;
    posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
    posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
    insert posAccount;
    
    AxtriaSalesIQTM__Business_Rules__c b1 = new AxtriaSalesIQTM__Business_Rules__c();
    b1.AxtriaSalesIQTM__Scenario__c = scen.id;
    b1.AxtriaSalesIQTM__Rule_Type__c = 'Bottom Up';
    insert b1;
    
    temp_Obj__c zt = new temp_Obj__c();
    zt.Status__c ='New';
    zt.Account_Text__c = 'test';
    zt.Team_Instance_Text__c = teamins.id;
    zt.AccountNumber__c = acc.AccountNumber;
    zt.Account_Text__c = acc.type ;
    zt.Team_Instance_Name__c = teamins.Name;
    zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
    zt.Object__c ='Deassign_Postiton_Account__c';
    insert zt;
    
    
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        DeassignPosAccTD obj=new DeassignPosAccTD();
        Database.executeBatch(obj);
        
    }
    Test.stopTest();
}


}