global with sharing class Batch_PositionProductMapping implements Database.Batchable<sObject> {
    list<SIQ_My_Setup_Products_vod_O__c> listProd;
    map<string, SIQ_My_Setup_Products_vod_O__c> mapProd;
    map<string, SIQ_My_Setup_Products_vod_O__c> mapPosProd;
    string jobType = util.getJobType('PositionProduct');
    list<string> allTeamInstance = jobType == 'Full Load'?util.getFullLoadTeamInstancesCallPlan():util.getDeltaLoadTeamInstancesCallPlan();
   // map<string, boolean> mapProdStatus;
    string query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(jobType == 'Full Load'){
            query = 'Select Id, Name, AxtriaSalesIQTM__isActive__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Product_Catalog__r.Product_Code__c, Product_Catalog__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c From AxtriaSalesIQTM__Position_Product__c Where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance and AxtriaSalesIQTM__isActive__c = true WITH SECURITY_ENFORCED Order by AxtriaSalesIQTM__Position__c, Product_Catalog__c Asc, AxtriaSalesIQTM__isActive__c Desc';
        }
        else{    
            query = 'Select Id, Name, AxtriaSalesIQTM__isActive__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Product_Catalog__r.Product_Code__c, Product_Catalog__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c From AxtriaSalesIQTM__Position_Product__c Where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED Order by AxtriaSalesIQTM__Position__c, Product_Catalog__c Asc, AxtriaSalesIQTM__isActive__c Desc';
        }
        return database.getQueryLocator(query);
    }
    
    public void initiateVariables(){
        listProd = new list<SIQ_My_Setup_Products_vod_O__c>();
        mapProd = new map<string, SIQ_My_Setup_Products_vod_O__c>();
        mapPosProd = new map<string, SIQ_My_Setup_Products_vod_O__c>();
        //mapProdStatus = new map<string,boolean>();
    }
    
    public void createMaps(set<string> setProd){
        for(SIQ_My_Setup_Products_vod_O__c p : [Select Id, Name, External_ID__c, SIQ_Favorite_vod__c,SIQ_Product_vod__c, SIQ_Country__c, SIQ_Employee_PRID__c, Status__c, Type__c from SIQ_My_Setup_Products_vod_O__c Where External_ID__c in :setProd WITH SECURITY_ENFORCED]){
            mapProd.put(p.External_ID__c, p);
        }
        for(SIQ_My_Setup_Products_vod_O__c p : [Select Id, Name, External_ID__c, Position_Product__c, SIQ_Favorite_vod__c,SIQ_Product_vod__c, SIQ_Country__c, SIQ_Employee_PRID__c, Status__c, Record_Status__c, Type__c from SIQ_My_Setup_Products_vod_O__c Where External_ID__c in :setProd and Status__c = 'Inserted' WITH SECURITY_ENFORCED]){
            mapPosProd.put(p.Position_Product__c, p);
        }
    }
        
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Product__c> scope){
        set<string> setProd = new set<string>();
        set<string> setIds = new set<string>();
        for(AxtriaSalesIQTM__Position_Product__c pe : scope){
            setProd.add(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c+'_'+pe.Product_Catalog__r.Product_Code__c);        
        }
        initiateVariables();
        createMaps(setProd);
        for(AxtriaSalesIQTM__Position_Product__c pe : scope){ 
            string externalId = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c+'_'+pe.Product_Catalog__r.Product_Code__c;
            string strPosProd = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
            if(!setIds.contains(externalId)){
                setIds.add(externalId);
                SIQ_My_Setup_Products_vod_O__c posProd = new SIQ_My_Setup_Products_vod_O__c();
                if(!mapProd.containsKey(externalId) && pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c == null && mapPosProd.containsKey(strPosProd)){
                    mapPosProd.get(strPosProd).Status__c = 'Deleted';
                    mapPosProd.get(strPosProd).Record_Status__c = 'Updated';
                    listProd.add(mapPosProd.get(strPosProd));
                }
                else if(!mapProd.containsKey(externalId) && pe.AxtriaSalesIQTM__isActive__c == true && mapPosProd.containsKey(strPosProd)){
                    //update existing record
                    mapPosProd.get(strPosProd).Status__c = 'Deleted';
                    mapPosProd.get(strPosProd).Record_Status__c = 'Updated';
                    listProd.add(mapPosProd.get(strPosProd));
                    //insert new record
                    posProd.SIQ_Favorite_vod__c = pe.Product_Catalog__r.Name+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c;
                    posProd.Status__c = 'Inserted';
                    posProd.External_ID__c = externalId;
                    posProd.SIQ_Employee_PRID__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                    posProd.SIQ_Product_vod__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
                    posProd.SIQ_Country__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c;
                    posProd.Record_Status__c = 'Updated';
                    posProd.Position_Product__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
                    listProd.add(posProd);    
                }
                else if(!mapProd.containsKey(externalId) && pe.AxtriaSalesIQTM__isActive__c == true){
                    posProd.SIQ_Favorite_vod__c = pe.Product_Catalog__r.Name+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c;
                    posProd.Status__c = 'Inserted';
                    posProd.External_ID__c = externalId;
                    posProd.SIQ_Employee_PRID__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                    posProd.SIQ_Product_vod__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
                    posProd.SIQ_Country__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c;
                    posProd.Record_Status__c = 'Updated';
                    posProd.Position_Product__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
                    listProd.add(posProd);
                }
                else if((mapProd.get(externalId).Status__c == 'Deleted' && pe.AxtriaSalesIQTM__isActive__c == true) || (mapProd.get(externalId).Status__c == 'Inserted' && pe.AxtriaSalesIQTM__isActive__c == false) || (mapProd.get(externalId).SIQ_Favorite_vod__c != pe.Product_Catalog__r.Name+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c)){
                    posProd.Id = mapProd.get(externalId).Id;
                    posProd.SIQ_Favorite_vod__c = pe.Product_Catalog__r.Name+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Alias__c;
                    posProd.Status__c = pe.AxtriaSalesIQTM__isActive__c == true?'Inserted':'Deleted';
                    posProd.SIQ_Employee_PRID__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                    posProd.SIQ_Product_vod__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c+'_'+pe.Product_Catalog__r.Product_Code__c;
                    posProd.Record_Status__c = 'Updated';
                    listProd.add(posProd);    
                }
            }
        }
        //upsert listProd;
        SnTDMLSecurityUtil.upsertRecords(listProd, 'Batch_PositionProductMapping');
    }
    
    global void finish(Database.BatchableContext bc){
    }
}