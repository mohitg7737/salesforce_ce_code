/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the Controller that show the HCO Parameters and store the changes being done to these parameters 
for a source rule and destination rule.
*/

@isTest
private class HCO_Segmentation_Ctrl_Test { 
    static testMethod void testMethod1() {
        //Data Objects creation begin
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Measure_Type__c = 'HCO';
        mmc1.Brand__c = mmc.Brand__c;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        Account_Compute_Final__c compFinal = new Account_Compute_Final__c();
        compFinal.Measure_Master__c=mmc.id;
        insert compFinal;
        //Data object creation ends
        //
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        HCO_Segmentation_Ctrl hcoSegmentationCtrlObj = new HCO_Segmentation_Ctrl();
        hcoSegmentationCtrlObj.countryID = countr.id;
        hcoSegmentationCtrlObj.selectedcycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        hcoSegmentationCtrlObj.fillBusinessUnitOptions();
        hcoSegmentationCtrlObj.selectedBusinessUnit = teamins.id;
        hcoSegmentationCtrlObj.fillSourceRuleOptions();
        hcoSegmentationCtrlObj.selectedSourceRule = mmc.id;
        hcoSegmentationCtrlObj.fillDestinationRuleOptions();
        hcoSegmentationCtrlObj.selectedDestinationRule = mmc.id;
        
        hcoSegmentationCtrlObj.loadParameterTable();
        hcoSegmentationCtrlObj.saveParameters();
        hcoSegmentationCtrlObj.calculateHCOParam();
    }

}