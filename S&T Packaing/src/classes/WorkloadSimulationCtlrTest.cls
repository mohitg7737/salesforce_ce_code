/*Author - Himanshu Tariyal(A0994)
Date : 7th January 2018*/
@isTest
private class WorkloadSimulationCtlrTest 
{
    private static testMethod void firstTest()
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', System.today()-10, System.today()+10);
        workspace.AxtriaSalesIQTM__Country__c  = country.ID;
        insert workspace;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        insert mm;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        /*BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Brand__c=bti.id);
        insert bur;*/
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;

        Grid_Master__c matrix2 = new Grid_Master__c(Name='Workload matrix',Grid_Type__c='1D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix2;
        
        //push Rule Parameter and its associated step
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp2;
        
        //insert steps for Compute Segment and TCF stages
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute TCF',Name='Dim1',Matrix__c=matrix2.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        Step__c matrixStep2 = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep2; 
        
        //push Grid Details
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Output_Value__c='9', Dimension_2_Value__c='Dim21 Value', Dimension_1_Value__c='A', Grid_Master__c=matrix2.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Output_Value__c='8', Dimension_2_Value__c='Dim2 Value', Dimension_1_Value__c='B', Grid_Master__c=matrix2.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Output_Value__c='7', Dimension_2_Value__c='Dim211 Value', Dimension_1_Value__c='C', Grid_Master__c=matrix2.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Output_Value__c='6', Dimension_2_Value__c='Dim222 Value', Dimension_1_Value__c='D', Grid_Master__c=matrix2.id);
        insert gd4;
        
        /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
        insert acfc;*/
        
        //insert Segment Simulation data
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=30,Sum_TCF__c=30,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=22,Sum_TCF__c=22,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=27,Sum_TCF__c=27,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        /*Test Data ends here*/
        
        Test.setCurrentPageReference(new PageReference('Page.WorkloadSimulation')); 
        System.currentPageReference().getParameters().put('rid', mm.Id);
        WorkloadSimulationCtlr ws = new WorkloadSimulationCtlr();
        ws.countryID = country.id;
        ws.selectedBu = ti.id;
        ws.getBusinessUnit();
        ws.search();
        ws.selectedTerr = pos.Name;
        ws.createTerrProductCharts();
        
        ws.jsonMap='{"'+mm.id+'":{"null" :{"A":"7","C":"5"}}}'; 
        ws.changeBrand();
        ws.saveWorkloadData();
        // // ws.segSimulation = true;
        ws.pushToBusinessRules();
        ws.jsonMap= '';
        ws.changedTCFmaps = new Map<String, Map<String, Map<String, String>>>();
        ws.pushToBusinessRules();
        
        // // mm.State__c = 'Executed';
        // // update mm;
        // // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // // ws.segSimulation = true;
        // // ws.saveWorkloadData();
        // // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = false;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        WorkloadSimulationCtlr.calculateFinalTCF(mm.id,'A',8,100,'1');
        ws.redirectToPage();
        ws.selectedSimulation = 'Segment Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Workload Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Modelling';
        ws.redirectToPage();
        System.test.stopTest();

    }
    
    private static testMethod void secondTest() 
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
       insert cycle;*/
       
       Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
       insert acc;
       
       AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
       insert team;
       
       AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
       insert ti;
       
       AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
       insert pos;

       Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
       insert pc;
       
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Name='rule1',Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        insert mm;
        
        Measure_Master__c mm2 = new Measure_Master__c(Name='rule2',Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        insert mm2;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        /*BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Brand__c=bti.id);
        insert bur;*/
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;
        
        //push Rule Parameter and its associated step
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp2;
        
        //push Grid Details
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Output_Value__c='9', Dimension_2_Value__c='Dim21 Value', Dimension_1_Value__c='A', Grid_Master__c=matrix.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Output_Value__c='8', Dimension_2_Value__c='Dim2 Value', Dimension_1_Value__c='B', Grid_Master__c=matrix.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Output_Value__c='7', Dimension_2_Value__c='Dim211 Value', Dimension_1_Value__c='C', Grid_Master__c=matrix.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Output_Value__c='6', Dimension_2_Value__c='Dim222 Value', Dimension_1_Value__c='D', Grid_Master__c=matrix.id);
        insert gd4;
        
        //push Grid Details Copy
        Grid_Details_Copy__c gdc1 = new Grid_Details_Copy__c(Name='2_2', Dimension_1_Value__c='A', Dimension_2_Value__c='Dim21 Value', Output_Value__c='9', Grid_Master__c=matrix.id);
        insert gdc1;
        
        Grid_Details_Copy__c gdc2 = new Grid_Details_Copy__c(Name='3_3', Dimension_1_Value__c='B', Dimension_2_Value__c='Dim2 Value', Output_Value__c='8', Grid_Master__c=matrix.id);
        insert gdc2;
        
        Grid_Details_Copy__c gdc3 = new Grid_Details_Copy__c(Name='4_4', Dimension_1_Value__c='C', Dimension_2_Value__c='Dim211 Value', Output_Value__c='7', Grid_Master__c=matrix.id);
        insert gdc3;
        
        Grid_Details_Copy__c gdc4 = new Grid_Details_Copy__c(Name='5_5', Dimension_1_Value__c='D', Dimension_2_Value__c='Dim222 Value', Output_Value__c='6', Grid_Master__c=matrix.id);
        insert gdc4;
        
        //insert steps for Compute Segment and TCF stages
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute TCF',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        Step__c matrixStep2 = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep2; 
        
        /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
        insert acfc;*/
        
        //insert Segment Simulation data
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=30,Sum_TCF__c=30,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=22,Sum_TCF__c=22,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=27,Sum_TCF__c=27,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        
        //insert Segment Simulation Copy data
        Segment_Simulation_Copy__c sscc = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        
        Segment_Simulation_Copy__c sscc2 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,CustomerSegment__c = '1' ,isFinalSegmentStep__c = true);
        
        Segment_Simulation_Copy__c sscc3 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        
        Segment_Simulation_Copy__c sscc4 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,CustomerSegment__c = '1',isFinalSegmentStep__c = true);
        insert sscc;
        insert sscc2;
        insert sscc3;
        insert sscc4;
        /*Test Data ends here*/
        
        /*Create an instance of the class*/
        Test.setCurrentPageReference(new PageReference('Page.WorkloadSimulation')); 
        System.currentPageReference().getParameters().put('rid', mm.Id);
        WorkloadSimulationCtlr ws = new WorkloadSimulationCtlr();
        ws.countryID = country.id;
        ws.selectedRule = mm.ID;
        //ws.selectedCycle = cycle.id;
        ws.selectedBu = ti.id;
        ws.getBusinessUnit();
        ws.search();
        ws.selectedTerr = pos.Name;
        ws.createTerrProductCharts();
        
        // ws.jsonMap='';
        // ws.saveWorkloadData();
        // ws.segSimulation = true;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // ws.segSimulation = true;
        // ws.saveWorkloadData();
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = false;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        WorkloadSimulationCtlr.calculateFinalTCF(mm.id,'A',8,100,'1'); //remote fn
        ws.redirectToPage();
        ws.selectedSimulation = 'Segment Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Workload Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Modelling';
        ws.redirectToPage();
        ws.selectedSimulation = 'Comparative Analysis';
        ws.redirectToPage();
        ws.selectedSimulation = 'Quantile Analysis';
        ws.redirectToPage();
        System.test.stopTest();

    }
     private static testMethod void mccpTest()
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active',MCCP_Enabled__c = true);
        insert country;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', System.today()-10, System.today()+10);
        workspace.AxtriaSalesIQTM__Country__c  = country.ID;
        insert workspace;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,MCCP_Enabled__c = true);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed',Channels__c = 'F2F;Webinar');
        insert mm;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        /*BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Brand__c=bti.id);
        insert bur;*/
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;
        
        //push Rule Parameter and its associated step
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp2;
        
        //insert steps for Compute Segment and TCF stages
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute TCF',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        Step__c matrixStep2 = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep2; 
        
        //push Grid Details
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Output_Value__c='9', Dimension_2_Value__c='Dim21 Value', Dimension_1_Value__c='A', Grid_Master__c=matrix.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Output_Value__c='8', Dimension_2_Value__c='Dim2 Value', Dimension_1_Value__c='B', Grid_Master__c=matrix.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Output_Value__c='7', Dimension_2_Value__c='Dim211 Value', Dimension_1_Value__c='C', Grid_Master__c=matrix.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Output_Value__c='6', Dimension_2_Value__c='Dim222 Value', Dimension_1_Value__c='D', Grid_Master__c=matrix.id);
        insert gd4;
        
        
        Grid_Master__c gMaster2 = TestDataFactory.gridMaster(country);
        gMaster2.Name = 'workload';
        gMaster2.Grid_Type__c = '2D';
        insert gMaster2;

        Compute_Master__c cm1 = new Compute_Master__c();
        cm1.Expression__c = '{"elif":[{"andOr":[],"input":"test12","condition":"less than","match":"70","returVal":"Average"},'+
        '{"andOr":[],"input":"test12","condition":"less than or equal to","match":"90","returVal":"Medium"}],'+
        '"ifCase":{"andOr":[],"input":"test12","condition":"less than","match":"50","returVal":"Low"},'+
        '"elseCase":{"andOr":[],"returVal":"High"}}';
        insert cm1;
        
        Compute_Master__c cm2 = new Compute_Master__c();
        cm2.Expression__c = '{"elif":[{"andOr":[],"input":"test13","condition":"greater than or equal to","match":"70","returVal":"Medium"},'+
        '{"andOr":[],"input":"test13","condition":"greater than","match":"50","returVal":"Average"}],'+
        '"ifCase":{"andOr":[],"input":"test13","condition":"greater than","match":"90","returVal":"High"},'+
        '"elseCase":{"andOr":[],"returVal":"Low"}}';
        insert cm2;

        
        Step__c potStep = new Step__c(UI_Location__c = 'Compute Values',Name='AdoptVal',Measure_Master__c=mm.id,Step_Type__c='Cases',Modelling_Type__c = 'Potential',Compute_Master__c=cm1.id);
        potStep.Step_Type__c = 'Cases';
        potStep.Sequence__c=1;
        insert potStep;
        
        Step__c adoptStep = new Step__c(UI_Location__c = 'Compute Values',Name='PotenVal',Measure_Master__c=mm.id,Step_Type__c='Cases',Modelling_Type__c = 'Adoption',Compute_Master__c=cm2.id);
        adoptStep.Step_Type__c = 'Cases';
        adoptStep.Sequence__c=2;
        insert adoptStep;

        
        
        
        Rule_Parameter__c rps1 = TestDataFactory.ruleParameter(mm,new Parameter__c(), adoptStep);
        rps1.CustomerSegmentedField__c = false;
        insert rps1;
        Rule_Parameter__c rps2 = TestDataFactory.ruleParameter(mm,new Parameter__c(), potStep);
        rps2.CustomerSegmentedField__c = false;
        insert rps2;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(country);
        insert gMaster;
        Step__c step = new Step__c();
        step.Name = 'csSeg';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Matrix';
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mm.id;
        step.Grid_Param_1__c = rps1.id;
        step.Grid_Param_2__c = rps2.id;
        step.Sequence__c=3;
        insert step;
        
        Rule_Parameter__c rps3 = TestDataFactory.ruleParameter(mm,new Parameter__c(), step);
        rps3.CustomerSegmentedField__c = false;
        insert rps3;
       

        step = new Step__c();
        step.Name = 'tcf';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute TCF';
        step.Step_Type__c = 'Multi Channel TCF Interactions';
        step.Matrix__c = gMaster2.id;
        step.Measure_Master__c = mm.id;
        step.Grid_Param_1__c = rps3.id;
        step.Sequence__c=6;
        insert step;

       

        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '1_1';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='B';
        gDetails.Dimension_2_Value__c='F2F'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;


        gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '1_2';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='C';
        gDetails.Dimension_2_Value__c='F2F'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;

        gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '1_3';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='A';
        gDetails.Dimension_2_Value__c='F2F'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;

        gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '2_1';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='B';
        gDetails.Dimension_2_Value__c='Webinar'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;


        gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '2_2';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='C';
        gDetails.Dimension_2_Value__c='Webinar'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;

        gDetails = TestDataFactory.gridDetails(gMaster);
        gDetails.Name = '2_3';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='3';
        gDetails.Dimension_1_Value__c='A';
        gDetails.Dimension_2_Value__c='Webinar'; 
        gDetails.Grid_Master__c = gMaster2.id;
        insert gDetails;

      

        
        
        //insert Segment Simulation data
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=30,Sum_TCF__c=30,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=22,Sum_TCF__c=22,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ws',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=27,Sum_TCF__c=27,isFinalSegmentStep__c = true,CustomerSegment__c = 'null');
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        /*Test Data ends here*/
        
        Test.setCurrentPageReference(new PageReference('Page.WorkloadSimulation')); 
        System.currentPageReference().getParameters().put('rid', mm.Id);
        WorkloadSimulationCtlr ws = new WorkloadSimulationCtlr();
        ws.countryID = country.id;
        ws.selectedBu = ti.id;
        ws.getBusinessUnit();
        ws.search();
        ws.selectedTerr = pos.Name;
        ws.createTerrProductCharts();
        
        ws.jsonMapMCCP='{"'+mm.id+'":{"null" :{"F2F":{"A":"7","C":"5"}}}}'; 
        ws.changeBrand();
        ws.saveWorkloadDataMCCP();
        // // ws.segSimulation = true;
        ws.pushToBusinessRulesMCCP();
        ws.jsonMapMCCP='';
        ws.changedTCFmapsMCCP = new Map<String, Map<String, Map<String, Map<String, String>>>>();
        ws.pushToBusinessRulesMCCP();
        
        // // mm.State__c = 'Executed';
        // // update mm;
        // // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // // ws.segSimulation = true;
        // // ws.saveWorkloadData();
        // // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='{"'+mm.id+'":{"A":"7","C":"5"}}'; 
        // ws.segSimulation = true;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        
        // mm.State__c = 'Executed';
        // update mm;
        // ws.jsonMap='';
        // ws.segSimulation = false;
        // ws.noChange = false;
        // ws.pushToBusinessRules();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        WorkloadSimulationCtlr.calculateFinalTCF(mm.id,'A',8,100,'1');
        ws.redirectToPage();
        ws.selectedSimulation = 'Segment Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Workload Simulation';
        ws.redirectToPage();
        ws.selectedSimulation = 'Modelling';
        ws.redirectToPage();
        System.test.stopTest();

    }
}