global with sharing class BatchCreateStagingSurveyDataForHCO implements Database.Batchable<sObject> {
    public String query;
    String ruleId;
    List<Staging_Survey_Data__c> listToInsert;
    String teamInsId;
    String sourceRuleId;
    //Added by Chirag Ahuja for STIMPS-286
    //public String product;
    public boolean HCOFlag = false;
    public String teamInstance;
    public String type;

    global BatchCreateStagingSurveyDataForHCO(String sourceRuleId,String ruleId) {
        this.ruleId = ruleId;
        this.sourceRuleId = sourceRuleId;
        query = 'Select Id,Account_Number__c,Measure_Master__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
        query += ' Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
        query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
        query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
        query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
        query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
        query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
        query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
        query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
        query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
        query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
        query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
        query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
        query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
        query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
        query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
        query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
        query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
        query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c where Measure_Master__c =: ruleId WITH SECURITY_ENFORCED';
        this.query = query;
    }
    //Added by Chirag Ahuja for STIMPS-286
    global BatchCreateStagingSurveyDataForHCO(String sourceRuleId,String ruleId, String teamInstance, String type, Boolean flag) {
        this.ruleId = ruleId;
        this.sourceRuleId = sourceRuleId;
        this.teamInstance = teamInstance;
        this.type = type;
        this.HCOFlag = flag;
        query = 'Select Id,Account_Number__c,Measure_Master__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
        query += ' Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
        query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
        query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
        query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
        query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
        query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
        query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
        query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
        query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
        query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
        query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
        query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
        query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
        query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
        query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
        query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
        query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
        query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c where Measure_Master__c =: ruleId WITH SECURITY_ENFORCED';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account_Compute_Final__c> scope) {
        listToInsert = new List<Staging_Survey_Data__c>();
        Staging_Survey_Data__c stgSurvDataTemp;
        for(Account_Compute_Final__c acf : scope){
            stgSurvDataTemp = new Staging_Survey_Data__c();
            stgSurvDataTemp.Account_Number__c = acf.Account_Number__c;
            //stgSurvDataTemp.Position_Code__c = (String)acf.get('Physician__r').get('Position_Code__c');
            stgSurvDataTemp.Product_Code__c = acf.Measure_Master__r.Brand_Lookup__r.Product_Code__c;
            stgSurvDataTemp.Product_Name__c = acf.Measure_Master__r.Brand_Lookup__r.Name;
            stgSurvDataTemp.SURVEY_ID__c = 'AXTRIA';
            stgSurvDataTemp.SURVEY_NAME__c = 'AXTRIA';
            stgSurvDataTemp.Team__c = acf.Measure_Master__r.Team__r.Name;
            stgSurvDataTemp.Team_Instance__c = acf.Measure_Master__r.Team_Instance__r.Name;
            stgSurvDataTemp.Sales_Cycle__c = stgSurvDataTemp.Team_Instance__c;
            stgSurvDataTemp.Type__c = 'HCO';
            stgSurvDataTemp.External_ID__c = stgSurvDataTemp.Account_Number__c + '_' + stgSurvDataTemp.Product_Name__c + '_' + stgSurvDataTemp.Team_Instance__c;

            for(Integer i=1; i<=25;i++){
                if((String)acf.get('Output_Name_'+i+'__c') != null && (String)acf.get('Output_Name_'+i+'__c') != ''){
                    stgSurvDataTemp.put('Question_ID'+i+'__c',i);
                    stgSurvDataTemp.put('Short_Question_Text'+i+'__c',(String)acf.get('Output_Name_'+i+'__c'));
                    stgSurvDataTemp.put('Response'+i+'__c',(String)acf.get('Output_Value_'+i+'__c'));
                }
                else
                {
                    stgSurvDataTemp.put('Question_ID'+i+'__c',null);
                    stgSurvDataTemp.put('Short_Question_Text'+i+'__c','');
                    stgSurvDataTemp.put('Response'+i+'__c','');
                }
            }

            listToInsert.add(stgSurvDataTemp);
        }

        //upsert listToInsert External_ID__c;
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, listToInsert);
        List<Staging_Survey_Data__c> acfUpsertList = securityDecision.getRecords();
        upsert acfUpsertList External_ID__c;

        //throw exception if permission is missing
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            if(listToInsert[0].External_ID__c != null)
                throw new SnTException(securityDecision, 'BatchCreateStagingSurveyDataForHCO', SnTException.OperationType.C );
            else
                throw new SnTException(securityDecision, 'BatchCreateStagingSurveyDataForHCO', SnTException.OperationType.U );
        }
    }

    global void finish(Database.BatchableContext BC) {

        List<HCO_Parameters__c> hcoParamList = new List<HCO_Parameters__c>();
        for(HCO_Parameters__c hcoParamTemp : [Select Id from HCO_Parameters__c where Source_Rule__c =:sourceRuleId and Destination_Rule__c =: ruleId WITH SECURITY_ENFORCED]){
            hcoParamTemp.Last_Published_Date__c = DateTime.now();
            hcoParamList.add(hcoParamTemp);
        }
        //update hcoParamList;
        SnTDMLSecurityUtil.updateRecords(hcoParamList,'BatchCreateStagingSurveyDataForHCO');

        Measure_Master__c mm = [Select Team_Instance__r.Name from Measure_Master__c where Id =: ruleId WITH SECURITY_ENFORCED];
        teamInsId = mm.Team_Instance__r.Name;
        //Updated by Chirag Ahuja for STIMPS-286
        if(HCOFlag){
            Database.executeBatch(new Survey_Data_Transformation_Utility(teamInstance,type,HCOFlag));
        }
        else{
            Database.executeBatch(new Survey_Data_Transformation_Utility(teamInsId,'HCO',true));
        }
    }
}