global class Prod_Metrics_to_Survey_Utility_Final implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,SIQ_Veeva_Product_Metrics_vod_Inbound__c> questionToObjectMap;
    public Map<String,Integer> questionToResponseFieldNoMap;
    public Integer GlobalCounter = 1;
    public Map<String,String> pcExtIDToNameMap;
    public Set<String> allExtIds;
  
    global Prod_Metrics_to_Survey_Utility_Final(String teamInstance,String accfieldname,String qname) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        questionToObjectMap = new Map<String,SIQ_Veeva_Product_Metrics_vod_Inbound__c>();
        questionToResponseFieldNoMap = new Map<String,Integer>();
        selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        allExtIds = new Set<String>();
        
        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Team_Instance__r.name,bti.id);
        }
        
        List<Product_Catalog__c> pcList = new List<Product_Catalog__c>();
        pcList = [select id,name,Veeva_External_ID__c from Product_Catalog__c where Team_Instance__c = :teamInstanceNametoIDMap.get(teamInstance) ];//and IsActive__c = true
        pcExtIDToNameMap = new Map<String,String>();
        
        for(Product_Catalog__c pcRec1 : pcList){
            allExtIds.add(pcRec1.Veeva_External_ID__c);
            pcExtIDToNameMap.put(pcRec1.Veeva_External_ID__c,pcRec1.name);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'SELECT Id,Name,CurrencyIsoCode,SIQ_Veeva_Account_vod__c,SIQ_Veeva_Adoption_AZ__c,SIQ_Veeva_External_ID__c,SIQ_Veeva_KOL_AZ_vod__c,SIQ_Veeva_Potential_AZ__c,SIQ_Veeva_Products_vod__c,SIQ_Veeva_Segment_vod__c FROM SIQ_Veeva_Product_Metrics_vod_Inbound__c  Where SIQ_Veeva_Products_vod__c in :allExtIds ';//Where Team_Instance__c = \'' + teamInstance + '\' order by PARTY_ID__c, QUESTION_ID__c 
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_Veeva_Product_Metrics_vod_Inbound__c> scopeRecs) {
        system.debug('Hello++++++++++++++++++++++++++++');
        
        
        Staging_Cust_Survey_Profiling__c tempSurveyResponseRec = new Staging_Cust_Survey_Profiling__c();
        List<Staging_Cust_Survey_Profiling__c> StagingsurveyProfilingList = new List<Staging_Cust_Survey_Profiling__c>();
        
        
        for(SIQ_Veeva_Product_Metrics_vod_Inbound__c scsp : scopeRecs){
            
            if(pcExtIDToNameMap.containsKey(scsp.SIQ_Veeva_Products_vod__c)){
            
                tempSurveyResponseRec = new Staging_Cust_Survey_Profiling__c();
                
                tempSurveyResponseRec.Name = 'Axtria Internal';
                tempSurveyResponseRec.PARTY_ID__c = scsp.SIQ_Veeva_Account_vod__c;
                tempSurveyResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance);
                tempSurveyResponseRec.BRAND_ID__c = scsp.SIQ_Veeva_Products_vod__c;
                tempSurveyResponseRec.BRAND_NAME__c = pcExtIDToNameMap.get(scsp.SIQ_Veeva_Products_vod__c);
                tempSurveyResponseRec.QUESTION_ID__c = 500;
                tempSurveyResponseRec.QUESTION_SHORT_TEXT__c = 'Adoption';
                tempSurveyResponseRec.RESPONSE__c = scsp.SIQ_Veeva_Adoption_AZ__c;
                tempSurveyResponseRec.SURVEY_ID__c = 'Axtria Internal';
                tempSurveyResponseRec.SURVEY_NAME__c = 'Axtria Internal';
                
                StagingsurveyProfilingList.add(tempSurveyResponseRec);
                
                //////////////---------------------------
                
                tempSurveyResponseRec = new Staging_Cust_Survey_Profiling__c();
                
                tempSurveyResponseRec.Name = 'Axtria Internal';
                tempSurveyResponseRec.PARTY_ID__c = scsp.SIQ_Veeva_Account_vod__c;
                tempSurveyResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance);
                tempSurveyResponseRec.BRAND_ID__c = scsp.SIQ_Veeva_Products_vod__c;
                tempSurveyResponseRec.BRAND_NAME__c = pcExtIDToNameMap.get(scsp.SIQ_Veeva_Products_vod__c);
                tempSurveyResponseRec.QUESTION_ID__c = 501;
                tempSurveyResponseRec.QUESTION_SHORT_TEXT__c = 'Potential';
                tempSurveyResponseRec.RESPONSE__c = scsp.SIQ_Veeva_Potential_AZ__c;
                tempSurveyResponseRec.SURVEY_ID__c = 'Axtria Internal';
                tempSurveyResponseRec.SURVEY_NAME__c = 'Axtria Internal';
                
                StagingsurveyProfilingList.add(tempSurveyResponseRec);
                //////////////---------------------------
                
                tempSurveyResponseRec = new Staging_Cust_Survey_Profiling__c();
                
                tempSurveyResponseRec.Name = 'Axtria Internal';
                tempSurveyResponseRec.PARTY_ID__c = scsp.SIQ_Veeva_Account_vod__c;
                tempSurveyResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance);
                tempSurveyResponseRec.BRAND_ID__c = scsp.SIQ_Veeva_Products_vod__c;
                tempSurveyResponseRec.BRAND_NAME__c = pcExtIDToNameMap.get(scsp.SIQ_Veeva_Products_vod__c);
                tempSurveyResponseRec.QUESTION_ID__c = 502;
                tempSurveyResponseRec.QUESTION_SHORT_TEXT__c = 'Segment';
                tempSurveyResponseRec.RESPONSE__c = scsp.SIQ_Veeva_Segment_vod__c;
                tempSurveyResponseRec.SURVEY_ID__c = 'Axtria Internal';
                tempSurveyResponseRec.SURVEY_NAME__c = 'Axtria Internal';
                
                StagingsurveyProfilingList.add(tempSurveyResponseRec);
                //////////////---------------------------
                
                tempSurveyResponseRec = new Staging_Cust_Survey_Profiling__c();
                
                tempSurveyResponseRec.Name = 'Axtria Internal';
                tempSurveyResponseRec.PARTY_ID__c = scsp.SIQ_Veeva_Account_vod__c;
                tempSurveyResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance);
                tempSurveyResponseRec.BRAND_ID__c = scsp.SIQ_Veeva_Products_vod__c;
                tempSurveyResponseRec.BRAND_NAME__c = pcExtIDToNameMap.get(scsp.SIQ_Veeva_Products_vod__c);
                tempSurveyResponseRec.QUESTION_ID__c = 504;
                tempSurveyResponseRec.QUESTION_SHORT_TEXT__c = 'KOL';
                tempSurveyResponseRec.RESPONSE__c = String.ValueOf(scsp.SIQ_Veeva_KOL_AZ_vod__c);
                tempSurveyResponseRec.SURVEY_ID__c = 'Axtria Internal';
                tempSurveyResponseRec.SURVEY_NAME__c = 'Axtria Internal';
                
                StagingsurveyProfilingList.add(tempSurveyResponseRec);
            
            }
            
        }
        
        insert StagingsurveyProfilingList;

    }

    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new Staging_BU_Response_Insert_Utility(selectedTeamInstance,questionToResponseFieldNoMap),2000);
    }
}