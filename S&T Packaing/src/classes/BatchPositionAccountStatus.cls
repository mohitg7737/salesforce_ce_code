global class BatchPositionAccountStatus implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    //public String teamIns;
    public Date lastModifiedDate;
    global DateTime lastjobDate=null;
    public Set<String> activeAccSet;
    public Set<String> setTI2Brand;
    public Map<String,Set<String>> mapInactiveAcc2PosSet;
    public Map<String,Set<String>> mapActiveAcc2PosSet;
    public Set<String> allAccounts;
    public String cycle{get;set;}
    public String nonProcessedAcs {get;set;}
    public integer errorcount{get;set;}
    public Integer recordsProcessed=0;
    public String batchID;
    public Set<String> teamInsSet;
    public Boolean chaining = false;

    public Map<String,Set<String>> mapInactiveAccTIkey2PosSet; //Acc+TeamIns key to Position Code Set for Inactive
    public Map<String,Set<String>> mapActiveAccTIkey2PosSet; //Acc+TeamIns key to Position Code Set for Active

    @Deprecated
    global BatchPositionAccountStatus(String ti) {
    }

    @Deprecated
    global BatchPositionAccountStatus() {
    }

    global BatchPositionAccountStatus(Set<String> ti) {
        query='';
        nonProcessedAcs ='';
        errorcount = 0;
        teamInsSet=new Set<String>();
        teamInsSet.addAll(ti);
        mapInactiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Inactive
        mapActiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Active
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);

        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Staging ATL Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc Limit 1];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c -1 ;   //CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Staging ATL Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //teamIns=teaminstance;
        //lastModifiedDate=Date.today().addDays(-1);
        query= 'select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c Where lastModifiedDate  >=:  lastjobDate and AxtriaSalesIQTM__Team_Instance__c in :teamInsSet and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null order by AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Assignment_Status__c DESC';
        /*
        if(lastjobDate!=null)
        {
            query = query + 'Where lastModifiedDate  >=:  lastjobDate ';
        }*/
        

    }

    global BatchPositionAccountStatus(Set<String> ti, Boolean chain) {
        chaining = chain;
        query='';
        nonProcessedAcs ='';
        errorcount = 0;
        teamInsSet=new Set<String>();
        teamInsSet.addAll(ti);
        mapInactiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Inactive
        mapActiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Active
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);

        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Staging ATL Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);   //CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate= System.today() - 3;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Staging ATL Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //teamIns=teaminstance;
        //lastModifiedDate=Date.today().addDays(-1);
        query= 'select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c Where lastModifiedDate  >=:  lastjobDate and AxtriaSalesIQTM__Team_Instance__c in :teamInsSet and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null order by AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Assignment_Status__c DESC';
        /*
        if(lastjobDate!=null)
        {
            query = query + 'Where lastModifiedDate  >=:  lastjobDate ';
        }*/
        

    }

    global BatchPositionAccountStatus(Set<String> ti, Date lmd) {
        chaining = false;
        query='';
        nonProcessedAcs ='';
        errorcount = 0;
        teamInsSet=new Set<String>();
        teamInsSet.addAll(ti);
        mapInactiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Inactive
        mapActiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Active
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);

        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Staging ATL Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);   //CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate= System.today() - 3;       //else we set the lastjobDate to null
        }

        lastjobDate = lmd;
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Staging ATL Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //teamIns=teaminstance;
        //lastModifiedDate=Date.today().addDays(-1);
        query= 'select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c Where lastModifiedDate  >=:  lastjobDate and AxtriaSalesIQTM__Team_Instance__c in :teamInsSet and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null order by AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Assignment_Status__c DESC';
        /*
        if(lastjobDate!=null)
        {
            query = query + 'Where lastModifiedDate  >=:  lastjobDate ';
        }*/
        

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {
        System.debug('=====Query::::::' +scope);
        allAccounts = new Set<String>();
        activeAccSet = new Set<String>();
        setTI2Brand = new Set<String>();
        mapInactiveAcc2PosSet = new Map<String,Set<String>>();
        mapActiveAcc2PosSet = new Map<String,Set<String>>();

        mapInactiveAccTIkey2PosSet= new Map<String,Set<String>>();   //Acc+TeamIns key to Position Code Set for Inactive
        mapActiveAccTIkey2PosSet= new Map<String,Set<String>>();
        
        //List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> inactiveCallPlanList = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {
            if(posAcc.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive')
            {
                System.debug('====Status is Inactive====');
                //inactiveAccSet.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
                if(mapInactiveAcc2PosSet.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    mapInactiveAcc2PosSet.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber).add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
                else
                {
                    Set<String> posSet = new Set<String>();
                    posSet.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    mapInactiveAcc2PosSet.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,posSet);
                }

                //Added for Key to Pos code for Inactive set
                System.debug('====Account+Team Ins key to Position Code for Inactive Status====');
                String key = posAcc.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name;
                if(mapInactiveAccTIkey2PosSet.containsKey(key))
                {
                    mapInactiveAccTIkey2PosSet.get(key).add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
                else
                {
                    Set<String> posSet = new Set<String>();
                    posSet.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    mapInactiveAccTIkey2PosSet.put(key,posSet);
                }
                //till here..
            }
            
            else
            {
                System.debug('=====Status is Active======');
                activeAccSet.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
                if(mapActiveAcc2PosSet.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
                {
                    mapActiveAcc2PosSet.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber).add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
                else
                {
                    Set<String> posSet = new Set<String>();
                    posSet.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    mapActiveAcc2PosSet.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,posSet);
                }

                //Added for Key to Pos code for Active set
                System.debug('====Account+Team Ins key to Position Code for Active Status====');
                String key = posAcc.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + posAcc.AxtriaSalesIQTM__Team_Instance__r.Name;
                if(mapActiveAccTIkey2PosSet.containsKey(key))
                {
                    mapActiveAccTIkey2PosSet.get(key).add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
                else
                {
                    Set<String> posSet = new Set<String>();
                    posSet.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    mapActiveAccTIkey2PosSet.put(key,posSet);
                }
                //till here..
            }
            allAccounts.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
        }
        System.debug('====mapInactiveAcc2PosSet====' +mapInactiveAcc2PosSet);
        System.debug('====mapActiveAcc2PosSet====' +mapActiveAcc2PosSet);

        System.debug('====mapActiveAccTIkey2PosSet====' +mapActiveAccTIkey2PosSet);
        System.debug('====mapInactiveAccTIkey2PosSet====' +mapInactiveAccTIkey2PosSet);

        System.debug('====allAccounts====' +allAccounts);

        // Doubt in this code
        for(String acc : activeAccSet){
            if(mapInactiveAcc2PosSet.containsKey(acc)){
                Set<String> activePosSet = mapActiveAcc2PosSet.get(acc);
                for(String pos : activePosSet)
                {
                    Set<String> inactivePosSet = new Set<String>();
                    inactivePosSet=mapInactiveAcc2PosSet.get(acc);
                    if(inactivePosSet.contains(pos))
                    {
                        System.debug('remove active position from inactive set after rule execute');
                        mapInactiveAcc2PosSet.get(acc).remove(pos);
                    }
                }
            }
        }

        System.debug('====mapInactiveAcc2PosSet final inactive====' +mapInactiveAcc2PosSet);

        System.debug('====Final Inactive Account Set====' +activeAccSet);
        System.debug('====Final map <Active Account to Position set>====' +mapActiveAcc2PosSet);


        System.debug('===== Remove Active Position from Inactive key set to handle call plan:::::::::::::::::::::::');
        for(String key : mapActiveAccTIkey2PosSet.keySet())
        {
          Set<String> activePosSet = mapActiveAccTIkey2PosSet.get(key);
          System.debug('activePosSet::::::::' +activePosSet);
          for(String pos : activePosSet)
          {
             System.debug('pos::::::::' +pos);
             Set<String> inactivePosSet = new Set<String>();
             if(mapInactiveAccTIkey2PosSet.get(key) != null)
             {
                inactivePosSet=mapInactiveAccTIkey2PosSet.get(key);
             }
             System.debug('inactivePosSet::::::::' +inactivePosSet);
             System.debug('inactivePosSet.contains(pos)::::::::' +inactivePosSet.contains(pos));
             if(inactivePosSet != null && inactivePosSet.size() > 0)
             {
               if(inactivePosSet.contains(pos))
               {
                  System.debug('remove active position from inactive set fron key map');
                  mapInactiveAccTIkey2PosSet.get(key).remove(pos);
               }
             }
          }
        }
        System.debug('====Final map < Inactive key Account+Team Instance to Position set>====' +mapInactiveAccTIkey2PosSet);

        /*System.debug('====Call Plan Handling for Position Accounts====');

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callPlanList = [select id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__isTarget__c, AxtriaSalesIQTM__isTarget_Approved__c,AxtriaSalesIQTM__Team_Instance__c,Brand_Name__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber in :mapInactiveAcc2PosSet.keySet()];

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlanRec : callPlanList){
            if(mapInactiveAcc2PosSet.containsKey(callPlanRec.AxtriaSalesIQTM__Account__r.AccountNumber))
            {
                System.debug('Call plan handling for inactive Account');
                callPlanRec.AxtriaSalesIQTM__isincludedCallPlan__c = false;
                callPlanRec.AxtriaSalesIQTM__lastApprovedTarget__c = false;
                
                inactiveCallPlanList.add(callPlanRec);
            }
         }

         System.debug('====inactiveCallPlanList.size()====' +inactiveCallPlanList.size());
         System.debug('====inactiveCallPlanList====' +inactiveCallPlanList);
         if(inactiveCallPlanList.size() > 0)
            update inactiveCallPlanList;*/

         System.debug('Staging ATL add and remove Handling++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++NEED To Check commented CODE');

         List<Staging_ATL__c> stagingRec=[select id, Account__c, Axtria_Account_ID__c, Country__c,External_ID_AZ__c, Territory__c from Staging_ATL__c where External_ID_AZ__c in :allAccounts];
         System.debug('stagingRec' +stagingRec);
         List<Staging_ATL__c> allATLrecs = new List<Staging_ATL__c>();
         //List<Staging_ATL__c> finalStagingATL = new List<Staging_ATL__c>();
         Map<String,Staging_ATL__c> mapExternalid2Record = new Map<String,Staging_ATL__c>();

         Map<String,Set<String>> allATLrecsMap = new Map<String,Set<String>>();

        for(Staging_ATL__c sa : stagingRec)
        {       
           Set<String> allTerr = new Set<String>(sa.Territory__c.split(';'));
           
           if(mapInactiveAcc2PosSet.containsKey(sa.External_ID_AZ__c))
           {
                for(String str : mapInactiveAcc2PosSet.get(sa.External_ID_AZ__c))
                {
                    if(allTerr.contains(str))
                    {
                        allTerr.remove(str);
                        
                    }
                    
                }
           
           System.debug('allTerr' +allTerr);
             if(allTerr.size() > 0)
             {
                sa.Territory__c = string.join(new List<String>(allTerr),';') ;
             

               System.debug('sa.Territory__c' +sa.Territory__c);
               
               if(sa.Territory__c == null || sa.Territory__c == '' || sa.Territory__c.length() == 0)
               {
                  sa.Territory__c = ';';
               }
               else
               {

                  if(sa.Territory__c.substring(0,1) != ';')
                  {
                      sa.Territory__c = ';' + sa.Territory__c+ ';';
                      System.debug('sa.Territory__c if part' +sa.Territory__c);
                  }
                  else
                  {
                      sa.Territory__c = sa.Territory__c+ ';';
                      System.debug('sa.Territory__c else part' +sa.Territory__c);
                  }               
               }
             }
           }
                sa.Status__c = 'Updated';
               allATLrecsMap.put(sa.External_ID_AZ__c, allTerr);
               //finalStagingATL.add(sa);
               mapExternalid2Record.put(sa.External_ID_AZ__c,sa);
               system.debug('recordsProcessed+'+recordsProcessed);
               recordsProcessed++;
             
             
         }
       

       System.debug('====allATLrecsMap====' +allATLrecsMap);

       for(String activeRec : mapActiveAcc2PosSet.keySet())
       {
            if(allATLrecsMap.containsKey(activeRec))
            {
                Set<String> allTerr = allATLrecsMap.get(activeRec);
                System.debug('====allTerr 297====' +allTerr);
                Staging_ATL__c sa = new Staging_ATL__c();
                sa.Axtria_Account_ID__c = activeRec;

                for(String str : mapActiveAcc2PosSet.get(activeRec))
                {
                    if(!allTerr.contains(str))
                    {
                        allTerr.add(str);
                        System.debug('====allTerr 306====' +allTerr);
                        
                    }
                    
                }
                sa.Status__c = 'Updated'; 
                System.debug('====allTerr 312====' +allTerr);
                sa.Account__c = activeRec;
                sa.External_ID_AZ__c = activeRec;
                System.debug('====External_ID_AZ__c 313====' +sa.External_ID_AZ__c);
                sa.Territory__c = string.join(new List<String>(allTerr),';');
                System.debug('====sa.Territory__c 317====' +sa.Territory__c);
           
               if(sa.Territory__c.substring(0,1) != ';')
               {
                   sa.Territory__c = ';' + sa.Territory__c+ ';';
               }
               else
               {
                   sa.Territory__c = sa.Territory__c+ ';';
               }
               System.debug('====sa.Territory__c 328====' +sa.Territory__c);
               
             //   stagingRec.add(sa);
             //   finalStagingATL.add(sa);
                System.debug('====sa====' +sa);
                mapExternalid2Record.put(sa.External_ID_AZ__c,sa);
            }
            else
            {
                Staging_ATL__c sa = new Staging_ATL__c();
                sa.Axtria_Account_ID__c = activeRec;
                sa.Account__c = activeRec;
                sa.External_ID_AZ__c = activeRec;
                System.debug('====External_ID_AZ__c====' +sa.External_ID_AZ__c);
            System.debug('mapActiveAcc2PosSet.get(activeRec)::::::::::'+mapActiveAcc2PosSet.get(activeRec));
                sa.Status__c = 'Updated';
                sa.Territory__c = string.join(new List<String>(mapActiveAcc2PosSet.get(activeRec)),';') ;
           
               if(sa.Territory__c.substring(0,1) != ';')
               {
                   sa.Territory__c = ';' + sa.Territory__c+ ';';
               }
               else
               {
                   sa.Territory__c = sa.Territory__c+ ';';
               }
               stagingRec.add(sa);
               mapExternalid2Record.put(sa.External_ID_AZ__c,sa);
               //finalStagingATL.add(sa);
               system.debug('recordsProcessed+'+recordsProcessed);
               recordsProcessed++;
            }

       }

       System.debug('====stagingRec====' +stagingRec);
       System.debug('====stagingRec.size()====' +stagingRec.size());
       System.debug('====mapExternalid2Record====' +mapExternalid2Record);

       //upsert finalStagingATL External_ID_AZ__c;
       upsert mapExternalid2Record.values() External_ID_AZ__c;
        
    }

    global void finish(Database.BatchableContext BC) 
    {

        System.debug('Call Plan Handling::::::::::::::');

        if(mapInactiveAccTIkey2PosSet.size() > 0)
        {
               BatchCallPlanHandlingforPosAccStatus callPlan = new BatchCallPlanHandlingforPosAccStatus(teamInsSet,mapInactiveAcc2PosSet,mapInactiveAccTIkey2PosSet);
           //    Database.executeBatch(callPlan,200);
        }

      // execute any post-processing operations
        System.debug('================Schedular Job Successful==============');
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs;                             
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sjob.Object_Name__c = 'Staging ATL';
        //sjob.Changes__c        
        sJob.Job_Status__c='Successful';
        sjob.Changes__c = ErrorMsg;               
        system.debug('sJob++++++++'+sJob);
        update sJob;

        if(chaining)
        {
            List<String> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.getDeltaTeamInstancesCallPlan();
            Database.executeBatch(new changeTSFDeltaStatus(teaminstancelistis,true), 2000);
        }

    }
}