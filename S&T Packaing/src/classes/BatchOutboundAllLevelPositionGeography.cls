global class BatchOutboundAllLevelPositionGeography implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    public String query;
    public List<String> posCodeList;
    public String countryName;
    public List<String> countryList;
    global boolean flag=true;
    public String cycle {get;set;}
    global DateTime lastjobDate=null;
    public String batchID;
    public Integer recordsProcessed=0;
    public Set<String> teamInsSet;
    //public Set<String> geoNameSet;
    public Set<String> countrySet;
    public Set<String> countryIDSet;
    public Set<String> mktCode;
    
    global BatchOutboundAllLevelPositionGeography(String country) {
    }

    global BatchOutboundAllLevelPositionGeography() {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Geography__c> scope) {

    }

    global void finish(Database.BatchableContext BC) {
    }

    global void execute(System.SchedulableContext SC){
       
        Database.executeBatch(new BatchOutboundAllLevelPositionGeography(), 2000);
    } 
}