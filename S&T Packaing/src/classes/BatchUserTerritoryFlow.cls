global with sharing class BatchUserTerritoryFlow implements Database.Batchable<sObject> {
    public String query;
    public Map<String,String> orgMaster2Group;
    public Map<String,String> newMapUser2Terr;
    global BatchUserTerritoryFlow() 
    {
        this.query = query;
        orgMaster2Group=new Map<String,String>();
        newMapUser2Terr=new Map<String,String>();
        query = 'Select id,Name,Public_Group__c from AxtriaSalesIQTM__Organization_Master__c where Public_Group_SFDC_id__c != null and AxtriaSalesIQTM__Parent_Organization_Name__c != null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Organization_Master__c> orgMasterList) 
    {
     /*try
        {*/
            System.debug('=====query:::::: ' +orgMasterList);
            System.debug('=====query size:::::: ' +orgMasterList.size());

            for(AxtriaSalesIQTM__Organization_Master__c orgRec : orgMasterList){
            
                orgMaster2Group.put(orgRec.Name,orgRec.Public_Group__c);
            }

            System.debug('======orgMaster2Group::::::'+orgMaster2Group);

            System.debug('Group Handling:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');

            List<Group> grpList = [SELECT Id,Name FROM Group where Name != null and Name in :orgMaster2Group.values()]; 
            Map<String,String> grpName2grpID = new Map<String,String>();
            for(Group grp : grpList){
            
                grpName2grpID.put(grp.Name,grp.Id);
            }
            System.debug('======grpName2grpID::::::'+grpName2grpID);

            System.debug('Group member to User::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<GroupMember> groupList = [Select GroupId,Id,UserOrGroupId from GroupMember where GroupId != null and UserOrGroupId != null and GroupId in :grpName2grpID.values() and UserOrGroupId != null and GroupId != null];

            Map<String,Set<String>> groupID2UserSet = new Map<String,Set<String>>();


            for(GroupMember grp2userRec : groupList){
            
                //Added for multiple user
                if(!groupID2UserSet.containsKey(grp2userRec.GroupId)){
                
                   Set<String> temp = new Set<String>();
                   temp.add(grp2userRec.UserOrGroupId);
                   groupID2UserSet.put(grp2userRec.GroupId,temp);  
                }
                else
                {
                    groupID2UserSet.get(grp2userRec.GroupId).add(grp2userRec.UserOrGroupId);   
                }
            }
            System.debug('======groupID2User Set::::::'+groupID2UserSet);

            Set<String> orgMasteruserSet = new Set<String>();
            for(String user :groupID2UserSet.keySet()){
            

                orgMasteruserSet.addAll(groupID2UserSet.get(user));
            }
            

            System.debug('User to Federation Identifier:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<User> userList = [Select Id, FederationIdentifier from User where FederationIdentifier != null and Id in :orgMasteruserSet];
            Map<String,String> user2fedID = new Map<String,String>();
            for(User userRec : userList) {
           
                user2fedID.put(userRec.Id,userRec.FederationIdentifier);
            }
            System.debug('======user2fedID::::::'+user2fedID);


            System.debug('=========================================Map for Org master User================================================================');

            System.debug('===============================Organization Master Data Flow Handling===========================================================');

            for(String orgName : orgMaster2Group.keySet()) {
           
                System.debug('======orgName org master map Organization Master Data Flow Handling::::::'+orgName);
                String groupName = orgMaster2Group.get(orgName);
                System.debug('======groupName org master map Organization Master Data Flow Handling::::::'+groupName);
                String groupID = grpName2grpID.get(groupName);
                System.debug('======groupID org master map Organization Master Data Flow Handling::::::'+groupID);
                Set<String> userIDSet = groupID2UserSet.get(groupID);
                System.debug('======userID org master map Organization Master Data Flow Handling::::::'+userIDSet);
                if(userIDSet != null)
                {
                    for(String userID : userIDSet)
                    {
                        String fedID = user2fedID.get(userID);
                        System.debug('======fedID org master map Organization Master Data Flow Handling::::::'+fedID);
                        if(fedID != null)
                        {
                            newMapUser2Terr.put(fedID,orgName);
                            System.debug('fedID::::: newMapUser2Terr org master' +fedID);
                            System.debug('orgName::::: newMapUser2Terr org master' +orgName);
                        }
                    }
                }
                System.debug('======newMapUser2Terr org master map ::::::'+newMapUser2Terr);
            }

            List<User_Territory__c> userTerrList = new List<User_Territory__c>();

            for(String user : newMapUser2Terr.keySet()){
            
                User_Territory__c userTerrRec = new User_Territory__c();
                userTerrRec.User__c = user;
                userTerrRec.Territory__c = newMapUser2Terr.get(user);
                userTerrRec.Event__c = 'Add';
                userTerrRec.External_ID__c= user + '_' + newMapUser2Terr.get(user);
                userTerrList.add(userTerrRec);
            }

            if(userTerrList.size() > 0)
                upsert userTerrList External_ID__c;
         /*}  
           catch(Exception e)
           {
                system.debug('++inside catch');
                String Header='USer Territory Flow Batch is failed, so delta has been haulted of this org for Today';
                tryCatchEmailAlert.veevaLoad(Header);
           }*/
    }
    global void finish(Database.BatchableContext BC) 
    {

    }
}