global with sharing class SelectRuleParametrsCtlr extends BusinessRule implements IBusinessRule{
    public list<Parameter__c> parameterList{get;set;}
    public String selectedParameters{get;set;}
    public List<selectOption> selectValues{get;set;}
    public Boolean displayPopup {get;set;}
    public list<Rule_Parameter__c> ruleParameters;
    public String PicklistSelectedVal ;
    public String selectedParam{get;set;}
    public transient List<SLDSPageMessage> PageMessages{get;set;}
    public String ruleId {get;set;}
    public SelectRuleParametrsCtlr(){
        try{
        init();
            ruleId = ApexPages.currentPage().getParameters().get('rid');
        selectedParameters = '';
        displayPopup = false;
        getParameterList();
        String PicklistSelectedVal = '';
        SnTDMLSecurityUtil.printDebugMessage('---constructor---');
        selectValues = new List<selectOption>();  
        ruleParameters = new list<Rule_Parameter__c>();      
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',ruleId);
            } 

    }

    public void getParameterList()
    {
       try{ 
        parameterList = new list<Parameter__c>();
        //ruleParameters = new list<Rule_Parameter__c>();

        //Purged
        /*List<Brand_Team_Instance__c> bTeamInstance = [select id from Brand_Team_Instance__c where Brand__c = :ruleObject.Brand_Lookup__c and Team_Instance__c = :ruleObject.Team_Instance__c];*/

        list<Rule_Parameter__c> destParams;
        try{
            destParams = [SELECT Id, Parameter__c, Parameter__r.Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        Set<String> destParamsSet = new Set<String>();
        for(Rule_Parameter__c rp : destParams){
            destParamsSet.add(rp.Parameter__r.Name__c);
        }
        //Purge start
        //if(bTeamInstance.size() > 0)
        //{
        /*for(Parameter__c param: [Select ID, Name__c FROM Parameter__c WHERE Id NOT IN :destParamsSet AND isActive__c=true AND Team_Instance__c =:ruleObject.Team_Instance__c and Product_Catalog__c = :ruleObject.Brand_Lookup__c and Parameter_Type__c =:ruleObject.Measure_Type__c ORDER BY CreatedDate DESC]) // Shivansh A1450 -- added an additional check of Parameter_Type to show only HCO/HCP parameters based on the rule type.
        {
                parameterList.add(param);
        }*/

        system.debug('+++++++++ destParamsSet');
        system.debug(destParamsSet);
        List<Parameter__c> paramtemp;
        try{
            paramtemp = [Select ID, Name__c FROM Parameter__c WHERE Name__c NOT IN :destParamsSet AND isActive__c=true AND Team_Instance__c =:ruleObject.Team_Instance__c and Product_Catalog__c = :ruleObject.Brand_Lookup__c and Parameter_Type__c =:ruleObject.Measure_Type__c WITH SECURITY_ENFORCED ORDER BY CreatedDate DESC];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        for(Parameter__c param:paramtemp){
            parameterList.add(param);
        }
        //Purge end
        //}
        /*else
        {
            for(Parameter__c param: [Select ID, Name FROM Parameter__c WHERE Id NOT IN :destParamsSet AND isActive__c=true AND Team_Instance__c =:ruleObject.Team_Instance__c ORDER BY CreatedDate DESC])
            {
                parameterList.add(param);   
            }
        }*/
        SnTDMLSecurityUtil.printDebugMessage('@@parameterList:'+parameterList);
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
        } 
    }

    public list<Rule_Parameter__c> getDestParameterList(){
        /*return [SELECT Id, Parameter__c, Parameter__r.Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];*/
        list<Rule_Parameter__c> ruleparamtemp;
        try{
            ruleparamtemp = [SELECT Id, Parameter__c, Parameter__r.Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id  WITH SECURITY_ENFORCED ORDER BY Name];
        }
        catch(Exception qe) 
        {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        return ruleparamtemp;
    }

        public String getMyPicklistvalue()

        {
        
            return  PicklistSelectedVal;
        
        }

 

        public void setMyPicklistvalue(String val)
        
        {
        
           PicklistSelectedVal = val;
        
        }


    @RemoteAction
    global static Parameter__c addNewParameter(String newParameter){
        if(String.isNotBlank(newParameter)){
            Parameter__c parameter = new Parameter__c();
            parameter.Name__c = newParameter;
            //insert parameter;
            SnTDMLSecurityUtil.insertRecords(parameter, 'SelectRuleParametrsCtlr');
            return parameter;
        }
        return null;
    }
	
	List<String> parameters = new List<String>(); // Added for SR-452
    public void saveFinal(){
        list<Rule_Parameter__c> avalRuleParam ;
        try{
            try{
            avalRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }


        set<String> paramSet = new set<String>();
        for(Rule_Parameter__c rp: avalRuleParam){
            paramSet.add(rp.Parameter__c);
        }
        ruleParameters = new list<Rule_Parameter__c>();
        SnTDMLSecurityUtil.printDebugMessage('++selectedParameters++ ' + selectedParameters);
       // list<String> parameters = (list<String>)JSON.deserialize(selectedParameters, list<String>.class);
        parameters = (list<String>)JSON.deserialize(selectedParameters, list<String>.class); // updated for SR-452
        
		SnTDMLSecurityUtil.printDebugMessage('--selectedParameters-- ' + parameters);
        for(String param: parameters){
            if(String.isNotBlank(param) && !paramSet.contains(param)){
                Rule_Parameter__c rp = new Rule_Parameter__c();
                rp.Measure_Master__c = ruleObject.Id;
                rp.Parameter__c = param;
                ruleParameters.add(rp);
            }
        }
        //insert ruleParameters;
        SnTDMLSecurityUtil.insertRecords(ruleParameters,'SelectRuleParametrsCtlr');
        SnTDMLSecurityUtil.printDebugMessage('selectValues---' + ruleParameters);
        selectValues = new List<SelectOption>();
        selectValues.add(new SelectOption('None','None'));
        List<Rule_Parameter__c> rptemp;
        try{
            rptemp = [Select Id,Parameter__c,Parameter_Name__c, CustomerSegmentedField__c from Rule_Parameter__c where Measure_Master__c = :ruleObject.Id and  Parameter_Name__c != null WITH SECURITY_ENFORCED ];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        for(Rule_Parameter__c Rp : rptemp){
            if(Rp.CustomerSegmentedField__c)
            { 
                selectedParam = Rp.Parameter_Name__c;
            }
            selectValues.add(new SelectOption(Rp.Parameter_Name__c,Rp.Parameter_Name__c));
        }
            
        SnTDMLSecurityUtil.printDebugMessage('selectValues---' + selectValues);
        }
      catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
        }
    }
    
    public void saveFinalCopy(){
        list<Rule_Parameter__c> avalRuleParam;
        try{
            try{
            avalRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        set<String> paramSet = new set<String>();
        for(Rule_Parameter__c rp: avalRuleParam){
            paramSet.add(rp.Parameter__c);
        }
        ruleParameters = new list<Rule_Parameter__c>();
        list<String> parameters = (list<String>)JSON.deserialize(selectedParameters, list<String>.class);
        SnTDMLSecurityUtil.printDebugMessage('--selectedParameters-- ' + parameters);
        for(String param: parameters){
            if(String.isNotBlank(param) && !paramSet.contains(param)){
                Rule_Parameter__c rp = new Rule_Parameter__c();
                rp.Measure_Master__c = ruleObject.Id;
                rp.Parameter__c = param;
                ruleParameters.add(rp);
            }
        }
        //insert ruleParameters;
        SnTDMLSecurityUtil.insertRecords(ruleParameters,'SelectRuleParametrsCtlr');
        SnTDMLSecurityUtil.printDebugMessage('selectValues---' + selectValues);
    }
       catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
        }  
    }
    public void save(){
        try{
        saveFinal();
        list<Rule_Parameter__c> allRuleParam;
        try{
            allRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        if(allRuleParam != null && allRuleParam.size() > 0){
            updateRule('Profiling Parameter', 'Basic Information');
        }else{
              PageMessages = SLDSPageMessage.add(PageMessages,'Please choose the required parameters','error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
            }
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
        } 
    }

    public PageReference saveAndNext(){
        try{
        saveFinal();
		
		//-- Added by RT for SR-452 ---
		list<Rule_Parameter__c> allRuleParam = new list<Rule_Parameter__c>(); 
        allRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        
		if(allRuleParam.size()==0 || parameters.size()== 0){
			 PageMessages = SLDSPageMessage.add(PageMessages,'Please choose the required parameters','error');
		} 
		else{
			displayPopup = true;
		}
		// -- ends here --
        
        //list<Rule_Parameter__c> allRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        // if(allRuleParam != null && allRuleParam.size() > 0){
        //     updateRule('Compute Values', 'Profiling Parameter');
        //     return nextPage('ComputeValues');
        // }else{
        //     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
        // }
        return null;
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
            return null;
        } 
    }
    
     public PageReference SaveParamSaveAndNext(){
        try{
        saveFinalCopy();
       
        
        displayPopup = false;
        List<Rule_Parameter__c> allRuleParam ;
        try{
            allRuleParam = [SELECT Id,Parameter__r.name__c, Parameter__c, CustomerSegmentedField__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
               PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
               SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        
        if(allRuleParam != null && allRuleParam.size() > 0)
        {
            SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Sekected Rule id is '+ selectedParam);
            for(Rule_Parameter__c rp : allRuleParam)
            {
                if(rp.Parameter__r.name__c == selectedParam)
                    rp.CustomerSegmentedField__c = true;
                else
                    rp.CustomerSegmentedField__c = false;
            }
            //update allRuleParam;
            SnTDMLSecurityUtil.updateRecords(allRuleParam,'SelectRuleParametrsCtlr');
            updateRule('Compute Values', 'Profiling Parameter');
            return nextPage('ComputeValues');
        }else{
              PageMessages = SLDSPageMessage.add(PageMessages,'Please choose the required parameters','error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
        }
        return null;
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SelectRuleParametrsCtlr',ruleId);
            return null;
        } 
        
    }
    
    public void hidePopup()
    {
        displayPopup = false;
    }
    
  /*  public List<selectOption> getselectValues(){
       List<Rule_Parameter__c> listRP = [Select ID, Parameter__r.Name__c FROM Rule_Parameter__c
                                   // WHERE  isActive__c=true AND 
                                   // Team_Instance__c =:ruleObject.Team_Instance__c and 
                                    //Brand_Team_Instance__c = :bTeamInstance[0].ID ORDER BY CreatedDate DESC
                                   ];
         
             SnTDMLSecurityUtil.printDebugMessage('listRP--------' +listRP );       
            List<SelectOption> Options = new List<SelectOption>();
            Options.add(new SelectOption('','-None-'));
            for(Rule_Parameter__c Rp : ruleParameters)
            {
                Options.add(new SelectOption(Rp.Id,Rp.Parameter__c));
            }
        return Options;
    }
    
   */ 
    
    
}