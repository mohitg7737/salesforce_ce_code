global class BatchPositionAccountAddedDropped2 implements Database.Batchable<sObject>, Database.Stateful{
    
    map<String,Integer> Active_Map = New map<String,Integer>();
    map<String,Integer> InActive_Map = New map<String,Integer>();
    Public integer intPA; 
    Public integer intPAIN;    
    public String Country;
    
    Public String query;
    public String batchID;
    global DateTime lastjobDate=null;
      public Integer recordsProcessed=0;
  
    
    global BatchPositionAccountAddedDropped2 (String Country){
        this.country = country;
        // this.Teaminstance = Teaminstance;
        
         List<AxtriaARSnT__Scheduler_Log__c> schLog = new List<AxtriaARSnT__Scheduler_Log__c>();
        schLog = [Select Id,AxtriaARSnT__Created_Date__c,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c= 'Create Position Account Records Count' and AxtriaARSnT__Job_Status__c='Successful'  Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLog.size() > 0)
        {
          lastjobDate=schLog[0].AxtriaARSnT__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        System.debug('last job'+lastjobDate);
        
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        sJob.AxtriaARSnT__Job_Name__c = 'Create Position Account Records Count';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Inbound';
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        
        query = 'SELECT Id, Name,LastModifiedDate ,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and  lastModifiedDate >:lastjobDate' ;        
         
        if(lastjobDate!=null){
          query = query + ' and LastModifiedDate >=:  lastjobDate '; 
        }
    }
       global Database.QueryLocator start(Database.BatchableContext BC) {
       // myDate = date.newInstance(2019, 01, 23);
        //String query = 'SELECT Id, Name,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country ' ;        
        system.debug('Position Account Records:::::::::::::' + query);        
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account__c> scope){     
         for(AxtriaSalesIQTM__Position_Account__c PA : Scope){
             if(PA.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){
                 if(!Active_Map.containsKey(PA.AxtriaSalesIQTM__Position__c)){
                     Active_Map.put(PA.AxtriaSalesIQTM__Position__c ,1);
                  }
                  else{
                    intPA = Active_Map.get(PA.AxtriaSalesIQTM__Position__c);
                    intPA++;
                    Active_Map.Put(PA.AxtriaSalesIQTM__Position__c,intPA);
                  }
                } 
                
                 else if(PA.AxtriaSalesIQTM__Assignment_Status__c == 'InActive'){
                     if(!InActive_Map.containsKey(PA.AxtriaSalesIQTM__Position__c)){
                         InActive_Map.put(PA.AxtriaSalesIQTM__Position__c ,1);
                      }
                      else{
                        intPAIN = InActive_Map.get(PA.AxtriaSalesIQTM__Position__c);
                        intPAIN++;
                        InActive_Map.Put(PA.AxtriaSalesIQTM__Position__c,intPAIN);
                      }
                } 
             
      }
      
      
      
      List<AxtriaSalesIQTM__Position__c> PosList = [SELECT Id, Name,AxtriaARSnT__PA_Active__c,AxtriaARSnT__PA_Inactive__c  FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country];
           for(AxtriaSalesIQTM__Position__c POS : PosList){
               POS.AxtriaARSnT__PA_Active__c = Active_Map.get(POS.ID); 
               POS.AxtriaARSnT__PA_Inactive__c  = InActive_Map.get(POS.ID); 
                  }
            update PosList;  
            
         recordsProcessed +=PosList.size();

    }
    
    global void finish(Database.BatchableContext BC) {
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
        sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
        sJob.AxtriaARSnT__Job_Status__c='Successful';
        update sJob;
        }    
   }