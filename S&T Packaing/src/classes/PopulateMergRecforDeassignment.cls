global with sharing class PopulateMergRecforDeassignment implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> loosingset;

    global PopulateMergRecforDeassignment(Set<String> loosingsetRec) {
        query = '';
        loosingset=new Set<String>();
        loosingset.addAll(loosingsetRec);
    }   

    global Database.QueryLocator start(Database.BatchableContext bc) {
        query = 'SELECT id,AccountNumber,Type,Merge_Account_Number__c FROM Account where AccountNumber != null and Merge_Account_Number__c != null and Merge_Account_Number__c IN :loosingset';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {

        SnTDMLSecurityUtil.printDebugMessage('==========Query:::::' +scope);
        Set<String> accSet = new Set<String>();

        for(Account accRec : scope)
        {
            accSet.add(accRec.Merge_Account_Number__c);
        }

        List<String> periodList=new List<String>();
        periodList.add('Current');
        periodList.add('Future');
        SnTDMLSecurityUtil.printDebugMessage('==========periodList:::::' +periodList);
        SnTDMLSecurityUtil.printDebugMessage('==========accSet:::::' +accSet);

        List<Deassign_Postiton_Account__c> deassignList = new List<Deassign_Postiton_Account__c>();

        List<AxtriaSalesIQTM__Position_Account__c>losPositionAccount = [select Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.Merge_Account_Number__c IN:accSet and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList   and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null WITH SECURITY_ENFORCED];

        SnTDMLSecurityUtil.printDebugMessage('==========losPositionAccount:::::' +losPositionAccount);
        SnTDMLSecurityUtil.printDebugMessage('==========losPositionAccount.size():::::' +losPositionAccount.size());

        for(AxtriaSalesIQTM__Position_Account__c posAcc : losPositionAccount)
        {
            Deassign_Postiton_Account__c deassignRec = new Deassign_Postiton_Account__c();
            deassignRec.Account__c=posAcc.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c;
            deassignRec.Position__c=posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            deassignRec.Team_Instance__c=posAcc.AxtriaSalesIQTM__Team_Instance__r.Name;
            deassignRec.Status__c='New Merge';
            deassignList.add(deassignRec);
            SnTDMLSecurityUtil.printDebugMessage('==========deassignRec:::::' +deassignRec);
        }

        SnTDMLSecurityUtil.printDebugMessage('==========deassignList.size():::::' +deassignList.size());

        if(deassignList.size() > 0){
            //insert deassignList;
            SnTDMLSecurityUtil.insertRecords(deassignList, 'PopulateMergRecforDeassignment');
        }
        
    }

    global void finish(Database.BatchableContext BC) {

        BatchPopulateAccRuleTypeforMerge affHandle = new BatchPopulateAccRuleTypeforMerge(loosingset);
        Database.executeBatch(affHandle,1);

    }
}