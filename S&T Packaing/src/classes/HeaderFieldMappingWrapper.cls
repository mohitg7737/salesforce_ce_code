public class HeaderFieldMappingWrapper {
	public string headerField {get; set;}
    public string selField {get; set;}
    public list <selectoption> optField {get; set;}
    
    public HeaderFieldMappingWrapper(){}
    
    public HeaderFieldMappingWrapper(String headerField,String selField, list <selectoption> optField){
        this.headerField = headerField;
        this.selField= selField;
        this.optField = optField;
    }
}