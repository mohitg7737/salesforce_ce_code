public with sharing class AffiliationMergeUnmerge {

    Set<String> setHcaAccNum = new Set<String>();
    Set<String> setHcpAccNum = new Set<String>();
    Set<String> setScenarioID = new Set<String>();
    Set<String> setBUaccnum = new Set<String>();
    Set<String> setTDaccnum = new Set<String>();
    Set<String> setBuHcp = new Set<String>();
    Set<String> setTdHca = new Set<String>();
    Set<String> setaccID = new Set<String>();
    Set<String> inactiveALooserAffSet = new Set<String>();
    List<String> marketCode = new List<String>();
    
    Integer hcpProcessed = 0;
    Integer hcaProcessed = 0;
    String hcpQuery;
    String hcaQuery;
        
    Map<String,String> mapPosAcc2Scenario = new Map<String,String>();
    Map<String,String> mapScenario2BusRule = new Map<String,String>();
    
    List<AxtriaSalesIQTM__Account_Affiliation__c> affList = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
    List<Account> accountList = new List<Account>();
    List<AxtriaSalesIQTM__Position_Account__c> posList = new List<AxtriaSalesIQTM__Position_Account__c>();
    List<AxtriaSalesIQTM__Account_Affiliation__c> LosAccAffListHCP = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
    List<AxtriaSalesIQTM__Account_Affiliation__c> LosAccAffListHCA = new List<AxtriaSalesIQTM__Account_Affiliation__c>(); 
 

    public AffiliationMergeUnmerge(Set<String> loosingset) {

        /////********/Country Code Fetch
        Map<String,MergeCountry__c> allCodes = MergeCountry__c.getAll();
           if(allCodes.size() != null || allCodes.size() > 0 )
              marketCode.addAll(allCodes.keySet());
        System.debug('====Custom country code=====' +marketCode);
        ///////////////***********  
        System.debug('========Looser Account============');
        List<Account> LosAccountList = [SELECT id,AccountNumber,Type FROM account where AccountNumber != null and AccountNumber IN :loosingset];

        System.debug('========Looser Account HCA/HCP============');
        for(Account acc : LosAccountList){
            if(acc.Type == 'HCP'){
                setHcpAccNum.add(acc.AccountNumber);
            }
            if(acc.Type == 'HCA'){
                setHcaAccNum.add(acc.AccountNumber);
            }
        }
        System.debug('======setHcpAccNum : ' +setHcpAccNum);
        System.debug('======setHcaAccNum : ' +setHcaAccNum);        

        System.debug('=======Looser Position Account List========');
        List<AxtriaSalesIQTM__Position_Account__c> LosPosAccList = [SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :marketCode];
        System.debug('=====Debug check for Looser PA====' +LosPosAccList);  

        System.debug('=======Looser Position Account Business Rule========');
        for(AxtriaSalesIQTM__Position_Account__c tempPosAcc : LosPosAccList){
            setScenarioID.add(tempPosAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c);
            mapPosAcc2Scenario.put(tempPosAcc.id,tempPosAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c);
        }

        List<AxtriaSalesIQTM__Business_Rules__c> LosBussRuleList = [SELECT id, AxtriaSalesIQTM__Rule_Type__c,AxtriaSalesIQTM__Scenario__c FROM AxtriaSalesIQTM__Business_Rules__c where AxtriaSalesIQTM__Scenario__c in :setScenarioID];

         for(AxtriaSalesIQTM__Business_Rules__c busRule : LosBussRuleList){
            mapScenario2BusRule.put(busRule.AxtriaSalesIQTM__Scenario__c,busRule.AxtriaSalesIQTM__Rule_Type__c);
        }
        
        System.debug('========Looser Position Account Rule type============');
        for(AxtriaSalesIQTM__Position_Account__c posAcc : LosPosAccList){
            String scenario = mapPosAcc2Scenario.get(posAcc.id);
            String ruleType = mapScenario2BusRule.get(scenario);
            if(ruleType == 'Bottom Up'){
                setBUaccnum.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            }
            if(ruleType == 'Top Down'){
                setTDaccnum.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            }
        }
        System.debug('========Looser Bottom UP Account Number======= ' +setBUaccnum);
        System.debug('========Looser Top Down Account Number======= ' +setTDaccnum);

        
        System.debug('==========Looser Affiliation Handling===============');
        String query = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset';

        List<AxtriaSalesIQTM__Account_Affiliation__c> LosAccAffList =  Database.query(query);
        
        for(AxtriaSalesIQTM__Account_Affiliation__c losAff : LosAccAffList){
            if(setBUaccnum.contains(losAff.AxtriaSalesIQTM__Account__r.AccountNumber)){
                if(setHcpAccNum.contains(losAff.AxtriaSalesIQTM__Account__r.AccountNumber)) {
                    setBuHcp.add(losAff.AxtriaSalesIQTM__Account__r.AccountNumber);
                    hcpProcessed+= 1;
                }
            }
            if(setTDaccnum.contains(losAff.AxtriaSalesIQTM__Account__r.AccountNumber)){
                if(setHcaAccNum.contains(losAff.AxtriaSalesIQTM__Account__r.AccountNumber)) {
                    setTdHca.add(losAff.AxtriaSalesIQTM__Account__r.AccountNumber);
                    hcaProcessed+= 1;
                }
            }
        }

        if(hcpProcessed > 0){
            hcpQuery = query + ' and Account_Number__c IN :setBuHcp and AxtriaSalesIQTM__Account__r.Type = \'HCA\'';
        }
        if(hcaProcessed > 0){
            hcaQuery = query + ' and Parent_Account_Number__c IN :setTdHca and AxtriaSalesIQTM__Account__r.Type = \'HCP\'';
        }

        system.debug('=========hcpQuery::'+hcpQuery);
        system.debug('=========hcaQuery::'+hcaQuery);
        
        if(hcpQuery != null)
          LosAccAffListHCP =  Database.query(hcpQuery);
        
        if(hcaQuery != null)
            LosAccAffListHCA =  Database.query(hcaQuery);
        
        System.debug('=====HCP Looser and HCA Affiliation=====' +LosAccAffListHCP);
        System.debug('=====HCA Looser and HCP Affiliation=====' +LosAccAffListHCA);
        Map<String,String> mapAff2HCPlos = new Map<String,String>();
        Map<String,String> mapAff2HCAlos = new Map<String,String>();

        System.debug('========Map for Affiliation Account and respective Looser Account===========');
        if(LosAccAffListHCP != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c hcpLosAff : LosAccAffListHCP){
                mapAff2HCPlos.put(hcpLosAff.Parent_Account_Number__c,hcpLosAff.Account_Number__c);
                inactiveALooserAffSet.add(hcpLosAff.id);
            }
        }

        if(LosAccAffListHCA != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c hcaLosAff : LosAccAffListHCA){
                mapAff2HCAlos.put(hcaLosAff.Account_Number__c,hcaLosAff.Parent_Account_Number__c);
                inactiveALooserAffSet.add(hcaLosAff.id);
            }
        }

        System.debug('========Map for Affiliation Account and respective Looser Account mapAff2HCPlos===========' +mapAff2HCPlos);
        System.debug('========Map for Affiliation Account and respective Looser Account mapAff2HCAlos===========' + mapAff2HCAlos);

        System.debug('========Further affiliation check for Looser Account===========');
        Map<String,String> mapHCALooser2Pos = new Map<String,String>();
        Map<String,String> mapHCPLooser2Pos = new Map<String,String>();

        List<AxtriaSalesIQTM__Position_Account__c> looserPosAcc = [SELECT AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__c in :loosingset and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :marketCode];

        if(looserPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c looserPosAccRec : looserPosAcc){
                if(looserPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCP'){
                   mapHCPLooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__c,looserPosAccRec.AxtriaSalesIQTM__Position__c);
                }
                if(looserPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCA'){
                   mapHCALooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__c,looserPosAccRec.AxtriaSalesIQTM__Position__c);
                }
            }
        }

        System.debug('========Affiliation Position===========');

        Set<String> setHCAFurtherAff = new Set<String>();
        Set<String> setHCPFurtherAff = new Set<String>();
        Set<String> inactiveAccSet = new Set<String>();
        
        Map<String,String> mapHCPAffAcc2Pos = new Map<String,String>();
        Map<String,String> mapHCAAffAcc2Pos = new Map<String,String>();

        //List<AxtriaSalesIQTM__Position_Account__c> finalAffPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

        List<AxtriaSalesIQTM__Position_Account__c> affPosAccList = [SELECT id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and (AxtriaSalesIQTM__Account__c in :setBuHcp or AxtriaSalesIQTM__Account__c in :setTdHca) and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :marketCode];

        System.debug('=====Affiliation Position Account List======' +affPosAccList);
        if(affPosAccList != null){
            for(AxtriaSalesIQTM__Position_Account__c affPosAccRec : affPosAccList){
                //mapAffAcc2Pos.put(affPosAccRec.AxtriaSalesIQTM__Account__c,affPosAccRec.AxtriaSalesIQTM__Position__c);
                //System.debug('===Map for Affiliation Account and Position====' +mapAffAcc2Pos);
                if(affPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCP'){
                    mapHCPAffAcc2Pos.put(affPosAccRec.AxtriaSalesIQTM__Account__c,affPosAccRec.AxtriaSalesIQTM__Position__c);
                    System.debug('===Map for HCP Affiliation Account and Position====' +mapHCPAffAcc2Pos);
                    System.debug('===Position Check for HCP Affliation and looser====');
                    String losAcc = mapAff2HCAlos.get(affPosAccRec.AxtriaSalesIQTM__Account__c);
                    if(affPosAccRec.AxtriaSalesIQTM__Position__c == mapHCALooser2Pos.get(losAcc)){
                        setHCAFurtherAff.add(affPosAccRec.AccountNumber__c);
                    }
                    else{
                        inactiveAccSet.add(losAcc);
                        }
                }
                if(affPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCA'){
                    mapHCAAffAcc2Pos.put(affPosAccRec.AxtriaSalesIQTM__Account__c,affPosAccRec.AxtriaSalesIQTM__Position__c);
                    System.debug('===Map for HCA Affiliation Account and Position====' +mapHCAAffAcc2Pos);
                    System.debug('===Position Check for HCA Affliation and looser====');
                    String losAcc = mapAff2HCPlos.get(affPosAccRec.AxtriaSalesIQTM__Account__c);
                    if(affPosAccRec.AxtriaSalesIQTM__Position__c == mapHCPLooser2Pos.get(losAcc)){
                        setHCPFurtherAff.add(affPosAccRec.AccountNumber__c);
                    }
                    else{
                        inactiveAccSet.add(losAcc);
                        }

                }
            }
        }

        
        System.debug('=======Inactive Affiliation, Position, Address and Account');
        System.debug('=====Inactive Looser Account set====' +inactiveAccSet);
        List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveAffList = [select id from AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c in :inactiveAccSet];
        List<AxtriaSalesIQTM__Account_Affiliation__c> affAccList = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        List<AxtriaSalesIQTM__Position_Account__c> posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
        List <AxtriaSalesIQTM__Account_Address__c> addList = new List <AxtriaSalesIQTM__Account_Address__c>(); 

        List<Account> accountList = new List<Account>();
            
        if(inactiveAffList != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c aff : inactiveAffList){
                //AxtriaSalesIQTM__Account_Affiliation__c aff = new AxtriaSalesIQTM__Account_Affiliation__c(id = temp1);
                //aff.AxtriaSalesIQTM__Account__r.Status__c = 'Inactive';
                aff.Affiliation_Status__c = 'Inactive';
                //aff.AxtriaSalesIQTM__Active__c = false;
                aff.Affiliation_End_Date__c = Date.today().addDays(-1);
                affAccList.add(aff);
            }


            List<AxtriaSalesIQTM__Position_Account__c> inactivePosAccList = [select id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber in :inactiveAccSet];
            
            
            for(AxtriaSalesIQTM__Position_Account__c pa : inactivePosAccList){
                if(pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                    pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                    pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                
                posAccList.add(pa);
            }

            List<AxtriaSalesIQTM__Account_Address__c> inactiveAddList = [select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,Status__c from AxtriaSalesIQTM__Account_Address__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:inactiveAccSet ];                                                                                                                                                                                                                 
            for(AxtriaSalesIQTM__Account_Address__c add : inactiveAddList){
               add.Status__c = 'Inactive';
               addList.add(add);
            }  

            List<Account> inactiveAccList = [select id from Account where AccountNumber in :inactiveAccSet];
            for(Account acc : inactiveAccList){
                //Account acc = new Account(id = temp);
                acc.Status__c = 'Inactive';
                accountList.add(acc);
            }
        }

        if(affAccList.size() > 0 && accountList.size() > 0 && posAccList.size() > 0 && addList.size() > 0){
            update affAccList;
            update posAccList;
            update addList;
            update accountList;         
        }

        System.debug('==========Further Affiliation Handling for HCA Affiliation===============');
        String affHCAQuery = 'SELECT AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,Id,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where Parent_Account_Number__c IN:setHCPFurtherAff and AxtriaSalesIQTM__Account__r.Type = \'HCP\'';

        /*System.debug('==========Further Affiliation Handling for HCP Affiliation===============');
        String affHCPQuery = 'SELECT AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,Id,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c IN:setHCPFurtherAff and AxtriaSalesIQTM__Account__r.Type = \'HCA\'';*/

        List<AxtriaSalesIQTM__Account_Affiliation__c> FurtherAffHCPList =  Database.query(affHCAQuery);
        //List<AxtriaSalesIQTM__Account_Affiliation__c> FurtherAffHCPList =  Database.query(affHCPQuery);
        Set<String> hcpfurtherAffSet = new Set<String>();
        //Set<String> hcafurtherAffSet = new Set<String>();
        Map<String,String> mapFurtherAff2HCAAff = new Map<String,String>();
        //Map<String,String> mapFurtherAff2HCPAff = new Map<String,String>();
        
        //System.debug('===HCP Affiliation to further HCA Affiliation====' +FurtherAffHCPList);
        System.debug('===HCA Affiliation to further HCP Affiliation====' +FurtherAffHCPList);

        if(FurtherAffHCPList != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c tempList : FurtherAffHCPList){
                mapFurtherAff2HCAAff.put(tempList.Account_Number__c,tempList.Parent_Account_Number__c);
                if(!loosingset.contains(tempList.Account_Number__c)){
                    hcpfurtherAffSet.add(tempList.Account_Number__c);
                }
            }
        }

        /*if(FurtherAffHCPList != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c tempList : FurtherAffHCPList){
                mapFurtherAff2HCAAff.put(tempList.Account_Number__c,tempList.Parent_Account_Number__c);
                if(!loosingset.contains(tempList.Account_Number__c)){
                    hcpfurtherAffSet.add(tempList.Account_Number__c);
                }
            }
        }*/

        System.debug('======Position Account check for Further Affiliation Account=========');

        Set<String> inactiveAssign = new Set<String>();
        List<AxtriaSalesIQTM__Position_Account__c> furtheraffPosAcc = [SELECT id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and (AxtriaSalesIQTM__Account__c in :hcpfurtherAffSet) and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c in :marketCode];

        if(furtheraffPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c frthrAffPosAcc : furtheraffPosAcc){
                if(frthrAffPosAcc.AxtriaSalesIQTM__Account__r.Type == 'HCP'){
                    System.debug('===Further Position Check for Affliation and Further HCP Affiliation====');
                    String accnum = mapFurtherAff2HCAAff.get(frthrAffPosAcc.AxtriaSalesIQTM__Account__c);
                    if(frthrAffPosAcc.AxtriaSalesIQTM__Position__c != mapHCAAffAcc2Pos.get(accnum)){
                        inactiveAssign.add(accnum);
                    }
                }
                /*if(frthrAffPosAcc.AxtriaSalesIQTM__Account__r.Type == 'HCA'){
                    System.debug('===Further Position Check for Affliation and Further HCA Affiliation====');
                    String accnum = mapFurtherAff2HCPAff.get(frthrAffPosAcc.AxtriaSalesIQTM__Account__c);
                    if(frthrAffPosAcc.AxtriaSalesIQTM__Position__c != mapHCPAffAcc2Pos.get(accnum)){
                        inactiveAssign.add(accnum);
                    }
                }*/
            }
        }
            

        System.debug('======Inactive affiliation account set====' +inactiveAssign);
        System.debug('======Inactive affiliation Assignment==========');
        List<AxtriaSalesIQTM__Position_Account__c> inactiveAssignList = [select id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber in :inactiveAssign];
        List<AxtriaSalesIQTM__Position_Account__c> paList = new List<AxtriaSalesIQTM__Position_Account__c>();
        
        if(inactiveAssignList != null){
            for(AxtriaSalesIQTM__Position_Account__c affPA : inactiveAssignList){
                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                    affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                    affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c=affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                
                paList.add(affPA);
            }
        }

        if(paList.size() > 0)
            update paList;
       

        /*** ADDED BY SIVA****/
        String query2 = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:loosingset or AxtriaSalesIQTM__Parent_Account__r.AccountNumber IN:loosingset';

        List<AxtriaSalesIQTM__Account_Affiliation__c> LosAccAffList2 =  Database.query(query2);

        if(LosAccAffList2!=null && LosAccAffList2.size() >0){
            for(AxtriaSalesIQTM__Account_Affiliation__c af: LosAccAffList2) {
                af.Affiliation_Status__c = 'Inactive';
                af.Affiliation_End_Date__c = Date.today().addDays(-1);
                af.AxtriaSalesIQTM__Active__c = false;
            }
            update LosAccAffList2;   
        }

        //Commentd the below code by siva
       /*List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveList = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
       for(String s : inactiveALooserAffSet){
          AxtriaSalesIQTM__Account_Affiliation__c tempAcc = new AxtriaSalesIQTM__Account_Affiliation__c(id=s);
          tempAcc.Affiliation_Status__c = 'Inactive';
          //tempAcc.AxtriaSalesIQTM__Active__c = false;
          tempAcc.Affiliation_End_Date__c = Date.today().addDays(-1);
           inactiveList.add(tempAcc);
            
       }
       if(inactiveList.size() > 0)
            update inactiveList;
        */
    }
}