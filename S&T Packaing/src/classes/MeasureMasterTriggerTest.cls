/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 6th October'2020
Description : Test class for MeasureMasterTrigger and Measure_Master_Trigger_Handler
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class MeasureMasterTriggerTest 
{
	//working fine
    static testMethod void mccpTestMethod() 
    {
    	String className = 'MeasureMasterTriggerTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.State__c ='In Progress';
        mmc.Rule_Type__c = 'MCCP';
        mmc.is_promoted__c = false;
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        mmc.is_promoted__c = true;
        SnTDMLSecurityUtil.updateRecords(mmc,className);

        System.Test.stopTest();
    }

    static testMethod void sntExecuteTestMethod() 
    {
    	String className = 'MeasureMasterTriggerTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        List<Measure_Master__c> insertMMList = new List<Measure_Master__c>();
        List<Measure_Master__c> updateMMList = new List<Measure_Master__c>();

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.State__c ='In Progress';
        mmc.Rule_Type__c = 'SNT';
        mmc.IsContinueProcessing__c = true;
        insertMMList.add(mmc);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.State__c ='In Progress';
        mmc2.Rule_Type__c = 'SNT';
        mmc.IsContinueProcessing__c = true;
        insertMMList.add(mmc2);
        SnTDMLSecurityUtil.insertRecords(insertMMList,className);

        BU_Response__c bu = new BU_Response__c();
        bu.Product__c = pcc.Id;
        bu.Team_Instance__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(bu,className);

        Step__c step = new Step__c();
        step.isQuantileComputed__c = false;
        step.Measure_Master__c = mmc.Id;
        step.Step_Type__c = 'Quantile';
        SnTDMLSecurityUtil.insertRecords(step,className);

        Step__c step2 = new Step__c();
        step2.isQuantileComputed__c = false;
        step2.Measure_Master__c = mmc2.Id;
        step2.Step_Type__c = 'Quantile';
        SnTDMLSecurityUtil.insertRecords(step2,className);

        Enqueue_Business_Rule_Job__c ebrj = new Enqueue_Business_Rule_Job__c();
        ebrj.Jobs_Id__c = mmc2.Id+','+mmc.Id;
        ebrj.Market__c = countr.Id;
        ebrj.State_Type__c = 'Execute';
        SnTDMLSecurityUtil.insertRecords(ebrj,className);

        mmc.State__c = 'Executed';
        updateMMList.add(mmc);

        mmc2.State__c = 'In Queue';
        updateMMList.add(mmc2);
        SnTDMLSecurityUtil.updateRecords(updateMMList,className);

        System.Test.stopTest();
    }

    //working fine
    static testMethod void sntPublishTestMethod() 
    {
    	String className = 'MeasureMasterTriggerTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        List<Measure_Master__c> insertMMList = new List<Measure_Master__c>();
        List<Measure_Master__c> updateMMList = new List<Measure_Master__c>();

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.State__c ='In Progress';
        mmc.Rule_Type__c = 'SNT';
        mmc.IsContinueProcessing__c = true;
        insertMMList.add(mmc);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.State__c ='In Progress';
        mmc2.Rule_Type__c = 'SNT';
        insertMMList.add(mmc2);
        SnTDMLSecurityUtil.insertRecords(insertMMList,className);

        BU_Response__c bu = new BU_Response__c();
        bu.Product__c = pcc.Id;
        bu.Team_Instance__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(bu,className);

        Enqueue_Business_Rule_Job__c ebrj = new Enqueue_Business_Rule_Job__c();
        ebrj.Jobs_Id__c = mmc2.Id+','+mmc.Id;
        ebrj.Market__c = countr.Id;
        ebrj.State_Type__c = 'Publish';
        SnTDMLSecurityUtil.insertRecords(ebrj,className);

        mmc.State__c = 'In Publish Queue';
        updateMMList.add(mmc);

        mmc2.State__c = 'In Publish Queue';
        updateMMList.add(mmc2);
        SnTDMLSecurityUtil.updateRecords(updateMMList,className);

        System.Test.stopTest();
    }

    //working fine
    static testMethod void sntPublishAlignTestMethod() 
    {
    	String className = 'MeasureMasterTriggerTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        List<Measure_Master__c> insertMMList = new List<Measure_Master__c>();
        List<Measure_Master__c> updateMMList = new List<Measure_Master__c>();

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.State__c ='In Progress';
        mmc.Rule_Type__c = 'SNT';
        mmc.IsContinueProcessing__c = true;
        insertMMList.add(mmc);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.State__c ='In Progress';
        mmc2.Rule_Type__c = 'SNT';
        insertMMList.add(mmc2);
        SnTDMLSecurityUtil.insertRecords(insertMMList,className);

        BU_Response__c bu = new BU_Response__c();
        bu.Product__c = pcc.Id;
        bu.Team_Instance__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(bu,className);

        Enqueue_Business_Rule_Job__c ebrj = new Enqueue_Business_Rule_Job__c();
        ebrj.Jobs_Id__c = mmc2.Id+','+mmc.Id;
        ebrj.Market__c = countr.Id;
        ebrj.State_Type__c = 'Publish';
        SnTDMLSecurityUtil.insertRecords(ebrj,className);

        mmc.State__c = 'In Publish To Alignment Queue';
        updateMMList.add(mmc);

        mmc2.State__c = 'In Publish To Alignment Queue';
        updateMMList.add(mmc2);
        SnTDMLSecurityUtil.updateRecords(updateMMList,className);

        Measure_Master_Trigger_Handler classCall = new Measure_Master_Trigger_Handler();

        System.Test.stopTest();
    }
}