global with sharing class Errorlognotification implements Schedulable {
    global list<AsyncApexJob>joblist {get;set;}
    public list<ErrorLogEmail__c> emailIds{get;set;} 
    global Errorlognotification() {
        joblist = new list<AsyncApexJob>();
        fetchdata();
    }
    global  void fetchdata(){
        joblist= [SELECT ApexClass.Name,CreatedDate,ExtendedStatus,Id,NumberOfErrors,Status,TotalJobItems FROM AsyncApexJob  where NumberOfErrors >0 and ExtendedStatus!='' and  createddate= TODAY  order by createddate DESC]; //and  createddate= TODAY
        if(joblist!=null && joblist.size()>0){
            if(ApexPages.currentPage() == null)
                senderroremail();
        }
    }

    global void senderroremail (){
        Messaging.SingleEmailMessage emailTobeSent = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment attachmentPDf = new Messaging.EmailFileAttachment();
            list<string>emaillist = new list<string>();
            PageReference PDf ;
            Blob b ;
            try {
                   if(!test.isRunningTest()){
                        PDF =  Page.ErrorLoggerpage;//Replace attachmentPDf with the page you have rendered as PDF
                        PDF.setRedirect(true);
                        b = PDf.getContentAsPDF();
                        //b = pdf.getContentAsPDF();
                    }
                    if(test.isRunningTest()){
                        b = Blob.valueOf('Test Email');
                    }
                } catch (VisualforceException e) {
                    b = Blob.valueOf('Error occurred. Not able to convert content to required format');
                }
            attachmentPDf.setFileName('Error Logger Information');
            attachmentPDf.setBody(b);
            attachmentPDf.setContentType('application/pdf');
            emailTobeSent.setSubject('Today Error Logger Info of Apex JObs');
            emailIds = ErrorLogEmail__c.getall().values();
            if(emailIds!=null){
                for(ErrorLogEmail__c LEM:emailIds){
                    emaillist.add(LEM.Name);
                }
            }
            //emailTobeSent.toaddresses = new String[] { 'jeedi.gopi@axtria.com'};
            emailTobeSent.setToAddresses(emaillist);
            emailTobeSent.setPlainTextBody('Hi,Please find the Attachment');
            emailTobeSent.setFileAttachments(new Messaging.EmailFileAttachment[] {attachmentPDf});
            Messaging.sendEmail(new Messaging.Email[] {emailTobeSent});
    }
    global void execute(SchedulableContext sc)
    {
       Errorlognotification LN=new Errorlognotification();
       //LN.senderroremail();
    }
}