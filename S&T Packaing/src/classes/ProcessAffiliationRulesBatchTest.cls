@isTest
private class ProcessAffiliationRulesBatchTest
{
    private static testMethod void firstTest() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc = TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        affnet.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Parent_Account_Number__c  = acc.AccountNumber;
        accaff.Account_Number__c = acc.AccountNumber;
        insert accaff;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        scen.AxtriaSalesIQTM__Enable_Affiliation_Based_Alignment__c = true;
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins.AxtriaSalesIQTM__EnableAccountSharing__c = true;
        teamins.AxtriaSalesIQTM__CIM_Available__c = true;
        teamins.AxtriaSalesIQTM__Affiliation_Network__c = affnet.id;
        insert teamins;
        
        AxtriaSalesIQTM__Business_Rule_Type_Master__c brm = new AxtriaSalesIQTM__Business_Rule_Type_Master__c();
        brm.name = 'Affiliation';
        insert brm;
        
        AxtriaSalesIQTM__Business_Rules__c br = new AxtriaSalesIQTM__Business_Rules__c();
        br.AxtriaSalesIQTM__Business_Rule_Type_Master__c = brm.id;
        br.AxtriaSalesIQTM__Rule_Type__c ='Bottom Up';
        br.AxtriaSalesIQTM__Scenario__c = scen.id;
        br.AxtriaSalesIQTM__Geographic_Preference__c ='Align in Geo';
        br.AxtriaSalesIQTM__Level__c ='All';
        insert br;
        
        AxtriaSalesIQTM__BR_JoinRule__c brjoin = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='parent';
        brjoin.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin;
        
        AxtriaSalesIQTM__BR_JoinRule__c brjoin1 = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin1.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin1.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='child';
        brjoin1.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin1;
                
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        insert pos1;
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        insert posteamins;
        //AxtriaSalesIQTM__Change_Request__c crObj = 
        //[SELECT Id, (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__CR_Accounts__r WHERE AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit') FROM AxtriaSalesIQTM__Change_Request__c WHERE Id =: crId];
        
         AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
         ccd1.Name = 'ChangeRequestTrigger';
         ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
         insert ccd1;
      
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Status__c = 'Pending';
        cr.AxtriaSalesIQTM__Team_Instance_Id__c = teamins.id;
        insert cr;
        
        AxtriaSalesIQTM__CR_Account__c cracc = new AxtriaSalesIQTM__CR_Account__c();
        cracc.AxtriaSalesIQTM__Account__c = Acc.id;
        cracc.AxtriaSalesIQTM__Destination_Position__c = pos.id;
        cracc.AxtriaSalesIQTM__Source_Position__c = pos1.id;
        cracc.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = posteamins.id;
        cracc.AxtriaSalesIQTM__Source_Position_Team_Instance__c = posteamins.id;
        //cracc.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        cracc.AxtriaSalesIQTM__Change_Request__c = cr.Id;
        cracc.AxtriaSalesIQTM__IsImpactCallPlan__c = true;
        cracc.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        insert cracc;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Locked__c =true;
        posAccount.AxtriaSalesIQTM__Comments__c ='';
        posAccount.AxtriaSalesIQTM__Affiliation_Based_Alignment__c =true;
        posAccount.AxtriaSalesIQTM__Account_Target_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Relationship_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c ='';
        posAccount.AxtriaSalesIQTM__logged__c =true;
        posAccount.AxtriaSalesIQTM__isExcluded__c =true;
        posAccount.AxtriaSalesIQTM__TempDestinationrgb__c ='AffBatch';
        posAccount.AxtriaSalesIQTM__SharedWith__c =null;
        posAccount.AxtriaSalesIQTM__Segment_1__c ='Primary';
        posAccount.AxtriaSalesIQTM__Position_Team_Instance__c =posteamins.id;
        posAccount.AxtriaSalesIQTM__Metric9__c =10;
        posAccount.AxtriaSalesIQTM__Metric10__c =10;
        posAccount.AxtriaSalesIQTM__Metric1__c =10;
        posAccount.AxtriaSalesIQTM__Metric2__c =10;
        posAccount.AxtriaSalesIQTM__Metric3__c =10;
        posAccount.AxtriaSalesIQTM__Metric4__c =10;
        posAccount.AxtriaSalesIQTM__Metric5__c =10;
        posAccount.AxtriaSalesIQTM__Metric6__c =10;
        posAccount.AxtriaSalesIQTM__Metric7__c =10;
        posAccount.AxtriaSalesIQTM__Metric8__c =10;
        posAccount.AxtriaSalesIQTM__IsShared__c = false;
        posAccount.Batch_Name__c = 'Process Affiliation Rule Batch';
        insert posAccount;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;
        
        AxtriaSalesIQTM__Alignment_Global_Settings__c a = new AxtriaSalesIQTM__Alignment_Global_Settings__c();
        a.name = 'AffiliationHierarchyThreshold';
        a.AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = '1';
        insert a;
        set<Id> accountsToBeProcessed = new set<Id> ();
        accountsToBeProcessed.add(acc.id);
        list<AxtriaSalesIQTM__Change_Request__c> crlist = new list<AxtriaSalesIQTM__Change_Request__c>();
        crlist.add(cr);
        
        System.Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ProcessAffiliationRulesBatch obj=new ProcessAffiliationRulesBatch(teamins.id,cr.id,accountsToBeProcessed);
            obj.excludedAccountIds.add(acc.id);
            //set<Id> accountIds, AffiliationRule affRule, string affiliationNetwork, boolean isNLevel
            //obj.fetchAffiliatedAccounts();
            Database.executeBatch(obj);
            
        }
        System.Test.stopTest();
    }
    private static testMethod void secondTest() 
    {
        //create a new test user.
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc = TestDataFactory.createAccount();
        insert acc;
        Account a1 = TestDataFactory.createAccount();
        insert a1;
               
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        affnet.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Parent_Account_Number__c  = acc.AccountNumber;
        accaff.Account_Number__c = acc.AccountNumber;
        insert accaff;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        scen.AxtriaSalesIQTM__Enable_Affiliation_Based_Alignment__c = true;
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins.AxtriaSalesIQTM__EnableAccountSharing__c = true;
        teamins.AxtriaSalesIQTM__CIM_Available__c = true;
        teamins.AxtriaSalesIQTM__Affiliation_Network__c = affnet.id;
        insert teamins;
        
        
        AxtriaSalesIQTM__Business_Rule_Type_Master__c brm = new AxtriaSalesIQTM__Business_Rule_Type_Master__c();
        brm.name = 'Affiliation';
        insert brm;
        
        AxtriaSalesIQTM__Business_Rules__c br = new AxtriaSalesIQTM__Business_Rules__c();
        br.AxtriaSalesIQTM__Business_Rule_Type_Master__c = brm.id;
        br.AxtriaSalesIQTM__Rule_Type__c ='Top Down';
        br.AxtriaSalesIQTM__Scenario__c = scen.id;
        br.AxtriaSalesIQTM__Geographic_Preference__c ='Align in Geo';
        br.AxtriaSalesIQTM__Level__c ='All';
        insert br;
        
        AxtriaSalesIQTM__BR_JoinRule__c brjoin = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='parent';
        brjoin.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin;
        
        AxtriaSalesIQTM__BR_JoinRule__c brjoin1 = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin1.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin1.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='child';
        brjoin1.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin1;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins1);
        insert pos1;
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        insert posteamins;
        //AxtriaSalesIQTM__Change_Request__c crObj = 
        //[SELECT Id, (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__CR_Accounts__r WHERE AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit') FROM AxtriaSalesIQTM__Change_Request__c WHERE Id =: crId];
        
         AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
          ccd1.Name = 'ChangeRequestTrigger';
          ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
          insert ccd1;
      
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        insert cr;
        
        AxtriaSalesIQTM__CR_Account__c cracc = new AxtriaSalesIQTM__CR_Account__c();
        cracc.AxtriaSalesIQTM__Account__c = Acc.id;
        cracc.AxtriaSalesIQTM__Destination_Position__c = pos.id;
        cracc.AxtriaSalesIQTM__Source_Position__c = pos1.id;
        cracc.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = posteamins.id;
        cracc.AxtriaSalesIQTM__Source_Position_Team_Instance__c = posteamins.id;
        //cracc.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        cracc.AxtriaSalesIQTM__Change_Request__c = cr.Id;
        cracc.AxtriaSalesIQTM__IsImpactCallPlan__c = true;
        insert cracc;

        List<AxtriaSalesIQTM__CR_Account__c> cracclist = new List<AxtriaSalesIQTM__CR_Account__c>();
        cracclist.add(cracc);
             
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Locked__c =true;
        posAccount.AxtriaSalesIQTM__Comments__c ='';
        posAccount.AxtriaSalesIQTM__Affiliation_Based_Alignment__c =true;
        posAccount.AxtriaSalesIQTM__Account_Target_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Relationship_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c ='';
        posAccount.AxtriaSalesIQTM__logged__c =true;
        posAccount.AxtriaSalesIQTM__isExcluded__c =true;
        posAccount.AxtriaSalesIQTM__TempDestinationrgb__c ='';
        posAccount.AxtriaSalesIQTM__SharedWith__c ='';
        posAccount.AxtriaSalesIQTM__Segment_1__c ='';
        posAccount.AxtriaSalesIQTM__Position_Team_Instance__c =posteamins.id;
        posAccount.AxtriaSalesIQTM__Metric9__c =10;
        posAccount.AxtriaSalesIQTM__Metric10__c =10;
        posAccount.AxtriaSalesIQTM__Metric1__c =10;
        posAccount.AxtriaSalesIQTM__Metric2__c =10;
        posAccount.AxtriaSalesIQTM__Metric3__c =10;
        posAccount.AxtriaSalesIQTM__Metric4__c =10;
        posAccount.AxtriaSalesIQTM__Metric5__c =10;
        posAccount.AxtriaSalesIQTM__Metric6__c =10;
        posAccount.AxtriaSalesIQTM__Metric7__c =10;
        posAccount.AxtriaSalesIQTM__Metric8__c =10;
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c p2 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        p2.AxtriaSalesIQTM__Locked__c =true;
        p2.AxtriaSalesIQTM__Comments__c ='';
        p2.AxtriaSalesIQTM__Affiliation_Based_Alignment__c =true;
        p2.AxtriaSalesIQTM__Account_Target_Type__c ='';
        p2.AxtriaSalesIQTM__Account_Relationship_Type__c ='';
        p2.AxtriaSalesIQTM__Account_Alignment_Type__c ='';
        p2.AxtriaSalesIQTM__logged__c =true;
        p2.AxtriaSalesIQTM__isExcluded__c =true;
        p2.AxtriaSalesIQTM__TempDestinationrgb__c ='';
        p2.AxtriaSalesIQTM__SharedWith__c ='';
        p2.AxtriaSalesIQTM__Segment_1__c ='';
        p2.AxtriaSalesIQTM__Position_Team_Instance__c =posteamins.id;
        p2.AxtriaSalesIQTM__Metric9__c =10;
        p2.AxtriaSalesIQTM__Metric10__c =10;
        p2.AxtriaSalesIQTM__Metric1__c =10;
        p2.AxtriaSalesIQTM__Metric2__c =10;
        p2.AxtriaSalesIQTM__Metric3__c =10;
        p2.AxtriaSalesIQTM__Metric4__c =10;
        p2.AxtriaSalesIQTM__Metric5__c =10;
        p2.AxtriaSalesIQTM__Metric6__c =10;
        p2.AxtriaSalesIQTM__Metric7__c =10;
        p2.AxtriaSalesIQTM__Metric8__c =10;
        insert p2;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;
        
        AxtriaSalesIQTM__Alignment_Global_Settings__c a = new AxtriaSalesIQTM__Alignment_Global_Settings__c();
        a.name = 'AffiliationHierarchyThreshold';
        a.AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = '1';
        insert a;
        list<AxtriaSalesIQTM__Position_Account__c> posAccToBeProcessed = new list<AxtriaSalesIQTM__Position_Account__c> ();
        posAccToBeProcessed.add(posAccount);

        Set<ID> accID = new Set<ID>();
        accID.add(acc.Id);

        System.Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ProcessAffiliationRulesBatch obj=new ProcessAffiliationRulesBatch(teamins.id,posAccToBeProcessed,'Create');
            obj.processPendingCR(posAccToBeProcessed,accID,teamins.id,'testID');
            obj.createExclulsionLogger(cracclist,'test');
            Database.executeBatch(obj);
            
        }
        System.Test.stopTest();
    }
    private static testMethod void thirdTest() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc = TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        affnet.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        accaff.Parent_Account_Number__c  = acc.AccountNumber;
        accaff.Account_Number__c = acc.AccountNumber;
        insert accaff;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        scen.AxtriaSalesIQTM__Enable_Affiliation_Based_Alignment__c = true;
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        AxtriaSalesIQTM__Business_Rule_Type_Master__c brm = new AxtriaSalesIQTM__Business_Rule_Type_Master__c();
        brm.name = 'Affiliation';
        insert brm;
        AxtriaSalesIQTM__Business_Rules__c br = new AxtriaSalesIQTM__Business_Rules__c();
        br.AxtriaSalesIQTM__Business_Rule_Type_Master__c = brm.id;
        br.AxtriaSalesIQTM__Rule_Type__c ='Bottom Up';
        br.AxtriaSalesIQTM__Scenario__c = scen.id;
        br.AxtriaSalesIQTM__Geographic_Preference__c ='Align in Geo';
        br.AxtriaSalesIQTM__Level__c ='All';
        insert br;
        AxtriaSalesIQTM__BR_JoinRule__c brjoin = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='parent';
        brjoin.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin;
        AxtriaSalesIQTM__BR_JoinRule__c brjoin1 = new AxtriaSalesIQTM__BR_JoinRule__c();
        brjoin1.AxtriaSalesIQTM__Business_Rules__c = br.id;
        brjoin1.AxtriaSalesIQTM__Affiliation_Hierarchy__c ='child';
        brjoin1.AxtriaSalesIQTM__where_expression_source__c ='AxtriaSalesIQTM__Active__c = true';
        insert brjoin1;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        teamins.AxtriaSalesIQTM__EnableAccountSharing__c = true;
        teamins.AxtriaSalesIQTM__CIM_Available__c = true;
        teamins.AxtriaSalesIQTM__Affiliation_Network__c = affnet.id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        insert pos1;
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        insert posteamins;
        //AxtriaSalesIQTM__Change_Request__c crObj = 
        //[SELECT Id, (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__CR_Accounts__r WHERE AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit') FROM AxtriaSalesIQTM__Change_Request__c WHERE Id =: crId];
        
         AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
          ccd1.Name = 'ChangeRequestTrigger';
          ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
          insert ccd1;
      
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        insert cr;
        AxtriaSalesIQTM__CR_Account__c cracc = new AxtriaSalesIQTM__CR_Account__c();
        cracc.AxtriaSalesIQTM__Account__c = Acc.id;
        cracc.AxtriaSalesIQTM__Destination_Position__c = pos.id;
        cracc.AxtriaSalesIQTM__Source_Position__c = pos1.id;
        cracc.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = posteamins.id;
        cracc.AxtriaSalesIQTM__Source_Position_Team_Instance__c = posteamins.id;
        //cracc.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        cracc.AxtriaSalesIQTM__Change_Request__c = cr.Id;
        cracc.AxtriaSalesIQTM__IsImpactCallPlan__c = true;
        insert cracc;
        
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Locked__c =true;
        posAccount.AxtriaSalesIQTM__Comments__c ='';
        posAccount.AxtriaSalesIQTM__Affiliation_Based_Alignment__c =true;
        posAccount.AxtriaSalesIQTM__Account_Target_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Relationship_Type__c ='';
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c ='';
        posAccount.AxtriaSalesIQTM__logged__c =true;
        posAccount.AxtriaSalesIQTM__isExcluded__c =true;
        posAccount.AxtriaSalesIQTM__TempDestinationrgb__c ='';
        posAccount.AxtriaSalesIQTM__SharedWith__c ='';
        posAccount.AxtriaSalesIQTM__Segment_1__c ='';
        posAccount.AxtriaSalesIQTM__Position_Team_Instance__c =posteamins.id;
        posAccount.AxtriaSalesIQTM__Metric9__c =10;
        posAccount.AxtriaSalesIQTM__Metric10__c =10;
        posAccount.AxtriaSalesIQTM__Metric1__c =10;
        posAccount.AxtriaSalesIQTM__Metric2__c =10;
        posAccount.AxtriaSalesIQTM__Metric3__c =10;
        posAccount.AxtriaSalesIQTM__Metric4__c =10;
        posAccount.AxtriaSalesIQTM__Metric5__c =10;
        posAccount.AxtriaSalesIQTM__Metric6__c =10;
        posAccount.AxtriaSalesIQTM__Metric7__c =10;
        posAccount.AxtriaSalesIQTM__Metric8__c =10;
        insert posAccount;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;
        
        AxtriaSalesIQTM__Alignment_Global_Settings__c a = new AxtriaSalesIQTM__Alignment_Global_Settings__c();
        a.name = 'AffiliationHierarchyThreshold';
        a.AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = '1';
        insert a;
        set<Id> accountsToBeProcessed = new set<Id> ();
        accountsToBeProcessed.add(acc.id);
        System.Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ProcessAffiliationRulesBatch obj=new ProcessAffiliationRulesBatch(teamins.id,accountsToBeProcessed);
            obj.excludedAccountIds.add(acc.id);
            Database.executeBatch(obj);
            
        }
        System.Test.stopTest();
    }
    
}