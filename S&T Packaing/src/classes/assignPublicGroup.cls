global class assignPublicGroup implements Database.Batchable<sObject> ,Schedulable
{
    public String query;
    Map<string,id> userinfo;
    Map<String,id> groupnametoidmap;
    Set<String> userid;

    global assignPublicGroup()
    {
        query = 'SELECT Country,Id,IsActive,Name,Federationidentifier FROM User where LastModifiedDate = TODAY';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<User> scope) 
    {
        system.debug('=============query=======:'+query);
        userinfo= new Map<string,id>();
        groupnametoidmap= new Map<string,id>();
        userid=new Set<String>();
        set<string>unq = new set<string>();
        for(User info: scope)
        {
               userinfo.put(info.Country+'_Market',info.Id);
               userid.add(info.Id);
        }
        system.debug('+++userinfo'+userinfo);
         system.debug('+++userid'+userid);
        List<Group> grp = [SELECT DeveloperName,Id,Name,Type FROM Group where type='Regular'];
         system.debug('+++grp'+grp);
        for(Group g: grp)
        {
            groupnametoidmap.put(g.Name,g.Id);
        }
        system.debug('+++groupnametoidmap'+groupnametoidmap);
        List<GroupMember> listgroupmember = new  List<GroupMember>();
        List<GroupMember> grpmem = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where UserOrGroupId in: userid];
         system.debug('+++grpmem+++'+grpmem);
        if(grpmem!=null && grpmem.size()>0)
            {
                for(GroupMember mem : grpmem)
                    {
                        unq.add(mem.UserOrGroupId);
                    }
                    system.debug('+++unq+++'+unq);
            }

       for(User us: scope)
       {
         system.debug('+++hey+++');
            if(!unq.contains(us.Id))
            {
               string key=us.country+'_Market';
               system.debug('+++key+++'+key);
                if(groupnametoidmap.containsKey(key))
                {
                     system.debug('+++key+++'+key);
                    GroupMember gm = new GroupMember();
                    gm.GroupId= groupnametoidmap.get(key);
                    gm.UserOrGroupId= us.id;
                   listgroupmember.add(gm);
                    system.debug('+++listgroupmember+++'+listgroupmember);
                }

            }
       }
       insert listgroupmember;

    }

    global void finish(Database.BatchableContext BC)
    {

    }
    global void execute(SchedulableContext sc)
    {
         database.executeBatch(new assignPublicGroup(),2000);   
    }
}