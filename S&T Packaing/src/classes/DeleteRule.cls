global with sharing class DeleteRule implements Database.Batchable<sObject> {
    
    public String query;
    public String ruleID;


    global DeleteRule(String rulesfdcID) 
    {
        ruleID = rulesfdcID;
        this.query = 'select id from Account_Compute_Final__c where Measure_Master__c = :ruleID';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        //delete scope;
        SnTDMLSecurityUtil.deleteRecords(scope, 'DeleteRule');
    }

    global void finish(Database.BatchableContext BC) 
    {
        set<string>cmpmaster = new set<string>();
        set<string>stepid =new set<string>();

        list<Measure_Master__c> measurelist = [select id, name from Measure_Master__c where id = :ruleID];

        list<Rule_Parameter__c>rplist= [select id,name ,Step__c from Rule_Parameter__c where Measure_Master__c in:measurelist];

        for(Rule_Parameter__c rp: rplist){
                        stepid.add(rp.Step__c);
        }
        list<Step__c>steplist = [select id,Compute_Master__c from Step__c where id IN: stepid];
        for(Step__c stp : steplist){
                        cmpmaster.add(stp.Compute_Master__c);
        }
        list<Compute_Master__c>cmplist = [select id from Compute_Master__c where ID IN:cmpmaster];

        //delete cmplist;
        SnTDMLSecurityUtil.deleteRecords(cmplist, 'DeleteRule');

        //delete steplist;
        SnTDMLSecurityUtil.deleteRecords(steplist, 'DeleteRule');

        //delete rplist;
        SnTDMLSecurityUtil.deleteRecords(rplist, 'DeleteRule');

        //delete measurelist;
        SnTDMLSecurityUtil.deleteRecords(measurelist, 'DeleteRule');


    }
}