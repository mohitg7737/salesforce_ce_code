@isTest
public class ValidateDropConditionTest 
{
    @istest 
    static void ValidateDropConditiontest()
    {
      String className = 'ValidateDropConditionTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__TriggerContol__c objTriggerControl = new AxtriaSalesIQTM__TriggerContol__c();
        objTriggerControl.Name = 'ValidateDropCondition';
        objTriggerControl.AxtriaSalesIQTM__IsStopTrigger__c = false;
        Insert objTriggerControl;
        
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins2,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins2, countr);
        SnTDMLSecurityUtil.insertRecords(pcc1,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        SnTDMLSecurityUtil.insertRecords(u,className);      
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        //Create Team Instance Config
        Team_Instance_Config__c teamInstConfig = TestDataFactory.createTeamInstConfig(teamins);
        teamInstConfig.Customer_Drop_Restrict_Criteria__c ='AxtriaSalesIQTM__Team_Instance__c != null';
        SnTDMLSecurityUtil.insertRecords(teamInstConfig,className);
        
        System.Test.startTest();

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> FIELD_LIST = new List<String>{nameSpace+'Customer_Drop_Restrict_Criteria__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Team_Instance_Config__c.SObjectType, FIELD_LIST, false));

        System.Test.stopTest();
        
        
        }
}