/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
@isTest(SeeAllData=true)
private class All_BU_Response_Insert_Utility_Test 
{
    private static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        Account acc2 = new Account(Name='test acc2',Marketing_Code__c='EU',AccountNumber='1256');
        insert acc2;
        
        Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',Business_Unit_Loopup__c=bu.id);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,Cycle__c=cycle.id);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,AxtriaARSnT__Country_Lookup__c=country.id);
        insert pc;
        
        Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1111111';
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c(Name='Pos2',AxtriaSalesIQTM__Team_iD__c=team.id);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '2222222';
        insert pos2;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos.id);
        pa.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(-2);
        pa.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(2);
        pa.AxtriaSalesIQTM__Segment_1__c = 'Primary';
        insert pa;
        
        AxtriaSalesIQTM__Position_Account__c pa2 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc2.id,AxtriaSalesIQTM__Position__c=pos2.id);
        pa.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(2);
        pa.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(20);
        pa.AxtriaSalesIQTM__Segment_1__c = '';
        insert pa2;
        
        /*Prepare Staging_BU_Response__c data*/
        Staging_BU_Response__c sbur = new Staging_BU_Response__c();
        sbur.CurrencyIsoCode = 'EUR';
        sbur.Brand_ID__c = pc.Veeva_External_ID__c;
        sbur.Brand__c = bti.id;
        sbur.Business_Unit__c = bu.id;
        sbur.Cycle2__c = cycle.id;
        sbur.Cycle__c = ti.id;
        sbur.External_ID__c = pc.Veeva_External_ID__c + ti.Name + acc.AccountNumber;
        sbur.Is_Active__c = true;
        sbur.Line__c = team.id;
        sbur.Physician__c = acc.id;
        sbur.Response1__c = '1';
        sbur.Response2__c = '2';
        sbur.Response3__c = '3';
        sbur.Response4__c = '4';
        sbur.Response5__c = '5';
        sbur.Response6__c = '6';
        sbur.Response7__c = '7';
        sbur.Response8__c = '8';
        sbur.Response9__c = '9';
        sbur.Response10__c = '10';
        sbur.Team_Instance__c = ti.id;
        sbur.Team__c = team.id;
        insert sbur;
        
        Staging_BU_Response__c sbur2 = new Staging_BU_Response__c();
        sbur2.CurrencyIsoCode = 'EUR';
        sbur2.Brand_ID__c = pc.Veeva_External_ID__c;
        sbur2.Brand__c = bti.id;
        sbur2.Business_Unit__c = bu.id;
        sbur2.Cycle2__c = cycle.id;
        sbur2.Cycle__c = ti.id;
        sbur2.External_ID__c = pc.Veeva_External_ID__c + ti.Name + acc2.AccountNumber;
        sbur2.Is_Active__c = true;
        sbur2.Line__c = team.id;
        sbur2.Physician__c = acc2.id;
        sbur2.Response1__c = '1';
        sbur2.Response2__c = '2';
        sbur2.Response3__c = '3';
        sbur2.Response4__c = '4';
        sbur2.Response5__c = '5';
        sbur2.Response6__c = '6';
        sbur2.Response7__c = '7';
        sbur2.Response8__c = '8';
        sbur2.Response9__c = '9';
        sbur2.Response10__c = '10';
        sbur2.Team_Instance__c = ti.id;
        sbur2.Team__c = team.id;
        insert sbur2;

        AxtriaSalesIQTM__Team__c t1= new AxtriaSalesIQTM__Team__c();
        t1.Name = 'test';

        insert t1;

        String teamid = t1.id;


        AxtriaSalesIQTM__Team_Instance__c t2 =  new AxtriaSalesIQTM__Team_Instance__c();
        t2.name='TestSC1';
        t2.AxtriaSalesIQTM__Team__c = teamid;

        insert t2;

        String teamInstid = t2.id;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /*AxtriaSalesIQTM__Organization_Master__c orgmast = new AxtriaSalesIQTM__Organization_Master__c();

        orgmast.Name='test';
        orgmast.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmast.AxtriaARSnT__Marketing_Code__c = 'test';

        insert orgmast;

        String orgmasterid = orgmast.id;

        AxtriaSalesIQTM__Country__c t4 = new AxtriaSalesIQTM__Country__c();

        t4.Name = 'test';
        t4.AxtriaSalesIQTM__Parent_Organization__c = orgmast.id;
        t4.AxtriaSalesIQTM__Status__c = 'Active';

        insert t4;*/



        Product_Catalog__c testdummy = new Product_Catalog__c();
        testdummy.name='Test';
        testdummy.Veeva_External_ID__c = 'dummytest';
        testdummy.Product_Code__c = 'dummytest';
        testdummy.Team_Instance__c = teamInstid;
        testdummy.Country_Lookup__c = country.id;

        insert testdummy;


        System.test.startTest();
        All_BU_Response_Insert_Utility batch = new All_BU_Response_Insert_Utility(ti.Name);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void secondTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        Account acc2 = new Account(Name='test acc2',Marketing_Code__c='EU',AccountNumber='1256');
        insert acc2;
        
        Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',Business_Unit_Loopup__c=bu.id);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,Cycle__c=cycle.id);
        ti.Include_Secondary_Affiliations__c = true;
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,AxtriaARSnT__Country_Lookup__c=country.id);
        insert pc;
        
        Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1111111';
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c(Name='Pos2',AxtriaSalesIQTM__Team_iD__c=team.id);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '2222222';
        insert pos2;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos.id);
        pa.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(-2);
        pa.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(2);
        pa.AxtriaSalesIQTM__Segment_1__c = 'Primary';
        insert pa;
        
        AxtriaSalesIQTM__Position_Account__c pa2 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos2.id);
        pa.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(2);
        pa.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(20);
        pa.AxtriaSalesIQTM__Segment_1__c = '';
        insert pa2;
        
        /*Prepare Staging_BU_Response__c data*/
        Staging_BU_Response__c sbur = new Staging_BU_Response__c();
        sbur.CurrencyIsoCode = 'EUR';
        sbur.Brand_ID__c = pc.Veeva_External_ID__c;
        sbur.Brand__c = bti.id;
        sbur.Business_Unit__c = bu.id;
        sbur.Cycle2__c = cycle.id;
        sbur.Cycle__c = ti.id;
        sbur.External_ID__c = pc.Veeva_External_ID__c + ti.Name + acc.AccountNumber;
        sbur.Is_Active__c = true;
        sbur.Line__c = team.id;
        sbur.Physician__c = acc.id;
        sbur.Response1__c = 'null';
        sbur.Response2__c = 'null';
        sbur.Response3__c = 'null';
        sbur.Response4__c = 'null';
        sbur.Response5__c = 'null';
        sbur.Response6__c = 'null';
        sbur.Response7__c = 'null';
        sbur.Response8__c = 'null';
        sbur.Response9__c = 'null';
        sbur.Response10__c = 'null';
        sbur.Team_Instance__c = ti.id;
        sbur.Team__c = team.id;
        insert sbur;
        
        Staging_BU_Response__c sbur2 = new Staging_BU_Response__c();
        sbur2.CurrencyIsoCode = 'EUR';
        sbur2.Brand_ID__c = pc.Veeva_External_ID__c;
        sbur2.Brand__c = bti.id;
        sbur2.Business_Unit__c = bu.id;
        sbur2.Cycle2__c = cycle.id;
        sbur2.Cycle__c = ti.id;
        sbur2.External_ID__c = pc.Veeva_External_ID__c + ti.Name + acc2.AccountNumber;
        sbur2.Is_Active__c = true;
        sbur2.Line__c = team.id;
        sbur2.Physician__c = acc2.id;
        sbur2.Team_Instance__c = ti.id;
        sbur2.Team__c = team.id;
        insert sbur2;

        AxtriaSalesIQTM__Team__c t1= new AxtriaSalesIQTM__Team__c();
        t1.Name = 'test';

        insert t1;

        String teamid = t1.id;


        AxtriaSalesIQTM__Team_Instance__c t2 =  new AxtriaSalesIQTM__Team_Instance__c();
        t2.name='TestSC1';
        t2.AxtriaSalesIQTM__Team__c = teamid;

        insert t2;

        String teamInstid = t2.id;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /*AxtriaSalesIQTM__Organization_Master__c orgmast = new AxtriaSalesIQTM__Organization_Master__c();

        orgmast.Name='test';
        orgmast.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmast.AxtriaARSnT__Marketing_Code__c = 'test';

        insert orgmast;

        String orgmasterid = orgmast.id;

        AxtriaSalesIQTM__Country__c t4 = new AxtriaSalesIQTM__Country__c();

        t4.Name = 'test';
        t4.AxtriaSalesIQTM__Parent_Organization__c = orgmast.id;
        t4.AxtriaSalesIQTM__Status__c = 'Active';

        insert t4;*/



        Product_Catalog__c testdummy = new Product_Catalog__c();
        testdummy.name='Test';
        testdummy.Veeva_External_ID__c = 'dummytest';
        testdummy.Product_Code__c = 'dummytest';
        testdummy.Team_Instance__c = teamInstid;
        testdummy.Country_Lookup__c =country.id;

        insert testdummy;

        System.test.startTest();
        All_BU_Response_Insert_Utility batch = new All_BU_Response_Insert_Utility(ti.Name);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
}