/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 23rd September'2020
Description : Test class for BatchUpdateTempObjRecsCR class
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class BatchUpdateTempObjRecsCRTest 
{
    static testMethod void testMethod1() 
    {
    	String className = 'BatchUpdateTempObjRecsCR';
    	AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgMaster,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        SnTDMLSecurityUtil.insertRecords(country,className);

        String workspaceName = 'TestWorkspace';
        Date startDate = Date.newInstance(2020, 2, 17);
        Date endDate = Date.newInstance(2020, 8, 17);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace(workspaceName, startDate, endDate);
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstance.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c rec1 = new temp_Obj__c();
        rec1.Change_Request__c = cr.Id;
        rec1.Status__c = 'Processed';
        SnTDMLSecurityUtil.insertRecords(rec1,className);

        temp_Obj__c rec2 = new temp_Obj__c();
        rec2.Change_Request__c = cr.Id;
        rec2.Status__c = 'Rejected';
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        temp_Obj__c rec3 = new temp_Obj__c();
        rec3.Change_Request__c = cr.Id;
        rec3.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(rec3,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Status__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new BatchUpdateTempObjRecsCR(cr.Id,true,'',''));

        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
    	String className = 'BatchUpdateTempObjRecsCR';
    	AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgMaster,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        SnTDMLSecurityUtil.insertRecords(country,className);

        String workspaceName = 'TestWorkspace';
        Date startDate = Date.newInstance(2020, 2, 17);
        Date endDate = Date.newInstance(2020, 8, 17);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace(workspaceName, startDate, endDate);
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstance.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c rec1 = new temp_Obj__c();
        rec1.Change_Request__c = cr.Id;
        rec1.Status__c = 'Processed';
        SnTDMLSecurityUtil.insertRecords(rec1,className);

        temp_Obj__c rec2 = new temp_Obj__c();
        rec2.Change_Request__c = cr.Id;
        rec2.Status__c = 'Rejected';
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        temp_Obj__c rec3 = new temp_Obj__c();
        rec3.Change_Request__c = cr.Id;
        rec3.Status__c = 'New';
        SnTDMLSecurityUtil.insertRecords(rec3,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Status__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));
        Database.executeBatch(new BatchUpdateTempObjRecsCR(cr.Id,false,'',''));

        System.Test.stopTest();
    }
}