@isTest
public class UpdatePosAcc_Test {

    static testMethod void testMethod1() {
        String className = 'UpdatePosAcc_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        insert workspace;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
        insert emp;
        
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = country.Id;
        insert type;
    
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
                
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';
        team.AxtriaSalesIQTM__Country__c = country.id;        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins.AxtriaSalesIQTM__Geography_Type_Name__c  = type.id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,country);
        insert pcc;
        
        AxtriaSalesIQTM__Position_Product__c posProd =  TestDataFactory.createPositionProduct(teamins,pos,pcc);
        insert posProd;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        posEmp.AxtriaSalesIQTM__Position__c = pos.id;
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        posEmp.AxtriaSalesIQTM__Status__c = 'Approved';
        posEmp.AxtriaSalesIQTM__Effective_start_date__c = date.today();
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posEmp;
        
        Staging_Position_Account__c stagPosAcc = new Staging_Position_Account__c ();
        stagPosAcc.Assignment_Status__c = 'Active';
        stagPosAcc.Record_Status__c = 'Updated';
        stagPosAcc.PositionCode__c = 'test';
        stagPosAcc.AccountNumber__c = 'test';
        stagPosAcc.External_Id__c = 'test';
        stagPosAcc.Account__c = acc.id;
        stagPosAcc.Account_Type__c = 'test';
        stagPosAcc.Association_Cause__c = 'test';
        stagPosAcc.Country__c = country.id;
        stagPosAcc.Position__c = pos.id;
        insert stagPosAcc;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Name = 'Test';
        zt.Agg_API_Name__c = 'Test1';
        zt.Agg_Cond_Value__c = 'Test2';
        zt.Agg_Object__c = 'Test3';
        zt.Agg_Type__c = 'Test4';
        zt.Type_Name__c = 'CR-00000008';
        zt.Display_Column_Order__c = 1;
        zt.Attribute_Display_Name__c = 'Test6';
        zt.Display_Name__c = 'Test7';
        zt.Enable__c = true;
        zt.Enforcable__c = true;
        zt.isOptimum__c = true;
        zt.Custom_Metric__c = true;
        zt.Metric_Name__c = 'test9';
        zt.API_Name__c = 'test10';
        zt.Object_Name__c = 'test11';
        zt.Team_Instance_Name__c = 'test12';
        zt.Threshold_Max__c = 'test13';
        zt.Threshold_Min__c = 'test14';
        zt.Warning_Max__c = 'test15';
        zt.Warning_Min__c = 'test16';
        zt.Tooltip_Message__c = 'test17';
        zt.Visible__c = true;
        zt.Approved_Value_Display_Name__c = 'test';
        zt.Cust_Type__c = 'test18';
        zt.Display_Approved_Value__c = true;
        zt.Display_Optimum_Value__c = true;
        zt.Display_Original_Value__c = true;
        zt.Display_Proposed_Value__c = true;
        zt.isCallCapacity__c = true;
        zt.Optimum_Value_Display_Name__c = 'test19';
        zt.Original_Value_Display_Name__c = 'test20';
        zt.Proposed_Value_Display_Name__c = 'test21';
        zt.Object__c = 'Acc_Terr';
        zt.Status__c = 'New';
        zt.isError__c = true;
        zt.Error_Message__c = 'Incorrect Team Instance present';
        zt.Team_Instance__c = teamins.id;
        zt.SalesIQ_Error_Message__c ='Incorrect Team Instance present';
        zt.Team__c = team.id;
        zt.Event__c = 'Insert';
        zt.Weights__c= 1;
        zt.Full_Load__c= true;
        zt.Product_Type__c= 'test';
        zt.Portfolio_Name__c= 'test';
        zt.Country_Code__c= 'test';
        zt.Product_Description__c= 'test19';
        zt.Detail_Group__c= 'test20';
        zt.Team_Instance_Text__c= 'test21';
        zt.AccountNumber__c= 'test';
        zt.Position_Code__c= 'test';
        zt.Parent_Account_ID__c= 'test';
        zt.Team_Instance_Name__c= 'testteaminsname';
        zt.Product_Name__c= 'test';
        zt.Product_Code__c='test';
        zt.Change_Request__c= cr.id;
        zt.Team_Name__c = 'ONCO';
        zt.Account__c = acc.id;
        zt.Territory__c = pos.id;
        zt.Account_Type__c = 'test';
        insert zt;
        
        Scheduler_Log__c sclogs= new Scheduler_Log__c();
        sclogs.Cycle__c = 'Full Load';
        sclogs.Job_Name__c = 'TeamInstance';
        sclogs.Country__c = country.id;
        insert sclogs;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__TriggerContol__c trigControl = new AxtriaSalesIQTM__TriggerContol__c();
        trigControl.Name= 'PositionGeographyTrigger';
        trigControl.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert trigControl;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdatePosAcc obj=new UpdatePosAcc(team.Id,teamins.id);
            //obj.query = 'SELECT id,AccountNumber__c,Team_Name__c,Account_Type__c,Territory_ID__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
     static testMethod void testMethod2() {
        String className = 'UpdatePosAcc_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        insert workspace;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
        insert emp;
        
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = country.Id;
        insert type;
    
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
                
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';
        team.AxtriaSalesIQTM__Country__c = country.id;        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins.AxtriaSalesIQTM__Geography_Type_Name__c  = type.id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,country);
        insert pcc;
        
        AxtriaSalesIQTM__Position_Product__c posProd =  TestDataFactory.createPositionProduct(teamins,pos,pcc);
        insert posProd;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        posEmp.AxtriaSalesIQTM__Position__c = pos.id;
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        posEmp.AxtriaSalesIQTM__Status__c = 'Approved';
        posEmp.AxtriaSalesIQTM__Effective_start_date__c = date.today();
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posEmp;
        
        Staging_Position_Account__c stagPosAcc = new Staging_Position_Account__c ();
        stagPosAcc.Assignment_Status__c = 'Active';
        stagPosAcc.Record_Status__c = 'Updated';
        stagPosAcc.PositionCode__c = 'test';
        stagPosAcc.AccountNumber__c = 'test';
        stagPosAcc.External_Id__c = 'test';
        stagPosAcc.Account__c = acc.id;
        stagPosAcc.Account_Type__c = 'test';
        stagPosAcc.Association_Cause__c = 'test';
        stagPosAcc.Country__c = country.id;
        stagPosAcc.Position__c = pos.id;
        insert stagPosAcc;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Name = 'Test';
        zt.Agg_API_Name__c = 'Test1';
        zt.Agg_Cond_Value__c = 'Test2';
        zt.Agg_Object__c = 'Test3';
        zt.Agg_Type__c = 'Test4';
        zt.Type_Name__c = 'CR-00000008';
        zt.Display_Column_Order__c = 1;
        zt.Attribute_Display_Name__c = 'Test6';
        zt.Display_Name__c = 'Test7';
        zt.Enable__c = true;
        zt.Enforcable__c = true;
        zt.isOptimum__c = true;
        zt.Custom_Metric__c = true;
        zt.Metric_Name__c = 'test9';
        zt.API_Name__c = 'test10';
        zt.Object_Name__c = 'test11';
        zt.Team_Instance_Name__c = 'test12';
        zt.Threshold_Max__c = 'test13';
        zt.Threshold_Min__c = 'test14';
        zt.Warning_Max__c = 'test15';
        zt.Warning_Min__c = 'test16';
        zt.Tooltip_Message__c = 'test17';
        zt.Visible__c = true;
        zt.Approved_Value_Display_Name__c = 'test';
        zt.Cust_Type__c = 'test18';
        zt.Display_Approved_Value__c = true;
        zt.Display_Optimum_Value__c = true;
        zt.Display_Original_Value__c = true;
        zt.Display_Proposed_Value__c = true;
        zt.isCallCapacity__c = true;
        zt.Optimum_Value_Display_Name__c = 'test19';
        zt.Original_Value_Display_Name__c = 'test20';
        zt.Proposed_Value_Display_Name__c = 'test21';
        zt.Object__c = 'Acc_Terr';
        zt.Status__c = 'New';
        zt.isError__c = true;
        zt.Error_Message__c = 'Incorrect Team Instance present';
        zt.Team_Instance__c = teamins.id;
        zt.SalesIQ_Error_Message__c ='Incorrect Team Instance present';
        zt.Team__c = team.id;
        zt.Event__c = 'Insert';
        zt.Weights__c= 1;
        zt.Full_Load__c= true;
        zt.Product_Type__c= 'test';
        zt.Portfolio_Name__c= 'test';
        zt.Country_Code__c= 'test';
        zt.Product_Description__c= 'test19';
        zt.Detail_Group__c= 'test20';
        zt.Team_Instance_Text__c= 'test21';
        zt.AccountNumber__c= 'test';
        zt.Position_Code__c= 'test';
        zt.Parent_Account_ID__c= 'test';
        zt.Team_Instance_Name__c= 'testteaminsname';
        zt.Product_Name__c= 'test';
        zt.Product_Code__c='test';
        zt.Change_Request__c= cr.id;
        zt.Team_Name__c = 'ONCO';
        zt.Account__c = null;
        zt.Territory__c = pos.id;
        zt.Account_Type__c = 'test';
        insert zt;
        
        Scheduler_Log__c sclogs= new Scheduler_Log__c();
        sclogs.Cycle__c = 'Full Load';
        sclogs.Job_Name__c = 'TeamInstance';
        sclogs.Country__c = country.id;
        insert sclogs;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__TriggerContol__c trigControl = new AxtriaSalesIQTM__TriggerContol__c();
        trigControl.Name= 'PositionGeographyTrigger';
        trigControl.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert trigControl;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdatePosAcc obj=new UpdatePosAcc(team.Id,teamins.id,cr.id);
            //obj.query = 'SELECT id,AccountNumber__c,Team_Name__c,Account_Type__c,Territory_ID__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
     static testMethod void testMethod3() {
        String className = 'UpdatePosAcc_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        insert workspace;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
        insert emp;
        
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = country.Id;
        insert type;
    
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
                
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';
        team.AxtriaSalesIQTM__Country__c = country.id;        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins.AxtriaSalesIQTM__Geography_Type_Name__c  = type.id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,country);
        insert pcc;
        
        AxtriaSalesIQTM__Position_Product__c posProd =  TestDataFactory.createPositionProduct(teamins,pos,pcc);
        insert posProd;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        posEmp.AxtriaSalesIQTM__Position__c = pos.id;
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        posEmp.AxtriaSalesIQTM__Status__c = 'Approved';
        posEmp.AxtriaSalesIQTM__Effective_start_date__c = date.today();
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posEmp;
        
        Staging_Position_Account__c stagPosAcc = new Staging_Position_Account__c ();
        stagPosAcc.Assignment_Status__c = 'Active';
        stagPosAcc.Record_Status__c = 'Updated';
        stagPosAcc.PositionCode__c = 'test';
        stagPosAcc.AccountNumber__c = 'test';
        stagPosAcc.External_Id__c = 'test';
        stagPosAcc.Account__c = acc.id;
        stagPosAcc.Account_Type__c = 'test';
        stagPosAcc.Association_Cause__c = 'test';
        stagPosAcc.Country__c = country.id;
        stagPosAcc.Position__c = pos.id;
        insert stagPosAcc;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Data Load Backend';
        cr.Records_Created__c = 1;
        insert cr;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Name = 'Test';
        zt.Agg_API_Name__c = 'Test1';
        zt.Agg_Cond_Value__c = 'Test2';
        zt.Agg_Object__c = 'Test3';
        zt.Agg_Type__c = 'Test4';
        zt.Type_Name__c = 'CR-00000008';
        zt.Display_Column_Order__c = 1;
        zt.Attribute_Display_Name__c = 'Test6';
        zt.Display_Name__c = 'Test7';
        zt.Enable__c = true;
        zt.Enforcable__c = true;
        zt.isOptimum__c = true;
        zt.Custom_Metric__c = true;
        zt.Metric_Name__c = 'test9';
        zt.API_Name__c = 'test10';
        zt.Object_Name__c = 'test11';
        zt.Team_Instance_Name__c = 'test12';
        zt.Threshold_Max__c = 'test13';
        zt.Threshold_Min__c = 'test14';
        zt.Warning_Max__c = 'test15';
        zt.Warning_Min__c = 'test16';
        zt.Tooltip_Message__c = 'test17';
        zt.Visible__c = true;
        zt.Approved_Value_Display_Name__c = 'test';
        zt.Cust_Type__c = 'test18';
        zt.Display_Approved_Value__c = true;
        zt.Display_Optimum_Value__c = true;
        zt.Display_Original_Value__c = true;
        zt.Display_Proposed_Value__c = true;
        zt.isCallCapacity__c = true;
        zt.Optimum_Value_Display_Name__c = 'test19';
        zt.Original_Value_Display_Name__c = 'test20';
        zt.Proposed_Value_Display_Name__c = 'test21';
        zt.Object__c = 'Acc_Terr';
        zt.Status__c = 'New';
        zt.isError__c = true;
        zt.Error_Message__c = 'Incorrect Team Instance present';
        zt.Team_Instance__c = teamins.id;
        zt.SalesIQ_Error_Message__c ='Incorrect Team Instance present';
        zt.Team__c = team.id;
        zt.Event__c = 'Insert';
        zt.Weights__c= 1;
        zt.Full_Load__c= true;
        zt.Product_Type__c= 'test';
        zt.Portfolio_Name__c= 'test';
        zt.Country_Code__c= 'test';
        zt.Product_Description__c= 'test19';
        zt.Detail_Group__c= 'test20';
        zt.Team_Instance_Text__c= 'test21';
        zt.AccountNumber__c= 'test';
        zt.Position_Code__c= 'test';
        zt.Parent_Account_ID__c= 'test';
        zt.Team_Instance_Name__c= 'testteaminsname';
        zt.Product_Name__c= 'test';
        zt.Product_Code__c='test';
        zt.Change_Request__c= cr.id;
        zt.Team_Name__c = 'ONCO';
        zt.Account__c = acc.id;
        zt.Territory__c = null;
        zt.Account_Type__c = 'test';
        insert zt;
        
        Scheduler_Log__c sclogs= new Scheduler_Log__c();
        sclogs.Cycle__c = 'Full Load';
        sclogs.Job_Name__c = 'TeamInstance';
        sclogs.Country__c = country.id;
        insert sclogs;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__TriggerContol__c trigControl = new AxtriaSalesIQTM__TriggerContol__c();
        trigControl.Name= 'PositionGeographyTrigger';
        trigControl.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert trigControl;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            UpdatePosAcc obj=new UpdatePosAcc(team.Id,teamins.id,cr.id,true);
            //obj.query = 'SELECT id,AccountNumber__c,Team_Name__c,Account_Type__c,Territory_ID__c FROM temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}