global with sharing class CopyProductCatalog 
{  
	public String query;
  public String sourceTeamInstance;
  public String destinationTeamInstance;
  List<AxtriaSalesIQTM__Team_Instance__c> teamInst;
  List<Product_Catalog__c> productList = new List<Product_Catalog__c>();
  Set<string> teaminstancetoproductcodeset;

  global CopyProductCatalog(String teaminstance, String destTeamInstance)
  {
   sourceTeamInstance = teaminstance;
   destinationTeamInstance = destTeamInstance;
   query= 'SELECT Name,Product_code__c,Product_type__c,Detail_Group__c, Country_Lookup__c,Full_load__c,veeva_external_id__c,team_instance__c FROM Product_Catalog__c where team_instance__c =:sourceTeamInstance and IsActive__c=true';
   copyProduct();
 }

 global void copyProduct()
 {
  SnTDMLSecurityUtil.printDebugMessage('=============query=======:'+query);
  list<Product_Catalog__c>scope = Database.query(query);
  teaminstancetoproductcodeset=new Set<String>();
    	//mapteaminstancetoproductcode= new map<string,string>();
  List<AxtriaSalesIQTM__Team_Instance__c> teamInst = [Select Id,Name, AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id =: destinationTeamInstance];
  SnTDMLSecurityUtil.printDebugMessage('++teamInst++'+teamInst);
  List<Product_Catalog__c> prodDest = [SELECT Name,Product_code__c, Country_Lookup__c,Product_type__c,Detail_Group__c,Full_load__c,Veeva_External_Id__c,team_instance__c,Team_Instance__r.name FROM Product_Catalog__c where team_instance__c =:destinationTeamInstance and IsActive__c=true];
  SnTDMLSecurityUtil.printDebugMessage('++prodDest++'+prodDest);
  for(Product_Catalog__c product : prodDest)
  {
    SnTDMLSecurityUtil.printDebugMessage('++product.Team_Instance__r.name++'+product.Team_Instance__r.name);
    teaminstancetoproductcodeset.add(product.Product_Code__c);
  }
  SnTDMLSecurityUtil.printDebugMessage('++teaminstancetoproductcodeset++'+teaminstancetoproductcodeset);
  for(Product_Catalog__c prod: scope)
  { 
   Product_Catalog__c pc = new Product_Catalog__c();
   string key= pc.Product_Code__c;
   if(!teaminstancetoproductcodeset.contains(key))
   {                 
     pc= prod.clone();
     pc.Team_Instance__c = teamInst[0].id;
     pc.Country_Lookup__c=teamInst[0].AxtriaSalesIQTM__Country__c;
     productList.add(pc);
     
   } 
 }	
 SnTDMLSecurityUtil.printDebugMessage('++++productList+++'+productList);
 if(!productList.isEmpty())
 {
           //INSERT productList;
   SnTDMLSecurityUtil.insertRecords(productList, 'CopyProductCatalog');
 }
 CopyPositionProduct copyPosProduct = new CopyPositionProduct(sourceTeamInstance,destinationTeamInstance);
 database.executeBatch(copyPosProduct,2000);
}

}