/**********************************************************************************************
@author     : Ritu Pandey
@date       : 24 August 2018
@description: 1. This Batch Class is used for fill the employee lookup with user record. 
@             If the employee lookup is blank it will match the PRID in User, if found, lookup is created.
              2. This Batch Assigns a basic permission set as well as fills the user lookup in employee object.
@Client     : AZ 
Revison(s)  : 
**********************************************************************************************/

global class BatchUpdateUserLookupinEmp implements Database.Batchable<sObject>, Database.Stateful {
    public list<AxtriaSalesIQTM__Employee__c> lsUpdEmployee;
    public list<sObject> lsPermissionset = new list<sObject>();
    public list<string> setUserSFID ;
    public string query = '';
    public list<sObject> permissionToInsert;
    global BatchUpdateUserLookupInEmp (set<string> setUserIds)
    {
        setUserSFID = new list<string>(setUserIds);
       //lsPermissionset = Database.query('SELECT Id,Name,Label FROM PermissionSet where Label like \'%RM%\' ');
        query = 'Select id,Employee_PRID__c,AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__Employee__c';
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
        system.debug('inside getQueryLocator ');
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Employee__c> EmpList)
    {
        set<string> setPRID = new set<string>();
        map<string,string> setPRID2UserIds = new map<string,string>();
        lsUpdEmployee = new list<AxtriaSalesIQTM__Employee__c>();
        permissionToInsert = new list<sObject>();
        
        for(AxtriaSalesIQTM__Employee__c emp:EmpList)
        {
            setPRID.add(emp.Employee_PRID__c);
        }
        system.debug('+++setPRID+'+setPRID);
        
        list<User> lsUsers = Database.query('Select id,FederationIdentifier from User where FederationIdentifier in:setPRID');
         system.debug('+++lsUsers+'+lsUsers);
        for(User urs:lsUsers )
        {
            if(urs.get('FederationIdentifier')!=null)
            {
                setPRID2UserIds.put(String.Valueof(urs.get('FederationIdentifier')),String.Valueof(urs.get('id')));
            }
        }
        system.debug('+++setPRID2UserIds++'+setPRID2UserIds);
        
        for(AxtriaSalesIQTM__Employee__c em:EmpList)
        {
            AxtriaSalesIQTM__Employee__c updEmp = new AxtriaSalesIQTM__Employee__c(id=em.id);
            
            if(em.Employee_PRID__c!=null && setPRID2UserIds.containsKey(em.Employee_PRID__c))
            {
                if(em.AxtriaSalesIQTM__User__c==null )
                {
                    updEmp.AxtriaSalesIQTM__User__c = setPRID2UserIds.get(em.Employee_PRID__c);
                    lsUpdEmployee.add(updEmp);
                }
            }
        }
         system.debug('+++lsUpdEmployee++'+lsUpdEmployee);
        
        
        //Create Permission set Assignment
        /*Integer i = 0;
        for(sObject permission: lsPermissionset){
            
            if(i<lsPermissionset.size())
            {
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permission.id, AssigneeId = setUserSFID[i]);
                permissionToInsert.add(psa);
                i++; 
             }
             system.debug('++permissionToInsert++'+permissionToInsert);
             
        }*/
        
        if(lsUpdEmployee.size()!=0){
            update lsUpdEmployee;
        }
        
        /*if(permissionToInsert.size()!=0){
            insert permissionToInsert;
        }*/
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}