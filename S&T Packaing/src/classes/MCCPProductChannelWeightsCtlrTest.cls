/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 9th August'2020
Description : Test class for MCCPProductChannelWeightsCtlr
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class MCCPProductChannelWeightsCtlrTest 
{
    public static testMethod void testMethod1() 
    {
        String className = 'MCCPProductChannelWeightsCtlrTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='In Progress';
        mmc.Target_Count_Maximum__c = 100;
        mmc.Target_Count_Minimun__c = 20;
        mmc.Workload_Hours__c = 8;
        mmc.Desired_Range__c = '-20';
        mmc.Single_Product_Rule__c = true;
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        //Mock Product Data
        MCCP_CNProd__c rec = new MCCP_CNProd__c();
        rec.Call_Plan__c = mmc.Id;
        rec.Product__c = 'PROD1';
        rec.Weights__c = 50;
        rec.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Product').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec,className);

        //Mock Channel Data
        MCCP_CNProd__c rec2 = new MCCP_CNProd__c();
        rec2.Call_Plan__c = mmc.Id;
        rec2.CE_Priority1__c = 1;
        rec2.CE_Priority2__c = 1;
        rec2.WLE_TP_PerHour__c = 8;
        rec2.Channel__c = 'Email';
        rec2.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Channel').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        //Mock Junction Data
        MCCP_CNProd__c rec3 = new MCCP_CNProd__c();
        rec3.Call_Plan__c = mmc.Id;
        rec3.Product__c = 'PROD1';
        rec3.Channel__c = 'Email';
        rec3.OptimisationSelected__c = true;
        rec3.PromotionSelected__c = true;
        rec3.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Junction').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec3,className); //Insert MCCP CN Prod Data

        Channel_Info__c ci = new Channel_Info__c();
        ci.Channel_Name__c = 'Email';
        ci.Channel_Effectiveness_P1__c = '10';
        ci.Channel_Effectiveness_P2__c = '5';
        ci.Country__c = countr.Id;
        ci.Team_Instance__c = mmc.Team_Instance__c;
        SnTDMLSecurityUtil.insertRecords(ci,className); //Insert MCCP CN Prod Data

        //Test getting data for the selected Business Rule
        MCCPProductChannelWeightsCtlr.getBusinessRuleData(null);
        MCCPProductChannelWeightsCtlr.getBusinessRuleData('abcd');
        MCCPProductChannelWeightsCtlr.getBusinessRuleData(mmc.Id);

        //Test resetting channel data for the selected Business Rule
        MCCPProductChannelWeightsCtlr.resetChannelEffectivenessData(null);
        MCCPProductChannelWeightsCtlr.resetChannelEffectivenessData('abcd');
        MCCPProductChannelWeightsCtlr.resetChannelEffectivenessData(mmc.Id);

        //Create mock data for saving BR
        MCCPProductChannelWeightsCtlr.BusinessRuleDataWrapper brw = new MCCPProductChannelWeightsCtlr.BusinessRuleDataWrapper();
        brw.ruleData = mmc;
        brw.productData = new List<MCCP_CNProd__c>();
        brw.channelData = new List<MCCP_CNProd__c>();
        brw.productData.add(rec);
        brw.channelData.add(rec2);

        MCCPProductChannelWeightsCtlr.ProductChannelWrapper pcw = new MCCPProductChannelWeightsCtlr.ProductChannelWrapper();
        brw.junctionData = new List<MCCPProductChannelWeightsCtlr.ProductChannelWrapper>();
        pcw.setProducts = new Set<String>{'Prod1'};
        pcw.setChannels = new Set<String>{'Email'};
        pcw.channelProductList = new Map<String,List<MCCP_CNProd__c>>();

        Map<String,List<MCCP_CNProd__c>> tempMap = new Map<String,List<MCCP_CNProd__c>>();
        tempMap.put('Email',new List<MCCP_CNProd__c>{rec3});
        pcw.channelProductList.putAll(tempMap);
        brw.junctionData.add(pcw);

        String testString = JSON.serialize(brw);

        //Test saving data for the selected Business Rule
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData('');
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData('abcd');
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData(testString);

        System.Test.stopTest();
    }
}