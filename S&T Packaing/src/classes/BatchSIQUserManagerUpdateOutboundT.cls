@isTest
public class BatchSIQUserManagerUpdateOutboundT {
    static testMethod void testMethod1() {
        String className = 'BatchSIQUserManagerUpdateOutboundT';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        insert acc1;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        
        Set<String> s1= new Set<String>();
        s1.add('USA');
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        insert teamins;
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        insert teamins2;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.AxtriaSalesIQTM__HR_Status__c ='Active';
        emp.AxtriaSalesIQTM__FirstName__c = 'test';
        emp.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert emp;

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        pos1.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos1.AxtriaSalesIQTM__Inactive__c = false;
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos1.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos1.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        pos1.AxtriaSalesIQTM__IsMaster__c = true;
        insert pos1;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert posAccount1;
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert positionAccountCallPlan;
       /* AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan1 = TestDataFactory.createPositionAccountCallPlan(mmc,acc1,teamins2,posAccount1,pPriority,pos1);
        positionAccountCallPlan1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan1.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan1.Final_TCF__c = 3.0;
        positionAccountCallPlan1.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert positionAccountCallPlan1;*/
        temp_Obj__c newpa= new temp_Obj__c();
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        insert newpa;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posEmp.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        //posEmp.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
        posEmp.AxtriaSalesIQTM__Position__c = pos.id;
        //posEmp.Country_Code__c ='USA';
        insert posEmp;
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchSIQUserManagerUpdateOutbound obj=new BatchSIQUserManagerUpdateOutbound(s1); 
            //obj.query = 'select id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}