trigger PrimaryAffiliationIndicator on AxtriaSalesIQTM__Account_Affiliation__c (after insert,after update) {
    
    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
    exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('PrimaryAffiliationIndicator')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('PrimaryAffiliationIndicator'):null;
    
    system.debug('execute trigger' +exeTrigger );
    //public static Boolean executeTrigger; 
    if(exeTrigger != null){
     if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true){
     if(PrimaryAffiliationIndicatorHandler.executeTrigger){
        System.debug('Call trigger');
        if(trigger.isInsert){
            PrimaryAffiliationIndicatorHandler.insertTrigger(trigger.new);
          }
        
        if(trigger.isUpdate){
           PrimaryAffiliationIndicatorHandler.updateTrigger(trigger.new);
     }
    }
    }
   }
}