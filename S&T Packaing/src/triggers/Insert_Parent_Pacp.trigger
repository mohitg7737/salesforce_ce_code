trigger Insert_Parent_Pacp on AxtriaSalesIQTM__Position_Account_Call_Plan__c(before insert) {
       
    if(AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp') != null && !AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp').AxtriaSalesIQTM__IsStopTrigger__c){

        InsertParentPacpUtility.createParentPos(trigger.new);
    }
}