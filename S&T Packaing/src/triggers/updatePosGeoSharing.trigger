trigger updatePosGeoSharing on AxtriaSalesIQTM__Scenario__c (After Update) 
{
    
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('updatePosGeoSharing');
    List<AxtriaSalesIQTM__Scenario__c> updateCheckbox = new List<AxtriaSalesIQTM__Scenario__c>();
    

     if(!Test.isRunningTest())
     {
        Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ;      
        if(myCCVal != true)
        { 
        system.debug('True Value');
            for(AxtriaSalesIQTM__Scenario__c sc : trigger.new)
            {             
                if(sc.AxtriaSalesIQTM__Request_Process_Stage__c == 'CIM Ready')
                {
                    //updatePosGeoAZ.updatePosGeo(sc.AxtriaSalesIQTM__Team_Instance__c);
                    Database.executeBatch(new updatePosGeoAZ(sc.AxtriaSalesIQTM__Team_Instance__c));
                }
                
               
            }
        }
    }
}