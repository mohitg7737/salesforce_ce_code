trigger ValidateDropCondition on Team_Instance_Config__c (before insert, before update) {
    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
    exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('ValidateDropCondition')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('ValidateDropCondition'):null;
    system.debug('execute trigger' +exeTrigger );
    public Boolean flag = false;

    if(exeTrigger != null && exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true){
        String query = 'Select count(id) From AxtriaSalesIQTM__Position_Account_Call_Plan__c Where AxtriaSalesIQTM__Team_Instance__c != null  ';
        for(Team_Instance_config__c ticObj:trigger.new)
        {
            if(!String.isBlank(ticObj.Customer_Drop_Restrict_Criteria__c)){
                query += ' and '+ ticObj.Customer_Drop_Restrict_Criteria__c;
                System.debug('Query :: '+query);
                flag = true;
            }
        }

        if(flag)
        {
            try{
                System.debug('Query :: '+query);
                Database.query(query);
            }catch(Exception ex){
                System.debug('Exception in ValidateDropCondition :: '+ex.getMessage());
                for(Team_Instance_config__c ticObj:trigger.new){
                    ticObj.adderror(System.Label.Validate_Drop_Criteria_Error_Message+ '<br/>' + ex,false);
                }
            }            
        }

    }
}