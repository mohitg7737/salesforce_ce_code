trigger PositionProductTrigger on AxtriaSalesIQTM__Position_Product__c (after update, before insert, before update) {
    
    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
    public Set<String> externalIDSet = new Set<String>();
    public Set<String> existedIDSet = new Set<String>();
    public List<AxtriaSalesIQTM__Position_Product__c> updateposProList = new List<AxtriaSalesIQTM__Position_Product__c>();


    if(trigger.isUpdate && trigger.isAfter)
    {
        if(AxtriaSalesIQTM__TriggerContol__c.getValues('UpdateCallcapacityCimconfig') != null && !AxtriaSalesIQTM__TriggerContol__c.getValues('UpdateCallcapacityCimconfig').AxtriaSalesIQTM__IsStopTrigger__c)
        {
            List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPosMetricAllList = new List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
            List<String> allPositions = new List<String>();
            List<String> allTeamInstances = new List<String>();
            Map<String, Decimal> callCapacity = new Map<String, Decimal>();

            for(AxtriaSalesIQTM__Position_Product__c posProd : trigger.new)
            {
                allPositions.add(posProd.AxtriaSalesIQTM__Position__c);
                allTeamInstances.add(posProd.AxtriaSalesIQTM__Team_Instance__c);
                callCapacity.put(posProd.AxtriaSalesIQTM__Position__c+'_'+posProd.AxtriaSalesIQTM__Team_Instance__c, (posProd.Call_Capacity_Formula__c));
            }

            for (AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimPosMetricList : [Select Id,AxtriaSalesIQTM__Optimum__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c  From AxtriaSalesIQTM__CIM_Position_Metric_Summary__c Where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances And AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c in :allPositions AND AxtriaSalesIQTM__CIM_Config__r.IsCallCapacity__c = true])
            {
                if(callCapacity.containsKey(cimPosMetricList.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c +'_'+cimPosMetricList.AxtriaSalesIQTM__Team_Instance__c))
                {
                    cimPosMetricList.AxtriaSalesIQTM__Optimum__c = callCapacity.get(cimPosMetricList.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c +'_'+cimPosMetricList.AxtriaSalesIQTM__Team_Instance__c);

                    cimPosMetricAllList.add(cimPosMetricList );
                }
            }
            update cimPosMetricAllList;  
        }  
    }
    

    if((trigger.isUpdate || trigger.isInsert) && trigger.isBefore)
    {
        exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('FillExternalIDonPosProduct')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('FillExternalIDonPosProduct'):null;
        
        system.debug('execute trigger' +exeTrigger );

        if(exeTrigger != null)
        {
            if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
            {
                System.debug('Call trigger');
                

                if(trigger.isInsert)
                {
                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        externalIDSet.add(externalID);
                        System.debug('externalIDSet:::::::' +externalIDSet);
                    }

                    updateposProList = [Select Id,External_ID__c from AxtriaSalesIQTM__Position_Product__c where External_ID__c in :externalIDSet and AxtriaSalesIQTM__isActive__c = true];

                    if(updateposProList.size() > 0)
                    {
                        for(AxtriaSalesIQTM__Position_Product__c posProrec : updateposProList)
                        {
                            existedIDSet.add(posProrec.External_ID__c);
                            System.debug('existedIDSet:::::::' +existedIDSet);
                        }
                    }

                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        if(!existedIDSet.contains(externalID))
                        {
                            rec.External_ID__c = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                            System.debug('rec:::::::' +rec);
                        }
                        else
                        {
                            rec.addError('External ID already exist');
                            System.debug('External ID already exist');
                        }   
                    }

                }

                if(trigger.isUpdate)
                {
                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        if(externalID != trigger.oldMap.get(rec.ID).External_ID__c)
                        {
                            rec.External_ID__c = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;   
                            System.debug('rec:::::::' +rec);
                        }
                    }
                }
            }
        }
        else
        {
            if(trigger.isInsert)
                {
                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        externalIDSet.add(externalID);
                        System.debug('externalIDSet:::::::' +externalIDSet);
                    }

                    updateposProList = [Select Id,External_ID__c from AxtriaSalesIQTM__Position_Product__c where External_ID__c in :externalIDSet and AxtriaSalesIQTM__isActive__c = true];

                    if(updateposProList.size() > 0)
                    {
                        for(AxtriaSalesIQTM__Position_Product__c posProrec : updateposProList)
                        {
                            existedIDSet.add(posProrec.External_ID__c);
                            System.debug('existedIDSet:::::::' +existedIDSet);
                        }
                    }

                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        if(!existedIDSet.contains(externalID))
                        {
                            rec.External_ID__c = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                            System.debug('rec:::::::' +rec);
                        }
                        else
                        {
                            rec.addError('External ID already exist');
                            System.debug('External ID already exist');
                        }   
                    }

                }

                if(trigger.isUpdate)
                {
                    for(AxtriaSalesIQTM__Position_Product__c rec : trigger.new)
                    {
                        string externalID = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;
                        if(externalID != trigger.oldMap.get(rec.ID).External_ID__c)
                        {
                            rec.External_ID__c = rec.AxtriaSalesIQTM__Position__c + '_' + rec.Product_Catalog__c;   
                            System.debug('rec:::::::' +rec);
                        }
                    }
                }
        }
    }

        
}