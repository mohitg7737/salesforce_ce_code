global with sharing class BatchCallPlanSummaryReports implements Database.Batchable<sObject>,database.stateful {
    public String query;
    public String teaminstanceis ;
    public string ruleIdis ;
    Boolean fifthFlag;
    public string Ids;
    Boolean flag= true;
    public boolean directFlag=false;
    public boolean directFlag2=false;
    public list<AxtriaSalesIQTM__Team_Instance__c>teaminstancelist ;
    public string teaminstancename ;

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;

    global BatchCallPlanSummaryReports(string teaminstance,string ruleID) {
        system.debug('================BatchCallPlanSummaryReports======== :: ');
        this.query = query;
        teaminstanceis = '';
        teaminstanceis = teaminstance;
        ruleIdis = '';
        ruleIdis = ruleID;
        teaminstancelist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teaminstancename = '';
        teaminstancelist=[select id,name from AxtriaSalesIQTM__Team_Instance__c where id=:teaminstanceis];
        if(teaminstancelist !=null && teaminstancelist.size()>0){
          teaminstancename = teaminstancelist[0].name;
        }
        this.query  = createQuery();
        system.debug('=========createQuery() returnd :====='+query);
        this.query += 'WHERE AxtriaSalesIQTM__Team_Instance__c =:teaminstanceis and Rule__c=:ruleIdis and AxtriaSalesIQTM__lastApprovedTarget__c = true';

    }

    //Added by HT(A0944) on 12th June 2020
    global BatchCallPlanSummaryReports(string teaminstance,String loggerID,String alignNmspPrefix) 
    {
        system.debug('================BatchCallPlanSummaryReports======== :: ');
        directFlag=true;
        this.query = query;
        teaminstanceis = '';
        teaminstanceis = teaminstance;
        teaminstancename = '';
        teaminstancename=teaminstanceis;

        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        //ruleIdis = '';
        //ruleIdis = ruleID;
        this.query  = createQuery();
        system.debug('=========createQuery() returnd :====='+query);
        this.query += 'WHERE AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstanceis  and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        //and Rule__c=:ruleIdis

    }

    global BatchCallPlanSummaryReports(string teaminstance) {
        system.debug('================BatchCallPlanSummaryReports======== :: ');
        directFlag=true;
        this.query = query;
        teaminstanceis = '';
        teaminstanceis = teaminstance;
        teaminstancename = '';
        teaminstancename=teaminstanceis;
        //ruleIdis = '';
        //ruleIdis = ruleID;
        this.query  = createQuery();
        system.debug('=========createQuery() returnd :====='+query);
        this.query += 'WHERE AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstanceis  and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        //and Rule__c=:ruleIdis

    }
    
    global BatchCallPlanSummaryReports(string teaminstance,string Ids, Boolean fifthFlag) {
        system.debug('================BatchCallPlanSummaryReports======== :: ');
        directFlag=true;
        this.Ids = Ids;
        this.fifthFlag = fifthFlag;
        this.query = query;
        teaminstanceis = '';
        teaminstanceis = teaminstance;
        teaminstancename = '';
        teaminstancename=teaminstanceis;
        //ruleIdis = '';
        //ruleIdis = ruleID;
        this.query  = createQuery();
        system.debug('=========createQuery() returnd :====='+query);
        this.query += 'WHERE AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstanceis  and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        //and Rule__c=:ruleIdis

    }
    
    global BatchCallPlanSummaryReports() {
        directFlag2 = true;
        this.query = query;
       }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=========Query inside start method ::'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
      try
      {
          map<id,AxtriaSalesIQTM__Position_Account_Call_Plan__c>oldmap = new map<id,AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
          for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
              oldmap.put(pacp.id,pacp);
              if(pacp.AxtriaSalesIQTM__Team_Instance__c!=null){
                  teaminstanceis=pacp.AxtriaSalesIQTM__Team_Instance__c;
              }
          }
          system.debug('=================CALLING TRIGGER HANDLER METHODS+++++++++++++++++++++');
          CallPlanSummaryTriggerHandler.afterInsert(scope, scope,OldMap);
          //CallPlanSummaryTriggerHandler.afterUpdate(scope, scope,OldMap);
          system.debug('=================CALLED TRIGGER HANDLER METHODS+++++++++++++++++++++');
      }
      catch(Exception e)
      {
          //Added by HT(A0994) on 12th June 2020
          SnTDMLSecurityUtil.printDebugMessage('Error in BatchCallPlanSummaryReports : execute()-->'+e.getMessage());
          SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
          proceedNextBatch = false;
          SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

          if(callPlanLoggerID!=null && callPlanLoggerID!=''){
              SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at creating Call Plan '+
                                                          'Summary Reports','Error',alignNmsp);
          }
          //End
      } 
    }

    global void finish(Database.BatchableContext BC) 
    {
      //calling the check user position controller to enable the iscall plan  check and call capacity custom settings and cim config 
      system.debug('============calling finish method');
      List<AxtriaSalesIQTM__Team_Instance__c> ti = [select id, Name, Product_Type__c from AxtriaSalesIQTM__Team_Instance__c where Name = :teaminstancename];
       if(ti.size()>0)
        {
            if(ti[0].Product_Type__c)
            {
              Database.executeBatch(new AssignProductType(ti[0].id),2000);  
            }
            
        }
        
      List<Report_Page_Configuration__c> allReportsPages = new List<Report_Page_Configuration__c>();
      allReportsPages = [ select Report_Name__c from Report_Page_Configuration__c where Type__c = 'Call After Summary'];
      
      SYSTEM.DEBUG('++++++++++ Hey RPC '+ allReportsPages);
      if(allReportsPages.size() > 0)
      {
          Callable extension = (Callable) Type.forName('',allReportsPages[0].Report_Name__c).newInstance();
            Map<String,Object> myObj = new Map<String,Object>();
            myObj.put('teamInstanceName', ti[0].Name);

          Extension.call(allReportsPages[0].Report_Name__c, myObj);
      }
      
      try
      {
        CallPlanSummaryTriggerHandler updatetotal = new CallPlanSummaryTriggerHandler();
        updatetotal.createTotalOnPositionwiseArea(teaminstanceis);
        CreateParentCounters parentrecs= new CreateParentCounters();
        parentrecs.createwospecrecords(teaminstanceis);

        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

        if(callPlanLoggerID!=null && callPlanLoggerID!=''){
          SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Copy Call Plan data job successfully completed','Success',alignNmsp);
        }
       
      }
      catch(Exception e)
      {
        System.debug('Exception occurs here :: '+e);
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

        if(callPlanLoggerID!=null && callPlanLoggerID!=''){
            SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at creating Call Plan Summary Reports','Error',alignNmsp);
        }
      }

       /*finally
       {
        List<Report_Page_Configuration__c> allReportsPages = new List<Report_Page_Configuration__c>();
      allReportsPages = [ select Report_Name__c from Report_Page_Configuration__c where Type__c = 'Call After Summary'];
      
      SYSTEM.DEBUG('++++++++++ Hey RPC '+ allReportsPages);
      if(allReportsPages.size() > 0)
      {
          Callable extension = (Callable) Type.forName('',allReportsPages[0].Report_Name__c).newInstance();
            Map<String,Object> myObj = new Map<String,Object>();
            myObj.put('teamInstanceName', ti[0].Name);

          Extension.call(allReportsPages[0].Report_Name__c, myObj);
      }

     }*/
     
      Checkuserpositions cup = new Checkuserpositions(teaminstancename);
    }

    private string createQuery(){
        String query  = 'SELECT ';
               query += 'Id, Name, AxtriaSalesIQTM__Account_Alignment_type__c, ';
               query += 'AxtriaSalesIQTM__Account__c, ';
               query += 'AxtriaSalesIQTM__Alignment_Change_Action__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Approved__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Original__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Updated__c, ';
               query += 'AxtriaSalesIQTM__Calls_Approved__c, ';
               query += 'AxtriaSalesIQTM__Calls_Original__c, ';
               query += 'AxtriaSalesIQTM__Calls_Updated__c, ';
               query += 'AxtriaSalesIQTM__Change_Status__c, ';
               query += 'AxtriaSalesIQTM__CheckBox1_Approved__c, ';
               query += 'AxtriaSalesIQTM__CheckBox1_Updated__c, ';
               query += 'AxtriaSalesIQTM__CheckBox2_Approved__c, ';
               query += 'AxtriaSalesIQTM__CheckBox2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox10__c, ';
               query += 'AxtriaSalesIQTM__Checkbox1__c, ';
               query += 'AxtriaSalesIQTM__Checkbox2__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5__c, ';
               query += 'AxtriaSalesIQTM__Checkbox6__c, ';
               query += 'AxtriaSalesIQTM__Checkbox7__c, ';
               query += 'AxtriaSalesIQTM__Checkbox8__c, ';
               query += 'AxtriaSalesIQTM__Checkbox9__c, ';
               query += 'AxtriaSalesIQTM__Comments__c, ';
               query += 'AxtriaSalesIQTM__Effective_End_Date__c, ';
               query += 'AxtriaSalesIQTM__Effective_Start_Date__c, ';
               query += 'AxtriaSalesIQTM__Metric10__c, ';
               query += 'AxtriaSalesIQTM__Metric1_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric1__c, ';
               query += 'AxtriaSalesIQTM__Metric2_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric2__c, ';
               query += 'AxtriaSalesIQTM__Metric3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric3__c, ';
               query += 'AxtriaSalesIQTM__Metric4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric4__c, ';
               query += 'AxtriaSalesIQTM__Metric5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric5__c, ';
               query += 'AxtriaSalesIQTM__Metric6__c, ';
               query += 'AxtriaSalesIQTM__Metric7__c, ';
               query += 'AxtriaSalesIQTM__Metric8__c, ';
               query += 'AxtriaSalesIQTM__Metric9__c, ';
               query += 'AxtriaSalesIQTM__NexavarCalls__c, ';
               query += 'AxtriaSalesIQTM__Nexavar_Original__c, ';
               query += 'AxtriaSalesIQTM__OverAll_Target__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric__c, ';
               query += 'AxtriaSalesIQTM__Position_Team_Instance__c, ';
               query += 'AxtriaSalesIQTM__Position__c, ';
               query += 'AxtriaSalesIQTM__Rank__c, ';
               query += 'AxtriaSalesIQTM__ReasonAdd__c, ';
               query += 'AxtriaSalesIQTM__ReasonDrop__c, ';
               query += 'AxtriaSalesIQTM__Sample_Flag__c, ';
               query += 'AxtriaSalesIQTM__Segment10__c, ';
               query += 'AxtriaSalesIQTM__Segment1_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment1__c, ';
               query += 'AxtriaSalesIQTM__Segment2_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment2__c, ';
               query += 'AxtriaSalesIQTM__Segment3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment3__c, ';
               query += 'AxtriaSalesIQTM__Segment4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment4__c, ';
               query += 'AxtriaSalesIQTM__Segment5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment5__c, ';
               query += 'AxtriaSalesIQTM__Segment6__c, ';
               query += 'AxtriaSalesIQTM__Segment7__c, ';
               query += 'AxtriaSalesIQTM__Segment8__c, ';
               query += 'AxtriaSalesIQTM__Segment9__c, ';
               query += 'AxtriaSalesIQTM__Source__c, ';
               query += 'AxtriaSalesIQTM__StivargaCalls__c, ';
               query += 'AxtriaSalesIQTM__Stivarga_Original__c, ';
               query += 'AxtriaSalesIQTM__Target_Type__c, ';
               //query += 'AxtriaSalesIQTM__Team_Instance_Account__c, ';
               query += 'AxtriaSalesIQTM__Team_Instance__c, ';
               query += 'AxtriaSalesIQTM__WasEverInCallPlan__c, ';
               query += 'AxtriaSalesIQTM__isAccountTarget__c, ';
               query += 'AxtriaSalesIQTM__isAddedFromAlignment__c, ';
               query += 'AxtriaSalesIQTM__isModified__c, ';
               query += 'AxtriaSalesIQTM__isincludedCallPlan__c, ';
               query += 'AxtriaSalesIQTM__lastApprovedTarget__c, ';
               query += 'AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c, ';
               query += 'Party_ID__c, ';
               query += 'Calculated_TCF__c, ';
               query += 'Proposed_TCF__c, ';
               query += 'Final_TCF__c, ';
               query += 'Segment__c, ';
               query += 'P1__c, ';
               query += 'P2__c, ';
               query += 'P3__c, ';
               query += 'P4__c, ';
               query += 'TCF_P1__c, ';
               query += 'TCF_P2__c, ';
               query += 'TCF_P3__c, ';
               query += 'TCF_P4__c, ';
               query += 'DYNAMIC_DPP_IV__c, ';
               query += 'DYNAMIC_SGLT2__c, ';
               query += 'DYNAMIC_DAPA__c, ';
               query += 'POTENTIAL_INN_ORAL__c, ';
               query += 'KOL_DAPA__c, ';
               query += 'Parameter1__c, ';
               query += 'Parameter2__c, ';
               query += 'Parameter3__c, ';
               query += 'Parameter4__c, ';
               query += 'Parameter5__c, ';
               query += 'Parameter6__c, ';
               query += 'isparmmodified__c, ';
               //query += 'Brand_Team_Instance__c, ';
               query += 'Speciality__c, ';
               query += 'isLocked__c, ';
               query += 'Product1__c, ';
               query += 'Product2__c, ';
               query += 'Product3__c, ';
               query += 'Product4__c, ';
               query += 'Parameter7__c, ';
               query += 'Parameter8__c, ';
               //query += 'Line__c, ';
               query += 'Parameter1_Original__c, ';
               query += 'Parameter2_Original__c, ';
               query += 'Parameter3_Original__c, ';
               query += 'Parameter4_Original__c, ';
               query += 'Parameter5_Original__c, ';
               query += 'Parameter6_Original__c, ';
               query += 'Parameter7_Original__c, ';
               query += 'Parameter8_Original__c, ';
               query += 'Parameter1_Approved__c, ';
               query += 'Parameter2_Approved__c, ';
               query += 'Parameter3_Approved__c, ';
               query += 'Parameter4_Approved__c, ';
               query += 'Parameter5_Approved__c, ';
               query += 'Parameter6_Approved__c, ';
               query += 'Parameter7_Approved__c, ';
               query += 'Parameter8_Approved__c, ';
               query += 'Initial_Count__c, ';
               query += 'Updated_Count__c, ';
               query += 'Count_Final_TCF__c, ';
               query += 'CL_Count__c, ';
               query += 'A_Count__c, ';
               query += 'B_Count__c, ';
               query += 'C_Count__c, ';
               query += 'ND_Count__c, ';
               query += 'Share__c, ';
               query += 'P2_Parameter1__c, ';
               query += 'P2_Parameter2__c, ';
               query += 'P2_Parameter3__c, ';
               query += 'P2_Parameter4__c, ';
               query += 'P2_Parameter5__c, ';
               query += 'P2_Parameter6__c, ';
               query += 'P2_Parameter7__c, ';
               query += 'P2_Parameter8__c, ';
               query += 'P3_Parameter1__c, ';
               query += 'P3_Parameter2__c, ';
               query += 'P3_Parameter3__c, ';
               query += 'P3_Parameter4__c, ';
               query += 'P3_Parameter5__c, ';
               query += 'P3_Parameter6__c, ';
               query += 'P3_Parameter7__c, ';
               query += 'P3_Parameter8__c, ';
               query += 'P4_Parameter1__c, ';
               query += 'P4_Parameter2__c, ';
               query += 'P4_Parameter3__c, ';
               query += 'P4_Parameter4__c, ';
               query += 'P4_Parameter5__c, ';
               query += 'P4_Parameter6__c, ';
               query += 'P4_Parameter7__c, ';
               query += 'P4_Parameter8__c, ';
               query += 'P2_Parameter1_Original__c, ';
               query += 'P2_Parameter2_Original__c, ';
               query += 'P2_Parameter3_Original__c, ';
               query += 'P2_Parameter4_Original__c, ';
               query += 'P2_Parameter5_Original__c, ';
               query += 'P2_Parameter6_Original__c, ';
               query += 'P2_Parameter7_Original__c, ';
               query += 'P2_Parameter8_Original__c, ';
               query += 'P3_Parameter1_Original__c, ';
               query += 'P3_Parameter2_Original__c, ';
               query += 'P3_Parameter3_Original__c, ';
               query += 'P3_Parameter4_Original__c, ';
               query += 'P3_Parameter5_Original__c, ';
               query += 'P3_Parameter6_Original__c, ';
               query += 'P3_Parameter7_Original__c, ';
               query += 'P3_Parameter8_Original__c, ';
               query += 'P4_Parameter1_Original__c, ';
               query += 'P4_Parameter2_Original__c, ';
               query += 'P4_Parameter3_Original__c, ';
               query += 'P4_Parameter4_Original__c, ';
               query += 'P4_Parameter5_Original__c, ';
               query += 'P4_Parameter6_Original__c, ';
               query += 'P4_Parameter7_Original__c, ';
               query += 'P4_Parameter8_Original__c, ';
               query += 'teamcall__c, ';
               query += 'allcall__c, ';
               query += 'Final_TCF_Approved__c, ';
               query += 'Final_TCF_Original__c, ';
               query += 'Record_Updated__c, ';
               query += 'Rule__c, ';
               query += 'isFinalTCFApproved__c, ';
               query += 'CL_finaltcf__c, ';
               query += 'C_Finaltcf_c__c, ';
               query += 'A_Finaltcf_c__c, ';
               query += 'B_Finaltcf_c__c, ';
               query += 'ND_Finaltcf_c__c, ';
               query += 'isClone__c, ';
               query += 'Adoption__c, ';
               query += 'Potential__c, ';
               query += 'Calculated_TCF2__c, ';
               query += 'Proposed_TCF2__c, ';
               query += 'Segment2__c, ';
               query += 'Accessibility_Range__c, ';
               //query += 'Brand_Name__c, ';
               query += 'isproductswapped__c, ';
               query += 'initialflag__c, ';
               query += 'P1_Original__c, ';
               query += 'is_LoggedIn__c, ';
               query += 'Account_HCP_NUmber__c, ';
               query += 'IsDMlogged__c, ';
               query += 'concatPosTeamInstance__c, ';
               query += 'Consent__c, ';
               query += 'Rep_Name__c, ';
               query += 'HCP__c, ';
               query += 'Area__c, ';
               query += 'Consent_Value__c, ';
               query += 'CountOriginal__c, ';
               query += 'CountApproved__c, ';
               query += 'CountTCForiginal__c, ';
               query += 'CountTCFapproved__c, ';
               query += 'Segment_Value_Name__c, ';
               query += 'Event__c, ';
               query += 'Hospital_Formula__c, ';
               query += 'CL1__c, ';
               query += 'CL2__c ';
               query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
        return query;
    }
}