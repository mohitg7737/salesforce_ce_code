global class WorkbenchlightningCtrl_1
{
    public list<CR_Employee_Feed__c> cremplist{get;set;}
    
    public String countryID {get;set;}
    
    @AuraEnabled
    public static String getLastBatchRun()
    {
        Datetime lastRun = [Select CreatedDate from CR_Employee_Feed__c Order by CreatedDate desc Limit 1].CreatedDate;
        return lastRun.format('MM/dd/yyyy HH:mm a');
    }
    
    @AuraEnabled
    public static CountWrapper GetCount(string countryId)
    {    
        string country = countryId;
        system.debug('INSIDE GETCOUNT--'+country);
        CountWrapper wrapper=new CountWrapper();
        wrapper.terminate_Employee  = 0;

        wrapper.promote_Employee    = 0; 
        wrapper.EmployeeNewHire     = 0;
        wrapper.TransferEmployee    = 0;
        wrapper.Employeedemotion    = 0;
        wrapper.Transfer_to_HO      = 0;
        wrapper.TransfertoField     = 0;
        wrapper.TransferoutOfSales  = 0;
        wrapper.allEvents           = 0;
        wrapper.rehireCount         = 0;
       //wrapper.returnFromLeaveCount = 0;
       //wrapper.completedCount = 0;
        //wrapper.actionRequiredCount = 0;
        
        /*list<CR_Employee_Feed__c> crList = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false];
        wrapper.allEvents = crList.size();
        
        for(CR_Employee_Feed__c cr : crList){
            if(cr.Event_Name__c == system.label.Terminate_Employee){
                wrapper.terminate_Employee++;
            }
            if(cr.Event_Name__c == system.label.Transfer_to_HO){
                wrapper.Transfer_to_HO++;
            }
            if(cr.Event_Name__c == system.label.Leave_of_Absence){
                wrapper.leave_of_Absence++;
            }
            if(cr.Event_Name__c == system.label.Promote_Employee){
                wrapper.promote_Employee++;
            }
            if(cr.Event_Name__c == system.label.Employee_NewHire){
                wrapper.EmployeeNewHire++;
            }
            if(cr.Event_Name__c == system.label.Transfer_Employee){
                wrapper.TransferEmployee++ ;
            }
            if(cr.Event_Name__c == system.label.Transfer_to_Field){
                wrapper.TransfertoField++;
            }
            if(cr.Event_Name__c == system.label.Rehire){
                wrapper.rehireCount++;
            }
            if(cr.Event_Name__c == system.label.Return_From_Leave){
                wrapper.returnFromLeaveCount++;
            }
            
            system.debug('rehire count ++++' + wrapper.rehireCount );
        }*/
        
        
        list<CR_Employee_Feed__c> crList = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country];
        wrapper.allEvents = crList.size();
        list<CR_Employee_Feed__c> crListNewHire = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false and Event_Name__c = 'Employee NewHire' and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country];
        wrapper.EmployeeNewHire = crListNewHire.size();
        list<CR_Employee_Feed__c> crListTerminated = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false and Event_Name__c = 'Terminate Employee' and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country];
        wrapper.terminate_Employee = crListTerminated.size();
        list<CR_Employee_Feed__c> crListTransfer = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false and Event_Name__c = 'Transfer Employee' and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country];
        wrapper.TransferEmployee = crListTransfer.size();
        return wrapper;
    }
    
    
    @AuraEnabled
    public static String getCountryAttributes(String countryId){
        string country = '';
        List<AxtriaSalesIQTM__Country__c> countryList = [select Name, AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Country_Flag__c , AxtriaSalesIQTM__Status__c from AxtriaSalesIQTM__Country__c where id =: country];
        system.debug('===Country===>'+countryId);
        return countryList[0].name+','+countryList[0].AxtriaSalesIQTM__Country_Flag__c;
        
    }
        @AuraEnabled 
    public static map<String,list<String>> getProfileNameMap(){
        map<String,list<String>> profileNameMap = new map<String,list<String>>();
     //   profileNameMap.put('Sales Ops',SalesIQProfile('Sales Ops'));
        profileNameMap.put('HO',SalesIQProfile('HO'));
        return profileNameMap;
    }  
    
    private static list<String> SalesIQProfile(string ProfileName){
        
        //string SalesIQProfile='System Administrator';   
        list<String> SalesIQProfile = new list<String>{'System Administrator'};
        String profiles = '';
        if(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName)!=null ){
            if(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c') !=null && String.Valueof(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c'))!=''){
                profiles = String.Valueof(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c'));
            }           
        } 
        if(!string.isBlank(profiles)){
            SalesIQProfile.addAll(profiles.split(';'));
        }

        return SalesIQProfile;
    }
    
    public void methodOne(){}
    
    @AuraEnabled
    public static string isStatusMarkCompleted(string crfeedid){
        
        list<CR_Employee_Feed__c> updatecreList=new list<CR_Employee_Feed__c>();
        list<CR_Employee_Feed__c> existingcreList = [select status__c from CR_Employee_Feed__c where id =:crfeedid];
        for(CR_Employee_Feed__c cr : existingcreList)
        {   
            cr.status__c='Completed Action';
            updatecreList.add(cr); 
        }
        System.debug('+++++Crelist+++++ ' + updatecreList);
        
        Update updatecreList;
            
        return '';
    } 
    
    @AuraEnabled
    public static string isRemovedEventfromWB(string rowID)
    {  
        list<string>feedIdList=new list<string>();
        feedIdList=rowID.split(',');
        if(rowID!=null&& rowID!='') {
            set<Id> setEmployeeId = new set<Id>();
            set<Id> setOtherEmployeeId = new set<Id>();
            list<CR_Employee_Feed__c> updatecreList=new list<CR_Employee_Feed__c>();
            list<AxtriaSalesIQTM__Employee__c> updateEmployeeList=new list<AxtriaSalesIQTM__Employee__c>();
            list<CR_Employee_Feed__c> existingcreList = [select IsRemoved__c, Employee__r.Id from CR_Employee_Feed__c where id in:feedIdList];
            list<CR_Employee_Feed__c> othercreList = [select IsRemoved__c, Employee__r.Id from CR_Employee_Feed__c where IsRemoved__c = false and id not in:feedIdList];
            for(CR_Employee_Feed__c cr : existingcreList)
            {
                cr.IsRemoved__c=true;
                updatecreList.add(cr);
                setEmployeeId.add(cr.Employee__r.Id);
            }
            for(CR_Employee_Feed__c cr : othercreList){
                setOtherEmployeeId.add(cr.Employee__r.Id);
            }
            for(AxtriaSalesIQTM__Employee__c emp : [Select Id, IsWorkbench__c from AxtriaSalesIQTM__Employee__c where Id in :setEmployeeId]){
                if(!setOtherEmployeeId.contains(emp.Id)){
                    emp.IsWorkbench__c = false;
                    updateEmployeeList.add(emp);
                }
            }
            
            System.debug('+++++Crelist+++++ ' + updatecreList);
            update updatecreList;
            update updateEmployeeList;
        }
        return '';
    }
    @AuraEnabled //get Account Industry Picklist Values
    public static Map<String, String> getCountry(){
        Map<String, String> options = new Map<String, String>();
        for(AxtriaSalesIQTM__User_Access_Permission__c up : [Select AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Id From AxtriaSalesIQTM__User_Access_Permission__c Where AxtriaSalesIQTM__User__c = :userInfo.getUserId() and AxtriaSalesIQTM__Is_Active__c = true and AxtriaSalesIQTM__Team_Instance__c != null]){
           options.put(up.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Id, up.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name);
        }
        return options;
    }
    
    public static string getCountryId(){
    //    string country = [Select AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Id From AxtriaSalesIQTM__User_Access_Permission__c Where AxtriaSalesIQTM__User__c = :userInfo.getUserId() and AxtriaSalesIQTM__Is_Active__c = true and AxtriaSalesIQTM__Team_Instance__c != null Limit 1].AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Id;
    /*    string countryID = AxtriaSalesIQTM.SalesIQUtility.getCookie(AxtriaSalesIQTM.SalesIQUtility.getCountryCookieName());
        map<string,string> successErrorMap;
        system.debug('##### countryID ' + countryID);
        successErrorMap = AxtriaSalesIQTM.SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
         
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');
        }
            system.debug('COUNTRYID------'+countryID);
         //   system.debug('COUNTRY------'+country); 
         countryID;*/
        return '';
    }
    
    @AuraEnabled
    public static List<wrapperReport_1> Feed(string eventType,String countryID)
    {   
        string country = countryID;
        system.debug('Country ID _____________'+countryID);
        String actionType;
        list<wrapperReport_1> wrapperlist = new list<wrapperReport_1>();
        list<CR_Employee_Feed__c> cremplistNew= new list<CR_Employee_Feed__c>();
        cremplistNew = [Select id,Position__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c
                        FROM CR_Employee_Feed__c 
                        where  IsRemoved__c=false and Employee__r.AxtriaSalesIQTM__Country__c =: country order by id];
      
        set<string> empData= new set<string>();
        map<string,CR_Employee_Feed__c> mapEmp= new map<string,CR_Employee_Feed__c>();
        
        //Get all employees from workday and their respective object
        for(CR_Employee_Feed__c emp : cremplistNew){
            empData.add(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c);
            mapEmp.put(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c, emp);
        }
        //Get data from position employee
        map<string, AxtriaSalesIQTM__Position__c> mapPos= new map<string, AxtriaSalesIQTM__Position__c>();
        //AxtriaSalesIQTM__Team_Instance__r.isRoster__c = true AND
        for(AxtriaSalesIQTM__Position__c pos : [select id, AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                                AxtriaSalesIQTM__Parent_Position__c from AxtriaSalesIQTM__Position__c
                                                where  AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c in: empData and AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__c =: country])
        {
            
            mapPos.put(pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, pos);
        }
        System.debug('mapPos+++= ' +  mapPos);
        
        list<CR_Employee_Feed__c> lstFeed= new list<CR_Employee_Feed__c>();
        for(string empId:mapPos.keyset())
        {
            AxtriaSalesIQTM__Position__c pos=mapPos.get(empId);
            CR_Employee_Feed__c feed=mapEmp.get(empId);
            feed.Position__c =pos.id;
            lstFeed.add(feed);
        }
        update lstFeed;
        
        ////
        cremplistNew= new list<CR_Employee_Feed__c>();
        system.debug('eventType---' + eventType);
        /*
        if(eventType == 'Completed'){
            actionType = 'Completed Action' ;
            eventType = '';
        }
        else if(eventType == 'Action Required'){
            actionType = 'Pending Action';  
            eventType = '';
        }*/
        
        if(string.isNotBlank(eventType)){
            system.debug('INSIDE EVENT TYPE -----'+eventType);
        cremplistNew = [Select id, ChangeRequest__r.Name,Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c,IsRemoved__c, Request_Date1__c,Employee__r.AxtriaSalesIQTM__Current_Territory__r.name,
                            Position__r.AxtriaSalesIQTM__Position_ID__c,Position__r.Name,Employee__r.AxtriaSalesIQTM__Current_Territory__c,
                            Position__r.id, Employee__r.Name, Employee__r.Id,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                            Event_Name__c, Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c, 
                            Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c ,Employee__r.AxtriaSalesIQTM__Job_Title__c,
                            Employee__r.AxtriaSalesIQTM__Original_Hire_Date__c,
                            Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c,Leave_Start_Date__c,Rehire_Date__c,
                            Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Name,Termination_Date__c,Created_Date__c
                            FROM CR_Employee_Feed__c                          
                            where  IsRemoved__c=false and Event_Name__c =:eventType/* and Employee__r.IsActive__c = True*/ and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country order by id];
        
        }/*
        else if(string.isNotBlank(actionType)){
        cremplistNew = [Select id,
                            ChangeRequest__r.Name,Employee__r.Project_Description_Allocated__c,Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c,Event_Name_for_Workbench__c, IsRemoved__c, Request_Date1__c,Employee__r.Territory_ID__c, Employee__r.AxtriaSalesIQTM__Current_Territory__r.name,
                            Position__r.AxtriaSalesIQTM__Position_ID__c,Position__r.Name,Employee__r.AxtriaSalesIQTM__Current_Territory__c,
                            Position__r.id, Employee__r.Name, Employee__r.Id,Employee__r.IsActive__c,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                            Event_Name__c, Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c, 
                            Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c ,Employee__r.AxtriaSalesIQTM__Job_Title__c,
                            Employee__r.AxtriaSalesIQTM__Original_Hire_Date__c,Employee__r.Leave_Start_Date__c,Employee__r.Return_From_Leave__c,
                            Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c,Employee__r.Promotion_Date__c,Hire_Date__c,Leave_Start_Date__c,Rehire_Date__c,
                            Return_From_Leave__c,Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Name,Termination_Date__c,Promotion_Date__c,Employee__r.Client_Tagged__c,Employee__r.Project_Ext_ID_Allocated__c,Employee__r.Project_Allocated__c,Employee__r.Client_Name_Tagged__c,Transfer_Date__c
                            FROM CR_Employee_Feed__c                          
                            where IsRemoved__c=false and  Status__c = :actionType
                            order by id];
        }*/
        else{
            cremplistNew = [Select id,
                            ChangeRequest__r.Name,Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c,IsRemoved__c, Request_Date1__c,Employee__r.AxtriaSalesIQTM__Current_Territory__r.name,
                            Position__r.AxtriaSalesIQTM__Position_ID__c,Position__r.Name,Employee__r.AxtriaSalesIQTM__Current_Territory__c,
                            Position__r.id, Employee__r.Name, Employee__r.Id,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                            Event_Name__c, Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c, 
                            Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c ,Employee__r.AxtriaSalesIQTM__Job_Title__c,
                            Employee__r.AxtriaSalesIQTM__Original_Hire_Date__c,
                            Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c,Leave_Start_Date__c,Rehire_Date__c,
                            Termination_Date__c,Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Name,Created_Date__c
                            FROM CR_Employee_Feed__c                          
                            where IsRemoved__c=false/*and Employee__r.IsActive__c = True*/ and Employee__r.AxtriaSalesIQTM__Country_Name__c =: country order by id];
        }
        wrapperReport_1 wrap;
        System.debug(' cremplistNew ++' + cremplistNew );
        
        for(CR_Employee_Feed__c cref : cremplistNew)
        {
            String ids = string.ValueOf(cref.id);
            String Remove = string.valueOf(cref.IsRemoved__c);
            Datetime tempdt     = cref.Request_Date1__c;    
            String Request_Date1 = tempdt != null ? tempdt.formatGmt('MM/dd/yyyy') : ''; //yyyy/MM/dd
            
            //Datetime tempdt1     = cref.Hire_Date__c;
            //String Hire_Date1 = tempdt1 != null ? tempdt1.formatGmt('MM/dd/yyyy') : '';
            
            Datetime tempdt2     = cref.Leave_Start_Date__c;
            String Leave_Start_Date = tempdt2 != null ? tempdt2.formatGmt('MM/dd/yyyy') : '';
            
           // Datetime tempdt3     = cref.Return_From_Leave__c;
           // String Return_From_Leave = tempdt3 != null ? tempdt3.formatGmt('MM/dd/yyyy') : '';
            
            Datetime tempdt4     = cref.Termination_Date__c;
            String Termination_Date = tempdt4 != null ? tempdt4.formatGmt('MM/dd/yyyy') : '';
            
           // Datetime tempdt5     = cref.Assignment_Start_Date__c;
           // String Assignment_Start_Date = tempdt5 != null ? tempdt5.formatGmt('MM/dd/yyyy') : '';
            
           // Datetime tempdt6     = cref.Assignment_End_Date__c;
           // String Assignment_End_Date = tempdt6 != null ? tempdt6.formatGmt('MM/dd/yyyy') : '';
            
           // Datetime tempdt7     = cref.Transfer_Date__c;
           // String Transfer_Date = tempdt7 != null ? tempdt7.formatGmt('MM/dd/yyyy') : '';
            
            Datetime tempdt8     = cref.Created_Date__c;
            String Created_Date = tempdt8 != null ? tempdt8.formatGmt('MM/dd/yyyy') : '';
            
            //Datetime tempdt9     = cref.Completed_Date__c;
           // String Completed_Date = tempdt9 != null ? tempdt9.formatGmt('MM/dd/yyyy') : '';
            
            
           // String position_id = string.ValueOf(cref.Employee__r.Territory_ID__c);
            String employee_id = string.ValueOf(cref.Employee__r.Id);
            
            wrap = new wrapperReport_1(
                                        ids,
                                        cref.ChangeRequest__r.Name, 
                                        Remove,
                                        Request_Date1, 
                                        //cref.Employee__r.Territory_ID__c, 
                                        cref.Employee__r.AxtriaSalesIQTM__Current_Territory__r.name,
                                        //position_id, 
                                        cref.Employee__r.Name,   
                                        employee_id,
                                        cref.Training_Completion_Date1__c, 
                                        cref.Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                                        cref.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c, 
                                        cref.Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                        //cref.Event_Name_for_Workbench__c,
                                        cref.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Name,
                                        cref.Employee__r.AxtriaSalesIQTM__Manager__r.Name, 
                                        cref.Status__c ,
                                        cref.Employee__r.AxtriaSalesIQTM__Job_Title__c,
                                        //Hire_Date1,
                                        Leave_Start_Date,
                                        //Return_From_Leave,
                                        Termination_Date,
                                        //Assignment_Start_Date,
                                        cref.Employee__c,
                                        cref.Employee__r.AxtriaSalesIQTM__Current_Territory__c,
                                        cref.Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                        //cref.Employee__r.Client_Name_Tagged__c,
                                        //Assignment_End_Date,
                                        //Transfer_Date,
                                        //cref.Employee__r.Client_Tagged__c,
                                        //cref.Employee__r.Project_Ext_ID_Allocated__c,
                                        //cref.Employee__r.Project_Description_Allocated__c,
                                        Created_Date
                                        //Completed_Date
                                    );
            
            wrapperlist.add(wrap);
            system.debug('==wrapperlist=='+wrapperlist);
        }
        return  wrapperlist;
    }
    
       
    global class wrapperReport_1
    {
         @AuraEnabled
        global String ids {get;set;}
         @AuraEnabled
        global String Name {get;set;}
         @AuraEnabled
        global String IsRemoved {get;set;}
         @AuraEnabled
        global String Request_Date1 {get;set;}
         /*@AuraEnabled
        global String Territory_ID {get;set;}*/
         @AuraEnabled
        global String Position_Name {get;set;}
         /*@AuraEnabled
        global String Position_id {get;set;}*/
         @AuraEnabled
        global String Employee_Name {get;set;}
         @AuraEnabled
        global String Employee_record_Id {get;set;}
         @AuraEnabled
        global String Training_Completion_Date1 {get;set;}
         @AuraEnabled       
        global String Assignment_Type {get;set;}
         @AuraEnabled
        global String Position_Type {get;set;}
         @AuraEnabled
        global String Employee_ID {get;set;}
         /*@AuraEnabled
        global String Event_Name {get;set;}*/
         @AuraEnabled
        global String Parent_Position_Name {get;set;}
         /*@AuraEnabled
        global String Field_Force {get;set;}*/
         /*@AuraEnabled
        global String Employee_Division {get;set;}*/
         @AuraEnabled
        global String Manager_Name {get;set;}
         @AuraEnabled
        global String Status {get;set;}
         @AuraEnabled
        global String Job_Title {get;set;}
        /*@AuraEnabled
        global String Hire_Date {get;set;}*/
        @AuraEnabled
        global String Leave_Date {get;set;}
        /*@AuraEnabled
        global String Return_from_Date {get;set;}*/
        @AuraEnabled
        global String Termination_Date {get;set;}
        /*@AuraEnabled
        global String Assignment_Start_Date {get;set;}*/
        @AuraEnabled
        global String Employee_lookup {get;set;}
        @AuraEnabled
        global String Position_lookup {get;set;}
        @AuraEnabled
        global String Syneos_Employee_Id {get;set;}
        /*@AuraEnabled
        global String Client {get;set;}
        @AuraEnabled
        global String Assignment_End_Date {get;set;}
        @AuraEnabled
        global String Transfer_Date {get;set;}
        @AuraEnabled
        global String EBS_Project {get;set;}
        @AuraEnabled
        global String Project_Ext_ID {get;set;}
        @AuraEnabled
        global String EBS_Project_Name {get;set;}
        @AuraEnabled*/
        global String Created_Date {get;set;}
        /*@AuraEnabled
        global String Completed_Date {get;set;}*/
        
        
        // Constructor
        public  wrapperReport_1(   String ids, 
                                 String Name, 
                                 String IsRemoved, 
                                 String Request_Date1, 
                                 //String Territory_ID,
                                 String Position_Name, 
                                 //String Position_id,
                                 String Employee_Name,
                                 String Employee_record_Id, 
                                 String Training_Completion_Date1, 
                                 String Assignment_Type, 
                                 String Position_Type, 
                                 String Employee_ID, 
                                 //String Event_Name, 
                                 String Parent_Position_Name, 
                                 String Manager_Name, 
                                 String Status,
                                 String Job_Title,
                                 //String Hire_Date,
                                 String Leave_Date,
                                 //String Return_from_Date,
                                 String Termination_Date,
                                 //String Assignment_Start_Date,
                                 String Employee_lookup,
                                 String Position_lookup,
                                 String Syneos_Employee_Id,
                                 /*String Client,
                                 String Assignment_End_Date,
                                 String Transfer_Date,
                                 String EBS_Project,*/
                                 //String Project_Ext_ID,
                                 //String EBS_Project_Name,
                                 String Created_Date
                                 //String Completed_Date
                                 
                              )
        {
             this.ids                         = ids; 
             this.Name                        = Name;
             this.IsRemoved                   = IsRemoved;
             this.Request_Date1               = Request_Date1;
             //this.Territory_ID                = Territory_ID;
             this.Position_Name               = Position_Name;
             //this.Position_id                 = Position_id;
             this.Employee_Name               = Employee_Name;
             this.Employee_record_Id          = Employee_record_Id;
             this.Training_Completion_Date1   = Training_Completion_Date1;
             this.Assignment_Type             = Assignment_Type;
             this.Position_Type               = Position_Type;
             this.Employee_ID                 = Employee_ID;
             //this.Event_Name                  = Event_Name;
             this.Parent_Position_Name        = Parent_Position_Name;
             //this.Field_Force                 = Field_Force;
             //this.Employee_Division           = Employee_Division;
             this.Manager_Name                = Manager_Name;
             this.Status                      = Status;
             this.Job_Title                   = Job_Title;
             //this.Hire_Date                   = Hire_Date;
             this.Leave_Date                  = Leave_Date;
             //this.Return_from_Date            = Return_from_Date;
             this.Termination_Date            = Termination_Date;
             //this.Assignment_Start_Date       = Assignment_Start_Date;
             this.Employee_lookup             = Employee_lookup;
             this.Position_lookup             = Position_lookup;
             this.Syneos_Employee_Id          = Syneos_Employee_Id;
             /*this.Client                      = Client;   
             this.Assignment_End_Date         = Assignment_End_Date;
             this.Transfer_Date               = Transfer_Date;
             this.EBS_Project                 = EBS_Project;*/
            // this.Project_Ext_ID              = Project_Ext_ID;
            // this.EBS_Project_Name            = EBS_Project_Name;
             this.Created_Date                = Created_Date;
             //this.Completed_Date              = Completed_Date;
        }
    }
}

