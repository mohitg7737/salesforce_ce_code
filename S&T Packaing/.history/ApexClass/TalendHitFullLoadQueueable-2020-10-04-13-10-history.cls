public class TalendHitFullLoadQueueable implements Queueable, Database.AllowsCallouts {

    String endPoint;
    String sfdcPath;
    String axtriaUsername;
    String axtriaPassword;
    String veevaUsername;
    String veevaPassword;
    boolean flag;
    
    public void execute(QueueableContext context)
    {
        system.debug('#### Hit Talend job for Full load called ####');

        endPoint='';
        sfdcPath='';
        axtriaUsername='';
        axtriaPassword='';
        veevaUsername='';
        veevaPassword='';   
        List<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select Id, Name, AxtriaSalesIQTM__Server_Type__c,AxtriaARSnT__MySetupExecutionError__c,AxtriaSalesIQTM__End_Point__c,AxtriaSalesIQTM__SFTP_Username__c,AxtriaSalesIQTM__SF_UserName__c,AxtriaSalesIQTM__SF_Password__c,AxtriaSalesIQTM__SFTP_Password__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'Veeva Full Load'];
        endPoint=etlConfig[0].AxtriaSalesIQTM__End_Point__c;
        sfdcPath=etlConfig[0].AxtriaSalesIQTM__Server_Type__c;
        axtriaUsername=etlConfig[0].AxtriaSalesIQTM__SF_UserName__c;
        axtriaPassword=etlConfig[0].AxtriaSalesIQTM__SF_Password__c;
        veevaUsername=etlConfig[0].AxtriaSalesIQTM__SFTP_Username__c;
        veevaPassword=etlConfig[0].AxtriaSalesIQTM__SFTP_Password__c;
        flag=etlConfig[0].AxtriaARSnT__MySetupExecutionError__c;

        if(endPoint != '' && endPoint != null)
        {
            string TalendEndpoint=endPoint;
            
            //string TalendEndpoint='http://54.72.147.25:8080/VI3_GAS_Inbound_From_VeevaTest/services/VI3_GAS_Inbound_From_Veeva?method=runJob&arg0=--context_param AZ_ARSNT_EUFULL_SIQ_Username=az.asia@salesiq.com.asiafull&arg1=--context_param AZ_ARSNT_EUFULL_SIQ_Password=azasia%21%40%23%24123&arg2=--context_param AZ_Veeva_Username=salesiq.admin@az.ie.iefull&arg3=--context_param AZ_Veeva_Password=AZadmin@2019&arg4=--context_param AZ_ARSNT_EUFULL_SIQ_URL=https://test.salesforce.com/services/Soap/u/39.0&arg5=--context_param AZ_Veeva_URL=https://test.salesforce.com/services/Soap/u/39.0'; 
 
            try{

                TalendEndpoint += '&arg0=--context_param%20AZ_ARSNT_EUFULL_SIQ_Username='+axtriaUsername;
                TalendEndpoint += '&arg1=--context_param%20AZ_ARSNT_EUFULL_SIQ_Password='+axtriaPassword;
                TalendEndpoint += '&arg2=--context_param%20AZ_Veeva_Username='+veevaUsername;
                TalendEndpoint += '&arg3=--context_param%20AZ_Veeva_Password='+veevaPassword;
                TalendEndpoint += '&arg4=--context_param%20AZ_ARSNT_EUFULL_SIQ_URL='+sfdcPath;
                TalendEndpoint += '&arg5=--context_param%20AZ_Veeva_URL='+sfdcPath;
                TalendEndpoint += '&arg6=--context_param%20MySetup_ExecutionFlag='+flag;
                
                
                system.debug('#### TalendEndpoint : '+TalendEndpoint);

                Http h = new Http();
                HttpRequest request = new HttpRequest();
                TalendEndpoint = TalendEndpoint.replaceAll( '\\s+', '%20');
                request.setEndPoint(TalendEndpoint);
                request.setHeader('Content-type', 'application/json');
                request.setMethod('GET');
                request.setTimeout(100000);
                system.debug('request '+request);
                HttpResponse response = h.send(request);
         //        if (response.getStatusCode() == 200)
         //        {
                        // System.debug('Job ran successfully');
         //        }
         //        else
         //        {
         //         //system.debug('Error in Talend job for Full load' +ex.getMessage());
         //         String subject = 'Error in Talend job for Full load';
         //         String body = 'Error while execution Talend Job for Full load';
         //         String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
         //         //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            //         Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            //         emailwithattch.setSubject(subject);
            //         emailwithattch.setToaddresses(address);
            //         emailwithattch.setPlainTextBody(body);
         //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
         //        }
                system.debug('#### Talend Delete Response : '+response);
            }
            catch(exception ex)
            {
                system.debug('Error in Talend job for Full load' +ex.getMessage());
                String subject = 'Error in Talend job for Full load';
                String body = 'Error while execution Talend Job for Full load:::::  ' +ex.getMessage();
                String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
                //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
                Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
                emailwithattch.setSubject(subject);
                emailwithattch.setToaddresses(address);
                emailwithattch.setPlainTextBody(body);
                //emailwithattch.setCcAddresses(ccAdd);

                //emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});

                // Sends the email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
            }
        }
        else
        {
            system.debug('Error in Talend job for Full load due to End Point');
            String subject = 'Error in Talend job for Full load due to End Point';
            String body = 'End Point Null for Full load';
            String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(address);
            emailwithattch.setPlainTextBody(body);
            //emailwithattch.setCcAddresses(ccAdd);

            //emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
        }
    }
}