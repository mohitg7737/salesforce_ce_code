global with sharing class MySetUpProd_Insert_Utility implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    ////removed occurance of Business_Unit_Loopup__c object purge A1422
    public String query;
    //public String selectedTeamInstance;
    public String selectedTeam;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    //Map<String,string> teamInstanceNametoAZCycleMap;//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
    //Map<String,string> teamInstanceNametoAZCycleNameMap;//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,Team_Instance_Product_AZ__c> questionToObjectMap;
    public List<String> allTeamsInstances;
    public list<string>teaminstancelistis;

    public List<Scheduler_Log__c> activityLogList = new List<Scheduler_Log__c>();
    public Set<String> activityLogIDSet;

    public Set<String> allCountries;

    public Boolean chaining = false;
    Set<String> positionSet= new Set<String>();
    public boolean errorBatches=false;

    global MySetUpProd_Insert_Utility(String teamInstance) 
    {
       
    }

    global MySetUpProd_Insert_Utility(List<String> teamInstance) 
    {
        allTeamsInstances = new List<String>(teamInstance);
        
        allCountries = new Set<String>();

        allTeamsInstances = teamInstance;
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        //teamInstanceNametoAZCycleMap = new Map<String,String>();//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //teamInstanceNametoAZCycleNameMap = new Map<String,String>();//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        //selectedTeamInstance = teamInstance;
        
        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__c,/*AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,*/AxtriaSalesIQTM__Team__r.name/*,Cycle__c,Cycle__r.name*/ From AxtriaSalesIQTM__Team_Instance__c where id in :allTeamsInstances]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            //teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            //teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
            allCountries.add(teamIns.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
        }
        //purge begins
        /*for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c where Team_Instance__c in :allTeamsInstances]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Team_Instance__r.name,bti.id);
        }*/
        //purge ends
        
        //selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        /*List<Product_Catalog__c> ProdList = new List<Product_Catalog__c>();
        for (Team_Instance_Product_AZ__c TIP : [Select id,Product_Catalogue__c,Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c = :teamInstance]){
            ProdList.add(String.Valueof(TIP.Product_Catalogue__c));
        }*/
        
        query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c  Where AxtriaSalesIQTM__Team_Instance__c in :allTeamsInstances and AxtriaSalesIQTM__isActive__c = true ';
    }

    global MySetUpProd_Insert_Utility(List<String> teamInstance, Boolean chain) 
    {
        allTeamsInstances = new List<String>(teamInstance);
        chaining = chain;
        allCountries = new Set<String>();
        allTeamsInstances = teamInstance;
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        //teamInstanceNametoAZCycleMap = new Map<String,String>();//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //teamInstanceNametoAZCycleNameMap = new Map<String,String>();//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        //selectedTeamInstance = teamInstance;
    
    //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c    
    for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__c,/*AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,*/AxtriaSalesIQTM__Team__r.name/*,Cycle__c,Cycle__r.name*/ From AxtriaSalesIQTM__Team_Instance__c where id in :allTeamsInstances]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            //teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            //teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
            allCountries.add(teamIns.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
        }
        //purge begins
        /*for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c where Team_Instance__c in :allTeamsInstances]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Team_Instance__r.name,bti.id);
        }*/
        //purge ends
        //selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        /*List<Product_Catalog__c> ProdList = new List<Product_Catalog__c>();
        for (Team_Instance_Product_AZ__c TIP : [Select id,Product_Catalogue__c,Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c = :teamInstance]){
            ProdList.add(String.Valueof(TIP.Product_Catalogue__c));
        }*/
        
        query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c FROM AxtriaSalesIQTM__Position_Product__c  Where AxtriaSalesIQTM__Team_Instance__c in :allTeamsInstances and AxtriaSalesIQTM__isActive__c = true';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
       SnTDMLSecurityUtil.printDebugMessage('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Product__c> scopeRecs) 
    {
     try
        {

           SnTDMLSecurityUtil.printDebugMessage('Hello++++++++++++++++++++++++++++');
            
            
            SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
            SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
            Map<String,SIQ_My_Setup_Products_vod_O__c> MySetupProdMap = new Map<String,SIQ_My_Setup_Products_vod_O__c>();
            Map<String,Set<String>> positionEmployeeMap = new  Map<String,Set<String>>();

            Set<String> detailGroup = new Set<String>();
            Map<String,List<Product_Catalog__c>> pCatalogMap= new Map<String,List<Product_Catalog__c>>();
            
            
            for(AxtriaSalesIQTM__Position_Product__c prodC : scopeRecs)
            {
                detailGroup.add(prodC.Product_Catalog__r.Detail_Group__c);
                positionSet.add(prodC.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            List<Product_Catalog__c> prodCatalog = [select Id,Detail_Group__c, Veeva_External_ID__c, Product_Type__c,Product_code__c FROM Product_Catalog__c where Detail_Group__c in :detailGroup and  Product_Type__c= :'Detail Topic' and IsActive__c=true];

           for(Product_Catalog__c pCatalog : prodCatalog)
           {
                if(pCatalogMap.containsKey(pCatalog.Detail_Group__c))
                {
                  pCatalogMap.get(pCatalog.Detail_Group__c).add(pCatalog);
                }
                else
                {
                    List<Product_Catalog__c> pcTemp = new List<Product_Catalog__c>();
                    pcTemp.add(pCatalog);
                    pCatalogMap.put(pCatalog.Detail_Group__c , pcTemp);
                }
           }
              
            List<AxtriaSalesIQTM__Position_Employee__c> posEmp= [Select id ,AxtriaSalesIQTM__Position__c, Employee_PRID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c=:'Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in: positionSet];

           SnTDMLSecurityUtil.printDebugMessage('+++posEmp'+posEmp);
            for(AxtriaSalesIQTM__Position_Employee__c positionEmp: posEmp)
            {
                if(positionEmployeeMap.containsKey(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
                {
                    Set<String> emp=positionEmployeeMap.get(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    emp.add(positionEmp.Employee_PRID__c);
                   SnTDMLSecurityUtil.printDebugMessage('+++emp'+emp);
                    positionEmployeeMap.put(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,emp);
                }
                
                else
                {
                    positionEmployeeMap.put(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,new set<string>{positionEmp.Employee_PRID__c});     
                }
               SnTDMLSecurityUtil.printDebugMessage('+++positionEmployeeMap'+positionEmployeeMap);
            }
            for(AxtriaSalesIQTM__Position_Product__c scsp : scopeRecs)
            {    
                Set<String> prid= positionEmployeeMap.get(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c); 
               SnTDMLSecurityUtil.printDebugMessage('+++prid'+prid);
                if(prid != null)
                for(string employeePrid:prid)
                {

                    tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
                    tempMySetupProdRec.CurrencyIsoCode = scsp.CurrencyIsoCode;//'EUR'
                    /*//tempMySetupProdRec.OwnerId = ;
                    tempMySetupProdRec.Name = ;*/
                    tempMySetupProdRec.SIQ_Favorite_vod__c = 'true';
                    tempMySetupProdRec.SIQ_Product_vod__c = scsp.Product_Catalog__r.Veeva_External_ID__c;
                    tempMySetupProdRec.SIQ_Employee_PRID__c = employeePrid;//scsp.AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c;

                    if(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
                    }
                    else if (scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                    }
                    //tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
                   SnTDMLSecurityUtil.printDebugMessage('++++++++++++++ scsp.Product_Catalog__r.Veeva_External_ID__c' + scsp.Product_Catalog__r.Veeva_External_ID__c); 
                   SnTDMLSecurityUtil.printDebugMessage('++tempMySetupProdRec.SIQ_Country__c'+tempMySetupProdRec.SIQ_Country__c);
                   SnTDMLSecurityUtil.printDebugMessage('+++pridemp'+employeePrid);  
                    tempMySetupProdRec.External_ID__c = tempMySetupProdRec.SIQ_Country__c  + '_' +employeePrid+ '_' + scsp.Product_Catalog__r.Veeva_External_ID__c;
                   SnTDMLSecurityUtil.printDebugMessage('++++++ Get '+ tempMySetupProdRec.SIQ_Country__c);
                    tempMySetupProdRec.Type__c = 'Product';
                    tempMySetupProdRec.Status__c = 'Updated';
                
                    
                    if(!MySetupProdMap.containsKey(tempMySetupProdRec.External_ID__c) && tempMySetupProdRec.SIQ_Employee_PRID__c != null && scsp.Product_Catalog__r.Veeva_External_ID__c != null)
                    {
                        MySetupProdMap.put(tempMySetupProdRec.External_ID__c,tempMySetupProdRec);
                        
                        if(scsp.Product_Catalog__r.Detail_Group__c != null && scsp.Product_Catalog__r.Detail_Group__c != '')
                        {
                            tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
                            tempMySetupProdRec2 = tempMySetupProdRec.clone();
                            tempMySetupProdRec2.SIQ_Product_vod__c = scsp.Product_Catalog__r.Detail_Group__c;
                            tempMySetupProdRec2.External_ID__c = tempMySetupProdRec2.SIQ_Country__c + '_' + employeePrid + '_' + scsp.Product_Catalog__r.Detail_Group__c;
                            tempMySetupProdRec2.Type__c = 'Detail Group'; 
                            tempMySetupProdRec.SIQ_Favorite_vod__c = 'false';
                            tempMySetupProdRec.Status__c = 'Updated';
                            if(!MySetupProdMap.containsKey(tempMySetupProdRec2.External_ID__c) && employeePrid != null && scsp.Product_Catalog__r.Detail_Group__c != null)
                            {
                                MySetupProdMap.put(tempMySetupProdRec2.External_ID__c,tempMySetupProdRec2);

                                if(pCatalogMap.containsKey(scsp.Product_Catalog__r.Detail_Group__c))
                                {
                                    for(Product_Catalog__c pc : pCatalogMap.get(scsp.Product_Catalog__r.Detail_Group__c))
                                    {
                                        tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
                                        tempMySetupProdRec2 = tempMySetupProdRec.clone();
                                        tempMySetupProdRec2.SIQ_Product_vod__c = pc.Veeva_External_ID__c;
                                        tempMySetupProdRec.SIQ_Favorite_vod__c = 'false';
                                        tempMySetupProdRec2.External_ID__c = tempMySetupProdRec2.SIQ_Country__c+ '_' +employeePrid + '_' + pc.Veeva_External_ID__c; 
                                        tempMySetupProdRec2.Type__c = 'Detail Topic';
                                        tempMySetupProdRec2.Status__c = 'Updated';
                                        MySetupProdMap.put(tempMySetupProdRec2.External_ID__c,tempMySetupProdRec2);
                                    }                            
                                }
                            }
                        }
                    }
                }  
            }
            
            upsert MySetupProdMap.values() External_ID__c;//insert MySetupProdMap.values();//
        } catch(Exception e)
           {
                errorBatches=true;
               //SnTDMLSecurityUtil.printDebugMessage('++inside catch');
                //String Header='Mysetup Utility batch is failed, so delta has been haulted of this org for Today';
                //Added by Ayushi   
                List<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select id,MySetupExecutionError__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'Veeva Full Load'];   
                System.debug('etlConfig::::: ' +etlConfig); 
                for(AxtriaSalesIQTM__ETL_Config__c rec : etlConfig) 
                {   
                    rec.MySetupExecutionError__c = errorBatches;    
                }   
                update etlConfig;   
                //till here
               //tryCatchEmailAlert.veevaLoad(Header);
           }

    }

    global void finish(Database.BatchableContext BC)
    {
         if(chaining)
            {
                list<string> teaminstancelistis = new list<string>();
                teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstancesCallPlan();
                //database.executeBatch(new changeTSFStatus(teaminstancelistis));

                //Added by Ayushi
                AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
                exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('VeevaFullLoad')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('VeevaFullLoad'):null;
    
               SnTDMLSecurityUtil.printDebugMessage('execute trigger' +exeTrigger );
                //public static Boolean executeTrigger; 
                if(exeTrigger != null)
                {
                    if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
                    {
                        activityLogIDSet = new Set<String>();

                        for(String teamIns : teaminstancelistis)
                        {
                            Scheduler_Log__c activityLogrec = new Scheduler_Log__c();
                            activityLogrec.Job_Status__c = 'Failed';
                            activityLogrec.Job_Type__c = 'Veeva Full Load';
                            activityLogrec.Team_Instance__c = teamIns;
                            activityLogrec.Job_Name__c = 'Veeva full load';
                            activityLogList.add(activityLogrec);

                        }

                       SnTDMLSecurityUtil.printDebugMessage('activityLogList.size() :::::::::' +activityLogList.size());
                       SnTDMLSecurityUtil.printDebugMessage('teaminstancelistis.size() :::::::::' +teaminstancelistis.size());

                        if(activityLogList.size() > 0){
                            //insert activityLogList;
                            SnTDMLSecurityUtil.insertRecords(activityLogList, 'MySetUpProd_Insert_Utility');
                        }

                       SnTDMLSecurityUtil.printDebugMessage('activityLogList after insert :::::::::' +activityLogList);

                        for(Scheduler_Log__c activityID : activityLogList)
                        {
                            activityLogIDSet.add(activityID.Id);
                        }
                       SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet.size() :::::::::' +activityLogIDSet.size());
                       SnTDMLSecurityUtil.printDebugMessage('activityLogIDSet :::::::::' +activityLogIDSet);
                    }
                }

                //till here

                if(activityLogIDSet != null)
                    AZ_Integration_Pack aipNew = new AZ_Integration_Pack(teaminstancelistis,activityLogIDSet);

                else    
                    AZ_Integration_Pack aip = new AZ_Integration_Pack(teaminstancelistis);            
            }
            //commented since DetailTopic_KAMPermission removed
            /*try
            {
              DetailTopic_KAMPermission detail= new  DetailTopic_KAMPermission(chaining);
              database.executeBatch(detail,2000);

            }
            catch(Exception e)
            {
                
            }  */ 
    }


    global void execute(System.SchedulableContext SC)
    {
        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
        database.executeBatch(new MySetUpProd_Insert_Utility(teaminstancelistis, false));
    }
}