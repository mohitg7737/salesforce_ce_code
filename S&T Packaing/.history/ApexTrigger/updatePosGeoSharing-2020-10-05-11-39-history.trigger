trigger updatePosGeoSharing on AxtriaSalesIQTM__Scenario__c (After Update) 
{
    
    AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('updatePosGeoSharing');
    List<AxtriaSalesIQTM__Scenario__c> updateCheckbox = new List<AxtriaSalesIQTM__Scenario__c>();
    

     if(!Test.isRunningTest())
     {
        Boolean  myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ;      
        if(myCCVal != true)
        { 
        system.debug('True Value');
            for(AxtriaSalesIQTM__Scenario__c sc : trigger.new)
            {             
                if(sc.AxtriaSalesIQTM__Request_Process_Stage__c == 'CIM Ready')
                {
                    //updatePosGeoAZ.updatePosGeo(sc.AxtriaSalesIQTM__Team_Instance__c);
                    Database.executeBatch(new updatePosGeoAZ(sc.AxtriaSalesIQTM__Team_Instance__c));
                }
                
                //Comment from here A1930        
                if(sc.AxtriaSalesIQTM__Scenario_Source__c == 'Existing Scenario'&& sc.AxtriaSalesIQTM__Request_Process_Stage__c == 'Ready'&& sc.AxtriaSalesIQTM__Scenario_Status__c == 'Active' )
    
                //sc.isCopy_Call_Plan__c = true;
                //updateCheckbox.add(sc);
                      
                System.debug('Scenario record ::::::::::' +sc);
                List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
                pacp = [select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c =:sc.AxtriaSalesIQTM__Team_Instance__c LIMIT 1];
                system.debug('Team Instance Name PACP'+ pacp);
                if(pacp.size()>0)
                
                system.debug('Team Instance Value');
                system.debug(sc.AxtriaSalesIQTM__Team_Instance__c + 'Team Instance Name');
                system.debug(sc.AxtriaSalesIQTM__Team_Instance__r.Name + 'Team Instance Name');
               // database.executebatch(new BatchUpdateSharedFlag(sc.AxtriaSalesIQTM__Team_Instance__r.Name,'1234'),2000);
         
                if(sc.AxtriaSalesIQTM__Request_Process_Stage__c == 'Ready' && Trigger.oldMap.get(sc.Id).AxtriaSalesIQTM__Scenario_Status__c=='Inactive' && Trigger.newMap.get(sc.Id).AxtriaSalesIQTM__Scenario_Status__c == 'Active')
                {
                  system.debug('Value ABC Print');
 
                    List<AxtriaSalesIQTM__Position__c> Pos = new List<AxtriaSalesIQTM__Position__c>();
                    Pos = [SELECT Id FROM AxtriaSalesIQTM__Position__c WHERE AxtriaSalesIQTM__Team_Instance__c = :sc.AxtriaSalesIQTM__Team_Instance__c AND AxtriaSalesIQTM__Hierarchy_Level__c > '5'];
                   
                }
            }
        }
    }
}