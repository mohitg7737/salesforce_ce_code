trigger tagEmployee on AxtriaSalesIQST__CR_Employee_Feed__c (after insert) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');
    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        set<Id> empIdSet = new Set<Id>();
        for(AxtriaSalesIQST__CR_Employee_Feed__c  cr : trigger.new){
            empIdSet.add(cr.AxtriaSalesIQST__Employee__c);
        }

        List<AxtriaSalesIQTM__Employee__c> empList = [select id, CR_Employee_Feed__c from AxtriaSalesIQTM__Employee__c where id =: empIdSet];
        
        if(empList.size()>0){
            for(AxtriaSalesIQTM__Employee__c emp : empList){
                for(AxtriaSalesIQST__CR_Employee_Feed__c  cr : trigger.new){
                    if(cr.AxtriaSalesIQST__Employee__c == emp.Id){
                        emp.CR_Employee_Feed__c = cr.Id;
                    }
                }
            }
            
            update empList;
        }
    }
}