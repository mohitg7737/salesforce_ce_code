trigger OpenPositionUpdate on OpenPosition__c (before insert, after update) {
if(trigger.isInsert){
        set<String> developmentNameSetInsert = new set<String>();
        set<String> developmentIdSetInsert = new set<String>();
        list<String> developmentNameListInsert = new list<String>();
        for(OpenPosition__c dev: trigger.New){
            developmentIdSetInsert.add(dev.Id);
            developmentNameSetInsert.add(dev.Name);
        }
        for(String str: developmentNameSetInsert){
            developmentNameListInsert.add(str);
        }
        system.debug('developmentIdSetInsert-----'+developmentIdSetInsert);
        system.debug('developmentNameSetInsert-----'+developmentNameSetInsert);
        system.debug('developmentNameListInsert------'+developmentNameListInsert);
        String leadObj = 'AxtriaSalesIQTM__Position__c';
        //String saleObj = 'Sale__c';
        String picklistToUpdate = 'Position__c';
        
        if(developmentNameListInsert != null && !developmentNameListInsert.isEmpty()){
            triggerOnOpenPositionPicklistUpdate.addDevelopmentNameToPickListValues(leadObj,picklistToUpdate,       
              developmentNameListInsert);
           }
        }
    if(trigger.isUpdate){
    
        String leadObj = 'AxtriaSalesIQTM__Position__c';
        //String saleObj = 'Position__c';
        String picklistToUpdate = 'Position__c';
        List<OpenPosition__c> developmentList = [select id, Name from OpenPosition__c where id In : 
        Trigger.New];

        system.debug('-developmentList-'+developmentList);
        
        set<String> developmentNameSetUpdate = new set<String>();
        set<String> developmentIdSetUpdate = new set<String>();
        list<String> developmentNameListUpdate = new list<String>();
        
        for(OpenPosition__c dev: trigger.New){
            developmentIdSetUpdate.add(dev.Id);
            developmentNameSetUpdate.add(dev.Name);
        }
        
       for(String str: developmentNameSetUpdate){
            developmentNameListUpdate.add(str);
        }
        system.debug('developmentIdSetUpdate-----'+developmentIdSetUpdate);
        system.debug('developmentNameSetUpdate-----'+developmentNameSetUpdate);
        system.debug('developmentNameListUpdate------'+developmentNameListUpdate);
        
        String jsonOldMap = JSON.serialize(Trigger.oldMap);
        system.debug('jsonOldMap-------'+jsonOldMap);
        
        String jsonNewMap = JSON.serialize(Trigger.newMap);
        system.debug('jsonNewMap-------'+jsonNewMap);
        
        if(developmentNameListUpdate != null && !developmentNameListUpdate.isEmpty()){
          
         triggerOnOpenPositionPicklistUpdate.updateDevelopmentNameToPickListValues(leadObj,picklistToUpdate,jsonOldMap, jsonNewMap);
        } 

  
    }}