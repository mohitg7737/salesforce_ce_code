trigger onDeletion on AxtriaSalesIQTM__Employee__c (before update,after update, after insert, after delete) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('onDeletionTrigger');
    if(trigger.isAfter && trigger.isDelete && myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        List<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c>();
        poslist = [Select id from AxtriaSalesIQTM__Position__c ];
        if(poslist.size()>0){
            update poslist;
        }
        update[select id from AxtriaSalesIQTM__Position_Employee__c];
    }
    if(BatchProcessCRRosterEvents.stopExecution != true && myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        if(trigger.isAfter && trigger.isUpdate){
            System.debug('@@@ Inside 2nd After Update Block of Emp Trigger');
            List<Time_Type_Jounery__c> existingTimeType2Jounery = [select id, Employee__c,Active_TimeType__c, FTE__c,FTE_End_Date__c, FTE_Start_Date__c, Time_Type__c  from Time_Type_Jounery__c];
            List<Time_Type_Jounery__c> updateTimeType2Jounery = new list<Time_Type_Jounery__c>(); 
            List<Time_Type_Jounery__c> insertTimeType2Jounery = new list<Time_Type_Jounery__c>();        
            for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                for(Time_Type_Jounery__c  typejourney : existingTimeType2Jounery){
                    if(emp.id == typejourney.Employee__c && typejourney.Active_TimeType__c == true){
                        System.debug('@@@ Inside time type journey code');
                        if(emp.Reason_Code__c != 'Time Type Journey Updated' && typeJourney.Active_TimeType__c == true){
                            if(emp.Reason_Code__c != 'Terminate Employee'){
                                typejourney.FTE__c = emp.FTE__c;
                                typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                                typejourney.Time_Type__c = emp.Time_Type__c;
                            }
                        }
                        if(emp.Reason_Code__c == 'Terminate Employee' && emp.Termination_Date__c == null){
                            typejourney.FTE_End_Date__c = emp.Termination_Date__c;
                            
                        }
                        if(emp.Reason_Code__c == 'Terminate Employee' && emp.Termination_Date__c != null){
                            typejourney.FTE_End_Date__c = emp.Termination_Date__c;
                            
                        }
                        if(emp.Reason_Code__c == 'Time Type Journey Updated'){
                            if(trigger.oldMap.get(emp.id).FTE_Start_Date__c == trigger.newMap.get(emp.id).FTE_Start_Date__c){
                                typejourney.FTE__c = emp.FTE__c;
                                typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                                typejourney.Time_Type__c = emp.Time_Type__c;
                            }
                            if(trigger.oldMap.get(emp.id).FTE_Start_Date__c != trigger.newMap.get(emp.id).FTE_Start_Date__c){
                                typejourney.Active_TimeType__c = false;
                                typejourney.FTE_End_Date__c = emp.FTE_Start_Date__c-1;
                            }
                        }
                        updateTimeType2Jounery.add(typejourney);
                        System.debug('@@@ Inside time type journey code typejourney.FTE_Start_Date__c ='+typejourney.FTE_Start_Date__c);
                    }

                }
                if(emp.Reason_Code__c == 'Time Type Journey Updated' ){
                    if(trigger.oldMap.get(emp.id).FTE_Start_Date__c != trigger.newMap.get(emp.id).FTE_Start_Date__c){
                        Time_Type_Jounery__c typejourney = new Time_Type_Jounery__c();
                        typejourney.FTE__c = emp.FTE__c;
                        if(emp.Reason_Code__c == 'Time Type Journey Updated'){
                            typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                        }
                        if(emp.Reason_Code__c == 'Transfer'){
                            typejourney.FTE_Start_Date__c = emp.Position_Start_Date__c;
                        }
                        typejourney.Time_Type__c = emp.Time_Type__c;
                        typejourney.Active_TimeType__c = true;
                        typejourney.Employee__c = emp.id;
                        insertTimeType2Jounery.add(typejourney);
                    }

                }
            }

            if(updateTimeType2Jounery.size()>0){
                update updateTimeType2Jounery;
            }
            if(insertTimeType2Jounery.size()>0){
                insert insertTimeType2Jounery;
            }
            //BatchProcessCRRosterEvents.stopExecution = true;
        }
        // email notification logic
        if(trigger.isAfter){
            if(trigger.isInsert || trigger.isUpdate){
                for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                    if(emp.Functional_Area_ID__c == '500604'){
                        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
                        String [] toAddresses = new list<string> {'piyush.shrivastava@axtria.com','shradha.chhabra@axtria.com','ankit.sogani@axtria.com','mayank.budhiraja@axtria.com','lakshmi.nair@axtria.com','akash.ranjan@axtria.com','bichiter.singh@axtria.com'};
                        
                        singEmail.setToAddresses (toAddresses);
                        String subject = 'DSI Roster Manual Action Reminder !!!';
                        singEmail.setSubject (subject);
                        string content;
                        content = 'Hi Team, <br/><br/>This Employee '+emp.AxtriaSalesIQTM__Employee_ID__c+' has been hired/transfered to market Access Team, Please Take appropriate action related to their SIC Status, History Records and Journey Records. <br/>Please refer to SOP and take appropriate actions.<br/> Thank you, <br/> Axtria SalesIQ Team';
                        
                        
                        singEmail.setHtmlBody(content);
                        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
                    }
                    if(emp.Action_Type__c == 'TERM-R' || emp.Action_Type__c == 'HIR-R' || emp.Action_Type__c == 'LOA-R'|| emp.Action_Type__c == 'RFL-R'){
                        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
                        String [] toAddresses = new list<string> {'piyush.shrivastava@axtria.com','shradha.chhabra@axtria.com','ankit.sogani@axtria.com','mayank.budhiraja@axtria.com','lakshmi.nair@axtria.com','akash.ranjan@axtria.com','bichiter.singh@axtria.com'};
                        singEmail.setToAddresses (toAddresses);
                        String subject = 'DSI Roster Manual Action Reminder !!!';
                        singEmail.setSubject (subject);
                        string content;
                        content = 'Hi Team, <br/><br/>The Action Code '+emp.Action_Type__c+' has occured on '+emp.AxtriaSalesIQTM__Employee_ID__c+'  Employee ID. <br/>Please refer to SOP and take appropriate actions.<br/> Thank you, <br/> Axtria SalesIQ Team';
                        
                        singEmail.setHtmlBody(content);
                        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
                    }
                } 
            }
        }
    }

    


}