trigger UpdateRelatedSiqEmployee on AxtriaSalesIQTM__Employee__c (before insert, before update, after insert, after update){
    System.debug('@@@ UpdateRelatedSiqEmployee Trigger Invoked!!!');
    System.debug('@@@ Inside Emp Trigger');
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('UpdateRelatedSiqEmployeeTrigger');

    public Set<String> empIdSet = new Set<String>();
    public Set<String> empRecSet = new Set<String>();
    List<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c>();
    
    for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
        empIdSet.add(emp.AxtriaSalesIQTM__Employee_ID__c);
        //empRecSet.add(emp.id);
    }
    poslist = [Select id from AxtriaSalesIQTM__Position__c ];
      
    if(trigger.isAfter){
        if(poslist.size()>0){
            update poslist;
        }
        
    }
    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        

        if(trigger.isAfter && trigger.isInsert){
            System.debug('@@@ Inside 1st After Insert Block of Emp Trigger');
            List<AxtriaSalesIQST__SIQ_Employee_Master__c> siqEmpList = [Select id, Employee__c, AxtriaSalesIQST__SIQ_Employee_ID__c from AxtriaSalesIQST__SIQ_Employee_Master__c where AxtriaSalesIQST__SIQ_Employee_ID__c =: empIdSet];

            if(siqEmpList.size()>0){
                for(AxtriaSalesIQST__SIQ_Employee_Master__c siqEmp : siqEmpList){
                    for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                        if(emp.AxtriaSalesIQTM__Employee_ID__c == siqEmp.AxtriaSalesIQST__SIQ_Employee_ID__c){
                            siqEmp.Employee__c = emp.Id;
                            System.debug('@@@ Condition Satisfied !!! siqEmp.Employee__c ='+siqEmp.Employee__c);
                        }
                    }
                }
                BatchComputeRosterEvents.isFirstTime = false;
                update siqEmpList;
            }

            List<Training_Journey__c> TrainingJourneylist = new List<Training_Journey__c>();
            List<Leave_Journey__c> LeaveJourneylist = new List<Leave_Journey__c>();
            List<Employee_History__c> EmployeeHistorylist = new List<Employee_History__c>();
            List<AxtriaSalesIQST__CR_Employee_Feed__c> CREmployeeFeedlist = new List<AxtriaSalesIQST__CR_Employee_Feed__c>();
            List<Time_Type_Jounery__c> TimeTypeJounerylist = new List<Time_Type_Jounery__c>();
            List<AxtriaSalesIQTM__Position_Employee__c> PositionEmployeelist = new List<AxtriaSalesIQTM__Position_Employee__c>();
            for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
            // Step 2 :- Create Workbench Event in after insert event of Emp Trigger
                if(emp.Reason_Code__c != 'Transfer'){
                    AxtriaSalesIQST__CR_Employee_Feed__c crEmpFeed = new AxtriaSalesIQST__CR_Employee_Feed__c();
                    crEmpFeed.AxtriaSalesIQST__Employee__c = emp.Id;
                    crEmpFeed.AxtriaSalesIQST__Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                    if(emp.Reason_for_Action_Name__c != 'Rehire'){
                        crEmpFeed.AxtriaSalesIQST__Event_Name__c = 'Employee New Hire';
                        crEmpFeed.Reason_Code__c = 'Employee New Hire';
                    }
                    if(emp.Reason_for_Action_Name__c == 'Rehire'){
                        crEmpFeed.AxtriaSalesIQST__Event_Name__c = 'Rehire';
                        crEmpFeed.Reason_Code__c = 'Rehire';
                    }

                    
                    crEmpFeed.AxtriaSalesIQST__Request_Date1__c = Date.today();
                    crEmpFeed.AxtriaSalesIQST__Status__c = 'No Action';
                    
                    crEmpFeed.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                    crEmpFeed.Event_Date__c = Date.Today();
                    crEmpFeed.Event_Start_Date_from_Staging__c = emp.Action_Start_Date__c;
                    CREmployeeFeedlist.add(crEmpFeed);
                }
            

                // Step 3 :- Create Position Employee Assignment in after insert event of Emp Trigger
                AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
                posEmp.Name = emp.Name+'_'+emp.Territory__c+'_'+emp.Territory_Name__c;
                posEmp.AxtriaSalesIQTM__Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                posEmp.AxtriaSalesIQTM__Employee__c = emp.Id;
                posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = emp.Assignment_Start_Date__c;
                posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
                posEmp.isActive__c = true;
                PositionEmployeelist.add(posEmp);

                // Step 4 :- Create Training Journey in after insert event of Emp Trigger
                Training_Journey__c tr = new Training_Journey__c();
                tr.Employee__c = emp.Id;
                tr.Training_Start_Date__c = emp.Last_Hire_Date__c;
                
                if(emp.Reason_for_Action_Name__c != 'Rehire' && emp.Reason_Code__c != 'Transfer'){
                    tr.Training_Type__c = 'New Hire Training';
                    tr.Training_Reason__c = 'On Training due to New Hire Event.';
                }
                if(emp.Reason_for_Action_Name__c == 'Rehire' && emp.Reason_Code__c != 'Transfer'){
                    tr.Training_Type__c = 'Rehire Training';
                    tr.Training_Reason__c = 'On Training due to Rehire Event.';
                }
                if(emp.Reason_Code__c == 'Transfer'){
                    tr.Training_Type__c = 'Transfer Training';
                    tr.Training_Reason__c = 'On Training due to TRANSFER FROM HO TO FIELD .';
                    tr.Training_Start_Date__c = emp.Position_Start_Date__c;
                }
                TrainingJourneylist.add(tr);

                // Step 5 :- Create Time Type Journey in after insert event of Emp Trigger
                Time_Type_Jounery__c typejourney = new Time_Type_Jounery__c();
                typejourney.Employee__c = emp.id;
                typejourney.FTE__c = emp.FTE__c;
                typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                typejourney.Time_Type__c = emp.Time_Type__c;
                typejourney.Active_TimeType__c = true;
                TimeTypeJounerylist.add(typejourney);


                // Step 6 :- Create History Record in after insert event of Emp Trigger
                Employee_History__c empHis1 = new Employee_History__c();
                empHis1.Employee_ID__c = emp.AxtriaSalesIQTM__Employee_ID__c;
                empHis1.IC_Status__c = emp.IC_Status__c;
                empHis1.Start_Date__c = emp.Position_Start_Date__c;
                empHis1.End_Date__c = Date.newInstance(4000, 12, 31);
                if(emp.Reason_for_Action_Name__c != 'Rehire' && emp.Reason_Code__c != 'Transfer'){
                    empHis1.Event_Name__c = 'Employee New Hire';
                    empHis1.Action_Reason__c = 'Created Due to New Hire of Employee';
                }
                if(emp.Reason_for_Action_Name__c == 'Rehire' && emp.Reason_Code__c != 'Transfer'){
                    empHis1.Event_Name__c = 'Rehire';
                    empHis1.Action_Reason__c = 'Created Due to Rehire of Employee';
                }
                if(emp.Reason_Code__c == 'Transfer'){
                    empHis1.Event_Name__c = 'Transfer';
                    empHis1.Action_Reason__c = 'Created Due to TRANSFER FROM HO TO FIELD ';
                }
                empHis1.Employee__c = emp.Id;
                empHis1.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                empHis1.LatestHistory__c = true;
                
                EmployeeHistorylist.add(empHis1);
            }
            if(TrainingJourneylist.size()>0){
                insert TrainingJourneylist;
            }
            if(LeaveJourneylist.size()>0){
                insert LeaveJourneylist;
            }
            if(EmployeeHistorylist.size()>0){
                insert EmployeeHistorylist;
            }
            if(CREmployeeFeedlist.size()>0){
                insert CREmployeeFeedlist;
            }
            if(TimeTypeJounerylist.size()>0){
                insert TimeTypeJounerylist;
            }
            if(PositionEmployeelist.size()>0){
                insert PositionEmployeelist;
            } 
        }

        if(trigger.isBefore){
            if(trigger.isUpdate){
                Id profileId= userinfo.getProfileId();
                String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                system.debug('ProfileName'+profileName);
                for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                   
                    if(profileName == 'HO'){
                        if(trigger.oldMap.get(emp.id).Name != trigger.newMap.get(emp.id).Name){
                            emp.Name.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        if(trigger.oldMap.get(emp.id).AxtriaSalesIQTM__FirstName__c!= trigger.newMap.get(emp.id).AxtriaSalesIQTM__FirstName__c){
                            emp.AxtriaSalesIQTM__FirstName__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        if(trigger.oldMap.get(emp.id).AxtriaSalesIQTM__Last_Name__c!= trigger.newMap.get(emp.id).AxtriaSalesIQTM__Last_Name__c){
                            emp.AxtriaSalesIQTM__Last_Name__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        //
                        if(trigger.oldMap.get(emp.id).Functional_Area_ID__c!= trigger.newMap.get(emp.id).Functional_Area_ID__c){
                            emp.Functional_Area_ID__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        if(trigger.oldMap.get(emp.id).Functional_Area_Description__c!= trigger.newMap.get(emp.id).Functional_Area_Description__c){
                            emp.Functional_Area_Description__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        if(trigger.oldMap.get(emp.id).Territory__c!= trigger.newMap.get(emp.id).Territory__c){
                            emp.Territory__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                        if(trigger.oldMap.get(emp.id).Territory_Name__c!= trigger.newMap.get(emp.id).Territory_Name__c){
                            emp.Territory_Name__c.addError('User cannot perform DML Operation on Employee manually !!! ');
                        }
                    }
                    
                    if(emp.On_Training__c == true && emp.on_Active_Leave__c == TRUE){
                        //emp.IC_Status__c = 'LOA';
                        emp.IC_Status__c = 'LTRN';
                    }
                    else if(emp.On_Training__c == true && emp.on_Active_Leave__c != TRUE){
                        emp.IC_Status__c = 'TRN';
                    }
                    else if(emp.On_Training__c != true && emp.on_Active_Leave__c == TRUE){
                        emp.IC_Status__c = 'LOA';
                    }
                    else{
                        emp.IC_Status__c = 'CE';
                    }
                    if(emp.Reason_Code__c == 'Terminate Employee'  ){
                        if(emp.Termination_Date__c != null){
                            emp.IC_Status__c = 'TERM';
                            emp.On_Training__c = false;
                            if(emp.Latest_Training_Journey__c!= null){
                                emp.Training_End_Date__c = emp.Termination_Date__c;
                            }
                            
                            emp.isTrainingCompleted__c = true;
                        }
                    }
                    if(emp.Reason_Code__c == 'Terminate Employee' && emp.Action_Type__c == 'TERM-R'){
                        
                        emp.Training_End_Date__c = null;
                        
                    }

                }
            }
            if(trigger.isinsert){
                for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                    if(emp.On_Training__c == true && emp.on_Active_Leave__c == TRUE){
                        emp.IC_Status__c = 'LTRN';
                    }
                    else if(emp.On_Training__c == true && emp.on_Active_Leave__c != TRUE){
                        emp.IC_Status__c = 'TRN';
                    }
                    else if(emp.On_Training__c != true && emp.on_Active_Leave__c == TRUE){
                        emp.IC_Status__c = 'LOA';
                    }
                    else{
                        emp.IC_Status__c = 'CE';
                    }

                    if(emp.Reason_Code__c == 'Terminate Employee'){
                        emp.IC_Status__c = 'TERM';
                        emp.On_Training__c = false;
                        emp.Training_End_Date__c = emp.Termination_Date__c;
                        emp.isTrainingCompleted__c = true;
                    }

                    if(emp.Reason_Code__c == 'Terminate Employee' && emp.Action_Type__c == 'TERM-R'){
                        
                        emp.Training_End_Date__c = null;
                        
                    }
                } 
            }
        }
        if(BatchProcessCRRosterEvents.stopExecution != true){
            if(trigger.isAfter && trigger.isUpdate){
                System.debug('@@@ Inside 1st After Update Block of Emp Trigger');
                Set<Id> empRecordIdSet = new Set<Id>();
                for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                    empRecordIdSet.add(emp.Id);

                }   
                List<AxtriaSalesIQST__SIQ_Employee_Master__c> siqEmpList = [Select id, Employee__c, AxtriaSalesIQST__SIQ_Employee_ID__c from AxtriaSalesIQST__SIQ_Employee_Master__c where AxtriaSalesIQST__SIQ_Employee_ID__c =: empIdSet];

                if(siqEmpList.size()>0){
                    for(AxtriaSalesIQST__SIQ_Employee_Master__c siqEmp : siqEmpList){
                        for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                            if(emp.AxtriaSalesIQTM__Employee_ID__c == siqEmp.AxtriaSalesIQST__SIQ_Employee_ID__c){
                                siqEmp.Employee__c = emp.Id;
                                System.debug('@@@ Condition Satisfied !!! siqEmp.Employee__c ='+siqEmp.Employee__c);
                            }
                        }
                    }
                    BatchComputeRosterEvents.isFirstTime = false;
                    update siqEmpList;
                }
                List<Training_Journey__c> newTrainingJourneyList = new List<Training_Journey__c>();
                List<AxtriaSalesIQTM__Position_Employee__c> existingPosEmpMapping = [Select id, AxtriaSalesIQTM__Assignment_Status__c, isActive__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Employee__c,Geography_Code__c, Position_Code__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Assignment_Type__c  from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__Employee__c =: empRecordIdSet AND AxtriaSalesIQTM__Assignment_Type__c != null];
                List<Training_Journey__c> existingTrainingJourneyMapping = [select id, Employee__c, Training_Start_Date__c, Training_End_Date__c, Training_Type__c, Training_Reason__c from Training_Journey__c];
                List<Employee_History__c> empHistoryList = [Select id, Employee__c, LatestHistory__c, Action_Reason__c, Employee_ID__c, End_Date__c, Event_Name__c, IC_Status__c, Position__c, Start_Date__c from Employee_History__c];
                List<Employee_History__c> empHistoryUpdate2List = [Select id, Employee__c, LatestHistory__c, Action_Reason__c, Employee_ID__c, End_Date__c, Event_Name__c, IC_Status__c, Position__c, Start_Date__c from Employee_History__c where Employee__c =: empRecordIdSet];
                List<Employee_History__c> empHistoryUpdateList = new list<Employee_History__c>();

                List<Employee_History__c> empHistoryInsertList = new list<Employee_History__c>();
                List<AxtriaSalesIQST__CR_Employee_Feed__c> existingCREmployeeFeedList = [select id, AxtriaSalesIQST__Employee__c, AxtriaSalesIQST__Position__c, AxtriaSalesIQST__Event_Name__c, AxtriaSalesIQST__Request_Date1__c, Reason_Code__c, Position__c, Event_Date__c, Event_Start_Date_from_Staging__c  from AxtriaSalesIQST__CR_Employee_Feed__c];

                List<AxtriaSalesIQST__CR_Employee_Feed__c> newCREmployeeFeedList = new List<AxtriaSalesIQST__CR_Employee_Feed__c>();
                List<Time_Type_Jounery__c> existingTimeTypeJounery = [select id, Employee__c, Active_TimeType__c, FTE__c,FTE_End_Date__c, FTE_Start_Date__c, Time_Type__c  from Time_Type_Jounery__c];

                List<Leave_Journey__c> existingLeaveJounery = [select id, Employee__c, Leave_Start_Date__c,Leave_End_Date__c, Leave_Reason__c, Leave_Type__c  from Leave_Journey__c];

                List<Leave_Journey__c> newLeaveJounery = new List<Leave_Journey__c>();
                
                Map<Id, Employee_History__c> HistoryMap = new Map<Id, Employee_History__c>();
                if(empHistoryList.size()>0){
                    for(Employee_History__c empHis : empHistoryList){
                        HistoryMap.put(empHis.Employee__c, empHis);
                    }
                }    

                // Step 2 :- Update Workbench Event in after insert event of Emp Trigger
                // Step 4 :- Update Training Journey in after insert event of Emp Trigger
                // Step 5 :- Update Time Type Journey in after insert event of Emp Trigger
                // Step 6 :- Update History Record in after insert event of Emp Trigger
                // Step 7 :- Position Update Scenario
                List<AxtriaSalesIQTM__Position_Employee__c> posEmpNewInsertList = new List<AxtriaSalesIQTM__Position_Employee__c>();
                for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                    // After Termination if rehire event gets occures
                    if((trigger.oldMap.get(emp.id).Reason_Code__c == 'Terminate Employee') && (trigger.newMap.get(emp.id).Reason_Code__c == 'Rehire') ){

                        //Creating New Position Employee
                        AxtriaSalesIQTM__Position_Employee__c posEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
                        posEmp2.Name = emp.Name+'_'+emp.Territory__c+'_'+emp.Territory_Name__c;
                        posEmp2.AxtriaSalesIQTM__Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                        posEmp2.AxtriaSalesIQTM__Employee__c = emp.Id;
                        posEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                        posEmp2.AxtriaSalesIQTM__Effective_Start_Date__c = emp.Assignment_Start_Date__c;
                        posEmp2.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
                        //posEmp2.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
                        posEmpNewInsertList.add(posEmp2);

                        Training_Journey__c ntr = new Training_Journey__c();
                        ntr.Employee__c = emp.Id;
                        ntr.Training_Start_Date__c = emp.Last_Hire_Date__c;
                        ntr.Training_Reason__c = 'On Training due to '+emp.Reason_Code__c;
                        if(emp.Reason_Code__c == 'Hire'){
                            ntr.Training_Type__c = 'New Hire Training';
                        }
                        if(emp.Reason_Code__c == 'Rehire'){
                            ntr.Training_Type__c = 'Rehire Training';
                        }
                        newTrainingJourneyList.add(ntr);

                        // create Employee History Record
                        Employee_History__c empHis = new Employee_History__c();
                        empHis.Employee_ID__c = emp.AxtriaSalesIQTM__Employee_ID__c;
                        empHis.IC_Status__c = emp.IC_Status__c;
                        empHis.Start_Date__c = emp.Last_Hire_Date__c;
                        empHis.End_Date__c = Date.newInstance(4000, 12, 31);
                        empHis.Event_Name__c = emp.Reason_Code__c;
                        empHis.Action_Reason__c = 'Training started again due to Rehire event therefore Creating this record';
                        empHis.Employee__c = emp.Id;
                        empHis.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                        empHis.LatestHistory__c = true;
                        empHistoryInsertList.add(empHis);

                    }
                    // on change of training End date manually by the user
                    if(trigger.oldMap.get(emp.id).Training_End_Date__c != trigger.newMap.get(emp.id).Training_End_Date__c){
                        if(emp.Termination_Date__c == null && trigger.oldMap.get(emp.id).Training_End_Date__c == null){
                            for(Employee_History__c empHis : empHistoryUpdate2List){
                                if(empHis.Employee__c == emp.Id && empHis.LatestHistory__c == true){
                                    empHis.End_Date__c = emp.Training_End_Date__c;
                                    empHis.LatestHistory__c = false;
                                    empHis.Action_Reason__c = 'Training is completed by the User manually therefore end dating this record';
                                    empHistoryUpdateList.add(empHis);
                                }
                            }
                            // create Employee History Record
                            Employee_History__c empHis = new Employee_History__c();
                            empHis.Employee_ID__c = emp.AxtriaSalesIQTM__Employee_ID__c;
                            empHis.IC_Status__c = emp.IC_Status__c;
                            empHis.Start_Date__c = emp.Training_End_Date__c+1;
                            empHis.End_Date__c = Date.newInstance(4000, 12, 31);
                            empHis.Event_Name__c = 'IC Status updated';
                            empHis.Action_Reason__c = 'IC Status Updated';
                            empHis.Employee__c = emp.Id;
                            empHis.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                            empHis.LatestHistory__c = true;
                            empHistoryInsertList.add(empHis);

                        }
                    } 
                    // on change of Position Code
                    if(trigger.oldMap.get(emp.id).Position_Code__c != trigger.newMap.get(emp.id).Position_Code__c){
                        // end dating old position Employee record and creating new one
                        for(AxtriaSalesIQTM__Position_Employee__c posEmp : existingPosEmpMapping){
                            if(emp.AxtriaSalesIQTM__Current_Territory__c != posEmp.AxtriaSalesIQTM__Position__c && (posEmp.AxtriaSalesIQTM__Assignment_Status__c == 'Active')){
                                posEmp.Name = emp.Name+'_'+emp.Territory__c+'_'+emp.Territory_Name__c;
                                posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                                posEmp.AxtriaSalesIQTM__Effective_End_Date__c = emp.Assignment_Start_Date__c -1;
                                //posEmp.AxtriaSalesIQTM__Assignment_Status__c = 'Inactive';
                                
                                /*if(HistoryMap.size()>0){
                                    
                                }*/
                                //Creating New Position Employee
                                AxtriaSalesIQTM__Position_Employee__c posEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
                                posEmp2.Name = emp.Name+'_'+emp.Territory__c+'_'+emp.Territory_Name__c;
                                posEmp2.AxtriaSalesIQTM__Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                                posEmp2.AxtriaSalesIQTM__Employee__c = emp.Id;
                                posEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                                posEmp2.AxtriaSalesIQTM__Effective_Start_Date__c = emp.Assignment_Start_Date__c;
                                posEmp2.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(4000, 12, 31);
                                //posEmp2.AxtriaSalesIQTM__Assignment_Status__c = 'Active';
                                posEmpNewInsertList.add(posEmp2);

                                
                            }
                        }
                        
                    }

                    // After HIR Update case, update the Position Start Date of Emp Assignment
                    if(emp.Reason_Code__c == 'Hire'){
                        if(trigger.oldMap.get(emp.id).Position_Start_Date__c != trigger.newMap.get(emp.id).Position_Start_Date__c){
                            if(trigger.oldMap.get(emp.id).Position_Code__c == trigger.newMap.get(emp.id).Position_Code__c){
                                for(AxtriaSalesIQTM__Position_Employee__c posEmp : existingPosEmpMapping){
                                    if((emp.AxtriaSalesIQTM__Current_Territory__c == posEmp.AxtriaSalesIQTM__Position__c) && 
                                        emp.Id == posEmp.AxtriaSalesIQTM__Employee__c){
                                        posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = emp.Position_Start_Date__c;
                                    }
                                }
                            }
                        }
                    }
                    

                    
                    if(emp.Reason_Code__c == 'Terminate Employee'){
                        for(AxtriaSalesIQTM__Position_Employee__c posEmp : existingPosEmpMapping){
                            if(emp.AxtriaSalesIQTM__Current_Territory__c == posEmp.AxtriaSalesIQTM__Position__c){
                                posEmp.Name = emp.Name+'_'+emp.Territory__c+'_'+emp.Territory_Name__c;
                                posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
                                posEmp.AxtriaSalesIQTM__Effective_End_Date__c = emp.Termination_Date__c;
                                posEmp.isActive__c = False;
                                
                            }
                        }
                    }
                    // on transfer event check if functional area id is changed then create transfer journey.
                    if(emp.Reason_Code__c == 'Transfer'){
                        if(trigger.oldMap.get(emp.id).Functional_Area_ID__c != trigger.newMap.get(emp.id).Functional_Area_ID__c){
                            // create new Training Journey Record
                            Training_Journey__c ntr = new Training_Journey__c();
                            ntr.Employee__c = emp.Id;
                            ntr.Training_Start_Date__c = emp.Position_Start_Date__c;
                
                            ntr.Training_Reason__c = 'On Training due to '+emp.Reason_Code__c+' because Functional Area ID is also changed';
                            ntr.Training_Type__c = 'Transfer Training';
                            newTrainingJourneyList.add(ntr);
                        }   
                    }

                    if(BatchProcessCRRosterEvents.stopExecution != true){
                        for(Training_Journey__c tr : existingTrainingJourneyMapping){
                            if(emp.Latest_Training_Journey__c == tr.id && (emp.Reason_Code__c == 'Transfer' || emp.Reason_Code__c == 'Hire' || emp.Reason_Code__c == 'Rehire' || emp.Reason_Code__c == 'Update Terminate Employee' || emp.Reason_Code__c == 'Terminate Employee')){
                                
                                
                                // on Update of New Hire and rehire case
                                if((tr.Training_Type__c == 'New Hire Training' || tr.Training_Type__c == 'Rehire Training') && emp.Reason_Code__c != 'Transfer'){
                                    // Existing Training does not end dated.
                                    if(tr.Training_End_Date__c == null){
                                        tr.Training_Start_Date__c = emp.Last_Hire_Date__c;
                                        tr.Training_Reason__c = 'On Training due to '+emp.Reason_Code__c;
                                        if(emp.Reason_Code__c == 'Hire'){
                                            tr.Training_Type__c = 'New Hire Training';
                                        }
                                        if(emp.Reason_Code__c == 'Rehire'){
                                            tr.Training_Type__c = 'Rehire Training';
                                        }
                                        if(emp.Reason_Code__c == 'Update Terminate Employee' || emp.Reason_Code__c == 'Terminate Employee'){
                                            tr.Training_End_Date__c = emp.Termination_Date__c;
                                            tr.Training_Reason__c = 'Training Completed due to '+emp.Reason_Code__c;
                                        }
                                    }
                                    else{
                                        // create new Training Journey Record
                                        if(emp.Reason_Code__c != 'Terminate Employee'){
                                            Training_Journey__c ntr = new Training_Journey__c();
                                            ntr.Employee__c = emp.Id;
                                            ntr.Training_Start_Date__c = emp.Position_Start_Date__c;
                                            ntr.Training_Reason__c = 'On Training due to '+emp.Reason_Code__c;
                                            if(emp.Reason_Code__c == 'Hire'){
                                                ntr.Training_Type__c = 'New Hire Training';
                                            }
                                            if(emp.Reason_Code__c == 'Rehire'){
                                                ntr.Training_Type__c = 'Rehire Training';
                                            }
                                            newTrainingJourneyList.add(ntr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /*
                    for(Time_Type_Jounery__c  typejourney : existingTimeTypeJounery){
                        if(emp.id == typejourney.Employee__c){
                            System.debug('@@@ Inside time type journey code');
                            if(emp.Reason_Code__c != 'Time Type Journey Updated' && typeJourney.Active_TimeType__c == true){
                                typejourney.FTE__c = emp.FTE__c;
                                typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                                typejourney.Time_Type__c = emp.Time_Type__c;
                            }
                            if(emp.Reason_Code__c == 'Update Terminate Employee' || emp.Reason_Code__c == 'Terminate Employee'){
                                typejourney.FTE_End_Date__c = emp.Termination_Date__c;
                            }
                            System.debug('@@@ Inside time type journey code typejourney.FTE_Start_Date__c ='+typejourney.FTE_Start_Date__c);
                        }
                    }*/
                }
                
                
                if(newLeaveJounery.size()>0){
                    insert newLeaveJounery;
                }
                if(existingLeaveJounery.size()>0){
                    update existingLeaveJounery;
                }

                if(newCREmployeeFeedList.size()>0){
                    insert newCREmployeeFeedList;
                }
                if(existingCREmployeeFeedList.size()>0){
                    update existingCREmployeeFeedList;
                }
                if(existingTimeTypeJounery.size()>0){
                    update existingTimeTypeJounery;
                }
                if(existingPosEmpMapping.size()>0){
                    update existingPosEmpMapping;
                }
                if(existingTrainingJourneyMapping.size()>0){
                    update existingTrainingJourneyMapping;
                }

                if(posEmpNewInsertList.size()>0){
                    insert posEmpNewInsertList;
                }

                if(empHistoryUpdateList.size()>0){
                    update empHistoryUpdateList;
                }

                if(empHistoryInsertList.size()>0){
                    insert empHistoryInsertList;
                }

                if(newTrainingJourneyList.size()>0){
                    insert newTrainingJourneyList;
                }
                update[select id from AxtriaSalesIQTM__Position_Employee__c];
            }  
        }
        /*
        if(trigger.isAfter && trigger.isUpdate){
            System.debug('@@@ Inside 2nd After Update Block of Emp Trigger');
            List<Time_Type_Jounery__c> existingTimeType2Jounery = [select id, Employee__c, FTE__c,FTE_End_Date__c, FTE_Start_Date__c, Time_Type__c  from Time_Type_Jounery__c];
            List<Time_Type_Jounery__c> updateTimeType2Jounery = new list<Time_Type_Jounery__c>();        
            for(AxtriaSalesIQTM__Employee__c  emp : trigger.new){
                for(Time_Type_Jounery__c  typejourney : existingTimeType2Jounery){
                    if(emp.id == typejourney.Employee__c ){
                        System.debug('@@@ Inside time type journey code');
                        typejourney.FTE__c = emp.FTE__c;
                        typejourney.FTE_Start_Date__c = emp.FTE_Start_Date__c;
                        typejourney.Time_Type__c = emp.Time_Type__c;
                        if(emp.Reason_Code__c == 'Terminate Employee'){
                            typejourney.FTE_End_Date__c = emp.Termination_Date__c;
                        }
                        updateTimeType2Jounery.add(typejourney);
                        System.debug('@@@ Inside time type journey code typejourney.FTE_Start_Date__c ='+typejourney.FTE_Start_Date__c);
                    }
                }
            }

            if(updateTimeType2Jounery.size()>0){
                update updateTimeType2Jounery;
            }
        }*/
        
        if(BatchProcessCRRosterEvents.stopExecution != true && trigger.isAfter){
            List<Employee_History__c> EmpHisUpdateList = new List<Employee_History__c> ();
            List<Employee_History__c> EmpHisList = [Select id, Employee__c, Email__c, Position__c , Position_Title__c, Reason_Code__c, Personnel_Area_Number__c, Personnel_Number__c, Personnel_Area__c, Personnel_Subarea_Name__c, Personnel_Subarea_Code__c, Geography_Name__c, First_Name__c, Hire_Date__c , Last_Name__c , Payroll_Id__c , Sales_Force__c , Sales_Force_Code__c , Client_Territory_Code__c, Termination_Date__c , Position_Type__c from Employee_History__c];
            List<AxtriaSalesIQTM__Position_Employee__c> PosEmpList = [select id, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.SalesforceCode__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__Territory_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.Sales_Force_Code__c, AxtriaSalesIQTM__Position__r.Sales_Force__c, AxtriaSalesIQTM__Employee__r.Termination_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.Payroll_Id__c, AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c, AxtriaSalesIQTM__Employee__r.Personnel_Area__c, AxtriaSalesIQTM__Employee__r.Personnel_Number__c, AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c, AxtriaSalesIQTM__Employee__r.Reason_Code__c, AxtriaSalesIQTM__Employee__r.Position_Title__c, AxtriaSalesIQTM__Position__r.SalesforceName__c,  AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c  from AxtriaSalesIQTM__Position_Employee__c];        
            for(Employee_History__c empHis : EmpHisList){
                for(AxtriaSalesIQTM__Position_Employee__c posEmp : PosEmpList){
                    if(empHis.Employee__c == posEmp.AxtriaSalesIQTM__Employee__c && empHis.Position__c == posEmp.AxtriaSalesIQTM__Position__c){
                        empHis.Client_Territory_Code__c = posEmp.AxtriaSalesIQST__Territory_ID__c;
                        empHis.Position_Type__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
                        empHis.Termination_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Termination_Date__c;
                        empHis.Sales_Force_Code__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceCode__c;
                        empHis.Sales_Force__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceName__c;
                        empHis.Payroll_Id__c = posEmp.AxtriaSalesIQTM__Employee__r.Payroll_Id__c;
                        empHis.Last_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                        empHis.Hire_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c;
                        empHis.First_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                        empHis.Geography_Name__c = posEmp.AxtriaSalesIQTM__Position__r.Name;
                        empHis.Personnel_Subarea_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c;
                        empHis.Personnel_Subarea_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c;
                        empHis.Personnel_Area__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area__c;
                        empHis.Personnel_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Number__c;
                        empHis.Personnel_Area_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c;
                        empHis.Reason_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Reason_Code__c;
                        empHis.Position_Title__c = posEmp.AxtriaSalesIQTM__Employee__r.Position_Title__c; 
                        empHis.Email__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                
                        EmpHisUpdateList.add(empHis);
                    }
                }
            }
            
            if(EmpHisUpdateList.size()>0){
                update EmpHisUpdateList;
            }

            BatchProcessCRRosterEvents.stopExecution = true;

        }
    }
}