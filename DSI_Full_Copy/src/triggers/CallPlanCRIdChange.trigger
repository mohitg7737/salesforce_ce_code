//Trigger to restrict update of Change_Request_ID of call plan CRs.
trigger CallPlanCRIdChange on AxtriaSalesIQTM__CR_Call_Plan__c (before update) {
	AxtriaSalesIQTM__TriggerContol__c isAllow = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanCRIdChange');
    if ( isAllow==null || !isAllow.AxtriaSalesIQTM__IsStopTrigger__c){
        for(AxtriaSalesIQTM__CR_Call_Plan__c crCallPlan : trigger.new){
            if(crCallPlan.AxtriaSalesIQTM__Change_Request_ID__c!=trigger.oldMap.get(crCallPlan.Id).AxtriaSalesIQTM__Change_Request_ID__c ){
                crCallPlan.addError(System.Label.Change_Request_ID_update);
            }
        }
    }
}