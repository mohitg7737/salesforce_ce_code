trigger sendEmailToOpsTeamOnTermREvent on AxtriaSalesIQTM__Employee__c(after update, after insert) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');
    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true && BatchProcessCRRosterEvents.stopExecution != true){
        for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
            if(emp.Functional_Area_ID__c == '500604'){
                Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
                String [] toAddresses = new list<string> {'piyush.shrivastava@axtria.com','shradha.chhabra@axtria.com','ankit.sogani@axtria.com','mayank.budhiraja@axtria.com','lakshmi.nair@axtria.com','akash.ranjan@axtria.com','bichiter.singh@axtria.com'};
                singEmail.setToAddresses (toAddresses);
                String subject = 'DSI Roster '+emp.Action_Type__c+' Reminder !!!';
                singEmail.setSubject (subject);
                string content;
                content = 'Hi Team, <br/><br/>This Employee '+emp.AxtriaSalesIQTM__Employee_ID__c+' has been hired/transfered to market Access Team, Please Take appropriate action related to their SIC Status, History Records and Journey Records. <br/>Please refer to SOP and take appropriate actions.<br/> Thank you, <br/> Axtria SalesIQ Team';
                
                
                singEmail.setHtmlBody(content);
                Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
            }
            if(emp.Action_Type__c == 'TERM-R' || emp.Action_Type__c == 'HIR-R' || emp.Action_Type__c == 'LOA-R'|| emp.Action_Type__c == 'RFL-R'){
                Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
                String [] toAddresses = new list<string> {'piyush.shrivastava@axtria.com','shradha.chhabra@axtria.com','ankit.sogani@axtria.com','mayank.budhiraja@axtria.com','lakshmi.nair@axtria.com','akash.ranjan@axtria.com','bichiter.singh@axtria.com'};
                singEmail.setToAddresses (toAddresses);
                String subject = 'DSI Roster '+emp.Action_Type__c+' Reminder !!!';
                singEmail.setSubject (subject);
                string content;
                content = 'Hi Team, <br/><br/>The Action Code '+emp.Action_Type__c+' has occured on '+emp.AxtriaSalesIQTM__Employee_ID__c+'  Employee ID. <br/>Please refer to SOP and take appropriate actions.<br/> Thank you, <br/> Axtria SalesIQ Team';
                
                singEmail.setHtmlBody(content);
                Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
            }
        } 
    }
}