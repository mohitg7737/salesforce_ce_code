trigger updateEmpHistoryFromPosEmp on AxtriaSalesIQTM__Position_Employee__c (after insert) {
    List<Employee_History__c> EmpHisUpdateList = new List<Employee_History__c> ();
    List<Employee_History__c> EmpHisList = [Select id, Employee__c, Email__c, Position__c , Position_Title__c, Reason_Code__c, Personnel_Area_Number__c, Personnel_Number__c, Personnel_Area__c, Personnel_Subarea_Name__c, Personnel_Subarea_Code__c, Geography_Name__c, First_Name__c, Hire_Date__c , Last_Name__c , Payroll_Id__c , Sales_Force__c , Sales_Force_Code__c , Client_Territory_Code__c, Termination_Date__c , Position_Type__c from Employee_History__c];
    for(Employee_History__c empHis : EmpHisList){
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : [select id, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__Territory_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.Sales_Force_Code__c, AxtriaSalesIQTM__Position__r.Sales_Force__c, AxtriaSalesIQTM__Employee__r.Termination_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.Payroll_Id__c, AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c, AxtriaSalesIQTM__Employee__r.Personnel_Area__c, AxtriaSalesIQTM__Employee__r.Personnel_Number__c, AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c, AxtriaSalesIQTM__Employee__r.Reason_Code__c, AxtriaSalesIQTM__Employee__r.Position_Title__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c  from AxtriaSalesIQTM__Position_Employee__c]){
            if(empHis.Employee__c == posEmp.AxtriaSalesIQTM__Employee__c && empHis.Position__c == posEmp.AxtriaSalesIQTM__Position__c){
                empHis.Client_Territory_Code__c = posEmp.AxtriaSalesIQST__Territory_ID__c;
                empHis.Position_Type__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
                empHis.Termination_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Termination_Date__c;
                empHis.Sales_Force_Code__c = posEmp.AxtriaSalesIQTM__Position__r.Sales_Force_Code__c;
                empHis.Sales_Force__c = posEmp.AxtriaSalesIQTM__Position__r.Sales_Force__c;
                empHis.Payroll_Id__c = posEmp.AxtriaSalesIQTM__Employee__r.Payroll_Id__c;
                empHis.Last_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                empHis.Hire_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c;
                empHis.First_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                empHis.Geography_Name__c = posEmp.AxtriaSalesIQTM__Position__r.Name;
                empHis.Personnel_Subarea_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c;
                empHis.Personnel_Subarea_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c;
                empHis.Personnel_Area__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area__c;
                empHis.Personnel_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Number__c;
                empHis.Personnel_Area_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c;
                empHis.Reason_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Reason_Code__c;
                empHis.Position_Title__c = posEmp.AxtriaSalesIQTM__Employee__r.Position_Title__c; 
                empHis.Email__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
        
                EmpHisUpdateList.add(empHis);
            }
        }
    }
    
    if(EmpHisUpdateList.size()>0){
        update EmpHisUpdateList;
    }
}