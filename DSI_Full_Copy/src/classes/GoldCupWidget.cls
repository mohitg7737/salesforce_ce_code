/*************************************************************************
* @author     : A1942
* @date       : June-08-2021
* @description: Controller class for all dashboard custom widgets VF pages
* Revison(s)  : v1.0
*************************************************************************/
global class GoldCupWidget{

    public boolean dataAvailable {get;set;}
    
    public String nationRank {get;set;}
    public String regionRank {get;set;}
    public String payout {get;set;}
    public String payoutText {get;set;}
    public String goldCupPublishDate  {get;set;}
    public String userBU {get;set;}
    public String userProfileName {get;set;}

    //Varaibles related to National Performance product widget
    public String selected_NP_Product {get;set;}
    public String publish_NP_Date {get;set;}
    public List<SelectOption> product_NP_List {get;set;}
    public Map<String, List<Map<String, String>>> productMetric_NP_Map;

    //public String m_p_monthlyDataJson {get;set;} // Metric performance monthly dataset

    public Map<String, List<Map<String, String>>> dataMap;


    public GoldCupWidget(){

        System.debug('GoldCupWidget constructor called ...........');
        dataAvailable = true;

        
        

        selected_NP_Product = '';
       
        publish_NP_Date = '';
        product_NP_List = new List<SelectOption>();
        productMetric_NP_Map = new Map<String, List<Map<String, String>>>();

        
        
        dataMap = new Map<String, List<Map<String, String>>>();

        try{
           
            List<User> loggedInUser = [Select Id, profileId,SIQIC__Business_Unit__c from User where Id =: UserInfo.getUserId() and IsActive=true WITH SECURITY_ENFORCED];
            List<Profile> userProfile = [Select Name from profile where id =:loggedInUser[0].profileId ];
            userBU = loggedInUser[0].SIQIC__Business_Unit__c;
            System.debug('loggedInUser :: '+loggedInUser + '-- bu- '+ userBU);
            System.debug('userProfile :: '+userProfile);

            //String userProfileName = '';
            if(userProfile!=null && userProfile.size()>0 && String.isNotBlank(userProfile[0].Name)){
                userProfileName = userProfile[0].Name;
            }
            System.debug('userProfileName :: '+userProfileName);
            
            List<SIQIC__Reports__c> reportRecord = new List<SIQIC__Reports__c>();

            if(!userProfileName.containsIgnoreCase('HO')){
                reportRecord = [Select Id, SIQIC__Report_Definition__c, SIQIC__Role__c, SIQIC__User__c, SIQIC__EmpId__c From SIQIC__Reports__c Where SIQIC__Report_Type__c ='GoldCup' and SIQIC__Latest__c = true and SIQIC__User__c=:UserInfo.getUserId() WITH SECURITY_ENFORCED];
            }
            System.debug('reportRecord :: '+reportRecord);

            if(reportRecord!=null && reportRecord.size()>0){
                String reportId = '', empId = '', filterStr= '';
                reportId = String.isBlank(reportRecord[0].SIQIC__Report_Definition__c) ? '' : reportRecord[0].SIQIC__Report_Definition__c;
                empId = String.isBlank(reportRecord[0].SIQIC__EmpId__c) ? '' : reportRecord[0].SIQIC__EmpId__c;
                System.debug('reportId : '+reportId+', empId : '+empId);
                if(String.isNotBlank(reportId)){
                    
                    if(String.isNotBlank(empId)){
                        filterStr = reportId +';'+ empId+ ';GoldCup';
                        dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', filterStr); // 2 is here type of server which is reporting server.
                    }
                }
            }
            
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', 'a335e000000HyMFAA0;10000070'); // TO DO remove this line after Data testing
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', '2;11111');
            System.debug('dataMap :: '+dataMap);

            if(dataMap != null && dataMap.size()>0){

                GoldCup();
                
                
            }else {
                dataAvailable = false;
            }
            

        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }        
    }

    public void GoldCup(){

        nationRank = '';
        regionRank = '';
        payout = '';
        payoutText = '';   
        goldCupPublishDate = '';     
        try{
            List<Map<String, String>> records = dataMap.get('DS_0');
            System.debug('GoldCup : '+records);
            if(records!=null && !records.isEmpty()){
                nationRank = records[0].get('Nation_Rank');
                regionRank =  records[0].get('Region_Rank');
                
                goldCupPublishDate = records[0].get('Publish_Date');
                Date dt = Date.valueOf( goldCupPublishDate ); 
                goldCupPublishDate = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            }
            
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.Goldcup()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    

  
}