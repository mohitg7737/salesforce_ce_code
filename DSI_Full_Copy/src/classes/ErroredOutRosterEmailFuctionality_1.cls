global class ErroredOutRosterEmailFuctionality_1 implements Schedulable {
  global string mainContent;
  global List<AxtriaSalesIQST__SIQ_Employee_Master__c> stagingEmpList;
  global list<String> toAddresses = new List<String>();
   global void execute(SchedulableContext sc) {
      
      
             stagingEmpList = [Select id, AxtriaSalesIQST__SIQ_Last_Name__c,AxtriaSalesIQST__SIQ_Employee_ID__c,SIQ_Position_Code_c__c,SIQ_Position_Title_c__c,SIQ_DSI_Email_Id_c__c,SIQ_Action_Type_c__c,Action_Reason__c,AxtriaSalesIQST__SIQ_First_Name__c,SIQ_Action_Start_Date_c__c,SIQ_Action_End_Date_c__c,name, Errorneous_Record__c, Error_Description__c from AxtriaSalesIQST__SIQ_Employee_Master__c where Errorneous_Record__c = true and isActive__c = true];
        System.debug('@@@ stagingEmpList = '+stagingEmpList);
        //Set Header values of the file
        string csvHeader = 'Employee_Id, First_Name, Last_Name, Position_Code, Position_Title, DSI_Email_Id, Action_Type, Action_Reason, Action_Start_Date, Action_End_Date, Reason Code \n';
         mainContent = csvHeader;
        if(stagingEmpList.size()>0){
            System.debug('@@@ stagingEmpList = '+stagingEmpList);
            for (AxtriaSalesIQST__SIQ_Employee_Master__c siqEmp: stagingEmpList){
                //Adding records in a string
                string recordString = siqEmp.AxtriaSalesIQST__SIQ_Employee_ID__c+','+siqEmp.AxtriaSalesIQST__SIQ_First_Name__c+','+siqEmp.AxtriaSalesIQST__SIQ_Last_Name__c+','+siqEmp.SIQ_Position_Code_c__c+','+siqEmp.SIQ_Position_Title_c__c+','+siqEmp.SIQ_DSI_Email_Id_c__c+','+siqEmp.SIQ_Action_Type_c__c+','+siqEmp.Action_Reason__c+','+siqEmp.SIQ_Action_Start_Date_c__c+','+siqEmp.SIQ_Action_End_Date_c__c+','+siqEmp.Error_Description__c +'\n';
                mainContent += recordString;
            }
         }

       
        

 

        Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment ();
        //Create CSV file using Blob
        blob csvBlob = Blob.valueOf (mainContent);
        string csvname= 'ErroredRosterData.csv';
        csvAttcmnt.setFileName (csvname);
        csvAttcmnt.setBody (csvBlob);
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage ();
        String [] toAddresses = new list<string> ();
        
        EmailRoster__mdt[] emailroster = [SELECT MasterLabel,DeveloperName FROM EmailRoster__mdt];

         for (EmailRoster__mdt email : emailroster ){
            
            toAddresses.add(email.MasterLabel);
        }

        system.debug('toAddresses'+toAddresses);
        
        //Set recipient list
        singEmail.setToAddresses (toAddresses);
        String subject = 'DSI Roster Kickout Response';
        singEmail.setSubject (subject);
        String yesterdayDate = string.valueOf(Date.today()-1);
        String recordCount = string.valueOf(stagingEmpList.size());
        string content = 'Hi, <br/><br/>The Workday feed received on  '+yesterdayDate+'has '+recordCount+' records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team';
        if(stagingEmpList.size()>0){
            singEmail.setHtmlBody(content);
            
        }
        else{
            singEmail.setHtmlBody(content);
            //singEmail.setHtmlBody('Hi, <br/><br/>The Workday feed received on yesterday has some records that have failed the validation checks while processing the feed. These records have been dropped from the system. Their details along with the reason to drop are present in the file attached. <br/><br/>This email is intended to notify DSI about the dropped cases, rest of the feed has been processed.<br/> Thank you, <br/> Axtria SalesIQ Team');
            //singEmail.setPlainTextBody ('There were no Errored records reported in Last Workday Feed');
        }
        
        
        //Set blob as CSV file attachment
        if(stagingEmpList.size()>0){
            singEmail.setFileAttachments (new Messaging.EmailFileAttachment []{csvAttcmnt});
        }
        Messaging.SendEmailResult [] r = Messaging.sendEmail (new Messaging.SingleEmailMessage [] {singEmail});
   
}

public void dummyFunction(){
integer i = 0;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
}

}