/*
@author     : A1942
@date       : June-10-2021
@description: Test class for ReportMappingCtrl.cls
Revison(s)  : v1.0
*/
@isTest (SeeAllData=true)
private class ReportMappingCtrlTest {
static testMethod void testMethod1() {
	ReportMappingCtrl r =  new ReportMappingCtrl();
	r.selectedReportType = [Select SIQIC__Report_Type__c from SIQIC__Report_Type_configuration__c where enable_Component_Metric_Mapping__c = true limit 1].SIQIC__Report_Type__c;
	r.fetchReportInstance();
	r.selectedReportInstance =[Select Id, Name From SIQIC__Report_Defination__c where SIQIC__ReportExecution__c = null and SIQIC__Type__c = :r.selectedReportType limit 1].id;
	r.fetchComponentType();
	r.selectedComponentType = [Select SIQIC__Component_Name__c from SIQIC__Report_Component_Mapping__c where SIQIC__Report_Definition__c = :r.selectedReportInstance order by SIQIC__SequenceNo__c limit 1].SIQIC__Component_Name__c;
	r.fetchMetricMapping();
	r.submitMapping();
}
}