/*************************************************************************
* @author     : A1942
* @date       : June-08-2021
* @description: Controller class for all dashboard custom widgets VF pages
* Revison(s)  : v1.0
*************************************************************************/
global class DashboardCustomWidgetsCtrl{

    public boolean dataAvailable {get;set;}
    
    public String nationRank {get;set;}
    public String regionRank {get;set;}
    public String payout {get;set;}
    public String payoutText {get;set;}
    public String payoutPublishDate {get;set;}
    public String userBU {get;set;}
    public String userProfileName {get;set;}

    //Varaibles related to National Performance product widget
    public String selected_NP_Product {get;set;}
    public String publish_NP_Date {get;set;}
    public List<SelectOption> product_NP_List {get;set;}
    public Map<String, List<Map<String, String>>> productMetric_NP_Map;

    //public String m_p_monthlyDataJson {get;set;} // Metric performance monthly dataset

    public Map<String, List<Map<String, String>>> dataMap;


    public DashboardCustomWidgetsCtrl(){

        System.debug('DashboardCustomWidgetsCtrl constructor called ...........');
        dataAvailable = true;

        selectedBudgetTeam = '';
        budgetPublishDate = '';
        budgetTeamValueMap = new Map<String, String>();
        budgetTeamPercentMap = new Map<String, String>();
        budgetTeamPublishMap = new Map<String, String>();

        selected_NP_Product = '';
        selected_NP_DataJson = '';
        publish_NP_Date = '';
        product_NP_List = new List<SelectOption>();
        productMetric_NP_Map = new Map<String, List<Map<String, String>>>();

        selected_MP_Product = '';
        publish_MP_Date = '';
        selected_MP_DataJson = '';
        product_MP_List = new List<SelectOption>();
        productMetric_MP_Map = new Map<String, List<Map<String, String>>>();

        //selectedVacantGeoType = '';
        //vacantGeoTypes = new List<SelectOption>();
        selectedVacantGeoData = '';
        //level_to_team_geo_map = new Map<String, Map<String, Integer>>();
        teamGeoCountMap = new Map<String, Integer>();

        cpfSummaryTeams = new List<SelectOption>();
        teamInstanceSet = new Set<String>();
        selectedCpfTeam = '';
        cpfTeamNameMap = new Map<String,String>();
        cpfTeamToTeamInsMap = new Map<String, String>();

        dataMap = new Map<String, List<Map<String, String>>>();

        try{
           
            List<User> loggedInUser = [Select Id, profileId,SIQIC__Business_Unit__c from User where Id =: UserInfo.getUserId() and IsActive=true WITH SECURITY_ENFORCED];
            List<Profile> userProfile = [Select Name from profile where id =:loggedInUser[0].profileId ];
            userBU = loggedInUser[0].SIQIC__Business_Unit__c;
            System.debug('loggedInUser :: '+loggedInUser + '-- bu- '+ userBU);
            System.debug('userProfile :: '+userProfile);

            //String userProfileName = '';
            if(userProfile!=null && userProfile.size()>0 && String.isNotBlank(userProfile[0].Name)){
                userProfileName = userProfile[0].Name;
            }
            System.debug('userProfileName :: '+userProfileName);
            
            List<SIQIC__Reports__c> reportRecord = new List<SIQIC__Reports__c>();

            if(userProfileName.containsIgnoreCase('HO')){
                reportRecord = [Select Id, SIQIC__Report_Definition__c, SIQIC__Role__c, SIQIC__User__c, SIQIC__EmpId__c From SIQIC__Reports__c Where SIQIC__Report_Type__c ='Dashboard' and Dashboard_Enabled__c = true WITH SECURITY_ENFORCED];
            }else{
                reportRecord = [Select Id, SIQIC__Report_Definition__c, SIQIC__Role__c, SIQIC__User__c, SIQIC__EmpId__c From SIQIC__Reports__c Where SIQIC__Report_Type__c ='Dashboard' and Dashboard_Enabled__c = true and SIQIC__User__c=:UserInfo.getUserId() WITH SECURITY_ENFORCED];
            }
            System.debug('reportRecord :: '+reportRecord);

            if(reportRecord!=null && reportRecord.size()>0){
                String reportId = '', empId = '', filterStr= '';
                reportId = String.isBlank(reportRecord[0].SIQIC__Report_Definition__c) ? '' : reportRecord[0].SIQIC__Report_Definition__c;
                empId = String.isBlank(reportRecord[0].SIQIC__EmpId__c) ? '' : reportRecord[0].SIQIC__EmpId__c;
                System.debug('reportId : '+reportId+', empId : '+empId);
                if(String.isNotBlank(reportId)){
                    
                    if(userProfileName.containsIgnoreCase('HO')){
                        dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', reportId); // 2 is here type of server which is reporting server.
                    }else if(String.isNotBlank(empId)){
                        filterStr = reportId +';'+ empId;
                        dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', filterStr); // 2 is here type of server which is reporting server.
                    }
                }
            }
            
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', 'a335e000000HyMFAA0;10000070'); // TO DO remove this line after Data testing
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', '2;11111');
            System.debug('dataMap :: '+dataMap);

            if(dataMap != null && dataMap.size()>0){

                budgetUtilization();
                
                QTD_Payout();

                nationalPerformance();
                metricPerformance();

                vacantGeos();
            }else {
                dataAvailable = false;
            }
            workbenchEvent();
            cpfSummary();

        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }        
    }

    public Boolean isBudget {get;set;}
    public String budgetPublishDate {get;set;}
    public String selectedBudgetTeam {get;set;}
    public String budgetUtilizationPercent {get;set;}
    public String budgetUtilizationPayout {get;set;}
    public List<SelectOption> budgetTeams {get;set;} 
    public Map<String, String> budgetTeamValueMap {get;set;}
    public Map<String, String> budgetTeamPercentMap {get;set;}
    public Map<String, String> budgetTeamPublishMap {get;set;}
    global void budgetUtilization(){

        System.debug('selectedBudgetTeam :: '+selectedBudgetTeam);
        budgetTeams = new List<SelectOption>();
        isBudget = false;
        try{
            if(String.isBlank(selectedBudgetTeam)){
                List<Map<String, String>> records = new List<Map<String, String>>();
                if(dataMap.get('DS_0')!=null){
                    records.addAll(dataMap.get('DS_0'));
                }
                System.debug('budget : '+records);
                for(Map<String, String> mapObj: records){
                    selectedBudgetTeam = String.isBlank(selectedBudgetTeam) ? mapObj.get('Team') : selectedBudgetTeam;
                    budgetUtilizationPercent = String.isBlank(budgetUtilizationPercent) ? mapObj.get('Budget_Utilization') : budgetUtilizationPercent;
                    budgetUtilizationPayout = String.isBlank(budgetUtilizationPayout) ? mapObj.get('Total_Payout') : budgetUtilizationPayout;
                    
                    budgetTeams.add(new SelectOption(mapObj.get('Team'), mapObj.get('Team')));
                    budgetTeamPercentMap.put(mapObj.get('Team'), mapObj.get('Budget_Utilization'));
                    budgetTeamValueMap.put(mapObj.get('Team'), mapObj.get('Total_Payout'));
                    budgetTeamPublishMap.put(mapObj.get('Team'), mapObj.get('Publish_Date'));
                }
                System.debug('budgetTeams :: '+budgetTeams);
                System.debug('budgetTeamValueMap :: '+budgetTeamValueMap);
            }
            budgetUtilizationPercent = budgetTeamPercentMap.get(selectedBudgetTeam);
            budgetUtilizationPercent = String.valueOf(Decimal.valueOf(budgetUtilizationPercent) * 100);
            budgetUtilizationPercent = String.valueOf(Decimal.valueOf(budgetUtilizationPercent).setScale(2));
            budgetUtilizationPayout =  budgetTeamValueMap.get(selectedBudgetTeam);
            budgetPublishDate = budgetTeamPublishMap.get(selectedBudgetTeam);
            Date dt = Date.valueOf( budgetPublishDate ); 
            budgetPublishDate = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            System.debug('budgetUtilizationPercent :: '+budgetUtilizationPercent+', budgetUtilizationPayout : '+budgetUtilizationPayout);

            if(String.isNotBlank(budgetUtilizationPercent)){
                isBudget = true;
            }
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.budgetUtilization()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public void QTD_Payout(){

        nationRank = '';
        regionRank = '';
        payout = '';
        payoutText = '';   
        payoutPublishDate = '';     
        try{
            List<Map<String, String>> records = dataMap.get('DS_2');
            System.debug('QTD_Payout : '+records);
            if(records!=null && !records.isEmpty()){
                nationRank = records[0].get('National_Rank');
                regionRank =  records[0].get('Region_Rank');
                payout = records[0].get('QTD_Payout');
                payoutText = records[0].get('Payout_Text');
                payoutPublishDate = records[0].get('Publish_Date');
                Date dt = Date.valueOf( payoutPublishDate ); 
                payoutPublishDate = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            }
            System.debug('nationRank : '+nationRank+', nationRank :'+nationRank+', payout : '+payout+', payoutText :'+payoutText);
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.QTD_Payout()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public String selected_NP_DataJson {get;set;}
    global void nationalPerformance(){

        selected_NP_DataJson = '';
        System.debug('selected_NP_Product : '+selected_NP_Product);
        try{
            if(String.isBlank(selected_NP_Product)){
                List<Map<String, String>> records = new List<Map<String, String>>();
                if(dataMap.get('DS_1')!=null){
                    records.addAll(dataMap.get('DS_1'));
                }
                Set<String> prodSet = new Set<String>();
                for(Map<String, String> obj:records){
                    selected_NP_Product = String.isBlank(selected_NP_Product) ? obj.get('Product') : selected_NP_Product;
                    if(!prodSet.contains(obj.get('Product'))){
                        product_NP_List.add(new SelectOption(obj.get('Product'), obj.get('Product')));
                        prodSet.add(obj.get('Product'));
                    }

                    List<Map<String, String>> npData = new List<Map<String, String>>();
                    if(productMetric_NP_Map.containskey(obj.get('Product'))){
                        npData.addAll(productMetric_NP_Map.get(obj.get('Product')));
                    }
                    npData.add(obj);
                    productMetric_NP_Map.put(obj.get('Product'), npData);
                }
                System.debug('selected_NP_Product :: '+selected_NP_Product);
                System.debug('product_NP_List : '+product_NP_List);
                System.debug('productMetric_NP_Map :: '+productMetric_NP_Map);
                if(product_NP_List!=null && product_NP_List.size()==0){
                    product_NP_List.add(new SelectOption('None', 'None'));
                }
            }

            if(productMetric_NP_Map !=null && productMetric_NP_Map.containsKey(selected_NP_Product)){
                selected_NP_DataJson = JSON.serialize(productMetric_NP_Map.get(selected_NP_Product));
                publish_NP_Date = productMetric_NP_Map.get(selected_NP_Product)[0].get('Publish_Date'); 
                Date dt = Date.valueOf( publish_NP_Date ); 
                publish_NP_Date = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            }   
            System.debug('selected_NP_DataJson : '+selected_NP_DataJson);
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.nationalPerformance()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public String selected_MP_Product {get;set;}
    public String selected_MP_DataJson {get;set;}
    public String publish_MP_Date {get;set;}
    public List<SelectOption> product_MP_List {get;set;}
    public Map<String, List<Map<String, String>>> productMetric_MP_Map;
    global void metricPerformance(){

        selected_MP_DataJson = '';
        System.debug('selected_MP_Product : '+selected_MP_Product);
        try{
            if(String.isBlank(selected_MP_Product)){
                List<Map<String, String>> records = new List<Map<String, String>>();
                if(dataMap.get('DS_3')!=null){
                    records.addAll(dataMap.get('DS_3'));
                }
                Set<String> prodSet = new Set<String>();
                for(Map<String, String> obj:records){
                    selected_MP_Product = String.isBlank(selected_MP_Product) ? obj.get('Product') : selected_MP_Product;
                    if(!prodSet.contains(obj.get('Product'))){
                        product_MP_List.add(new SelectOption(obj.get('Product'), obj.get('Product')));
                        prodSet.add(obj.get('Product'));
                    }

                    List<Map<String, String>> mpData = new List<Map<String, String>>();
                    if(productMetric_MP_Map.containskey(obj.get('Product'))){
                        mpData.addAll(productMetric_MP_Map.get(obj.get('Product')));
                    }
                    mpData.add(obj);
                    productMetric_MP_Map.put(obj.get('Product'), mpData);

                }
                System.debug('selected_MP_Product :: '+selected_MP_Product);
                System.debug('product_MP_List : '+product_MP_List);
                System.debug('productMetric_MP_Map :: '+productMetric_MP_Map);
                if(product_MP_List!=null && product_MP_List.size()==0){
                    product_MP_List.add(new SelectOption('None', 'None'));
                }

            }
            if(productMetric_MP_Map !=null && productMetric_MP_Map.containsKey(selected_MP_Product)){
                selected_MP_DataJson = JSON.serialize(productMetric_MP_Map.get(selected_MP_Product));
                publish_MP_Date = productMetric_MP_Map.get(selected_MP_Product)[0].get('Publish_Date');
                Date dt = Date.valueOf( publish_MP_Date ); 
                publish_MP_Date = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            } 
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.metricPerformance()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    //public String selectedVacantGeoType {get;set;}
    //public List<SelectOption> vacantGeoTypes {get;set;} 
    //public Map<String, Map<String, Integer>> level_to_team_geo_map;
    public Map<String, Integer> teamGeoCountMap;
    public String selectedVacantGeoData {get;set;}
    global void vacantGeos(){
        //System.debug('selectedVacantGeoType :: '+selectedVacantGeoType);
        try{
            selectedVacantGeoData = '';
            //if(String.isBlank(selectedVacantGeoType)){
                List<DashboardVacantGeoTeams__c> icTeams = [Select TeamName__c From DashboardVacantGeoTeams__c WITH SECURITY_ENFORCED];
                Set<String> icTeamsSet = new Set<String>();
                for(DashboardVacantGeoTeams__c obj: icTeams){
                    if(String.isNotBlank(obj.TeamName__c)){
                        icTeamsSet.add(obj.TeamName__c);
                        teamGeoCountMap.put(obj.TeamName__c + ' Team', 0);
                    }
                }
                List<DashboardPositionType__c> icPosTypes = [Select Position_Type__c From DashboardPositionType__c WITH SECURITY_ENFORCED];
                Set<String> icPosTypeSet = new Set<String>();
                for(DashboardPositionType__c obj: icPosTypes){
                    if(String.isNotBlank(obj.Position_Type__c)){
                        //selectedVacantGeoType = String.isBlank(selectedVacantGeoType) ? obj.Position_Type__c : selectedVacantGeoType;
                        icPosTypeSet.add(obj.Position_Type__c);
                        //vacantGeoTypes.add(new SelectOption(obj.Position_Type__c,obj.Position_Type__c));
                    }
                }
                System.debug('icTeamsSet : '+icTeamsSet);
                System.debug('icPosTypeSet : '+icPosTypeSet);

                //TO DO remove condition for whitespace from query
                List<AxtriaSalesIQTM__Position__c> positionLst = [Select Id, AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_iD__r.Name IN: icTeamsSet and AxtriaSalesIQTM__Position_Type__c IN: icPosTypeSet and AxtriaSalesIQTM__IsMaster__c = true and (AxtriaSalesIQTM__Is_Unassigned_Position__c=false or AxtriaSalesIQTM__Is_Global_Unassigned_Position__c=false) and AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Manage_Vacant__c=false and AxtriaSalesIQTM__Position_Type__c != 'Hierarchy' and AxtriaSalesIQTM__Assignment_status__c = 'Vacant' WITH SECURITY_ENFORCED];
                System.debug('positionLst :: '+positionLst);
                /*for(AxtriaSalesIQTM__Position__c obj:positionLst){
                    if(String.isNotBlank(obj.AxtriaSalesIQTM__Position_Type__c)){
                        Map<String, Integer> vacantGeoCount = new Map<String, Integer>();
                        if(level_to_team_geo_map.containsKey(obj.AxtriaSalesIQTM__Position_Type__c)){
                            vacantGeoCount.putAll(level_to_team_geo_map.get(obj.AxtriaSalesIQTM__Position_Type__c));
                        }
                        if(vacantGeoCount.containsKey(obj.AxtriaSalesIQTM__Team_iD__r.Name)){
                            vacantGeoCount.put(obj.AxtriaSalesIQTM__Team_iD__r.Name + ' Team', vacantGeoCount.get(obj.AxtriaSalesIQTM__Team_iD__r.Name) + 1);
                        }else {
                            vacantGeoCount.put(obj.AxtriaSalesIQTM__Team_iD__r.Name + ' Team',1);
                        }
                        level_to_team_geo_map.put(obj.AxtriaSalesIQTM__Position_Type__c, vacantGeoCount);
                    }
                }
                System.debug('level_to_team_geo_map :: '+level_to_team_geo_map);*/
                for(AxtriaSalesIQTM__Position__c obj:positionLst){
                    if(String.isNotBlank(obj.AxtriaSalesIQTM__Team_iD__r.Name)){
                        String teamName = obj.AxtriaSalesIQTM__Team_iD__r.Name;
                        /*if(teamName == 'ONCSC2'){
                            teamName = 'ONCSC';
                        }*/
                        teamName += ' Team';
                        Integer count = 1;
                        if(teamGeoCountMap.containsKey(teamName)){
                            count += teamGeoCountMap.get(teamName);
                        }
                        teamGeoCountMap.put(teamName, count);
                    }
                }
            //}

            /*if(level_to_team_geo_map!=null && level_to_team_geo_map.containsKey(selectedVacantGeoType)){
                selectedVacantGeoData = JSON.serialize(level_to_team_geo_map.get(selectedVacantGeoType));
            }*/
            if(teamGeoCountMap!=null && teamGeoCountMap.size()>0){
                selectedVacantGeoData = JSON.serialize(teamGeoCountMap);
            }
            System.debug('selectedVacantGeoData : '+selectedVacantGeoData);

        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.vacantGeos()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public Map<String, Integer> eventEmployeeMap {get;set;}
    public String workbenchEventData {get;set;}
    public void workbenchEvent(){
        workbenchEventData = '';
        eventEmployeeMap = new Map<String, Integer>();
        eventEmployeeMap.put('Employee New Hire', 0);
        eventEmployeeMap.put('Promotion', 0);
        eventEmployeeMap.put('Transfer', 0);
        eventEmployeeMap.put('Terminate Employee', 0);
        eventEmployeeMap.put('Leave of Absence', 0);
        eventEmployeeMap.put('Return from LOA', 0);
        eventEmployeeMap.put('Re-Hire', 0);
        eventEmployeeMap.put('Demotion', 0);
        try{

            List<AxtriaSalesIQST__CR_Employee_Feed__c> employeeList = [Select Id, AxtriaSalesIQST__Event_Name__c, Event_Start_Date__c  From AxtriaSalesIQST__CR_Employee_Feed__c where Event_Start_Date__c = THIS_QUARTER WITH SECURITY_ENFORCED];

           /* List<AxtriaSalesIQTM__Employee__c> employeeList = [Select Id, Action_Type__c, Original_Hire_Date__c, AxtriaSalesIQST__Transferred_to_HO_Date__c, Termination_Date__c, Promotion_date__c From AxtriaSalesIQTM__Employee__c where Original_Hire_Date__c = THIS_QUARTER or AxtriaSalesIQST__Transferred_to_HO_Date__c = THIS_QUARTER or Termination_Date__c = THIS_QUARTER or Promotion_date__c = THIS_QUARTER  WITH SECURITY_ENFORCED];*/
            System.debug('employeeList.size :: '+employeeList.size());
            for(AxtriaSalesIQST__CR_Employee_Feed__c obj: employeeList){

                if(obj.Event_Start_Date__c <= Date.today()){
                    switch on obj.AxtriaSalesIQST__Event_Name__c {
                        when 'Employee New Hire' { // New Hire
                            eventEmployeeMap.put('Employee New Hire', eventEmployeeMap.get('Employee New Hire')+1);
                        }   
                        when 'Transfer' { // Transfer   
                            eventEmployeeMap.put('Transfer', eventEmployeeMap.get('Transfer')+1);
                        }
                        when 'Promotion' { // Promotion
                            eventEmployeeMap.put('Promotion', eventEmployeeMap.get('Promotion')+1);
                        }
                        when 'Terminate Employee' { // Termination
                            eventEmployeeMap.put('Terminate Employee', eventEmployeeMap.get('Terminate Employee')+1);
                        }
                        when 'Leave of Absence' { // LOA
                            eventEmployeeMap.put('Leave of Absence', eventEmployeeMap.get('Leave of Absence')+1);
                        }
                        when 'Return from LOA' { // R-LOA
                            eventEmployeeMap.put('Return from LOA', eventEmployeeMap.get('Return from LOA')+1);
                        }
                        when 'Re-Hire' { // RE-HIRE
                            eventEmployeeMap.put('Re-Hire', eventEmployeeMap.get('Re-Hire')+1);
                        }
                        when 'Demotion' { // Demotion
                            eventEmployeeMap.put('Demotion', eventEmployeeMap.get('Demotion')+1);
                        }
                    }
                }
            }
            workbenchEventData = JSON.serialize(eventEmployeeMap);
            System.debug('workbenchEventData :: '+workbenchEventData);
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.workbenchEvent()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public List<SelectOption> cpfSummaryTeams {get;set;} 
    public String selectedCpfTeam {get;set;}
    public CpfSummaryWrapper cpfSummaryData {get;set;}
    public Set<String> teamInstanceSet;
    public Map<String,String> cpfTeamNameMap;
    public Map<String, String> cpfTeamToTeamInsMap;
    global void cpfSummary(){
        System.debug('selectedCpfTeam : '+selectedCpfTeam);

        cpfSummaryData = new CpfSummaryWrapper();
        try{
            if(String.isBlank(selectedCpfTeam)){
                List<AxtriaSalesIQTM__User_Access_Permission__c> userPositions = [Select Id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name From AxtriaSalesIQTM__User_Access_Permission__c Where AxtriaSalesIQST__isCallPlanEnabled__c=true and AxtriaSalesIQTM__Is_Active__c=true and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__User__c =:UserInfo.getUserId() WITH SECURITY_ENFORCED];
                System.debug('userPositions : '+userPositions);
                Set<String> teamUniqueSet = new Set<String>();
                for(AxtriaSalesIQTM__User_Access_Permission__c obj: userPositions){
                    if(String.isNotBlank(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name) && !teamUniqueSet.contains(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name)){
                        selectedCpfTeam = String.isNotBlank(selectedCpfTeam) ? selectedCpfTeam : obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
                        cpfSummaryTeams.add(new SelectOption(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name + ' Team'));
                        teamUniqueSet.add(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name);
                        teamInstanceSet.add(obj.AxtriaSalesIQTM__Team_Instance__c);
                        cpfTeamNameMap.put(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name + ' Team');
                        cpfTeamToTeamInsMap.put(obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, obj.AxtriaSalesIQTM__Team_Instance__c);
                    }
                }
                System.debug('selectedCpfTeam : '+selectedCpfTeam);
                System.debug('cpfSummaryTeams : '+cpfSummaryTeams);
            }

            List<AxtriaSalesIQTM__Position__c> positions = [Select Id,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Controlling_Team__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQST__Call_Plan_Status__c From AxtriaSalesIQTM__Position__c Where /*AxtriaSalesIQTM__Hierarchy_Level__c IN ('1','2') and*/ AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Team_iD__r.Name=:selectedCpfTeam and (AxtriaSalesIQTM__Is_Unassigned_Position__c=false and AxtriaSalesIQTM__Is_Global_Unassigned_Position__c=false) and AxtriaSalesIQTM__Position_Type__c != 'Hierarchy' and AxtriaSalesIQTM__Team_Instance__c In: teamInstanceSet WITH SECURITY_ENFORCED];
            cpfSummaryData.territoryCount =0; cpfSummaryData.districtCount = 0; cpfSummaryData.territoryApproved = 0; cpfSummaryData.territorySubmitted = 0;
            cpfSummaryData.teamName = cpfTeamNameMap.containsKey(selectedCpfTeam) ? cpfTeamNameMap.get(selectedCpfTeam) : '';
            Set<String> uniqueDistrict = new Set<String>();
            for(AxtriaSalesIQTM__Position__c obj:positions){
                if(obj.AxtriaSalesIQTM__Hierarchy_Level__c == '1'){
                    cpfSummaryData.territoryCount++;
                    if(obj.AxtriaSalesIQST__Call_Plan_Status__c == 'Submitted'){
                        cpfSummaryData.territorySubmitted++;
                    }else if(obj.AxtriaSalesIQST__Call_Plan_Status__c == 'Approved'){
                        cpfSummaryData.territoryApproved++;
                    }

                    if(String.isNotBlank(obj.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Controlling_Team__c) && String.isNotBlank(obj.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                        uniqueDistrict.add(obj.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    }
                }else if(obj.AxtriaSalesIQTM__Hierarchy_Level__c == '2'){
                    cpfSummaryData.districtCount++;
                }
            }
            if(cpfSummaryData.districtCount==0 && positions.size()>0 && String.isNotBlank(positions[0].AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Controlling_Team__c)){
                cpfSummaryData.districtCount = uniqueDistrict.size();
            }
            cpfSummaryData.territorySubmittedPercent = 0; cpfSummaryData.territoryApprovedPercent = 0;
            if(cpfSummaryData.territoryCount!=0){
                cpfSummaryData.territorySubmittedPercent = ((Decimal)cpfSummaryData.territorySubmitted/(Decimal)cpfSummaryData.territoryCount)*100;
                cpfSummaryData.territoryApprovedPercent = ((Decimal)cpfSummaryData.territoryApproved/(Decimal)cpfSummaryData.territoryCount)*100;

                cpfSummaryData.territorySubmittedPercent = cpfSummaryData.territorySubmittedPercent!=0 ? cpfSummaryData.territorySubmittedPercent.setScale(2) : 0;
                cpfSummaryData.territoryApprovedPercent = cpfSummaryData.territoryApprovedPercent!=0 ? cpfSummaryData.territoryApprovedPercent.setScale(2) : 0;
            }
            System.debug(cpfSummaryData.territorySubmittedPercent);
            System.debug(cpfSummaryData.territoryApprovedPercent);

            Set<String> repUserSet = new Set<String>();
            Set<String> dmUserSet = new Set<String>();
            List<AxtriaSalesIQTM__User_Access_Permission__c> repUserPositions = [Select Id, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__User__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQST__Rep_Start_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Rep_End_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQST__DM_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__DM_End_Date__c From AxtriaSalesIQTM__User_Access_Permission__c Where AxtriaSalesIQST__isCallPlanEnabled__c=true and AxtriaSalesIQTM__Is_Active__c=true and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__User__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c IN ('1','2') and AxtriaSalesIQTM__Team_Instance__c =:cpfTeamToTeamInsMap.get(selectedCpfTeam) WITH SECURITY_ENFORCED];
            Date repStartDate, repEndDate, dmStartDate, dmEndDate;
            for(AxtriaSalesIQTM__User_Access_Permission__c obj:repUserPositions){
                if(obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c == '1'){
                    repUserSet.add(obj.AxtriaSalesIQTM__User__c);
                    repStartDate = obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQST__Rep_Start_Date__c;
                    repEndDate = obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Rep_End_Date__c;
                }else if(obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c == '2'){
                    dmUserSet.add(obj.AxtriaSalesIQTM__User__c);
                    dmStartDate = obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQST__DM_Start_Date__c;
                    dmEndDate = obj.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__DM_End_Date__c;
                }
            }
            System.debug('repUserSet.size ::'+repUserSet.size());
            System.debug('dmUserSet.size :: '+dmUserSet.size());
            System.debug('repStartDate : '+repStartDate + ', repEndDate : '+repEndDate +', dmStartDate : '+dmStartDate+', dmEndDate : '+dmEndDate);

            //List<LoginHistory> loggedInUsers = [SELECT Id,LoginTime,UserId FROM LoginHistory where UserId IN:repUserSet or UserId IN:dmUserSet /*and startDate <= LoginTime <= endDate*/ WITH SECURITY_ENFORCED order by LoginTime desc];
            cpfSummaryData.repLoggedin = 0; cpfSummaryData.dmLoggedin = 0;
            List<User> loggedInUsers = [Select Id,LastLoginDate From User where Id IN: repUserSet or Id IN:dmUserSet WITH SECURITY_ENFORCED];
            System.debug('loggedInUsers :: '+loggedInUsers);
            for(User obj: loggedInUsers){
                if(obj.LastLoginDate != null){
                   if(repUserSet.contains(obj.Id) && obj.LastLoginDate>=repStartDate && obj.LastLoginDate<=repEndDate) {
                        cpfSummaryData.repLoggedin++;
                   }else if(dmUserSet.contains(obj.Id) && obj.LastLoginDate>=dmStartDate && obj.LastLoginDate<=dmEndDate) {
                        cpfSummaryData.dmLoggedin++;
                   }
                }
            }
            System.debug('repLoginCount : '+cpfSummaryData.repLoggedin+', dmLoginCount : '+cpfSummaryData.dmLoggedin);

            cpfSummaryData.repLoggedinPercent = 0; cpfSummaryData.dmLoggedinPercent = 0;
            if(repUserSet!=null & repUserSet.size()>0){
                cpfSummaryData.repLoggedinPercent = ((Decimal)cpfSummaryData.repLoggedin/(Decimal)cpfSummaryData.territoryCount)*100;
                cpfSummaryData.repLoggedinPercent = (cpfSummaryData.repLoggedinPercent).setScale(2);
            }
            if(dmUserSet!=null & dmUserSet.size()>0){
                cpfSummaryData.dmLoggedinPercent = ((Decimal)cpfSummaryData.dmLoggedin/(Decimal)cpfSummaryData.districtCount)*100;
                cpfSummaryData.dmLoggedinPercent = (cpfSummaryData.dmLoggedinPercent).setScale(2);
            }
            
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.cpfSummary()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }

    public class CpfSummaryWrapper {
        public Integer territoryCount {get;set;}
        public Integer repLoggedin {get;set;}
        public Decimal repLoggedinPercent {get;set;}
        public Integer territorySubmitted {get;set;}
        public Decimal territorySubmittedPercent {get;set;}
        public Integer territoryApproved {get;set;}
        public Decimal territoryApprovedPercent {get;set;}

        public Integer districtCount {get;set;}
        public Integer dmLoggedin {get;set;}
        public Decimal dmLoggedinPercent {get;set;}

        public String teamName {get;set;}
    }
}