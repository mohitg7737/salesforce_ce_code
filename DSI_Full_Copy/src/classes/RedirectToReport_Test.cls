@isTest
private class RedirectToReport_Test {
@isTest(SeeAllData=true)
static void testMethod1() {
String className = 'RedirectToReport_Test ';
User loggedInUser = new User(id=UserInfo.getUserId());
Account acc= TestDataFactory.createAccount();
acc.AxtriaSalesIQTM__AccountType__c ='HCO';
acc.AccountNumber = '12345';
acc.AxtriaSalesIQST__Marketing_Code__c = 'USA';
insert acc;
AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
orgmas1.Name = 'SnT';
insert orgmas1;
AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c();
//country.Load_Type__c = 'Full Load';
country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
country.AxtriaSalesIQTM__Status__c = 'Active';
insert country;

 AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
workspace.AxtriaSalesIQTM__Country__c = country.id;
insert workspace;
AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
insert emp;
AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
type.AxtriaSalesIQTM__Country__c = country.Id;
insert type;
AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
zt0.name = 'Y';
zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
insert zt0;
AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
geo.name = 'X';
geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
insert geo;
AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
team.Name = 'ONCO';
team.AxtriaSalesIQTM__Country__c = country.id;
insert team;
AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
teamins.AxtriaSalesIQTM__Geography_Type_Name__c = type.id;
insert teamins;
AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
pos.AxtriaSalesIQTM__Inactive__c = false;
pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
pos.AxtriaSalesIQTM__Employee__c = emp.id;
//pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
//pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
insert pos;
//AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
//insert posAccount;
//Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,country);
//insert pcc;
//AxtriaSalesIQTM__Position_Product__c posProd = TestDataFactory.createPositionProduct(teamins,pos,pcc);
//insert posProd;
AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
posEmp.AxtriaSalesIQTM__Position__c = pos.id;
posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
posEmp.AxtriaSalesIQTM__Status__c = 'Approved';
posEmp.AxtriaSalesIQTM__Effective_start_date__c = date.today();
posEmp.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
insert posEmp;
AxtriaSalesIQTM__User_Access_Permission__c uap = TestDataFactory.createUserAccessPerm(pos,teamins,loggedInUser.id);
insert uap;
Test.startTest();
//System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
//System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
List <Report> reportList = [SELECT Id,DeveloperName,Name FROM Report where name=  'ZIP TERR_5']; 
List <Report> reportList1 = [SELECT Id,DeveloperName,Name FROM Report where name =  'Metric Impact_5']; 
List <Report> reportList2 = [SELECT Id,DeveloperName,Name FROM Report where name =  'Metric Impact_5']; 
List <Report> reportList3 = [SELECT Id,DeveloperName,Name FROM Report where name =  'Metric Impact_4']; 
List <Report> reportList4 = [SELECT Id,DeveloperName,Name FROM Report where name =  'ZIP TERR_1']; 
List <Report> reportList5 = [SELECT Id,DeveloperName,Name FROM Report where name =  'Territory Hierarchy_1'];
List <Report> reportList6 = [SELECT Id,DeveloperName,Name FROM Report where name =  'Metric Impact_1'];
List <Report> reportList7 = [SELECT Id,DeveloperName,Name FROM Report where name =  'ZIP TERR_4'];

system.debug('reportList$$$$ '+reportList );   
system.debug('reportList1$$$$ '+reportList1 ); 
RedirectToReport rt= new RedirectToReport();
rt.buChangedA();
rt.selectARReports();
rt.reportnameA = 'ZIP TERR_5';
rt.openReport();
rt.reportnameA = 'Metric Impact_5';
rt.openReport();
rt.reportnameA = 'Metric Impact_4';
rt.openReport();
rt.reportnameA = 'ZIP TERR_1';
rt.openReport();
rt.reportnameA = 'Territory Hierarchy_1';
rt.openReport();
rt.reportnameA = 'Metric Impact_1';
rt.openReport();
rt.reportnameA = 'ZIP TERR_4';
rt.openReport();

rt.reportname_Roster = 'Position_To_Geography_Mapping';
rt.openReport_Roster();

rt.reportname_Roster = 'Promotion_New_Report';
rt.openReport_Roster();

rt.reportname_Roster = 'RosterGeographyReport';
rt.openReport_Roster();

rt.reportname_Roster = 'Training_New_Report';
rt.openReport_Roster();

rt.reportname_Roster = 'Workbench_Summary_Report';
rt.openReport_Roster();

rt.reportname_Roster = 'Vacant_Geography_Report';
rt.openReport_Roster();
rt.dummy();

//}
Test.stopTest();

}
}