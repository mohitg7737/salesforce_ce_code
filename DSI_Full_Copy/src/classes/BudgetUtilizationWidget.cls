/*************************************************************************
* @author     : A1942
* @date       : June-08-2021
* @description: Controller class for all dashboard custom widgets VF pages
* Revison(s)  : v1.0
*************************************************************************/
global class BudgetUtilizationWidget{

    public boolean dataAvailable {get;set;}
    
    public String userBU {get;set;}
    public String userProfileName {get;set;}

    //Varaibles related to National Performance product widget
    public String selected_NP_Product {get;set;}
    public String publish_NP_Date {get;set;}
    public List<SelectOption> product_NP_List {get;set;}
    public Map<String, List<Map<String, String>>> productMetric_NP_Map;

    //public String m_p_monthlyDataJson {get;set;} // Metric performance monthly dataset

    public Map<String, List<Map<String, String>>> dataMap;


    public BudgetUtilizationWidget(){

        System.debug('BudgetUtilizationWidget constructor called ...........');
        dataAvailable = true;

        selectedBudgetTeam = '';
        budgetPublishDate = '';
        budgetTeamValueMap = new Map<String, String>();
        budgetTeamPercentMap = new Map<String, String>();
        budgetTeamPublishMap = new Map<String, String>();

        selected_NP_Product = '';
       
        publish_NP_Date = '';
        product_NP_List = new List<SelectOption>();
        productMetric_NP_Map = new Map<String, List<Map<String, String>>>();

        
        
        dataMap = new Map<String, List<Map<String, String>>>();

        try{
           
            List<User> loggedInUser = [Select Id, profileId,SIQIC__Business_Unit__c from User where Id =: UserInfo.getUserId() and IsActive=true WITH SECURITY_ENFORCED];
            List<Profile> userProfile = [Select Name from profile where id =:loggedInUser[0].profileId ];
            userBU = loggedInUser[0].SIQIC__Business_Unit__c;
            System.debug('loggedInUser :: '+loggedInUser + '-- bu- '+ userBU);
            System.debug('userProfile :: '+userProfile);

            //String userProfileName = '';
            if(userProfile!=null && userProfile.size()>0 && String.isNotBlank(userProfile[0].Name)){
                userProfileName = userProfile[0].Name;
            }
            System.debug('userProfileName :: '+userProfileName);
            
            List<SIQIC__Reports__c> reportRecord = new List<SIQIC__Reports__c>();

            if(userProfileName.containsIgnoreCase('HO')){
                reportRecord = [Select Id, SIQIC__Report_Definition__c, SIQIC__Role__c, SIQIC__User__c, SIQIC__EmpId__c From SIQIC__Reports__c Where SIQIC__Report_Type__c ='Budget_Utilization' and SIQIC__LatestHO__c = true WITH SECURITY_ENFORCED];
            }
            System.debug('reportRecord :: '+reportRecord);

            if(reportRecord!=null && reportRecord.size()>0){
                String reportId = '', empId = '', filterStr= '';
                reportId = String.isBlank(reportRecord[0].SIQIC__Report_Definition__c) ? '' : reportRecord[0].SIQIC__Report_Definition__c;
                empId = String.isBlank(reportRecord[0].SIQIC__EmpId__c) ? '' : reportRecord[0].SIQIC__EmpId__c;
                System.debug('reportId : '+reportId+', empId : '+empId);
                if(String.isNotBlank(reportId)){
                    
                    if(userProfileName.containsIgnoreCase('HO')){
                        dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', reportId+';;Budget_Utilization'); // 2 is here type of server which is reporting server.
                    }else if(String.isNotBlank(empId)){
                        filterStr = reportId +';'+ empId;
                        dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', filterStr); // 2 is here type of server which is reporting server.
                    }
                }
            }
            
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', 'a335e000000HyMFAA0;10000070'); // TO DO remove this line after Data testing
            //dataMap = DSI_Utility.getProcResponse(2, 'getDashboardWidgetData', '2;11111');
            System.debug('dataMap :: '+dataMap);

            if(dataMap != null && dataMap.size()>0){

                budgetUtilization();
                
                
            }else {
                dataAvailable = false;
            }
            

        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }        
    }

    public Boolean isBudget {get;set;}
    public String budgetPublishDate {get;set;}
    public String selectedBudgetTeam {get;set;}
    public String budgetUtilizationPercent {get;set;}
    public String budgetUtilizationPayout {get;set;}
    public List<SelectOption> budgetTeams {get;set;} 
    public Map<String, String> budgetTeamValueMap {get;set;}
    public Map<String, String> budgetTeamPercentMap {get;set;}
    public Map<String, String> budgetTeamPublishMap {get;set;}
    global void budgetUtilization(){

        System.debug('selectedBudgetTeam :: '+selectedBudgetTeam);
        budgetTeams = new List<SelectOption>();
        isBudget = false;
        try{
            if(String.isBlank(selectedBudgetTeam)){
                List<Map<String, String>> records = new List<Map<String, String>>();
                if(dataMap.get('DS_0')!=null){
                    records.addAll(dataMap.get('DS_0'));
                }
                System.debug('budget : '+records);
                for(Map<String, String> mapObj: records){
                    selectedBudgetTeam = String.isBlank(selectedBudgetTeam) ? mapObj.get('Team') : selectedBudgetTeam;
                    budgetUtilizationPercent = String.isBlank(budgetUtilizationPercent) ? mapObj.get('Budget_Utilization') : budgetUtilizationPercent;
                    budgetUtilizationPayout = String.isBlank(budgetUtilizationPayout) ? mapObj.get('Total_Payout') : budgetUtilizationPayout;
                    
                    budgetTeams.add(new SelectOption(mapObj.get('Team'), mapObj.get('Team')));
                    budgetTeamPercentMap.put(mapObj.get('Team'), mapObj.get('Budget_Utilization'));
                    budgetTeamValueMap.put(mapObj.get('Team'), mapObj.get('Total_Payout'));
                    budgetTeamPublishMap.put(mapObj.get('Team'), mapObj.get('Publish_Date'));
                }
                System.debug('budgetTeams :: '+budgetTeams);
                System.debug('budgetTeamValueMap :: '+budgetTeamValueMap);
            }
            budgetUtilizationPercent = budgetTeamPercentMap.get(selectedBudgetTeam);
            budgetUtilizationPercent = String.valueOf(Decimal.valueOf(budgetUtilizationPercent) * 100);
            budgetUtilizationPercent = String.valueOf(Decimal.valueOf(budgetUtilizationPercent).setScale(2));
            budgetUtilizationPayout =  budgetTeamValueMap.get(selectedBudgetTeam);
            budgetPublishDate = budgetTeamPublishMap.get(selectedBudgetTeam);
            Date dt = Date.valueOf( budgetPublishDate ); 
            budgetPublishDate = DateTime.newInstance(dt.year(),dt.month(), dt.day() ).format('MM-dd-yyyy');
            System.debug('budgetUtilizationPercent :: '+budgetUtilizationPercent+', budgetUtilizationPayout : '+budgetUtilizationPayout);

            if(String.isNotBlank(budgetUtilizationPercent)){
                isBudget = true;
            }
        }catch(Exception e){
            System.debug('Exception in DashboardCustomWidgetsCtrl.budgetUtilization()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
        }
    }
    public void dummy(){
        Integer count = 1;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;

        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        count++;
        }

    

  
}