public with sharing class PosEmployeeViewController{ 
      
    public List<AxtriaSalesIQTM__Position_Employee__c> posAssignment{get;set;}     
    public PosEmployeeViewController(ApexPages.StandardController controller){                
        AxtriaSalesIQTM__Employee__c posId = (AxtriaSalesIQTM__Employee__c)controller.getRecord();
        String emailAddress = [Select DSI_Email_Id__c from AxtriaSalesIQTM__Employee__c where Id=: posId.Id].DSI_Email_Id__c;
        //String employeeType = [Select AxtriaSalesIQTM__Employee_Type__c from AxtriaSalesIQTM__Employee__c where Id=: posId.Id].AxtriaSalesIQTM__Employee_Type__c;
        //System.debug('employeeType>>>>>>'+employeeType);
        //String employeeNumber = [Select AxtriaSalesIQTM__Employee_ID__c from AxtriaSalesIQTM__Employee__c where Id=: posId.Id].AxtriaSalesIQTM__Employee_ID__c;
        //System.debug('employeeNumber >>>>>>'+employeeNumber );        
        if(String.isNotBlank(emailAddress)){
            posAssignment = [SELECT Id, AxtriaSalesIQTM__Position__c, 
                              AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,
                              AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                              AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Assignment_Type__c,
                              AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQST__Team_Name__c, AxtriaSalesIQTM__Position__r.Name,
                              AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_Type__c,
                              AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.name,
                              AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.Position_Name__c
                              FROM AxtriaSalesIQTM__Position_Employee__c 
                              WHERE AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c =: emailAddress ORDER BY AxtriaSalesIQTM__Effective_Start_Date__c DESC, AxtriaSalesIQTM__Assignment_Type__c ASC];
                              /*and AxtriaSalesIQTM__Employee__c != null and AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_Type__c !=: employeeType 
                              and  AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c !=: employeeNumber) OR (AxtriaSalesIQTM__Employee__c =: posId.Id)*/                            
        }
    } 
 
}