public class PBI_WithWebQuery_Ctlr
{
    public Map<String,Object> results;       //removed getset
    public String application_name;
    public String accessToken {get;set;}
    public String embedToken ;
    public String client_id;
    public String resource_URI ;
    public String access_token_url;
    public String username ;
    public String password ;
    public String PBI_GroupId {get;set;} //group id of the report
    public String PBI_ReportID {get;set;} //report id of the report
    public String PBI_PageId {get;set;} //page id of the report
    public String PBI_Param {get;set;} //Parameter of the report  added feb
    public user U;
    public SIQIC__Reports__c R ;
    public String reportListLink;
    public String gobacklink{get;set;}
    public String UserEmpID {get;set;}
    public String UserGeoID {get;set;} 
    public String BusinessUnit {get;set;}
    public String UserRole {get;set;}
    public String Report {get;set;} 
    public String Version {get;set;}   
    public String Param1 {get;set;}  
    public String Param2 {get;set;}  
    public String Param3 {get;set;} 
    public String ReportType;  //ReportType
        
    public Map<String,Object> SCHeaderDataList;    //removed getset
    public SIQIC__Reports__c report1 {get;set;}  

    public User loggedInUser;
    public String URL ;
    public PBI_WithWebQuery_Ctlr()
    {
        
        String reportId = apexpages.currentpage().getparameters().get('Id');
        
        if(reportId!=null)
        {
            report1=reportUtility.fetchReportMetadata(reportId);
        }
        else
        {
            UserEmpID='50000101';
            BusinessUnit='NS';
            UserGeoID='T0104';
            Report='a363h0000005Jp1AAE';
            version='V2';
            UserRole='Rep';
        }
                
        loggedInUser = [select id,name,SIQIC__Employee_ID__c,SIQIC__Geo_Id__c,UserRole.Name, SIQIC__TenantId__c,SIQIC__Tenant__c,LocaleSidKey from User where ID =:Userinfo.getuserId() limit 1];
        
        if(report1!=null)
        {
            UserGeoID = report1.SIQIC__Geo_Id__c;
            Report  = report1.SIQIC__publish__r.SIQIC__Report_Data_Push__c;
            BusinessUnit= report1.SIQIC__Business_Unit__c;
            UserEmpID = report1.SIQIC__EmpId__c;
            UserRole    = report1.SIQIC__Role__c;
            Version = report1.SIQIC__version__c;  
            ReportType=report1.SIQIC__Report_Type__c; //ReportType
            
            PBI_GroupId = report1.SIQIC__Path3__c.replace('/public/','');       
            PBI_ReportID = report1.SIQIC__path4__c.replace('/public/',''); 
            PBI_PageID = report1.SIQIC__path2__c.replace('/public/',''); 
            PBI_Param = report1.SIQIC__path5__c.replace('/public/','');     //added feb
            System.debug(report1.SIQIC__path4__c.replace('/public/',''));
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Access Denied'));        
        }
            
        if(!Test.isRunningTest())
        {
            reportListLink = '/apex/siqic__ViewReports';
            gobacklink = reportListLink;
            //reportListLink = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/ViewReports';
            //gobacklink = reportListLink.replace('--c','--siqic');
            System.debug(reportListLink);
        }
    
        application_name= OAuthApp_pbi__c.getall().values()[0].Name;
        System.debug(application_name);
        client_id = OAuthApp_pbi__c.getValues(application_name).Client_Id__c;
        resource_URI = OAuthApp_pbi__c.getValues(application_name).Resource_URI__c;
        access_token_url = OAuthApp_pbi__c.getValues(application_name).Access_Token_URL__c;
        username = OAuthApp_pbi__c.getValues(application_name).Username__c;
        password = OAuthApp_pbi__c.getValues(application_name).Password__c;
        
        
        GetDataFromWebQuery();
        getAccessToken();
    }    
        
    public void GetDataFromWebQuery()
    {
        List<Object> summaryDataMap=new List<Object>();
        String procName='PBI_Takeda_Proc(-,-,-,-,-,-,-)';
        procName = EncodingUtil.urlEncode(procName, 'UTF-8');
        String parameters;
        
        parameters=UserEmpID +','+UserGeoID+','+BusinessUnit+','+UserRole+','+Report+','+Version+','+ReportType;
        system.debug(parameters);
        parameters = EncodingUtil.urlEncode(parameters, 'UTF-8');
        String endPoint=URL+ procName+'/'+parameters;
        System.Debug('EndPoint ===== ' + endPoint);
        
        results=reportUtility.restCallOut(endPoint);
        System.debug('results+================+ ' + results);
                      
        summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('1')));
        if(summaryDataMap.size()>0)
        {
            SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
            System.debug(SCHeaderDataList.get('Param1'));                          //Slicercount
            Param1=String.valueOf(SCHeaderDataList.get('Param1'));
          
        }
        else
        {
            Param1='1';  
        }
        
        summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('2')));
        if(summaryDataMap.size()>0)
        {
            SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
            System.debug(SCHeaderDataList.get('Param2'));                          //Slicercount
            Param2=String.valueOf(SCHeaderDataList.get('Param2'));  
        }
        else
        {
            Param2='1';
        }
        
        summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('3')));
        if(summaryDataMap.size()>0)
        {       
            SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
            System.debug(SCHeaderDataList.get('Param3'));                          //Slicercount
            Param3=String.valueOf(SCHeaderDataList.get('Param3'));          
        }
        else
        {
            Param3='1';  
        }
    }   
        
    public void getAccessToken()
    {
        accessToken = '';
        try{
            List<String> urlParams = new List<String> {
                'grant_type=password',
                'scope=' + EncodingUtil.urlEncode('openid', 'UTF-8'),
                'username=' + EncodingUtil.urlEncode(username, 'UTF-8'),
                'password=' + EncodingUtil.urlEncode(password, 'UTF-8'),
                'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
                'resource=' + EncodingUtil.urlEncode(resource_URI, 'UTF-8')
            };
            system.debug(urlParams);
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(access_token_url);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            String body = String.join(urlParams, '&');
            req.setBody(body);
            HttpResponse res = h.send(req);
            System.debug('response-->'+res.getBody());
            Map<String,String> newMap = (Map<String,String>)JSON.deserialize(res.getBody(), Map<String,String>.class);
            System.debug(newMap);
            accessToken = newMap.get('access_token');
            }
            catch(Exception e)
            {
                System.debug('Error Message-->'+e.getMessage());
            }
    }
    
    public void trackReportInst()
    {
        Id userId = UserInfo.getUserId();
        Id reportId = ApexPages.currentPage().getParameters().get('id');
        SIQIC__Reports__c inst = [select OwnerId,SIQIC__Report_Type__c,SIQIC__Report_Period__c,LastViewedDate from SIQIC__Reports__c where id = :reportId limit 1];
        reportUtility.createReportTrackingRecord(userId,reportId,inst.SIQIC__Report_Type__c,inst.OwnerId,inst.SIQIC__Report_Period__c);       
    }
}