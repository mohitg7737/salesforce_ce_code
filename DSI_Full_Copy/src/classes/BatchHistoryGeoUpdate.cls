global class BatchHistoryGeoUpdate implements Database.Batchable<sObject> {
    public String query;
    public AxtriaSalesIQST__Scheduler_Log__c scLog;
    public Map<String, AxtriaSalesIQTM__Position_Employee__c> posEmpMap;

    global BatchHistoryGeoUpdate(AxtriaSalesIQST__Scheduler_Log__c scLog1) {
        this.query = 'Select id, Employee__r.Name, Employee__r.AxtriaSalesIQTM__Current_Territory__c, Position__c,Client_Territory_Code__c from Employee_History__c';// where LatestHistory__c = true';
     
     if(sclog1!=null){
            scLog = scLog1;
        }
        else{
            scLog = new AxtriaSalesIQST__Scheduler_Log__c();
            scLog.AxtriaSalesIQST__Created_Date__c = System.today();
            scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
            scLog.AxtriaSalesIQST__Job_Type__c = 'Operations';
            scLog.AxtriaSalesIQST__Job_Name__c = 'History Position Update Batch';
            scLog.AxtriaSalesIQST__Job_Status__c = 'Job Initiated';
            insert scLog;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Employee_History__c > scope) {
        List<Employee_History__c > EmpHisExtractList = new List<Employee_History__c >();
        List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select id, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.SalesforceCode__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__Territory_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.Sales_Force_Code__c, AxtriaSalesIQTM__Position__r.Sales_Force__c, AxtriaSalesIQTM__Employee__r.Termination_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.Payroll_Id__c, AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c, AxtriaSalesIQTM__Employee__r.Personnel_Area__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Current_Territory__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Number__c, AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c, AxtriaSalesIQTM__Employee__r.Reason_Code__c, AxtriaSalesIQTM__Employee__r.Position_Title__c, AxtriaSalesIQTM__Position__r.SalesforceName__c,  AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c,AxtriaSalesIQTM__Position__r.Position_Name__c from AxtriaSalesIQTM__Position_Employee__c];
        posEmpMap = new Map<String,AxtriaSalesIQTM__Position_Employee__c> ();
        for (AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
            
            //system.debug('###########PosEmpList Insisde for & Before If ' +posEmp); 
            if (posEmpMap.isEmpty()){
                //system.debug('###########PosEmpList After If ' +posEmp);
                posEmpMap.put(posEmp.AxtriaSalesIQTM__Employee__c+'_'+posEmp.AxtriaSalesIQST__Territory_ID__c,posEmp);
            }
            else {
                if (!posEmpMap.containsKey(posEmp.AxtriaSalesIQTM__Employee__c+'_'+posEmp.AxtriaSalesIQST__Territory_ID__c))
                {
                    //system.debug('###########PosEmpList After Else ' +posEmp);
                    posEmpMap.put(posEmp.AxtriaSalesIQTM__Employee__c+'_'+posEmp.AxtriaSalesIQST__Territory_ID__c,posEmp);
                }
            }

        }
        //system.debug('###########PosEmpMap ' +posEmpMap);
        for(Employee_History__c empHis: scope){
                if(posEmpMap.containsKey(empHis.Employee__c+'_'+empHis.Client_Territory_Code__c)){
                    empHis.Position__c = (posEmpMap.get(empHis.Employee__c+'_'+empHis.Client_Territory_Code__c)).AxtriaSalesIQTM__Position__c; 
                    EmpHisExtractList.add(empHis);
            }
        }
        
    if(EmpHisExtractList.size()>0){
            //system.debug('###########EmpHisExtractList ' +EmpHisExtractList);
            update EmpHisExtractList ;
    }
}
    global void finish(Database.BatchableContext BC) {
    
        try{
            scLog.AxtriaSalesIQST__Job_Status__c = 'Successful';
            update scLog;                     
        }catch(Exception exc){
            scLog.AxtriaSalesIQST__Job_Status__c = 'Error';
            update scLog;
        }

    }
}