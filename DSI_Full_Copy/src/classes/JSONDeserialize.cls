public class JSONDeserialize {
    public OperationsWrapper wrapper {
        get;
        set;
    }

    public void deserialize() {
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint('https://dsi-arc.axtria.com/dsi/employeeStatus');
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(120000);

        HttpResponse response = h.send(request);

        wrapper = (OperationsWrapper) JSON.deserializeStrict(response.getBody(), OperationsWrapper.class);
    }
}