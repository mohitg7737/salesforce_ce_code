@isTest(seeALLData = false)

public class QuickLinkCtrlCustomTest {
    static testMethod void QuickLinkCtrlCustomTestMethod1() {
          User user1;
        SIQIC__R_Calender__c tenantcalender = new SIQIC__R_Calender__c();
        tenantcalender = TestUtilCustom.createTestCalender(true,'Calender1',null,true,null);
        Account Tenant=TestUtilCustom.createTestTenant(true,'US',tenantcalender.Id);

        Profile profile = [select id from profile where name = 'System Administrator'];
        Account acc = new account(name = 'Tenant001', SIQIC__Abbreviated_Name__c = 'US', AxtriaSalesIQST__Marketing_Code__c = 'US');
        insert acc;
        SIQIC__Country__c cau = new SIQIC__Country__c(name = 'US', SIQIC__tenant__c = acc.id);
         User currentuser= [Select id from user where id=:userinfo.getuserid()];
         System.runAs(currentuser){
             
            Profile pid1=[Select ID from Profile where Name='System Administrator' Limit 1];
            user1=new User(Username='abokabmnababcbaba@mail.com', LastName='wayne', Email='abcbaba@mail.com', Alias='wayne', CommunityNickname='mnabokabmnbwayne',isActive=true,
                        TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US' , 
                        EmailEncodingKey='UTF-8',ProfileID=pid1.id, LanguageLocaleKey='en_US',SIQIC__Geo_Id__c='PFA09P',SIQIC__Country__c='US',SIQIC__TenantId__c=Tenant.ID,SIQIC__Tenant__c=Tenant.name );
            insert user1;
             PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'IC_HO'];
             insert new PermissionSetAssignment(AssigneeId = user1.id, PermissionSetId = ps.Id);
             System.assertEquals(user1.ProfileId, pid1.id);
        }
       

        Id RecordTypeId = Schema.SObjectType.SIQIC__View_Data_Mapping__c.getRecordTypeInfosByName().get('QuickLink').getRecordTypeId();
        SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('RecordTypeId====' + RecordTypeId );

        string namespace = 'SIQIC__';
        string strObjectName = namespace + 'view_data_mapping__c';


        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = :strObjectName and name = 'QuickLinkSelf'];
        Schema.DescribeSObjectResult d = Schema.SObjectType.SIQIC__view_data_mapping__c;
        Map<Id, Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Schema.RecordTypeInfo rtById =  rtMapById.get(rt.id);
        Map<String, Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get(rt.name);


        system.runAs(user1) {


            try{
                 SIQIC__View_Data_Mapping__c viewdata = new SIQIC__View_Data_Mapping__c(Name = 'System Administrator', SIQIC__DefaultFilterNew__c = 'SIQIC__Visible_To_Field__c=True', SIQIC__DisplayColumns__c = 'SIQIC__Report_name__c', SIQIC__Fieldapi__c = namespace + 'Report_name__c,' + namespace + 'URL__c,id,' + namespace + 'View_Link__c', SIQIC__Quicklink__c = 'SIQIC__View_Link__c', SIQIC__Tenant__c = acc.id, SIQIC__Object_API__c = 'SIQIC__Reports__c', recordtypeid = RecordTypeId );
            insert viewdata;

            SIQIC.SalesIqDmlSecurityUtil.printDebugMessage(' view data id ' + viewdata.SIQIC__Tenant__c);



            System.test.startTest();
            String query = 'select SIQIC__report_name__c,id from SIQIC__reports__c where (SIQIC__Visible_To_HO__c=True OR SIQIC__Visible_to_field__c=TRUE) ';
            QuickLinkCtrlCustom qlc = new QuickLinkCtrlCustom();

            QuickLinkCtrlCustom.Wrapper wr = new QuickLinkCtrlCustom.Wrapper('test', viewdata.id, 'test');
            qlc.queryMethod(query);
            qlc.dummyOne();
            qlc.linkurl = 'SIQIC__View_link__c';
            system.assertequals(qlc.vdMapping.size(),0);
            System.Test.stoptest();
            }
            catch(Exception ex){
                
            }

           
        }
    }

}