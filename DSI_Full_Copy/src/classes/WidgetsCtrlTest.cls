@isTest (SeeAllData=true)
private class WidgetsCtrlTest {
    static testMethod void testMethod1() {
    Profile pid1=[Select Id from Profile where Name='HO' Limit 1];  
        Profile pid2=[Select Id from Profile where Name='Rep' Limit 1];
        User u1 = [Select id from user where ProfileID =:pid1.id Limit 1];
        /*new User(Username='testusery1@axtria.com', LastName='test1', Email='testuse1r@axtria.com', Alias='test1', CommunityNickname='test1',TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',EmailEncodingKey='UTF-8',ProfileID=pid1.id, LanguageLocaleKey='en_US',SIQIC__Tenant__c='DSI',SIQIC__TenantID__c='0015e000004fcvsAAA',IsActive=true);
        insert u1;*/
        User u2 = new User(Username='testusery2@axtria.com', LastName='test2', Email='testuser2@axtria.com', Alias='test2', CommunityNickname='test2',
                    TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', 
                    EmailEncodingKey='UTF-8',ProfileID=pid2.id, LanguageLocaleKey='en_US');
        insert u2;
        	

        SIQIC__Reports__c r1 = new SIQIC__Reports__c();
        r1.SIQIC__Report_Definition__c = [select id from SIQIC__Report_Defination__c limit 1].id; 
        r1.SIQIC__Role__c ='HO';
        r1.SIQIC__User__c = u1.id;
        r1.SIQIC__Report_Type__c = 'Dashboard';
        r1.SIQIC__Latest__c =true;
        r1.SIQIC__LatestHO__c = true;
        r1.Dashboard_Enabled__c = true;
        insert r1;
         SIQIC__Reports__c r2 = new SIQIC__Reports__c();
        r2.SIQIC__Report_Definition__c =  [select id from SIQIC__Report_Defination__c limit 1].id;
                r2.SIQIC__Role__c ='HO';
                r2.SIQIC__User__c = UserInfo.getUserId();
                r2.SIQIC__Report_Type__c = 'Dashboard';
        		r2.SIQIC__LatestHO__c = true;
                r2.SIQIC__Latest__c =true;
                r2.SIQIC__EmpId__c='10000070';
        r2.Dashboard_Enabled__c = true;
                insert r2;
                
        DashboardVacantGeoTeams__c df1= new DashboardVacantGeoTeams__c();
        df1.Name = 'OCE';
        df1.TeamName__c = 'OCE';
        insert df1;
        
        
         AxtriaSalesIQTM__Employee__c e1 =    new AxtriaSalesIQTM__Employee__c(Name='Emp1',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='1',Action_Start_Date__c=Date.today());    
         insert e1;
         AxtriaSalesIQTM__Employee__c e2 =    new AxtriaSalesIQTM__Employee__c(Name='Emp2',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='2',Action_Start_Date__c=Date.today());    
         insert e2;
         AxtriaSalesIQTM__Employee__c e3 =    new AxtriaSalesIQTM__Employee__c(Name='Emp3',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='3',Action_Start_Date__c=Date.today());    
         insert e3;
         AxtriaSalesIQTM__Employee__c e4 =    new AxtriaSalesIQTM__Employee__c(Name='Emp4',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='4',Action_Start_Date__c=Date.today());    
         insert e4;
         AxtriaSalesIQTM__Employee__c e5 =    new AxtriaSalesIQTM__Employee__c(Name='Emp5',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='5',Action_Start_Date__c=Date.today());    
         insert e5;
         AxtriaSalesIQTM__Employee__c e6 =    new AxtriaSalesIQTM__Employee__c(Name='Emp6',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='6',Action_Start_Date__c=Date.today());    
         insert e6;
         AxtriaSalesIQTM__Employee__c e7 =    new AxtriaSalesIQTM__Employee__c(Name='Emp7',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='7',Action_Start_Date__c=Date.today());    
         insert e7;
         AxtriaSalesIQTM__Employee__c e8 =    new AxtriaSalesIQTM__Employee__c(Name='Emp8',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='8',Action_Start_Date__c=Date.today());    
         insert e8;
         AxtriaSalesIQST__CR_Employee_Feed__c feed1 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed1.AxtriaSalesIQST__Event_Name__c = 'Employee New Hire';
         feed1.AxtriaSalesIQST__Employee__c= e1.id;
         //feed1.Event_Start_Date__c = Date.today();
         insert feed1;
          AxtriaSalesIQST__CR_Employee_Feed__c feed2 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed2.AxtriaSalesIQST__Event_Name__c = 'Transfer';
         feed2.AxtriaSalesIQST__Employee__c= e2.id;
        // feed2.Event_Start_Date__c = Date.today();
         insert feed2;
          AxtriaSalesIQST__CR_Employee_Feed__c feed3 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed3.AxtriaSalesIQST__Event_Name__c = 'Promotion';
         feed3.AxtriaSalesIQST__Employee__c= e3.id;
        // feed3.Event_Start_Date__c = Date.today();
         insert feed3;
          AxtriaSalesIQST__CR_Employee_Feed__c feed4 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed4.AxtriaSalesIQST__Event_Name__c = 'Terminate Employee';
         feed4.AxtriaSalesIQST__Employee__c= e4.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed4;
        AxtriaSalesIQST__CR_Employee_Feed__c feed5 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed5.AxtriaSalesIQST__Event_Name__c = 'Leave of Absence';
         feed5.AxtriaSalesIQST__Employee__c= e5.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed5;
          AxtriaSalesIQST__CR_Employee_Feed__c feed6 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed6.AxtriaSalesIQST__Event_Name__c = 'Return from LOA';
         feed6.AxtriaSalesIQST__Employee__c= e6.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed6;
         AxtriaSalesIQST__CR_Employee_Feed__c feed7 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed7.AxtriaSalesIQST__Event_Name__c = 'Demotion';
         feed7.AxtriaSalesIQST__Employee__c= e7.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed7;
         AxtriaSalesIQST__CR_Employee_Feed__c feed8 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed8.AxtriaSalesIQST__Event_Name__c = 'Re-Hire';
         feed8.AxtriaSalesIQST__Employee__c= e8.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed8;
           DashboardCustomWidgetsCtrl d = new DashboardCustomWidgetsCtrl();
           d.workbenchEvent();
           
            SIQIC__Reports__c report = new SIQIC__Reports__c(SIQIC__Business_Unit__c='All', SIQIC__Role__c='All',  name = 'GoldCup', 
                                        SIQIC__Report_Type__c='GoldCup',SIQIC__User__c = u2.id );
            insert report; 
           
           System.runas(u2){
            
           MetricPerformanceWidget obj1 = new MetricPerformanceWidget();
           GoldCupWidget obj2 = new GoldCupWidget();
           QTDPayoutWidget obj3 = new QTDPayoutWidget();
           budgetUtilizationWidget obj4 = new budgetUtilizationWidget();
           nationalPerformanceWidget obj5 = new nationalPerformanceWidget();
           
           
           obj1.metricPerformance(); 
           obj2.GoldCup(); 
           obj3.QTD_Payout(); 
           obj4.budgetUtilization(); 
           obj4.dummy();
           obj5.nationalPerformance();
           obj5.dummy();
           }
           
           DSI_Utility.getProcResponse(2,'' , 'a335e000000Hj1qAAC;10000070');
            DSI_Utility du = new DSI_Utility();
            du.dummy();
           //du.getProcResponse('', 'getDashboardWidgetData', 'a335e000000HwNCAA0;10000070');
           
       /* system.runAs(u1)
    { DashboardCustomWidgetsCtrl d = new DashboardCustomWidgetsCtrl(); }*/
  }
      static testMethod void testMethod2(){
                  Set<Id> teamInstanceSet = new Set<Id>();
                AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='ONCSC2',AxtriaSalesIQTM__Type__c='Base');
        insert team;

                 
        DashboardVacantGeoTeams__c df1= new DashboardVacantGeoTeams__c();
        df1.Name = 'ONCSC2';
        df1.TeamName__c = 'ONCSC2';
        insert df1;
        
        DashboardPositionType__c df2= new DashboardPositionType__c();
        df2.Name= '1';
        df2.Position_Type__c = 'Territory';
        insert df2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        teamInstance.Name = 'HTN_Q1_2016';
        teamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'HTN_Q1_2016';
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        insert teamInstance;


        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Long Island East, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long Island East, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos.AxtriaSalesIQTM__Related_Position_Type__c=team.AxtriaSalesIQTM__Type__c;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c='2';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
        pos.AxtriaSalesIQTM__Manage_Vacant__c = false;
        
        insert pos;

        AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
        pos1.name = 'Long  East, NY';
        pos1.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long  East, NY';
        pos1.AxtriaSalesIQTM__Client_position_Code__c = '1NE30003';
        pos1.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30003';
        pos1.AxtriaSalesIQTM__position_Type__c='Territory';
        pos1.AxtriaSalesIQTM__Related_position_Type__c= team.AxtriaSalesIQTM__Type__c;
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c='1';
        pos1.AxtriaSalesIQTM__inactive__c = false;
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos1.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos1.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos1.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos1.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos1.AxtriaSalesIQTM__IsMaster__c = true;
        pos1.AxtriaSalesIQST__Call_Plan_Status__c = 'Submitted';
        pos1.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
        pos1.AxtriaSalesIQTM__Manage_Vacant__c = false;

        insert pos1;

        AxtriaSalesIQTM__Position__c pos2= new AxtriaSalesIQTM__Position__c();
        pos2.name = 'Long Island East, NY';
        pos2.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long Island East, NY';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos2.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos2.AxtriaSalesIQTM__Related_Position_Type__c=team.AxtriaSalesIQTM__Type__c;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c='1';
        pos2.AxtriaSalesIQTM__inactive__c = false;
        pos2.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos2.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos2.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos2.AxtriaSalesIQTM__IsMaster__c = true;
        pos2.AxtriaSalesIQST__Call_Plan_Status__c = 'Approved';
        
        insert pos2;

        teamInstanceSet.add(teamInstance.id);
        
        Profile p1 = new Profile(name = 'HO_Testing');
        Profile p2 = new Profile(name = 'HO');

        User user1 = new User();
        user1.IsActive = true;
        user1.id= userinfo.getuserid();

        User user2 = new User();
        user2.IsActive = true;
        user2.id= userinfo.getuserid();
        
        Profile pid1=[Select Id from Profile where Name='HO' Limit 1];
        User u3 = [Select id from user where ProfileID =:pid1.id and IsActive = true Limit 1];

        AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap.AxtriaSalesIQTM__Is_Active__c = true;
        uap.AxtriaSalesIQTM__Position__c = pos.id;
        uap.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap.AxtriaSalesIQTM__User__c = user1.Id;
        uap.AxtriaSalesIQTM__Sharing_Type__c = 'Explicit';
        uap.AxtriaSalesIQST__isCallPlanEnabled__c = true;
        insert uap;

        AxtriaSalesIQTM__User_Access_Permission__c uap1 = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap1.AxtriaSalesIQTM__Is_Active__c = false;
        uap1.AxtriaSalesIQTM__Position__c = pos.id;
        uap1.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap1.AxtriaSalesIQTM__User__c = user1.Id;
        uap1.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
        uap.AxtriaSalesIQST__isCallPlanEnabled__c = true;
        insert uap1;

        AxtriaSalesIQTM__User_Access_Permission__c uap2 = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap2.AxtriaSalesIQTM__Is_Active__c = false;
        uap2.AxtriaSalesIQTM__Position__c = pos.id;
        uap2.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap2.AxtriaSalesIQTM__User__c = user2.Id;
        uap2.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
        uap.AxtriaSalesIQST__isCallPlanEnabled__c = true;
        insert uap2;
        DashboardCustomWidgetsCtrl d1 = new DashboardCustomWidgetsCtrl();
        d1.cpfSummary();
        DashboardCustomWidgetsCtrl d2 = new DashboardCustomWidgetsCtrl();
        d2.vacantGeos();
        
            SIQIC__Reports__c report1 = new SIQIC__Reports__c(name = 'GoldCup Report',SIQIC__Report_Type__c='GoldCup',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__Latest__c = true , SIQIC__User__c=user2.id);
                insert report1;
                
                SIQIC__Reports__c report2 = new SIQIC__Reports__c(name = 'QTD_Payout Report',SIQIC__Report_Type__c='QTD_Payout',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__Latest__c = true , SIQIC__User__c=user2.id);
                insert report2;
                
                SIQIC__Report_Defination__c reportDef = new SIQIC__Report_Defination__c(Name = 'Sales_Goals');
                insert reportDef;
                SIQIC__Reports__c report3 = new SIQIC__Reports__c(name = 'Sales_Goals Report',SIQIC__Report_Type__c='Sales_Goals',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__Latest__c = true , SIQIC__User__c=user2.id,SIQIC__Report_Definition__c = reportDef.id,SIQIC__EmpId__c='12345678');
                insert report3;
                
                SIQIC__Report_Defination__c reportDef1 = new SIQIC__Report_Defination__c(Name = 'National_Performance');
                insert reportDef1;
                
                SIQIC__Reports__c report5 = new SIQIC__Reports__c(name = 'National_Performance Report',SIQIC__Report_Type__c='National_Performance',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__Latest__c = true ,SIQIC__LatestHO__c= true, SIQIC__User__c='0055e0000054ZWAAA2',SIQIC__Report_Definition__c = reportDef1.id,SIQIC__EmpId__c='123456789');
                insert report5;
          		SIQIC__Reports__c report7 = new SIQIC__Reports__c(name = 'National_Performance Report',SIQIC__Report_Type__c='National_Performance',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__Latest__c = true ,SIQIC__LatestHO__c= true, SIQIC__User__c=u3.id,SIQIC__Report_Definition__c = reportDef1.id,SIQIC__EmpId__c='123456789');
                insert report7;
                
                SIQIC__Report_Defination__c reportDef2 = new SIQIC__Report_Defination__c(Name = 'Budget_Utilization');
                insert reportDef2;
                
                SIQIC__Reports__c report4 = new SIQIC__Reports__c(name = 'Budget_Utilization Report',SIQIC__Report_Type__c='Budget_Utilization',
                                           SIQIC__path4__c='/public/abc',SIQIC__path3__c='/public/abc',SIQIC__LatestHO__c = true , SIQIC__User__c=u3.id,SIQIC__Report_Definition__c = reportDef2.id,SIQIC__EmpId__c='123456789');
                insert report4;
                
           system.runas(user2){
           MetricPerformanceWidget obj1 = new MetricPerformanceWidget();
           GoldCupWidget obj2 = new GoldCupWidget();
           QTDPayoutWidget obj3 = new QTDPayoutWidget();
            
           
           obj1.metricPerformance(); 
           obj2.GoldCup(); 
           obj3.QTD_Payout(); 
           
           }
           system.runas(u3){
           
           budgetUtilizationWidget obj4 = new budgetUtilizationWidget();
          
            obj4.budgetUtilization();
            obj4.dummy();
            
           }
           user user_nat = [select id from user where id = '0055e000007Fj0IAAS'];
           system.runas(user_nat){
           nationalPerformanceWidget obj5 = new nationalPerformanceWidget();
           obj5.nationalPerformance();
           obj5.dummy();
           }
}
      }