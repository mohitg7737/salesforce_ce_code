public class Territory_Summary_Report_Ctrl
{
	public List<Wrapclass> wrapperList              {get; set;}
	public String selectedTeamInstance 				{get; set;}
	public Territory_Summary_Report_Ctrl()
	{

		selectedTeamInstance = ApexPages.currentPage().getParameters().get('selectedTeamInstance');

		Map<String, AxtriaSalesIQTM__User_Access_Permission__c> userAccessPosMap = new Map<String, AxtriaSalesIQTM__User_Access_Permission__c>();
		
		List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData = SalesIQUtility.getUserAccessPermistion(UserInfo.getUserId());
		if(selectedTeamInstance == null)
		{
			selectedTeamInstance = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
		}

		List<AxtriaSalesIQTM__User_Access_Permission__c> userAccessList= [Select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__User__c, AxtriaSalesIQTM__User__r.LastLoginDate, AxtriaSalesIQTM__User__r.Name, AxtriaSalesIQTM__Team_Instance__c From AxtriaSalesIQTM__User_Access_Permission__c Where AxtriaSalesIQTM__Is_Active__c = true and AxtriaSalesIQTM__User__r.IsActive = true and AxtriaSalesIQST__isCallPlanEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c =: selectedTeamInstance];

		AxtriaSalesIQTM__Team_Instance__c tiRec = [Select AxtriaSalesIQST__Rep_Start_Date__c, AxtriaSalesIQST__DM_Start_Date__c, AxtriaSalesIQST__Level3_Start_Date__c, AxtriaSalesIQST__Level4_Start_Date__c from AxtriaSalesIQTM__Team_Instance__c where Id =:selectedTeamInstance];

		for(Integer i = 0, j = userAccessList.size(); i<j; i++)
		{
			userAccessPosMap.put(userAccessList[i].AxtriaSalesIQTM__Position__c, userAccessList[i]);
		}

		List<AxtriaSalesIQTM__Position__c> positionList = new List<AxtriaSalesIQTM__Position__c>();
		positionList = [Select id, Name, AxtriaSalesIQST__Call_Plan_Status__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Client_Position_Code__c From AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance AND (AxtriaSalesIQTM__Is_Global_Unassigned_Position__c = false and AxtriaSalesIQTM__Is_Unassigned_Position__c = false)];

		Map<String, Integer> preRefinementPhyNoPosMap = new Map<String, Integer>();
		Map<String, Integer> postRefinementPhyNoPosMap = new Map<String, Integer>();
		Map<String, Integer> phyAddedPosMap = new Map<String, Integer>();
		Map<String, Integer> phyDroppedPosMap = new Map<String, Integer>();

		List<AggregateResult> aggResList = [Select AxtriaSalesIQTM__Position__c pos, sum(AxtriaSalesIQST__CountOriginal__c) noOfPhyOriginal, sum(AxtriaSalesIQST__Updated_Count__c) noOfPhyUpdated, sum(isAdded__c) addedPhy, sum(isDropped__c) droppedPhy  From AxtriaSalesIQTM__Position_Account_Call_Plan__c Where AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance AND (AxtriaSalesIQST__CountOriginal__c = 1 or AxtriaSalesIQST__Updated_Count__c = 1) Group By AxtriaSalesIQTM__Position__c];

		for(Integer i = 0, j = aggResList.size(); i<j; i++)
		{
			String posId = (String)aggResList[i].get('pos');
			Integer orgNum = Integer.valueOf(aggResList[i].get('noOfPhyOriginal'));
			Integer updatedNum = Integer.valueOf(aggResList[i].get('noOfPhyUpdated'));
			Integer addedNum = Integer.valueOf(aggResList[i].get('addedPhy'));
			Integer droppedNum = Integer.valueOf(aggResList[i].get('droppedPhy'));

			preRefinementPhyNoPosMap.put(posId,orgNum);
			postRefinementPhyNoPosMap.put(posId,updatedNum);
			phyAddedPosMap.put(posId,addedNum);
			phyDroppedPosMap.put(posId,droppedNum);
		}

		String teamName;
		String regionName;
		String districtName;
		String terrIDName;
		String repName;
		String repId;
		String dmName;
		String dmId;
		Boolean isRepLogin;
		Boolean isDMLogin;
		Boolean isSubmitted;
		Boolean isApproved;
		Integer preRefinementCalls;
		Integer preRefinementPhy;
		Integer postRefinementPhy;
		Integer postRefinementCalls;
		Decimal changeInCalls;
		Decimal changeInPhy;
		Integer addedPhy;
		Integer droppedPhy;

		wrapperList = new List<WrapClass>();
		for(Integer i = 0, j = positionList.size(); i<j ; i++)
		{
			teamName = positionList[i].AxtriaSalesIQTM__Team_Instance__r.Name;
			regionName = positionList[i].AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
			districtName = positionList[i].AxtriaSalesIQTM__Parent_Position__r.Name;
			terrIDName = positionList[i].AxtriaSalesIQTM__Client_Position_Code__c + ' - ' + positionList[i].Name;
			if(userAccessPosMap.containsKey(positionList[i].Id))
			{
				repName = userAccessPosMap.get(positionList[i].Id).AxtriaSalesIQTM__User__r.Name;
				repId = userAccessPosMap.get(positionList[i].Id).AxtriaSalesIQTM__User__c;
				isRepLogin = userAccessPosMap.get(positionList[i].Id).AxtriaSalesIQTM__User__r.LastLoginDate >= tiRec.AxtriaSalesIQST__Rep_Start_Date__c;
			}
			else
			{
				repName = '';
				repId = '';
				isRepLogin = false;
			}

			if(userAccessPosMap.containsKey(positionList[i].AxtriaSalesIQTM__Parent_Position__c))
			{
				dmName = userAccessPosMap.get(positionList[i].AxtriaSalesIQTM__Parent_Position__c).AxtriaSalesIQTM__User__r.Name;
				dmID = userAccessPosMap.get(positionList[i].AxtriaSalesIQTM__Parent_Position__c).AxtriaSalesIQTM__User__c;
				isDMLogin = userAccessPosMap.get(positionList[i].AxtriaSalesIQTM__Parent_Position__c).AxtriaSalesIQTM__User__r.LastLoginDate >= tiRec.AxtriaSalesIQST__DM_Start_Date__c;
			}
			else
			{
				dmName = '';
				dmID = '';
				isDMLogin = false;
			}

			if(positionList[i].AxtriaSalesIQST__Call_Plan_Status__c == 'Submitted' || positionList[i].AxtriaSalesIQST__Call_Plan_Status__c == 'Approved')
			{
				isSubmitted = true;
			}
			else
			{
				isSubmitted = false;
			}
			if(positionList[i].AxtriaSalesIQST__Call_Plan_Status__c == 'Approved')
			{
				isApproved = true;
			}
			else
			{
				isApproved = false;
			}
			
			preRefinementPhy = preRefinementPhyNoPosMap.get(positionList[i].Id) == null ? 0 : preRefinementPhyNoPosMap.get(positionList[i].Id);
			
			postRefinementPhy = postRefinementPhyNoPosMap.get(positionList[i].Id) == null ? 0 : postRefinementPhyNoPosMap.get(positionList[i].Id);
			addedPhy = phyAddedPosMap.get(positionList[i].Id);
			droppedPhy = phyDroppedPosMap.get(positionList[i].Id);

			

			if(preRefinementPhy != 0)
				changeInPhy = ((postRefinementPhy - preRefinementPhy) * 100.0 / preRefinementPhy).setScale(2);
			else
				changeInPhy = 0.0;
			wrapperList.add(new Wrapclass(teamName, regionName, districtName, terrIDName, repName, dmName, isRepLogin, isDMLogin, isSubmitted, isApproved, preRefinementCalls, postRefinementCalls, changeInCalls, preRefinementPhy, postRefinementPhy, changeInPhy, addedPhy, droppedPhy));
		}

	}


	public class Wrapclass
	{
		public string teamName                      {get; set;}
		public string regionName                    {get; set;}
		public string districtName                  {get; set;}
		public string terrIDName                    {get; set;}
		public string repName                       {get; set;}
		public string dmName                        {get; set;}
		public string isRepLogin                    {get; set;}
		public string isDMLogin                     {get; set;}
		public string isSubmitted                   {get; set;}
		public string isApproved                    {get; set;}
		public Integer preRefinementCalls           {get; set;}
		public Integer preRefinementPhy             {get; set;}
		public Integer postRefinementPhy            {get; set;}
		public Integer postRefinementCalls          {get; set;}
		public Decimal changeInCalls                {get; set;}
		public Decimal changeInPhy                  {get; set;}
		public Integer addedPhy                     {get; set;}
		public Integer droppedPhy                   {get; set;}


		public Wrapclass(String teamName, String regionName, String districtName, String terrIDName, String repName, String dmName, Boolean isRepLogin, Boolean isDMLogin, Boolean isSubmitted, Boolean isApproved, Integer preRefinementCalls, Integer postRefinementCalls, Decimal changeInCalls, Integer preRefinementPhy, Integer postRefinementPhy, Decimal changeInPhy, Integer addedPhy, Integer droppedPhy)
		{
			if(teamName != null)
				this.teamName = teamName;
			else
				this.teamName = '';

			this.regionName = regionName;
			this.districtName = districtName;
			this.terrIDName = terrIDName;
			this.repName = repName;
			this.dmName = dmName;

			if(isRepLogin)
				this.isRepLogin = 'Y';
			else
				this.isRepLogin = 'N';

			if(isDMLogin)
				this.isDMLogin = 'Y';
			else
				this.isDMLogin = 'N';

			if(isSubmitted)
				this.isSubmitted = 'Y';
			else
				this.isSubmitted = 'N';

			if(isApproved)
				this.isApproved = 'Y';
			else
				this.isApproved = 'N';

			this.preRefinementCalls = preRefinementCalls;
			this.postRefinementCalls = postRefinementCalls;
			this.changeInCalls = changeInCalls;
			this.preRefinementPhy = preRefinementPhy;
			this.postRefinementPhy = postRefinementPhy;
			this.changeInPhy = changeInPhy;
			this.addedPhy = addedPhy != null ? addedPhy : 0;
			this.droppedPhy = droppedPhy != null ? droppedPhy : 0;
		}
	}

}