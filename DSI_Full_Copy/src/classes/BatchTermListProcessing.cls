global class BatchTermListProcessing implements Database.Batchable<sObject> {
    public String query;

    global BatchTermListProcessing() {
        delete[select id from Term_List__c];
        query = 'Select id, Termination_Date__c, Reason_for_Action_Name__c, Personnel_Area__c, AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee_ID__c, Personnel_Subarea_Code__c, Territory__c, Personnel_Subarea_Name__c, Personnel_Number__c, Personnel_Area_Number__c from AxtriaSalesIQTM__Employee__c where Termination_Date__c != null';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('@@@ Start method of BatchICTermDataProcessing invoked');
        System.debug('@@@ query = '+query);
        return Database.getQueryLocator(query);
        
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Employee__c> scopeEmpList) {
        List<Term_List__c> TermInsertionList = new List<Term_List__c>();

        for(AxtriaSalesIQTM__Employee__c emp :  scopeEmpList){
            Term_List__c term = new Term_List__c();
            term.personnel_area__c = emp.Personnel_Area__c;
            term.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c;
            term.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            term.action_type__c = emp.Reason_for_Action_Name__c;
            term.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            //DateTime dT = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Termination_Date__c;
            //Date d = Date.newInstance(dT.year(), dT.month(), dT.day());
            term.separation_date__c = emp.Termination_Date__c;
            term.s__c = '0';
            term.psubarea__c = emp.Personnel_Subarea_Code__c;
            term.prev_term_pos_stext__c = ' ';
            term.prev_term_pos_short__c = ' ';
            term.prev_term_pos_code__c = emp.Territory__c;
            term.personnel_subarea__c = emp.Personnel_Subarea_Name__c;
            term.pers_no__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            term.pa__c = emp.Personnel_Area_Number__c;

            TermInsertionList.add(term);
        }
        if(TermInsertionList.size()>0){
            insert TermInsertionList;
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}