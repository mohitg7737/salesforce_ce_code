@istest
public with sharing class BatchUserInactiveTest {
    static testMethod void validateOnInsertUserPositions()
    {
      

          list<Profile> hoProfile = [SELECT Id FROM Profile WHERE Name =: SalesIQGlobalConstants.HR_PROFILE limit 1];
        if(hoProfile.size() == 0){
            hoProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
        }
        AxtriaSalesIQTM__TriggerContol__c cs = new AxtriaSalesIQTM__TriggerContol__c();
        cs.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs.Name = 'RosterTriggers';
        insert cs;
        AxtriaSalesIQTM__TriggerContol__c cs1 = new AxtriaSalesIQTM__TriggerContol__c();
        cs1.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs1.Name = 'UpdateRelatedSiqEmployee';
        insert cs1;
        AxtriaSalesIQTM__Employee__c obj = new AxtriaSalesIQTM__Employee__c (
        Name = 'Test Empl',
        AxtriaSalesIQTM__Employee_ID__c = '12345',
        AxtriaSalesIQTM__Last_Name__c = 'Empl',
        AxtriaSalesIQTM__FirstName__c = 'Test',
        AxtriaSalesIQTM__Gender__c = 'M',
        DSI_Email_Id__c = 'test.ho@test.com',
        AxtriaSalesIQTM__Change_status__c = 'Pending',
        Termination_Date__c = Date.today()-1);
        insert obj;
        
        User usr = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile[0].id,
                            FederationIdentifier = 'test.ho@test.com',
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');

        insert usr;

        User usr2 = new User(LastName = 'ho2',
                            FirstName='test2',
                            Alias = 'test2',
                            Email = 'test.ho2@test.com',
                            Username = 'test.h2@test.com',
                            ProfileId = hoProfile[0].id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert usr2;
        

        test.startTest();
        database.executeBatch(new BatchUserInactive(),2000);

        test.stopTest();
        

    }
}