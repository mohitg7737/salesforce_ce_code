/***************************************************************************************
// (c)2018 Axtria, Inc.
//
// Name       : QuickLinkPage
// Author     : Naman
// Purpose    : Quick Links Widget on Dashboard
// History    : Date          | Updated By       | Reason
//              July 2018      | Naman           | Original
******************************************************************************************/


public with sharing class  QuickLinkCtrlCustom {

    public string query;
    //public Map<string,list<String>> mapoffieldapi;
    //public Map<string,String> mapofwhrclause;
    //public string profilename;
    public String columnsListComa;
    public list<String> objName;
    public String whrclause;
    public Id tenantid;
    public string tenantname;
    public string linkurl;
    public string imagelink;
    public map<String, string> mapoflink;
    public map<String, String> mapofcolumn;
    public transient String theme {set;get;}
    public transient String message{set;get;}

    public list<Wrapper> urlwrapper {get; set;}
    public List < SIQIC__View_Data_Mapping__c > vdMapping {get; set;}
    public List < sObject > repList {get; set;}
    Id currentUSerId = UserInfo.getUserId();
    public Set<String> buList;

    public QuickLinkCtrlCustom()
    {
    try{        
        Map<string,String> mapofwhrclause;
        Map<string,String> mapofOrderByclause = new Map<String,String>();
        tenantId = SIQIC.fetchuserinfo.userFetchTenantID();
        string name;
        tenantName = SIQIC.fetchuserinfo.userFetchTenant();
        buList=new Set<String>();


        urlwrapper = new list<Wrapper>();
        //Added new field in query - orderby_limit__c
        vdMapping = [Select Name, SIQIC__DefaultFilterNew__c, SIQIC__Fieldapi__c, SIQIC__DisplayColumns__c,SIQIC__linkmapping__c, 
                            SIQIC__Quicklink__c,SIQIC__Object_API__c, SIQIC__Tenant__c,RecordType.Name,SIQIC__orderby_limit__c
                     from   SIQIC__View_Data_Mapping__c
                     where  SIQIC__tenant__c =: tenantId and Recordtype.name='QuickLinkSelf' 
                     With Security_Enforced  order by SIQIC__Object_API__c desc
                     ];
        SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('vdmaping=' + vdMapping + ' size=' + vdMapping.size());
        //mapoffieldapi= new Map<String, list<String>>();
        mapofwhrclause = new Map<String, String>();
        mapoflink = new Map<String, String>();
        mapofcolumn = new Map<String, String>();
        objName = new list<string>();

        for (SIQIC__View_Data_Mapping__c viewdata : vdMapping) {
           //Start:Added by Geeta
            if(viewdata.SIQIC__orderby_limit__c != null && viewdata.SIQIC__orderby_limit__c.trim() != '')
            {   //in this the default filter will come... which we will use in where clause
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('orderby===='+viewdata.SIQIC__orderby_limit__c);
                mapofOrderByclause.put(viewdata.SIQIC__object_api__c,viewdata.SIQIC__orderby_limit__c);
            }
            //End:Added by Geeta
            if (viewdata.SIQIC__DefaultFilterNew__c != null) {
                //in this the default filter will come... which we will use in where clause
                whrClause = viewdata.SIQIC__DefaultFilterNew__c;
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('whrClause====' + whrClause);
                mapofwhrclause.put(viewdata.SIQIC__object_api__c, whrclause);
            }

            if (viewdata.SIQIC__object_api__c != null) {
                // single object is expected here
                // the object api for which we have to show data on quick link
                if (viewdata.SIQIC__object_api__c.contains(' ') || viewdata.SIQIC__object_api__c.contains(';') || viewdata.SIQIC__object_api__c.contains(',')) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Configuration is missing! Correct the value in: SIQIC__displaycolumns__c,SIQIC__object_api__c');
                    ApexPages.addMessage(myMsg);
                } else
                    objName.add(viewdata.SIQIC__object_api__c);
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('objName====' + objName );
            }



            if (viewdata.SIQIC__fieldapi__c != null) {
                //list of fields used for displaying
                columnsListComa = viewdata.SIQIC__Fieldapi__c;
                mapofcolumn.put(viewdata.SIQIC__object_api__c, columnsListComa);
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('mapofcolumn====' + mapofcolumn);
                List < string > columnsApi = viewdata.SIQIC__Fieldapi__c.split(',');
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('columnsListComa====' + columnsListComa);
            }

            if (viewdata.SIQIC__Quicklink__c != null) {
                linkurl = viewdata.SIQIC__QuickLink__c;
                SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('linkurl====' + linkurl);
                mapoflink.put(viewdata.SIQIC__object_api__c, linkurl);
            }




        }



        for (String s : objname) {
            buList=SIQIC.fetchuserinfo.getHOUserBuList();
            String query1 = 'SELECT ' + mapofcolumn.get(s) + '  from ' + s ;
            SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('query1===' + query1);

           //Added  by Geeta: added orderby clasue dynamically
           if(mapofwhrclause.containsKey(s))
           {    
               String query2= + ' Where ' + mapofwhrclause.get(s);
               query = query1 + query2 +' and SIQIC__tenant__c=:tenantName ';

               query += mapofOrderByclause.containsKey(s) ? mapofOrderByclause.get(s) : ' With Security_Enforced order by createddate limit 100';
           }
           else{
                query = query1 + ' Where SIQIC__tenant__c=:tenantName ' ;

                query += mapofOrderByclause.containsKey(s) ? mapofOrderByclause.get(s) : ' With Security_Enforced   order by createddate limit 1';
            }
             repList = queryMethod(query);
             
             for(sobject sobj : repList)
                 {
                     //String Page = mapofobjectpage.get(s);
                     String baseUrl=  System.URL.getSalesforceBaseUrl().toExternalForm();
                     SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('baseUrl==='+baseUrl);
                     
                     SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('test====='+(mapofcolumn.get(s)).subString(0,(mapofcolumn.get(s)).indexOf(',')));
                     //if(s == 'Reports__c')
                     if(mapoflink.containsKey(s)  &&  mapoflink.get(s) != 'id')
                     {
                         //Since for reports the url is different
                         //so we have appended the url as below
                         //Eg. url : https://ammax--ICQA.cs19.my.salesforce.com/apex/IC_scorecard?id=a0s29000000a6g3
                         //baseurl+= '/'+Page+'?id='+sobj.id;
                         //baseurl= (string)sobj.get(mapoflink.get(s))+'?id='+sobj.id;
                        baseurl= (string)sobj.get(mapoflink.get(s)); //updated by Geeta fullurl with id would be available in link field
                          string x= baseurl.substringBetween('"');
                          SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('x===='+x);
                          SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('baseurl==='+baseurl);
                         urlwrapper.add(new wrapper(baseurl,sobj.id,(String)sobj.get
                         (((mapofcolumn.get(s)).subString(0,(mapofcolumn.get(s)).indexOf(','))))));
                     }
                     
                     else
                     {    
                         //in others, by only id field we can display.
                         //Eg. url: https://ammax--icqa.cs19.my.salesforce.com/a0m29000000TxQPAA0
                         
                         baseurl+= '/'+sobj.id;
                         urlwrapper.add(new wrapper(baseurl,sobj.id,(String)sobj.get
                                    (((mapofcolumn.get(s)).subString(0,(mapofcolumn.get(s)).indexOf(','))))));
                     }
                     
                  }
       }
                
      //getLatestPublishVideo();
        }            
        catch(Exception e){
            SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('--- Error ---- ' + e.Getstacktracestring());
            theme = SIQIC.Constant.TOAST_THEME_ERROR;
            message=e.Getstacktracestring();
        }
    }


    public list<sobject> queryMethod(string query) {

        SIQIC.SalesIqDmlSecurityUtil.printDebugMessage('FinalQuery====' + query);

        return Database.query(query);
    }

    public class Wrapper {
        public String url {get; set;}
        public Id wid {get; set;}
        public String name {get; set;}


        public Wrapper(String url, id wid, String name) {
            this.url = url;
            this.wid = wid;
            this.name = name;


        }
    }
public void dummyOne(){
        Integer i = 0;
        
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
}
         

}