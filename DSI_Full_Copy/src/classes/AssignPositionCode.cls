//Author - Mohit Goyal
//A1691
public class AssignPositionCode {

    public static Set<String> allClientPositionCodes;



  public static String getnextCode(String pos , Integer breakcount){
       

       Integer breakat ;
       String code;
       String left,right,finalstring;
       Integer middle;
       breakat = breakcount;

       code = pos;
       
 
          if (!string.isEmpty(code))
          {

              Integer totallenght = code.length();

              left = code.substring(0,breakat);
              middle = code.charAt(breakat);
              right = code.substring(breakat+1,totallenght);

          } 
    
          middle= middle +1;

          System.debug('middle++++++++'+middle);
          String c = String.fromCharArray(new List<Integer> {middle});

          finalstring = left+c+right;

          System.debug('lasttosend++++++++'+finalstring);

          return finalstring;
          
       
    }


  public static String checkexistigPosition (String tocheckexisting , Integer brekpoint){

    /*nextpos = calculate();

    while(isExist(nextPos)){
          nextPos = calculate();
        }

     return nextPos;
*/
    list<ExistingPosition__c> existposlist = [Select id,name , clientcode__c from ExistingPosition__c where clientcode__c =:tocheckexisting];

    if(existposlist!=null && existposlist.size()>0){

      tocheckexisting=AssignPositionCode.getnextCode(tocheckexisting,brekpoint);

      if (tocheckexisting!=null){

        tocheckexisting = AssignPositionCode.checkexistigPosition(tocheckexisting,brekpoint);

      }

        
    }

      return tocheckexisting;

  }


    public static String assignCode(AxtriaSalesIQTM__Position__c pos , Integer breakcount){
       

       Integer breakat ;
       String left,right,finalstring,intermediatestring;
       Integer middle;
       breakat = breakcount;

       String parentcode,between;
       List<AxtriaSalesIQTM__Position__c> greatestchild = new List<AxtriaSalesIQTM__Position__c>();
       
       greatestchild = [Select name,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Parent_Position_Code__c,Territory_Code__c  from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Parent_Position__c =: pos.AxtriaSalesIQTM__Parent_Position__c and AxtriaSalesIQTM__Team_Instance__c = :pos.AxtriaSalesIQTM__Team_Instance__c  and  (not name like '%unassigned%') and (not name like '%whitespace%') and (not name like '%Whitespace%') and (not name like '%Unassigned%')  order by AxtriaSalesIQTM__Client_Position_Code__c desc limit 1];
           System.debug('---greatestchild--'+greatestchild);
       
       if(greatestchild.size()>0)
       {
          String code = greatestchild[0].AxtriaSalesIQTM__Client_Position_Code__c;
          if (!string.isEmpty(code))
          {

              Integer totallenght = code.length();

              left = code.substring(0,breakat);
              middle = code.charAt(breakat);
              right = code.substring(breakat+1,totallenght);

          } 
    
          middle= middle +1;

          System.debug('middle++++++++'+middle);
          String c = String.fromCharArray(new List<Integer> {middle});

          intermediatestring = left+c+right;

          finalstring = AssignPositionCode.checkexistigPosition(intermediatestring,breakat);

          System.debug('lasttosend++++++++'+finalstring);

          return finalstring;
          
       }
        

        else{

            System.debug('Entered the else of Insert Event');


              if(pos.AxtriaSalesIQTM__Parent_Position_Code__c !=null)
              {

                parentcode = pos.AxtriaSalesIQTM__Parent_Position_Code__c;

                System.debug('parentcode$$$$$$$$$'+parentcode);

                if (!string.isEmpty(parentcode))
                    {

                        Integer totallenght = parentcode.length();
                        if(pos.AxtriaSalesIQTM__Position_Type__c == 'District') 
                        {
                                left = parentcode.substring(0,4);
                                if(pos.AxtriaSalesIQTM__Hierarchy_Level__c == '2')
                                {
                                  between = 'A';
                                  right = parentcode.substring(5,totallenght);
                                }

                                else if(pos.AxtriaSalesIQTM__Hierarchy_Level__c == '1')

                                {
                                  between = 'AA';
                                  right = pos.Territory_Code__c;
                                }

                        }

                        else if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory' && pos.AxtriaSalesIQTM__Hierarchy_Level__c == '1')

                               {

                                left = parentcode.substring(0,5);
                                between = 'A';
                                right = pos.Territory_Code__c;

                               }

                         else if (pos.AxtriaSalesIQTM__Position_Type__c == 'Region' )
                               {
                                 left = parentcode.substring(0,2);
                                 between = 'AA';
                                 right = parentcode.substring(4,totallenght);

                              }



                        intermediatestring = left+between+right;

                        finalstring = AssignPositionCode.checkexistigPosition(intermediatestring,breakat);
                        System.debug('finalstring$$$$$$$'+finalstring);
                    } 

              }


          return finalstring;
        }
        
   }



     public static void assignPositionCodeForUpdateEvent (List<AxtriaSalesIQTM__Position__c> posList)
   {
            
            allClientPositionCodes = new Set<String>();

            List<Id> allTI = new List<Id>();
            List<AxtriaSalesIQTM__Position__c> requiredPosList = new List<AxtriaSalesIQTM__Position__c>();
            List<ID> requiredPosSet = new List<ID>();

            List<AxtriaSalesIQTM__Position__c> current_pos = new List<AxtriaSalesIQTM__Position__c>();

            Set<ID> team_set = new Set<Id>();

            
            for(AxtriaSalesIQTM__Position__c each: posList)  
            {
                allTI.add(each.AxtriaSalesIQTM__Team_Instance__c);
            }

            
            Map<Id,AxtriaSalesIQTM__Team_Instance__c> mapTI = new  Map<Id,AxtriaSalesIQTM__Team_Instance__c>([Select Id,Name, AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c, AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Create_Mirror_Position__c from AxtriaSalesIQTM__Team_Instance__c where Id IN : allTI]);

            for(AxtriaSalesIQTM__Team_Instance__c ti :[Select AxtriaSalesIQTM__Team__r.ID from AxtriaSalesIQTM__Team_Instance__c where id in :allTI]){
                team_set.add(ti.AxtriaSalesIQTM__Team__c);
            }
            

            Map<Id,AxtriaSalesIQTM__Position__c> mapPos = new Map<Id,AxtriaSalesIQTM__Position__c>([Select Id,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__IsMaster__c,AxtriaSalesIQTM__Master_Position_Reference__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Effective_End_Date__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.id in :team_set]);

            
            for(AxtriaSalesIQTM__Position__c each: posList)  
            {
                String status = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c;
                if(status != 'In Queue' && status != 'In Progress'){
                    requiredPosList.add(each);
                    requiredPosSet.add(each.ID);
                    /*requiredPosSet.add(each.AxtriaSalesIQTM__Master_Position_Reference__c);*/
                }
            }
           
          
            
            if(requiredPosList.size()>0)
            {
                for(AxtriaSalesIQTM__Position__c each: requiredPosList)
                {

                   String code;
                    if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '1' &&   (each.AxtriaSalesIQTM__Position_Type__c == 'Rep' || each.AxtriaSalesIQTM__Position_Type__c == 'Territory' || each.AxtriaSalesIQTM__Position_Type__c == 'District')){

                      code = assignCode(each,5);

                      System.debug('Territory code upd++++++'+code);
                    }

                    else if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '2')

                    {


                              if(each.AxtriaSalesIQTM__Position_Type__c == 'District')
                              {
                                code = assignCode(each,4);
                                System.debug('2--Districtupd---'+code);
                    
                              }

                              else if(each.AxtriaSalesIQTM__Position_Type__c == 'Region')
                              {

                              code = assignCode(each,3);
                              System.debug('2--Regionupd---'+code);

                              }
                  
                    }

                    else if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '3' && each.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                    code = assignCode(each,3);
                    System.debug(' Regionupd---'+code);
                    }
                    
                    if(code != null && !code.equals(''))
                    {

                        each.AxtriaSalesIQTM__Client_Territory_Code__c = code;
                        each.AxtriaSalesIQTM__Client_Position_Code__c = code;
                        each.AxtriaSalesIQTM__Client_Territory_Name__c =  each.Name + ' (' + code + ')';
                        each.AxtriaSalesIQTM__Effective_Start_Date__c = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c;
                          System.debug('Hi Testing !!');
                          /*if(each.Id != each.AxtriaSalesIQTM__Master_Position_Reference__c){

                            current_pos.add(new AxtriaSalesIQTM__Position__c(Id = each.AxtriaSalesIQTM__Master_Position_Reference__c,AxtriaSalesIQTM__IsMaster__c = True, AxtriaSalesIQTM__Effective_End_Date__c = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1)));
                          }
                         
                        
                        each.AxtriaSalesIQTM__Master_Position_Reference__c = each.Id;

                        current_pos.add(each);*/

                        

                    }
                    
                }


                /*if(current_pos.size()>0){
                  update current_pos;    
                }*/
               

            }
   }

     public static void assignPositionCodeForCreateEvent (List<AxtriaSalesIQTM__Position__c> posList )
   {
            List<Id> allTI = new List<Id>();
            List<AxtriaSalesIQTM__Position__c> requiredPosList = new List<AxtriaSalesIQTM__Position__c>();
            allClientPositionCodes = new Set<String>();


            for(AxtriaSalesIQTM__Position__c each: posList)  
            {
                allTI.add(each.AxtriaSalesIQTM__Team_Instance__c);
            }

            
            Map<Id,AxtriaSalesIQTM__Team_Instance__c> mapTI = new  Map<Id,AxtriaSalesIQTM__Team_Instance__c>([Select Id, AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c,AxtriaSalesIQTM__Create_Mirror_Position__c,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where Id IN : allTI]);
            
            for(AxtriaSalesIQTM__Position__c each: posList)  
            {
                String status = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c;
                if(status != 'In Queue' && status != 'In Progress'){
                    //each.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Create_Mirror_Position__c=mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Create_Mirror_Position__c;
                    requiredPosList.add(each);
                }
            }
            
            if(requiredPosList.size()>0)
            {
                for(AxtriaSalesIQTM__Position__c each: requiredPosList)
                {
                    String code;
                    if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '1' &&   (each.AxtriaSalesIQTM__Position_Type__c == 'Rep' || each.AxtriaSalesIQTM__Position_Type__c == 'Territory' || each.AxtriaSalesIQTM__Position_Type__c == 'District')){

                        code = assignCode(each,5);

                      System.debug('Territory code++++++'+code);
                    }

                    else if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '2')

                    {


                              if(each.AxtriaSalesIQTM__Position_Type__c == 'District')
                              {
                                code = assignCode(each,4);
                                System.debug('2--District---'+code);
                    
                              }

                              else if(each.AxtriaSalesIQTM__Position_Type__c == 'Region')
                              {

                              code = assignCode(each,3);
                              System.debug('2--Region---'+code);

                              }
                  
                    }

                    else if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '3') 
                           {

                              if (each.AxtriaSalesIQTM__Position_Type__c == 'Region')
                              {
                                code = assignCode(each,3);
                                System.debug(' 3--Region---'+code);
                              }

                              else if (each.AxtriaSalesIQTM__Position_Type__c == 'Area')
                              {

                                code = assignCode(each,1);
                                System.debug(' 3--Area---'+code);
                              }


                            }
                      else if(each.AxtriaSalesIQTM__Hierarchy_Level__c == '4' && each.AxtriaSalesIQTM__Position_Type__c == 'Area' )

                              {

                                code = assignCode(each,1);
                               System.debug(' 4--Area---'+code);

                              }
                      
                    System.debug('Hi each here'+ each);
                    System.debug('Hi code here'+ code);

                    if(code != null && !code.equals(''))
                    {
                        each.AxtriaSalesIQTM__Client_Position_Code__c = code;
                        each.AxtriaSalesIQTM__Client_Territory_Code__c = code;
                        each.Name = each.AxtriaSalesIQTM__Description__c;
                        each.AxtriaSalesIQTM__Client_Territory_Name__c =  each.Name + ' (' + code + ')';
                        each.AxtriaSalesIQTM__Effective_Start_Date__c = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c;
                        each.AxtriaSalesIQTM__Effective_End_Date__c = date.ValueOf('2099-12-31');
                        System.debug('each.AxtriaSalesIQTM__Client_Position_Code__c ----'+each.AxtriaSalesIQTM__Client_Position_Code__c );
                    }

                    else 
                    {

                        each.Name = each.AxtriaSalesIQTM__Description__c;
                        each.AxtriaSalesIQTM__Client_Territory_Name__c =  each.Name + ' (' + each.AxtriaSalesIQTM__Client_Territory_Code__c + ')';
                        each.AxtriaSalesIQTM__Effective_Start_Date__c = mapTI.get(each.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c;
                        each.AxtriaSalesIQTM__Effective_End_Date__c = date.ValueOf('2099-12-31');


                    }
                }
            }
   }


   public void dummyFunction(){

    Integer i = 0;
    i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
   }

}