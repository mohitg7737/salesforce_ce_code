@isTest
public with sharing class TestBatchInactiveUserPosition {

    static testMethod void testTeamInstance(){
        Set<Id> teamInstanceSet = new Set<Id>();
       
        AxtriaSalesIQTM__TriggerContol__c cs = new AxtriaSalesIQTM__TriggerContol__c();
        cs.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs.Name = 'RosterTriggers';
        insert cs;
        
        AxtriaSalesIQTM__TriggerContol__c cs1 = new AxtriaSalesIQTM__TriggerContol__c();
        cs1.AxtriaSalesIQTM__IsStopTrigger__c = false;
        cs1.Name = 'UpdateRelatedSiqEmployee';
        insert cs1;
        
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='HTN',AxtriaSalesIQTM__Type__c='Base');
        insert team;

        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        teamInstance.Name = 'HTN_Q1_2016';
        teamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'HTN_Q1_2016';
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        insert teamInstance;


        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Long Island East, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long Island East, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos.AxtriaSalesIQTM__Related_Position_Type__c=team.AxtriaSalesIQTM__Type__c;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c='1';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        
        insert pos;

        AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
        pos1.name = 'Long  East, NY';
        pos1.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long  East, NY';
        pos1.AxtriaSalesIQTM__Client_position_Code__c = '1NE30003';
        pos1.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30003';
        pos1.AxtriaSalesIQTM__position_Type__c='Territory';
        pos1.AxtriaSalesIQTM__Related_position_Type__c= team.AxtriaSalesIQTM__Type__c;
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c='1';
        pos1.AxtriaSalesIQTM__inactive__c = false;
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos1.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos1.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos1.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos1.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos1.AxtriaSalesIQTM__IsMaster__c = true;

        insert pos1;

        AxtriaSalesIQTM__Position__c pos2= new AxtriaSalesIQTM__Position__c();
        pos2.name = 'Long Island East, NY';
        pos2.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long Island East, NY';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos2.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos2.AxtriaSalesIQTM__Related_Position_Type__c=team.AxtriaSalesIQTM__Type__c;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c='1';
        pos2.AxtriaSalesIQTM__inactive__c = false;
        pos2.AxtriaSalesIQTM__Effective_Start_Date__c  =system.today().AddDays(-5);
        pos2.AxtriaSalesIQTM__Effective_End_Date__c  =system.today().AddDays(5);
        pos2.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        pos2.AxtriaSalesIQTM__IsMaster__c = true;
        
        insert pos2;

        teamInstanceSet.add(teamInstance.id);

        User user1 = new User();
        user1.IsActive = true;
        user1.FederationIdentifier = 'dsi@test.com';
        user1.id= userinfo.getuserid();

        User user2 = new User();
        user2.IsActive = true;
        user2.FederationIdentifier = 'dsi2@test.com';
        user2.id= userinfo.getuserid();

        AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap.AxtriaSalesIQTM__Is_Active__c = true;
        uap.AxtriaSalesIQTM__Position__c = pos.id;
        uap.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap.AxtriaSalesIQTM__User__c = user1.Id;
        uap.AxtriaSalesIQTM__Sharing_Type__c = 'Explicit';
        insert uap;

        AxtriaSalesIQTM__User_Access_Permission__c uap1 = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap1.AxtriaSalesIQTM__Is_Active__c = false;
        uap1.AxtriaSalesIQTM__Position__c = pos.id;
        uap1.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap1.AxtriaSalesIQTM__User__c = user1.Id;
        uap1.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
        insert uap1;

        AxtriaSalesIQTM__User_Access_Permission__c uap2 = new AxtriaSalesIQTM__User_Access_Permission__c();
        uap2.AxtriaSalesIQTM__Is_Active__c = false;
        uap2.AxtriaSalesIQTM__Position__c = pos.id;
        uap2.AxtriaSalesIQTM__Team_Instance__c = teamInstance.Id;
        uap2.AxtriaSalesIQTM__User__c = user2.Id;
        uap2.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
        insert uap2;
        
        AxtriaSalesIQTM__Employee__c emp2 = new AxtriaSalesIQTM__Employee__c();
        emp2.Name = 'Test Emp2';
        emp2.AxtriaSalesIQTM__Employee_ID__c = '12346';
        emp2.AxtriaSalesIQTM__Last_Name__c = 'Emp2';
        emp2.AxtriaSalesIQTM__FirstName__c = 'Test1';
        emp2.AxtriaSalesIQTM__HR_Status__c = 'Active';
        emp2.DSI_Email_Id__c = 'dsi@test.com';
        emp2.AxtriaSalesIQTM__USER__c = userinfo.getuserid();
        insert emp2;

        AxtriaSalesIQTM__Employee__c emp3 = new AxtriaSalesIQTM__Employee__c();
        emp3.Name = 'Test Emp2';
        emp3.AxtriaSalesIQTM__Employee_ID__c = '12346';
        emp3.AxtriaSalesIQTM__Last_Name__c = 'Emp2';
        emp3.AxtriaSalesIQTM__FirstName__c = 'Test1';
        emp3.AxtriaSalesIQTM__HR_Status__c = 'Inactive';
        emp3.DSI_Email_Id__c = 'dsi2@test.com';
        emp3.AxtriaSalesIQTM__USER__c = userinfo.getuserid();
        insert emp3;

        AxtriaSalesIQTM__Position_Employee__c postionEmp = new AxtriaSalesIQTM__Position_Employee__c();
        postionEmp.AxtriaSalesIQTM__Effective_End_Date__c = system.today().AddDays(5);
        System.debug(system.today());
        System.debug(postionEmp.AxtriaSalesIQTM__Effective_End_Date__c);
        postionEmp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today().AddDays(-5);
        System.debug(postionEmp.AxtriaSalesIQTM__Effective_Start_Date__c);
        postionEmp.AxtriaSalesIQTM__Employee__c= emp2.id;
        postionEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary' ;
        postionEmp.AxtriaSalesIQTM__Status__c = 'Approved';
        postionEmp.AxtriaSalesIQTM__Position__c =  pos.id;
        insert postionEmp;

        AxtriaSalesIQTM__Position_Employee__c postionEmp1 = new AxtriaSalesIQTM__Position_Employee__c();
        postionEmp1.AxtriaSalesIQTM__Effective_End_Date__c = system.today().AddDays(-1);
        System.debug(system.today());
        System.debug(postionEmp.AxtriaSalesIQTM__Effective_End_Date__c);
        postionEmp1.AxtriaSalesIQTM__Effective_Start_Date__c = system.today().AddDays(-5);
        System.debug(postionEmp.AxtriaSalesIQTM__Effective_Start_Date__c);
        postionEmp1.AxtriaSalesIQTM__Employee__c= emp2.id;
        postionEmp1.AxtriaSalesIQTM__Assignment_Type__c = 'Secondary' ;
        postionEmp1.AxtriaSalesIQTM__Status__c = 'Approved';
        postionEmp1.AxtriaSalesIQTM__Position__c =  pos1.id;
        insert postionEmp1;

         AxtriaSalesIQTM__Position_Employee__c postionEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
         postionEmp2.AxtriaSalesIQTM__Effective_End_Date__c = system.today().AddDays(-4);
         //System.debug(system.today());
         //System.debug(postionEmp.AxtriaSalesIQTM__Effective_End_Date__c);
         postionEmp2.AxtriaSalesIQTM__Effective_Start_Date__c = system.today().AddDays(-5);
         //System.debug(postionEmp.AxtriaSalesIQTM__Effective_Start_Date__c);
         postionEmp2.AxtriaSalesIQTM__Employee__c= emp2.id;
         postionEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Primary' ;
         postionEmp2.AxtriaSalesIQTM__Status__c = 'Approved';
         postionEmp2.AxtriaSalesIQTM__Position__c =  pos1.id;
         insert postionEmp2;

         AxtriaSalesIQTM__Position_Employee__c postionEmp3 = new AxtriaSalesIQTM__Position_Employee__c();
         postionEmp3.AxtriaSalesIQTM__Effective_End_Date__c = system.today().AddDays(5);
         System.debug(system.today());
         System.debug(postionEmp.AxtriaSalesIQTM__Effective_End_Date__c);
         postionEmp3.AxtriaSalesIQTM__Effective_Start_Date__c = system.today().AddDays(-5);
         System.debug(postionEmp.AxtriaSalesIQTM__Effective_Start_Date__c);
         postionEmp3.AxtriaSalesIQTM__Employee__c= emp3.id;
         postionEmp3.AxtriaSalesIQTM__Assignment_Type__c = 'Primary' ;
         postionEmp3.AxtriaSalesIQTM__Status__c = 'Approved';
         postionEmp3.AxtriaSalesIQTM__Position__c =  pos1.id;
         insert postionEmp3;
         


         BatchInactiveUserPosition updateUserPos4 =new BatchInactiveUserPosition ();
         Database.executeBatch(updateUserPos4, 200); 
         BatchInactiveUserPosition upi= new BatchInactiveUserPosition ();
         upi.dummyFunction();


         System.assert(postionEmp3 != null, 'Record found');
    }
}