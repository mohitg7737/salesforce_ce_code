@isTest
private class ST_Utility_Test {
    static testMethod void testMethod1() {

        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.Name = 'TI';
        insert teamIns;

        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        acc.AccountNumber ='BH123456';
        acc.AxtriaSalesIQST__Marketing_Code__c='ES';
        acc.Type = 'HCO';
        insert acc;

        AxtriaSalesIQST__Source_to_Destination_Mapping__c sToD= new AxtriaSalesIQST__Source_to_Destination_Mapping__c();
        sToD.AxtriaSalesIQST__Source_Object_Field__c='source_field__c';
        sToD.AxtriaSalesIQST__Destination_Object_Field__c = 'dest_field__c';
        sToD.AxtriaSalesIQST__Team_Instance__c = teamIns.id;
        sToD.AxtriaSalesIQST__Load_Type__c  = '1';
        insert sToD;

        Set<String> fields_in_query_set = new Set<String>();
        Map<String, List<String>> sourceToDestMapping = new  Map<String, List<String>>();
        sourceToDestMapping.put('temp-abc', new List<String>{'temp1','temp2'});
        sourceToDestMapping.put('tem-abc', new List<String>{'tem1','tem2'});
        sourceToDestMapping.put('tempX-abc', new List<String>{'tempX1','tempX2'});
        //call functions
        ST_Utility.fetchFieldsUsedInSOQL('select id, accountnumber from Account');
        ST_Utility.fetchSourceFieldsInSourceToDestinationObject(sourceToDestMapping,fields_in_query_set);
        ST_Utility.fetchSourceToDestinationMapping('1',new list<String>{teamIns.id});
        ST_Utility.getAPIName('setAbc ',' where temp__c IN :setAbc');
        Map<String, Set<String>> mapAccProd = new Map<String, Set<String>>();
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos2.Name = 'Chico CA_SPEC';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c='5555' ;
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='5555' ;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c= '1';
        pos2.AxtriaSalesIQTM__RGB__c='45,210,117';
        pos2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        pos2.AxtriaSalesIQTM__Team_iD__c  = team.id;
        //pos2.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C= '1';
        insert pos2;
        
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        //posAcc.isAccountTarget__c = true;
        posAcc.AxtriaSalesIQTM__Position__c = pos2.id;
        posAcc.AxtriaSalesIQST__p1__c = 'CRC';
        posAcc.AxtriaSalesIQST__p2__c = 'CRC';
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        posAcc.AxtriaSalesIQTM__Account__c = acc.id;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
        //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        posAcc.AxtriaSalesIQTM__Picklist1_Updated__c = '12';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
        posAcc.AxtriaSalesIQTM__Metric6__c = 4;
        posAcc.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
        //posAcc.Line__c= line.id;
        insert posAcc;

        mapAccProd.put(acc.AccountNumber,new set<string>{'CRC','CRC'});
        //call fetchRestrictedAccounts
        ST_Utility.fetchRestrictedAccounts('AxtriaSalesIQTM__Position_Account_Call_Plan__c', ' AxtriaSalesIQTM__Account__r.AccountNumber in :accNumSet and AxtriaSalesIQST__p1__c in :prodNameSet',teamIns.id,
                                            '1111', mapAccProd,new list<id> {posAcc.id});
        // call enabledisableTrigger    
        AxtriaSalesIQTM__TriggerContol__c triggerControl = new AxtriaSalesIQTM__TriggerContol__c(Name='ChangeRequestTriggerOnBeforeInsert', AxtriaSalesIQTM__IsStopTrigger__c=true);
        insert triggerControl;                    
        ST_Utility.enabledisableTrigger(new Map<String, Boolean>{'ChangeRequestTriggerOnBeforeInsert'=>false});
        
        //call updateRuleStatus
        AxtriaSalesIQST__Measure_Master__c mm = new AxtriaSalesIQST__Measure_Master__c();
        insert mm;
        ST_Utility.updateRuleStatus(mm.id,'Ready','Page not found','not found',true);
        
        //call isNumericString
        ST_Utility.isNumericString('sssss');
        
        //call fetchNamespace
        //ST_Utility.fetchNamespace('NewPhysicianUniverseCtrl');
    }
}