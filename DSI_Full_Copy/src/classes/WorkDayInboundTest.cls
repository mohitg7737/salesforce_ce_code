@isTest
private class WorkDayInboundTest {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';
    static testMethod void validate_WorkDayInbound(){
    
    AxtriaSalesIQTM__SalesIQ_Logger__c DS = New AxtriaSalesIQTM__SalesIQ_Logger__c();
    DS.AxtriaSalesIQTM__Status__c = 'In Progress';
    DS.AxtriaSalesIQTM__Module__c = 'WorkDayInbound';
    DS.AxtriaSalesIQTM__Type__c = 'WorkDayInbound';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'WorkDayInbound';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'WorkDayInbound';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__BDT_DataSet_Id__c = 'EUfull';
    ETL.AxtriaSalesIQTM__BR_PG_Database__c= 'EU_IB_OB';
    ETL.AxtriaSalesIQTM__BR_PG_Host__c= 'localhost';
    Insert ETL;    
    
        
   WorkDayInbound.getBRMSConfigValues('WorkDayInbound');
   WorkDayInbound.getETLConfigByName('WorkDayInbound');
   WorkDayInbound.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('WorkDayInboundTest',CRON_EXP,new WorkDayInbound());

Test.StopTest(); 
    }
}