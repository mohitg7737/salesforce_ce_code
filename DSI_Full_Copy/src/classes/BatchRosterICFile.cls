global class BatchRosterICFile implements Database.Batchable<sObject> {
    public String query;

    global BatchRosterICFile() {
        delete[select id from Roster__c];
        query = 'Select id, Termination_Date__c, Action_Type__c,Reason_for_Action_Name__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Current_Territory__r.Name, Employment_Status__c, Reason_Code__c, Personnel_Area__c, AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee_ID__c, Personnel_Subarea_Code__c, Territory__c, Personnel_Subarea_Name__c, Personnel_Number__c, Personnel_Area_Number__c,DSI_Email_Id__c, IC_Status__c, Territory_Name__c, AxtriaSalesIQTM__Current_Territory__r.SalesforceName__c, Payroll_Id__c, AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c  from AxtriaSalesIQTM__Employee__c where Termination_Date__c = null';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Employee__c> scopeEmployees) {
        List<Roster__c> RosterInsertionList = new List<Roster__c>();
        for(AxtriaSalesIQTM__Employee__c emp : scopeEmployees){
            Roster__c ros = new Roster__c();
            ros.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c; 
            ros.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            ros.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            ros.email__c = emp.DSI_Email_Id__c;
            ros.status__c = 'Active';
            ros.salesforce__c = emp.AxtriaSalesIQTM__Current_Territory__r.SalesforceName__c;
            ros.sales_area_name__c = emp.AxtriaSalesIQTM__Current_Territory__r.Name;
            ros.sales_area_level__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c;
            ros.sales_area__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c;
            ros.payroll_id__c = emp.Payroll_Id__c;
            ros.date__c = Date.Today();

            RosterInsertionList.add(ros);
        }
        if(RosterInsertionList.size()>0){
            insert RosterInsertionList;
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}