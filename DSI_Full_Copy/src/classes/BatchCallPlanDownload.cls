global with sharing class BatchCallPlanDownload implements Database.Batchable<sObject>, Database.Stateful{
    /*************Replaced Brand_Team_Instance with Product_catalog due to object purge activity A1422*****************/
    public String query;
    public String positionId;
    public map<String, String> parameterAPIName;
    public list<String> parameters;
    public AxtriaSalesIQTM__Position__c position;
    global String results;
    public String localeSeperator;
    global BatchCallPlanDownload(String positionId, String seperator){
        localeSeperator = seperator;
        results = '';
        position = [SELECT Id, AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Team_Instance__c FROM AxtriaSalesIQTM__Position__c WHERE Id =: positionId];
        list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> teamInstances = [SELECT Id, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c FROM AxtriaSalesIQTM__Team_Instance_Object_Attribute__c WHERE AxtriaSalesIQTM__Team_Instance__c =: position.AxtriaSalesIQTM__Team_Instance__c /*AND Product_Catalog__c != null*/ AND AxtriaSalesIQTM__isEnabled__c = true AND AxtriaSalesIQTM__isRequired__c = true AND AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' and AxtriaSalesIQST__cust_Type__c =null WITH SECURITY_ENFORCED ORDER BY AxtriaSalesIQTM__Display_Column_Order__c ASC];
        parameters = new list<String>();
        parameterAPIName = new map<String, String>();
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ta: teamInstances){
            parameters.add(ta.AxtriaSalesIQTM__Attribute_Display_Name__c);
            parameterAPIName.put(ta.AxtriaSalesIQTM__Attribute_Display_Name__c, ta.AxtriaSalesIQTM__Attribute_API_Name__c);
        }
        
        results  = createCSVHeader(parameters);
        results += '\n';
        String paramApi = String.join(parameterAPIName.values(), ',');
        SnTDMLSecurityUtil.printDebugMessage('========paramApi:::'+paramApi);
        if(paramApi !=null && paramApi!=''){
            paramApi =paramApi+',';
        }
        else{
            paramApi='';
        }
        paramApi = paramApi.removeEnd(',');
        SnTDMLSecurityUtil.printDebugMessage('===New=====paramApi:::'+paramApi);

        
        query =  'select '+ paramApi +'  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Position__c = \''+positionId+'\' and AxtriaSalesIQTM__isIncludedCallPlan__c = true';
        SnTDMLSecurityUtil.printDebugMessage('++++++++ Query');
        SnTDMLSecurityUtil.printDebugMessage(query);
    }
    
    /*@Param customerType Added by A1942 for SNT-69 (Customer type based call plan)*/
    Public BatchCallPlanDownload(String positionId, String seperator, String customerType){
        localeSeperator = seperator;
        results = '';
        position = [SELECT Id, AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Team_Instance__c FROM AxtriaSalesIQTM__Position__c WHERE Id =: positionId];
        list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> teamInstances = new  list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
        teamInstances = [SELECT Id, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c FROM AxtriaSalesIQTM__Team_Instance_Object_Attribute__c WHERE AxtriaSalesIQTM__Team_Instance__c =: position.AxtriaSalesIQTM__Team_Instance__c AND AxtriaSalesIQTM__isEnabled__c = true AND AxtriaSalesIQTM__isRequired__c = true AND AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' AND AxtriaSalesIQST__cust_Type__c =:customerType WITH SECURITY_ENFORCED ORDER BY AxtriaSalesIQTM__Display_Column_Order__c ASC];
        parameters = new list<String>();
        parameterAPIName = new map<String, String>();
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ta: teamInstances){
          if(ta.AxtriaSalesIQTM__Attribute_API_Name__c!='AxtriaSalesIQTM__Checkbox3__c'){ //-- Added by RT for STIMPS-428 to remove select all first col from file ---
              parameters.add(ta.AxtriaSalesIQTM__Attribute_Display_Name__c);
              parameterAPIName.put(ta.AxtriaSalesIQTM__Attribute_Display_Name__c, ta.AxtriaSalesIQTM__Attribute_API_Name__c);
          }
        }
        
        results  = createCSVHeader(parameters);
        results += '\n';
        String paramApi = String.join(parameterAPIName.values(), ',');
        SnTDMLSecurityUtil.printDebugMessage('========paramApi:::'+paramApi);
        if(paramApi !=null && paramApi!=''){
            paramApi =paramApi+',';
        }
        else{
            paramApi='';
        }
        paramApi = paramApi.removeEnd(',');
        SnTDMLSecurityUtil.printDebugMessage('===New=====paramApi:::'+paramApi);

        if(!String.isBlank(customerType)){
          query =  'select '+ paramApi +'  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Position__c = \''+positionId+'\' and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Account__r.Type = \'' + customerType + '\' ';
        }else{
          query =  'select '+ paramApi +'  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Position__c = \''+positionId+'\' and AxtriaSalesIQTM__isIncludedCallPlan__c = true';
        }
        SnTDMLSecurityUtil.printDebugMessage('++++++++ Query :: '+ query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('============Query is::::'+query);
        return Database.getQueryLocator(query);
    }
    
  
    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        /*map<String, list<sObject>> accountCallPlan = new map<String, list<sObject>>();
        //map<String, sObject> accountCallPlan = new map<String,sObject>();
        list<String> sharedAccounts = new list<String>();
        try{
           
        
           for(sObject callplan : scope)
           {
               String key=(String)callplan.get('AxtriaSalesIQTM__Account__c');
               if(!accountCallPlan.containsKey(key))
               {
                   accountCallPlan.put((String)callplan.get('AxtriaSalesIQTM__Account__c'), new list<sobject>{callplan});
               }
               else
               {
                   accountCallPlan.get(key).add(callplan);
               }
               
               if((Boolean)callplan.get('share__c') == true){
                   sharedAccounts.add((String)callplan.get('AxtriaSalesIQTM__Account__c'));
               }
           }
        }
        catch(Exception ex)
        {
           SnTDMLSecurityUtil.printDebugMessage('Shared Flag not configured');
        }*/
        /*if(sharedAccounts != null && sharedAccounts.size()>0)
        {
            list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedCallPlans = [SELECT Id, AxtriaSalesIQTM__Account__c, Final_TCF__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Account__c IN:sharedAccounts];
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c cp: sharedCallPlans)
            {
                sObject callplan = accountCallPlan.get(cp.AxtriaSalesIQTM__Account__c);
                Decimal totalCalls = (Decimal)callplan.get('AllCall__c') != null ? (Decimal)callplan.get('AllCall__c') : 0;
                totalCalls += cp.Final_TCF__c != null ? cp.Final_TCF__c : 0;
                callplan.put('AllCall__c', totalCalls);
                accountCallPlan.put(cp.AxtriaSalesIQTM__Account__c, callplan);
            }
        }*/
        /*list<sobject>finalpacps = new list<sobject>();
        //finalpacps.add(accountCallPlan.values());
        for(String key1 : accountCallPlan.keyset())
        {
            for( Sobject s : accountCallPlan.get(key1))
            {
                finalpacps.add(s);
            }
        }*/
        SnTDMLSecurityUtil.printDebugMessage('===============finalpacps.size():::'+scope.size());
        for(sObject callplan: scope){
            results += createCSVRow(callplan);
            results += '\n';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        datetime myDateTime = datetime.now();
        String fileTitle = position.AxtriaSalesIQTM__Client_Territory_Code__c + '_' + myDateTime.format();
        ContentVersion file = new ContentVersion(title = fileTitle + '.csv', versionData =  Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + fileTitle + '.csv');
        insert file;
        
        SnTDMLSecurityUtil.printDebugMessage(file.ID);
        
        String subject = System.label.Email_Subject+' '+ position.AxtriaSalesIQTM__Client_Territory_Code__c; // --Added Label for STIMPS-428
        String body = system.label.Pacp_Hi +' '+ UserInfo.getName() +' \n \n'+ System.label.emailtext+'\n \n'+system.label.Territory_ID+' : '+ position.AxtriaSalesIQTM__Client_Territory_Code__c +'\n'+ system.label.PACP_Name+' : '+UserInfo.getUserName()+ '\n' + system.label.btn_Download  +' : '+myDateTime.format()+'\n\n'+system.label.PACP_Thanks + '\n' + system.label.SalesIQ_Support_Team ;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject( subject );
        email.setToAddresses(new list<String>{ UserInfo.getUserEmail() }); //
        email.setPlainTextBody( body );
        List<Messaging.EmailFileAttachment> attachments = ContentDocumentAsAttachement(new Id[]{file.Id});
        email.setFileAttachments(attachments);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
    
    public static List<Messaging.EmailFileAttachment> ContentDocumentAsAttachement(Id[] contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>{};
        List<ContentVersion> documents                  = new List<ContentVersion>{};
    
        documents.addAll([
          SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
          FROM ContentVersion
          WHERE Id IN :contentDocumentIds
        ]);
    
        for (ContentVersion document: documents) {
          Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
    
          attachment.setBody(document.VersionData);
          attachment.setFileName(document.Title);
    
          attachments.add(attachment);
        }
    
        return attachments;
    
    }
    
    private String createCSVRow(sObject callplan){
        String row = '';
        /*row += '\"'+(String)callplan.get('AxtriaSalesIQTM__Change_Status__c')  +'\"' + localeSeperator;
        row += '\"' +(String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Name')  +'\"'+ localeSeperator;
        row += '\"' +(String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('AxtriaSalesIQTM__External_Account_Number__c')  +'\"' + localeSeperator;
        row += '\"'+ (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('AxtriaSalesIQTM__Speciality__c') +'\"' + localeSeperator;
        row += '\"'+ (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('BillingCity') +'\"' + localeSeperator;
        row += '\"'+(String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Micro_Brick__c') +'\"' + localeSeperator;
        //row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Parent_Name__c') + localeSeperator;
        if(callplan.getsObject('AxtriaSalesIQTM__Account__r').get('ParentID')!=null && callplan.getsObject('AxtriaSalesIQTM__Account__r').getsObject('Parent').get('Name')!=null)
            row += '\"'+callplan.getsObject('AxtriaSalesIQTM__Account__r').getsObject('Parent').get('Name') +'\"' + localeSeperator;
        else
           row+= '\"'+'VACANT' + localeSeperator +'\"';
       
        row += '\"'+(String)callplan.get('P1__c') +'\"' + localeSeperator;
        row += '\"'+(String)callplan.get('Segment__c') +'\"' + localeSeperator;
        row += '\"'+Integer.valueOf((Decimal)callplan.get('Proposed_TCF__c') )+'\"' + localeSeperator;
        row +='\"'+ Integer.valueOf((Decimal)callplan.get('Final_TCF__c')) +'\"' + localeSeperator;
        row += '\"'+(String)callplan.get('Accessibility_Range__c') +'\"'+ localeSeperator;
        row += '\"'+(String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Profile_Consent__c') +'\"'+ localeSeperator;
        if(callplan.getsObject('AxtriaSalesIQTM__Position__r').get('AxtriaSalesIQTM__Employee__c') != null && callplan.getsObject('AxtriaSalesIQTM__Position__r').getsObject('AxtriaSalesIQTM__Employee__r').get('Name') !=null)
        row += '\"'+((String)callplan.getsObject('AxtriaSalesIQTM__Position__r').getsObject('AxtriaSalesIQTM__Employee__r').get('Name')).replace(',' , ' ') +'\"'+ localeSeperator;
        else
        row+= '\"'+'VACANT' +'\"'+ localeSeperator;
        row += '\"'+(Boolean)callplan.get('share__c') +'\"'+ localeSeperator;
        row += (Integer.valueOf((Decimal)callplan.get('AllCall__c')) != null && Integer.valueOf((Decimal)callplan.get('AllCall__c')) > 0 ? Integer.valueOf((Decimal)callplan.get('AllCall__c')) : Integer.valueOf((Decimal)callplan.get('Final_TCF__c'))) + localeSeperator;
        row += '\"'+(String)callplan.get('AxtriaSalesIQTM__Picklist3_Updated__c') +'\"'+ localeSeperator;*/
        for(String parameter: parameters){
           
           if(parameterAPIName.get(parameter).contains('.'))
           {
              String paramName = String.valueof(parameterAPIName.get(parameter));
              SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey pARAM nAME ' + paramName);
              List<String> allRecs = new List<String>(paramName.split('\\.'));
              List<String>correctedRecs = new List<String>();
              System.debug(allRecs);
              for(String str : allRecs)
              {
               //  if(str.contains('__r'))
               //  {
               //     str = str.substring(0,str.length() - 1);
               //     str = str+'c';
               //  }
                 correctedRecs.add(str);
              }
              System.debug(correctedRecs);
              SnTDMLSecurityUtil.printDebugMessage('+++++++++');
              System.debug(callplan);
              if(correctedRecs.size() > 2)
              {   
                SnTDMLSecurityUtil.printDebugMessage('++++ row in if of correctedRecs.size() > 2  bef '+ row);
                  if(callplan.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]) !=null){
                row += ''+'\"'+ (String.isNotBlank(String.valueof(callplan.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]).get(correctedRecs[2]))) ? String.valueof(callplan.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]).get(correctedRecs[2])):'-' )+'\"'+localeSeperator ; // Modified by RT for STIMPS-428
                    SnTDMLSecurityUtil.printDebugMessage('++++ Wuhu '+ row);
                  }
                  else
               row+= '-'+localeSeperator; // Modified by RT for STIMPS-428
                SnTDMLSecurityUtil.printDebugMessage('++++ row in if of correctedRecs.size() > 2  aft'+ row);
                  
              }
              else{
            row += ''+'\"'+(String.isNotBlank(String.valueof(callplan.getSObject(correctedRecs[0]).get(correctedRecs[1]))) ? String.valueof(callplan.getSObject(correctedRecs[0]).get(correctedRecs[1])):'-' )+'\"'+localeSeperator ; // Modified by RT for STIMPS-428
                //row += String.valueof(callplan.getSObject(correctedRecs[0]).get(correctedRecs[1]));
                SnTDMLSecurityUtil.printDebugMessage('++++ row in else of correctedRecs.size() > 2 '+ row);
              }
              
           }
           else
            row += ''+'\"'+(String.isNotBlank((String.valueof(callplan.get(parameterAPIName.get(parameter))))) ? (String.valueof(callplan.get(parameterAPIName.get(parameter)))).replace(',',' '): '-') +'\"'+localeSeperator; // Modified by RT for STIMPS-428
        }
        return row;
    }
    
    private string createCSVHeader(list<String> parameters){
        String headers = '';
        
        
        for(String parameter: parameters){
            try
            {
               Component.Apex.OutputText output = new Component.Apex.OutputText();
               output.expressions.value = '{!$Label.' + parameter + '}';
               
               headers += String.valueof(output.value);
               headers += localeSeperator ;
               
            }
            catch(Exception ex)
            {
               headers += parameter;
               headers += localeSeperator ;
            }
            
        }
        return headers;
    }
}