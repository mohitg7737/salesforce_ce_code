/************************************************************************************************
    Name        :   GeographyUtil.cls
    Description :   Utility class for Position hierarchy. 
************************************************************************************************/
public with sharing class GeographyUtilSnT {
    public static GeographyNodeWrapper createNode(Id objId, map<Id,AxtriaSalesIQTM__Position__c>allPositionMap, map<Id,list<AxtriaSalesIQTM__Position__c>>parentChildPositionMap) {
        GeographyNodeWrapper n = new GeographyNodeWrapper();
        n.myGeographyId = objId;
        n.myGeographyName = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Territory_Name__c;
        n.myGeographyTitle = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Territory_Name__c;

        n.myParentGeographyId = allPositionMap.get(objId).AxtriaSalesIQTM__Parent_Position__c;
        n.rgb = allPositionMap.get(objId).AxtriaSalesIQTM__RGB__c;
        n.TerritoryCode = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Position_Code__c;
        n.PositionType = allPositionMap.get(objId).AxtriaSalesIQTM__Position_Type__c;
        n.PositionLevel = allPositionMap.get(objId).AxtriaSalesIQTM__Hierarchy_Level__c;
        n.PositionCategory = allPositionMap.get(objId).AxtriaSalesIQTM__Position_Category__c;
        n.status =allPositionMap.get(objId).AxtriaSalesIQTM__Change_Status_del__c;
        n.PositionName= allPositionMap.get(objId).Name;
        n.TeamName = allPositionMap.get(objId).AxtriaSalesIQTM__Team_iD__r.Name;
        n.relatedPosType = allPositionMap.get(objId).AxtriaSalesIQTM__Related_Position_Type__c;
        
        if(parentChildPositionMap.containsKey(objId)){
            n.hasChildren = true;
            n.isLeafNode = false;
            List<GeographyNodeWrapper> lst = new List<GeographyNodeWrapper>();
            for(AxtriaSalesIQTM__Position__c r : parentChildPositionMap.get(objId)) {
                lst.add(createChildNode(r.Id,allPositionMap,parentChildPositionMap));
            }           
            n.myChildNodes = lst;
        }else{
            n.isLeafNode = true;
            n.hasChildren = false;
        }
        return n;
    }
    
    public static GeographyNodeWrapper createChildNode(Id objId, map<Id,AxtriaSalesIQTM__Position__c>allPositionMap, map<Id,list<AxtriaSalesIQTM__Position__c>>parentChildPositionMap) {
        GeographyNodeWrapper n = new GeographyNodeWrapper();
        n.myGeographyId = objId;
        n.myGeographyName = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Territory_Name__c;
        n.myGeographyTitle = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Territory_Name__c;
        n.myParentGeographyId = allPositionMap.get(objId).AxtriaSalesIQTM__Parent_Position__c;
        n.rgb = allPositionMap.get(objId).AxtriaSalesIQTM__RGB__c;
        n.TerritoryCode = allPositionMap.get(objId).AxtriaSalesIQTM__Client_Position_Code__c;
        n.PositionType = allPositionMap.get(objId).AxtriaSalesIQTM__Position_Type__c;
        n.PositionLevel = allPositionMap.get(objId).AxtriaSalesIQTM__Hierarchy_Level__c;
        n.PositionCategory = allPositionMap.get(objId).AxtriaSalesIQTM__Position_Category__c;
        n.status =allPositionMap.get(objId).AxtriaSalesIQTM__Change_Status_del__c;
        n.PositionName=allPositionMap.get(objId).Name;
        n.TeamName = allPositionMap.get(objId).AxtriaSalesIQTM__Team_iD__r.Name;
        n.relatedPosType = allPositionMap.get(objId).AxtriaSalesIQTM__Related_Position_Type__c;
        
        if(parentChildPositionMap.containsKey(objId)){
            n.hasChildren = true;
            n.isLeafNode = false;
            List<GeographyNodeWrapper> lst = new List<GeographyNodeWrapper>();
            for(AxtriaSalesIQTM__Position__c r : parentChildPositionMap.get(objId)) {
                lst.add(createChildNode(r.Id,allPositionMap,parentChildPositionMap));
            }           
            n.myChildNodes = lst;
        }
        else { 
            n.isLeafNode = true;
            n.hasChildren = false;
        }
        return n;
    }    
        
    public static JSONGenerator convertNodeToJSON(JSONGenerator gen, GeographyNodeWrapper objRNW, boolean isPrimary){
    	if(objRNW.TerritoryCode != null){
		if(objRNW.TerritoryCode.contains('<') || objRNW.TerritoryCode.contains('>'))
			objRNW.TerritoryCode = '1N000000';
    	}
        
        gen.writeStartObject();
        if(objRNW.myGeographyName!=null){
        	gen.writeStringField('title', objRNW.myGeographyTitle.replaceAll('<[^>]+>',' '));
        	gen.writeStringField('text', objRNW.myGeographyName.replaceAll('<[^>]+>',' '));
        }
        
        gen.writeStringField('key', objRNW.myGeographyId);
        
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('opened', true);
        gen.writeBooleanField('isFolder', true);
        gen.writeBooleanField('isPrimary', isPrimary);
        gen.writeFieldName('state');
        gen.writeStartObject();
        gen.writeObjectField('opened', true); 
        gen.writeEndObject();
        if(objRNW.extents != null){
            gen.writeStringField('extents', objRNW.extents);
        }else{
            gen.writeStringField('extents', '');
        }
        if(objRNW.TeamName != null){
            gen.writeStringField('TeamName', objRNW.TeamName.replaceAll('<[^>]+>',' '));
        }
        if(objRNW.rgb != null){
            gen.writeStringField('rgb', objRNW.rgb.replaceAll('<[^>]+>',' '));
        }
        if(objRNW.TerritoryCode != null){
            gen.writeStringField('TerritoryCode', objRNW.TerritoryCode.replaceAll('<[^>]+>',''));
            gen.writeStringField('id', objRNW.TerritoryCode.replaceAll('<[^>]+>',''));
            gen.writeStringField('icon','circle _'+ objRNW.TerritoryCode.replaceAll('<[^>]+>',''));
        }else{
            gen.writeStringField('id', '');
        }
        if(objRNW.DistrictCode != null){
            gen.writeStringField('DistrictCode', objRNW.DistrictCode.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('DistrictCode', '');
        }
        if(objRNW.AreaCode != null){
            gen.writeStringField('AreaCode', objRNW.AreaCode.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('AreaCode', '');
        }
        if(objRNW.RegionCode != null){
            gen.writeStringField('RegionCode', objRNW.RegionCode.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('RegionCode', '');
        }
        if(objRNW.PositionType != null){        
            gen.writeStringField('Type', objRNW.PositionType.replaceAll('<[^>]+>',' '));
        }
        if(objRNW.PositionLevel != null){        
            gen.writeStringField('Level', objRNW.PositionLevel.replaceAll('<[^>]+>',' '));
        }
        if(objRNW.PositionCategory != null){        
            gen.writeStringField('PositionCategory', objRNW.PositionCategory.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('PositionCategory', '');
        }
        if(objRNW.status != null){        
            gen.writeStringField('status', objRNW.status.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('status', '');
        }
         if(objRNW.PositionName != null){        
            gen.writeStringField('PositionName', objRNW.PositionName.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('PositionName', '');
        }
        
        if(objRNW.relatedPosType != null){        
            gen.writeStringField('RelatedPosType', objRNW.relatedPosType.replaceAll('<[^>]+>',' '));
        }else{
            gen.writeStringField('RelatedPosType', '');
        }
        
        if(objRNW.hasChildren){
            gen.writeFieldName('children');
            gen.writeStartArray();
            for(GeographyNodeWrapper r : objRNW.myChildNodes){
                convertNodeToJSON(gen,r,isPrimary);
            }
            gen.writeEndArray();
        }
        gen.writeEndObject();
        return gen;
    }
    
    public class GeographyNodeWrapper {
        public String myGeographyName {get; set;}
        public String myGeographyTitle{get;set;}
        public Id myGeographyId {get; set;}
        public String myParentGeographyId {get; set;}
        public string extents {get; set;}
        public string rgb {get; set;}
        public string TerritoryCode {get; set;}
        public string DistrictCode {get; set;}
        public string AreaCode {get; set;}
        public string RegionCode {get; set;}
        public string PositionType {get; set;}
        public string PositionLevel {get; set;}
        public string PositionCategory{get;set;}
        public string status{get;set;}
        public string TeamName {get; set;}
        public string PositionName{get;set;}
        public Boolean isChangeRequestPending {get;set;}
        public string relatedPosType {get;set;}
        
        // Node children identifier properties - begin
        public Boolean hasChildren {get; set;}
        public Boolean isLeafNode {get; set;}
        // Node children identifier properties - end
        
        // Node children properties - begin
        public List<GeographyNodeWrapper> myChildNodes {get; set;}
        // Node children properties - end   
        
        public GeographyNodeWrapper(){
            hasChildren = false;
            isLeafNode = false;
        }
    }
}