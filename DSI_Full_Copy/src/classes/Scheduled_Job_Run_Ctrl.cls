public class Scheduled_Job_Run_Ctrl {

    public List<SelectOption>                                           jobList             {get;set;}
    public String                                                       selectedJobType     {get;set;}
    public String                                                       selectedJobName     {get;set;}
    public Boolean                                                      pollerBool          {get;set;}
    public List<AxtriaSalesIQST__Scheduler_Log__c>                      scLogList           {get;set;}

    public Scheduled_Job_Run_Ctrl() {
        jobList = new List<SelectOption>();
        jobList.add(new SelectOption('None','--None--'));
        checkStatus();
    }

    public void checkStatus()
    {
        scLogList = new List<AxtriaSalesIQST__Scheduler_Log__c>();
        scLogList = [Select AxtriaSalesIQST__Job_Type__c,AxtriaSalesIQST__Job_Name__c,AxtriaSalesIQST__Job_Status__c,AxtriaSalesIQST__Created_Date2__c,CreatedBy.Name,Url__c from AxtriaSalesIQST__Scheduler_Log__c order by AxtriaSalesIQST__Created_Date2__c desc LIMIT 10];
        pollerBool = false;
        for(Integer i=0,j=scLogList.size();i<j;i++)
        {

            pollerBool = true;
            break;
        }
    }

    public void jobNameRefresh()
    {
        jobList = new List<SelectOption>();
        jobList.add(new SelectOption('None','--None--'));
        if(selectedJobType == 'Inbound')
        {   
            jobList.add(new SelectOption('WorkDay Employee Inbound','WorkDay Employee Inbound'));
            jobList.add(new SelectOption('Open Position Inbound','Open Position Inbound'));
            jobList.add(new SelectOption('Veeva Network Inbound','Veeva Network Inbound'));
            jobList.add(new SelectOption('SalesIQ Self Align Process','SalesIQ Self Align Process'));
            jobList.add(new SelectOption('IC Extracts','IC Extracts'));
            jobList.add(new SelectOption('Callplan Universe Extracts','Callplan Universe Extracts'));
            //jobList.add(new SelectOption('BatchMerge','BatchMerge'));
            //jobList.add(new SelectOption('Master Data SFDC Push','Master Data SFDC Push'));
        }
        else if(selectedJobType == 'Outbound')
        {
            jobList.add(new SelectOption('WorkDay Employee Outbound','WorkDay Employee Outbound'));
            jobList.add(new SelectOption('CRM Veeva Roster Outbound','CRM Veeva Roster Outbound'));
            jobList.add(new SelectOption('CRM Veeva ATL Outbpund','CRM Veeva ATL Outbpund'));
            jobList.add(new SelectOption('Azure Outbound','Azure Outbound'));
        }
        else if(selectedJobType == 'Operations')
        {
            jobList.add(new SelectOption('Sceanrio Delta Execution','Sceanrio Delta Execution'));
            jobList.add(new SelectOption('Master Sync Execution','Master Sync Execution'));
            jobList.add(new SelectOption('DSI Position Copy Batch','DSI Position Copy Batch'));
            jobList.add(new SelectOption('History Position Update Batch','History Position Update Batch'));
            jobList.add(new SelectOption('Batch Delete Future Explicit Assignments','Batch Delete Future Explicit Assignments'));
            jobList.add(new SelectOption('Geohraphy Is Master Validation','Geohraphy Is Master Validation'));
            jobList.add(new SelectOption('User Position Creaetion Batch','User Position Creaetion Batch'));              
        }
    }

    public void runJob()
    {
        if(selectedJobName != 'None' && selectedJobName != 'Geohraphy Is Master Validation')
        {
            
            IntegrationScheduledClass a = new IntegrationScheduledClass(selectedJobType,selectedJobName);
            IntegrationScheduledClass.RunScheduledJobsSNN(selectedJobType,selectedJobName);
            checkStatus();
        }
        if(selectedJobName == 'Geohraphy Is Master Validation'){

            callpythonService(selectedJobType,selectedJobName);
            checkStatus();
        }
    }

        public static Boolean isSandboxOrg() {
        List<AxtriaSalesIQTM__PackageExtension__c> sandbox = [select Id from AxtriaSalesIQTM__PackageExtension__c where Name like 'sandbox'];
        if(sandbox.isEmpty())
            return false;
        else
            return true;
    }
    public static AxtriaSalesIQTM__ETL_Config__c getETLConfigByName(string etlConfigName){
        AxtriaSalesIQTM__ETL_Config__c etlConfigList;
        etlConfigList = [SELECT AxtriaSalesIQTM__SF_UserName__c, AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c FROM AxtriaSalesIQTM__ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }

    @future(callout=true)
    public static void  callpythonService(String jobType,String jobName){
        System.debug('--jobType--' + jobType);
        System.debug('--jobName--' + jobName);
        AxtriaSalesIQST__Scheduler_Log__c scLog = new AxtriaSalesIQST__Scheduler_Log__c();
        scLog.AxtriaSalesIQST__Created_Date__c = System.today();
        scLog.AxtriaSalesIQST__Created_Date2__c = System.now();
        scLog.AxtriaSalesIQST__Job_Type__c = jobType;
        scLog.AxtriaSalesIQST__Job_Name__c = jobName;
        scLog.AxtriaSalesIQST__Job_Status__c = 'Job Submitted';
        insert scLog;
        
        string pythonService = 'https://dsi-arc.axtria.com/dsi/ismasterstatus';
        //SSL CHANGES
        Map<String,String> urlParams = new Map<String,String>();
        urlParams.put('serviceUrl',pythonService);
        string ETL_BRMS_Instance = 'ScenarioOperations';
        AxtriaSalesIQTM__ETL_Config__c etlObj = getETLConfigByName(ETL_BRMS_Instance);
        String sfUserName = etlObj.AxtriaSalesIQTM__SF_UserName__c;
        String sfPassword = etlObj.AxtriaSalesIQTM__SF_Password__c;
        String sfToken = etlObj.AxtriaSalesIQTM__S3_Security_Token__c;
        
        try{

            String isSandbox = ( isSandboxOrg() ) ? 'True' : 'False';
            String url = pythonService+'?user_name='+sfUserName+'&password='+sfPassword+'&security_token='+sfToken+'&sandbox='+isSandbox +'&scLogId=' + scLog.Id;
        
            /*urlParams.put('user_name',sfUserName);
            urlParams.put('password',sfPassword);
            urlParams.put('security_token',sfToken);
            urlParams.put('namespace','');
            urlParams.put('sandbox',isSandbox);


            HttpResponse response = getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'GET',120000);

            system.debug('#### Python Response : '+response);
            if(response.getStatusCode() != 200) {
                system.debug('Error in master sync : '+response.getStatus());
            }*/

            scLog.Url__c = url;
            update scLog;
        }
        catch(exception ex){
            system.debug('Error in master sync : '+ex.getMessage());
        }
    }
    public static HttpResponse getHTTPResponse(String endPoint, Map<String,String> mapParams, String requestMethod, Integer timeOut ){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint(endPoint);
        String body='';
        for(String key : mapParams.keySet()){
            body+= key+'='+String.valueof(mapParams.get(key)) + '&';
        }            
        request.setMethod(requestMethod);
        request.setTimeout(timeOut);
        request.setBody(body.removeEnd('&'));  
        HttpResponse reponse = h.send(request);
        return reponse;
    }
    
}