@isTest
public with sharing class TestUtilCustom {
    
    public static Profile profileSystemAdmin = new Profile();
    public static PermissionSet permissionSetForHO = new PermissionSet();

    static {
        profileSystemAdmin =[select id from profile where name='System Administrator'];
        permissionSetForHO = [SELECT Id FROM PermissionSet WHERE Name = 'IC_HO'];
        
        list<SIQIC__TriggerONOFF__c> triggOnOff = new list<SIQIC__TriggerONOFF__c> ();
    
        triggOnOff.add(new SIQIC__TriggerONOFF__c(Name='UserActive',SIQIC__Activate__c = True,SIQIC__Migration_Trigger__c=False));
        triggOnOff.add(new SIQIC__TriggerONOFF__c(Name='BURoleMapping',SIQIC__Activate__c = False,SIQIC__Migration_Trigger__c=False));

        insert triggOnOff;
        
        
    }
    public static SIQIC__R_Calender__c createTestCalender(boolean isInsert,String name,String countryCode,boolean isActive,Integer level){
            SIQIC__R_Calender__c calender = new SIQIC__R_Calender__c();
            calender.name = name;
            calender.SIQIC__Country_Code__c = countryCode;
            calender.SIQIC__IsActive__c = isActive;
            calender.SIQIC__Level__c = level;
            
            if(isInsert){
            insert calender;

                
            }
                return calender;
    }
    
     public static Account  createTestTenant(boolean isInsert, string name ,Id calenderId){
          Account  ten = new Account ();
          ten.name = name;
          ten.SIQIC__Default_CalenderId__c = calenderId;
          ten.SIQIC__bucketURL__c='testbucket';
          ten.SIQIC__payroll_dept_email__c='compmax@axtria.com';
          ten.AxtriaSalesIQST__Marketing_Code__c = 'US';
        
           if(isInsert){
           insert ten ;
        
                   
                }
        return ten ;
    }
    
 }