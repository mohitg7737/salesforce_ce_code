@isTest
private class OpenPositionInboundTest {
public static String CRON_EXP = '0 0 0 28 9 ? 2022';
    static testMethod void validate_OpenPositionInbound(){
    
    AxtriaSalesIQTM__SalesIQ_Logger__c DS = New AxtriaSalesIQTM__SalesIQ_Logger__c();
    DS.AxtriaSalesIQTM__Status__c = 'In Progress';
    DS.AxtriaSalesIQTM__Module__c = 'OpenPositionInbound';
    DS.AxtriaSalesIQTM__Type__c = 'OpenPositionInbound';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'OpenPositionInbound';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'OpenPositionInbound';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__BDT_DataSet_Id__c = 'EUfull';
    ETL.AxtriaSalesIQTM__BR_PG_Database__c= 'EU_IB_OB';
    ETL.AxtriaSalesIQTM__BR_PG_Host__c= 'localhost';
    Insert ETL;    
    
        
    OpenPositionInbound.getBRMSConfigValues('OpenPositionInbound');
    OpenPositionInbound.getETLConfigByName('OpenPositionInbound');
    OpenPositionInbound.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('OpenPositionInboundTest',CRON_EXP,new OpenPositionInbound());

Test.StopTest(); 
    }
}