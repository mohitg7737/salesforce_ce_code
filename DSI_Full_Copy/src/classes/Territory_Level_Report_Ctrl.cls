public class Territory_Level_Report_Ctrl
{

	public Map<String, List<WrapClass>> summaryMap  {get; set;}
	public String selectedTeamInstance              {get; set;}

	public Territory_Level_Report_Ctrl()
	{
		summaryMap = new Map<String, List<WrapClass>>();

		//To get the team Instance id for which report would be run
		selectedTeamInstance = ApexPages.currentPage().getParameters().get('selectedTeamInstance');

		// Fetching the user position record for the selected Team instance
		List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData = [SELECT AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name, AxtriaSalesIQTM__User__r.Profile.Name FROM AxtriaSalesIQTM__User_Access_Permission__c WHERE AxtriaSalesIQTM__User__c = :UserInfo.getUserId() AND AxtriaSalesIQTM__Is_Active__c = True AND AxtriaSalesIQST__isCallPlanEnabled__c = True AND AxtriaSalesIQTM__Team_Instance__c = : selectedTeamInstance ORDER BY AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c ASC];

		if(loggedInUserData.size() > 0)
		{
			String selectedPosition = loggedInUserData[0].AxtriaSalesIQTM__Position__c;
			System.debug('selectedPosition --> ' + selectedPosition);
			System.debug('selectedTeamInstance --> ' + selectedTeamInstance);
			//Aggregating the data for the report
			List<AggregateResult> aggResList = [SELECT AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c distId, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name distName, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c terrId, AxtriaSalesIQTM__Position__r.Name terrName, sum(isAdded__c) addedPhy, sum(isDropped__c) droppedPhy, sum(isRecordChanged__c) totalChanges, sum(isHighValueTarget__c) highValueTarget  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance AND (AxtriaSalesIQTM__Position__c = :selectedPosition OR AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition OR AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition OR AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition OR AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition) GROUP BY AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__r.Name LIMIT 45000];

			List<WrapClass> tempList; 

			for(Integer i = 0, j = aggResList.size(); i < j; i++)
			{
				String distId = (String)aggResList[i].get('distId') + ' - ' + (String)aggResList[i].get('distName');
				String terrId = (String)aggResList[i].get('terrId') + ' - ' + (String)aggResList[i].get('terrName');
				Integer addedPhy = Integer.valueOf(aggResList[i].get('addedPhy'));
				Integer droppedPhy = Integer.valueOf(aggResList[i].get('droppedPhy'));
				Integer totalChanges = Integer.valueOf(aggResList[i].get('totalChanges'));
				Integer highValueTarget = Integer.valueOf(aggResList[i].get('highValueTarget'));

				tempList = new List<WrapClass>();
				if(summaryMap.containsKey(distId))
				{
					tempList = summaryMap.get(distId);
				}

				WrapClass wrap = new WrapClass();
				wrap.terrID = terrId;
				wrap.addedPhy = addedPhy;
				wrap.droppedPhy = droppedPhy;
				wrap.totalChanges = totalChanges;
				wrap.highValueTarget = highValueTarget;

				tempList.add(wrap);
				summaryMap.put(distId,tempList);
			}

		}
	}

	public class Wrapclass
	{
		public String terrID                    	{get; set;}
		public Integer addedPhy                     {get; set;}
		public Integer droppedPhy                   {get; set;}
		public Integer totalChanges 		        {get; set;}
		public Integer highValueTarget  	        {get; set;}
		
	}
}