import xml.etree.ElementTree as ET
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
# path = os.path.abspath(script_dir + "\\azOrg\\classes/")  + "\\"
#orgPath = os.path.abspath(script_dir + "\\azOrg\\")

# for root, dirList, fileList in os.walk(script_dir):
# 	print dirList
# 	for dir in dirList:
# 		for file in fileList:
# 			if not file.endswith('-meta.xml'): continue

# 			print dir + file
# 			ET.register_namespace('','http://soap.sforce.com/2006/04/metadata')
# 			tree = ET.parse(dir + file)

# 			root = tree.getroot()
# 			for child in root:
# 				if 'packageVersion' in child.tag:
# 					root.remove(child)
# 			tree.write(dir + file, encoding="utf-8", xml_declaration=True)

# path = 'D:\Deployments\\azOrg\classes\\'

dirList = ['classes','pages','components','triggers']

for dir in dirList:
	path = script_dir +'\\' + dir + '\\'
	if os.path.isdir(path):
		for filename in os.listdir(path):
			print path + filename
			if not filename.endswith('-meta.xml'): continue

			ET.register_namespace('','http://soap.sforce.com/2006/04/metadata')
			tree = ET.parse(path + filename)

			root = tree.getroot()
			for child in root:
				if 'packageVersion' in child.tag:
					root.remove(child)
				elif 'packageVersions' in child.tag:
					root.remove(child)
				if 'apiVersion' in child.tag:
					child.text = '41.0'
					#root.remove(child)
			tree.write(path + filename, encoding="utf-8", xml_declaration=True)