@isTest
public class ChangeRequestCallPlanInlineCtlr2_Test {
    @istest static void ChangeRequestCallPlanInlineCtlr2_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        insert om;
        
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        insert c;
        
        Grid_Master__c g=new Grid_Master__c();
        g.name='test';
        g.Country__c=c.id;
        g.CurrencyIsoCode='USD';
        insert g;
        
        Test.startTest();
         System.runAs(loggedinuser){
            PageReference pageRef = Page.ChangeRequestInline2;
            Test.setCurrentPage(pageRef);
            //AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
            //insert cr;
            //pageRef.getParameters().put('Id', String.valueOf(cr.Id));
            //ApexPages.StandardController sc = new ApexPages.standardController(g);       
            //ChangeRequestCallPlanInlineCtlr2 obj=new ChangeRequestCallPlanInlineCtlr2(sc);
            //obj.cimConfig();
         }
        Test.stopTest();
    }
}