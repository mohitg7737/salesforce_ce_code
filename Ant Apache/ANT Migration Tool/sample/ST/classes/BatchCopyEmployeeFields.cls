/**********************************************************************************************
@author     : Dhruv Mahajan
@date       : 10 July'2017
@description: This Batch Class is used for updating all fields in employee object(AxtriaSalesIQTM__Employee__c) from Employee Staging
              (CurrentPersonFeed__c) data.
@Revison(s) :
**********************************************************************************************/
global class BatchCopyEmployeeFields implements Database.Batchable<sObject>, Database.stateful{
    
    global string query;
    public String batchID;
    global list<string> empIDList = new list<string>(); //List to store employee IDs of all employees.
    global Map<string,AxtriaSalesIQTM__Employee__c> empMap       = new Map<string,AxtriaSalesIQTM__Employee__c>(); //Map with key as Employee ID and value as Employee
    global set<string> fieldEmpDepartmentSet = new set<string>();
    global map<string,list<string>> mapId2FieldEmpDetails = new map<string,list<string>>();
    global map<string,Date> mapEmp2EventTypeDate = new map<string,Date>();
    global map<string,string> mapCountryName2SFID = new map<string,string>();
    
    global BatchCopyEmployeeFields(String ID){
      batchID=Id;
 
    }
    
    global BatchCopyEmployeeFields(String ID,map<string,Date> mapEmp2Date){
        batchID=Id; 
        mapEmp2EventTypeDate = mapEmp2Date;
        //Create a country map
        list<AxtriaSalesIQTM__Country__c> lsCountry = [select id,name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        for(AxtriaSalesIQTM__Country__c con: lsCountry){
            mapCountryName2SFID.put(con.AxtriaSalesIQTM__Country_Code__c,con.id);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        DateTime firstCreatedInTheBatch;
        DateTime lastCreatedInTheBatch;
        firstCreatedInTheBatch = system.now();
        lastCreatedInTheBatch = system.now();
       for(AxtriaSalesIQTM__Employee__c emp : Database.query('select id, AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__HR_Status__c from AxtriaSalesIQTM__Employee__c')){
                empIDList.add(emp.AxtriaSalesIQTM__Employee_ID__c); //Filling empIDList with IDs 
                empMap.put(emp.AxtriaSalesIQTM__Employee_ID__c,emp); //Filling empMap 
            }
            query = 'select id,AddressCity__c, AddressCountry__c,AddressLine1__c,AddressLine2__c,'+
                'AddressPostalCode__c,AddressStateCode__c,AssignmentStatusValue__c,PRID__c,Department__c,'+
                'Department_Code__c,Email__c,Employee_ID__c,Last_Name__c,First_Name__c,'+
                'JobChangeReason__c,JobCode__c,JobCodeName__c,LegalAddressCityName__c,LegalAddressCountryCode__c,LegalAddressLine1__c,'+
                'LegalAddressLine2__c,LegalAddressPostalCode__c,NickName__c,OriginalHireDate__c,ReportingToWorkerName__c,'+
                'ReportsToAssociateOID__c,StateTerritory__c,TerminationDate__c,Worker_Category__c,WorkPhone__c,Full_Name__c,Group_Name__c,'+
                'PersonalCell__c,company__c,SOA_Country_Code__c,Work_Fax__c,Hire_Date__c,Veeva_Country_Code__c,Marketing_Code__c from CurrentPersonFeed__c where isRejected__c=false ';         
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CurrentPersonFeed__c> scopeCurrentEmpList){
        List<AxtriaSalesIQTM__Employee__c> empUpdateList = new List<AxtriaSalesIQTM__Employee__c>(); //List to store employees for updating.
        for(CurrentPersonFeed__c cur : scopeCurrentEmpList){
            if(empMap.containsKey(cur.PRID__c)){
                AxtriaSalesIQTM__Employee__c emp        = new AxtriaSalesIQTM__Employee__c(Id = empMap.get(cur.PRID__c).Id,AxtriaSalesIQTM__HR_Status__c =empMap.get(cur.PRID__c).AxtriaSalesIQTM__HR_Status__c);
                /**Mapping Fields of Employee object from Employee Staging Fields**/
              emp.AddressCity__c = cur.AddressCity__c;
                emp.AddressLine1__c = cur.AddressLine1__c;
                emp.AddressLine2__c = cur.AddressLine2__c;
                emp.AddressPostalCode__c = cur.AddressPostalCode__c;
                emp.AddressStateCode__c = cur.AddressStateCode__c;
                
                emp.AddressCountry__c = cur.AddressCountry__c; //SOA Country Code
                emp.AxtriaSalesIQTM__country_name__c   = mapCountryName2SFID!=null && mapCountryName2SFID.containskey(cur.Veeva_Country_Code__c)?mapCountryName2SFID.get(cur.Veeva_Country_Code__c):null;
                emp.AxtriaSalesIQTM__Country__c        = cur.Veeva_Country_Code__c;
                emp.SOA_Marketing_Code__c              = cur.Marketing_Code__c;
                emp.SOA_Country_Code__c                = cur.SOA_Country_Code__c;
                
                emp.AssignmentStatusValue__c = cur.AssignmentStatusValue__c;
                /*if(cur.AssignmentStatusValue__c=='Active' && cur.TerminationDate__c!=null 
                    && cur.TerminationDate__c<=system.today()){
                    //emp.AssignmentStatusValue__c = 'Inactive';
                    emp.Employee_Status__c = 'Inactive';
                    emp.AxtriaSalesIQTM__HR_Status__c       = 'Inactive';
                }else if(cur.AssignmentStatusValue__c=='Active' && cur.TerminationDate__c!=null
                    && cur.TerminationDate__c>system.today()){
                    //emp.AssignmentStatusValue__c = cur.AssignmentStatusValue__c;
                    emp.Employee_Status__c = cur.AssignmentStatusValue__c;
                    emp.AxtriaSalesIQTM__HR_Status__c       = cur.AssignmentStatusValue__c;
                }else if(cur.AssignmentStatusValue__c=='Terminated'){
                    // && cur.TerminationDate__c!=null
                    //&& cur.TerminationDate__c<=system.today()){
                    //emp.AssignmentStatusValue__c = 'Terminated';
                    emp.Employee_Status__c = 'Terminated';
                    emp.AxtriaSalesIQTM__HR_Status__c       = 'Terminated';
                }*/
                //Transfer out of field
                if(cur.AssignmentStatusValue__c=='Active' && cur.TerminationDate__c!=null 
                    && cur.TerminationDate__c<=system.today() && emp.AxtriaSalesIQTM__HR_Status__c == 'Active'){
                    //emp.AssignmentStatusValue__c = 'Inactive';
                    emp.Employee_Status__c = 'Inactive';
                    emp.AxtriaSalesIQTM__HR_Status__c       = 'Inactive';
                }//Transfer to field
                else if(cur.AssignmentStatusValue__c=='Active' && emp.AxtriaSalesIQTM__HR_Status__c == 'Inactive'
                    ){
                    //emp.AssignmentStatusValue__c = cur.AssignmentStatusValue__c;
                    emp.Employee_Status__c = 'Active'; //cur.AssignmentStatusValue__c;
                    emp.AxtriaSalesIQTM__HR_Status__c       = 'Active';//cur.AssignmentStatusValue__c;
                }else if(cur.AssignmentStatusValue__c=='Terminated'){
                   // && cur.TerminationDate__c!=null
                    //&& cur.TerminationDate__c<=system.today() ){
                    //emp.AssignmentStatusValue__c = 'Terminated';
                    emp.Employee_Status__c = 'Terminated';
                    emp.AxtriaSalesIQTM__HR_Status__c       = 'Terminated';
                }else{
                    emp.Employee_Status__c = cur.AssignmentStatusValue__c;
                    emp.AxtriaSalesIQTM__HR_Status__c       = cur.AssignmentStatusValue__c;
                }
                emp.Department__c = cur.Department__c;
                emp.Department_Code__c = cur.Department_Code__c;
                emp.AxtriaSalesIQTM__Email__c = cur.Email__c;
                emp.AxtriaSalesIQTM__Employee_ID__c = cur.PRID__c;
                emp.FamilyName__c = cur.Last_Name__c;
                emp.GivenName__c = cur.First_Name__c;
                emp.JobChangeReason__c = cur.JobChangeReason__c;
                emp.JobCode__c = cur.JobCode__c;
                emp.Job_Title__c = cur.JobCodeName__c;
                emp.LegalAddressCityName__c = cur.AddressCity__c;
                emp.LegalAddressCountryCode__c = cur.AddressCountry__c;
                emp.LegalAddressLine1__c = cur.AddressLine1__c;
                emp.LegalAddressLine2__c = cur.AddressLine2__c;
                emp.LegalAddressPostalCode__c = cur.AddressPostalCode__c;
                //emp.AddressStateCode__c = cur.AddressStateCode__c;
                emp.NickName__c = cur.NickName__c;
                //Originial Hire Date should never be updated. If an employee comes as a transfer to field then only hire date should be updated.
                //emp.OriginalHireDate__c = cur.OriginalHireDate__c; 
                emp.Hire_Date__c= cur.Hire_Date__c;
                emp.ReportingToWorkerName__c = cur.ReportingToWorkerName__c;
                emp.ReportsToAssociateOID__c = cur.ReportsToAssociateOID__c;
                emp.StateTerritory__c = cur.StateTerritory__c;
             //  emp.TerminationDate__c = cur.TerminationDate__c;
                //emp.WorkLocation__c = cur.WorkLocation__c;
                emp.WorkPhone__c = cur.WorkPhone__c;
                emp.AxtriaSalesIQTM__HR_Termination_Date__c= cur.TerminationDate__c;
                //emp.AxtriaSalesIQTM__Last_Name__c=cur.Last_Name__c;
                //emp.AxtriaSalesIQTM__FirstName__c=cur.First_Name__c;
                emp.AxtriaSalesIQTM__Employee_ID__c     = string.valueof(cur.PRID__c);
                //emp.AxtriaSalesIQTM__Original_Hire_Date__c = cur.OriginalHireDate__c;
                //emp.AxtriaSalesIQTM__Rehire_Date__c     = cur.Rehire_Date__c;
                //emp.Name                        = cur.Full_Name__c;
                emp.Worker_Category__c          = cur.Worker_Category__c;
                emp.Group_Name__c               = cur.Group_Name__c;
                emp.AxtriaSalesIQTM__Cellphone_Number__c    = cur.PersonalCell__c; 
                emp.AxtriaSalesIQTM__Company__c             = cur.company__c; 
                emp.Work_Fax__c                             = cur.Work_Fax__c;
                
                if(cur.OriginalHireDate__c != null && cur.TerminationDate__c != null && cur.TerminationDate__c < cur.Hire_Date__c){
                    emp.AxtriaSalesIQTM__HR_Termination_Date__c = null;    
                }
                
                empUpdateList.add(emp); //Adding Employees to list empUpdateList for updation
            }
        }
        if(!empUpdateList.IsEmpty()){
          Database.update(empUpdateList,false);
        }
    }
    
        
    /*public Boolean checkFieldEmployee(string deptValue, string jobTitleValue){
        
        
        system.debug('====deptValue===='+deptValue);
        system.debug('====jobTitleValue===='+jobTitleValue);
        
        if(fieldEmpDepartmentSet.contains(deptValue)){
            for(string str : mapId2FieldEmpDetails.keySet()){
                if((mapId2FieldEmpDetails.get(str))[1]=='equals'){
                    if((mapId2FieldEmpDetails.get(str))[0]==deptValue && (mapId2FieldEmpDetails.get(str))[2]==jobTitleValue){
                        return true;
                    }
                }
                else if((mapId2FieldEmpDetails.get(str))[1]=='contains'){
                    if((mapId2FieldEmpDetails.get(str))[0]==deptValue && (jobTitleValue.contains(mapId2FieldEmpDetails.get(str)[2]))){
                        return true;
                    }
                }
            }
        }
        else{
            return false;
        }
        return false;
        
    }*/
    
    global void finish(Database.BatchableContext BC){
        BatchEmployeeChangeStatusForOutbound newb1 = new BatchEmployeeChangeStatusForOutbound(mapEmp2EventTypeDate);
        Database.executeBatch(newb1, 200);
    
        BatchAutoUnassignForTerminatedEmp newb = new BatchAutoUnassignForTerminatedEmp(batchID);
        Database.executeBatch(newb, 200);    
    }
}