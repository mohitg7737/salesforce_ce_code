global class MarkMCCPtargetDeleted_EU implements Database.Batchable<sObject> {
    public String query;

    global MarkMCCPtargetDeleted_EU() {
        this.query = 'select id from SIQ_MC_Cycle_Plan_Target_vod_O__c where Rec_Status__c != \'Updated\' ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_MC_Cycle_Plan_Target_vod_O__c> scope) {
        
        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mct : scope)
        {
            mct.Rec_Status__c ='Deleted';
        }   

        update scope;
    }

    global void finish(Database.BatchableContext BC) {

    }
}