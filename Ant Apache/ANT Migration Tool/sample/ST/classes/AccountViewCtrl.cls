public class AccountViewCtrl {

public  Account selectedAcc;
    public list<Wrapper>wrapperList{get;set;}
    public list<Wrapper>wrapperList1{get;set;}
    public list<Wrapper>wrapperList2{get;set;}
    public list<Wrapper>wrapperList3{get;set;}
    public integer count1 {get;set;}
    public integer count2 {get;set;}
    public integer count3 {get;set;}
    public integer count4 {get;set;}
    public string selectedteaminstance1;
    public string selectedteaminstance2;
    public string selectedteaminstance3;
    public string selectedteaminstance4;
    public set<string>Teaminstancelist{get;set;}
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpListteam{get;set;}
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpList{get;set;}
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpList1{get;set;}
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpList2{get;set;}
    public list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpList3{get;set;}
    
    
    public string teamInstancename{get;set;}
    List<columnwrapper> colwrpr=new list<columnwrapper>();
    
    
   public  AccountViewCtrl(ApexPages.StandardController controller){
     Integer allCall=0;
     selectedAcc = (Account)controller.getRecord();
     pacpList= new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     pacpList1= new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     pacpList2= new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     pacpList3= new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     
     Teaminstancelist = new set<string>();
     wrapperList=new list<Wrapper>();
     wrapperList1=new list<Wrapper>();
     wrapperList2=new list<Wrapper>();
     wrapperList3=new list<Wrapper>();
     map<string,string>mapPosToUser=new map<string,string>();
     for(AxtriaSalesIQTM__User_Access_Permission__c up:[select AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.name,AxtriaSalesIQTM__User__r.name from AxtriaSalesIQTM__User_Access_Permission__c ])//where AxtriaSalesIQTM__position__r.AxtriaSalesIQTM__Position_Type__c='Territory'
     {
        mapPosToUser.put(up.AxtriaSalesIQTM__Position__c,up.AxtriaSalesIQTM__User__r.name);
		//system.debug('userInfo.Position__c'+up.AxtriaSalesIQTM__Position__c);
        //system.debug('up.User__r.name'+up.AxtriaSalesIQTM__User__r.name);
     }
     pacpListteam = [select id,AxtriaSalesIQTM__Team_Instance__c from  AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__account__c=:selectedAcc.Id];
     
     for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : pacpListteam){
       if(!Teaminstancelist.contains(p.AxtriaSalesIQTM__Team_Instance__c)){
           Teaminstancelist.add(p.AxtriaSalesIQTM__Team_Instance__c);
       }
     }
   list<string> newlist = new list<string>();
   newlist.addAll(Teaminstancelist);
   
   if(newlist.size() == 1){
       
       selectedteaminstance1=newlist[0];
       system.debug('-------------teaminstance list is:'+newlist);
   }
   if(newlist.size() == 2){
       selectedteaminstance1=newlist[0];
       selectedteaminstance2=newlist[1];
   }
   if(newlist.size() == 3){
       selectedteaminstance1=newlist[0];
       selectedteaminstance2=newlist[1];
       selectedteaminstance3=newlist[2];
   }
   if(newlist.size() == 4){
       selectedteaminstance1=newlist[0];
       selectedteaminstance2=newlist[1];
       selectedteaminstance3=newlist[2];
       selectedteaminstance4=newlist[3];
   }
   
   system.debug('selected team instance is:'+selectedteaminstance1);
   if(selectedteaminstance1!=null){
       system.debug('-------inside One BU-----');
         pacpList=[select Id,
         				  AxtriaSalesIQTM__Team_Instance__r.Name,
         				  AxtriaSalesIQTM__Team_Instance__c,	
         				  AxtriaSalesIQTM__Position__c,	
         				  AxtriaSalesIQTM__Position__r.Name,
         				  AxtriaSalesIQTM__Account_Alignment_type__c,
         				  AxtriaSalesIQTM__Picklist1_Segment__c,
         				  AxtriaSalesIQTM__Picklist1_Updated__c,
         				  TCF_P1__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,P1__c,Parameter7__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c
         				  from AxtriaSalesIQTM__Position_Account_Call_Plan__c
         				  where AxtriaSalesIQTM__account__c=:selectedAcc.Id and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance1];
   }
   system.debug('---------PACPLIST :'+pacpList);
   if(selectedteaminstance2!=null){
       system.debug('-------inside  BU 2-----');
     pacpList1=[select Id,
         				  AxtriaSalesIQTM__Team_Instance__r.Name,
         				  AxtriaSalesIQTM__Team_Instance__c,	
         				  AxtriaSalesIQTM__Position__c,	
         				  AxtriaSalesIQTM__Position__r.Name,
         				  AxtriaSalesIQTM__Account_Alignment_type__c,
         				  AxtriaSalesIQTM__Picklist1_Segment__c,
         				  AxtriaSalesIQTM__Picklist1_Updated__c,
         				  TCF_P1__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,P1__c,Parameter7__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c
         				  from AxtriaSalesIQTM__Position_Account_Call_Plan__c
         				  where AxtriaSalesIQTM__account__c=:selectedAcc.Id and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance2];
   }
    if(selectedteaminstance3!=null){
        system.debug('-------inside  BU 3-----');
         pacpList2=[select Id,
             				  AxtriaSalesIQTM__Team_Instance__r.Name,
             				  AxtriaSalesIQTM__Team_Instance__c,	
             				  AxtriaSalesIQTM__Position__c,	
             				  AxtriaSalesIQTM__Position__r.Name,
             				  AxtriaSalesIQTM__Account_Alignment_type__c,
             				  AxtriaSalesIQTM__Picklist1_Segment__c,
             				  AxtriaSalesIQTM__Picklist1_Updated__c,
             				  TCF_P1__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,P1__c,Parameter7__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c
             				  from AxtriaSalesIQTM__Position_Account_Call_Plan__c
             				  where AxtriaSalesIQTM__account__c=:selectedAcc.Id and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance3];
       }  
       
      if(selectedteaminstance4!=null){
          system.debug('-------inside  BU 4-----');
         pacpList3=[select Id,
             				  AxtriaSalesIQTM__Team_Instance__r.Name,
             				  AxtriaSalesIQTM__Team_Instance__c,	
             				  AxtriaSalesIQTM__Position__c,	
             				  AxtriaSalesIQTM__Position__r.Name,
             				  AxtriaSalesIQTM__Account_Alignment_type__c,
             				  AxtriaSalesIQTM__Picklist1_Segment__c,
             				  AxtriaSalesIQTM__Picklist1_Updated__c,
             				  TCF_P1__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,P1__c,Parameter7__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c
             				  from AxtriaSalesIQTM__Position_Account_Call_Plan__c
             				  where AxtriaSalesIQTM__account__c=:selectedAcc.Id and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance4];
       }   
   
  // system.debug('selected team instance is:'+selectedteaminstance1);
    system.debug('---------PACPLIST1 :'+pacpList1);
   system.debug('---------PACPLIST2 :'+pacpList2);
   system.debug('---------PACPLIST3 :'+pacpList3);
     				 // and AxtriaSalesIQTM__isIncludedCallPlan__c =true]; commented by siva on 4-10-17
   /*	map<string,Integer> mapTeamToPos= new map<string,integer>();
    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp:pacpList)
    {
    	teamInstancename=pacp.AxtriaSalesIQTM__Team_Instance__r.Name;
    	//allCall=allCall+integer.valueof(pacp.AxtriaSalesIQTM__Picklist1_Updated__c);
    	if(!mapTeamToPos.containskey(pacp.AxtriaSalesIQTM__Team_Instance__c))
		{
			//map<string,Integer>temp=new  map<string,Integer>();
			//temp.put(pacp.Position__c,integer.valueof(pacp.Picklist1_Updated__c));
			//mapTeamToPos.put(pacp.AxtriaSalesIQTM__Team_Instance__c,integer.valueof(pacp.AxtriaSalesIQTM__Picklist1_Updated__c));
		}
	}
	List<String>fieldList =new list<string>{'Parameter1__c','Parameter2__c','Parameter3__c','Parameter4__c','Parameter5__c','Parameter6__c','Parameter7__c'};
	List<String>fieldList1 =new list<string>{'P2_Parameter1__c','P2_Parameter2__c','P2_Parameter3__c','P2_Parameter4__c','P2_Parameter5__c','P2_Parameter6__c','P2_Parameter7__c'};
	List<String>fieldList2 =new list<string>{'P3_Parameter1__c','P3_Parameter2__c','P3_Parameter3__c','P3_Parameter4__c','P3_Parameter5__c','P3_Parameter6__c','P3_Parameter7__c'};
	//list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>displaylist = new list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
	*/
	map<string,string>dsplay =new map<string,string>();
	map<string,string>dsplay1 =new map<string,string>();
	map<string,string>dsplay2 =new map<string,string>();
	map<string,string>dsplay3 =new map<string,string>();
	
	for( AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ds : [select id,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Attribute_Display_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where Brand_Team_Instance__c!=null and AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance1]){
		if(!dsplay.containsKey(ds.AxtriaSalesIQTM__Attribute_API_Name__c)){
			dsplay.put(ds.AxtriaSalesIQTM__Attribute_API_Name__c,ds.AxtriaSalesIQTM__Attribute_Display_Name__c);
		}
		colwrpr.add(new columnwrapper(ds.AxtriaSalesIQTM__Attribute_Display_Name__c));
	}
	
	if(selectedteaminstance2 != null){
    	for( AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ds : [select id,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Attribute_Display_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where Brand_Team_Instance__c!=null and AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance2]){
    		if(!dsplay1.containsKey(ds.AxtriaSalesIQTM__Attribute_API_Name__c)){
    			dsplay1.put(ds.AxtriaSalesIQTM__Attribute_API_Name__c,ds.AxtriaSalesIQTM__Attribute_Display_Name__c);
    		}
    		colwrpr.add(new columnwrapper(ds.AxtriaSalesIQTM__Attribute_Display_Name__c));
    	}
	}
	if(selectedteaminstance3 != null){
    	for( AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ds : [select id,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Attribute_Display_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where Brand_Team_Instance__c!=null and AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance3]){
    		if(!dsplay2.containsKey(ds.AxtriaSalesIQTM__Attribute_API_Name__c)){
    			dsplay2.put(ds.AxtriaSalesIQTM__Attribute_API_Name__c,ds.AxtriaSalesIQTM__Attribute_Display_Name__c);
    		}
    		colwrpr.add(new columnwrapper(ds.AxtriaSalesIQTM__Attribute_Display_Name__c));
    	}
	
	}
	if(selectedteaminstance4 != null){
    	for( AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ds : [select id,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Attribute_Display_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where Brand_Team_Instance__c!=null and AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c=:selectedteaminstance4]){
    		if(!dsplay3.containsKey(ds.AxtriaSalesIQTM__Attribute_API_Name__c)){
    			dsplay3.put(ds.AxtriaSalesIQTM__Attribute_API_Name__c,ds.AxtriaSalesIQTM__Attribute_Display_Name__c);
    		}
    		colwrpr.add(new columnwrapper(ds.AxtriaSalesIQTM__Attribute_Display_Name__c));
    	}
	
	}
	
	
	
	
	system.debug('--------Before wrapper-----'+pacpList);
    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c wr:pacpList)
    {
        Wrapper obj=new Wrapper(wr.Id,wr.AxtriaSalesIQTM__Team_Instance__r.Name,wr.AxtriaSalesIQTM__Position__r.Name,String.valueof(mapPosToUser.get(wr.AxtriaSalesIQTM__Position__c)),Integer.Valueof(wr.TCF_P1__c),wr.Parameter1__c,wr.Parameter2__c,wr.Parameter3__c,wr.Parameter4__c,wr.Parameter5__c,wr.Parameter6__c,wr.P1__c,wr.P2_Parameter1__c,wr.P2_Parameter2__c,wr.P2_Parameter3__c,wr.P2_Parameter4__c,wr.P2_Parameter5__c,wr.P2_Parameter6__c,wr.P2_Parameter7__c,wr.P3_Parameter1__c,wr.P3_Parameter2__c,wr.P3_Parameter3__c,wr.P3_Parameter4__c,wr.P3_Parameter5__c,wr.P3_Parameter6__c,wr.P3_Parameter7__c,dsplay);
		system.debug('--------obj isss:'+obj);
		wrapperList.add(obj);
    }
    system.debug('--------wrapperList-:'+wrapperList);
     for(AxtriaSalesIQTM__Position_Account_Call_Plan__c wr:pacpList1)
    {   
       Wrapper obj1=new Wrapper(wr.Id,wr.AxtriaSalesIQTM__Team_Instance__r.Name,wr.AxtriaSalesIQTM__Position__r.Name,String.valueof(mapPosToUser.get(wr.AxtriaSalesIQTM__Position__c)),Integer.Valueof(wr.TCF_P1__c),wr.Parameter1__c,wr.Parameter2__c,wr.Parameter3__c,wr.Parameter4__c,wr.Parameter5__c,wr.Parameter6__c,wr.P1__c,wr.P2_Parameter1__c,wr.P2_Parameter2__c,wr.P2_Parameter3__c,wr.P2_Parameter4__c,wr.P2_Parameter5__c,wr.P2_Parameter6__c,wr.P2_Parameter7__c,wr.P3_Parameter1__c,wr.P3_Parameter2__c,wr.P3_Parameter3__c,wr.P3_Parameter4__c,wr.P3_Parameter5__c,wr.P3_Parameter6__c,wr.P3_Parameter7__c,dsplay1);
	   wrapperList1.add(obj1);
    }
    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c wr:pacpList2)
    {   
       Wrapper obj2=new Wrapper(wr.Id,wr.AxtriaSalesIQTM__Team_Instance__r.Name,wr.AxtriaSalesIQTM__Position__r.Name,String.valueof(mapPosToUser.get(wr.AxtriaSalesIQTM__Position__c)),Integer.Valueof(wr.TCF_P1__c),wr.Parameter1__c,wr.Parameter2__c,wr.Parameter3__c,wr.Parameter4__c,wr.Parameter5__c,wr.Parameter6__c,wr.P1__c,wr.P2_Parameter1__c,wr.P2_Parameter2__c,wr.P2_Parameter3__c,wr.P2_Parameter4__c,wr.P2_Parameter5__c,wr.P2_Parameter6__c,wr.P2_Parameter7__c,wr.P3_Parameter1__c,wr.P3_Parameter2__c,wr.P3_Parameter3__c,wr.P3_Parameter4__c,wr.P3_Parameter5__c,wr.P3_Parameter6__c,wr.P3_Parameter7__c,dsplay2);
	   wrapperList2.add(obj2);
    }
    
    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c wr:pacpList3)
    {   
       Wrapper obj3=new Wrapper(wr.Id,wr.AxtriaSalesIQTM__Team_Instance__r.Name,wr.AxtriaSalesIQTM__Position__r.Name,String.valueof(mapPosToUser.get(wr.AxtriaSalesIQTM__Position__c)),Integer.Valueof(wr.TCF_P1__c),wr.Parameter1__c,wr.Parameter2__c,wr.Parameter3__c,wr.Parameter4__c,wr.Parameter5__c,wr.Parameter6__c,wr.P1__c,wr.P2_Parameter1__c,wr.P2_Parameter2__c,wr.P2_Parameter3__c,wr.P2_Parameter4__c,wr.P2_Parameter5__c,wr.P2_Parameter6__c,wr.P2_Parameter7__c,wr.P3_Parameter1__c,wr.P3_Parameter2__c,wr.P3_Parameter3__c,wr.P3_Parameter4__c,wr.P3_Parameter5__c,wr.P3_Parameter6__c,wr.P3_Parameter7__c,dsplay3);
	   wrapperList3.add(obj3);
    }
    
    
     system.debug('88888Dispaly wrapper:'+wrapperList1);
    count1=	wrapperList.size();
    count2 = wrapperList1.size();
    count3=	wrapperList2.size();
    count4 = wrapperList3.size();
}
	 
	/* public void saveChanges()
	 {
	 	list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacpUpdateList=new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
	 	
	 	for(Wrapper w:wrapperList)
	 	{
	 	    AxtriaSalesIQTM__Position_Account_Call_Plan__c obj=new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
	 	 	
	 	 	obj.Id=w.pacpId;
	 	 	obj.isOverride__c=w.isOverride;
	 	 	obj.Account_Alignment_type__c=w.alignmentType;
	 	 	pacpUpdateList.add(obj);
	 	 
	 	}
	 	update  pacpUpdateList;
	 }*/
	 public class columnwrapper{
		  public String title {get;set;}
		  public String defaultContent {get;set;}
		  public columnwrapper(String s1){
		        title=s1;
		        defaultContent='';
		   }
		    public columnwrapper(){}
	}
	
	public class Wrapper
	{
		public string TeamInstanceName{get;set;}
		public id pacpId{get;set;}
		
     	public	string  positionName{get;set;}	
     	public	integer allCall{get;set;}	
     	public	integer teamCall{get;set;}	
     	public	string  callBefore{get;set;}	
     	public  string  callAfter{get;set;}	
     	public  boolean share{get;set;}	
     	public  string  rep{get;set;}	
     	public  string  credit{get;set;}
     	public  boolean  isOverride{get;set;}	
     	public  string  alignmentType{get;set;}	
     	public  integer tcfp1 {get;set;}
     	public	string p1{get;set;}
     	public	string p2{get;set;}
     	public	string p3{get;set;}
     	public	string p4{get;set;}
     	public	string p5{get;set;}
     	public	string p6{get;set;}
     	public string p2p1 {get;set;}
     	public string p2p2 {get;set;}
     	public string p2p3 {get;set;}
     	public string p2p4 {get;set;}
     	public string p2p5 {get;set;}
     	public string p2p6 {get;set;}
     	public string p2p7 {get;set;}
     	
     	public string p3p1 {get;set;}
     	public string p3p2 {get;set;}
     	public string p3p3 {get;set;}
     	public string p3p4 {get;set;}
     	public string p3p5 {get;set;}
     	public string p3p6 {get;set;}
     	public string p3p7 {get;set;}
     	
     	public	string t1{get;set;}
     	public	string t2{get;set;}
     	public	string t3{get;set;}
     	public	string t4{get;set;}
     	public	string t5{get;set;}
     	public	string t6{get;set;}
     	
     	public	string t2t1{get;set;}
     	public	string t2t2{get;set;}
     	public	string t2t3{get;set;}
     	public	string t2t4{get;set;}
     	public	string t2t5{get;set;}
     	public	string t2t6{get;set;}
     	
     	public	string t3t1{get;set;}
     	public	string t3t2{get;set;}
     	public	string t3t3{get;set;}
     	public	string t3t4{get;set;}
     	public	string t3t5{get;set;}
     	public	string t3t6{get;set;}
     	
     	public  string product {set;get;}
     	
     	public Wrapper(Id pacpId,string TeamInstanceName ,string positionName,string rep,integer tcfp1,string p1,string p2,string p3,string p4,string p5,string p6,string product,string p2p1,string p2p2,string p2p3,string p2p4,string p2p5,string p2p6,string p2p7,string p3p1,string p3p2,string p3p3,string p3p4,string p3p5,string p3p6,string p3p7,map<string,string>display)
     	{
     		this.pacpId=pacpId;
     		this.TeamInstanceName=TeamInstanceName;
     		this.positionName=positionName;
     		//this.callBefore=callBefore;
     	//	this.callAfter=callAfter;
     		this.rep=rep;
     		//this.alignmentType=alignmentType;
     		this.tcfp1=tcfp1;
     		this.p1=p1;
     		this.p2=p2;
     		this.p3=p3;
     		this.p4=p4;
     		this.p5=p5;
     		this.p6=p6;
     		if(p2p1!=null && p2p1!=''){
     		    this.p2p1=p2p1;
     		}
     		if(p2p2!=null && p2p2!=''){
     		    this.p2p2=p2p2;
     		}
     		if(p2p3!=null && p2p3!=''){
     		    this.p2p3=p2p3;
     		}
     		if(p2p4!=null && p2p4!=''){
     		    this.p2p4=p2p4;
     		} 
     		if(p2p5!=null && p2p5!=''){
     		    this.p2p5=p2p5;
     		} 
     		if(p2p6!=null && p2p6!=''){
     	    	this.p2p6=p2p6;
     		}
     		
     		system.debug('--------before p3p1 is:'+p3p1);
     		if(p3p1!=null && p3p1!=''){
     		    this.p3p1=p3p1;
     		  system.debug('--------this p3p1 is:'+this.p3p1);  
     		}
     		if(p3p2!=null && p3p2!=''){
     		    this.p3p2=p3p2;
     		}
     		if(p3p3!=null && p3p3!=''){
     	    	this.p3p3=p3p3;
     		}
     		if(p3p4!=null && p3p4!=''){
     		    this.p3p4=p3p4;
     		}
     		if(p3p5!=null && p3p5!=''){
     		    this.p3p5=p3p5;
     		}
     		if(p3p6!=null && p3p6!=''){
     	    	this.p3p6=p3p6;
     		}
     		this.product=product;
     		if(display.get('Parameter1__c')!=null){
     		    this.t1=display.get('Parameter1__c');
     		}
     		if(display.get('Parameter2__c')!=null){
     		    this.t2=display.get('Parameter2__c');
     		}
     		if(display.get('Parameter3__c')!=null){
     		    this.t3=display.get('Parameter3__c');
     		}
     		if(display.get('Parameter4__c')!=null){
     		    this.t4=display.get('Parameter4__c');
     	    }
     	    if(display.get('Parameter5__c')!=null){
     	        this.t5=display.get('Parameter5__c');
     	    }
     	    if(display.get('Parameter6__c')!=null){
         		this.t6=display.get('Parameter6__c');
     	    } 
     	    
     	    if(display.get('P2_Parameter1__c')!=null){
     		    this.t2t1=display.get('P2_Parameter1__c');
     	    }
     	    if(display.get('P2_Parameter2__c')!=null){
     		    this.t2t2=display.get('P2_Parameter2__c');
     	    }
     	    if(display.get('P2_Parameter3__c')!=null){
         		this.t2t3=display.get('P2_Parameter3__c');
     	    }
     	    if(display.get('P2_Parameter4__c')!=null){
         		this.t2t4=display.get('P2_Parameter4__c');
     	    }
         	if(display.get('P2_Parameter5__c')!=null){
         		this.t2t5=display.get('P2_Parameter5__c');
         	}
         	if(display.get('P2_Parameter6__c')!=null){
     		    this.t2t6=display.get('P2_Parameter6__c');
         	}
         	
         	if(display.get('P3_Parameter1__c')!=null){
     		    this.t3t1=display.get('P3_Parameter1__c');
     	    }
     	    if(display.get('P3_Parameter2__c')!=null){
     		    this.t3t2=display.get('P3_Parameter2__c');
     	    }
     	    if(display.get('P3_Parameter3__c')!=null){
         		this.t3t3=display.get('P3_Parameter3__c');
     	    }
     	    if(display.get('P3_Parameter4__c')!=null){
         		this.t3t4=display.get('P3_Parameter4__c');
     	    }
         	if(display.get('P3_Parameter5__c')!=null){
         		this.t3t5=display.get('P3_Parameter5__c');
         	}
         	if(display.get('P3_Parameter6__c')!=null){
     		    this.t3t6=display.get('P3_Parameter6__c');
         	}
         	
     		
     		system.debug('---T1 is::'+this.t1);
     		system.debug('---Dispaly is::'+display);
     		system.debug('---p3p1:'+p3p1);
     		system.debug('---p3p2:'+p3p2);
     		system.debug('---p3p3:'+p3p3);
     		system.debug('---p3p4:'+p3p4);
     		system.debug('---p3p5:'+p3p5);
     		
     	}
	} 
	
	/*public class Wrapper1
	{
		public string TeamInstanceName{get;set;}
		public id pacpId{get;set;}
		
     	public	string  positionName{get;set;}	
     	public	integer allCall{get;set;}	
     	public	integer teamCall{get;set;}	
     	public	string  callBefore{get;set;}	
     	public  string  callAfter{get;set;}	
     	public  boolean share{get;set;}	
     	public  string  rep{get;set;}	
     	public  string  credit{get;set;}
     	public  boolean  isOverride{get;set;}	
     	public  string  alignmentType{get;set;}	
     	public  integer tcfp1 {get;set;}
     	public	string p1{get;set;}
     	public	string p2{get;set;}
     	public	string p3{get;set;}
     	public	string p4{get;set;}
     	public	string p5{get;set;}
     	public	string p6{get;set;}
     	public	string t1{get;set;}
     	public	string t2{get;set;}
     	public	string t3{get;set;}
     	public	string t4{get;set;}
     	public	string t5{get;set;}
     	public	string t6{get;set;}
     	
     	public	string t2t1{get;set;}
     	public	string t2t2{get;set;}
     	public	string t2t3{get;set;}
     	public	string t2t4{get;set;}
     	public	string t2t5{get;set;}
     	public	string t2t6{get;set;}
     	
     	
     	public  string product {set;get;}
     	
     	public Wrapper1(Id pacpId,string TeamInstanceName ,string positionName,string rep,integer tcfp1,string p1,string p2,string p3,string p4,string p5,string p6,string product,map<string,string>display)
     	{
     		this.pacpId=pacpId;
     		this.TeamInstanceName=TeamInstanceName;
     		this.positionName=positionName;
     		//this.callBefore=callBefore;
     	//	this.callAfter=callAfter;
     		this.rep=rep;
     		//this.alignmentType=alignmentType;
     		this.tcfp1=tcfp1;
     		this.p1=p1;
     		this.p2=p2;
     		this.p3=p3;
     		this.p4=p4;
     		this.p5=p5;
     		this.p6=p6;
     		this.product=product;
     		if(display.get('Parameter1__c')!=null){
     		    this.t1=display.get('Parameter1__c');
     		}
     		if(display.get('Parameter2__c')!=null){
     		    this.t2=display.get('Parameter2__c');
     		}
     		if(display.get('Parameter3__c')!=null){
     		    this.t3=display.get('Parameter3__c');
     		}
     		if(display.get('Parameter4__c')!=null){
     		    this.t4=display.get('Parameter4__c');
     	    }
     	    if(display.get('Parameter5__c')!=null){
     	        this.t5=display.get('Parameter5__c');
     	    }
     	    if(display.get('Parameter6__c')!=null){
         		this.t6=display.get('Parameter6__c');
     	    } 
     	    
     	    if(display.get('P2_Parameter1__c')!=null){
     		    this.t2t1=display.get('P2_Parameter1__c');
     	    }
     	    if(display.get('P2_Parameter2__c')!=null){
     		    this.t2t2=display.get('P2_Parameter2__c');
     	    }
     	    if(display.get('P2_Parameter3__c')!=null){
         		this.t2t3=display.get('P2_Parameter3__c');
     	    }
     	    if(display.get('P2_Parameter4__c')!=null){
         		this.t2t4=display.get('P2_Parameter4__c');
     	    }
         	if(display.get('P2_Parameter5__c')!=null){
         		this.t2t5=display.get('P2_Parameter5__c');
         	}
         	if(display.get('P2_Parameter6__c')!=null){
     		    this.t2t6=display.get('P2_Parameter6__c');
         	}
     		 
     		system.debug('---T1 is::'+this.t1);
     		system.debug('---Dispaly is::'+display);
     		
     	}
	}*/
	
	

}