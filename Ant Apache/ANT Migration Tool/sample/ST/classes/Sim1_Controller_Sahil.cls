public with sharing class Sim1_Controller_Sahil {
    public list<SelectOption> businessUnits {get;set;}
    public list<SelectOption> lines {get;set;}
    public list<SelectOption> brands {get;set;}
    public list<SelectOption> cycles {get;set;}
    public list<SelectOption> rules {get;set;}
    public list<SelectOption> matrices {get;set;}
    public String selectedCycle{get;set;}
    public String selectedBu{get;set;}
    public String selectedLine{get;set;}
    public String selectedBrand{get;set;}
    public String selectedRule{get;set;}
    public String selectedMatrix{get;set;}
    public String selectedDimension{get;set;}
    public String dimention1{get;set;}
    public String dimention2{get;set;}
    public Map<String, Integer>  hcpCountPerCell{get;set;}
    public Map<String, String> mapGridDetails{get;set;}
    public Map<String, String> mapCellDetails{get;set;}
    public Set<String> rows{get;set;}
    public String selectedSimulation{get;set;}
    public Integer rowCount1{get;set;}
    public Integer colCount1{get;set;}
    public Set<String> cols{get;set;}
    public Set<String> dimn1{get;set;}
    public Set<String> dimn2{get;set;}
    public Set<String> dimn3{get;set;}
    public list<SelectOption> selectOptionList{get;set;}
    public Boolean flag{get;set;}
    public list<PieChart> pieChartData{get;set;}
    public Map<String, Integer> dimOneMap{get;set;}
    public Map<String, Integer> dimTwoMap{get;set;}
    public String chnagedGridValue{get;set;}
    public list<SelectOption> allSimulations{get;set;}
    public String selectedMatrixCopy {get;set;}
    public String field1{get;set;}
    public String field2{get;set;}
    public String output{get;set;}
    public Boolean showSimulation{get;set;}
    public String colorSet{get;set;}
    public Sim1_Controller_Sahil(){
        init();
        fillCycleOptions();
        fillBusinessUnitOptions();
        fillLineOptions();
        fillBrandOptions();
        fillRuleOptions();
    }

    public void init(){
        flag = false;
        chnagedGridValue = '';
        colCount1 = 0;
        rowCount1 = 0;
        selectedCycle = '';
        selectedBu = '';
        selectedLine = '';
        selectedBrand = '';
        selectedRule = '';
        dimention1 = '';
        dimention2 = '';
        field1 = '';
        field2 = '';
        output = '';
        showSimulation = false;
        selectedDimension = '';
        matrices = new list<SelectOption>();
        allSimulations = new List<SelectOption>();
        selectedSimulation = 'Segment Simulation';
        allSimulations.add(new SelectOption('Segment Simulation', 'Segment Simulation'));
        allSimulations.add(new SelectOption('Workload Simulation', 'Workload Simulation'));
    }

    public void fillCycleOptions(){
        matrices = new list<SelectOption>();
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        for(Cycle__c cycle: [SELECT Id, Name FROM Cycle__c]){
            cycles.add(new SelectOption(cycle.Id, cycle.Name));
        }
    }

    public void fillBusinessUnitOptions(){
        matrices = new list<SelectOption>();
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None', '--None--'));
        for(AxtriaSalesIQTM__Team_Instance__c bu: [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c]){
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }
    }

    public void fillLineOptions(){
        matrices = new list<SelectOption>();
        lines = new list<SelectOption>();
        lines.add(new SelectOption('None', '--None--'));
        if(String.isNotBlank(selectedBu)){
            for(Line__c line: [SELECT Id, Name FROM Line__c WHERE Team_Instance__c =:selectedBu]){
                lines.add(new SelectOption(line.Id, line.Name));
            }
        }
    }

    public void fillBrandOptions(){
        matrices = new list<SelectOption>();
        brands = new list<SelectOption>();
        brands.add(new SelectOption('None', '--None--'));
        for(Product_Catalog__c product: [SELECT Id, Name FROM Product_Catalog__c WHERE Team_Instance__c =: selectedBu]){
            brands.add(new SelectOption(product.Id, product.Name));
        }
    }

    public void fillRuleOptions(){
        matrices = new list<SelectOption>();
        rules = new list<SelectOption>();
        rules.add(new SelectOption('None', '--None--'));
        if(String.isNotBlank(selectedBu)  && String.isNotBlank(selectedBrand)){ //&& String.isNotBlank(selectedLine)
            for(Measure_Master__c rule: [SELECT Id, Name FROM Measure_Master__c WHERE Team_Instance__c =: selectedBU  AND Brand_Lookup__c =:selectedBrand]){//AND Line_2__c =: selectedLine
                rules.add(new SelectOption(rule.Id, rule.Name));
            }
        }
    }

     public PageReference redirectToPage()
    {
        system.debug('+++++++'+selectedSimulation);
        if(selectedSimulation == 'Segment Simulation')
        {
            PageReference pf = new PageReference('/apex/Simulation2');
            pf.setRedirect(true);
            return pf;
        }
        else if (selectedSimulation == 'Workload Simulation')
        {
            PageReference pf = new PageReference('/apex/Aversh_Simulation_3_change');
            pf.setRedirect(true);
            return pf;
        }
        else
        {
            return null;
        }
    }

    public void populateMatrixData(){
        try{
            list<Step__c> matricesStep = [SELECT Id, Name, Matrix__c, Matrix__r.Name, Grid_Param_1__r.Parameter_Name__c, Grid_Param_2__r.Parameter_Name__c FROM Step__c WHERE Measure_Master__c =: selectedRule AND Step_Type__c ='Matrix' AND UI_Location__c = 'Compute Segment' AND Matrix__r.Grid_Type__c = '2D'];
            System.debug('--matricesStep-- ' + selectedRule);
            System.debug('--matricesStep-- ' + matricesStep);
            matrices = new list<SelectOption>();
            selectedMatrix = '';
            if(matricesStep != null && matricesStep.size() > 0){
                for(Step__c step: matricesStep){
                    matrices.add(new SelectOption(step.Matrix__c, step.Matrix__r.Name));
                }
                if(String.isBlank(selectedMatrix))
                    selectedMatrix = matricesStep[0].Matrix__c;
                System.debug('---selectedMatrix--- ' + selectedMatrix);
                fillMatrixDetails();
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Data Found!'));
            }
        }catch(MyException e){
            matrices = new list<SelectOption>();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Data Found!'));
        }
    }

    public GridMaster_Copy__c copyMatrix(String selMatrix){
        Grid_Master__c matrix = [SELECT Id, Name, Dimension_2_Name__c, Dimension_1_Name__c FROM Grid_Master__c WHERE Id =:selMatrix LIMIT 1];
        GridMaster_Copy__c gCopy = new GridMaster_Copy__c();
        gCopy.Name = matrix.Name;
        gCopy.Dimension_2_Name__c = matrix.Dimension_2_Name__c;
        gCopy.Dimension_1_Name__c = matrix.Dimension_1_Name__c;
        gCopy.Matrix_Original__c = selMatrix;
        insert gCopy;
        list<Grid_Details_Copy__c> detailsCopy = new list<Grid_Details_Copy__c>();
        for(Grid_Details__c gd: [SELECT Id, Name, Dimension_1_Value__c, Dimension_2_Value__c, Output_Value__c, Grid_Master__c FROM Grid_Details__c WHERE Grid_Master__c=:matrix.Id]){
            Grid_Details_Copy__c dc = new Grid_Details_Copy__c();
            dc.Name = gd.Name;
            dc.Dimension_1_Value__c = gd.Dimension_1_Value__c;
            dc.Dimension_2_Value__c = gd.Dimension_2_Value__c;
            dc.Output_Value__c = gd.Output_Value__c;
            dc.GridMaster_Copy__c = gCopy.Id;
            dc.Grid_Master__c = gd.Grid_Master__c;
            detailsCopy.add(dc);
        }
        insert detailsCopy;
        return gCopy;
    }
    public void fillMatrixDetails(){
        showSimulation = false;
        list<Account_Compute_Final_Copy__c> accountComputeFinalList = [SELECT Id, Physician_2__c, Output_Name_1__c, Output_Name_2__c, Output_Name_3__c, Output_Name_4__c, Output_Name_5__c, Output_Name_6__c, Output_Name_7__c, Output_Name_8__c, Output_Name_9__c, Output_Name_10__c, Output_Name_11__c, Output_Name_12__c, Output_Name_13__c, Output_Name_14__c, Output_Name_15__c, Output_Name_16__c, Output_Name_17__c, Output_Name_18__c, Output_Name_19__c, Output_Name_20__c FROM Account_Compute_Final_Copy__c WHERE Measure_Master__c=:selectedRule LIMIT 1];
        if(String.isNotBlank(selectedMatrix) && (accountComputeFinalList != null && accountComputeFinalList.size() >0)){
            pieChartData = new list<PieChart>();
            list<GridMaster_Copy__c> copyMatrix = [SELECT Id, Name, Dimension_2_Name__c, Dimension_1_Name__c FROM GridMaster_Copy__c WHERE Matrix_Original__c=:selectedMatrix LIMIT 1];
            GridMaster_Copy__c matrix;
            if(copyMatrix != null && copyMatrix.size() >0){
                matrix = copyMatrix[0];
            }else{
                matrix = copyMatrix(selectedMatrix);
            }
            selectedMatrixCopy = matrix.Id;
            system.debug('--selectedMatrixCopy-- ' + selectedMatrixCopy);
            if(matrix != null){
                dimention1 = matrix.Dimension_1_Name__c;
                dimention2 = matrix.Dimension_2_Name__c;
            }

            String gp1;
            String gp2;
            String op1;
            Step__c matricesStep = [SELECT Id, Name, Matrix__c, Matrix__r.Name, Grid_Param_1__r.Parameter_Name__c, Grid_Param_2__r.Parameter_Name__c FROM Step__c WHERE Matrix__c=:selectedMatrix AND Measure_Master__c=:selectedRule LIMIT 1];
            if(matricesStep != null){
                gp1 = matricesStep.Grid_Param_1__r.Parameter_Name__c;
                gp2 = matricesStep.Grid_Param_2__r.Parameter_Name__c;
                op1 = matricesStep.Name;
            }
            system.debug('--gp1-- '  +gp1);
            system.debug('--gp2-- '  +gp2);
            system.debug('--op1-- '  +op1);
            list<Grid_Details_Copy__c> matrixDetails = [SELECT Id, Name, Dimension_1_Value__c, Dimension_2_Value__c, Output_Value__c FROM Grid_Details_Copy__c WHERE GridMaster_Copy__c=:matrix.Id];
            mapGridDetails = new Map<String, String>();
            mapCellDetails = new Map<String, String>();
            hcpCountPerCell = new Map<String, Integer>();
            rows = new Set<String>();
            cols = new Set<String>();
            Set<String> dim = new Set<String>();
            dimOneMap = new Map<String, Integer>();
            dimTwoMap = new Map<String, Integer>();
            dimn1 = new Set<String>();
            dimn2 = new Set<String>();
            dimn3 = new Set<String>();

            system.debug('+++++++++++++ matrixDetails' + matrixDetails);
            for(Grid_Details_Copy__c gd : matrixDetails){
                mapGridDetails.put(gd.Dimension_1_Value__c.toUpperCase() +'_'+ gd.Dimension_2_Value__c.toUpperCase(), gd.Output_Value__c.toUpperCase());
                hcpCountPerCell.put(gd.Dimension_1_Value__c.toUpperCase() +'_'+ gd.Dimension_2_Value__c.toUpperCase(), 0);
                mapCellDetails.put(gd.Name, gd.Dimension_1_Value__c.toUpperCase() +'_'+ gd.Dimension_2_Value__c.toUpperCase());
                rows.add(gd.Dimension_1_Value__c.toUpperCase());
                cols.add(gd.Dimension_2_Value__c.toUpperCase());
                dim.add(gd.Output_Value__c.toUpperCase());
                dimOneMap.put(gd.Dimension_1_Value__c.toUpperCase() +'_'+gd.Output_Value__c.toUpperCase(), 0);
                dimTwoMap.put(gd.Dimension_2_Value__c.toUpperCase() +'_'+gd.Output_Value__c.toUpperCase(), 0);
                dimn1.add(gd.Dimension_1_Value__c.toUpperCase());
                dimn2.add(gd.Dimension_2_Value__c.toUpperCase());
                dimn3.add(gd.Output_Value__c.toUpperCase());
                system.debug('-gd.Output_Value__c.toUpperCase()-- ' + gd.Output_Value__c.toUpperCase());
            }


            system.debug('++++++++++ Hey Map cELL DETAIL mapGridDetails'+ mapGridDetails);
            system.debug('++++++++++ Hey Map cELL DETAIL hcpCountPerCell'+ hcpCountPerCell);
            system.debug('++++++++++ Hey Map cELL DETAIL mapCellDetails'+ mapCellDetails);
            system.debug('++++++++++ Hey Map cELL DETAIL rows'+ rows);
            system.debug('++++++++++ Hey Map cELL DETAIL cols'+ cols);
            system.debug('++++++++++ Hey Map cELL DETAIL dim'+ dim);
            system.debug('++++++++++ Hey Map cELL DETAIL dimOneMap'+ dimOneMap);
            system.debug('++++++++++ Hey Map cELL DETAIL dimTwoMap '+ dimTwoMap);
            system.debug('++++++++++ Hey Map cELL DETAIL dimn1 '+ dimn1);
            system.debug('++++++++++ Hey Map cELL DETAIL dimn2 '+ dimn2);
            system.debug('++++++++++ Hey Map cELL DETAIL dimn3 '+ dimn3);


            selectOptionList = new list<SelectOption>();
            list<String> segments = new list<String>();
            segments.addAll(dim);
            segments.sort();
            for(String a: segments){
                selectOptionList.add(new SelectOption(a, a));
            }

            System.debug('---- ' + dimOneMap);
            System.debug('--mapCellDetails-- ' + mapCellDetails);
            colCount1 = cols.size();
            rowCount1 = rows.size();
            Account_Compute_Final_Copy__c accountComputeFinal = accountComputeFinalList[0];
            Integer counter = 1;
            system.debug('--accountComputeFinal--' + accountComputeFinal);
            field1 = '';
            field2 = '';
            output = '';
            while(counter <= 20){
                String key = (String)accountComputeFinal.get('Output_Name_'+counter+'__c');
                if(key == gp1){
                    field1 = 'Output_Value_'+counter+'__c';
                }

                if(key == gp2){
                    field2 = 'Output_Value_'+counter+'__c';
                }

                if(key == op1){
                    output = 'Output_Value_'+counter+'__c';
                }
                counter += 1;
            }

            Set<String> allPhy = new Set<String>();

            String query = 'SELECT Id, Physician_2__c, '+field1+', '+field2+', '+output+' FROM Account_Compute_Final_Copy__c WHERE Measure_Master__c=:selectedRule';
            System.debug('--query--' + query);
            if(String.isNotEmpty(field1) && String.isNotBlank(field2) && String.isNotBlank(output)){
                //list<sObject> allData = Database.query(query);
                for(Segment_Simulation__c acp: [SELECT Distinct_Recs__c,Count_of_Accounts__c, AdopPtSegDistinct__c, Dimension1__c,Dimension2__c,External_ID__c,Id, Measure_Master__c,Name,Segment__c,Sum_Proposed_TCF__c,Sum_TCF__c FROM Segment_Simulation__c WHERE Measure_Master__c=:selectedRule])
                {
                   // system.debug('++++++++++++ Hey acp is '+ (String)acp.get('Physician_2__c'));
                    //if(!allPhy.contains((String)acp.get('Physician_2__c')))
                    //{     
                        //allPhy.add((String)acp.get('Physician_2__c'));           
                        String key = ((String)acp.Dimension1__c.toUpperCase() +'_'+ ((String)acp.Dimension2__c.toUpperCase() != null ? (String)acp.Dimension2__c.toUpperCase() : 'ND'));
                        Integer hcpCount;

                        System.debug('++++++++++ Hey Key is '+ key);
                        System.debug('++++++++++ Hey hcpCountPerCell is '+ hcpCountPerCell);
                        if(hcpCountPerCell.containsKey(key)){
                            hcpCount = hcpCountPerCell.get(key);
                        }else{
                            hcpCount = 0;
                        }
                        hcpCount = Integer.valueof(acp.AdopPtSegDistinct__c);

                        Integer count2;
                        if(((String)acp.Dimension1__c) != null && ((String)acp.Segment__c) != null){
                            String key2 = ((String)acp.Dimension1__c).toUpperCase() +'_'+ ((String)acp.Segment__c).toUpperCase();
                            if(dimOneMap.containsKey(key2)){
                                count2 = dimOneMap.get(key2);
                            }else{
                                count2 = 0;
                            }
                            count2 += 1;
                            dimOneMap.put(key2, Integer.valueOf(acp.AdopPtSegDistinct__c));

                            system.debug('++++++++++++ Hey Guys '+ key2);
                            system.debug('++++++++++++ Hey Guys '+ acp.Sum_Proposed_TCF__c);
                            system.debug('++++++ dimOneMap '+ dimOneMap);
                        }
        
                        Integer count3;
                        if(((String)acp.Dimension2__c) != null && ((String)acp.Segment__c) != null){
                            String key3 = ((String)acp.Dimension2__c).toUpperCase() +'_'+ ((String)acp.Segment__c).toUpperCase();
                            if(dimTwoMap.containsKey(key3)){
                                count3 = dimTwoMap.get(key3);
                            }else{
                                count3 = 0;
                            }
                            count3 += 1;
                            dimTwoMap.put(key3, Integer.valueof(acp.AdopPtSegDistinct__c));
                        }
                        hcpCount += 1;
                        hcpCountPerCell.put(key,Integer.valueof(acp.AdopPtSegDistinct__c));
                    //}
                }
    
                for(String d: dimn3){
                    for(String k: dimn1){
                        if(!dimOneMap.containsKey(k+'_'+d)){
                            dimOneMap.put(k+'_'+d, 0);
                        }
                    }
                }
    
                for(String d: dimn3){
                    for(String k: dimn2){
                        if(!dimTwoMap.containsKey(k+'_'+d)){
                            dimTwoMap.put(k+'_'+d, 0);
                        }
                    }
                }
                System.debug('---- ' + dimOneMap);
    
                system.debug('--hcpCountPerCell--' + hcpCountPerCell);
                if(hcpCountPerCell != null && hcpCountPerCell.size() > 0){
                    flag = true;
                }
    
                Map<String, Integer> piChartSummation = new Map<String, Integer>();
                for(String key : mapGridDetails.keyset()){
                    Integer tempCount;
                    if(piChartSummation.containsKey(mapGridDetails.get(key))){
                        tempCount = piChartSummation.get(mapGridDetails.get(key));
                    }else{
                        tempCount = 0;
                    }
                    system.debug('--key-- '  + key);
                    tempCount += hcpCountPerCell.get(key);
                    piChartSummation.put(mapGridDetails.get(key), tempCount);
                }
    
                colorSet = '';
                list<String> colorSetList = new list<String>();
                list<String> keyset = new list<String>();
                keyset.addAll(piChartSummation.keySet());
                keyset.sort();
                for(String key : keyset){
                    if(key == 'A'){
                        colorSetList.add('#FFCCCC');
                    }else if(key == 'B'){
                        colorSetList.add('#FFCC99');
                    }else if(key == 'C'){
                        colorSetList.add('#CCE5FF');
                    }else if(key == 'D'){
                        colorSetList.add('#A5E0B4');
                    }else if(key == 'ND'){
                        colorSetList.add('#ADC7CF');
                    }
                    pieChartData.add(new PieChart(key, piChartSummation.get(key)));
                }
                colorSet = String.join(colorSetList, ',');
            }else{
                mapGridDetails = new Map<String, String>();
                mapCellDetails = new Map<String, String>();
                hcpCountPerCell = new Map<String, Integer>();
                rows = new Set<String>();
                cols = new Set<String>();
                dim = new Set<String>();
                dimOneMap = new Map<String, Integer>();
                dimTwoMap = new Map<String, Integer>();
                dimn1 = new Set<String>();
                dimn2 = new Set<String>();
                dimn3 = new Set<String>();
                throw new MyException();
            }
            showSimulation = true;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Data Found!'));
        }
    }

    public void changeMatrixData(){
        System.debug('--selectedMatrixCopy-- ' + selectedMatrixCopy);
        System.debug('--selectedMatrixCopy-- ' + chnagedGridValue);
        system.debug('++++++++++ Hey Selected Dimension is '+ selectedDimension);
        if(String.isNotBlank(chnagedGridValue) && String.isNotBlank(selectedMatrixCopy))
        {
            List<Grid_Details_Copy__c> changeGd = [SELECT Id, Output_Value__c, Dimension_1_Value__c, Dimension_2_Value__c FROM Grid_Details_Copy__c WHERE Name =:chnagedGridValue AND GridMaster_Copy__c =: selectedMatrixCopy];
            
            for(Grid_Details_Copy__c gd : changeGd)
                gd.Output_Value__c = selectedDimension;
            update changeGd;
            
            system.debug('++++++++ Hey Changed GD' + changeGd);
            
            list<Segment_Simulation__c> updateAccountComputeFinalsCopy = new list<Segment_Simulation__c>();
            for(sObject acfCopy : Database.query('SELECT Id, Segment__c FROM Segment_Simulation__c WHERE Measure_Master__c = \'' +selectedRule+ '\' AND Dimension1__c = \'' + changeGd[0].Dimension_1_Value__c + '\' AND Dimension2__c = \'' + changeGd[0].Dimension_2_Value__c + '\' ')){
                Segment_Simulation__c acfc = new Segment_Simulation__c();
                acfc.put('Id', (Id)acfCopy.get('Id'));
                acfc.put('Segment__c', selectedDimension);
                updateAccountComputeFinalsCopy.add(acfc);
            }
            try
            {
                //update updateAccountComputeFinalsCopy;    
            }
            catch(Exception ex)
            {
                system.debug('++++++++++++ Message' + ex.getMessage());
            }
            
            system.debug('+++'+ updateAccountComputeFinalsCopy);
            update updateAccountComputeFinalsCopy;
            system.debug('+++'+ updateAccountComputeFinalsCopy);
        }


        fillMatrixDetails();
    }

    public class PieChart
    {
        public String name {get;set;}
        public Integer data {get;set;}

        public PieChart(String name1, Integer data1)
        {
            name = name1 + ' : ' + data1;
            data = data1;
        }
    }
    
    public class MyException extends Exception {}


    public void pushToBusinessRules()
    {
        system.debug('============= Hey Selected Rule is '+ selectedRule);
        system.debug('============= Hey Selected Matrix is '+ selectedMatrixCopy);

        Measure_Master__c rule = [SELECT Id, State__c, Brand_Lookup__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id=:selectedRule];

        if(rule.State__c == 'Executed')
        {
            Grid_Master__c gm = [SELECT Brand_Team_Instance__c,Brand__c,Col__c,Country__c, Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c, Name,Output_Name__c,Output_Type__c,OwnerId,Row__c ,Unique_Name__c FROM Grid_Master__c where id = :selectedMatrix];

            List<Grid_Details_Copy__c> changeGd = [SELECT Id, Name, Output_Value__c, Dimension_1_Value__c, Dimension_2_Value__c FROM Grid_Details_Copy__c WHERE GridMaster_Copy__c =: selectedMatrixCopy];

            delete [select id from Grid_Details__c where Grid_Master__c = :selectedMatrix];

            List<Grid_Details__c> gdList = new List<Grid_Details__c>();

            for(Grid_Details_Copy__c gdc : changeGd)
            {
                Grid_Details__c gd = new Grid_Details__c();
                gd.Name = gdc.Name;
                gd.Output_Value__c = gdc.Output_Value__c;
                gd.Dimension_1_Value__c = gdc.Dimension_1_Value__c;
                gd.Dimension_2_Value__c = gdc.Dimension_2_Value__c;
                gd.Grid_Master__c = selectedMatrix;

                gdList.add(gd);
            }

            insert gdList;

            system.debug('+++++++++++++++++++ Hello Friends '+ changeGd);
            String WhereClause = ' Brand__r.Brand__c =\''+rule.Brand_Lookup__c+'\' AND Cycle__c =\''+rule.Team_Instance__c+'\' AND Cycle2__c =\''+rule.Cycle__c+'\' ';//'\' AND Cycle__c =\''+rule.Team_Instance__c+

            BatchDeleteRecsBeforereExecute batchExecute = new BatchDeleteRecsBeforereExecute(rule.ID, WhereClause );
            Database.executeBatch(batchExecute, 1000);
            rule.State__c = 'Executing';
            update rule;
            

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Business Rule work in Progress'));            
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Business Rule not allowed to be executed Again'));               
        }

    }
}