/**********************************************************************************************
@author     : Unknown
@date       : Unknown
@description: This Class is used for show Peoplesoft events in workbench.
@Client     : Insmed
Revison(s)  : Ankit Manendra - 22 Nov'17
**********************************************************************************************/

global class PeoplesoftWorkBenchCtrl{
     //public string sortField{get;set;}
    public string msg{get;set;}
    public String soqlsort                      {get;set;}
    public boolean sortflag                     {get;set;}
    public map<string,object>mapRemovedIdToflag {get;set;}
    public list<CR_Employee_Feed__c> cremplist  {get;set;}
    public String JsonMap                       {get;set;} 
    public string testing                       {get;set;}
    // FOR TEVA== Added by Dhruv== 10-july'17
    public Integer terminate_Employee           {get;set;}
    public Integer leave_of_Absence             {get;set;}
    public Integer promote_Employee             {get;set;}
    public Integer EmployeeNewHire              {get;set;}
    public Integer TransferEmployee             {get;set;}
    public Integer Employeedemotion             {get;set;}
    public Integer Transfer_to_HO               {get;set;}
    public Integer TransfertoField              {get;set;}
    public Integer TransferoutOfSales           {get;set;}
    public Integer allEvents                    {get;set;}
    public string rowId                         {get;set;}
    public Boolean errorFlag                    {get;set;}
    public Boolean displayPopup                 {get;set;}
    public Boolean removePopUp                  {get;set;}
    public String profileName                   {get;set;}
    public boolean sendemailcolumnhidden        {get;set;}  
    public CR_Employee_Feed__c empfeed          {get;set;}
    public Integer rehireCount                  {get;set;}              
    
    public list<CR_Employee_Feed__c> SortCREmpList = new list<CR_Employee_Feed__c>();
    Id profileId = userinfo.getProfileId();
    
    public PeoplesoftWorkBenchCtrl(){
        
        sendemailcolumnhidden = false;
        mapRemovedIdToflag=new map<string,object>();
        JsonMap='';
        sortflag = True;
        testing='oo';
        
        // FOR TEVA== Added by Dhruv== 10-july'17
        terminate_Employee  = 0;
        leave_of_Absence    = 0; 
        promote_Employee    = 0; 
        EmployeeNewHire     = 0;
        TransferEmployee    = 0;
        Employeedemotion    = 0;
        Transfer_to_HO      = 0;
        TransfertoField     = 0;
        TransferoutOfSales  = 0;
        allEvents           = 0;
        rehireCount         = 0;
        
        //Dt is used for Archival e.g if the events need to be archived after 14 days then the Numbe rof days in the configuraton shoul be 14
        InsmedRosterConfig__c rosterconfig = InsmedRosterConfig__c.getValues('Config1'); 
        
        date dt = date.today().addDays(- integer.valueof(rosterconfig.Number_Of_Days__c));
        
        list<CR_Employee_Feed__c> crList = [SELECT Id, Event_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false AND createdDate >: dt];
        for(CR_Employee_Feed__c cr : crList){
            if(cr.Event_Name__c == system.label.Terminate_Employee){
                terminate_Employee++;
            }
            /*if(cr.Event_Name__c == 'Transfer to HO'){
                Transfer_to_HO++;
            }*/
            if(cr.Event_Name__c == system.label.Leave_of_Absence){
                leave_of_Absence++;
            }
            /*if(cr.Event_Name__c == system.label.Promote_Employee){
                promote_Employee++;
            }*/
            if(cr.Event_Name__c == system.label.Employee_Newhire){
                EmployeeNewHire++;
            }
            /*if(cr.Event_Name__c == system.label.Demote_Employee){
                Employeedemotion++;
            }*/
            /*if(cr.Event_Name__c == system.label.Transfer_out_of_Sales_team){
                TransferoutOfSales++ ;
            }*/
            
            /*if(cr.Event_Name__c == system.label.Transfer_employee){
                TransferEmployee++ ;
            }
            if(cr.Event_Name__c == system.label.transfer_to_field){
                TransfertoField++;
            }*/
            if(cr.Event_Name__c == system.label.Employee_Rehire){
                rehireCount++;
            }
        }                   // == ENDS===//
        
        list<CR_Employee_Feed__c> workBenchCount = [select id from CR_Employee_Feed__c where  IsRemoved__c = false];
        allEvents = workBenchCount.size();
        
        Id profileId = userinfo.getProfileId();
        profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName == system.label.ColumnProfileWise){
            sendemailcolumnhidden = true;
        }
    }
    
    public void methodOne(){}
    
    public void closePopup(){    
        //errorFlag=false;
        displayPopup = false;
    }        
    
    public void showPopup(){
        displayPopup = true;
    }
    
    
    public void removeEventFromWorkbench(){
        removePopUp = true;
    }
    
    public void Cancel_remove_from_workbench(){
        removePopUp = false;
    }
    
    public void refresh(){ 
        testing='iiiii';
        if(JsonMap!=null&& JsonMap!=''){
            system.debug('=====JsonMap======'+JsonMap);
            mapRemovedIdToflag=( map<string,object>)JSON.deserializeUntyped(JsonMap);
            system.debug('mapremovedId'+mapRemovedIdToflag.keySet());
            list<string>feedIdList=new list<string>();

            for (String key : mapRemovedIdToflag.keySet()){
                system.debug('===key==='+key);
                if(key != null)
                feedIdList.add(key);
            }
           
            list<CR_Employee_Feed__c> updatecreList=new list<CR_Employee_Feed__c>();
            list<CR_Employee_Feed__c> existingcreList = [select IsRemoved__c,Employee__c from CR_Employee_Feed__c where id in:feedIdList];
            
            set<id> employeeId= new set<id>();
            for(CR_Employee_Feed__c cr : existingcreList){
                cr.IsRemoved__c=true; 
                updatecreList.add(cr);
                system.debug('==updatecreList=='+updatecreList);
                employeeId.add(cr.Employee__c);    
                system.debug('==log=='+cr.Employee__c);        
            }
            
            system.debug('==log=='+employeeId);
            list<AxtriaSalesIQTM__Employee__c> lstUpdateEmp = new list<AxtriaSalesIQTM__Employee__c>();
            list<AxtriaSalesIQTM__Employee__c> lstEmp = [select id,IsWorkbench__c from AxtriaSalesIQTM__Employee__c 
                                                            where id in: employeeId];
            for(AxtriaSalesIQTM__Employee__c emp: lstEmp){
                emp.IsWorkbench__c= false;
                lstUpdateEmp.add(emp);
            }
            update lstUpdateEmp;                //=============Ends New Code================//
            Update updatecreList;
            JsonMap='';
        }
        else{
            JsonMap='';
            system.debug('error msg+++++++++++++++');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please checked atleast one checkbox');
            ApexPages.addMessage(myMsg);
        }
        removePopUp = false;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Event has been removed from workbench.');
        ApexPages.addMessage(myMsg);
        //cremplist= new list<CR_Employee_Feed__c>();
    }
    
    Date d=system.today();
    String y=String.valueOf(d);
    
    
  
   @RemoteAction      
    global static list<CR_Employee_Feed__c> Feed(string eventType)
    {   
        list<CR_Employee_Feed__c> cremplistNew= new list<CR_Employee_Feed__c>();
        cremplistNew = [Select id,Position__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c
                        FROM CR_Employee_Feed__c 
                        where  IsRemoved__c=false order by id];
      
        set<string> empData= new set<string>();
        map<string,CR_Employee_Feed__c> mapEmp= new map<string,CR_Employee_Feed__c>();
        
        //Get all employees from workday and their respective object
        for(CR_Employee_Feed__c emp : cremplistNew){
            empData.add(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c);
            mapEmp.put(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c, emp);
        }
        //Get data from position employee
        map<string, AxtriaSalesIQTM__Position__c> mapPos= new map<string, AxtriaSalesIQTM__Position__c>();
       
        for(AxtriaSalesIQTM__Position__c pos : [select id, AxtriaSalesIQTM__Position_Type__c,
                                                AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                                AxtriaSalesIQTM__Parent_Position__c
                                                from AxtriaSalesIQTM__Position__c
                                                where AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c in: empData])
        {
            mapPos.put(pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, pos);
        }
        
        list<CR_Employee_Feed__c> lstFeed= new list<CR_Employee_Feed__c>();
        for(string empId : mapPos.keyset())
        {
            AxtriaSalesIQTM__Position__c pos=mapPos.get(empId);
            CR_Employee_Feed__c feed=mapEmp.get(empId);
            feed.Position__c =pos.id;
            
            lstFeed.add(feed);
        }
        update lstFeed;
        
        ////
        cremplistNew= new list<CR_Employee_Feed__c>();
        if(eventType != Null){
            list<String> eType = eventType.split(':'); 
            cremplistNew = [Select id,ChangeRequest__r.Name,IsRemoved__c,Request_Date1__c,Employee__r.ReportingToWorkerName__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_ID__c, 
                            Employee__r.Current_Position__r.Name,Employee__r.AssociateOID__c,Employee__r.Current_Position__c,Employee__r.Name, Employee__r.Id,Status_Completion_Date__c,
                            Training_Completion_Date1__c,Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,createdDate,Employee__r.AxtriaSalesIQTM__Job_Title__c,
                            Event_Name__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name,Employee__r.JobChangeReason__c,
                            Field_Force__c,Employee__r.AxtriaSalesIQTM__Division__c,Status__c,Employee__r.Employee_Status__c,Employee__r.Department__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Team_iD__r.name,Employee__r.JobCodeName__c,
                            Employee__r.Current_Position__r.Id,StatusCompletionDateString__c,CreatedDateString__c 
                            FROM CR_Employee_Feed__c where Event_Name__c  IN :eType AND IsRemoved__c=false order by id];
        
        }
        else{
            cremplistNew = [Select id,ChangeRequest__r.Name,IsRemoved__c,Employee__r.ReportingToWorkerName__c,Request_Date1__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_ID__c, 
                            Employee__r.Current_Position__r.Name,Employee__r.AssociateOID__c,Employee__r.Current_Position__c,Employee__r.Name, Employee__r.Id,Status_Completion_Date__c,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c,
                             Employee__r.Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Employee__r.Employee_Status__c,Employee__r.AxtriaSalesIQTM__Job_Title__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,createdDate,
                            Event_Name__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name,Employee__r.JobChangeReason__c,
                            Field_Force__c,Employee__r.AxtriaSalesIQTM__Division__c,Status__c,Employee__r.Department__c,Employee__r.JobCodeName__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Team_iD__r.name,
                            Employee__r.Current_Position__r.Id,StatusCompletionDateString__c,CreatedDateString__c   
                            FROM CR_Employee_Feed__c where  IsRemoved__c=false order by id];
        
        }
        
        return  cremplistNew;
    }
    
     //----- Added by Dhruv Mahajan ------//
     public String sortDir {
        // To set a Direction either in ascending order or descending order.
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;}
        set;
    }
    
    public String sortField {
        // To set a Field for sorting.
        get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
        set;
    }
    
     public void toggleSort() {
      
        system.debug('++++++++cremplist++++++++ ' + cremplist); 
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        soqlsort = 'Select id, name,Title__c, ChangeRequest__r.Name,Employee__r.Current_Position__c, Request_Date1__c, Position__r.AxtriaSalesIQTM__Position_ID__c,'+
                   'Position__r.Name, Employee__r.Name, Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c,'+
                   'Position__r.AxtriaSalesIQTM__Position_Type__c, Employee__r.AxtriaSalesIQTM__Employee_ID__c,'+
                   'Event_Name__c,Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c,  Employee__r.AxtriaSalesIQTM__Division__c,'+
                           'Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c '+
                   'from CR_Employee_Feed__c  where  Status__c!=\''+String.escapeSingleQuotes('Removed')+'\'';
         system.debug('---soqlsort---'+soqlsort);          
        // Adding String array to a List array
        SortCREmpList = Database.query(soqlsort + ' order by ' + sortField + ' ' + sortDir +' NULLS LAST limit 1000'); 
        system.debug('++++++++SortCREmpList++++++++ ' + SortCREmpList);
        sortflag = false; 
        Feed(' ');
        //----- Added by Dhruv Mahajan ------//
     
    }
    
    public void sendEmail(){
        if(rowid != Null){
           empfeed = [Select id,Title__c,ChangeRequest__r.Name,IsRemoved__c,Request_Date1__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_ID__c, 
                            Employee__r.Current_Position__r.Name,Employee__r.Current_Position__c,Employee__r.Name, Employee__r.Id,Status_Completion_Date__c,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,createdDate,
                            Event_Name__c,Employee__r.Current_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, 
                            Field_Force__c,Employee__r.AxtriaSalesIQTM__Division__c,Status__c,Employee__r.AssociateOID__c,
                            Employee__r.Current_Position__r.AxtriaSalesIQTM__Team_iD__r.name,
                            Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c, Employee__r.AxtriaSalesIQTM__Manager__r.Name,
                            Employee__r.AxtriaSalesIQTM__Current_Territory__r.Name, Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Team_iD__r.Name,
                            Employee__r.AxtriaSalesIQTM__FirstName__c, Employee__r.AxtriaSalesIQTM__Last_Name__c,CreatedDateString__c
                            FROM CR_Employee_Feed__c where  id=:rowid]; 
                            
           Map<String, InsmedRosterConfig__c> mcs = InsmedRosterConfig__c.getAll();
           list<string> toAddress = new list<string>();
           for(InsmedRosterConfig__c tConfig : mcs.values()){
               if(tConfig.Event_Type__c == empfeed.Event_Name__c){
                   //toAddress = toAddress != Null ? toAddress +','+ tConfig.Emails__c : tConfig.Emails__c; 
                   if(tConfig.Emails__c != null){
                       toAddress = tConfig.Emails__c.split(',');
                   }   
               }
           }
           Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            //message.toAddresses = new String[] {toAddress};
            message.toAddresses = toAddress;
            message.optOutPolicy = 'FILTER';
            
            string salesForceCode = empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Team_iD__r.Name;
            if(empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Team_iD__r.Name == Null){
                salesForceCode = '';
            }
            string currentTerritoryID = empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c;
            if(empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Client_Position_Code__c == NULL){
                currentTerritoryID = '';
            }
            string currentTerritoryName = empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.Name;
            if(empfeed.Employee__r.AxtriaSalesIQTM__Current_Territory__r.Name == NULL){
                currentTerritoryName = '';
            }
            /*string localEmpID = empfeed.Employee__r.Local_Employee_Id__c;
            if(empfeed.Employee__r.Local_Employee_Id__c == NULL){
                localEmpID = '';
            }*/
            string supervisorName = empfeed.Employee__r.AxtriaSalesIQTM__Manager__r.Name;
            if(empfeed.Employee__r.AxtriaSalesIQTM__Manager__r.Name == NULL){
                supervisorName = '';
            }
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{message};
            message.subject = empfeed.Event_Name__c+' Notification for '+empfeed.Employee__r.AxtriaSalesIQTM__FirstName__c+' '+empfeed.Employee__r.AxtriaSalesIQTM__Last_Name__c+', '+currentTerritoryID; 
             msg = '<html><body>'+
                            '<p>This is an automated email.Please do not reply to this email.'+
                            '<br/><br/>Date: '+Date.Today().format()+'<br/><br/>'+
                            '<p>Dear User,</b></p>'+ 
                            'Refer to the details below regarding ' +empfeed.Employee__r.AxtriaSalesIQTM__FirstName__c+' '+empfeed.Employee__r.AxtriaSalesIQTM__Last_Name__c+', '+currentTerritoryID+'<br/><br/>'+
                            '<table style="border-width:0px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0" width="100%"><tr>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Event Name</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Event Effective Date</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Sales Force</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">RMS Territory ID</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Employee Name</th>'+                            
                            //'<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Local Employee ID</th>'+
                            //'<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Global Employee ID</th>'+
                           // '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Position Title</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Territory Name</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Supervisor Name</th>';
                            
                            if(empfeed.Event_Name__c == system.label.Employee_Newhire){
                                msg +=  '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Home Address</th>'+
                                        '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Home Phone Number</th>';
                            }
               msg  +=     '</tr>';
            
            if(empfeed != Null){
                 msg += '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Event_Name__c+'</td>'+
                        //'<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.CreatedDate.format()+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.CreatedDateString__c+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+salesForceCode+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+currentTerritoryID+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Employee__r.AxtriaSalesIQTM__FirstName__c+' '+empfeed.Employee__r.AxtriaSalesIQTM__Last_Name__c+'</td>'+
                        //'<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+localEmpID+'</td>'+
                        //'<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Employee__r.Global_Employee_ID__c+'</td>'+
                       // '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Employee__r.Position_Title__c+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+currentTerritoryName+'</td>'+
                        '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+supervisorName+'</td>';
                        
                       /* if(empfeed.Event_Name__c == system.label.Employee_Newhire){
                        msg+=   '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Employee__r.Home_State_Province__c+'</td>'+
                                '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+empfeed.Employee__r.Home_Phone__c+'</td>';
                        }*/                
                    msg+=   '</td></tr>' ;
            }
            
            msg += '</table><br/><br/>Thank you<br/>Axtria SalesIQ Support<br/></body></html>' ;
            
            message.setHTMLbody(msg);
            system.debug('==============' + message);
            if(!Test.isRunningTest()){
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Mail Sent Successfully');
            ApexPages.addMessage(myMsg);        
        }
        closePopup();
    }
   
}