@isTest
private class DeleteCurrentPersonFeedTest {
	 @testSetup 
    static void setup() {
        List<CurrentPersonFeed__c> personList = new List<CurrentPersonFeed__c>();
       
             // insert 5 accounts
        for (Integer i=0;i<5;i++) {
            personList.add(new CurrentPersonFeed__c(First_Name__c='A '+i));
        } 
        insert personList;
       /* list<string> feedIds = new list<string>();
       for(CurrentPersonFeed__c feed: [select id from CurrentPersonFeed__c]){
    		feedIds.add(feed.id);
    	}
    	if(feedIds.size()!=0){
    		Database.delete(feedIds);
    	} */
    }
    static testmethod void test() {        
        Test.startTest();
        DeleteCurrentPersonFeed usa = new DeleteCurrentPersonFeed();
        Id batchId = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}