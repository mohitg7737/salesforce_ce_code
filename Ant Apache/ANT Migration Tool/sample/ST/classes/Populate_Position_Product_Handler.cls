public with sharing class Populate_Position_Product_Handler {
    
    public Populate_Position_Product_Handler() 
    {
        
    }

    public static void populatePosition(List<Team_Instance_Product_AZ__c> allPosProducts)
    {
    	Set<String> allTeamInstances = new Set<String>();
    	Map<String, Set<ID>> allProductsInTeam = new Map<String, Set<ID>>();
        Map<String, Decimal> TeamInstanceProductIDtoHolidaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoVacationDaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoOtherDaysOffMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoCallsperDayMap = new Map<String, Decimal>();
        
    	for(Team_Instance_Product_AZ__c tip : allPosProducts)
    	{
    		allTeamInstances.add(tip.Team_Instance__c);
    		String teamInstance = String.valueOf(tip.Team_Instance__c);
    		TeamInstanceProductIDtoHolidaysMap.put(teamInstance+tip.Product_Catalogue__c,tip.Holidays__c);
    		TeamInstanceProductIDtoVacationDaysMap.put(teamInstance+tip.Product_Catalogue__c,tip.Vacation_Days__c);
    		TeamInstanceProductIDtoOtherDaysOffMap.put(teamInstance+tip.Product_Catalogue__c,tip.Other_Days_Off__c);
    		TeamInstanceProductIDtoCallsperDayMap.put(teamInstance+tip.Product_Catalogue__c,tip.Calls_Day__c);

    		if(allProductsInTeam.containsKey(teamInstance))
    		{
    			allProductsInTeam.get(teamInstance).add(tip.Product_Catalogue__c);
    		}
    		else
    		{
    			Set<ID> tempSet = new Set<ID>();
    			tempSet.add(tip.Product_Catalogue__c);
    			allProductsInTeam.put(teamInstance, tempSet);
    		}
    	}

    	List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances];

    	List<AxtriaSalesIQTM__Position_Product__c> positionProductsToInsert = new List<AxtriaSalesIQTM__Position_Product__c>();

    	for(AxtriaSalesIQTM__Position__c pos : allPositions)
    	{
    		String teamInstance = pos.AxtriaSalesIQTM__Team_Instance__c;

    		for(ID product : allProductsInTeam.get(teamInstance))
    		{
    			AxtriaSalesIQTM__Position_Product__c posPro = new AxtriaSalesIQTM__Position_Product__c();
    			posPro.AxtriaSalesIQTM__Position__c = pos.id;
	            //obj.Product_Master__c = (Id) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Product_Master__c');
	            posPro.AxtriaSalesIQTM__Team_Instance__c =teamInstance;
				posPro.Product_Catalog__c = product;
	            posPro.Vacation_Days__c = TeamInstanceProductIDtoVacationDaysMap.get(teamInstance+product);//0;
	            posPro.Holidays__c = TeamInstanceProductIDtoHolidaysMap.get(teamInstance+product);//0;
	            posPro.Other_Days_Off__c = TeamInstanceProductIDtoOtherDaysOffMap.get(teamInstance+product);//0;
	            posPro.Calls_Day__c = TeamInstanceProductIDtoCallsperDayMap.get(teamInstance+product);//0;
	            //posPro.Business_Days_in_Cycle__c = 0;
	            posPro.AxtriaSalesIQTM__Product_Weight__c = 0;    
	            posPro.AxtriaSalesIQTM__isActive__c = true;
                posPro.AxtriaSalesIQTM__Effective_Start_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                posPro.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c;

                positionProductsToInsert.add(posPro);
    		
            }
    	}
    	insert positionProductsToInsert;
    }
    
    public static void deletePosition(List<Team_Instance_Product_AZ__c> allPosProducts)
    {
        Set<String> allTeamInstances = new Set<String>();
        Set<String> allProducts = new Set<String>();
        Set<String> posProdCombo = new Set<String>();
        system.debug('+++allPosProducts++'+allPosProducts);

        for(Team_Instance_Product_AZ__c ti :allPosProducts)
        {
            allTeamInstances.add(ti.Team_Instance__c);
            allProducts.add(ti.Product_Catalogue__c);
            posProdCombo.add(ti.Team_Instance__c + '_' + ti.Product_Catalogue__c);
        }

        List<AxtriaSalesIQTM__Position_Product__c> allPosProductsDel = [select id, AxtriaSalesIQTM__Position__c, Product_Catalog__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and Product_Catalog__c in :allProducts];
        
        List<AxtriaSalesIQTM__Position_Product__c> deletePosProduct = new List<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position_Product__c pp : allPosProductsDel)
        {
            if(posProdCombo.contains(pp.AxtriaSalesIQTM__Team_Instance__c + '_' + pp.Product_Catalog__c))
            {
                deletePosProduct.add(pp);
            }
        }

        delete deletePosProduct;
    }
    
}