@isTest
public class BatchExecuteRuleEngine_test {
    @istest static void gridtest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Measure_Master__c m = new Measure_Master__c();
        insert m;
        Account_Compute_Final__c a = new Account_Compute_Final__c();
        a.Measure_Master__c = m.Id;
        insert a;
        Test.startTest();
        System.runAs(loggedinuser){
        	BatchExecuteRuleEngine obj=new BatchExecuteRuleEngine(m.Id, ' ');
			Database.executeBatch(obj);
        }
        
        Test.stopTest();

    }
}