global class Integration_Batch_GAS_History_Data2 implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    public String query;
    public List<String> teamInstances;

    public Integer totalRecs;
    public Integer errorRecsCount;
    
    global Integration_Batch_GAS_History_Data2()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> ti = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current'];
        LIst<String> allTeams = new List<String>();
        teamInstances = new List<String>();
        for(AxtriaSalesIQTM__Team_Instance__c t : ti)
        {
            teamInstances .add(t.ID);
        }  
        
        totalRecs = 0;
        errorRecsCount = 0;
        this.query = 'select id, AxtriaARSnT__SIQ_Added_Territory__c, AxtriaARSnT__SIQ_Account__c, AxtriaARSnT__Run_Count__c, AxtriaARSnT__Deployment_Status__c from AxtriaARSnT__SIQ_GAS_History__c where (AxtriaARSnT__Deployment_Status__c = \'New\' OR AxtriaARSnT__Deployment_Status__c = null) OR (AxtriaARSnT__Deployment_Status__c = \'Rejected\' AND AxtriaARSnT__Run_Count__c <2)';

    }
    global Integration_Batch_GAS_History_Data2(List<String> teamInstances) {

        this.teamInstances = teamInstances;
        this.query = 'select id, AxtriaARSnT__SIQ_Added_Territory__c, AxtriaARSnT__SIQ_Account__c, AxtriaARSnT__Run_Count__c, AxtriaARSnT__Deployment_Status__c from AxtriaARSnT__SIQ_GAS_History__c where (AxtriaARSnT__Deployment_Status__c = \'New\' OR AxtriaARSnT__Deployment_Status__c = null) OR (AxtriaARSnT__Deployment_Status__c = \'Rejected\' AND AxtriaARSnT__Run_Count__c <2)';

        totalRecs = 0;
        errorRecsCount = 0;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }


   global void execute(System.SchedulableContext SC){

              
        database.executeBatch(new Integration_Batch_GAS_History_Data2());
    }

    global void execute(Database.BatchableContext BC, list<AxtriaARSnT__SIQ_GAS_History__c> scope) {
        
        
        Set<String> allAccounts = new Set<String>();
        Set<String> allPositionsSet = new Set<String>();
        Map<String, String> atlMap = new Map<String,String>();
        Map<String,String> veevaToSalesIQID = new Map<String,String>();
        Map<String,String> posIDMapping = new Map<String,String>();
        Map<String,String> posToTeaInstanceMap = new Map<String,String>();

        List<AxtriaSalesIQTM__Position_Account__c> newPositionAccounts = new List<AxtriaSalesIQTM__Position_Account__c>();


        for(AxtriaARSnT__SIQ_GAS_History__c sgh : scope)
        {
            allAccounts.add(sgh.AxtriaARSnT__SIQ_Account__c);
            allPositionsSet.add(sgh.AxtriaARSnT__SIQ_Added_Territory__c);
            totalRecs ++;
        }  

        List<AxtriaARSnT__Staging_ATL__c> allStagingRecs = [select AxtriaARSnT__Axtria_Account_ID__c, AxtriaARSnT__Account__c, AxtriaARSnT__Territory__c from AxtriaARSnT__Staging_ATL__c where AxtriaARSnT__Account__c in :allAccounts];
        List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :allPositionsSet and AxtriaSalesIQTM__Team_Instance__c in :teamInstances];

        for(AxtriaSalesIQTM__Position__c pos : allPositions)
        {
            posIDMapping.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.ID);
            posToTeaInstanceMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.AxtriaSalesIQTM__Team_Instance__c);
        }
        
        for(Account acc : [select id, AxtriaARSnT__AZ_VeevaID__c from Account where AxtriaARSnT__AZ_VeevaID__c in :allAccounts])
        {
            veevaToSalesIQID.put(acc.AxtriaARSnT__AZ_VeevaID__c, acc.id);
        }

        for(AxtriaARSnT__Staging_ATL__c sa : allStagingRecs)
        {
            atlMap.put(sa.AxtriaARSnT__Account__c, sa.AxtriaARSnT__Territory__c);
        }

        //NEED TO ADD RECORDS TO CP AFTER Global exclusion rule
        List<AxtriaARSnT__Gas_History_Error_Records__c> errorRecs = new List<AxtriaARSnT__Gas_History_Error_Records__c>();

        for(AxtriaARSnT__SIQ_GAS_History__c sgh : scope)
        {
            //if(atlMap.containsKey(sgh.AxtriaARSnT__SIQ_Account__c))
            //{
            
                List<String> territoriesList = new List<String>();

                String terrs;
                
                if(atlMap.containsKey(sgh.AxtriaARSnT__SIQ_Account__c))
                {
                    terrs = atlMAp.get(sgh.AxtriaARSnT__SIQ_Account__c);
                
                    if(terrs != null)
                    {
                        territoriesList = terrs.split(';');
                    }                    
                }
                
                if(terrs != null)
                {
                    territoriesList = terrs.split(';');
                }
               
                if(!territoriesList.contains(sgh.AxtriaARSnT__SIQ_Added_Territory__c))
                {
                    AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
                    posAcc.AxtriaSalesIQTM__Team_Instance__c = posToTeaInstanceMap.get(sgh.AxtriaARSnT__SIQ_Added_Territory__c);
                    sgh.AxtriaARSnT__Run_Count__c = sgh.AxtriaARSnT__Run_Count__c + 1;
                    if(veevaToSalesIQID.containsKey(sgh.AxtriaARSnT__SIQ_Account__c))
                    {
                        posAcc.AxtriaSalesIQTM__Account__c = veevaToSalesIQID.get(sgh.AxtriaARSnT__SIQ_Account__c);    
                    }
                    else
                    {
                        errorRecs.add(new AxtriaARSnT__Gas_History_Error_Records__c(AxtriaARSnT__SIQ_GAS_History__c = sgh.id, AxtriaARSnT__Reason__c = 'Account Not Found'));
                        //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                        sgh.AxtriaARSnT__Deployment_Status__c = 'Rejected';
                        errorRecsCount++;
                        continue;
                    }

                    if(posIDMapping.containsKey(sgh.AxtriaARSnT__SIQ_Added_Territory__c))
                        posAcc.AxtriaSalesIQTM__Position__c = posIDMapping.get(sgh.AxtriaARSnT__SIQ_Added_Territory__c);
                    else
                    {
                        errorRecs.add(new AxtriaARSnT__Gas_History_Error_Records__c(AxtriaARSnT__SIQ_GAS_History__c = sgh.id, AxtriaARSnT__Reason__c = 'Territory Not Found'));
                        //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                        sgh.AxtriaARSnT__Deployment_Status__c = 'Rejected';
                        errorRecsCount++;
                        continue;
                    }

                    posAcc.AxtriaSalesIQTM__Effective_Start_Date__c =  System.today();
                    posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2099,12,31);
                    newPositionAccounts.add(posAcc);
                    sgh.AxtriaARSnT__Deployment_Status__c = 'Processed';
                }
                else
                {
                    errorRecs.add(new AxtriaARSnT__Gas_History_Error_Records__c(AxtriaARSnT__SIQ_GAS_History__c = sgh.id, AxtriaARSnT__Reason__c = 'Account already assigned to territory'));
                    errorRecsCount++;
                    //sgh.Deployment_Status__c = 'Rejected';
                    //sgh.AxtriaARSnT__Deployment_Status__c = 'Not Required';
                }

            //}
            /*else
            {
                errorRecs.add(new AxtriaARSnT__Gas_History_Error_Records__c(AxtriaARSnT__SIQ_GAS_History__c = sgh.id, AxtriaARSnT__Reason__c = 'Account Not Found'));
                //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                sgh.AxtriaARSnT__Run_Count__c = sgh.AxtriaARSnT__Run_Count__c + 1;
                errorRecsCount++;
                continue;
            }*/

        }



        if(errorRecs.size()>0)
            insert errorRecs;

        insert newPositionAccounts;

        update scope;
    }

    global void finish(Database.BatchableContext BC) {
         AxtriaARSnT__Scheduler_Log__c slog = new AxtriaARSnT__Scheduler_Log__c();


        slog.AxtriaARSnT__Changes__c = 'GAS Assignment';
        //slog.AxtriaARSnT__Created_Date__c = DateTime.now();
        slog.AxtriaARSnT__Created_Date2__c = DateTime.now();
        //s1.Cycle__c = 
        slog.AxtriaARSnT__Job_Type__c = 'Inbound';
        slog.AxtriaARSnT__Job_Name__c = 'GAS Assignment';
        slog.AxtriaARSnT__Job_Status__c = 'Executed';
        slog.AxtriaARSnT__No_of_Records_Processed__c = totalRecs;
        slog.AxtriaARSnT__Total_Errors__c = errorRecsCount;

        insert slog;
    }
}