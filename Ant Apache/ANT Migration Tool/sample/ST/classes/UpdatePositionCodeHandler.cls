/**********************************************************************************************
@author     : Pradeep Singh
@date       : 29 May 2018
**********************************************************************************************/

public with sharing class UpdatePositionCodeHandler{
    public static string grpsString = '';
    public static string grpOldString = '';
    
    public static void updatePositionCode(list<AxtriaSalesIQTM__Position__c> lsPositionsNew){
        if(lsPositionsNew.size()!=0){
            for(AxtriaSalesIQTM__Position__c pos : lsPositionsNew){
                system.debug('inside for loop'+pos);
                pos.AxtriaSalesIQTM__Client_Territory_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c; //pos.Name;
                pos.AxtriaSalesIQTM__Client_Territory_Name__c = pos.AxtriaSalesIQTM__Client_Position_Code__c; //pos.Name;
                //pos.AxtriaSalesIQTM__Client_Position_Code__c = pos.Name;
                system.debug('inside for loop position updated'+pos);
            }
        }
        
    }
    public static void createPositionProduct(list<AxtriaSalesIQTM__Position__c> lsNewPosition){
        Set<string> teamInsIdSet = new Set<string>();
        List<AxtriaSalesIQTM__Position_Product__c> posProductList = new List<AxtriaSalesIQTM__Position_Product__c>();
        for(AxtriaSalesIQTM__Position__c pos : lsNewPosition){
            if(pos.AxtriaSalesIQTM__Team_Instance__c != Null){
                teamInsIdSet.add(pos.AxtriaSalesIQTM__Team_Instance__c);
            }
        }
        
        if(teamInsIdSet.size() > 0){
            Map<string,List<Team_Instance_Product_AZ__c>>  tipMap = new Map<string,List<Team_Instance_Product_AZ__c>>();
            system.debug('=-=-=-=-=-=' + teamInsIdSet);
        
            for(Team_Instance_Product_AZ__c teamInstPro : [SELECT id,Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c,Product_Catalogue__c FROM Team_Instance_Product_AZ__c WHERE Team_Instance__c IN : teamInsIdSet]){
                if(tipMap.containsKey(teamInstPro.Team_Instance__c)){
                    tipMap.get(teamInstPro.Team_Instance__c).add(teamInstPro);
                }
                else if(!tipMap.containsKey(teamInstPro.Team_Instance__c)){
                    tipMap.put(teamInstPro.Team_Instance__c,new List<Team_Instance_Product_AZ__c>{teamInstPro});
                }
            }
            
            
            for(AxtriaSalesIQTM__Position__c  position: lsNewPosition){
               if(tipMap.containsKey(position.AxtriaSalesIQTM__Team_Instance__c)) {
                    system.debug('asdfasf');
                    for(Team_Instance_Product_AZ__c teamInstanceProductAZ : tipMap.get(position.AxtriaSalesIQTM__Team_Instance__c)){
                        if(tipMap.containsKey(position.AxtriaSalesIQTM__Team_Instance__c)){                        
                            AxtriaSalesIQTM__Position_Product__c posPro = new AxtriaSalesIQTM__Position_Product__c();
                            
                            posPro.AxtriaSalesIQTM__Position__c = position.Id;
                            posPro.AxtriaSalesIQTM__Team_Instance__c = teamInstanceProductAZ.Team_Instance__c;
                            posPro.Product_Catalog__c = teamInstanceProductAZ.Product_Catalogue__c;
                            posPro.Vacation_Days__c = 0;
                            posPro.Holidays__c = 0;
                            posPro.Calls_Day__c = 0;
                            posPro.Business_Days_in_Cycle__c = 0;
                            posPro.AxtriaSalesIQTM__Product_Weight__c = 0;    
                            posPro.AxtriaSalesIQTM__isActive__c = true;
                            posPro.AxtriaSalesIQTM__Effective_Start_Date__c = teamInstanceProductAZ.Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                            posPro.AxtriaSalesIQTM__Effective_End_Date__c = teamInstanceProductAZ.Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c;
                            posProductList.add(posPro); 
                        
                        }
                    } 
                }      
            } 
        }
        if(posProductList.size() > 0){
            insert posProductList ;
        }
    }
    
    
    public static void deletePositionChilds(set<string> PositionList){
        List<AxtriaSalesIQTM__Position_Product__c> posProListDel = [select id FROM AxtriaSalesIQTM__Position_Product__c WHERE AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c IN : PositionList];
        if(posProListDel != Null && posProListDel.size() > 0){
            delete posProListDel;  
        }  
        //added change by ankit---modification for veeva pos hierarchy
        //Delete [select id from Veeva_Position_Hierarchy__c where Position__c IN :PositionList];
        list<Veeva_Position_Hierarchy__c> veevaPosHieDel = new list<Veeva_Position_Hierarchy__c> ();
        veevaPosHieDel=[select id from Veeva_Position_Hierarchy__c where Position_Code__c IN :PositionList];
        for(Veeva_Position_Hierarchy__c veeva:veevaPosHieDel){
            veeva.Status__c='Deleted';
        }
        update veevaPosHieDel;
        
        system.debug('===delete this====='+veevaPosHieDel);
            
    }  
    
    public static void createPositionHierarchy(list<AxtriaSalesIQTM__Position__c> PositionList, String triggerEvent,map<Id,AxtriaSalesIQTM__Position__c> posOldMap){
        List<Veeva_Position_Hierarchy__c> veevaPosHistLst = new List<Veeva_Position_Hierarchy__c>();
        List<Veeva_Position_Hierarchy__c> veevaPosLst = new List<Veeva_Position_Hierarchy__c>();
        Map<string,Veeva_Position_Hierarchy__c> mapPosID2Veeva = new Map<string,Veeva_Position_Hierarchy__c>();
        Map<string,Veeva_Position_Hierarchy__c> mapParPosID2Veeva = new Map<string,Veeva_Position_Hierarchy__c>();
        
        set<string> posCodeSet = new set<string>();
        set<string> parentPosCodeSet = new set<string>();
        system.debug('PositionList++'+PositionList);
        if(triggerEvent == 'Updated'){
            veevaPosLst=[select id,Position_Code__c,Position__c from Veeva_Position_Hierarchy__c where Position__c=:PositionList];
            for(Veeva_Position_Hierarchy__c veeva:veevaPosLst){
                mapPosID2Veeva.put(veeva.Position__c,veeva);
            }
        }
        
        if(triggerEvent == 'Insert'){
            for(AxtriaSalesIQTM__Position__c pos:PositionList){
                parentPosCodeSet.add(pos.AxtriaSalesIQTM__Parent_Position__c);
            }
            /*veevaPosLst=[select id,Position_Code__c,Position__c from Veeva_Position_Hierarchy__c where Position__c=:parentPosCodeSet];
            for(Veeva_Position_Hierarchy__c veeva:veevaPosLst){
                mapParPosID2Veeva.put(veeva.Position__c,veeva);
            }*/
        }
        
        
        for(AxtriaSalesIQTM__Position__c position : [select id,name,AxtriaSalesIQTM__Team_Instance__c,Country_Code__c,AxtriaSalesIQTM__inactive__c,
                                                     AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                                                     AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c,
                                                     AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Parent_Position__c,Channel_AZ__c,Sales_Team_Attribute__c FROM AxtriaSalesIQTM__Position__c 
                                                     WHERE id IN:PositionList])
        {
            Veeva_Position_Hierarchy__c veevaPosH;
            
            if(triggerEvent == 'Insert'){
                veevaPosH = new Veeva_Position_Hierarchy__c ();
                veevaPosH.Status__c = 'Added';
                veevaPosH.Country__c = position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c ;
                veevaPosH.AccountAccessLevel__c='Edit';
                //veevaPosH.Description__c = position.AxtriaSalesIQTM__Position_Type__c ;
                veevaPosH.Channel__c = position.Channel_AZ__c;
                veevaPosH.Sales_team_Attribute__c = position.Sales_team_Attribute__c;
                        
                veevaPosH.Sales_Team_AZ__c = '';
                veevaPosH.Position_Code__c = position.AxtriaSalesIQTM__Client_Position_Code__c ;
                veevaPosH.Veeva_ID__c = '';
                veevaPosH.Position__c = position.id;
                if(position.AxtriaSalesIQTM__Parent_Position__c!=null ){ //&& mapParPosID2Veeva.containsKey(position.AxtriaSalesIQTM__Parent_Position__c)){
                    //veevaPosH.Parent_Territory__c=mapParPosID2Veeva.get(position.AxtriaSalesIQTM__Parent_Position__c).id;
                    veevaPosH.Parent_Pos_Code__c = position.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                
                }
                veevaPosHistLst.add(veevaPosH);
            }
            system.debug('=====mapPosID2Veeva====='+mapPosID2Veeva);
            if(triggerEvent == 'Updated'){
                system.debug('==inside updated===');
                if(mapPosID2Veeva.containsKey(position.id)){
                    
                    if(posOldMap.get(position.id).AxtriaSalesIQTM__inactive__c==false && position.AxtriaSalesIQTM__inactive__c==true){
                        posCodeSet.add(position.AxtriaSalesIQTM__Client_Position_Code__c);
                    }else if(posOldMap.get(position.id).AxtriaSalesIQTM__Parent_Position__c!= position.AxtriaSalesIQTM__Parent_Position__c){
                        system.debug('inside++2+ Old parent value'+posOldMap.get(position.id).AxtriaSalesIQTM__Parent_Position__c);
                        veevaPosH=mapPosID2Veeva.get(position.id);
                        if(position.AxtriaSalesIQTM__Parent_Position__c!=null ){ 
                            veevaPosH.Parent_Pos_Code__c = position.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                        }
                        veevaPosH.Status__c = 'Updated';
                        
                        veevaPosHistLst.add(veevaPosH);
                    }
                    else if(posOldMap.get(position.id).AxtriaSalesIQTM__Client_Position_Code__c != position.AxtriaSalesIQTM__Client_Position_Code__c){
                        system.debug('inside++3+ old pos code value'+posOldMap.get(position.id).AxtriaSalesIQTM__Client_Position_Code__c);
                        system.debug('inside++3+ new pos code value'+position.AxtriaSalesIQTM__Client_Position_Code__c);
                        
                        //Delete the changed position hierarchy
                        posCodeSet.add(posOldMap.get(position.id).AxtriaSalesIQTM__Client_Position_Code__c);
                        //Insert the position with new name
                        veevaPosH = new Veeva_Position_Hierarchy__c ();
                        veevaPosH.Status__c = 'Added';
                        veevaPosH.Country__c = position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c ;
                        veevaPosH.AccountAccessLevel__c='Edit';
                        //veevaPosH.Description__c = position.AxtriaSalesIQTM__Position_Type__c ;
                        veevaPosH.Channel__c = position.Channel_AZ__c;
                        veevaPosH.Sales_team_Attribute__c = position.Sales_team_Attribute__c;
                        veevaPosH.Sales_Team_AZ__c = '';
                        veevaPosH.Position_Code__c = position.AxtriaSalesIQTM__Client_Position_Code__c ;
                        veevaPosH.Veeva_ID__c = '';
                        veevaPosH.Position__c = position.id;
                        if(position.AxtriaSalesIQTM__Parent_Position__c!=null ){ 
                            veevaPosH.Parent_Pos_Code__c = position.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                        }
                        veevaPosHistLst.add(veevaPosH);
                    }
                }   
            }
            
            if(posCodeSet.size()>0){
                deletePositionChilds(posCodeSet);
            }
            
            if(veevaPosHistLst.size() > 0){
                upsert veevaPosHistLst ;
            }
        }
        
    }  
    
    public static void createNewGroups(set<string> setGroupName, string groupType){
        system.debug('set groupnames:'+setGroupName);
        list<GroupAZ__c> newGrp = new list<GroupAZ__c>(); 
        map<string,string> mapGrpNametoId = new map<string,string>();
        list<GroupAZ__c> lsExistingGrps = [select id,name from GroupAZ__c where Group_Type__c =:groupType];
        
        if(lsExistingGrps.size()!=0){
            for(GroupAZ__c grp:lsExistingGrps){
                mapGrpNametoId.put(grp.name,grp.id);
            }
        }
        for(string nameG :setGroupName){
            if(lsExistingGrps.size()==0 || (mapGrpNametoId.size()!=0 && !mapGrpNametoId.containsKey(nameG))){
                system.debug('inside create');
                newGrp.add(new GroupAZ__c(name=nameG, Group_Type__c = groupType));
            }
        }
        if(newGrp.size()!=0){
            insert newGrp;
        }
    }
    public static void handleADGroup(list<AxtriaSalesIQTM__Employee__c> triggerNew,map<Id,AxtriaSalesIQTM__Employee__c> triggerOldMap){
        list<UserGroups__c> usergrps = new list<UserGroups__c>();
        list<UserGroups__c> usergrpsUpdate = new list<UserGroups__c>();
        list<UserGroups__c> usergrpsInsert = new list<UserGroups__c>();
        
        map<string,map<String,string>> mapEmp2UserGroups = new map<string,map<String,string>>();
        map<string,set<string>> mapPos2SalesTeams = new map<string,set<string>>();
        map<string,Id> mapGrpName2Id = new map<string,Id>();
        
        
        usergrps = [Select id,name,employee__c,position__c,start_date__c,end_date__c,Group__c from UserGroups__c where Group_Type__c = 'AD Group'];// where Group_Name__c in:setNewGrpNames ];
        
        for(UserGroups__c usr:usergrps ){
            if(!mapEmp2UserGroups.containsKey(usr.Employee__c)){
                map<string,string> mapTemp = new map<string,string>();
                mapTemp.put(usr.Name,usr.id);
                mapEmp2UserGroups.put(usr.Employee__c,mapTemp);
            }else{
                mapEmp2UserGroups.get(usr.Employee__c).put(usr.Name,usr.Id);
            }
        }
        //Make a map of groupname and group id
        for(GroupAZ__c gp:[select name,id from GroupAZ__c where group_type__c = 'AD Group']){
            mapGrpName2Id.put(gp.name,gp.id);
        }
        
        
        for(AxtriaSalesIQTM__Employee__c emp : triggerNew){
            //isInsert Case
            if(triggerOldMap==null){
                if(emp.Group_name__c!=null){
                    UserGroups__c newUsr = new UserGroups__c();
                    newUsr.Employee__c = emp.id;
                    newUsr.Group_Type__c = 'AD Group';
                    newUsr.Event__c      = 'Add';
                    newUsr.Name = emp.Group_name__c;
                    newUsr.Group__c = mapGrpName2Id.containskey(emp.Group_name__c)?mapGrpName2Id.get(emp.Group_name__c):null;
                    newUsr.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                    newUsr.start_date__c = system.today();
                    usergrpsInsert.add(newUsr);
                }
            }else{
                //if old map != new map, mark the old record of usergroup as delete and insert the new record
                if(emp.Group_name__c != triggerOldMap.get(emp.Id).Group_Name__c){
                    
                    //Insert UserGroup Record
                    if(emp.Group_name__c!=null){
                        UserGroups__c newUsr = new UserGroups__c();
                        newUsr.Employee__c = emp.id;
                        newUsr.Group_Type__c = 'AD Group';
                        newUsr.Event__c      = 'Add';
                        newUsr.Name = emp.Group_name__c;
                        newUsr.Group__c = mapGrpName2Id.containskey(emp.Group_name__c)?mapGrpName2Id.get(emp.Group_name__c):null;
                        newUsr.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                        newUsr.start_date__c = system.today();
                        usergrpsInsert.add(newUsr);
                    }
                    
                    //Mark the previous record of the group as Delete
                    if(triggerOldMap.get(emp.Id).Group_Name__c!=null){
                        string oldGrpName = triggerOldMap.get(emp.Id).Group_Name__c;
                        if(mapEmp2UserGroups.containsKey(emp.Id) && mapEmp2UserGroups.get(emp.Id).get(oldGrpName)!=null){
                            UserGroups__c oldUsr = new UserGroups__c(id=mapEmp2UserGroups.get(emp.Id).get(oldGrpName));
                            oldUsr.Event__c = 'Delete';
                            oldUsr.end_date__c = system.today();
                            usergrpsUpdate.add(oldUsr);
                        }
                    }
                }
            }
        }
        if(usergrpsInsert.size()!=0){
            insert usergrpsInsert;
        }
        if(usergrpsUpdate.size()!=0){
            update usergrpsUpdate;
        }
    }
    public static void handleSalesTeamAttribute(list<AxtriaSalesIQTM__Position__c> triggerNewPos,map<Id,AxtriaSalesIQTM__Position__c> triggerOldMap){
        //Added for AZ-Global : Add sales Team Attribute and delete
        //----Added for AZ - When an AD Group is updated in an employee, create a UserGroup record.---
        //If the AD group is changed, mark the record as deleted and insert a new record.
        list<UserGroups__c> usergrps = new list<UserGroups__c>();
        list<UserGroups__c> usergrpsUpdate = new list<UserGroups__c>();
        list<UserGroups__c> usergrpsInsert = new list<UserGroups__c>();
        map<string,set<string>> mapPos2SalesTeams = new map<string,set<string>>();
        set<string> lsGroups = new set<string>();
        set<string> lsOldGroups = new set<string>();
   
        system.debug('inside AD new :'+triggerNewPos);
        system.debug('inside AD old:'+triggerNewPos);
        for (AxtriaSalesIQTM__Position__c p: triggerNewPos){
            
            //Added for AZ
            grpsString = p.Sales_Team_Attribute__c;
            if(grpsString!=null && grpsString!=''){
                lsGroups.addAll(grpsString.split(';'));
            }
            grpOldString = triggerOldMap.get(p.Id).Sales_team_Attribute__c;
            if(grpOldString!=null && grpOldString!=''){
                lsOldGroups.addAll(grpOldString.split(';'));
            }
            
        }
        system.debug('lsGroups'+lsGroups);
        system.debug('lsOldGroups'+lsOldGroups);
        map<string,map<String,string>> mapEmp2UserGroups = new map<string,map<String,string>>();
        map<string,Id> mapGrpName2Id = new map<string,Id>();
        
        usergrps = [Select id,name,employee__c,position__c,Group__c,start_date__c,end_date__c from UserGroups__c where Group_Type__c = 'Sales Team Attribute' and Event__c='Add'];// where Group_Name__c in:setNewGrpNames ];
        
        //Make a map of groupname and group id
        for(GroupAZ__c gp:[select name,id from GroupAZ__c where group_type__c = 'Sales Team Attribute']){
            mapGrpName2Id.put(gp.name,gp.id);
        }
        //Map<Employee id, map<Group Name,UserGroup ID>
        for(UserGroups__c usr:usergrps ){
            if(usr.Employee__c!=null && !mapEmp2UserGroups.containsKey(usr.Employee__c)){
                map<string,string> mapTemp = new map<string,string>();
                mapTemp.put(usr.Name,usr.id);
                mapEmp2UserGroups.put(usr.Employee__c,mapTemp);
            }else if(usr.Employee__c!=null){
                map<string,string> tempmap = new map<string,string>();
                tempmap = mapEmp2UserGroups.get(usr.Employee__c);
                tempmap.put(usr.Name,usr.Id);
                mapEmp2UserGroups.put(usr.Employee__c,tempmap);
                //mapEmp2UserGroups.get(usr.Employee__c).put(usr.Name,usr.Id);
            }
            system.debug('mapEmp2UserGroupssdfgdgdrgd+++++'+mapEmp2UserGroups);
            //position to active groups
            if(!mapPos2SalesTeams.containsKey(usr.Position__c)){
                set<string> temp = new set<string>();
                temp.add(usr.Name);
                mapPos2SalesTeams.put(usr.Position__c,temp);
            }else{
                mapPos2SalesTeams.get(usr.Position__c).add(usr.Name);
            }
            
        }
        for(AxtriaSalesIQTM__Position__c pos : triggerNewPos){
            //if old map != new map, mark the old record of usergroup as delete and insert the new record
            //pos.AxtriaSalesIQTM__Employee__c!=null &&
            if(( pos.AxtriaSalesIQTM__Employee__c != triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c)
            || (pos.Sales_Team_Attribute__c != triggerOldMap.get(pos.Id).Sales_Team_Attribute__c)){
                //When a new Group added to the Sales Team Attribute
                for(string grp:lsGroups){
                    //Insert UserGroup Record with the new employee
                    if((pos.AxtriaSalesIQTM__Employee__c!=null && pos.Sales_Team_Attribute__c!=null) 
                    && (!lsOldGroups.contains(grp) || triggerOldMap.get(pos.id).AxtriaSalesIQTM__Employee__c==null)){
                    //(!mapPos2SalesTeams.containskey(pos.id) || !mapPos2SalesTeams.get(pos.id).contains(grp)))
                        UserGroups__c newUsr = new UserGroups__c();
                        newUsr.Employee__c = pos.AxtriaSalesIQTM__Employee__c;
                        newUsr.Group_Type__c = 'Sales Team Attribute';
                        newUsr.Event__c      = 'Add';
                        newUsr.Name = grp;
                        newUsr.Position__c = pos.Id;
                        newUsr.Group__c = mapGrpName2Id.containskey(grp)?mapGrpName2Id.get(grp):null;
                        newUsr.start_date__c = system.today();
                        usergrpsInsert.add(newUsr);
                    }
                    //Mark the previous record of the group as Delete if employee has changed.
                    if((triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c!=null 
                    && triggerOldMap.get(pos.Id).Sales_Team_Attribute__c !=null 
                    && !pos.Sales_Team_Attribute__c.contains(grp)) 
                    || (pos.AxtriaSalesIQTM__Employee__c==null)){
                        //
                        if(triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c!=null && grp!=null ){
                            string oldEmpName = triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c;
                            system.debug('inside isupdate'+mapEmp2UserGroups);
                            system.debug('inside isupdate'+grp);
                            system.debug('inside isupdate'+oldEmpName);
                            if(mapEmp2UserGroups.containskey(oldEmpName) && mapEmp2UserGroups.get(oldEmpName).get(grp)!=null){
                                UserGroups__c oldUsr = new UserGroups__c(id=mapEmp2UserGroups.get(oldEmpName).get(grp));
                                oldUsr.Event__c = 'Delete';
                                oldUsr.end_date__c = system.today();
                                system.debug('update oldusr:'+oldUsr);
                                usergrpsUpdate.add(oldUsr);
                            }
                        }
                    }
                }   
            }
            //When a group is removed from the Sales Team Attribute
            for(string oldGrp:lsOldGroups){                 
                //Mark the previous record of the group as Delete if employee has changed.
                if(triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c!=null 
                && triggerOldMap.get(pos.Id).Sales_Team_Attribute__c !=null 
                && (pos.Sales_Team_Attribute__c==null || !pos.Sales_Team_Attribute__c.contains(oldGrp)) ){
                    string oldEmpName = triggerOldMap.get(pos.Id).AxtriaSalesIQTM__Employee__c;
                    system.debug('mapEmp2UserGroups+'+mapEmp2UserGroups);
                    if(mapEmp2UserGroups!=null && mapEmp2UserGroups.get(oldEmpName)!=null && mapEmp2UserGroups.get(oldEmpName).get(oldGrp)!=null){
                        UserGroups__c oldUsr = new UserGroups__c(id=mapEmp2UserGroups.get(oldEmpName).get(oldGrp));
                        oldUsr.Event__c = 'Delete';
                        oldUsr.end_date__c = system.today();
                        system.debug('update oldusr:'+oldUsr);
                        usergrpsUpdate.add(oldUsr);
                    }
                }
            }
        }
        if(usergrpsInsert.size()!=0){
            insert usergrpsInsert;
        }
        if(usergrpsUpdate.size()!=0){
            update usergrpsUpdate; 
        }
    }
    
    //POSITION CODE LOGIC FOR AZ, COMMENTED FOR LATER USE
    /*system.debug('mapParentIdToPos+++000000000++'+mapParentIdToPos); 
        list<string> lsPosIds = new list<string>();
        List<AxtriaSalesIQTM__Position__c> posList = new List<AxtriaSalesIQTM__Position__c>();
        
        for (AxtriaSalesIQTM__Position__c p: triggerNewPos){
            posList.add(p);   
            lsPosIds.add(p.id);     
            mapParentIdToPos.put(p.AxtriaSalesIQTM__Parent_Position__c,p); 
            
         }
        map<string,AxtriaSalesIQTM__Position__c> mapParentIdToPos = new map<string,AxtriaSalesIQTM__Position__c>();
        
        if(Trigger.isinsert && Trigger.isBefore){    
            set<id> parentposset = new Set<id>();
            map<AxtriaSalesIQTM__Position__c,string> posId2NewPosCode = new map<AxtriaSalesIQTM__Position__c,string>();
            
            list<Position_Code_Matrix__c> lsPosCodeMatrix = new list<Position_Code_Matrix__c>([select id, Marketing_code__c,Team_Name__c,
                                                    Counter_Start_Location__c,Counter_End_Location__c,Territory_Type__c,
                                                    Total_Length__c from position_code_matrix__c]);
            map<string,map<string,Position_Code_Matrix__c>> mapMCTeam2terrType2posMatrix = new map<string,map<string,Position_Code_Matrix__c>>();
            
            for(Position_Code_Matrix__c pcMatrix:lsPosCodeMatrix ){
                if(!mapMCTeam2terrType2posMatrix.containsKey(pcMatrix.Marketing_code__c +pcMatrix.Team_Name__c)){
                    map<string,Position_Code_Matrix__c> temp = new map<string,Position_Code_Matrix__c>();
                    temp.put(pcMatrix.Territory_Type__c,pcMatrix);
                    mapMCTeam2terrType2posMatrix.put(pcMatrix.Marketing_code__c +pcMatrix.Team_Name__c,temp );
                }else{
                    //map<string,Position_Code_Matrix__c> updatedMap = new map<string,Position_Code_Matrix__c>();
                    mapMCTeam2terrType2posMatrix.get(pcMatrix.Marketing_code__c +pcMatrix.Team_Name__c).put(pcMatrix.Territory_Type__c,pcMatrix);
                }
            }
            system.debug('mapMCTeam2terrType2posMatrix++'+mapMCTeam2terrType2posMatrix);
            for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                if(pos.AxtriaSalesIQTM__Parent_Position__c != Null)
                parentposset.add(pos.AxtriaSalesIQTM__Parent_Position__c);
            }
            list<AxtriaSalesIQTM__Position__c> parPosWithChildPos = new list<AxtriaSalesIQTM__Position__c>([SELECT id,AxtriaSalesIQTM__Client_Territory_Code__c,
                                            AxtriaSalesIQTM__Client_Position_Code__c,Country_Name__c,Team_Name__c,AxtriaSalesIQTM__Position_Type__c,
                                            (SELECT id,AxtriaSalesIQTM__Client_Position_Code__c,
                                            AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Position_Type__c
                                            from AxtriaSalesIQTM__Positions__r where AxtriaSalesIQTM__inactive__c = false)
                                            from AxtriaSalesIQTM__Position__c where id IN: parentposset and AxtriaSalesIQTM__inactive__c = false]);
            
            for(AxtriaSalesIQTM__Position__c parent: parPosWithChildPos){
                Integer maxCounter = 0;
                String posCode;
                String NewCode;
                Integer counterLength = 0;
                Integer startIndex = 0;
                Integer endIndex = 0;
                Position_Code_Matrix__c positionCodeRecord = new Position_Code_Matrix__c();
                for(AxtriaSalesIQTM__Position__c child:parent.AxtriaSalesIQTM__Positions__r){
                    //find maximum counter of the position
                    //Skip the position record which is about to be created
                    //if(setPosIds!=null && !setPosIds.contains(child.id)){
                    system.debug('child++'+child.AxtriaSalesIQTM__Client_Position_Code__c);
                        posCode = child.AxtriaSalesIQTM__Client_Position_Code__c;
                        
                        positionCodeRecord = mapMCTeam2terrType2posMatrix.get(parent.Country_Name__c + parent.Team_Name__c).get(child.AxtriaSalesIQTM__Position_Type__c);
                        counterLength = Integer.Valueof(positionCodeRecord.Counter_End_Location__c - positionCodeRecord.Counter_Start_Location__c);
                         
                        Integer counter = Integer.Valueof(posCode.substring(Integer.Valueof(positionCodeRecord.Counter_Start_Location__c),Integer.Valueof(positionCodeRecord.Counter_End_Location__c)));
                        
                        system.debug('child.AxtriaSalesIQTM__Client_Position_Code__c+++'+child.AxtriaSalesIQTM__Client_Position_Code__c);
                        //system.debug(child.AxtriaSalesIQTM__Client_Position_Code__c.substring(6,7));
                        system.debug((child.AxtriaSalesIQTM__Client_Position_Code__c).substring((Integer)positionCodeRecord.Counter_Start_Location__c,(Integer)positionCodeRecord.Counter_End_Location__c));
                        system.debug('counterLength++'+counterLength);
                        system.debug('positionCodeRecord.Counter_Start_Location__c++'+Integer.Valueof(positionCodeRecord.Counter_Start_Location__c));
                        system.debug('positionCodeRecord.Counter_End_Location__c++'+Integer.Valueof(positionCodeRecord.Counter_End_Location__c));
                        
                        system.debug('counter+'+counter);
                        if(counter>maxCounter){
                            maxCounter = counter;
                        }
                        system.debug('maxCounter+++++++:'+maxCounter);
                    //}
                    
                }
                //Increment that counter by 1
                //Get the new code generated
                if(posCode!=null){
                    NewCode = posCode.substring(0,Integer.Valueof(positionCodeRecord.Counter_Start_Location__c)) ;
                    NewCode = NewCode + ((String.Valueof(maxCounter + 1).length()<counterLength)? String.Valueof(maxCounter + 1).leftPad(counterLength,'0'):String.Valueof(maxCounter + 1));
                    if(positionCodeRecord.Counter_End_Location__c!=positionCodeRecord.Total_Length__c){
                        NewCode = NewCode + posCode.substring(Integer.Valueof(positionCodeRecord.Counter_End_Location__c));
                    }
                }
                //Set the new position code in the position and maintain it in a map of position id and position code
                posId2NewPosCode.put(mapParentIdToPos.get(parent.id),NewCode);
            }
            system.debug('posId2NewPosCode++'+posId2NewPosCode);
            
            Map<String, AxtriaSalesIQTM__Position__c> posIdCode = new Map<String, AxtriaSalesIQTM__Position__c>();
            for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                pos.AxtriaSalesIQTM__Client_Position_Code__c = posId2NewPosCode.get(pos);
                system.debug('pos.AxtriaSalesIQTM__Client_Position_Code__c+'+pos.AxtriaSalesIQTM__Client_Position_Code__c);
                pos.AxtriaSalesIQTM__Client_Territory_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
                pos.AxtriaSalesIQTM__Client_Territory_Name__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
                pos.Name                                      = pos.AxtriaSalesIQTM__Client_Position_Code__c;
                pos.AxtriaSalesIQTM__inactive__c = false;
                if(pos.AxtriaSalesIQTM__Effective_End_Date__c < = system.today()){
                    pos.AxtriaSalesIQTM__inactive__c = true; 
                }
                if(pos.AxtriaSalesIQTM__Effective_End_Date__c == pos.AxtriaSalesIQTM__Effective_Start_Date__c){
                    pos.addError('Territory start Date and Territory End Date should not be same.');
                }
            }
            system.debug('posId2NewPosCode++'+posId2NewPosCode);
            
        }
        
        //added for before update
        if(Trigger.isbefore && Trigger.isUpdate){
            for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                pos.AxtriaSalesIQTM__Client_Territory_Name__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
            }
        }*/
    
}