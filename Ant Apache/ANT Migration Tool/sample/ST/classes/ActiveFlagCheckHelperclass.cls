public with sharing class ActiveFlagCheckHelperclass {
    public ActiveFlagCheckHelperclass() {
        
    }
    public static void check(list<Parameter__c>triggernew,map<id,Parameter__c>triggeroldmap){
        list<Parameter__c>parmList = new list<Parameter__c>();

        system.debug('======Check method is called');
        for(Parameter__c p: triggernew){
            system.debug('========PARAMETER IS:'+p);
                Parameter__c oldparm = triggeroldmap.get(p.id);
                system.debug('========oldparm IS:'+oldparm);
                if(oldparm.isActive__c == TRUE && p.isActive__c == FALSE){
                    parmList.add(p);
                }
        }
        System.debug('===========parmList----'+parmList);

        list<Rule_Parameter__c> RuleParamList = [select id,Parameter__c from Rule_Parameter__c where Parameter__c in:parmList];
        set<String>RuleparamSet = new set<String>();
        
        
        if(!RuleParamList.isEmpty() && RuleParamList!=null && RuleParamList.size()>0 ){
            system.debug('-------RuleParamList::'+RuleParamList);
            for(Rule_Parameter__c RP :RuleParamList){
                RuleparamSet.add(RP.Parameter__c);
            }
            system.debug('=======RuleparamSet:::'+RuleparamSet);
            for(Parameter__c p : triggernew){
                if(RuleparamSet.contains(p.id)){
                    p.addError('You Can not Deactivate a Parameter if it is used in existing Business Rule.');
                }
            }
        }
    }
}