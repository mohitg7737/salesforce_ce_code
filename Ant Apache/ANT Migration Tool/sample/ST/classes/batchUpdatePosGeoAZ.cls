global class batchUpdatePosGeoAZ implements Database.Batchable<sObject> {
    
    public ID team_ins;
    Map<String, Set<String>> mapPosGeo = new Map<String, Set<String>>();
    global batchUpdatePosGeoAZ(String mapserialised, ID ti) {
        mapPosGeo= (Map<String, Set<String>>)JSON.deserialize(mapSerialised,Map<String, Set<String>>.class);
        team_ins = ti;
        
         }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([select id, AxtriaSalesIQTM__Geography__r.name , AxtriaSalesIQTM__Position__r.name, AxtriaSalesIQTM__SharedWith__c,AxtriaSalesIQTM__isshared__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__c = :team_ins]);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Geography__c> scope) {
        Set<String> pos = new Set<String>(); 
        List<AxtriaSalesIQTM__Position_Geography__c> posGeoUpdate = new List<AxtriaSalesIQTM__Position_Geography__c>();
        for(AxtriaSalesIQTM__Position_Geography__c pg : scope){
            if(mapPosGeo.get(pg.AxtriaSalesIQTM__Geography__r.name).size()>1){

                pg.AxtriaSalesIQTM__IsShared__c =true;
                Set<String> sharedPos = new Set<String>(mapPosGeo.get(pg.AxtriaSalesIQTM__Geography__r.name));
                sharedPos.remove(pg.AxtriaSalesIQTM__Position__r.name);
                List<String> ListSharedPos = new List<String>(sharedPos);
                pg.AxtriaSalesIQTM__SharedWith__c = String.join(ListSharedPos, ';');
                posGeoUpdate.add(pg);
            }
        }
        
        update posGeoUpdate;
    }

    global void finish(Database.BatchableContext BC) {

    }
}