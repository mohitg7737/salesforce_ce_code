@isTest
private class BatchInBoundUpdAccAffiliationTest {
   @testSetup 
    static void setup() {
        List<SIQ_Account_Affiliation__c> siqaccounts = new List<SIQ_Account_Affiliation__c>();
        List<AxtriaSalesIQTM__Account_Affiliation__c> accounts = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
         AxtriaSalesIQTM__Affiliation_Network__c af = new  AxtriaSalesIQTM__Affiliation_Network__c();
          af.Name = 'AZ Network';
          af.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
          insert af;
          String afiliationid = af.id;
         SIQ_Account_Master__c sa = new  SIQ_Account_Master__c();
          sa.name='xyz';
          sa.SIQ_Marketing_Country__c='US';
          sa.SIQ_Country__c='US';
          insert sa;
          String accId = sa.id;
        List<AxtriaSalesIQTM__Account_Affiliation__c> accAff = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        // insert 10 accounts
        for (Integer i=0;i<5;i++) {
            siqaccounts.add(new SIQ_Account_Affiliation__c(SIQ_Account_Number__c='Account '+i, SIQ_Account_Id__c=accId, SIQ_Parent_Account_Id__c=accId,
                SIQ_Affiliation_Hierarchy__c='abc',SIQ_Affiliation_Type__c='abc',SIQ_Country_Code__c='US',SIQ_Parent_Account_Number__c='xx',
          SIQ_Marketing_Code__c='USA'));
        } 
        insert siqaccounts;
       // for (SIQ_Account_Affiliation__c siqaccount : [select SIQ_Account_Number__c,SIQ_Parent_Account_Number__c from SIQ_Account_Affiliation__c]) {
        //    accounts.add(new AxtriaSalesIQTM__Account_Affiliation__c(AxtriaSalesIQTM__Affiliation_Network__c=afiliationid,Unique_Id__c=siqaccount.SIQ_Account_Number__c+'_'+siqaccount.SIQ_Parent_Account_Number__c));
       // }
       // upsert accounts Unique_Id__c;
    }
    static testmethod void test() {        
        Test.startTest();
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchInBoundUpdAccAffiliation usa = new BatchInBoundUpdAccAffiliation();
        Id batchId = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}