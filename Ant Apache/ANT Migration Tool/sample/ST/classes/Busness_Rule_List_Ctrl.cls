public class Busness_Rule_List_Ctrl {
    public String ruleList{get;set;}

    public String countryID;
    Map<string,string> successErrorMap;
    Public AxtriaSalesIQTM__Country__c Country {get;set;}

    public Boolean publishFlag {get;set;}
    public Boolean checkboxEnabled{get;set;}
    public String tcfFinalValue {get;set;}


    public Measure_Master__c ruleObject; 
    public List<String> allSegments {get;set;}
     public String optOutString {get;set;}


    public Busness_Rule_List_Ctrl(){

        //Fetch Country ID from cookie
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
           Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }


        populateRuleList();
    }

    public void populateRuleList(){
        list<GridListWrapper> GridWrapperList = new list<GridListWrapper>();
        list<Measure_Master__c> ruleMaster = [select id,Name,is_executed__c, File_Download_Id__c, Cycle__c, Cycle__r.Name, is_promoted__c,Brand_Lookup__r.Name,Team_Instance__r.name,Line_2__r.name,LastModifiedDate,State__c,lastmodifiedBy.Name,Team__r.Name,Team_Instance__c, isGlobal__c from Measure_Master__c where Country__c = :countryID OR isGlobal__c = true Order By isGlobal__c DESC , CreatedDate DESC];
        for(Measure_Master__c rule : ruleMaster){
            GridWrapperList.add(new GridListWrapper(rule));
        }

        if(GridWrapperList != null && GridWrapperList.size() > 0){
            ruleList = JSON.serialize(GridWrapperList);
        }
    }

    public pagereference newrule(){
        pageReference pr = new PageReference('/apex/BusinessRuleConstruct?mode=New');
        pr.setRedirect(true);
        return pr;
    }

    public pagereference editrule(){
        pageReference pr = new PageReference('/apex/BusinessRuleConstruct?mode=Edit&rid='+ruleId+'');
        pr.setRedirect(true);
        return pr;
    }

    public pagereference previewrule(){
        pageReference pr = new PageReference('/apex/PreviewResults?rid='+ruleId+'');
        pr.setRedirect(true);
        return pr;
    }

    public pageReference viewRule()
    {
        PageReference pr = new PageReference('/apex/BusinessRuleConstruct?mode=View&rid='+ruleId+'');
        pr.setRedirect(true);
        return pr;
    }

    public String ruleId{get;set;}
    public void ExecuteRule(){
        System.debug('--ruleId-- ' + ruleId);

        // Execute Business Rule
        Measure_Master__c rule;
        if(String.isNotBlank(ruleId)){
            rule = [SELECT Id, Name, Team_Instance__c,Team_Instance__r.Name, Brand_Lookup__c, Business_Unit_Loopup__c, Line_2__c, Cycle__c, State__c,Team__r.Name FROM Measure_Master__c WHERE Id=:ruleId];
            System.debug(rule);

            /*
                Use Case -      When business rule is executed again
                Action -        Delete any previous Computation
                Developer -     Sahil Mahajan
                Developer Employee ID - A0705
                Date - March 21,2018
                JIRA Bug Code (if Applicable)- NA
            */

            Integer BUResponseCount = database.countQuery('SELECT count() FROM BU_Response__c WHERE Brand__r.Brand__c =\''+rule.Brand_Lookup__c+'\' AND Cycle__c =\''+rule.Team_Instance__c+'\' AND Cycle2__c =\''+rule.Cycle__c+'\' LIMIT 1');//+'\' AND Brand__r.Line__c =\''+rule.Line_2__c
            if(BUResponseCount > 0){
                String WhereClause = ' Brand__r.Brand__c =\''+rule.Brand_Lookup__c+'\' AND Cycle__c =\''+rule.Team_Instance__c+'\' AND Cycle2__c =\''+rule.Cycle__c+'\' ';//'\' AND Cycle__c =\''+rule.Team_Instance__c+
                BatchDeleteRecsBeforereExecute batchExecute = new BatchDeleteRecsBeforereExecute(ruleId, WhereClause );
                Database.executeBatch(batchExecute, 2000);
                rule.State__c = 'Executing';
                update rule;
                populateRuleList();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Submitted for Execution'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No data found'));
                System.debug('No data found');
            }
        }
    }

    public PageReference cloneRule(){
        Measure_Master__c rule = [SELECT Id, Name, Country__c, Team_Instance__c,Team_Instance__r.Name, Stage__c, Brand_Lookup__c, Team__c, Line_2__c, State__c, Cycle__c, IsActive__c FROM Measure_Master__c WHERE Id=:ruleId];
        
        Measure_Master__c clonedRule = new Measure_Master__c();
        String prefix = 'Cloned_' + rule.Name;
       //Measure_Master__c clonedRule = new Measure_Master__c();

        List<Measure_Master__c> allRecs = [select Name from Measure_Master__c];
        List<String> allNames = new List<String>();

        for(Measure_Master__c mm : allRecs)
        {
            allNames.add(mm.Name);
        }

        Boolean flag = true;

        Integer i = 1;
        String tempBRName = prefix;

        while (flag == true)
        {
            if(allNames.contains(tempBRName))
            {
                tempBRName = prefix + '_'+ String.valueof(i);
                i++;
            }
            else
            {
                flag = false;
            }
        }

        clonedRule.Name = tempBRName;
        clonedRule.Cycle__c = rule.Cycle__c;
        clonedRule.Stage__c = 'Basic Information';
        clonedRule.Brand_Lookup__c = rule.Brand_Lookup__c;
        //clonedRule.Business_Unit_Loopup__c = rule.Team__c;
        clonedRule.Team_Instance__c = rule.Team_Instance__c;
        //clonedRule.Line_2__c = rule.Line_2__c;
        clonedRule.State__c = 'In Progress';
        clonedRule.IsActive__c = true;
        
        insert clonedRule;

        System.debug('clonedRule--- ' + clonedRule);
        list<Rule_Parameter__c> clonedParameters = new list<Rule_Parameter__c>();
        Map<String, Rule_Parameter__c> nameToParmeter = new Map<String, Rule_Parameter__c>();
        Map<String, Rule_Parameter__c> stepRuleParam = new Map<String, Rule_Parameter__c>();
        set<String> stepName = new set<String>();
        for(Rule_Parameter__c rp: [SELECT Id, Name, Measure_Master__c, Parameter__c, Step__c, Step__r.Name, Step__r.Sequence__c, Type1__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id]){
            Rule_Parameter__c crp = new Rule_Parameter__c();
            crp.Measure_Master__c = clonedRule.Id;
            crp.Parameter__c = rp.Parameter__c != null ? rp.Parameter__c : null;
            crp.Type1__c = rp.Type1__c;
            clonedParameters.add(crp);

            if(rp.Step__c != null){
                stepRuleParam.put(rp.Step__r.Sequence__c + '_' + rp.Step__r.Name, crp);
                stepName.add(rp.Step__r.Name);
            }

            nameToParmeter.put(rp.Name, crp);
        }

        insert clonedParameters;
        System.debug('--stepRuleParam-- ' + stepRuleParam);
        System.debug('--nameToParmeter-- ' + nameToParmeter);

        list<Step__c> clonedSteps = new list<Step__c>();
        list<Compute_Master__c> clonedComputes = new list<Compute_Master__c>();
        list<Rule_Parameter__c> updateRuleParam = new list<Rule_Parameter__c>();
        Map<Decimal, Compute_Master__c> seqCompute = new Map<Decimal, Compute_Master__c>();
        Map<String,String>MatrixID = new Map<String,String>();
        list<String>GMList = new list<String>();
        List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c ];
        List<String> allgms = new List<String>();

        for(Grid_Master__c g : allGridMasters)
        {
            allgms.add(g.Name);
        }

        for(Step__c step : [SELECT Id, Name, Measure_Master__c,Measure_Master__r.Name, Compute_Master__c, Output__r.Name, Grid_Param_1__r.Name, Grid_Param_2__r.Name, Compute_Master__r.Expression__c, Compute_Master__r.Field_2_val__c, Compute_Master__r.Field_2_Type__c, Compute_Master__r.Field_1__r.Name, Compute_Master__r.Field_2__r.Name, Compute_Master__r.Operator__c, Compute_Master__r.Type__c, Description__c, Matrix__c,Matrix__r.Name, Output__c, Grid_Param_1__c, Grid_Param_2__c, Sequence__c, Step_Type__c, Type__c, UI_Location__c FROM Step__c WHERE Measure_Master__c =: ruleId ORDER BY Sequence__c]){
            Step__c cStep = new Step__c();
            cStep.Measure_Master__c = clonedRule.Id;
            cStep.Name = step.Name;
            cStep.Description__c = step.Description__c;
            cStep.Sequence__c = step.Sequence__c;
            cStep.Step_Type__c = step.Step_Type__c;
            cStep.Type__c = step.Type__c;
            cStep.UI_Location__c = step.UI_Location__c;
            cStep.Output__c = step.Output__r.Name != null ? nameToParmeter.get(step.Output__r.Name).Id : null;
            if(step.Step_Type__c == 'Matrix'){
                
                Grid_Master__c GM = [select id,name,Brand__c,Brand_Team_Instance__c,Col__c,Country__c,Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c,Output_Name__c,Output_Type__c,Row__c from Grid_Master__c where id =:step.Matrix__c];
                Grid_Master__c CloneGM = GM.Clone();
                //String name =step.Matrix__r.Name ;

                String nameMatrix = GM.name;
                String tempName = nameMatrix;
                i = 1;
                
                flag = true;

                while(flag)
                {
                    system.debug('+++++++++++ Hey Matrix is '+ tempName);
                    if(allgms.contains(tempName))
                    {
                        tempName = nameMatrix + '_' + String.valueof(i);
                        i++;
                    }
                    else
                    {
                        allgms.add(tempName);
                        flag = false;
                    }
                }
                //nameMatrix = tempName;
                //String name =step.Matrix__r.Name ;*/
                CloneGM.Name = tempName;

                //CloneGM.Name = clonedRule.Name+'_'+name;
                CloneGM.Country__c = rule.Country__c;
                CloneGM.CurrencyIsoCode = 'EUR';
                system.debug('====CloneGM===:'+CloneGM);
                insert CloneGM;
                if(!MatrixID.containsKey(step.Matrix__c)){
                    MatrixID.put(step.Matrix__c,CloneGM.id);   
                }
                GMList.add(step.Matrix__c);

                //cStep.Matrix__c = step.Matrix__c;
                cStep.Matrix__c = CloneGM.id;

                cStep.Grid_Param_1__c = step.Grid_Param_1__r.Name != null ? nameToParmeter.get(step.Grid_Param_1__r.Name).Id : null;
                cStep.Grid_Param_2__c = step.Grid_Param_2__r.Name != null ? nameToParmeter.get(step.Grid_Param_2__r.Name).Id : null;

                
               // System.debug('========CloneGD====:'+CloneGD);
                
            }if(step.Step_Type__c == 'Compute' || step.Step_Type__c == 'Cases'){
                Compute_Master__c cm = new Compute_Master__c();
                cm.Expression__c = step.Compute_Master__r.Expression__c != null ? step.Compute_Master__r.Expression__c : '';
                cm.Field_2_val__c = step.Compute_Master__r.FIeld_2_val__c != null ? step.Compute_Master__r.FIeld_2_val__c : 0;
                cm.Field_2_Type__c = step.Compute_Master__r.Field_2_Type__c != null ? step.Compute_Master__r.Field_2_Type__c : '';
                cm.Field_1__c = step.Compute_Master__r.Field_1__r.Name != null ? nameToParmeter.get(step.Compute_Master__r.Field_1__r.Name).Id : null;
                cm.Field_2__c = step.Compute_Master__r.Field_2__r.Name != null ? nameToParmeter.get(step.Compute_Master__r.Field_2__r.Name).Id : null;
                cm.Operator__c = step.Compute_Master__r.Operator__c;
                cm.Type__c = step.Compute_Master__r.Type__c;
                seqCompute.put(cStep.Sequence__c, cm);
                clonedComputes.add(cm);
            }
            clonedSteps.add(cStep);
            /*--Insert Grid Details for cloned Grid Matrix---*/
            //system.debug('====CloneGM===:'+CloneGM.id);
                
                
        }

        insert clonedComputes;

        list<Grid_Details__c> GD = new list<Grid_Details__c>();
        list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();

        GD = [select id,Name,Grid_Master__c,colvalue__c,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c  from Grid_Details__c where Grid_Master__c IN: GMList];
        for(Grid_Details__c g : GD){
            Grid_Details__c newGD = g.clone();
                newGD.Grid_Master__c =MatrixID.get(g.Grid_Master__c);
                newGD.CurrencyIsoCode = 'EUR';
                newGD.Name = g.name;
                CloneGD.add(newGD);
            
        }
        insert CloneGD;

        System.debug('--clonedComputes-- ' + clonedComputes);

        for(Step__c s: clonedSteps){
            if(seqCompute.containsKey(s.Sequence__c)){
                s.Compute_Master__c = seqCompute.get(s.Sequence__c).Id;
            }
        }
        insert clonedSteps;
        System.debug('--clonedSteps-- ' + clonedSteps);

        if(stepRuleParam != null && stepRuleParam.size() > 0){
            for(Step__c s: [SELECT Id, Name, Sequence__c FROM Step__c WHERE Measure_Master__c=:clonedRule.Id AND Name IN :stepName]){
                if(stepRuleParam.containsKey(s.Sequence__c + '_' + s.Name)){
                    Rule_Parameter__c rp = stepRuleParam.get(s.Sequence__c + '_' + s.Name);
                    rp.Step__c = s.Id;
                    updateRuleParam.add(rp);
                }
            }
        }

        System.debug('---updateRuleParam--- ' + updateRuleParam);
        if(updateRuleParam.size() > 0)
            update updateRuleParam;

        pageReference pr = new PageReference('/apex/BusinessRuleConstruct?mode=Edit&rid=' + clonedRule.Id);
        pr.setRedirect(false);
        return pr;
    }

    

    public void initiatePublishPopup()
    {
        ruleObject = new Measure_Master__c();
        publishFlag = true;
        allSegments = new List<String>();

        ruleObject = [SELECT Id, Name, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, Line_2__c, Line_2__r.Name, Line_Lookup__r.Name, isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, Cycle__c, Cycle__r.Name, Team_Instance__r.Name,Team_Instance__c, All_Segments__c FROM Measure_Master__c WHERE Id=:ruleId and Country__c = :countryID];

        allSegments = ruleObject.All_Segments__c.split(',');

        checkboxEnabled = false;
        tcfFinalValue = '0';

    }

    public void hidePopup()
    {
        publishFlag = false;
    }


    public void Publish(){
        System.debug('--ruleId-- ' + ruleId);
        if(String.isNotBlank(ruleId)){

            ruleObject = [SELECT Id, Name, Excluded_Segments__c, Stage__c, Final_TCF__c, TcfOverride__c, Team__c,Team__r.Name, Line_2__c, Line_2__r.Name, Line_Lookup__r.Name, isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, Cycle__c, Cycle__r.Name, Team_Instance__r.Name,Team_Instance__c FROM Measure_Master__c WHERE Id=:ruleId and Country__c = :countryID];

            if(optOutString != null && optOutString.length() > 0)
            {
                ruleObject.Excluded_Segments__c = optOutString;
            }
            ruleObject.TcfOverride__c = checkboxEnabled;
            ruleObject.Final_TCF__c = Decimal.valueof(tcfFinalValue);

            update ruleObject;


            BatchPublishCallPlan batchExecute = new BatchPublishCallPlan(ruleId, ' Measure_Master__c =\''+ruleId+'\' ' );
            Measure_Master__c rule = [SELECT Id, State__c FROM Measure_Master__c WHERE Id=:ruleId];
            rule.State__c = 'Publishing';
            update rule;
            Database.executeBatch(batchExecute, 1500);
            populateRuleList();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Submitted for Publishing'));
        }

        publishFlag = false;
    }

    public string recordsToCheck {get;set;}
    public void checkExecutionState(){
        list<CheckedRecordsState> recordsState = (List<CheckedRecordsState>)JSON.deserialize(recordsToCheck, CheckedRecordsState.class);
        system.debug(recordsState);
    }

    public class CheckedRecordsState{
        public string state{get;set;}
        public Integer row{get;set;}
        public Integer col{get;set;}
        public String recordId{get;set;}
    }

    public class GridListWrapper{
        public String Id{get;set;}
        public String action{get;set;}
        public String name{get;set;}
        public String product{get;set;}
        public String cycle{get;set;}
        public String businessUnit{get;set;}
        public String line{get;set;}
        public DateTime createdOn{get;set;}
        public String lastUpdatedBy{get;set;}
        public String Status{get;set;}
        public String preview{get;set;}
        public Boolean isExecuted{get;set;}
        public Boolean isPublished{get;set;}
        public String downloadLink{get;set;}
        public Boolean isGlobal {get;set;}
        public GridListWrapper(Measure_Master__c rule){
            this.Id = rule.Id;
            this.Name = rule.Name;
            this.action = '';
            this.product = rule.Brand_Lookup__r.Name;
            this.cycle = rule.Cycle__r.Name;
           // this.line = rule.Line_2__r.name;
            //this.businessUnit = rule.Team__r.Name;
            this.businessUnit = rule.Team_Instance__r.Name;
            this.createdOn = rule.LastModifiedDate;
            this.lastUpdatedBy = rule.LastModifiedBy.Name;
            this.Status = rule.State__c;
            this.isExecuted = rule.is_executed__c;
            this.isPublished = rule.is_promoted__c;
            this.preview = '';
            this.isGlobal = rule.isGlobal__c;
            this.downloadLink = String.isNotBlank(rule.File_Download_Id__c)? rule.File_Download_Id__c : '';
        }
    }
}