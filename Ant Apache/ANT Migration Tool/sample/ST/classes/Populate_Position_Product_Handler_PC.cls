public with sharing class Populate_Position_Product_Handler_PC {
    public Populate_Position_Product_Handler_PC() {
        
    }
    
    public static void populatePosition(List<Product_Catalog__c> allPosProducts)
    {
        List<Team_Instance_Product_AZ__c> TeamInstProdAZToInsert = new List<Team_Instance_Product_AZ__c>();
        Team_Instance_Product_AZ__c TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
        for(Product_Catalog__c tip : allPosProducts)
        {
            TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
            
            TeamInstProdAZRec.Product_Catalogue__c=tip.id;
            TeamInstProdAZRec.Team_Instance__c=tip.Team_Instance__c;
            TeamInstProdAZRec.Vacation_Days__c = 0;
            TeamInstProdAZRec.Holidays__c = 0;
            TeamInstProdAZRec.Calls_Day__c = 0;
            TeamInstProdAZRec.Other_Days_Off__c = 0;
            
            TeamInstProdAZToInsert.add(TeamInstProdAZRec);
        }

        insert TeamInstProdAZToInsert;
    }

    public static void deletePosition(List<Product_Catalog__c> allPosProducts)
    {
        system.debug('allPosProducts++++++'+allPosProducts);
        Set<String> allTeamInstances = new Set<String>();
        Set<String> allProducts = new Set<String>();
        Set<String> posProdCombo = new Set<String>();

        for(Product_Catalog__c ti :allPosProducts)
        {
            allTeamInstances.add(ti.Team_Instance__c);
            allProducts.add(ti.ID);
            posProdCombo.add(ti.Team_Instance__c + '_' + ti.ID);
        }
        system.debug('allTeamInstances++++++'+allTeamInstances);
        system.debug('allProducts++++++'+allProducts);
        system.debug('posProdCombo++++++'+posProdCombo);

        List<Team_Instance_Product_AZ__c> TeamInstProdAZToDel = new List<Team_Instance_Product_AZ__c>();
        List<Team_Instance_Product_AZ__c> TeamInstProdAZList = new List<Team_Instance_Product_AZ__c>();
        TeamInstProdAZList = [select id, Product_Catalogue__c, Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c in :allTeamInstances and Product_Catalogue__c in :allProducts];
        
        //Team_Instance_Product_AZ__c TeamInstProdAZRec = new Team_Instance_Product_AZ__c();
        system.debug('TeamInstProdAZList++++++'+TeamInstProdAZList);
        for(Team_Instance_Product_AZ__c pp : TeamInstProdAZList)
        {
            if(posProdCombo.contains(pp.Team_Instance__c + '_' + pp.Product_Catalogue__c))
            {
                TeamInstProdAZToDel.add(pp);
            }
        }
        system.debug('TeamInstProdAZToDel++++++'+TeamInstProdAZToDel);
        delete TeamInstProdAZToDel;
    }

    /*public static void populatePosition(List<Product_Catalog__c> allPosProducts)
    {
        Set<String> allTeamInstances = new Set<String>();
        Map<String, Set<ID>> allProductsInTeam = new Map<String, Set<ID>>();

        for(Product_Catalog__c tip : allPosProducts)
        {
            allTeamInstances.add(tip.Team_Instance__c);
            String teamInstance = String.valueOf(tip.Team_Instance__c);

            if(allProductsInTeam.containsKey(teamInstance))
            {
                allProductsInTeam.get(teamInstance).add(tip.id);
            }
            else
            {
                Set<ID> tempSet = new Set<ID>();
                tempSet.add(tip.id);
                allProductsInTeam.put(teamInstance, tempSet);
            }
        }

        List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances];

        List<AxtriaSalesIQTM__Position_Product__c> positionProductsToInsert = new List<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position__c pos : allPositions)
        {
            String teamInstance = pos.AxtriaSalesIQTM__Team_Instance__c;

            for(ID product : allProductsInTeam.get(teamInstance))
            {
                AxtriaSalesIQTM__Position_Product__c posPro = new AxtriaSalesIQTM__Position_Product__c();
                posPro.AxtriaSalesIQTM__Position__c = pos.id;
                //obj.Product_Master__c = (Id) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Product_Master__c');
                posPro.AxtriaSalesIQTM__Team_Instance__c =teamInstance;
                posPro.Product_Catalog__c = product;
                posPro.Vacation_Days__c = 0;
                posPro.Holidays__c = 0;
                posPro.Calls_Day__c = 0;
                posPro.Business_Days_in_Cycle__c = 0;
                posPro.AxtriaSalesIQTM__Product_Weight__c = 0;    
                posPro.AxtriaSalesIQTM__isActive__c = true;
                posPro.AxtriaSalesIQTM__Effective_Start_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                posPro.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffendDate__c;

                positionProductsToInsert.add(posPro);
            
            }
        }
        insert positionProductsToInsert;
    }

    public static void deletePosition(List<Product_Catalog__c> allPosProducts)
    {
        Set<String> allTeamInstances = new Set<String>();
        Set<String> allProducts = new Set<String>();
        Set<String> posProdCombo = new Set<String>();

        for(Product_Catalog__c ti :allPosProducts)
        {
            allTeamInstances.add(ti.Team_Instance__c);
            allProducts.add(ti.ID);
            posProdCombo.add(ti.Team_Instance__c + '_' + ti.ID);
        }

        List<AxtriaSalesIQTM__Position_Product__c> allPosProductsDel = [select id, AxtriaSalesIQTM__Position__c, Product_Catalog__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and Product_Catalog__c in :allProducts];
        
        List<AxtriaSalesIQTM__Position_Product__c> deletePosProduct = new List<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position_Product__c pp : allPosProductsDel)
        {
            if(posProdCombo.contains(pp.AxtriaSalesIQTM__Team_Instance__c + '_' + pp.Product_Catalog__c))
            {
                deletePosProduct.add(pp);
            }
        }

        delete deletePosProduct;
    }*/
}