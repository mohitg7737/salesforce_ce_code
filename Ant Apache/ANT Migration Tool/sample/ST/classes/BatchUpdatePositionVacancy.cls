global class BatchUpdatePositionVacancy implements Database.Batchable<sObject>{
    string query;
    public String batchID;
    set<Id> newEmpId = new set<Id>();
    list<AxtriaSalesIQTM__Employee__c> lstEmployee = new list<AxtriaSalesIQTM__Employee__c>();
    set<AxtriaSalesIQTM__Employee__c> lstEmployeeset = new set<AxtriaSalesIQTM__Employee__c>();
    map<string,AxtriaSalesIQTM__Employee__c> newMapEmp= new map<string,AxtriaSalesIQTM__Employee__c>();
    
    global BatchUpdatePositionVacancy(String ID){
    	 batchID=ID;
        query='select id,AxtriaSalesIQTM__Assignment_status__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Employee__c'+
            ' from AxtriaSalesIQTM__Position__c';
    }
    
         
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void UpdateVacanyData(list<AxtriaSalesIQTM__Position__c> listPosition)
    {
        list<AxtriaSalesIQTM__Position__c> lstUpdatePosition= new list<AxtriaSalesIQTM__Position__c>();
        set<id> posId= new set<id>();
        
        for(AxtriaSalesIQTM__Position__c pos : listPosition){
            posId.add(pos.Id);
            newEmpId.add(pos.AxtriaSalesIQTM__Employee__c);
        }
        map<string, AxtriaSalesIQTM__Position_Employee__c> mapPosEmp = 
                new map<string, AxtriaSalesIQTM__Position_Employee__c>();
        
        for(AxtriaSalesIQTM__Position_Employee__c posemp: [select id,AxtriaSalesIQTM__Effective_End_Date__c,
                                                           AxtriaSalesIQTM__position__r.AxtriaSalesIQTM__Client_Position_Code__c  
                                                           from AxtriaSalesIQTM__Position_Employee__c where 
                                                          AxtriaSalesIQTM__position__r.id in: posId and 
                                                          AxtriaSalesIQTM__Assignment_Type__c = 'Primary' and 
                                                           AxtriaSalesIQTM__Effective_End_Date__c <:system.today() 
                                                          order by AxtriaSalesIQTM__Effective_End_Date__c desc])
        {
            mapPosEmp.put(posemp.AxtriaSalesIQTM__position__r.AxtriaSalesIQTM__Client_Position_Code__c, posemp);
        }
        
        for(AxtriaSalesIQTM__Position__c position : listPosition)
        {
            if(position.AxtriaSalesIQTM__Assignment_status__c == 'Vacant')
            {
                position.isTriggerRequired__c=false;
                AxtriaSalesIQTM__Position_Employee__c  posEmp =  mapPosEmp.get(position.AxtriaSalesIQTM__Client_Position_Code__c);
                if(posEmp != null)                
                position.Vacant_for_days__c=posEmp.AxtriaSalesIQTM__Effective_End_Date__c.daysBetween(system.today());
                else
                position.Vacant_for_days__c=position.AxtriaSalesIQTM__Effective_Start_Date__c.daysBetween(system.today());
            }
            else
            { 
                position.isTriggerRequired__c=false;
                position.Vacant_for_days__c=0;
            }
            lstUpdatePosition.add(position);
        }
        update lstUpdatePosition;
    }
    
    public void UpdateEmployeePosition(list<AxtriaSalesIQTM__Position__c> listPosition){
        for(AxtriaSalesIQTM__Employee__c employee : [select id,AxtriaSalesIQTM__HR_Status__c,
                                                     CurrentPositionId__c,Current_Position__c,
                                                     AxtriaSalesIQTM__Current_Territory__c
                                                     from AxtriaSalesIQTM__Employee__c where 
                                                     id in: newEmpId])
        {
            newMapEmp.put(employee.id, employee);
        }
        
        for(AxtriaSalesIQTM__Position__c newPosition: listPosition){
            if(newPosition.AxtriaSalesIQTM__Assignment_status__c=='Filled' && 
               newPosition.AxtriaSalesIQTM__Employee__c != null)
            {                        
                system.debug('---log AxtriaSalesIQTM__Employee__c----'+newPosition.AxtriaSalesIQTM__Employee__c);
                AxtriaSalesIQTM__Employee__c employee = newMapEmp.get(newPosition.AxtriaSalesIQTM__Employee__c);
                system.debug('---log employee----'+employee);
                if(employee.AxtriaSalesIQTM__HR_Status__c != 'Terminated')
                {
                    employee.CurrentPositionId__c=newPosition.AxtriaSalesIQTM__Client_Position_Code__c;
                    //employee.CurrentPosition__c=newPosition.name;
                    employee.Current_Position__c=newPosition.id;
                    employee.AxtriaSalesIQTM__Current_Territory__c=newPosition.id;
                    lstEmployee.add(employee);
                }
            } 
        }
        lstEmployeeset.addAll(lstEmployee);
        lstEmployee = new list<AxtriaSalesIQTM__Employee__c>();
        lstEmployee.addAll(lstEmployeeset);
        update lstEmployee;
    }
    
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position__c> listPosition){
        UpdateVacanyData(listPosition);
        UpdateEmployeePosition(listPosition);
    }
    
    global void finish(Database.BatchableContext BC)
    { 
       Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
    } 
    
}