public with sharing class ChnageRequestTriggerHandlerAZ {
public static void createCRCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList){    
	system.debug('---entered create CR callplan func');
      AxtriaSalesIQTM__Change_Request__c CR;
        for(AxtriaSalesIQTM__Change_Request__c objCR: changeRequestList ) CR = objCR;
        ID crID = CR.ID;
        List<String> accChanged = CR.AxtriaSalesIQTM__Account_Moved_Id__c.split(',');        
        ID destPos = CR.AxtriaSalesIQTM__Destination_Position__c;
    id teamInstanceID = CR.AxtriaSalesIQTM__Team_Instance_ID__c;  
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allChangedRec = new list <AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    
        string soql ='select Segment_Approved__c,Segment__c, AxtriaSalesIQTM__lastApprovedTarget__c,TCF_Change__c,AxtriaSalesIQTM__Account__r.ID,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,Final_TCF__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Segment7__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Segment2__c,AxtriaSalesIQTM__Segment8__c,Final_TCF_Original__c,Final_TCF_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id in :accChanged and AxtriaSalesIQTM__Position__c =:destPos and AxtriaSalesIQTM__Team_Instance__c =:teamInstanceID';
        
            allChangedRec = Database.query(soql);
       
        List<AxtriaSalesIQTM__CR_Call_Plan__c> crCallPlanRequests = new List<AxtriaSalesIQTM__CR_Call_Plan__c>();        
        AxtriaSalesIQTM__CR_Call_Plan__c tempCRCallPlan;         
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allChangedRec) 
        {
            tempCRCallPlan = new AxtriaSalesIQTM__CR_Call_Plan__c();
            tempCRCallPlan.AxtriaSalesIQTM__Change_Request_ID__c = crID;
            tempCRCallPlan.name = 'CR-' + SalesIQUtility.GenerateRandomNumber();
            tempCRCallPlan.TCF_Change_Reason__c = pacp.TCF_Change__c;
           // if(pacp.Final_TCF_Approved__c != (pacp.Final_TCF__c)){
              tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_Before__c = (pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c);
              tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_After__c = pacp.AxtriaSalesIQTM__Picklist1_Updated__c;
              tempCRCallPlan.AxtriaSalesIQTM__Original_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c;
              
              tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Updated__c;
             // tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
              tempCRCallPlan.Updated_Calls__c = pacp.Final_TCF__c;
              tempCRCallPlan.Original_Calls__c = pacp.Final_TCF_Original__c;
              tempCRCallPlan.AxtriaSalesIQTM__Before_Segment1__c = pacp.Segment_Approved__c;
              tempCRCallPlan.AxtriaSalesIQTM__After_Segment1__c = pacp.Segment__c;
                    
              
         // }
          if(tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c== 'None'){
              tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c='';
          }
          
          tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
          tempCRCallPlan.AxtriaSalesIQTM__isExcludedCallPlan__c = pacp.AxtriaSalesIQTM__isAccountTarget__c;
          if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c== true && tempCRCallPlan.AxtriaSalesIQTM__isExcludedCallPlan__c== true){
               tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c='';
          }

          

          if(pacp.Segment_Approved__c !=pacp.Segment__c){
              tempCRCallPlan.change__c = 'Segment Change';
          }
          if((pacp.Segment_Approved__c ==pacp.Segment__c) && (pacp.Final_TCF_Approved__c!=pacp.Final_TCF__c)){
              tempCRCallPlan.change__c = 'TCF Change';
          }

          if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && pacp.AxtriaSalesIQTM__lastApprovedTarget__c == false)
          {
              tempCRCallPlan.change__c = 'Added';
          }

          if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == false && pacp.AxtriaSalesIQTM__lastApprovedTarget__c == true)
          {
              tempCRCallPlan.change__c = 'Dropped';
          }


          tempCRCallPlan.AxtriaSalesIQTM__Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
          tempCRCallPlan.AxtriaSalesIQTM__Account__c = pacp.AxtriaSalesIQTM__Account__r.ID;
          crCallPlanRequests.add(tempCRCallPlan);
        }
        
            
        if(crCallPlanRequests!=null && crCallPlanRequests.size() > 0){
          
          
              insert crCallPlanRequests;
          
        }
        
        if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
          updatePositionAccountCallPlan(changeRequestList);
        }
    }
    
    /**
  * This method updates Position Account Call Plan after Call Plan Request is Approved / Rejected  
  * @param List<AxtriaSalesIQTM__Change_Request__c> list of all Change Requests
  */    
    public static void updatePositionAccountCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList)
    {
    	system.debug('---entered update PACP func');
        AxtriaSalesIQTM__Change_Request__c CR;
        for(AxtriaSalesIQTM__Change_Request__c objCR: changeRequestList ) CR = objCR;
        if(CR != null){
        	system.debug('-----status11 is:'+CR.AxtriaSalesIQTM__Status__c);
          if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || CR.AxtriaSalesIQTM__Status__c ==SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED) {
          ID crID = CR.ID;
             List<String> accChanged = CR.AxtriaSalesIQTM__Account_Moved_Id__c.split(',');
             Map<String,AxtriaSalesIQTM__CR_Call_Plan__c> crCallPlanMap = null;            
           
             string soql= 'select AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Updated_Reason_Code__c   ,AxtriaSalesIQTM__Rep_Goal_After__c from AxtriaSalesIQTM__CR_Call_Plan__c where AxtriaSalesIQTM__Change_Request_ID__c= :crID';
            
            List<AxtriaSalesIQTM__CR_Call_Plan__c> allCRrecs = new list <AxtriaSalesIQTM__CR_Call_Plan__c>();
          
              allCRrecs = Database.query(soql);
          
            
            if(allCRrecs != null && allCRrecs.size() > 0){
              crCallPlanMap = new Map<String,AxtriaSalesIQTM__CR_Call_Plan__c>();
                for(AxtriaSalesIQTM__CR_Call_Plan__c crcp : allCRrecs){
                  crcp.AxtriaSalesIQTM__Status__c = CR.AxtriaSalesIQTM__Status__c;
                    crCallPlanMap.put(crcp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,crcp);  
                }
            
              ID destPos = CR.AxtriaSalesIQTM__Destination_Position__c;
        ID teamInstanceID = CR.AxtriaSalesIQTM__Team_Instance_ID__c;
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> posAccCallPlanRecs = new list <AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
            soql ='select AxtriaSalesIQTM__Position__r.Call_Plan_Status__c,Segment_Approved__c,Segment__c, AxtriaSalesIQTM__Account__r.ID,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment__c, AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Picklist2_Segment__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c,Final_TCF__c,Final_TCF_Approved__c,isFinalTCFApproved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id in :accChanged and AxtriaSalesIQTM__Position__c =:destPos and AxtriaSalesIQTM__Team_Instance__c =:teamInstanceID';
               
            
                  posAccCallPlanRecs = Database.query(soql);
                  AxtriaSalesIQTM__Position__c pos=[select id ,Call_Plan_Status__c from AxtriaSalesIQTM__Position__c where  id=: cr.AxtriaSalesIQTM__Destination_Position__c] ;
             
              boolean flag = true; // Incase any other changes have been made
              
              list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> lstToUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
              for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : posAccCallPlanRecs) {
                if(crCallPlanMap.containsKey(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c)){
                    AxtriaSalesIQTM__CR_Call_Plan__c crcp = crCallPlanMap.get(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c);
                      system.debug('-----status22 is:'+CR.AxtriaSalesIQTM__Status__c);
                      if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || CR.AxtriaSalesIQTM__Status__c == 'Approvato')
                      {
                      	system.debug('---inside approved state--');
                          pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c  =  pacp.AxtriaSalesIQTM__Picklist3_Updated__c ;
                          pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c  =  pacp.AxtriaSalesIQTM__Picklist1_Updated__c ;
                          pacp.AxtriaSalesIQTM__Change_Status__c    =    SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
                          pacp.AxtriaSalesIQTM__Metric3_Approved__c =    pacp.AxtriaSalesIQTM__Metric3_Updated__c;
                          pacp.AxtriaSalesIQTM__Picklist2_Segment_Approved__c =  pacp.AxtriaSalesIQTM__Picklist2_Updated__c;
                          pacp.Parameter1_Approved__c= pacp.Parameter1__c;  
                          pacp.Parameter2_Approved__c=pacp.Parameter2__c;  
                          pacp.Parameter3_Approved__c=pacp.Parameter3__c;  
                          pacp.Parameter4_Approved__c =pacp.Parameter4__c;  
                          pacp.Parameter5_Approved__c=pacp.Parameter5__c;  
                          pacp.Parameter6_Approved__c= pacp.Parameter6__c;  
                          pacp.Parameter7_Approved__c=pacp.Parameter7__c;  
                          pacp.Parameter8_Approved__c= pacp.Parameter8__c;  
                          pacp.Final_TCF_Approved__c=pacp.Final_TCF__c; 
                          pacp.Segment_Approved__c =pacp.Segment__c;
                          pacp.isFinalTCFApproved__c = true;                
           	              pacp.AxtriaSalesIQTM__lastApprovedTarget__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
           	              
           	              //pos.Call_Plan_Status__c='Approved';
                          
                          
                          
                      }
                      else if(CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED || CR.AxtriaSalesIQTM__Status__c == 'Respinto'){
                          
	                        	pacp.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
	                        //	pacp.AxtriaSalesIQTM__Position__r.Call_Plan_Status__c='Saved';
	                            /*pacp.Final_TCF__c = pacp.Final_TCF_Approved__c;
	                            if(pacp.Final_TCF_Original__c !=pacp.Final_TCF_Approved__c){
                                 pacp.AxtriaSalesIQTM__Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
                                 
                             }*/
                              //pos.Call_Plan_Status__c='Pending for Submission';
                     }
                     
                      else{ flag = false; }
                      lstToUpdate.add(pacp);
                  }
              
                 
              }
              try{
                  
                    
                    update lstToUpdate;
                  
                    AxtriaSalesIQTM__Position__c ob=[select id,Call_Plan_Status__c from AxtriaSalesIQTM__Position__c where id=:destPos];
                    
                    update allCRrecs;
                    if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || CR.AxtriaSalesIQTM__Status__c == 'Approvato')
                    {
                        ob.Call_Plan_Status__c='Approved';
                    }
                    else if(CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED || CR.AxtriaSalesIQTM__Status__c == 'Respinto')
                    {
                        ob.Call_Plan_Status__c='Pending for Submission';    
                    }
                    update ob;
              }
              catch(Exception e){              
                  system.debug('exception id updatePositionAccountCallPlan ' + e.getMessage());
              }
          }
            updateMetricSummaryTable(CR.AxtriaSalesIQTM__Status__c , CR.AxtriaSalesIQTM__Destination_Position__c,CR.AxtriaSalesIQTM__Team_Instance_ID__c );            
        }
        }
        
    }
    
    /**
  * This method updates metric Summary Table after call plan request is Approved or Rejected  
  * @param String :- Request is approved or rejected
  * @param string Position ID
  */    
     
    public static void updateMetricSummaryTable(string type , string posID , string teamInstance){
      Set<ID> allCIMids = new Set<ID>();
      List<AxtriaSalesIQTM__CIM_Config__c> cimConfigRecs = new list <AxtriaSalesIQTM__CIM_Config__c>();
    string cimQuery = 'select id from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstance and AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c = \'Call_Plan_Change\'';
    
         cimConfigRecs = Database.query(cimQuery);
    
    for(AxtriaSalesIQTM__CIM_Config__c ccm : cimConfigRecs){
      allCIMids.add(ccm.ID);  
    }
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPosMetricRecs = new list <AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();        
        string fetchAllPosMetric = 'select AxtriaSalesIQTM__Proposed__c, AxtriaSalesIQTM__Approved__c,AxtriaSalesIQTM__CIM_Config__r.Name from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in :allCIMids and AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c = :posID ';
     
      cimPosMetricRecs = Database.query(fetchAllPosMetric);
        
      if(type == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){    
            for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms : cimPosMetricRecs)
              cpms.AxtriaSalesIQTM__Approved__c = cpms.AxtriaSalesIQTM__Proposed__c;
      }else if(type == SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || type ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED  || type ==  SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED   ){
          for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms : cimPosMetricRecs){
          	
          	if(AxtriaSalesIQTM__TriggerContol__c.getValues('LastApprovedState').AxtriaSalesIQTM__IsStopTrigger__c){
              cpms.AxtriaSalesIQTM__Proposed__c = cpms.AxtriaSalesIQTM__Approved__c;
	      	}
          }	
        }
      update cimPosMetricRecs;   
  }

}