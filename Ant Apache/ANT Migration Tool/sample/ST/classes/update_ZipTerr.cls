global class update_ZipTerr implements Database.Batchable<sObject>, Database.Stateful  {
    
    global string query;
    global string teamID;
    global string teamInstance;
    global string teamName;
    global list<AxtriaSalesIQTM__Team__c> temlst=new list<AxtriaSalesIQTM__Team__c>();
    global list<AxtriaSalesIQTM__Geography__c> ziplist=new list<AxtriaSalesIQTM__Geography__c>();
    global list<AxtriaSalesIQTM__Position__c> poslist=new list<AxtriaSalesIQTM__Position__c>();
    global list<temp_Zip_Terr__c> zipTerrlist=new list<temp_Zip_Terr__c>();
    
    
    global update_ZipTerr(String Team,String TeamIns){
        
        teamInstance=TeamIns;
        teamID=Team;
        temlst=[select name from AxtriaSalesIQTM__Team__c where id=:teamID];
        teamName=temlst[0].name;
        poslist=[select id,AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_iD__c=:teamID and AxtriaSalesIQTM__Team_Instance__c=:teamInstance];
        query='SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Zip_Terr__c where Team_Name__c=:teamName';
       

    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Zip_Terr__c>zipTerrlist){
        
        for(temp_Zip_Terr__c rec:zipTerrlist){
            while (rec.Zip_Name__c.length() < 5)  
               { 
              rec.Zip_Name__c = '0' + rec.Zip_Name__c; 
              } 
        }                                                    //formating zip to "00000"
        
        //NISHANT Update
        
        
        list<string> list1=new list<string>();
        for(temp_Zip_Terr__c a:zipTerrlist){
            list1.add(a.Zip_Name__c);
        }
        
      
        list<temp_Zip_Terr__c> zipTerListUpdate = new list<temp_Zip_Terr__c>();
        ziplist=[select id,name from AxtriaSalesIQTM__Geography__c where name in: list1 ];
        for(temp_Zip_Terr__c rec:zipTerrlist){ 
            
            while (rec.Zip_Name__c.length() < 5)  
               { 
              rec.Zip_Name__c = '0' + rec.Zip_Name__c; 
              }                                                            //formating zip to "00000" for update in object
             
            for(AxtriaSalesIQTM__Geography__c zip : ziplist){
                if(zip.name == rec.Zip_Name__c)  rec.Geography__c=zip.id;               //geography lookup
            }
            for(AxtriaSalesIQTM__Position__c pos:poslist) {
                if(rec.Territory_ID__c==pos.AxtriaSalesIQTM__Client_Position_Code__c)  rec.Position__c=pos.id;   //position lookup
            }
            rec.Team__c=teamID;
            rec.Team_Instance__c=teamInstance;
            zipTerListUpdate.add(rec);
        }
        update zipTerListUpdate;
        
        system.debug('******************************update done************************************');
          
    }
    
          global void finish(Database.BatchableContext BC){

            Database.executeBatch(new  update_PositionGeography(teamID,teamInstance),500);  
       }
}