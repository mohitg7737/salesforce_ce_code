global class update_position_Acc implements Database.Batchable<sObject>, Database.Stateful  {
    
    global string query;
    global string teamID;
    global string teamInstance;
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Account__c> accTI=new list<AxtriaSalesIQTM__Team_Instance_Account__c>();
    
    
    global update_position_Acc(String Team,String TeamIns){
        teamInstance=TeamIns;
        teamID=Team;
        teminslst=[select AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c=:teamID];
        query='SELECT Account__c,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,AccountNumber__c,Id,Territory__c,Territory_ID__c,Team__c,Status__c FROM temp_Acc_Terr__c where Team__c=: teamID and Territory__c != null and status__c = \'New\'';
        
    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Acc_Terr__c>accterrlist){
        
        list<String> list1                 = new list<String>();
        list<String> list2                 = new list<String>();
        set<string> setPosAccTeamInstance  = new set<string>();
        list<temp_Acc_Terr__c> lsTempRecordToUpdate = new list<temp_Acc_Terr__c>();
        
        
        for(temp_Acc_Terr__c rec:accterrlist){
            list1.add(rec.Account__c);
            list2.add(rec.Territory__c);
        }
        
        //query existing position accounts with the key = Position+AccountNumber+TeamInstanceName
        list<AxtriaSalesIQTM__Position_Account__c> lsposAcc = [select id,name,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Account__c in:list1 and AxtriaSalesIQTM__Assignment_Status__c = 'Active']; 
        
        for(AxtriaSalesIQTM__Position_Account__c pa : lsposAcc){
            setPosAccTeamInstance.add(string.valueof(pa.AxtriaSalesIQTM__Position__c)+string.valueof(pa.AxtriaSalesIQTM__Account__c)+string.valueof(pa.AxtriaSalesIQTM__Team_Instance__c));
        }
        
        map<string,string> posmap           =new map<string,string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI:[select AxtriaSalesIQTM__Position_ID__c,id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstance])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c,posTI.id);
        }
        map<string,string> accmap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Account__c accTI:[select AxtriaSalesIQTM__Account_ID__c,id from AxtriaSalesIQTM__Team_Instance_Account__c where AxtriaSalesIQTM__Account_ID__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            accmap.put(accTI.AxtriaSalesIQTM__Account_ID__c,accTI.id);
        }
        
        list<AxtriaSalesIQTM__Position_Account__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(temp_Acc_Terr__c cprcd : accterrlist){
                if(!setPosAccTeamInstance.contains(string.valueof(cprcd.Territory__c)+string.valueof(cprcd.Account__c)+string.valueof(teamInstance))){
                      AxtriaSalesIQTM__Position_Account__c posAcc=new AxtriaSalesIQTM__Position_Account__c();
                  
                      posAcc.AxtriaSalesIQTM__Account__c                  = cprcd.Account__c;
                      posAcc.AxtriaSalesIQTM__Position__c                 = cprcd.Territory__c;
                      posAcc.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                      posAcc.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(cprcd.Territory__c);
                      posAcc.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                      posAcc.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                      posAcc.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                      posAcc.AxtriaSalesIQTM__Account_Alignment_Type__c   = 'Explicit';
                      posAccListUpdate.add(posAcc);
                      //Update Temp Record
                      cprcd.status__c = 'Processed';
                      lsTempRecordToUpdate.add(cprcd);
                }else{
                    cprcd.status__c = 'Rejected';
                    lsTempRecordToUpdate.add(cprcd);
                
                }   
              
          }
          
          if(posAccListUpdate.size()!=0){
            insert posAccListUpdate;
          }
          
          if(lsTempRecordToUpdate.size()!=0){
            update lsTempRecordToUpdate;
          }
          
          
          
          system.debug('============insert done===================');
          
    }
    
          global void finish(Database.BatchableContext BC){

             /*string selectedObject = 'temp_Acc_Terr__c';
            Database.executeBatch(new batchdelete3(teamID,selectedObject),1000);*/
            
       }

}