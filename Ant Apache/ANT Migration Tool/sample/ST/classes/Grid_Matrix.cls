public class Grid_Matrix {
	//public Matrix_Component_Controller thedata{get;set;}
	public ComponentData comp{get;set;}
    public String Name {get;set;}
    public String Description {get;set;}
    public boolean show1{get;set;}
    public integer row1 {get;set;}
    public integer col1{get;set;}
    public list<integer> rows {get;set;}
    public list<integer> cols{get;set;}
    public boolean showtable {get;set;}
    public boolean unique {get;set;}
    public String dm1 {get;set;}
    public String dm2 {get;set;}
    public boolean oned {get;set;}
    public boolean twod {get;set;}
    public boolean newtbl {get;set;}
    public list<String>dmlist {get;set;}
    public list<Grid_Details__c>d1 {get;set;}
    public boolean showdefineblock {get;set;}
    public String header_col {get;set;}
    public String header_row {get;set;}
    public String Output_Text {get;set;}
    public String gmid{get;set;}
    public list<Grid_Details__c>gridlist {get;set;}
    public Grid_Details__c gd {get;set;}
    public String JSONdata{get;set;}
    public boolean showoneD {get;set;}
    public String gridid {get;set;}
    public String modeis {get;set;}
	public Grid_Master__c gridMaster;
	public boolean Viewblock {get;set;}
	public boolean Cloneblock {get;set;}
	public boolean Editblock {get;set;}
	public list<Grid_Master__c>gridmasterlist {get;set;}
	public list<Grid_Details__c>GridListOld {get;set;}
	public boolean newgrid {get;set;}
	public boolean ViewShow2d {get;set;}
	public boolean Viewrowsiz {get;set;}
	public boolean View2Dtable {get;set;}
	public boolean View1Dtable {get;set;}
	public String VDM1{get;set;}
	public String VDM2{get;set;}
	public set<String>RHeaderlist {get;set;}
	public set<String>CHeaderlist {get;set;}
	public list<String>result {get;set;}
	public list<String>rowheaderlist {get;set;}
	public map<String,Grid_Details__c>map2d {get;set;}
	public list<Grid_Master__c>updatelist {get;set;}
	public map<String,Grid_Details__c>map1d {get;set;}
	public string myJSON{get;set;}
    public String selectedBrand{get;set;}
    public list<SelectOption> brands{get;set;}
    public String countryID {get;set;}
	
	public Grid_Matrix(ApexPages.StandardController gm){
		comp = new ComponentData();
	    dmlist = new list<String>();
	    d1 = new list<Grid_Details__c>();
	    show1=false;
	    showtable=false;
	    showoneD = false;
	    ViewShow2d = false;
	    Viewrowsiz = false;
	    unique=false;
	    oned=false;
	    twod=false;
	    newtbl =false;
	    Viewblock=false;
	    Cloneblock=false;
	    showdefineblock=false;
	    Editblock=false;
	    newgrid=false;
	    Output_Text='';
	    JSONdata='';
	    cols = new list<Integer>();
	   	rows = new list<Integer>();
	    this.gridMaster = (Grid_Master__c)gm.getRecord();
	    system.debug('----in controller:::'+this.gridMaster);
	    modeis=Apexpages.currentPage().getParameters().get('mode');
	    system.debug('-----mode----------is:'+modeis);
	    gridid=Apexpages.currentPage().getParameters().get('msg');
	    system.debug('-----id----------is:'+gridid);
	    comp.Mode='View';
	    if(modeis == 'View'){
	    	comp.Mode='View';
	    	ViewBlock(modeis,gridid);
	    }
	    else if(modeis == 'Clone'){
	    	comp.Mode='Clone';
	    	CloneBlock(modeis,gridid);
	    }
	    else if(modeis == 'Edit'){
	    	comp.Mode='Edit';
	    	EditBlock(modeis,gridid);
	    }
	    else {
	    	newgrid=true;
	    }

        brands = new list<SelectOption>();
        brands.add(new SelectOption('None', 'None'));
        
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());

        for(Product_Catalog__c brand : [select id, Name FROM Product_Catalog__c where Country__c = :countryID]){
            brands.add(new SelectOption(brand.Id, brand.Name));
        }
}
	public void ViewBlock(String mode,String gid){
		system.debug('******Entered View Block ********');
		Viewblock=true;
		
		gridmasterlist=new list<Grid_Master__c>();
		GridListOld=new list<Grid_Details__c>();
		RHeaderlist = new set<String>();
		CHeaderlist = new set<String>();
		result=new list<String>();
		rowheaderlist = new list<String>();
		map2d = new map<String,Grid_Details__c>();
		
		gridmasterlist=[SELECT id,Name,Description__c, isGlobal__c, Brand__r.Name,Dimension_1_Name__c,Dimension_2_Name__c,Grid_Type__c,Output_Name__c,Output_Type__c,Brand_Team_Instance__r.Team_Instance__r.Name,Row__c,Col__c,DM2_Output_Type__c,DM1_Output_Type__c from Grid_Master__c where id=:gid];
		system.debug('----gridmasterlist size is():'+gridmasterlist.size());
		GridListOld=[select id,Name,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c,colvalue__c from Grid_Details__c where Grid_Master__c=:gid order by Rowvalue__c];
		system.debug('----gridlistOLD size is():'+GridListOld.size());
		for(Grid_Details__c g : GridListOld){
			RHeaderlist.add(g.Dimension_1_Value__c);
			CHeaderlist.add(g.Dimension_2_Value__c);
			map2d.put(g.Name,g);
			system.debug('----CHeaderlist Set Elemnet:'+CHeaderlist);
		}
		
		result.addAll(CHeaderlist);
		rowheaderlist.addAll(RHeaderlist);
		comp.griddata=map2d;
		comp.cheaders=result;
		comp.rheaders=rowheaderlist;
		comp.Mode='View';
		system.debug('---COMP.Griddata---:'+comp.griddata);
		system.debug('---COMP.Cheaders---:'+comp.cheaders);
		system.debug('---COMP.Rheaders---:'+comp.rheaders);
		system.debug('----RHeaderlist size is():'+RHeaderlist.size());
		system.debug('----CHeaderlist size is():'+CHeaderlist.size());
		system.debug('----result List size is():'+result);
		system.debug('----Map Value is:::'+map2d);
		
		VDM1=gridmasterlist[0].Dimension_1_Name__c;
		//if(gridmasterlist[0].Grid_Type__c == '2D'){
			
			Viewrowsiz = true;
			View2Dtable= true;	
			VDM2=gridmasterlist[0].Dimension_2_Name__c;
			if(gridmasterlist[0].Col__c != null){
				for(integer i=1;i<=gridmasterlist[0].Col__c;i++){
					cols.add(i);
				}
			}
			if(gridmasterlist[0].Row__c != null){
				for(integer i=1;i<=gridmasterlist[0].Row__c;i++){
					Rows.add(i);
				}
			}
			if(gridmasterlist[0].Grid_Type__c == '2D')
				ViewShow2d=true;
		/*else{
			View1Dtable =true;
			if(gridmasterlist[0].Col__c != null){
				for(Integer i = 1; i<=gridmasterlist[0].Col__c;i++){
	                    cols.add(i);
	                }
				}
				
			}*/
			
		
	}
	public void CloneBlock(String mode,String gid){
		system.debug('******Entered Clone Block ********');
		Cloneblock=true;
		gridmasterlist=new list<Grid_Master__c>();
		GridListOld=new list<Grid_Details__c>();
		RHeaderlist = new set<String>();
		CHeaderlist = new set<String>();
		result=new list<String>();
		rowheaderlist = new list<String>();
		map2d = new map<String,Grid_Details__c>();
		
		
		gridmasterlist=[SELECT id,Name,Description__c, isGlobal__c, Brand__r.Name,Brand__c,Dimension_1_Name__c,Dimension_2_Name__c,Grid_Type__c,Output_Name__c,Output_Type__c,Brand_Team_Instance__r.Team_Instance__r.Name,Row__c,Col__c,DM2_Output_Type__c,DM1_Output_Type__c from Grid_Master__c where id=:gid];
		system.debug('----gridmasterlist size is():'+gridmasterlist.size());
		GridListOld=[select id,Name,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c,colvalue__c from Grid_Details__c where Grid_Master__c=:gid order by Rowvalue__c];
		system.debug('----gridlistOLD size is():'+GridListOld.size());
		
		for(Grid_Details__c g : GridListOld){
			RHeaderlist.add(g.Dimension_1_Value__c);
			CHeaderlist.add(g.Dimension_2_Value__c);
			map2d.put(g.Name,g);
			system.debug('----CHeaderlist Set Elemnet:'+CHeaderlist);
		}
		result.addAll(CHeaderlist);
		rowheaderlist.addAll(RHeaderlist);
		comp.griddata=map2d;
		comp.cheaders=result;
		comp.rheaders=rowheaderlist;
		
		List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c ];
        List<String> allgms = new List<String>();

        for(Grid_Master__c g : allGridMasters)
        {
            allgms.add(g.Name);
        }


		String nameMatrix = gridmasterlist[0].Name;
        String tempName = nameMatrix;


        Integer i2 = 1;
		Boolean flag = true;
        while(flag)
        {
            system.debug('+++++++++++ Hey Matrix is '+ tempName);
            if(allgms.contains(tempName))
            {
                tempName = nameMatrix + '_' + String.valueof(i2);
                i2++;
            }
            else
            {
                allgms.add(tempName);
                flag = false;
            }
        }

		gridmasterlist[0].Name = nameMatrix;
		system.debug('----RHeaderlist size is():'+RHeaderlist.size());
		system.debug('----CHeaderlist size is():'+CHeaderlist.size());
		system.debug('----result List size is():'+result);
		system.debug('----Map Value is:::'+map2d);
		system.debug('---COMP.Griddata---:'+comp.griddata);
		system.debug('---COMP.Griddata---:'+comp.griddata);
		system.debug('---COMP.Cheaders---:'+comp.cheaders);
		system.debug('---COMP.Rheaders---:'+comp.rheaders);
		//system.debug('---COMP.headers---:'+comp.headers);
		
		
		VDM1=gridmasterlist[0].Dimension_1_Name__c;
		//
			Viewrowsiz = true;
			View2Dtable= true;	
			VDM2=gridmasterlist[0].Dimension_2_Name__c;
			if(gridmasterlist[0].Col__c != null){
				for(integer i=1;i<=gridmasterlist[0].Col__c;i++){
					cols.add(i);
				}
			}
			if(gridmasterlist[0].Row__c != null){
				for(integer i=1;i<=gridmasterlist[0].Row__c;i++){
					Rows.add(i);
				}
			}
			if(gridmasterlist[0].Grid_Type__c == '2D'){
			ViewShow2d=true;
			}
			
		//}
		/*else{
			View1Dtable =true;
			if(gridmasterlist[0].Col__c != null){
				for(Integer i = 1; i<=gridmasterlist[0].Col__c;i++){
	                    cols.add(i);
	                }
				}
				
			}*/
		
	} 
	public void EditBlock(String mode,String gid){
		system.debug('******Entered Edit Block ********');
		Editblock=true; 
		system.debug('******EDITbloick ********'+Editblock);
		gridmasterlist=new list<Grid_Master__c>();
		gridmasterlist=new list<Grid_Master__c>();
		GridListOld=new list<Grid_Details__c>();
		RHeaderlist = new set<String>();
		CHeaderlist = new set<String>();
		result=new list<String>();
		rowheaderlist = new list<String>();
		map2d = new map<String,Grid_Details__c>();
		map1d = new map<String,Grid_Details__c>();
		VDM1='';
		VDM2='';
		
		gridmasterlist=[SELECT id,Name,Description__c,Brand__r.Name,Dimension_1_Name__c, isGlobal__c, Dimension_2_Name__c,Grid_Type__c,Output_Name__c,Output_Type__c,Brand_Team_Instance__r.Team_Instance__r.Name,Row__c,Col__c,DM2_Output_Type__c,DM1_Output_Type__c from Grid_Master__c where id=:gid];
		system.debug('----gridmasterlist size is():'+gridmasterlist.size());
		GridListOld=[select id,Name,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c,colvalue__c from Grid_Details__c where Grid_Master__c=:gid order by Rowvalue__c];
		system.debug('----gridlistOLD size is():'+GridListOld.size());
		//if(gridmasterlist[0].Grid_Type__c == '2D'){
			for(Grid_Details__c g : GridListOld){
				RHeaderlist.add(g.Dimension_1_Value__c);
				CHeaderlist.add(g.Dimension_2_Value__c);
				map2d.put(g.Name,g);
				system.debug('----CHeaderlist Set Elemnet:'+CHeaderlist);
			}
		/*}
		else{
			for(Grid_Details__c g : GridListOld){
				RHeaderlist.add(g.Dimension_1_Value__c);
				CHeaderlist.add(g.Dimension_2_Value__c);
				map1d.put(g.Name,g);
				system.debug('----CHeaderlist Set Elemnet:'+CHeaderlist);
			}
			
		}*/
		result.addAll(CHeaderlist);
		rowheaderlist.addAll(RHeaderlist);
		comp.griddata=map2d;
		comp.cheaders=result;
		comp.rheaders=rowheaderlist;
		system.debug('----RHeaderlist size is():'+RHeaderlist.size());
		system.debug('----CHeaderlist size is():'+CHeaderlist.size());
		system.debug('----result List size is():'+result);
		system.debug('----Map Value is:::'+map2d);
		system.debug('---COMP.Griddata---:'+comp.griddata);
		system.debug('---COMP.Cheaders---:'+comp.cheaders);
		system.debug('---COMP.Rheaders---:'+comp.rheaders);
		
		
		VDM1=gridmasterlist[0].Dimension_1_Name__c;
		//
			Viewrowsiz = true;
			View2Dtable= true;	
			VDM2=gridmasterlist[0].Dimension_2_Name__c;
			if(gridmasterlist[0].Col__c != null){
				for(integer i=1;i<=gridmasterlist[0].Col__c;i++){
					cols.add(i);
				}
			}
			if(gridmasterlist[0].Row__c != null){
				for(integer i=1;i<=gridmasterlist[0].Row__c;i++){
					Rows.add(i);
				}
			}
			if(gridmasterlist[0].Grid_Type__c == '2D'){
				ViewShow2d=true;
			}
					
		/*else{
			View1Dtable =true;
			if(gridmasterlist[0].Col__c != null){
				for(Integer i = 1; i<=gridmasterlist[0].Col__c;i++){
	                    cols.add(i);
	                }
				}
			
				
			}*/
		
	}

    public void show2D(){
        system.debug('Entered show2D method');
        system.debug('******String.valueof::::'+String.Valueof(gridMaster.Grid_Type__c));
        if(gridMaster.Grid_Type__c == '1D')
            col1 = 1;
        showdefineblock=true;
        /*if(String.Valueof(gridMaster.Grid_Type__c)== '2D'){
            show1=true; 
            twod=true;
           
        }
        else{
            show1=false;
            oned=true;
            twod=false;
        }
        system.debug('*********Show1 Contains:'+show1);
        system.debug('*********showdefineblock :'+showdefineblock);
     */   
    }
    public void createtable(){
        system.debug('***Rows:'+row1);
        system.debug('***columns:'+col1);
        cols = new list<Integer>();
   		rows = new list<Integer>();
        
        //if(oned){
        	 showoneD = true;
        	if(col1!=null){
        		//row1=col1;
            	dm1=String.Valueof(gridMaster.Dimension_1_Name__c);
            	dmlist.add(dm1);
                for(Integer i = 0; i< col1;i++){
                    cols.add(i);
                }
        	}
        	if(row1!=null){
	        	dm2=String.Valueof(gridMaster.Dimension_2_Name__c);
	        	dmlist.add(dm2);
	            for(Integer i = 0; i< row1;i++){
	                rows.add(i);
	            }
		    } 
        //}else if(twod){
        	/*if(gridMaster.Dimension_1_Name__c == ''){
        		 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Dimension1 Name');
            	 ApexPages.addMessage(myMsg);
        	}
        	 else if(gridMaster.Dimension_2_Name__c == ''){
        		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Dimension2 Name');
            	ApexPages.addMessage(myMsg);
        	}
        	 if(col1 == null ){
        		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Column Size');
            	ApexPages.addMessage(myMsg);
        	}
        	 if(row1 == null ){
        		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Row Size');
            	ApexPages.addMessage(myMsg);
        	}
        	if(gridMaster.Dimension_1_Name__c == '' && gridMaster.Dimension_2_Name__c == '' && col1 == null){*/
        	
		  /*      	showtable=true;
		            if(col1!=null){
		            	dm1=String.Valueof(gridMaster.Dimension_1_Name__c);
		            	dmlist.add(dm1);
		                for(Integer i = 0; i< col1;i++){
		                    cols.add(i);
		                }
		            }
		            if(row1!=null){
		            	dm2=String.Valueof(gridMaster.Dimension_2_Name__c);
		            	dmlist.add(dm2);
		                for(Integer i = 0; i< row1;i++){
		                    rows.add(i);
		                }
		            }
		       // }
		 }*/
        system.debug('***Dimensionlist contains:'+dmlist);
    }
    public void createtable2(){
    	
        system.debug('***Rows:'+row1);
        system.debug('***columns:'+col1);
        cols = new list<Integer>();
   		rows = new list<Integer>(); 
        
       
        system.debug('----Mode is:'+comp.Mode);
        
        	 newtbl = true;
        	 View2Dtable =false;
        	 system.debug('----COLSSS:'+gridmasterlist[0].Col__c);
        	 system.debug('-----Rowssss:'+gridmasterlist[0].row__c);
        	if(gridmasterlist[0].Col__c!=null){
        		
            	dm1=String.Valueof(gridmasterlist[0].Dimension_1_Name__c);
            	dmlist.add(dm1);
                for(Integer i = 0; i<col1;i++){
                    cols.add(i);
                }
        	}
        	if(row1!=null){
	        	dm2=String.Valueof(gridmasterlist[0].Dimension_2_Name__c);
	        	dmlist.add(dm2);
	            for(Integer i = 0; i<row1;i++){
	                rows.add(i);
	            }
		    } 
		    comp.Mode='NewM';
		    gridmasterlist[0].Col__c=cols.size();
		    gridmasterlist[0].row__c=rows.size();
		    comp.row=rows;
		    comp.column=cols;
		    system.debug('--COMP:'+comp);
           system.debug('***Dimensionlist contains:'+dmlist);
    }
    
    /*public void save(){
    	
    	list<Grid_Master__c> gm = new list<Grid_Master__c>();
    	Grid_Master__c obj=new Grid_Master__c();
    	obj.Name=gridMaster.Name;
    	obj.Description__c=gridMaster.Description__c;
    	obj.Brand__c = gridmaster.Brand__c;
    	obj.Grid_Type__c = gridMaster.Grid_Type__c;
    	obj.Dimension_1_Name__c = gridMaster.Dimension_1_Name__c;
    	obj.Dimension_2_Name__c = gridMaster.Dimension_2_Name__c;
    	obj.Output_Type__c = gridMaster.Output_Type__c;
    	obj.Dimension_2_Name__c = gridMaster.Dimension_2_Name__c;
    	obj.Output_Name__c = gridMaster.Output_Name__c;
    	//insert obj;
    	gm.add(obj);
    	Database.SaveResult[] sr=Database.insert(gm,false);
    	if(sr[0].isSuccess()){
    		gmid=sr[0].getid();
    	}
    	system.debug('New Grid_Master__c id is:'+gmid);
    }*/
    public PageReference savegriddetail(){
    	string passedParam1 = Apexpages.currentPage().getParameters().get('JSONdata');
    	//JSONdata=pgJSON;//String pgJSON
    	passedParam1=JSONdata;
    	system.debug('JSONdata from Page::'+JSONdata);
    	system.debug('passedParam1 from Page::'+passedParam1);
    	//list<Grid_Details__c>Data=deserialize(passedParam1,List);
    	//list<Sobject>response=JSON.deserializeUntyped(passedParam1);
    	//system.debug(':::data list contains:'+Data.size());
    	/* code to insert Grid MAster Data*/

    	List<Grid_Master__c> testGm = [select id from Grid_Master__c where Name = :gridMaster.Name];

    	list<Grid_Master__c> gm = new list<Grid_Master__c>();
    	Grid_Master__c obj=new Grid_Master__c();
	    	obj.Name=gridMaster.Name;
	    	obj.Description__c=gridMaster.Description__c;
	    	obj.Brand__c = gridmaster.Brand__c;
	    	obj.Grid_Type__c = gridMaster.Grid_Type__c;
	    	obj.Dimension_1_Name__c = gridMaster.Dimension_1_Name__c;
	    	obj.DM1_Output_Type__c = gridMaster.DM1_Output_Type__c;
	    	obj.Dimension_2_Name__c = gridMaster.Dimension_2_Name__c;
	    	obj.DM2_Output_Type__c = gridMaster.DM2_Output_Type__c;
	    	obj.Output_Type__c = gridMaster.Output_Type__c;
	    	obj.isGlobal__c = gridMaster.isGlobal__c;
	    	obj.Output_Name__c = gridMaster.Output_Name__c;
	    	obj.Row__c=row1;
	    	obj.Col__c=col1;
	    	obj.Country__c = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
	    	//insert obj;
    	gm.add(obj);
    	Database.SaveResult[] sr=Database.insert(gm,false);
    	if(sr[0].isSuccess()){
    		gmid=sr[0].getid();
    		unique=true;
    	}
    	else{
	    		/*
	                Use Case - 
	                Action - 
	                Developer -
	                Developer Employee ID -
	                Date -
	                JIRA Bug Code (if Applicable)-
				*/
    			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Fill All Data and Click on Save or Else Click Cancel');
            	ApexPages.addMessage(myMsg);
		    		unique=false;
		    		gmid='';

    	}
    	system.debug('New Grid_Master__c id is:'+gmid);
    	
    	/*Code end for inserting grid master insert data*/
    	if(unique){
	    	list<Map<String,String>> gg = (list<Map<String,String>>)JSON.deserialize(passedParam1, list<Map<String,String>>.Class);
	    	system.debug('DATA IS:'+gg);
	    	list<Grid_Details__c>insertgm=new list<Grid_Details__c>();

	    	for(Map<String,String> g : gg){
	    		Grid_Details__c gd = new Grid_Details__c();
	    		gd.Dimension_1_Value__c = g.get('Dimension_1_Value__c');

	    		system.debug('---GridType::'+gridMaster.Grid_Type__c);
	    		//if(gridMaster.Grid_Type__c!='1D')
	    		gd.Dimension_2_Value__c = g.get('Dimension_2_Value__c');
	    		gd.Output_Value__c = g.get('Output_Value__c');
	    		gd.Grid_Master__c = gmid;
	    		gd.Name = g.get('Name');
	    		insertgm.add(gd);
	    		system.debug('NEW gridDetails Data is::'+gd);
	    	}
	    	system.debug('INSert gridlist size is:'+insertgm.size());
	    	try{
	    		Database.SaveResult[] griddetail=Database.insert(insertgm,false);
	    	}
	    	Catch(Exception e){
	    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Fill All Data and Click on Save or Else Click Cancel');
            	ApexPages.addMessage(myMsg);
	    		System.debug('Exception Caught::'+e);
	    	}
    	}
    	string returnurl=Apexpages.currentPage().getParameters().get('retUrl');
        system.debug('--returnurl---' + returnurl);
        string rid=Apexpages.currentPage().getParameters().get('rid');
        system.debug('--rid---' + rid);
    	PageReference pr;
    	if(returnurl==null || returnurl==''){
    		pr= new PageReference('/apex/List_View');
    		pr.setRedirect(true);
    	}
    	else{
            String encoded = returnurl+'&rid='+rid;
    		pr= new PageReference(encoded);
    		pr.setRedirect(true);
    	}
    	system.debug('---pr contains:'+pr);
    	return pr;
    }
    public PageReference cancelfun(){
    	System.debug('*******Cancel func Called');
    /*	try{
    	PageReference pr= new PageReference('/apex/List_View');///apex/List_View?sfdc.tabName=01r0Y000000Zvh3
    	//pr.setRedirect(true);
    	return pr;
    	}
    	catch(Exception e){
    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            	 ApexPages.addMessage(myMsg);
            	 return null;
    		    	}
    */
    PageReference pr= new PageReference('/apex/List_View?sfdc.tabName=01r0Y000000Zvh3');
    pr.setRedirect(true);
    return pr;
    	
    }
    public PageReference cancelfunc2(){
        system.debug('----------inside cancelfunc2');
        return null;
    }
    public PageReference updategrid(){
    	system.debug('*** entered updategrid method');
    	system.debug('JSONdata from Page::'+JSONdata);
    	/*updatelist = new list<Grid_master__c>(); 
    	Grid_Master__c gridm = new Grid_Master__c();
    	if(gridid!=null || gridid!='')
    		gridm.id=gridid;
    	system.debug('----in updategrid method:::'+gridmasterlist[0]);	
    	gridm.Name=gridmasterlist[0].name;
    	gridm.Description__c=gridmasterlist[0].Description__c;
    	gridm.Brand__c = gridmasterlist[0].Brand__c;
    	gridm.Grid_Type__c = gridmasterlist[0].Grid_Type__c;
    	gridm.Dimension_1_Name__c = gridmasterlist[0].Dimension_1_Name__c;
    	gridm.Dimension_2_Name__c = gridmasterlist[0].Dimension_2_Name__c;
    	//,DM2_Output_Type__c,DM1_Output_Type__c
    	gridm.DM1_Output_Type__c = gridmasterlist[0].DM1_Output_Type__c;
    	gridm.DM2_Output_Type__c = gridmasterlist[0].DM2_Output_Type__c;
    	
    	gridm.Output_Type__c = gridmasterlist[0].Output_Type__c;
    	gridm.Output_Name__c = gridmasterlist[0].Output_Name__c;
    	gridm.Row__c=gridmasterlist[0].Row__c;
    	gridm.Col__c=gridmasterlist[0].Col__c;
    	System.debug('Grid MAster is::'+gridm);
    	updatelist.add(gridm);
    	try{
    		DataBase.SaveResult[] sr = database.update(updatelist,false);
    	}
    	Catch(Exception e){
    		system.debug('---Exception Caught::'+e);
    	}*/
    	
    	/* Code for updating Grid Details_data*/
    	list<Map<String,String>> gg = (list<Map<String,String>>)JSON.deserialize(JSONdata, list<Map<String,String>>.Class);
    	system.debug('DATA for updating:'+gg);
    	list<Grid_Details__c>updategriddetails=new list<Grid_Details__c>();
    	for(Map<String,String> g : gg){
    		Grid_Details__c gd = new Grid_Details__c();
    		gd.Dimension_1_Value__c = g.get('Dimension_1_Value__c');

    		//system.debug('---GridType::'+gridMaster.Grid_Type__c);
    		//if(gridMaster.Grid_Type__c!='1D')
    		gd.Dimension_2_Value__c = g.get('Dimension_2_Value__c');
    		gd.Output_Value__c = g.get('Output_Value__c');
    		gd.id = g.get('Id');
    		gd.Name = g.get('Name');
    		updategriddetails.add(gd);
    		system.debug('NEW gridDetails Data is::'+gd);
    	}
    	system.debug('updategriddetails size is:'+updategriddetails.size());
    	try{
    		Database.SaveResult[] griddetail=Database.update(updategriddetails,false);
    		system.debug('------SaveResult Size is:'+griddetail.size());
    	}
    	Catch(Exception e){
    		System.debug('Exception Caught::'+e);
    	}
    	
    	//PageReference p = new pageReference('/apex/')
    	PageReference pr= new PageReference('/apex/List_View');
    	pr.setRedirect(true);
    	return pr;
     }
     
     public PageReference Clonegriddetails(){
    	system.debug('*** entered Clone method');
    	system.debug('JSONdata from Page::'+JSONdata);
    	//updatelist = new list<Grid_master__c>(); 
    	list<Grid_Master__c>insetlist = new list<Grid_master__c>();
    	Grid_Master__c gridm = new Grid_Master__c();
    		
    	gridm.Name=gridmasterlist[0].name;
    	gridm.Description__c=gridmasterlist[0].Description__c;
    	gridm.Brand__c = gridmasterlist[0].Brand__c;
    	gridm.Grid_Type__c = gridmasterlist[0].Grid_Type__c;
    	gridm.Dimension_1_Name__c = gridmasterlist[0].Dimension_1_Name__c;
    	gridm.Dimension_2_Name__c = gridmasterlist[0].Dimension_2_Name__c;
    	gridm.DM1_Output_Type__c = gridmasterlist[0].DM1_Output_Type__c;
    	gridm.DM2_Output_Type__c = gridmasterlist[0].DM2_Output_Type__c;
    	gridm.Output_Type__c = gridmasterlist[0].Output_Type__c;
    	gridm.Output_Name__c = gridmasterlist[0].Output_Name__c;
    	gridm.Row__c=gridmasterlist[0].Row__c;
    	gridm.Col__c=gridmasterlist[0].Col__c;
    	gridm.Country__c = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
    	gridm.isGlobal__c = gridmasterlist[0].isGlobal__c;
    	
    	System.debug('Grid MAster is::'+gridm);
    	insetlist.add(gridm);
    	try{
    		DataBase.SaveResult[] sr = database.insert(insetlist,false);
    		if(sr[0].isSuccess()){
    			gmid=sr[0].getid();
    			unique=true;
	    	}
	    	else{
	    		gmid='';
	    		unique=false;
	    		system.debug('---GridMaster is not inserted---');
	    		system.debug('---GMID::---'+gmid);
	    		return null;
	    	}
    	}
    	
    	Catch(Exception e){
    		system.debug('---Exception Caught::'+e);
    	}
    	system.debug('--Unique value is:'+unique);
    	
    	/* Code for inserting Grid Details_data*/
    	if(unique){
    		system.debug('--Entered if block GMID not equal to Null::'+gmid);
    		system.debug('--Unique value is:'+unique);

	    	list<Map<String,String>> gg = (list<Map<String,String>>)JSON.deserialize(JSONdata, list<Map<String,String>>.Class);

	    	system.debug('DATA for inserting:'+gg);
	    	list<Grid_Details__c>insertgriddetails=new list<Grid_Details__c>();
	    	for(Map<String,String> g : gg){
	    		Grid_Details__c gd = new Grid_Details__c();
	    		gd.Dimension_1_Value__c = g.get('Dimension_1_Value__c');

	    		system.debug('---GridType::'+gridMaster.Grid_Type__c);
	    		//if(gridMaster.Grid_Type__c!='1D')
	    		gd.Dimension_2_Value__c = g.get('Dimension_2_Value__c');
	    		gd.Output_Value__c = g.get('Output_Value__c');
	    		gd.Grid_Master__c = gmid;
	    		gd.Name = g.get('Name');
	    		gd.id = g.get('id');
	    		insertgriddetails.add(gd);
	    		system.debug('NEW gridDetails Data is::'+gd);
	    	}
	    	system.debug('updategriddetails size is:'+insertgriddetails.size());
	    	try{
	    		if(gmid!=null && gmid !=''){
		    		Database.SaveResult[] griddetail=Database.insert(insertgriddetails,false);
		    		system.debug('------SaveResult Size is:'+griddetail.size());
	    		}
	    	}
	    	Catch(Exception e){
	    		System.debug('Exception Caught::'+e);
	    	}
	    	
	    	//PageReference p = new pageReference('/apex/')
	    	}
	    	PageReference pr= new PageReference('/apex/List_View');
	    	pr.setRedirect(true);
	    	return pr;
	    
    }
    public void comp_grid(){
    	System.debug('--Function called from component-----');
    	system.debug('JSONdata from Page::'+myJSON);
    }
}