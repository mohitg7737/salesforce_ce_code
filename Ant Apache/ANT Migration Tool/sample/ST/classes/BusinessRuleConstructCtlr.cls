public with sharing class BusinessRuleConstructCtlr extends BusinessRule implements IBusinessRule{
    public list<SelectOption> businessUnits {get;set;}
    public list<SelectOption> lines {get;set;}
    public list<SelectOption> brands {get;set;}
    public list<SelectOption> cycles {get;set;}
    public list<Measure_Master__c>Bulist {get;set;}
    public set<String>Buset {get;set;}

   // public String countryID {get;set;}
   // Map<string,string> successErrorMap;

    public BusinessRuleConstructCtlr(){
     /*   countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
        }
*/
        Bulist = new list<Measure_Master__c>();
        Buset= new set<String>();
        fillBulist();

        init();
        fillBusinessUnitOptions();
       fillLineOptions(); 
        //fillBrandOptions();
        fillCycleOptions();
    }

    public void fillBulist(){
        Bulist= [select id,Name from Measure_Master__c];
        for(Measure_Master__c mm : Bulist){
            Buset.add(mm.Name);
        }
    }




    public void fillCycleOptions(){
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        for(Cycle__c cycle: [SELECT Id, Name FROM Cycle__c]){
            cycles.add(new SelectOption(cycle.Id, cycle.Name));
        }
    }

    public void fillBusinessUnitOptions(){
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None', '--None--'));
        system.debug('============ hEY COUNTRY ID IS '+countryID);
        for(AxtriaSalesIQTM__Team_Instance__c bu: [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c = :countryID]){
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }
        
    }

    public void fillLineOptions(){
        /*lines = new list<SelectOption>();
        lines.add(new SelectOption('None', '--None--'));
        if(String.isNotBlank(selectedBuisnessUnit)){
            for(Line__c line: [SELECT Id, Name FROM Line__c WHERE Team_Instance__c =:selectedBuisnessUnit]){
                lines.add(new SelectOption(line.Id, line.Name));
            }
        }*/

        fillBrandOptions();
    }

    public void fillBrandOptions(){
        brands = new list<SelectOption>();
        brands.add(new SelectOption('None', '--None--'));
        system.debug('Hey Selcted Business unit is '+ selectedBuisnessUnit);
        for(Product_Catalog__c product: [SELECT Id, Name FROM Product_Catalog__c where Team_Instance__c =:selectedBuisnessUnit]){
            brands.add(new SelectOption(product.Id, product.Name));
        }
    }

    public String msg;
    public void save(){
        if(isRuleValid()){
            updateRule('Basic Information', 'Basic Information');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Rule Saved Successfully');
            ApexPages.addMessage(myMsg);
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
            ApexPages.addMessage(myMsg);
        }
    }

    public PageReference saveAndNext(){
        if(isRuleValid()){
            updateRule('Profiling Parameter', 'Basic Information');
            return nextPage('SelectRuleParameters');
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
            ApexPages.addMessage(myMsg);
            return null;
        }
    }

    private boolean isRuleValid(){
        msg = '';
        if(String.isBlank(ruleObject.Name)){
            msg += 'Rule name cannot be empty <br/>';
        }

        if(String.isBlank(selectedCycle) || selectedCycle == 'None'){
            msg += 'Select a Cycle <br/>';
        }

        if(String.isBlank(selectedBuisnessUnit) || selectedBuisnessUnit == 'None'){
            msg += 'Select a Business Unit <br/>';
        }

       /* if(String.isBlank(selectedLine) || selectedLine == 'None'){
            msg += 'Select a Line <br/>';
        }
        */

        if(String.isBlank(selectedBrand) || selectedBrand == 'None'){
            msg += 'Select a Brand <br/>';
        }
        if(Buset.contains(ruleObject.Name) && mode=='new'){
            msg += 'Rule Name already exists.Kindly Give unique name.';
        }


        if(String.isNotBlank(msg)){
            return false;
        }

        return true;
    }
}