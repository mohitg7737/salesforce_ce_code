global class BatchCallPlanDownload implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public String positionId;
    public map<String, String> parameterAPIName;
    public list<String> parameters;
    public AxtriaSalesIQTM__Position__c position;
    global String results;
    public String localeSeperator;
    global BatchCallPlanDownload(String positionId, String seperator){
        localeSeperator = seperator;
        results = '';
        position = [SELECT Id, AxtriaSalesIQTM__Client_Territory_Code__c, Line__c,AxtriaSalesIQTM__Team_Instance__c FROM AxtriaSalesIQTM__Position__c WHERE Id =: positionId];
        list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> teamInstances = [SELECT Id, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c FROM AxtriaSalesIQTM__Team_Instance_Object_Attribute__c WHERE AxtriaSalesIQTM__Team_Instance__c =: position.AxtriaSalesIQTM__Team_Instance__c AND Brand_Team_Instance__c != null AND AxtriaSalesIQTM__isEnabled__c = true AND AxtriaSalesIQTM__isRequired__c = true AND AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' ORDER BY AxtriaSalesIQTM__Display_Column_Order__c ASC];
        parameters = new list<String>();
        parameterAPIName = new map<String, String>();
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c ta: teamInstances){
            parameters.add(ta.AxtriaSalesIQTM__Attribute_Display_Name__c);
            parameterAPIName.put(ta.AxtriaSalesIQTM__Attribute_Display_Name__c, ta.AxtriaSalesIQTM__Attribute_API_Name__c);
        }
        
        results  = createCSVHeader(parameters);
        results += '\n';
        String paramApi = String.join(parameterAPIName.values(), ',');

        
        query =  'SELECT Id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Change_Status__c, AxtriaSalesIQTM__Account__r.First_Last_Name__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.Primary_Speciality_Name__c, AxtriaSalesIQTM__Account__r.BillingCity, AxtriaSalesIQTM__Account__r.Micro_Brick__c, ';
        query += 'AxtriaSalesIQTM__Account__r.Parent_Name__c, P1__c, Segment__c, Proposed_TCF__c, Final_TCF__c, Accessibility_Range__c, AxtriaSalesIQTM__Account__r.Profile_Consent__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.LastFirstName__c, ';
        query += 'share__c, AllCall__c, '+paramApi+'AxtriaSalesIQTM__Picklist3_Updated__c  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Position__c = \''+positionId+'\' and AxtriaSalesIQTM__isIncludedCallPlan__c = true';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('============Query is::::'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        map<String, sObject> accountCallPlan = new map<String, sObject>();
        list<String> sharedAccounts = new list<String>();
        for(sObject callplan : scope){
            accountCallPlan.put((String)callplan.get('AxtriaSalesIQTM__Account__c'), callplan);
            if((Boolean)callplan.get('share__c') == true){
                sharedAccounts.add((String)callplan.get('AxtriaSalesIQTM__Account__c'));
            }
        }
        
        if(sharedAccounts != null && sharedAccounts.size()>0){
            list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedCallPlans = [SELECT Id, AxtriaSalesIQTM__Account__c, Final_TCF__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Account__c IN:sharedAccounts];
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c cp: sharedCallPlans){
                sObject callplan = accountCallPlan.get(cp.AxtriaSalesIQTM__Account__c);
                Decimal totalCalls = (Decimal)callplan.get('AllCall__c') != null ? (Decimal)callplan.get('AllCall__c') : 0;
                totalCalls += cp.Final_TCF__c != null ? cp.Final_TCF__c : 0;
                callplan.put('AllCall__c', totalCalls);
                accountCallPlan.put(cp.AxtriaSalesIQTM__Account__c, callplan);
            }
        }
        
        for(sObject callplan: accountCallPlan.values()){
            results += createCSVRow(callplan);
            results += '\n';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        datetime myDateTime = datetime.now();
        String fileTitle = position.AxtriaSalesIQTM__Client_Territory_Code__c + '_' + myDateTime.format();
        ContentVersion file = new ContentVersion(title = fileTitle + '.csv', versionData = Blob.valueOf( results ), pathOnClient = '/' + fileTitle + '.csv');
        insert file;
        
        String subject = 'Download Call Plan : ' + position.AxtriaSalesIQTM__Client_Territory_Code__c;
        String body = system.label.Pacp_Hi +' \n \n '+ System.Label.emailtext+'\n \n '+system.label.Territory_ID+' : '+ position.AxtriaSalesIQTM__Client_Territory_Code__c +'\n'+ system.Label.PACP_Name+' : '+UserInfo.getUserName()+' \n '+system.label.btn_Download +'Time : '+myDateTime.format()+'\n\n '+system.label.PACP_Thanks;
        //String body = 'In allegato il Call Plan di cui hai richiesto l'invio.';
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject( subject );
        email.setToAddresses(new list<String>{ UserInfo.getUserEmail() }); //
        email.setPlainTextBody( body );
        List<Messaging.EmailFileAttachment> attachments = ContentDocumentAsAttachement(new Id[]{file.Id});
        email.setFileAttachments(attachments);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
    
    public static List<Messaging.EmailFileAttachment> ContentDocumentAsAttachement(Id[] contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>{};
        List<ContentVersion> documents                  = new List<ContentVersion>{};
    
        documents.addAll([
          SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
          FROM ContentVersion
          WHERE Id IN :contentDocumentIds
        ]);
    
        for (ContentVersion document: documents) {
          Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
    
          attachment.setBody(document.VersionData);
          attachment.setFileName(document.Title);
    
          attachments.add(attachment);
        }
    
        return attachments;
    
    }
    
    private String createCSVRow(sObject callplan){
        String row = '';
        row += (String)callplan.get('AxtriaSalesIQTM__Change_Status__c') + localeSeperator ;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('First_Last_Name__c') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('AxtriaSalesIQTM__External_Account_Number__c') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Primary_Speciality_Name__c') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('BillingCity') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Micro_Brick__c') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Parent_Name__c') + localeSeperator;
        row += (String)callplan.get('P1__c') + localeSeperator;
        row += (String)callplan.get('Segment__c') + localeSeperator;
        row += Integer.valueOf((Decimal)callplan.get('Proposed_TCF__c') )+ localeSeperator;
        row += Integer.valueOf((Decimal)callplan.get('Final_TCF__c')) + localeSeperator;
        row += (String)callplan.get('Accessibility_Range__c') + localeSeperator;
        row += (String)callplan.getsObject('AxtriaSalesIQTM__Account__r').get('Profile_Consent__c') + localeSeperator;
        if(callplan.getsObject('AxtriaSalesIQTM__Position__r').getsObject('AxtriaSalesIQTM__Employee__r').get('LastFirstName__c') !=null)
        row += ((String)callplan.getsObject('AxtriaSalesIQTM__Position__r').getsObject('AxtriaSalesIQTM__Employee__r').get('LastFirstName__c')).replace(',' , ' ') + localeSeperator;
        else
        row+= 'VACANT';
        row += (Boolean)callplan.get('share__c') + localeSeperator;
        row += (Integer.valueOf((Decimal)callplan.get('AllCall__c')) != null && Integer.valueOf((Decimal)callplan.get('AllCall__c')) > 0 ? Integer.valueOf((Decimal)callplan.get('AllCall__c')) : Integer.valueOf((Decimal)callplan.get('Final_TCF__c'))) + localeSeperator;
        row += (String)callplan.get('AxtriaSalesIQTM__Picklist3_Updated__c') + localeSeperator;
        for(String parameter: parameters){
            row += ' ' + (String.isNotBlank((String)callplan.get(parameterAPIName.get(parameter))) ? (String)callplan.get(parameterAPIName.get(parameter)) : '-') + localeSeperator;
        }
        return row;
    }
    
    private string createCSVHeader(list<String> parameters){
        String headers = '';
        headers += System.Label.Pacp_Status + localeSeperator ;
        headers += system.Label.Pacp_AccountName + localeSeperator ;
        headers += system.Label.Pacp_AccountNumber + localeSeperator ;
        headers += system.Label.Pacp_Speciality + localeSeperator ;
        headers += system.Label.Pacp_City + localeSeperator ;
        headers += system.Label.Pacp_Mb + localeSeperator ;
        headers += system.Label.Pacp_Parent + localeSeperator ;
        headers += system.Label.Pacp_Product + localeSeperator ;
        headers += system.Label.Pacp_Segment + localeSeperator ;
        headers += system.Label.Pacp_ProposedTCF + localeSeperator ;
        headers += system.Label.Pacp_TCFFinal  + localeSeperator ;
        headers += system.Label.Pacp_Accessibility  + localeSeperator ;
        headers += system.Label.Pacp_Consent  + localeSeperator ;
        headers += system.label.Pacp_RepName  + localeSeperator ;
        headers += system.Label.Pacp_SharedFlag  + localeSeperator ;
        headers += system.Label.Pacp_TCFTOTAL  + localeSeperator ;
        headers += system.Label.Pacp_ReasonCode;
        
        
        for(String parameter: parameters){
            headers += localeSeperator ;
            headers += parameter;
        }
        return headers;
    }
}