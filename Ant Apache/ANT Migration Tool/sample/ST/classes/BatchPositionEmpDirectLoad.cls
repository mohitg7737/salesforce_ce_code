global class BatchPositionEmpDirectLoad implements Database.Batchable<sObject>, Database.Stateful  {
     
    global string query='';
    global string teamID = '';
    global string teamInstanceG = '';
    global string teamG;
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    
    global set<AxtriaSalesIQTM__Position_employee__c> setPEInsert = new set<AxtriaSalesIQTM__Position_employee__c>();
    global set<AxtriaSalesIQTM__Position_employee__c> setPEUpdate = new set<AxtriaSalesIQTM__Position_employee__c>();
    global list<AxtriaSalesIQTM__Position_employee__c> lsPEInsert = new list<AxtriaSalesIQTM__Position_employee__c>();
    global list<AxtriaSalesIQTM__Position_employee__c> lsPEUpdate = new list<AxtriaSalesIQTM__Position_employee__c>();
    global list<temp_Position_Employee__c> lsPEToBeUpdated = new list<temp_Position_Employee__c>();
    
    
    global BatchPositionEmpDirectLoad(string team, string teamInstance){
        
        list<AxtriaSalesIQTM__Team__c> lsTeam = [select id,name from AxtriaSalesIQTM__Team__c where id =:team];
        list<AxtriaSalesIQTM__Team_Instance__c> lsTeamIns = [select id,name from AxtriaSalesIQTM__Team_Instance__c where id =:teamInstance];
        if(lsTeam.size()!=0){
            teamG = lsTeam[0].name;
        }
        if(lsTeamIns.size()!=0){
            teamInstanceG = lsTeamIns[0].name;
        }
        if(teamInstanceG !='' && teamG !=''){
            query='SELECT Id,Assignment_Type__c,Effective_End_Date__c,Effective_Start_Date__c,Employee_PRID__c,Position_Code__c,Team_Instance__c,Team__c,Status__c  FROM temp_Position_Employee__c where Status__c = \'New\' and Team__c =:teamG and Team_Instance__c=:teamInstanceG';
        }
    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Position_Employee__c> posEmpList){
        
        set<String> setEmp = new set<String>();
        set<String> setPos = new set<String>();
        set<string> setTeamInstance = new set<string>();
        //Changed by Ayushi on 23 July 2018
        set<String> setEmpPRID = new set<String>();
        //<team Instance Name, list<Pos Emp>
        map<String,list<AxtriaSalesIQTM__Position_employee__c>> mapPosEmp = new map<String,list<AxtriaSalesIQTM__Position_employee__c>>();
        map<string,map<string,string>> mapTI2Code2Sfid = new map<string,map<string,string>>();
        map<string,string> mapEmpPRID2SFID = new map<string,string>();
        //Changed by Ayushi on 23 July 2018
        map<String,set<String>> mapTI2setPosCode = new map<String,set<String>>(); 
        //map<string,string> mapPosCode2SFID = new map<string,string>();
        
        list<AxtriaSalesIQTM__Position_employee__c> lsPE = new list<AxtriaSalesIQTM__Position_employee__c>();
        
        for(temp_Position_Employee__c rec:posEmpList){
            setEmp.add(rec.Employee_PRID__c);
            setPos.add(rec.Position_Code__c);
            setTeamInstance.add(rec.Team_Instance__c);
            
        }
        //query all position employee records for the positions and employees mentioned in temp object 
        string queryPosEmp = 'select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Employee__c, Team_Instance_Name__c,AxtriaSalesIQTM__Employee__r.Employee_PRID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,';
               queryPosEmp = queryPosEmp + 'AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,';
               queryPosEmp = queryPosEmp + 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_employee__c ';
               queryPosEmp = queryPosEmp + 'where (AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in: setPos ';
               queryPosEmp = queryPosEmp + 'or AxtriaSalesIQTM__Employee__r.Employee_PRID__c in: setEmp) and Team_Instance_Name__c =:setTeamInstance and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'';
        
        lsPE = Database.query(queryPosEmp);
        
        //Query Employee in temp object
        for(AxtriaSalesIQTM__Employee__c em:[select id,Employee_PRID__c from AxtriaSalesIQTM__Employee__c where Employee_PRID__c IN:setEmp]){
            mapEmpPRID2SFID.put(em.Employee_PRID__c,em.id);
            //Changed by Ayushi on 23 July 2018
            setEmpPRID.add(em.Employee_PRID__c);
        }
        //Query Position from temp object and put it in the team instance map
        for(AxtriaSalesIQTM__Position__c po:[select id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c IN:setPos and  AxtriaSalesIQTM__Team_Instance__r.Name IN:setTeamInstance]){
            
            //Changed by Ayushi on 23 July 2018
            if(!mapTI2setPosCode.containsKey(po.AxtriaSalesIQTM__Team_Instance__r.Name)){
                set<string> setPosCode2 = new set<String>(); 
                setPosCode2.add(po.AxtriaSalesIQTM__Client_Position_Code__c);
                mapTI2setPosCode.put(po.AxtriaSalesIQTM__Team_Instance__r.Name,setPosCode2);
            }else{
                mapTI2setPosCode.get(po.AxtriaSalesIQTM__Team_Instance__r.Name).add(po.AxtriaSalesIQTM__Client_Position_Code__c);
            }

            if(!mapTI2Code2Sfid.containsKey(po.AxtriaSalesIQTM__Team_Instance__r.Name)){
                map<string,string> mapPosCode2Id = new map<string,string>();
                mapPosCode2Id.put(po.AxtriaSalesIQTM__Client_Position_Code__c,po.id);
                mapTI2Code2Sfid.put(po.AxtriaSalesIQTM__Team_Instance__r.Name,mapPosCode2Id);
            }else{
                mapTI2Code2Sfid.get(po.AxtriaSalesIQTM__Team_Instance__r.Name).put(po.AxtriaSalesIQTM__Client_Position_Code__c,po.id);
            }
        }
        
        system.debug('lsPE++'+lsPE);
        //Add position and employee from exisiting position employee into the maps
        for(AxtriaSalesIQTM__Position_employee__c existingPE:lsPE){
            
            if(!mapPosEmp.containsKey(existingPE.Team_Instance_Name__c)){
                list<AxtriaSalesIQTM__Position_employee__c> lstempPE = new list<AxtriaSalesIQTM__Position_employee__c>();
                lstempPE.add(existingPE);
                mapPosEmp.put(existingPE.Team_Instance_Name__c,lstempPE);
            }else{
                mapPosEmp.get(existingPE.Team_Instance_Name__c).add(existingPE);
            }
            //create a map of position code to position id 
            if(!mapTI2Code2Sfid.containsKey(existingPE.Team_Instance_Name__c)){
                map<string,string> mapPosCode2Id = new map<string,string>();
                mapPosCode2Id.put(existingPE.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,existingPE.AxtriaSalesIQTM__Position__c);
                mapTI2Code2Sfid.put(existingPE.Team_Instance_Name__c,mapPosCode2Id);
            }else{
                mapTI2Code2Sfid.get(existingPE.Team_Instance_Name__c).put(existingPE.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,existingPE.AxtriaSalesIQTM__Position__c);
            }
            //add the employees in the position employee in the map created for employees
            mapEmpPRID2SFID.put(existingPE.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,existingPE.AxtriaSalesIQTM__Employee__c);
            /*if(!mapTI2Code2Sfid.containsKey(existingPE.Team_Instance_Name__c)){
                map<string,string> mapPosCode2Id = new map<string,string>();
                mapPosCode2Id.put(existingPE.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,existingPE.AxtriaSalesIQTM__Employee__c);
                mapTI2Code2Sfid.put(existingPE.Team_Instance_Name__c,mapPosCode2Id);
            }else{
                mapTI2Code2Sfid.get(existingPE.Team_Instance_Name__c).put(existingPE.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,existingPE.AxtriaSalesIQTM__Employee__c);
            }*/
        }
        //Add the position and employee mentioned in the temp_Position_Employee__c object into the maps
        
        
        for(temp_Position_Employee__c tempPE: posEmpList){
            AxtriaSalesIQTM__Position_employee__c positionEmp = new AxtriaSalesIQTM__Position_employee__c();
            
            if(mapPosEmp.containsKey(tempPE.Team_Instance__c)
            && mapPosEmp.get(tempPE.Team_Instance__c).size()!=0){
                //Secondary assignments
                if(tempPE.Assignment_Type__c == 'Secondary'){
                    system.debug('inside Secondary Assignemnts++'+tempPE);
                    Date startDate = null;
                    Date endDate = null;
                    string posId = '';
                    string empId = '';
                    Boolean isInsert = true;
                    //check if the existing pos emp Secondary combination exists, if yes then end date that record and insert a new record.
                    for(AxtriaSalesIQTM__Position_employee__c exisPe: mapPosEmp.get(tempPE.Team_Instance__c)){
                        system.debug('inside for exisPe++'+exisPe);
                        system.debug('exisPe++'+exisPe);
                        system.debug('tempPE++'+tempPE);
                        if(tempPE.Position_Code__c == exisPe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c
                        && tempPE.Employee_PRID__c == exisPe.AxtriaSalesIQTM__Employee__r.Employee_PRID__c 
                        && exisPe.AxtriaSalesIQTM__Assignment_Type__c == 'Secondary'){
                            system.debug('inside ifs start++');
                    
                            //End Date previous assignment
                            if(exisPe.AxtriaSalesIQTM__Effective_Start_Date__c < tempPE.Effective_Start_Date__c
                            && exisPe.AxtriaSalesIQTM__Effective_End_Date__c > tempPE.Effective_Start_Date__c){
                                system.debug('inside first if');
                                //end date the previous found and create a new one.
                                AxtriaSalesIQTM__Position_employee__c updPE = new AxtriaSalesIQTM__Position_employee__c(id=exisPe.id);
                                updPE.AxtriaSalesIQTM__Effective_End_date__c = tempPE.Effective_Start_Date__c.adddays(-1);
                                setPEUpdate.add(updPE);
                                
                                startDate = exisPe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_Start_date__c>tempPE.Effective_Start_date__c?exisPe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_Start_date__c:tempPE.Effective_Start_date__c;
                                endDate   = exisPe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_date__c>tempPE.Effective_End_date__c?tempPE.Effective_End_date__c:exisPe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_date__c;
                                posId     = exisPe.AxtriaSalesIQTM__Position__c;
                                empId     = exisPe.AxtriaSalesIQTM__Employee__c;
                                isInsert  = true;
                            }else if(exisPe.AxtriaSalesIQTM__Effective_Start_Date__c == tempPE.Effective_Start_Date__c
                            && exisPe.AxtriaSalesIQTM__Effective_End_Date__c == tempPE.Effective_Start_Date__c){
                                //do not create the same record again
                                system.debug('inside second if');
                                
                                isInsert = false;
                            }else if(exisPe.AxtriaSalesIQTM__Effective_Start_Date__c >= tempPE.Effective_Start_Date__c){
                                //Rejected the record bacause of a conflict
                                system.debug('inside third if');
                                
                                //tempPE.status__c = 'Rejected';
                                isInsert = false;
                                //lsPEToBeUpdated.add(tempPE);
                            }
                        }else{
                            system.debug('mapTI2Code2Sfid++'+mapTI2Code2Sfid);
                            if(mapTI2Code2Sfid.size()!=0 && mapTI2Code2Sfid.get(tempPE.Team_Instance__c)!=null
                                && mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c)!=null){
                                posId = mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c);
                            }
                            if(mapEmpPRID2SFID.size()!=0 && mapEmpPRID2SFID.get(tempPE.Employee_PRID__c)!=null){
                                empId = mapEmpPRID2SFID.get(tempPE.Employee_PRID__c);
                            }
                            isInsert  = true;
                            system.debug('else++posId+empId'+posId+':'+empId+':'+isInsert);
                            
                        }
                        //break from forloop if we get isInsert = false even once
                        if(!isInsert){
                            break;
                        }
                    }
                    //Add the new Assignment
          system.debug('++++ isInsert at add new assignment' + isInsert);
          system.debug('++++ posId at add new assignment' + posId);
          system.debug('++++ empId at add new assignment' + empId);
                    if(isInsert && posId!='' && empId!=''){
                        AxtriaSalesIQTM__Position_employee__c newPE = new AxtriaSalesIQTM__Position_employee__c();
                        newPE.AxtriaSalesIQTM__Position__c = posId;
                        newPE.AxtriaSalesIQTM__Employee__c = empId;
                        newPE.AxtriaSalesIQTM__Effective_Start_date__c = tempPE.Effective_Start_Date__c;
                        newPE.AxtriaSalesIQTM__Effective_End_date__c = tempPE.Effective_End_Date__c;
                        newPE.AxtriaSalesIQTM__Assignment_Type__c = tempPE.Assignment_Type__c;
                        tempPE.status__c = 'Processed';
                        setPEInsert.add(newPE);
                        lsPEToBeUpdated.add(tempPE);
                    }else if(posId==''){
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'Position code does not exist in SalesIQ';
                        lsPEToBeUpdated.add(tempPE);
            
                   }else if(empId == ''){
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'Employee PRID does not exist in SalesIQ';
                        lsPEToBeUpdated.add(tempPE);
                   }else{
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'There is a conflict in assignments';
                        lsPEToBeUpdated.add(tempPE);
                   }
                    
                 }
                  else if(tempPE.Assignment_Type__c == 'Primary'){
                    system.debug('inside primary Assignemnts++'+tempPE);
                    
                    Date startDate = null;
                    Date endDate = null;
                    string posId = '';
                    string empId = '';
                    Boolean isInsert = true;
                    string reasonCode = '';
                    //End date all the primary assignemnts of the position
                    for(AxtriaSalesIQTM__Position_Employee__c priPosEmp: mapPosEmp.get(tempPE.Team_Instance__c)){
                        if(priPosEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c == tempPE.Position_Code__c
                            && priPosEmp.AxtriaSalesIQTM__Assignment_Type__c == 'Primary'){
                                
                            if(priPosEmp.AxtriaSalesIQTM__Effective_Start_Date__c < tempPE.Effective_Start_Date__c
                            && priPosEmp.AxtriaSalesIQTM__Effective_End_Date__c > tempPE.Effective_Start_Date__c){
                                
                                //end date the previous found and create a new one.
                                AxtriaSalesIQTM__Position_employee__c updPE = new AxtriaSalesIQTM__Position_employee__c(id=priPosEmp.id);
                                updPE.AxtriaSalesIQTM__Effective_End_date__c = tempPE.Effective_Start_Date__c.adddays(-1);
                                setPEUpdate.add(updPE);
                                
                                startDate = priPosEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_Start_date__c>tempPE.Effective_Start_date__c?priPosEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_Start_date__c:tempPE.Effective_Start_date__c;
                                endDate   = priPosEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_date__c>tempPE.Effective_End_date__c?tempPE.Effective_End_date__c:priPosEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_date__c;
                                posId     = priPosEmp.AxtriaSalesIQTM__Position__c;
                                empId     = priPosEmp.AxtriaSalesIQTM__Employee__c;
                                isInsert  = true;
                            } else if(priPosEmp.AxtriaSalesIQTM__Effective_Start_Date__c == tempPE.Effective_Start_Date__c
                            && priPosEmp.AxtriaSalesIQTM__Effective_End_Date__c == tempPE.Effective_Start_Date__c
                            && priPosEmp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c==tempPE.Employee_PRID__c){
                                //Same employee if assigned already in position for the same timeperiod, then, do not insert the record again
                                isInsert = false;
                                reasonCode = 'There is a conflict in assignments';
                                
                            }else if(priPosEmp.AxtriaSalesIQTM__Effective_Start_Date__c >= tempPE.Effective_Start_Date__c){
                                //Rejected the record bacause of a conflict
                                //tempPE.status__c = 'Rejected';
                                isInsert = false;
                                reasonCode = 'There is a conflict in assignments';
                                //lsPEToBeUpdated.add(tempPE);
                            }                         
                        }else{
                            system.debug('mapTI2Code2Sfid++'+mapTI2Code2Sfid);
                            if(mapTI2Code2Sfid.size()!=0 && mapTI2Code2Sfid.get(tempPE.Team_Instance__c)!=null
                                && mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c)!=null){
                                posId = mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c);
                            }
                            if(mapEmpPRID2SFID.size()!=0 && mapEmpPRID2SFID.get(tempPE.Employee_PRID__c)!=null){
                                empId = mapEmpPRID2SFID.get(tempPE.Employee_PRID__c);
                            }
                            isInsert  = true;
                            system.debug('else++posId+empId'+posId+':'+empId+':'+isInsert);
                        }
                        //break from forloop if we get isInsert = false even once
                        if(!isInsert){
                            break;
                        }
                    }
                    //Add the new Primary Assignment
          system.debug('++++ isInsert at add new primary assignment' + isInsert);
          system.debug('++++ posId at add new primary assignment' + posId);
          system.debug('++++ empId at add new primary assignment' + empId);
                    if(isInsert && posId!='' && empId!=''){
                        AxtriaSalesIQTM__Position_employee__c newPE = new AxtriaSalesIQTM__Position_employee__c();
                        newPE.AxtriaSalesIQTM__Position__c = posId;
                        newPE.AxtriaSalesIQTM__Employee__c = empId;
                        newPE.AxtriaSalesIQTM__Effective_Start_date__c = tempPE.Effective_Start_Date__c;
                        newPE.AxtriaSalesIQTM__Effective_End_date__c = tempPE.Effective_End_Date__c;
                        newPE.AxtriaSalesIQTM__Assignment_Type__c = tempPE.Assignment_Type__c;
                        tempPE.status__c = 'Processed';
                        setPEInsert.add(newPE);
                        lsPEToBeUpdated.add(tempPE);
                    }else if(posId=='' && reasonCode == ''){
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'Position code does not exist in SalesIQ';
                        lsPEToBeUpdated.add(tempPE);
            
                    }else if(empId == '' && reasonCode == ''){
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'Employee PRID does not exist in SalesIQ';
                        lsPEToBeUpdated.add(tempPE);
                    }else{
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'There is a conflict in assignments';
                        lsPEToBeUpdated.add(tempPE);
                    }
                  }
                }else{
                   //Changed by Ayushi on 23 July 2018
                  system.debug('+++++++ Team Instance '+ tempPE.Team_Instance__c);
                  system.debug('+++++++ Team Instance '+ mapTI2setPosCode);
                  system.debug('++++++ Position code' + mapTI2setPosCode.get(tempPE.Team_Instance__c));
                  system.debug('++++++ Employee PRID' + setEmpPRID.contains(tempPE.Employee_PRID__c));
                
                if((mapTI2setPosCode.size()!=0 && mapTI2setPosCode.get(tempPE.Team_Instance__c)!=null
                   && !mapTI2setPosCode.get(tempPE.Team_Instance__c).contains(tempPE.Position_Code__c))
                   || (setEmpPRID.size()!=0 && !setEmpPRID.contains(tempPE.Employee_PRID__c))){

                        tempPE.status__c = 'Rejected';
                        if(mapTI2setPosCode.size()!=0 && !mapTI2setPosCode.get(tempPE.Team_Instance__c).contains(tempPE.Position_Code__c) 
                           && setEmpPRID.size()!=0 && !setEmpPRID.contains(tempPE.Employee_PRID__c)){
                            //System.debug('Position Code and Employee PRID both null');  
                            tempPE.Reason_Code__c = 'Position Code and Employee PRID does not exist in SalesIQ';
                        }else if(setEmpPRID.size()!=0 && !setEmpPRID.contains(tempPE.Employee_PRID__c)){
                            tempPE.Reason_Code__c = 'Employee PRID does not exist in SalesIQ';
                        }else{
                            tempPE.Reason_Code__c = 'Position Code does not exist in SalesIQ';
                        }
                       lsPEToBeUpdated.add(tempPE);

                    }else if(mapTI2setPosCode.size()!=0){
                        //directly insert pos emp records
                        string posId = '';
                        string empId = '';
                        boolean isInsert = true;
                        if(mapTI2Code2Sfid.size()!=0 && mapTI2Code2Sfid.get(tempPE.Team_Instance__c)!=null
                            && mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c)!=null){
                            posId = mapTI2Code2Sfid.get(tempPE.Team_Instance__c).get(tempPE.Position_Code__c);
                        }
                        if(mapEmpPRID2SFID.size()!=0 && mapEmpPRID2SFID.get(tempPE.Employee_PRID__c)!=null){
                            empId = mapEmpPRID2SFID.get(tempPE.Employee_PRID__c);
                        }
                        isInsert  = true;
                        if(isInsert && posId!='' && empId!=''){
                            AxtriaSalesIQTM__Position_employee__c newPE = new AxtriaSalesIQTM__Position_employee__c();
                            newPE.AxtriaSalesIQTM__Position__c = posId;
                            newPE.AxtriaSalesIQTM__Employee__c = empId;
                            newPE.AxtriaSalesIQTM__Effective_Start_date__c = tempPE.Effective_Start_Date__c;
                            newPE.AxtriaSalesIQTM__Effective_End_date__c = tempPE.Effective_End_Date__c;
                            newPE.AxtriaSalesIQTM__Assignment_Type__c = tempPE.Assignment_Type__c;
                            tempPE.status__c = 'Processed';
                            setPEInsert.add(newPE);
                            lsPEToBeUpdated.add(tempPE);
                        }
                    }else {
                        tempPE.status__c = 'Rejected';
                        tempPE.Reason_Code__c = 'Position Code/Employee PRID does not exist in SalesIQ';
                        lsPEToBeUpdated.add(tempPE);
                    }
            }
            
        }
        
        //Update Current Position and Employee
        
        if(lsPEToBeUpdated.size()!=0){
            update lsPEToBeUpdated;
        }
        if(setPEUpdate.size()!=0){
            lsPEUpdate.addAll(setPEUpdate);
            update lsPEUpdate;
        }
        if(setPEInsert.size()!=0){
            lsPEInsert.addAll(setPEInsert);
            insert lsPEInsert;
        }
        
        list<AxtriaSalesIQTM__Position_employee__c> lsAll = new list<AxtriaSalesIQTM__Position_employee__c>();
        set<string> setAllPE = new set<string>();
        lsAll.addAll(lsPEUpdate);
        lsAll.addAll(lsPEInsert);
        for(AxtriaSalesIQTM__Position_employee__c p:lsAll){
            setAllPE.add(p.id);
        }
        
        list<AxtriaSalesIQTM__Position_employee__c> lsAllPE = new list<AxtriaSalesIQTM__Position_employee__c>([select id,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Effective_Start_date__c,AxtriaSalesIQTM__Effective_End_date__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Employee__c where id in:setAllPE]);
        list<AxtriaSalesIQTM__Position__c> lsPos = new list<AxtriaSalesIQTM__Position__c>();
        list<AxtriaSalesIQTM__Employee__c> lsEmp = new list<AxtriaSalesIQTM__Employee__c>();
        
        for(AxtriaSalesIQTM__Position_employee__c pe:lsAllPE){
            if(pe.AxtriaSalesIQTM__Effective_Start_date__c <=system.today() && pe.AxtriaSalesIQTM__Effective_End_date__c >system.today()
                && pe.AxtriaSalesIQTM__Assignment_Type__c == 'Primary'){
                AxtriaSalesIQTM__Position__c posToUpdate = new AxtriaSalesIQTM__Position__c(id=pe.AxtriaSalesIQTM__Position__c);
                posToUpdate.AxtriaSalesIQTM__Employee__c = pe.AxtriaSalesIQTM__Employee__c;
                posToUpdate.AxtriaSalesIQTM__Assignment_Status__c = 'Filled';
                lsPos.add(posToUpdate);
                
                if(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c == 'Current'){
                    AxtriaSalesIQTM__Employee__c empToUpdate = new AxtriaSalesIQTM__Employee__c(id=pe.AxtriaSalesIQTM__Employee__c);
                    empToUpdate.AxtriaSalesIQTM__Current_Territory__c = pe.AxtriaSalesIQTM__Position__c;
                    empToUpdate.AxtriaSalesIQTM__Field_Status__c = 'Assigned';
                    lsEmp.add(empToUpdate);
                }
            }
            //Added on 20/7/2018 by Ayushi Jain
         if(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c =='Future' && pe.AxtriaSalesIQTM__Assignment_Type__c == 'Primary'){
             AxtriaSalesIQTM__Position__c posToUpdate = new AxtriaSalesIQTM__Position__c(id=pe.AxtriaSalesIQTM__Position__c);
             posToUpdate.AxtriaSalesIQTM__Employee__c = pe.AxtriaSalesIQTM__Employee__c;
             posToUpdate.AxtriaSalesIQTM__Assignment_Status__c = 'Filled';
             lsPos.add(posToUpdate);
           }
      }
        if(lsPos.size()!=0){
            update lsPos;
        }
        if(lsEmp.size()!=0){
            update lsEmp;
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
            
    }

}