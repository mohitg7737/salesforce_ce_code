global class ATL_Transformation_null_Accounts implements Database.Batchable<sObject> {
    public String query;

    global ATL_Transformation_null_Accounts() {
        this.query = 'select id, Territory__c, Status__c, External_ID__c from Staging_ATL__c where Status__c != \'Updated\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) {

        for(Staging_ATL__c sa : scope)
        {
            sa.Territory__c = null;
            sa.Status__c = 'Updated';
        }

        upsert scope External_ID__c;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}