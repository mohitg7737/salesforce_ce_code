@isTest
public class Grid_Matrix_Clone_Test {
    @istest static void Grid_Matrix_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        insert om;
        
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        insert c;
        
        Grid_Master__c g=new Grid_Master__c();
        g.name='test';
        g.Country__c=c.id;
        g.CurrencyIsoCode='USD';
        insert g;
        Step__c  s=new Step__c();
        s.Matrix__c =g.id;
        insert s;
        Test.startTest();
         System.runAs(loggedinuser){
             SalesIQUtility.setCookieString('CountryID', c.id);
         ApexPages.StandardController sc = new ApexPages.standardController(g);    
         Grid_Matrix_Clone obj=new Grid_Matrix_Clone(sc);
             obj.ViewBlock('view', g.id);
             obj.CloneBlock('view', g.id); 
             obj.EditBlock('view', g.id);
             obj.show2D();
             obj.createtable();
             obj.createtable2();
             obj.savegriddetail();
             obj.cancelfun();
       
             //obj.updategrid();
             obj.Clonegriddetails();
             obj.comp_grid();             
         }
        Test.stopTest();
    }
}