global class BatchInBoundUpdSIQAccounts implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     public map<String, String> mapCountryCode ;
    public set<id> AccList{get;set;}
     public map<String, String> mapVeevaCode ;
     public map<String, String> mapMarketingCode ;
     public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
     //public list<Account>Acclist {set;get;}
     //public set<String>Acc_Id {get;set;}
   Public String nonProcessedAcs {get;set;}
     public integer errorcount{get;set;}
     global BatchInBoundUpdSIQAccounts (){
    nonProcessedAcs = '';
         errorcount = 0;
         AccList=new set<id>();
        mapCountryCode = new map<String, String>();
         mapVeevaCode=new map<String, String>();
        mapMarketingCode =new map<String, String>();
        countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        for(AxtriaSalesIQTM__Country__c c : countrylist){
            if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
                mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
            }
        }
        list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
        for(SIQ_MC_Country_Mapping__c c : countryMap){
            if(!mapVeevaCode.containskey(c.SIQ_Country_Code__c)){
                mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
            }
              if(!mapMarketingCode.containskey(c.SIQ_Country_Code__c)){
                mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
            }
        }
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Accounts Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;   //CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Accounts Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        query = 'SELECT Name,CurrencyIsoCode,OwnerId,CreatedById,SIQ_Marketing_Country__c,SIQ_Billing_City__c, ' +
        'SIQ_Billing_Country__c, SIQ_Postal_Code_in_Customer_Address__c, SIQ_Billing_State_Province__c, Account_Class__c, ' +
        'SIQ_Billing_Address__c, SIQ_Country__c, SIQ_Department_ID__c, SIQ_Department_Name__c, SIQ_External_Organization_Type__c,SIQ_AZ_Internal_Speciality__c, ' +
        'SIQ_External_Account_Id__c, SIQ_External_Account_Number__c, SIQ_External_Parent_Account_Id__c, ' +
        'SIQ_First_Name__c, SIQ_Last_Modified_Date__c, SIQ_Last_Name__c, SIQ_Latitude__c, SIQ_Longitude__c, ' +
        'SIQ_Middle_Name__c, SIQ_Micro_Brick__c, SIQ_Parent_Account_Number__c, SIQ_Primary_Speciality_Id__c, ' +
        'SIQ_Primary_Speciality_Name__c, SIQ_Profile_Consent__c, ' +
        'SIQ_Right_to_be_forgotten__c, SIQ_Secondary_Speciality_Id__c, ' +
        'SIQ_Secondary_Speciality_Name__c, SIQ_Segment1__c, SIQ_Segment_10__c, SIQ_Segment_2__c, SIQ_Segment_3__c, ' +
        'SIQ_Segment_4__c, SIQ_Segment_5__c, SIQ_Segment_6__c, SIQ_Segment_7__c, SIQ_Segment_8__c, SIQ_Segment_9__c, ' +
        'LastModifiedById, SIQ_Account_Name__c, SIQ_Account_Number__c, SIQ_Account_Type__c, SIQ_Status__c ' +
        'FROM SIQ_Account_Master__c ' ;
        if(lastjobDate!=null){
            query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
        //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
    }
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchInBoundUpdSIQAccounts());
    }
     global void execute(Database.BatchableContext bc, List<SIQ_Account_Master__c> records){
         set<String> AclList = new Set < String > ();
        map < String, String > AccId = new Map < String, String > ();
         for (SIQ_Account_Master__c SAA: records) {
            AclList.add(SAA.SIQ_Parent_Account_Number__c);
        }
        List < Account > AccountList = [select id,AccountNumber, External_Account_Id__c from Account where AccountNumber IN: AclList];
        for (Account Acc: AccountList) {
            if (!AccId.containsKey(Acc.AccountNumber)) {
                AccId.put(Acc.AccountNumber, Acc.id);
                System.debug(Acc.External_Account_Id__c);
            }
        }
        // process each batch of records
      set<String> uniqueid = new set<String>();                   
        List<Account> accounts = new List<Account>();
        for (SIQ_Account_Master__c acc : records) {
            if(!uniqueid.contains(acc.SIQ_Account_Number__c)){                             
                Account account=new Account();
                account.AccountNumber=acc.SIQ_Account_Number__c;
               // account.AxtriaSalesIQTM__Active__c=acc.SIQ_Status__c;
                account.AxtriaARSnT__Active__c=acc.SIQ_Status__c;
               // account.BillingAddress=acc.SIQ_Billing_Address__c;
                account.BillingCity=acc.SIQ_Billing_City__c;
                account.BillingCountry=acc.SIQ_Billing_Country__c;
               // account.BillingGeocodeAccuracy=  
              //  account.BillingLatitude=Decimal.valueOf(acc.SIQ_Latitude__c);
              //  account.BillingLongitude=Decimal.valueOf(acc.SIQ_Longitude__c);
                account.BillingPostalCode=acc.SIQ_Micro_Brick__c;
                account.BillingState=acc.SIQ_Billing_State_Province__c;
                account.BillingStreet=acc.SIQ_Billing_Address__c;
                account.AxtriaSalesIQTM__Speciality__c=acc.SIQ_Primary_Speciality_Name__c;
                account.AxtriaSalesIQTM__Speciality1__c=acc.SIQ_Secondary_Speciality_Name__c;
                account.External_HCP_No__c=acc.SIQ_Account_Number__c;
                account.AxtriaSalesIQTM__External_Account_Number__c=acc.SIQ_Account_Number__c;                                    
                account.AxtriaSalesIQTM__AccountType__c=acc.SIQ_Account_Type__c;
                if(acc.SIQ_Account_Name__c!=null){                              
                    if(acc.SIQ_Account_Name__c.length()>70)
                    {
                        account.Name=acc.SIQ_Account_Name__c.substring(0,69);
                    }
                    else{
                        account.Name=acc.SIQ_Account_Name__c;
                    }                                    
                }
                else{
                    account.Name=acc.Name;
                }      
                account.Name__c=acc.SIQ_Account_Name__c;
                account.Type=acc.Account_Class__c;                                                      
                account.CurrencyIsoCode=acc.CurrencyIsoCode;                                                               
                account.OwnerId=acc.OwnerId;                               
                if(acc.SIQ_Country__c!=null) {                        
                    if(mapMarketingCode.get(acc.SIQ_Country__c)!=null){
                        account.Marketing_Code__c=mapMarketingCode.get(acc.SIQ_Country__c);
                    }
                    else{
                        account.Marketing_Code__c=acc.SIQ_Country__c;
                    }
                }
                else{
                    account.Marketing_Code__c='ES';
                }
                //account.Marketing_Code__c=acc.SIQ_Marketing_Country__c;
                account.Billing_Country__c=acc.SIQ_Billing_Country__c;
                //account.Billing_Postal_Code__c=acc.SIQ_Postal_Code_in_Customer_Address__c;
                account.Billing_Postal_Code__c=acc.SIQ_Micro_Brick__c;
                account.Billing_State__c =acc.SIQ_Billing_State_Province__c ;
                account.Billing_Street__c =acc.SIQ_Billing_Address__c;
                if(mapVeevaCode.get(acc.SIQ_Country__c)!=null){
                    account.AxtriaSalesIQTM__Country__c =mapCountryCode.get(mapVeevaCode.get(acc.SIQ_Country__c));
                    account.Country_Code__c=mapVeevaCode.get(acc.SIQ_Country__c);
                    account.AxtriaSalesIQTM__External_Country_Id__c=mapCountryCode.get(mapVeevaCode.get(acc.SIQ_Country__c));                                                            
                }
                else
                {
                    account.AxtriaSalesIQTM__Country__c=null;
                }
                account.Department_ID__c = acc.SIQ_Department_ID__c;
                account.Department_Name__c =acc.SIQ_Department_Name__c;
                account.External_Organization_Type__c =acc.SIQ_External_Organization_Type__c;
                account.External_Account_Id__c =acc.SIQ_External_Account_Id__c;
                account.External_Account_Number__c =acc.SIQ_External_Account_Number__c;
               // account.External_Hcp_no__c = acc.SIQ_External_Account_Number__c;
                account.External_Parent_Account_Id__c = acc.SIQ_External_Parent_Account_Id__c;
                if(acc.SIQ_First_Name__c!=null){
                    if(acc.SIQ_First_Name__c.length()>40){
                        account.AxtriaSalesIQTM__FirstName__c = acc.SIQ_First_Name__c.subString(0,39);
                    }
                    else{
                        account.AxtriaSalesIQTM__FirstName__c = acc.SIQ_First_Name__c;
                    }
                }
                if(acc.SIQ_Last_Name__c!=null){
                    if(acc.SIQ_Last_Name__c.length()>80){
                        account.AxtriaSalesIQTM__LastName__c = acc.SIQ_Last_Name__c.subString(0,79);
                    }
                    else{
                        account.AxtriaSalesIQTM__LastName__c = acc.SIQ_Last_Name__c;
                    }
                }
                account.Last_Modified_Date__c =acc.SIQ_Last_Modified_Date__c;  
                //account.AxtriaSalesIQTM__LastName__c = acc.SIQ_Last_Name__c;
                account.Latitude__c = acc.SIQ_Latitude__c;
                account.Longitude__c =acc.SIQ_Longitude__c;
                account.Middle_Name__c =acc.SIQ_Middle_Name__c;
                account.Micro_Brick__c =acc.SIQ_Micro_Brick__c;
                if (AccId.containsKey(acc.SIQ_Parent_Account_Number__c)) {
                        account.ParentID= AccId.get(acc.SIQ_Parent_Account_Number__c);
                }
                account.Parent_ID__c= acc.SIQ_Parent_Account_Number__c;
                account.Primary_Speciality_Code__c =  acc.SIQ_Primary_Speciality_Id__c;
                account.Primary_Speciality_Name__c = acc.SIQ_Primary_Speciality_Name__c;
                account.Profile_Consent__c = acc.SIQ_Profile_Consent__c;
               // account.Publish_Date_Active__c = acc.SIQ_Publish_Date_Active__c;
                account.AZ_Internal_Speciality__c=acc.SIQ_AZ_Internal_Speciality__c;
               // account.Publish_Date_Address__c = acc.SIQ_Publish_Date_Address__c;
               // account.Publish_Date_Specialty__c =acc.SIQ_Publish_Date_Specialty__c;
               // account.Publish_Date_Type__c =acc.SIQ_Publish_Date_Type__c;
               // account.Publish_Event_Active__c = acc.SIQ_Publish_Event_Active__c;
               // account.Publish_Event_Address__c =acc.SIQ_Publish_Event_Address__c;
               // account.Publish_Event_CustomerType__c = acc.SIQ_Publish_Event_CustomerType__c;
               // account.Publish_Event_Specialty__c = acc.SIQ_Publish_Event_Specialty__c;
                account.Right_to_be_forgotten__c = acc.SIQ_Right_to_be_forgotten__c;
                account.Secondary_speciality_code__c =acc.SIQ_Secondary_Speciality_Id__c;
                account.Secondary_Speciality_Name__c =acc.SIQ_Secondary_Speciality_Name__c;
                account.Segment1__c = acc.SIQ_Segment1__c;
                account.Segment10__c = acc.SIQ_Segment_10__c;
                account.Segment2__c =  acc.SIQ_Segment_2__c;
                account.Segment3__c = acc.SIQ_Segment_3__c;
                account.Segment4__c =acc.SIQ_Segment_4__c;
                account.Segment5__c = acc.SIQ_Segment_5__c;
                account.Segment6__c = acc.SIQ_Segment_6__c;
                account.Segment7__c =acc.SIQ_Segment_7__c;
                account.Segment8__c =acc.SIQ_Segment_8__c;
                account.Segment9__c = acc.SIQ_Segment_9__c;
                account.Status__c =acc.SIQ_Status__c;
                account.AxtriaSalesIQTM__Active__c = acc.SIQ_Status__c;
                accounts.add(account);
                uniqueid.add(acc.SIQ_Account_Number__c);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
            else{
                nonProcessedAcs +=acc.SIQ_Account_Number__c+';';
                errorcount++;
            }                 
            
            
        }
            for(Account A : accounts){
                AccList.add(A.id);
            }
            Schema.SObjectField f = Account.Fields.External_Account_Number__c;
            database.upsert(accounts ,f,false);
          //  upsert accounts External_Account_Number__c;
    }
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs;                             
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sjob.Object_Name__c = 'Account';
                //sjob.Changes__c        
                sJob.Job_Status__c='Successful';
                sjob.Changes__c = ErrorMsg;               
                system.debug('sJob++++++++'+sJob);
                update sJob;
       Database.executeBatch(new BatchUpdAccAfterTrigger(AccList));
    }   
}