public with sharing class SalesIQUtility {

   public static String namespaceClass = 'SnTAxtria';
   public static String namespacePage = 'SnTAxtria__';    

    public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistion(string UserID, String countryID){
        
        system.debug('Hey Country ID is'+ countryID);
        system.debug('Hey User ID is'+ userID);
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.Line__r.Name,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True and Country__c = :countryID order by AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__isActiveCycle__c DESC ];
       
        return accessRecs;
    }

    public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistion(string UserID){
        
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.Line__r.Name,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.Cycle__r.name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True order by AxtriaSalesIQTM__Position__r.Name];
       
        return accessRecs;
    }

    
  public static id getselctedRecordType(string ObjectName, string Type){

    system.debug('Hey Record Type data is '+ ObjectName + '_' + Type);
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = m.get(ObjectName) ;
       
        Schema.DescribeSObjectResult cfrSchema = s.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();       

        system.debug('Hey Record Type data is '+ RecordTypeInfo);
        
        id recordType =  RecordTypeInfo.get(Type).getRecordTypeId();      
        return recordType;
    }
    public static string GenerateRandomNumber(){
    	Double rnd =  Math.random();		
		string randomstring =  string.valueof(Math.random());		
		string str  = randomstring.replace('.','2');
		return str.subString(0,5);
	}

    public static String getCountryCookieName()
    {
        return 'CountryID';
    }

    /*@author : Sahil Mahajan
    @date : March 26, 2018  
    @description : This method create a cookie.
    @param1: set a unique name for a cookie
    @param2: set a desired value
    @return : void
    */ 
    public static void setCookieString(String cookieName, String value){
        Cookie cookieVar = ApexPages.currentPage().getCookies().get(cookieName);
        cookieVar = new Cookie(cookieName, String.valueOf(value),null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{cookieVar});
    }
     
    /*@author : Sahil Mahajan
    @date : March 26, 2018  
    @description : This method get a cookie on a value.
    @param1: cookie name to get data from.
    @return : string
    */
    public static String getCookie(String cookieName){
        Cookie cookieVar = ApexPages.currentPage().getCookies().get(cookieName);
        return cookieVar != null ? cookieVar.getValue() : '';
    }
    
    
    /*@author : Sahil Mahajan
    @date : March 26, 2018  
    @description : This methos will return list of country objects on basis of country Id's   
    @param1: list of country SF id   
    @return : list<Country__c>
    */ 
     public static list<AxtriaSalesIQTM__Country__c> getCountryByCountryId(list<String> countryIds){         
        list<AxtriaSalesIQTM__Country__c> lsCountry = new list<AxtriaSalesIQTM__Country__c>();
        
        lsCountry = [select Name, AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Country_Flag__c , AxtriaSalesIQTM__Status__c from AxtriaSalesIQTM__Country__c where id in: countryIds];
        
        return lsCountry;
     }


     /*@author : Sahil Mahajan
    @date : March 26, 2018   
    @description : This method will User Peristence Record for the user from User Peristence Object if there is some record for the user.  
    @param1: User  
    @return : list<User_Persistence__c>
    */ 
    public static List<AxtriaSalesIQTM__User_Persistence__c> getUserPersistenceByUserID(String userID){
          list<AxtriaSalesIQTM__User_Persistence__c> userPerst = new list<AxtriaSalesIQTM__User_Persistence__c>();
          
          userPerst = [select AxtriaSalesIQTM__User__c, AxtriaSalesIQTM__Default_Country__c, AxtriaSalesIQTM__Workspace_Country__c from AxtriaSalesIQTM__User_Persistence__c where AxtriaSalesIQTM__User__c =: userID];
          
          return userPerst;
     }
     
    /*@author : Sahil Mahajan
    @date : March 26, 2018   
    @description : This is the method that will give the list of countries which are accessible to user.
                   As Country object will have OWD as private therefore it will only return countries that are there in sharing setting. 
    @return : list of countries
    */ 
    public static list<AxtriaSalesIQTM__Country__c> getCountriesFromCountry(){
        list<AxtriaSalesIQTM__Country__c> countries = new list<AxtriaSalesIQTM__Country__c>();
        
        
        countries = [select  id,name from AxtriaSalesIQTM__Country__c]; 
        
        return countries;
    }

    /*@author : Sahil Mahajan
    @date : March 26, 2018    
    @description : This method will be used to check Country Access through all modules step by step.
                   1. It will check whether Sharing rule is configured or not.
                   2. It will check if the country ID (coming from cookie in calling method)that is passes as parameter still has access in the Sharing setting.
                   3. It will check user record in UserPersistence.
                   4. If none of this gives country record then it will give first record of sharing setting.
    @param1: Country ID   
    @return : map of string,string (which will contain Success:CountryID or Erro:ErroMessage)
    */ 
    public static map<string,string> checkCountryAccess(string countryId){ 
        
            map<string,string> successErrorMap = new map<string,string>(); // This map will contain either success:CountryID or Error:Error message
            string firstCountryID = '';
            //This is to check whether Sharing rule exist or not. IF assignedCountries return something that means Sharing rule is there. 
            list<AxtriaSalesIQTM__Country__c> assignedCountries = getCountriesFromCountry();
            system.debug('######### Shared Countries ' + assignedCountries);
            if(assignedCountries != null && assignedCountries.size()>0){
               firstCountryID = assignedCountries[0].id;
               system.debug('######### Shared Countries ' + assignedCountries[0]);
            }
            //This means Sharing rule is not configured therefore return error.
            if(string.isEmpty(firstCountryID)){
              successErrorMap.put('Error', 'No Access to Country');
              return successErrorMap; 
            }
            // This else will execute only if there is sharing rule configured.
            else{
                //If countryID is there in the cookie check whether user has access to that by querying on Country
                if(!string.isEmpty(countryId)){
                  //This will confirm whether user still have access to the country ID selected above.
                  list<AxtriaSalesIQTM__Country__c> countries = getCountryByCountryId(new list<string>{countryId});
                  system.debug('############ countries ' + countries);
                  if(countries == null || countries.size() == 0){
                     successErrorMap.put('Error', 'No COuntry Access');
                     return successErrorMap; 
                  }
               }else{
                  //This will fetch Default country from User Persistence if there is any.
                  list<AxtriaSalesIQTM__User_Persistence__c> userPerst = getUserPersistenceByUserID(UserInfo.getUserId());
                  for(AxtriaSalesIQTM__User_Persistence__c up : userPerst){
                       countryID = up.AxtriaSalesIQTM__Default_Country__c;
                  }
                }
                system.debug('############ countryId ' + countryId);
                if(!string.isEmpty(countryId)){
                  //This will confirm whether user still have access to the country ID selected above.
                  list<AxtriaSalesIQTM__Country__c> countries = getCountryByCountryId(new list<string>{countryId});
                  if(countries == null || countries.size() == 0){
                     successErrorMap.put('Error', 'No Access tp Country');
                     return successErrorMap; 
                  }
                }else{  
                   countryId = firstCountryID;
                }
            }
            successErrorMap.put('Success',countryId);
            return successErrorMap;
      }
     

    /*@author : Sahil Mahajan
    @date : March 26, 2018    
    @description : This methos will return default country ID for the user if user has not selected country from country Tab  
    @param1: User  
    @return : String (Country ID)
    */ 
    
    public static string getDefaultCountryID(String userID){
          string countryID ='';
          list<AxtriaSalesIQTM__User_Access_Permission__c> accessPermission = getUserAccessPermistion(userID);
          for(AxtriaSalesIQTM__User_Access_Permission__c userAccess : accessPermission){
             if(userAccess.AxtriaSalesIQTM__Default_Country__c){
                countryID = userAccess.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c;
                break;
             }
          }
          return countryID;
    }


    private static Map<String,String> getLocaleToDateTimeFmtMap () {
        
        Map<String,String> localeToDateTimeFmtMap = new Map<String,String> {
                                                                    'ar'            => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_AE'         => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_BH'         => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_JO'         => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_KW'         => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_LB'         => 'dd/MM/yyyy hh:mm a',
                                                                    'ar_SA'         => 'dd/MM/yyyy hh:mm a',
                                                                    'bg_BG'         => 'yyyy-M-d H:mm',
                                                                    'ca'            => 'dd/MM/yyyy HH:mm',
                                                                    'ca_ES'         => 'dd/MM/yyyy HH:mm',
                                                                    'ca_ES_EURO'    => 'dd/MM/yyyy HH:mm',
                                                                    'cs'            => 'd.M.yyyy H:mm',
                                                                    'cs_CZ'         => 'd.M.yyyy H:mm',
                                                                    'da'            => 'dd-MM-yyyy HH:mm',
                                                                    'da_DK'         => 'dd-MM-yyyy HH:mm',
                                                                    'de'            => 'dd.MM.yyyy HH:mm',
                                                                    'de_AT'         => 'dd.MM.yyyy HH:mm',
                                                                    'de_AT_EURO'    => 'dd.MM.yyyy HH:mm',
                                                                    'de_CH'         => 'dd.MM.yyyy HH:mm',
                                                                    'de_DE'         => 'dd.MM.yyyy HH:mm',
                                                                    'de_DE_EURO'    => 'dd.MM.yyyy HH:mm',
                                                                    'de_LU'         => 'dd.MM.yyyy HH:mm',
                                                                    'de_LU_EURO'    => 'dd.MM.yyyy HH:mm',
                                                                    'el_GR'         => 'd/M/yyyy h:mm a',
                                                                    'en_AG'         => 'd/M/yyyy h:mm a',
                                                                    'en_AU'         => 'dd/MM/yyyy h:mm a',
                                                                    'en_BS'         => 'd/M/yyyy h:mm a',
                                                                    'en_BB'         => 'dd/MM/yyyy HH:mm',
                                                                    'en_BZ'         => 'M/d/yyyy h:mm a',
                                                                    'en_BM'         => 'M/d/yyyy h:mm a',
                                                                    'en_BW'         => 'MM/dd/yyyy h:mm a',
                                                                    'en_B'          => 'M/d/yyyy h:mm a',
                                                                    'en_CM'         => 'M/d/yyyy h:mm a',
                                                                    'en_CA'         => 'dd/MM/yyyy h:mm a',
                                                                    'en_KY'         => 'M/d/yyyy h:mm a',
                                                                    'en_ER'         => 'M/d/yyyy h:mm a',
                                                                    'en_FK'         => 'M/d/yyyy h:mm a',
                                                                    'en_FJ'         => 'M/d/yyyy h:mm a',
                                                                    'en_GM'         => 'M/d/yyyy h:mm a',
                                                                    'en_GH'         => 'dd/MM/yyyy HH:mm',
                                                                    'en_GI'         => 'M/d/yyyy h:mm a',
                                                                    'en_GY'         => 'M/d/yyyy h:mm a',
                                                                    'en_HK'         => 'd/M/yyyy h:mm a',
                                                                    'en_IN'         => 'd/M/yyyy h:mm a',
                                                                    'en_GB'         => 'dd/MM/yyyy HH:mm',
                                                                    'en_JM'         => 'M/d/yyyy h:mm a',
                                                                    'en_KE'         => 'M/d/yyyy h:mm a',
                                                                    'en_LR'         => 'M/d/yyyy h:mm a',
                                                                    'en_MY'         => 'd/M/yyyy HH:MM',
                                                                    'en_MU'         => 'M/d/yyyy h:mm a',
                                                                    'en_NA'         => 'M/d/yyyy h:mm a',
                                                                    'en_NG'         => 'dd/MM/yyyy HH:mm',
                                                                    'en_PG'         => 'd/M/yyyy h:mm a',
                                                                    'en_PH'         => 'd/M/yyyy h:mm a',
                                                                    'en_GH'         => 'M/d/yyyy h:mm a',
                                                                    'en_ID'         => 'M/d/yyyy h:mm a',
                                                                    'en_IE'         => 'dd/MM/yyyy HH:mm',
                                                                    'en_IE_EURO'    => 'dd/MM/yyyy HH:mm',
                                                                    'en_NZ'         => 'd/MM/yyyy HH:mm',
                                                                    'en_SG'         => 'M/d/yyyy h:mm a',
                                                                    'en_US'         => 'M/d/yyyy h:mm a',
                                                                    'en_ZA'         => 'yyyy/MM/dd hh:mm a',
                                                                    'es'            => 'd/MM/yyyy H:mm',
                                                                    'es_AR'         => 'dd/MM/yyyy HH:mm',
                                                                    'es_BO'         => 'dd-MM-yyyy hh:mm a',
                                                                    'es_CL'         => 'dd-MM-yyyy hh:mm a',
                                                                    'es_CO'         => 'd/MM/yyyy hh:mm a',
                                                                    'es_CR'         => 'dd/MM/yyyy hh:mm a',
                                                                    'es_EC'         => 'dd/MM/yyyy hh:mm a',
                                                                    'es_ES'         => 'd/MM/yyyy H:mm',
                                                                    'es_ES_EURO'    => 'd/MM/yyyy H:mm',
                                                                    'es_GT'         => 'd/MM/yyyy hh:mm a',
                                                                    'es_HN'         => 'MM-dd-yyyy hh:mm a',
                                                                    'es_MX'         => 'd/MM/yyyy hh:mm a',
                                                                    'es_PE'         => 'dd/MM/yyyy hh:mm a',
                                                                    'es_PR'         => 'MM-dd-yyyy hh:mm a',
                                                                    'es_PY'         => 'dd/MM/yyyy hh:mm a',
                                                                    'es_SV'         => 'MM-dd-yyyy hh:mm a',
                                                                    'es_UY'         => 'dd/MM/yyyy hh:mm a',
                                                                    'es_VE'         => 'dd/MM/yyyy hh:mm a',
                                                                    'et_EE'         => 'd.MM.yyyy H:mm',
                                                                    'fi'            => 'd.M.yyyy H:mm',
                                                                    'fi_FI'         => 'd.M.yyyy H:mm',
                                                                    'fi_FI_EURO'    => 'd.M.yyyy H:mm',
                                                                    'fr'            => 'dd/MM/yyyy HH:mm',
                                                                    'fr_BE'         => 'd/MM/yyyy H:mm',
                                                                    'fr_CA'         => 'yyyy-MM-dd HH:mm',
                                                                    'fr_CH'         => 'dd.MM.yyyy HH:mm',
                                                                    'fr_FR'         => 'dd/MM/yyyy HH:mm',
                                                                    'fr_FR_EURO'    => 'dd/MM/yyyy HH:mm',
                                                                    'fr_LU'         => 'dd/MM/yyyy HH:mm',
                                                                    'fr_MC'         => 'dd/MM/yyyy HH:mm',
                                                                    'hr_HR'         => 'yyyy.MM.dd HH:mm',
                                                                    'hu'            => 'yyyy.MM.dd. H:mm',
                                                                    'hy_AM'         => 'M/d/yyyy h:mm a',
                                                                    'is_IS'         => 'd.M.yyyy HH:mm',
                                                                    'in_ID'         => 'dd/MM/yyyy HH:mm',
                                                                    'it'            => 'dd/MM/yyyy H.mm',
                                                                    'it_CH'         => 'dd.MM.yyyy HH:mm',
                                                                    'it_IT'         => 'dd/MM/yyyy H.mm',
                                                                    'ja'            => 'yyyy/MM/dd H:mm',
                                                                    'ja_JP'         => 'yyyy/MM/dd H:mm',
                                                                    'kk_KZ'         => 'M/d/yyyy h:mm a',
                                                                    'km_KH'         => 'M/d/yyyy h:mm a',
                                                                    'ko'            => 'yyyy. M. d a h:mm',
                                                                    'ko_KR'         => 'yyyy. M. d a h:mm',
                                                                    'lt_LT'         => 'yyyy.M.d HH.mm',
                                                                    'lv_LV'         => 'yyyy.d.M HH:mm',
                                                                    'ms_MY'         => 'dd/MM/yyyy h:mm a',
                                                                    'nl'            => 'd-M-yyyy H:mm',
                                                                    'nl_BE'         => 'd/MM/yyyy H:mm',
                                                                    'nl_NL'         => 'd-M-yyyy H:mm',
                                                                    'nl_SR'         => 'd-M-yyyy H:mm',
                                                                    'no'            => 'dd.MM.yyyy HH:mm',
                                                                    'no_NO'         => 'dd.MM.yyyy HH:mm',
                                                                    'pl'            => 'yyyy-MM-dd HH:mm',
                                                                    'pt'            => 'dd-MM-yyyy H:mm',
                                                                    'pt_AO'         => 'dd-MM-yyyy H:mm',
                                                                    'pt_BR'         => 'dd/MM/yyyy HH:mm',
                                                                    'pt_PT'         => 'dd-MM-yyyy H:mm',
                                                                    'ro_RO'         => 'dd.MM.yyyy HH:mm',
                                                                    'ru'            => 'dd.MM.yyyy H:mm',
                                                                    'sk_SK'         => 'd.M.yyyy H:mm',
                                                                    'sl_SI'         => 'd.M.y H:mm',
                                                                    'sv'            => 'yyyy-MM-dd HH:mm',
                                                                    'sv_SE'         => 'yyyy-MM-dd HH:mm',
                                                                    'th'            => 'M/d/yyyy h:mm a',
                                                                    'th_TH'         => 'd/M/yyyy, H:mm ?.',
                                                                    'tr'            => 'dd.MM.yyyy HH:mm',
                                                                    'ur_PK'         => 'M/d/yyyy h:mm a',
                                                                    'vi_VN'         => 'HH:mm dd/MM/yyyy',
                                                                    'zh'            => 'yyyy-M-d ah:mm',
                                                                    'zh_CN'         => 'yyyy-M-d ah:mm',
                                                                    'zh_HK'         => 'yyyy-M-d ah:mm',
                                                                    'zh_TW'         => 'yyyy/M/d a h:mm'
                                                                };
        return localeToDateTimeFmtMap;
    }



    
}