global class MarkMCCPchannelDeleted_EU implements Database.Batchable<sObject> {
    public String query;

    global MarkMCCPchannelDeleted_EU() {
        this.query = 'select id from AxtriaARSnT__SIQ_MC_Cycle_Plan_Channel_vod_O__c where AxtriaARSnT__Rec_Status__c != \'Updated\' ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaARSnT__SIQ_MC_Cycle_Plan_Channel_vod_O__c> scope) {

        for(AxtriaARSnT__SIQ_MC_Cycle_Plan_Channel_vod_O__c mct : scope)
        {
            mct.AxtriaARSnT__Rec_Status__c ='Deleted';
        }   

        update scope;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}