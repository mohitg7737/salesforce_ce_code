public with sharing class StepMatrix implements Step{
    public map<String, String> matrixMap;
    public String type;
    public StepMatrix(Step__c step){
        matrixMap = new Map<String, String>();
        for(Grid_Details__c gd : [SELECT Id, Dimension_1_Value__c, Dimension_2_Value__c, Output_Value__c, Grid_Master__r.Grid_Type__c FROM Grid_Details__c WHERE Grid_Master__c=:step.Matrix__c]){
            type = gd.Grid_Master__r.Grid_Type__c;

            system.debug('+++++++++ Hey ' + gd);
            if(type == '2D'){
                matrixMap.put(gd.Dimension_1_Value__c.toUpperCase() + '_' + gd.Dimension_2_Value__c.toUpperCase(), gd.Output_Value__c.toUpperCase());
            }else{

                matrixMap.put(gd.Dimension_1_Value__c != null ? gd.Dimension_1_Value__c.toUpperCase() : '', gd.Output_Value__c.toUpperCase());
            }
        }

        system.debug('+++++ Hey Matrix Map is '+ matrixMap);

    }
    
    
    public String solveStep(Step__c step, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu , List<BU_Response__c> allBuResponses, List<String> allAggregateFunc){
        String dim1 = '';
        String dim2 = '';
        if(step.Grid_Param_1__r.Parameter_Name__c != null){
            if(nameFieldMap.containsKey(step.Grid_Param_1__r.Parameter_Name__c.toUpperCase())){
                dim1 = String.isNotBlank((String)bu.get(nameFieldMap.get(step.Grid_Param_1__r.Parameter_Name__c.toUpperCase())))?((String)bu.get(nameFieldMap.get(step.Grid_Param_1__r.Parameter_Name__c.toUpperCase()))).toUpperCase(): 'ND';
            }else{
                dim1 = String.isNotBlank(acfMap.get(step.Grid_Param_1__r.Parameter_Name__c))? (acfMap.get(step.Grid_Param_1__r.Parameter_Name__c)).toUpperCase() : 'ND';
            }
        }

        system.debug('+++++++ Hey DIM 1 is '+ dim1);

        if(step.Grid_Param_2__r.Parameter_Name__c != null){
            if(nameFieldMap.containsKey(step.Grid_Param_2__r.Parameter_Name__c.toUpperCase())){
                dim2 = String.isNotBlank((String)bu.get(nameFieldMap.get(step.Grid_Param_2__r.Parameter_Name__c.toUpperCase())))? ((String)bu.get(nameFieldMap.get(step.Grid_Param_2__r.Parameter_Name__c.toUpperCase()))).toUpperCase() : 'ND';
            }else{
                dim2 = String.isNotBlank(acfMap.get(step.Grid_Param_2__r.Parameter_Name__c).toUpperCase()) ? (acfMap.get(step.Grid_Param_2__r.Parameter_Name__c)).toUpperCase() : 'ND';
            }
        }
        
        system.debug('+++++++ Hey Concat String is '+ dim1 + '_' + dim2);


        if(type == '2D'){
            if(matrixMap.containsKey(dim1 + '_' + dim2))
            {
                system.debug('+++ Hey Corresponding value is '+ matrixMap.get(dim1 + '_' + dim2));
            }

            return matrixMap.get(dim1 + '_' + dim2);
        }

        return matrixMap.get(dim1);
    }
}