/**********************************************************************************************
@author     : Ritu Pandey
@date       : 1 May 2010
@description: This Batch Class is used for copying SOA Inbound Employee Master to currentPersonFeed__c
@Client     : AZ Global
Revison(s)  :
**********************************************************************************************/
global class BatchCopySOAInbound2CurrentFeed implements Database.Batchable<sObject>, Database.Stateful,schedulable{ 
    global String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    map<string,string> mapVeevaCode ;
    map<string,string> mapMarketingCode ;
        
    global BatchCopySOAInbound2CurrentFeed(){
        recordsProcessed=0;
        mapVeevaCode = new map<string,string>();
        mapMarketingCode = new map<string,string>();
        getVeevaCountryCode();
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Employee Feed' and Job_Status__c='Successful' and Job_Type__c = 'Inbound' Order By CreatedDate desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'Employee Feed';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
    
        query  = 'select id,Name,SIQ_Email__c,SIQ_Employee_ID__c,SIQ_PRID__c,SIQ_Company__c,' +
            'SIQ_Job_Title__c,SIQ_HR_Status__c,SIQ_First_Name__c,SIQ_Middle_Name__c,' +
            'SIQ_Last_Name__c,SIQ_Gender__c,SIQ_Group_Name__c,SIQ_Hire_Date__c,SIQ_Rehire_Date__c,'+
            'SIQ_Termination_Date__c,SIQ_First_Day_of_Leave__c,SIQ_on_Leave__c,SIQ_Last_Day_of_Leave_Actual__c,'+
            'SIQ_Address__c,SIQ_Street__c,SIQ_City__c,SIQ_State__c,SIQ_Postal_Code__c,SIQ_Country__c,'+
            'SIQ_Cellphone_Number__c,SIQ_Direct_Manager__c,SIQ_Mgr_ID__c,SIQ_Employee_Type__c,SIQ_Experience_Company__c,'+
            'SIQ_Experience_Industry__c,SIQ_Experience_Therapeutic_Area__c,SIQ_Home_Phone__c,SIQ_Job_Title_Code__c,'+
            'SIQ_Last_Day_of_work__c,SIQ_Last_Modified_Date__c,SIQ_Marketing_Code__c,SIQ_Nick_Name__c,SIQ_Work_Phone__c,'+
            'SIQ_Segment1__c,SIQ_Segment2__c,SIQ_Segment3__c,SIQ_Segment4__c,SIQ_Segment5__c,'+
            'SIQ_Segment6__c,SIQ_Segment7__c,SIQ_Segment8__c,SIQ_Segment9__c,SIQ_Work_Fax__c,SIQ_Segment10__c,SIQ_Team_Name__c,SIQ_Country_Code__c from SIQ_Employee_Master__c';
            //' where SIQ_Last_Modified_Date=Date.today().addDays(-1)';
        if(lastjobDate!=null){
            query = query + ' where lastModifiedDate >:lastjobDate';
        }
         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchCopySOAInbound2CurrentFeed());
    }
    
    public void getVeevaCountryCode(){
        
        list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
        for(SIQ_MC_Country_Mapping__c c : countryMap){
            if(c.SIQ_Country_Code__c!=null && !mapVeevaCode.containskey(c.SIQ_Country_Code__c)){
                mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
            }
              if(c.SIQ_Country_Code__c!=null && !mapMarketingCode.containskey(c.SIQ_Country_Code__c)){
                mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
            }
        }              
        
    }
    global void execute(Database.BatchableContext BC,list<SIQ_Employee_Master__c> lsInboundEmployeeFeed){ 
        list<CurrentPersonFeed__c> lsRecordsToBeInserted = new list<CurrentPersonFeed__c>();
        //copy the data to currentPersonFeed__c
        for(SIQ_Employee_Master__c inboundSOARec: lsInboundEmployeeFeed){
            //recordsProcessed++;
            CurrentPersonFeed__c CurrPersonFeedRec  = new CurrentPersonFeed__c();
            currPersonFeedRec.Full_Name__c          = inboundSOARec.Name;
            currPersonFeedRec.Email__c              = inboundSOARec.SIQ_Email__c;
            currPersonFeedRec.Employee_ID__c        = inboundSOARec.SIQ_Employee_ID__c;
            currPersonFeedRec.PRID__c               = inboundSOARec.SIQ_PRID__c;
            currPersonFeedRec.Company__c            = inboundSOARec.SIQ_Company__c; 
            currPersonFeedRec.JobCodeName__c        = inboundSOARec.SIQ_Job_Title__c;
            currPersonFeedRec.HR_Status__c          = inboundSOARec.SIQ_HR_Status__c;
            currPersonFeedRec.First_Name__c         = inboundSOARec.SIQ_First_Name__c;
            currPersonFeedRec.Middle_Name__c        = inboundSOARec.SIQ_Middle_Name__c;
            currPersonFeedRec.Last_Name__c          = inboundSOARec.SIQ_Last_Name__c;
            currPersonFeedRec.Gender__c             = inboundSOARec.SIQ_Gender__c;
            currPersonFeedRec.Group_Name__c         = inboundSOARec.SIQ_Group_Name__c;
            currPersonFeedRec.AssignmentStatusValue__c = inboundSOARec.SIQ_HR_Status__c;
            currPersonFeedRec.OriginalHireDate__c   = inboundSOARec.SIQ_Hire_Date__c;
            currPersonFeedRec.Hire_Date__c          = inboundSOARec.SIQ_Hire_Date__c;
            currPersonFeedRec.TerminationDate__c    = inboundSOARec.SIQ_Termination_Date__c;
            currPersonFeedRec.First_Day_of_Leave__c = inboundSOARec.SIQ_First_Day_of_Leave__c;
            //currPersonFeedRec.On_Leave__c         = inboundSOARec.SIQ_on_Leave__c;
            currPersonFeedRec.Last_Day_of_Leave__c  = inboundSOARec.SIQ_Last_Day_of_Leave_Actual__c;
            
            currPersonFeedRec.Address__c            = inboundSOARec.SIQ_Address__c;
            currPersonFeedRec.AddressLine1__c       = inboundSOARec.SIQ_Street__c;
            currPersonFeedRec.AddressCity__c        = inboundSOARec.SIQ_City__c;
            currPersonFeedRec.AddressStateCode__c   = inboundSOARec.SIQ_State__c;
            currPersonFeedRec.AddressPostalCode__c  = inboundSOARec.SIQ_Postal_Code__c;
            
            currPersonFeedRec.AddressCountry__c     = inboundSOARec.SIQ_Country__c; //Country Name
            currPersonFeedRec.SOA_Country_Code__c   = inboundSOARec.SIQ_Country_Code__c; //Country Code
            currPersonFeedRec.Veeva_Country_Code__c = mapVeevaCode!=null && mapVeevaCode.containsKey(inboundSOARec.SIQ_Country_Code__c)?mapVeevaCode.get(inboundSOARec.SIQ_Country_Code__c):'';
            currPersonFeedRec.Marketing_Code__c     = inboundSOARec.SIQ_Marketing_Code__c;
            currPersonFeedRec.PersonalCell__c       = inboundSOARec.SIQ_Cellphone_Number__c;
            
            currPersonFeedRec.ReportingToWorkerName__c  = inboundSOARec.SIQ_Direct_Manager__c; //Manager Name
            currPersonFeedRec.ReportsToAssociateOID__c  = inboundSOARec.SIQ_Mgr_ID__c; //Manager Emp ID/Emp Code
            currPersonFeedRec.Worker_Category__c        = inboundSOARec.SIQ_Employee_Type__c;
            currPersonFeedRec.Experience_Company__c     = inboundSOARec.SIQ_Experience_Company__c;
            currPersonFeedRec.Experience_Industry__c    = inboundSOARec.SIQ_Experience_Industry__c;
            currPersonFeedRec.Experience_Therapeutic_Area__c = inboundSOARec.SIQ_Experience_Therapeutic_Area__c;
            //currPersonFeedRec.Fax_Number__c                = inboundSOARec.SIQ_Fax_Number__c;
            //currPersonFeedRec.Email__c                     = inboundSOARec.SIQ_Field_Status__c;
            
            currPersonFeedRec.HomePhone__c              = inboundSOARec.SIQ_Home_Phone__c;
            currPersonFeedRec.JobCode__c                = inboundSOARec.SIQ_Job_Title_Code__c;
            currPersonFeedRec.Last_Day_Of_Work__c       = inboundSOARec.SIQ_Last_Day_of_work__c;
            currPersonFeedRec.Last_Modified_Date__c     = inboundSOARec.SIQ_Last_Modified_Date__c;
            currPersonFeedRec.NickName__c               = inboundSOARec.SIQ_Nick_Name__c;
            currPersonFeedRec.WorkPhone__c              = inboundSOARec.SIQ_Work_Phone__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_OrigHireDate__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_SalesforceUserName__c;
            currPersonFeedRec.Segment_10__c             = inboundSOARec.SIQ_Segment10__c;
            currPersonFeedRec.Segment_1__c              = inboundSOARec.SIQ_Segment1__c;
            currPersonFeedRec.Segment_2__c              = inboundSOARec.SIQ_Segment2__c;
            currPersonFeedRec.Segment_3__c              = inboundSOARec.SIQ_Segment3__c;
            currPersonFeedRec.Segment_4__c              = inboundSOARec.SIQ_Segment4__c;
            currPersonFeedRec.Segment_5__c              = inboundSOARec.SIQ_Segment5__c;
            currPersonFeedRec.Segment_6__c              = inboundSOARec.SIQ_Segment6__c;
            currPersonFeedRec.Segment_7__c              = inboundSOARec.SIQ_Segment7__c;
            currPersonFeedRec.Segment_8__c              = inboundSOARec.SIQ_Segment8__c;
            currPersonFeedRec.Segment_9__c              = inboundSOARec.SIQ_Segment9__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_Status__c;
            
            currPersonFeedRec.Department__c             = inboundSOARec.SIQ_Team_Name__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_Training_Completion_Date__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_Training_Name__c;
            currPersonFeedRec.Work_Fax__c               = inboundSOARec.SIQ_Work_Fax__c;
            currPersonFeedRec.WorkPhone__c              = inboundSOARec.SIQ_Work_Phone__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_Office_Number__c;
            //currPersonFeedRec.Email__c                = inboundSOARec.SIQ_isSalesforceUser__c;
            
            lsRecordsToBeInserted.add(currPersonFeedRec);
            recordsProcessed++;
            
        }
        if(lsRecordsToBeInserted.size()!=0){
            Database.insert(lsRecordsToBeInserted);
        }
    }
    
    global void finish(Database.BatchableContext BC){
         Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
       // sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
        Database.executeBatch(new BatchProcessInbound(batchID),200);
    }
}