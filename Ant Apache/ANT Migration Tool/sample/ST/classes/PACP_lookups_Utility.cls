global class PACP_lookups_Utility implements Database.Batchable<sObject> {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
  
    global PACP_lookups_Utility(String teamInstance) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        selectedTeamInstance = teamInstance;
        
        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.name From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id,Name,Cycle__c, Account_ID__c, Team_ID__c, Territory_ID__c, Segment__c, Objective__c, Product_ID__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Position__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM Call_Plan__c  Where Cycle__c = \'' + teamInstance + '\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Call_Plan__c> scopeRecs) {
      system.debug('Hello++++++++++++++++++++++++++++');
        Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Map<string,id> posCodeIdMap = new Map<string,id>();
        Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        Map<string,id> accountNumberIdMap = new Map<string,id>();
        Map<string,id> accountNumberPosIDtoPosAccIdMap = new Map<string,id>();
        Map<string,id> posTeamInstMap = new Map<string,id>();
        
        
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;

        for(Call_Plan__c cp : scopeRecs){      //Positions Set
            if(!string.IsBlank(cp.Territory_ID__c)){
                posCodeSet.add(cp.Territory_ID__c);
            }
            if(!string.IsBlank(cp.Account_ID__c)){            //Accounts Set
                accountNumberSet.add(cp.Account_ID__c);
            }
                   
        }
        String key;
        /*for(AxtriaSalesIQTM__Position__c  pos : [Select id,  AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position__c 
                                                where AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet]){
            if(pos.AxtriaSalesIQTM__inactive__c == false){
                key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                posCodeIdMap.put(key,pos.id);
            }
            else{
              key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                inactivePosCodeIdMap.put(key,pos.id);
            }
        } */
        //-------- Position and PTI Map
        for(AxtriaSalesIQTM__Position_Team_Instance__c  posTeamInst : [Select Id,AxtriaSalesIQTM__Team_Instance_ID__r.Name,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position_Team_Instance__c 
                                                where AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet]){
            key = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c + (string)posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            if(posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c == false){
                
                posCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);        //Map of Active Positions and their lookups
            }
            else{
                inactivePosCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);    //Map of InActive Positions and their lookups
            }
            String position = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c;
          String teamInstance = posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            posTeamInstMap.put(posTeamInst.AxtriaSalesIQTM__Position_ID__c,posTeamInst.Id);
            
        }
        
        
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber from Account where AccountNumber IN:accountNumberSet ]){ //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of accounts and their lookups
            
        }
        
        //-------- Position Account Map
         for(AxtriaSalesIQTM__Position_Account__c acc : [Select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:accountNumberSet and AXTRIASALESIQTM__ASSIGNMENT_STATUS__C ='Active' and AxtriaSalesIQTM__Team_Instance__r.name = :selectedTeamInstance]){ //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberPosIDtoPosAccIdMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber+acc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,acc.id);                  //Map of accounts and their lookups
            
        }
        
        
        for(Call_Plan__c cp : scopeRecs){
          
            
            
            //---------- Team Instance lookups
            if(!string.IsBlank(cp.Cycle__c)){
                if(teamInstanceNametoIDMap.containsKey(cp.Cycle__c)){
                    cp.Team_Instance__c = teamInstanceNametoIDMap.get(cp.Cycle__c);    //Filling lookups of Team Instance
                }
                else{
                    cp.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Team_Instance__c';
                    tempErrorObj.Comments__c = 'Problem in Name of Team Instance: Team Instance name not found in map';
                    tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                    errorList.add(tempErrorObj);
                }
            }
            else{
              
                cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Team_Instance__c';
                tempErrorObj.Comments__c = 'Team Instance Name is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
            }
            //---------- Account lookups
            if(!string.isBlank(cp.Account_ID__c)){
              if(accountNumberIdMap.containsKey(cp.Account_ID__c)){
                cp.Account__c = accountNumberIdMap.get(cp.Account_ID__c);        //Filling lookups of Accounts
              }
              else{
                cp.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Account__c';
                    tempErrorObj.Comments__c = 'Account Number not found in map';
                    tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Account__c';
                tempErrorObj.Comments__c = 'Account Number is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
              
            }
            
            //------------  Position and PTI lookups
            String keyClientCodeTeamName;
            keyClientCodeTeamName = (string)cp.Territory_ID__c + (string)cp.Cycle__c ;
            
            if(string.IsBlank(cp.Territory_ID__c)){
                cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Position Code is blank';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
            }
            
            else if(!string.IsBlank(cp.Territory_ID__c) && posCodeIdMap.containsKey(keyClientCodeTeamName)){
              String activePosID = posCodeIdMap.get(keyClientCodeTeamName);          
                cp.Position__c = activePosID;                          //Filling lookups of Active Positions
                cp.Position_Team_Instance__c = posTeamInstMap.get(activePosID);        //Filling lookups of PTI
                
            }
            else if(!string.IsBlank(cp.Territory_ID__c) && inactivePosCodeIdMap.containsKey(keyClientCodeTeamName)){
              String inActivePosID = inactivePosCodeIdMap.get(keyClientCodeTeamName);      
              cp.Position__c = inActivePosID;                        //Filling lookups of InActive Positions
              cp.Position_Team_Instance__c = posTeamInstMap.get(inActivePosID);        //Filling lookups of PTI
              
              
                cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Warning: Position is Inactive in system, but its lookup is filled';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
            }
            else if((!posCodeIdMap.containsKey(keyClientCodeTeamName)) && (!inactivePosCodeIdMap.containsKey(keyClientCodeTeamName))){
              cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Either Position or its Position Team Instance does not exist';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
              
            }
            
            
            if(!string.isBlank(cp.Account_ID__c) && !string.isBlank(cp.Territory_ID__c)){
              if(accountNumberPosIDtoPosAccIdMap.containsKey(cp.Account_ID__c+cp.Territory_ID__c)){
                cp.Position_Account__c = accountNumberPosIDtoPosAccIdMap.get(cp.Account_ID__c+cp.Territory_ID__c);        //Filling lookups of Accounts
              }
              else{
                cp.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Account__c';
                    tempErrorObj.Comments__c = 'Position Account not found in map';
                    tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Account__c';
                tempErrorObj.Comments__c = 'Position or Account is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Cycle__c;
                errorList.add(tempErrorObj);
              
            }
            
            
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        update scopeRecs ;

    }

    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new  Call_Plan_Data_to_PACP(selectedTeamInstance),2000);  
    }
}