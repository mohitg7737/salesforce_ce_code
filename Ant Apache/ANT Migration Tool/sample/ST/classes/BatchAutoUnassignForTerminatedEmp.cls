global class BatchAutoUnassignForTerminatedEmp implements Database.Batchable<sObject>, Database.stateful{
    global string query;
     public String batchID;
    list<AxtriaSalesIQTM__Position_Employee__c> toBeDeletedList;
    list<AxtriaSalesIQTM__Position_Employee__c> toBeUpdatedList;
    list<id> empIdList;
    list<CR_Employee_Feed__c> toBeUpdatedCrEmpFeed;
    list<AxtriaSalesIQTM__Employee__c> empUpdatelist;
    Map<id,list<AxtriaSalesIQTM__Position_Employee__c>> empIdPosEmpListMap;
    map<string,Date> mapAssId2TermDate = new map<string,Date>();
    global BatchAutoUnassignForTerminatedEmp(String ID){
         batchID=ID;
        
        query = 'select id,AxtriaSalesIQTM__HR_Termination_Date__c,AxtriaSalesIQTM__Employee_ID__c from AxtriaSalesIQTM__Employee__c where Employee_Status__c = \'Inactive\' or Employee_Status__c = \'Separated\' or'+
                ' Employee_Status__c = \'Retired\' or Employee_Status__c = \'Deceased\' or Employee_Status__c = \'Terminated\'';
    }
    public void InitialiseVariables(){
        toBeDeletedList = new list<AxtriaSalesIQTM__Position_Employee__c>();
        toBeUpdatedList = new list<AxtriaSalesIQTM__Position_Employee__c>();
        empIdList = new list<id>();
        toBeUpdatedCrEmpFeed = new list<CR_Employee_Feed__c>();
        empUpdatelist = new list<AxtriaSalesIQTM__Employee__c>();
        empIdPosEmpListMap = new Map<id,list<AxtriaSalesIQTM__Position_Employee__c>>();
    }
    public void CreateMaps(List<AxtriaSalesIQTM__Employee__c> scopeCurrentEmpList){
        list<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select id,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                                                  AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Employee__r.id,
                                                                  AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Effective_End_Date__c,
                                                                  AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Reason_Code__c
                                                                  from AxtriaSalesIQTM__Position_Employee__c];
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
            if(!empIdPosEmpListMap.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.id)){
                empIdPosEmpListMap.put(posEmp.AxtriaSalesIQTM__Employee__r.id,new list<AxtriaSalesIQTM__Position_Employee__c>());    
            }
            empIdPosEmpListMap.get(posEmp.AxtriaSalesIQTM__Employee__r.id).add(posEmp);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
         system.debug('inside getQueryLocator ');
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Employee__c> scopeCurrentEmpList){
        system.debug('scopeCurrentEmpList.size()'+scopeCurrentEmpList.size());
        InitialiseVariables();
        createMaps(scopeCurrentEmpList);
        for(AxtriaSalesIQTM__Employee__c Emp : scopeCurrentEmpList){
            if(empIdPosEmpListMap.containsKey(Emp.id) && Emp.AxtriaSalesIQTM__HR_Termination_Date__c != null){
                list<AxtriaSalesIQTM__Position_Employee__c> posEmpOfEmp = new list<AxtriaSalesIQTM__Position_Employee__c>();
                posEmpOfEmp = empIdPosEmpListMap.get(Emp.id);
                system.debug('posEmpOfEmp.size()'+posEmpOfEmp.size());
                for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpOfEmp){
                    //For future dated assignments
                    if(posEmp.AxtriaSalesIQTM__Effective_Start_Date__c > Emp.AxtriaSalesIQTM__HR_Termination_Date__c){
                        toBeDeletedList.add(posEmp); 
                        empIdList.add(posEmp.AxtriaSalesIQTM__Employee__c);
                        mapAssId2TermDate.put(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,Emp.AxtriaSalesIQTM__HR_Termination_Date__c);             
                    }
                    else if(posEmp.AxtriaSalesIQTM__Effective_End_Date__c > Emp.AxtriaSalesIQTM__HR_Termination_Date__c &&
                            posEmp.AxtriaSalesIQTM__Effective_Start_Date__c != Emp.AxtriaSalesIQTM__HR_Termination_Date__c){
                        AxtriaSalesIQTM__Position_Employee__c toBeUpdated = new AxtriaSalesIQTM__Position_Employee__c();
                        toBeUpdated.id = posEmp.id;
                        toBeUpdated.AxtriaSalesIQTM__Effective_End_Date__c = Emp.AxtriaSalesIQTM__HR_Termination_Date__c;
                        toBeUpdatedList.add(toBeUpdated);
                        empIdList.add(posEmp.AxtriaSalesIQTM__Employee__c);
                    }
                }
            }
            empUpdatelist.add(new AxtriaSalesIQTM__Employee__c(id = Emp.id,AxtriaSalesIQTM__Field_Status__c = 'Unassigned'));
        }
        if(empUpdatelist.size() > 0){
            update empUpdatelist;
        }
        if(toBeUpdatedList.size() > 0){
            update toBeUpdatedList;
        }
        if(toBeDeletedList.size() > 0){
            list<Deleted_Position_Employee__c> toBeInsertedList = new list<Deleted_Position_Employee__c>();
            for(AxtriaSalesIQTM__Position_Employee__c PEtoDel : toBeDeletedList){
                Deleted_Position_Employee__c newDPE = new Deleted_Position_Employee__c();
                newDPE.Assignment_Type__c = PEtoDel.AxtriaSalesIQTM__Assignment_Type__c;
                newDPE.Employee__c = PEtoDel.AxtriaSalesIQTM__Employee__c;
                if(mapAssId2TermDate.containsKey(PEtoDel.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c)){
                    newDPE.End_Date__c = mapAssId2TermDate.get(PEtoDel.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
                }
                else{
                    newDPE.End_Date__c = PEtoDel.AxtriaSalesIQTM__Effective_End_Date__c;
                }
                newDPE.Position__c = PEtoDel.AxtriaSalesIQTM__Position__c;
                newDPE.Reason_Code__c = PEtoDel.AxtriaSalesIQTM__Reason_Code__c;
                newDPE.Start_Date__c = PEtoDel.AxtriaSalesIQTM__Effective_Start_Date__c;
                
                toBeInsertedList.add(newDPE);    
            }
            insert toBeInsertedList;
            delete toBeDeletedList;    
        }
        toBeUpdatedCrEmpFeed = [select id from CR_Employee_Feed__c where Employee__c in :empIdList and Event_Name__c = 'Terminate Employee'];
        for(CR_Employee_Feed__c cr : toBeUpdatedCrEmpFeed){
            cr.Status__c = 'Completed Action';    
        }
        update toBeUpdatedCrEmpFeed;
    }
    global void finish(Database.BatchableContext BC){
        // To send mail in case of mismatch between expected and separation date 
        //ExpectedSeparationDateCheck ESDC = new ExpectedSeparationDateCheck();
        
        BatchUpdateEmployeeStatus obj1 = new BatchUpdateEmployeeStatus(batchID);
        database.executeBatch(obj1,200); 
          
    }
}