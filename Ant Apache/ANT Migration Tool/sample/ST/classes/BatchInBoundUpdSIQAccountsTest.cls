@isTest
private class BatchInBoundUpdSIQAccountsTest {
   @testSetup 
    static void setup() {
        List<SIQ_Account_Master__c> siqaccounts = new List<SIQ_Account_Master__c>();
        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;
        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
        
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active'));
        insert country;
        SIQ_MC_Country_Mapping__c mc=new SIQ_MC_Country_Mapping__c();
        mc.SIQ_MC_Code__c='US';
        mc.SIQ_Veeva_Country_Code__c='US';
        mc.SIQ_Country_Code__c='US';
        insert mc;
      
        // insert 10 accounts
        for (Integer i=0;i<5;i++) {
            siqaccounts.add(new SIQ_Account_Master__c(name='Account '+i,SIQ_First_Name__c='a'+i,SIQ_Last_Name__c='a'+i,SIQ_Account_Name__c= 'Account '+i,
                SIQ_Billing_Address__c='New York', SIQ_External_Account_Number__c='1'+i,SIQ_Billing_Country__c='US', SIQ_Country__c='US', SIQ_Marketing_Country__c='US'
                                                     ));
        } 
        insert siqaccounts;

                                                                                                                                     
                                                                                                                                              
         
                                                   
    }
    static testmethod void test() {        
        Test.startTest();
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        insert teamIns;
        BatchInBoundUpdSIQAccounts usa = new BatchInBoundUpdSIQAccounts();
        Id batchId = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}