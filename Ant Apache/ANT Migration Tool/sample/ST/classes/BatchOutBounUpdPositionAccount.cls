global class BatchOutBounUpdPositionAccount implements Database.Batchable<sObject>, Database.Stateful,schedulable{
   public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     global Date today=Date.today();
     public String UnAsgnPos='00000';
     public String UnAsgnPos1='0';
     public List<String> posCodeList=new List<String>();
     public map<String,String>Countrymap {get;set;}
     public set<String> Uniqueset {get;set;}  
    public set<String> mkt {get;set;} 
     public list<Custom_Scheduler__c> mktList {get;set;}  
      public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}                                                    

     global BatchOutBounUpdPositionAccount (){//set<String> Accountid
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
       Countrymap = new map<String,String>();
        Uniqueset = new set<String>(); 
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();

      mkt = new set<String>();
      mktList=new list<Custom_Scheduler__c>();
      mktList=Custom_Scheduler__c.getall().values();
      for(Custom_Scheduler__c cs:mktList){
        if(cs.Status__c==true && cs.Schedule_date__c!=null){
           if(cs.Schedule_date__c>today.addDays(1)){
              mkt.add(cs.Marketing_Code__c);
            }
            else{
                    //update Custom scheduler record
                   cs.Status__c = False;
                   lsCustomSchedulerUpdate.add(cs);
                }
        }
      }
      
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Position Account' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
      else{
        lastjobDate=null;       //else we set the lastjobDate to null
      }
      System.debug('last job'+lastjobDate);
        //Last Bacth run ID

    for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

    Scheduler_Log__c sJob = new Scheduler_Log__c();
    
    sJob.Job_Name__c = 'Position Account';
    sJob.Job_Status__c = 'Failed';
    sJob.Job_Type__c='Outbound';
    sJob.Cycle__c=cycle;
    sJob.Created_Date2__c = DateTime.now();
  
    insert sJob;
      batchID = sJob.Id;
     
      recordsProcessed =0;
     query = 'Select Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,Event__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, ' +
     'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, ' +
      'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Account__r.External_Account_Id__c, AxtriaSalesIQTM__Account__r.Type ' +
      'FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList ' ;
    
                    
                          
       
                            
                                                                                                                           
                                                                                                                                                        
                                                                                 
                                     
      
        
        if(lastjobDate!=null){
          query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
   
          
   
    if(mkt.size()>0){
        query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
    }
    System.debug('query'+ query);
       //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }        
    }
    
    
    
   global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchOutBounUpdPositionAccount());                                                           
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Account__c> records){
        // process each batch of records
       

        List<SIQ_Position_Account_O__c> positionAccounts = new List<SIQ_Position_Account_O__c>();
        for (AxtriaSalesIQTM__Position_Account__c position : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
              //String key = position.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Effective_End_Date__c;
               String key = position.id;
               if(!Uniqueset.contains(Key)){
               SIQ_Position_Account_O__c positionAccount=new SIQ_Position_Account_O__c();
           
           // positionAccount.SIQ_Marketing_Code__c=countrymap.get(position.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
           positionAccount.SIQ_Marketing_Code__c=position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
        //positionAccount.SIQ_Country_Code__c=countrymap.get(position.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
        positionAccount.SIQ_Country_Code__c=position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
        positionAccount.SIQ_Event__c=position.Event__c;
        positionAccount.SIQ_Salesforce_Name__c=position.AxtriaSalesIQTM__Team_Instance__r.Name;
        positionAccount.SIQ_CUSTOMER_ID__c=position.AxtriaSalesIQTM__Account__r.AccountNumber;
        positionAccount.SIQ_Customer_Class__c= position.AxtriaSalesIQTM__Account__r.Type;
        positionAccount.SIQ_Team_Instance__c=position.AxtriaSalesIQTM__Team_Instance__r.name;
        positionAccount.Name=position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Account__r.External_Account_Id__c;
        positionAccount.SIQ_Created_Date__c=position.CreatedDate;
        positionAccount.SIQ_Updated_Date__c=position.LastModifiedDate;
        positionAccount.SIQ_POSITION_CODE__c=position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
        positionAccount.SIQ_EFFECTIVE_START_DATE__c=position.AxtriaSalesIQTM__Effective_Start_Date__c;
        
        positionAccount.SIQ_EFFECTIVE_END_DATE__c=position.AxtriaSalesIQTM__Effective_End_Date__c;
                //positionAccount.Unique_Id__c=position.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Effective_End_Date__c;                                            
               positionAccount.Unique_Id__c=position.id;
                positionAccounts.add(positionAccount);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                   Uniqueset.add(Key);    
                //comments
            }
        }
     
        upsert positionAccounts Unique_Id__c;
       
      
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sjob.Object_Name__c = 'Position Account';
                //sjob.Changes__c
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        //Database.ExecuteBatch(new BatchOutBoundUpdPosGeography(),200);
         Set<String> updMkt = new set<String>();
           for(Custom_Scheduler__c cs:mktList){
             if(cs.Status__c==true && cs.Schedule_date__c!=null){
               if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
               }
             }
            }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position_Account__c> posAccList=[Select Id FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update posAccList;
        }
    }   
}