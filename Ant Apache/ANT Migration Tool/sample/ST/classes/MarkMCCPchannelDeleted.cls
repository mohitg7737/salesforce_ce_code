global class MarkMCCPchannelDeleted implements Database.Batchable<sObject> {
    public String query;

    global MarkMCCPchannelDeleted() {
        this.query = 'select id from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Rec_Status__c != \'Updated\' ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scope) {

        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mct : scope)
        {
            mct.Rec_Status__c ='Deleted';
        }   

        update scope;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}