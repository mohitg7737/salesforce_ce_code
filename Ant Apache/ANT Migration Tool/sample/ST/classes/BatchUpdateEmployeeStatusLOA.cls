global class BatchUpdateEmployeeStatusLOA implements Database.Batchable<sObject>, Database.stateful{
    set<string> employeesToUpdate = new set<string>();
    
    global BatchUpdateEmployeeStatusLOA(set<string> empToUpdate){
        employeesToUpdate.addAll(empToUpdate);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  //keep the batch size low.
        string query = ' Select id, Name, AssociateOID__c,AxtriaSalesIQTM__HR_Termination_Date__c,AxtriaSalesIQTM__HR_Status__c,AxtriaSalesIQTM__Employee_ID__c, '
                +' AxtriaSalesIQTM__Rehire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c, AxtriaSalesIQTM__Current_Territory__c '
                +' from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Employee_ID__c IN : employeesToUpdate ';
        system.debug('inside getQueryLocator :: query: '+query);
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> empToUpdateLOAList){
        set<string> empAssIdsSet = new set<string>();
        set<string> empSfIDSet = new set<string>();
        map<string,string> empAssIdToEmpSfidMapping = new map<string,string>();
        map<string,list<Current_Leave_Data_Feed__c>> empIdToCurrFeedDataMapping = new map<string,list<Current_Leave_Data_Feed__c>>();
        map<string,list<Employee_Status__c>> empIdToEmpStatusMapping = new map<string,list<Employee_Status__c>>();
        map<string, AxtriaSalesIQTM__Employee__c> empIdToEmpRecMapping = new map<string, AxtriaSalesIQTM__Employee__c>();
        map<string,Date> empIdToMinStatusDateMapping = new map<string,Date>();
        
        for(AxtriaSalesIQTM__Employee__c emp : empToUpdateLOAList){
            empAssIdsSet.add(emp.AxtriaSalesIQTM__Employee_ID__c);
            empSfIDSet.add(emp.Id);
            empAssIdToEmpSfidMapping.put(emp.AxtriaSalesIQTM__Employee_ID__c,emp.Id);
            empIdToEmpRecMapping.put(emp.Id,emp);
            
        }
        
        string currQuery = 'SELECT Id, Name, AssociateOID__c, LOA_StartDate__c, LOA_EndDate__c, Paid_Time_Off_Policy__c, Total_Quantity__c '
            +' FROM Current_Leave_Data_Feed__c where AssociateOID__c IN: empAssIdsSet AND isRejected__c = false order by LOA_StartDate__c asc ';
       
        for(Current_Leave_Data_Feed__c curr : Database.query(currQuery)){
            AxtriaSalesIQTM__Employee__c emp = empIdToEmpRecMapping.get(empAssIdToEmpSfidMapping.get(curr.AssociateOID__c));
            Date hireDate = emp.AxtriaSalesIQTM__Original_Hire_Date__c;
            Date rehireDate = emp.AxtriaSalesIQTM__Rehire_Date__c;
            Date terminationDate = emp.AxtriaSalesIQTM__HR_Termination_Date__c;
            String currEmpStatus = emp.AxtriaSalesIQTM__HR_Status__c;
            Boolean isRehireEmp = isRehireEmployee(emp);
            Boolean isNotTerminatedEmp = isEmployeeNotTerminated(emp);
            
            if((isRehireEmp && curr.LOA_StartDate__c >= rehireDate)
                || (isNotTerminatedEmp && curr.LOA_StartDate__c >= hireDate && (terminationDate == null || curr.LOA_EndDate__c <= terminationDate))
            ){
                if(empIdToCurrFeedDataMapping.containsKey(empAssIdToEmpSfidMapping.get(curr.AssociateOID__c))){
                    empIdToCurrFeedDataMapping.get(empAssIdToEmpSfidMapping.get(curr.AssociateOID__c)).add(curr);
                }else{
                    empIdToCurrFeedDataMapping.put(empAssIdToEmpSfidMapping.get(curr.AssociateOID__c), new list<Current_Leave_Data_Feed__c>{curr});
                }
            } 
            
            if(!empIdToMinStatusDateMapping.containsKey(emp.Id)){
                Date minDate = (isRehireEmp)? rehireDate : hireDate;
                empIdToMinStatusDateMapping.put(emp.Id,minDate);
            }
            
        }
        system.debug('---empIdToCurrFeedDataMapping--'+empIdToCurrFeedDataMapping);
       
       string empStatusQuery = ' SELECT Id, Name, Effective_End_Date__c, Effective_Start_Date__c, Employee_Status__c, Employee__c, Position__c, Employee__r.Id,LOA_Reason__c '
                +' FROM Employee_Status__c where Employee__c IN: empSfIDSet order by Effective_Start_Date__c asc ';
       
       // collect the employee status records that will be affected for the employee.
       for(Employee_Status__c empStatus : Database.query(empStatusQuery)){
            system.debug('---empStatus--'+empStatus);
            //Date minLOAStartDate = empIdToCurrFeedDataMapping.get(empStatus.Employee__r.Id)[0].LOA_StartDate__c;
            Date minLOAStartDate = empIdToMinStatusDateMapping.get(empStatus.Employee__r.Id);   //it is either hire date or rehire date.
            
            if((empStatus.Effective_Start_Date__c > minLOAStartDate) || (empStatus.Effective_Start_Date__c <= minLOAStartDate && minLOAStartDate < empStatus.Effective_End_Date__c)){
                if(empIdToEmpStatusMapping.containsKey(empStatus.Employee__r.Id)){
                    empIdToEmpStatusMapping.get(empStatus.Employee__r.Id).add(empStatus);
                }else{
                    empIdToEmpStatusMapping.put(empStatus.Employee__r.Id, new list<Employee_Status__c>{empStatus});
                }
            }
       }
       system.debug('---empIdToEmpStatusMapping--'+empIdToEmpStatusMapping);
       
       //update employee status records for the selected range
       list<Employee_Status__c> updateNewEmpStatusList = new list<Employee_Status__c>();
       list<Employee_Status__c> deleteOldEmpStatusList = new list<Employee_Status__c>();
       for(String empId : empIdToCurrFeedDataMapping.keySet()){         
            
            if(empIdToEmpStatusMapping.containskey(empId) && empIdToEmpStatusMapping.get(empId).size() >0 ){
                boolean removeFirst = false;
            
                Employee_Status__c updateOldRec =  empIdToEmpStatusMapping.get(empId)[0];   
                
                if(updateOldRec.Effective_Start_Date__c < empIdToCurrFeedDataMapping.get(empId)[0].LOA_StartDate__c){
                    removeFirst = true;
                    updateOldRec.Effective_End_Date__c = empIdToCurrFeedDataMapping.get(empId)[0].LOA_StartDate__c.addDays(-1);     
                    system.debug('--updateOldRec--'+updateOldRec);
                    updateNewEmpStatusList.add(updateOldRec);   
                }   
                integer lastIndex = empIdToCurrFeedDataMapping.get(empId).size();
                integer currIndex = 0;
               
               for(currIndex = 0; currIndex < empIdToCurrFeedDataMapping.get(empId).size(); currIndex++){
                    Current_Leave_Data_Feed__c curr = empIdToCurrFeedDataMapping.get(empId)[currIndex];
                    Employee_Status__c newRec       = new Employee_Status__c();
                    newRec.Employee__c              = empId;
                    newRec.Position__c              = updateOldRec.Position__c; 
                    
                    if(currIndex == lastIndex - 1){ // last record
                        newRec.Employee_Status__c       = System.label.LOA; 
                        newRec.Effective_Start_Date__c  = curr.LOA_StartDate__c;
                        newRec.Effective_End_Date__c    = curr.LOA_EndDate__c;
                        newRec.LOA_Reason__c            = curr.Paid_Time_Off_Policy__c;
                        system.debug('--if newRec--'+newRec);
                        updateNewEmpStatusList.add(newRec);                 
                                
                        Date lastRecStartDate   = curr.LOA_EndDate__c.addDays(1);   
                        Employee_Status__c lastEmpStausRec = empIdToEmpStatusMapping.get(empId)[empIdToEmpStatusMapping.get(empId).size() - 1];
                        //get the status of the last record in the old employee status data -> terminated/ retired/ deceased etc.
                        String lastRecStatus    = lastEmpStausRec.Employee_Status__c;                   
                        Date terminationDate    = lastEmpStausRec.Effective_Start_Date__c.addDays(-1);
                        
                        if((lastRecStatus != System.label.Active && lastRecStatus != System.label.LOA) && curr.LOA_EndDate__c < terminationDate){
                                
                            Employee_Status__c newRec1      = new Employee_Status__c();
                            newRec1.Employee__c             = empId;
                    //          newRec1.Position__c             = updateOldRec.Position__c; 
                            newRec1.Effective_Start_Date__c = lastRecStartDate; 
                            newRec1.Employee_Status__c      = system.label.Active;
                            newRec1.Effective_End_Date__c   = empIdToEmpRecMapping.get(empId).AxtriaSalesIQTM__HR_Termination_Date__c;
                            lastRecStartDate                = newRec1.Effective_End_Date__c.addDays(1);
                            
                            system.debug('--if newRec1--'+newRec1);
                            updateNewEmpStatusList.add(newRec1);
                        }else{
                            lastRecStatus = System.label.Active;
                        }   
                        
                        //create the last record
                        Employee_Status__c lastRec      = new Employee_Status__c();
                        lastRec.Employee__c             = empId;
                //          lastRec.Position__c             = updateOldRec.Position__c; 
                        lastRec.Employee_Status__c      = lastRecStatus;
                        lastRec.Effective_Start_Date__c = lastRecStartDate; 
                        lastRec.Effective_End_Date__c   = Date.newinstance(4000,12,31);
                        if(lastRecStatus == System.label.LOA){
                            lastRec.LOA_Reason__c           = lastEmpStausRec.LOA_Reason__c;
                        }
                        system.debug('--lastRec--'+lastRec);
                        updateNewEmpStatusList.add(lastRec); 
                    }
                    else{
                        newRec.Employee_Status__c       = System.label.LOA;
                        newRec.Effective_Start_Date__c  = curr.LOA_StartDate__c;
                        newRec.Effective_End_Date__c    = curr.LOA_EndDate__c;
                        newRec.LOA_Reason__c            = curr.Paid_Time_Off_Policy__c;
                        system.debug('--else newRec--'+newRec);
                        updateNewEmpStatusList.add(newRec);
                        
                        //if there is gap between the LOA records, create an active record in between
                        if(empIdToCurrFeedDataMapping.get(empId)[currIndex+1].LOA_StartDate__c != curr.LOA_EndDate__c.addDays(1)){
                            Employee_Status__c newRec1      = new Employee_Status__c();
                            newRec1.Employee__c             = empId;
            //              newRec1.Position__c             = updateOldRec.Position__c; 
                            newRec1.Employee_Status__c      = System.label.Active;
                            newRec1.Effective_Start_Date__c = curr.LOA_EndDate__c.addDays(1);
                            newRec1.Effective_End_Date__c   = empIdToCurrFeedDataMapping.get(empId)[currIndex+1].LOA_StartDate__c.addDays(-1);
                            system.debug('--else newRec1--'+newRec1);
                            updateNewEmpStatusList.add(newRec1);
                        }
                    }
                }
                if(removeFirst == true){
                    empIdToEmpStatusMapping.get(empId).remove(0);   
                }                       
                deleteOldEmpStatusList.addAll(empIdToEmpStatusMapping.get(empId));
            }
        }
        
        system.debug('--deleteOldEmpStatusList--'+deleteOldEmpStatusList);
        system.debug('--updateNewEmpStatusList--'+updateNewEmpStatusList);
        
        if(deleteOldEmpStatusList.size() > 0){
            delete deleteOldEmpStatusList;
        }
       
        if(updateNewEmpStatusList.size() > 0){
            upsert updateNewEmpStatusList;
        }
            
     }
     
   /*  public boolean isEmployeeActive(AxtriaSalesIQTM__Employee__c empRec){
        if((empRec.Employee_Status__c != system.label.Terminated) && (empRec.AxtriaSalesIQTM__HR_Termination_Date__c == null) 
                && (empRec.AxtriaSalesIQTM__Rehire_Date__c == null ))
        {
            return true;
        }       
        return false;
    }*/
    
    public boolean isRehireEmployee(AxtriaSalesIQTM__Employee__c empRec){
        if((empRec.AxtriaSalesIQTM__HR_Status__c == system.label.Active || empRec.AxtriaSalesIQTM__HR_Status__c == system.label.LOA)
            && empRec.AxtriaSalesIQTM__Rehire_Date__c != null 
            &&  empRec.AxtriaSalesIQTM__HR_Termination_Date__c < empRec.AxtriaSalesIQTM__Rehire_Date__c){
                return true;
        }
        return false;
    }
    
    public boolean isEmployeeNotTerminated(AxtriaSalesIQTM__Employee__c empRec){
        if(//(empRec.AxtriaSalesIQTM__HR_Status__c == system.label.Active || empRec.AxtriaSalesIQTM__HR_Status__c == system.label.LOA) &&
             (empRec.AxtriaSalesIQTM__Rehire_Date__c == null ) 
            && ( empRec.AxtriaSalesIQTM__HR_Termination_Date__c == null || empRec.AxtriaSalesIQTM__HR_Termination_Date__c > system.today())
        ){
            return true;
        }       
        return false;
    }
    
    global void finish(Database.BatchableContext BC){  
        list<Current_Leave_Data_Feed__c> currList = [select id from Current_Leave_Data_Feed__c limit 10];
        if(currList.size() > 0){
            Database.executeBatch(new BatchDeletePrevDayLOAFeed(), 200);    
        }
        system.debug('--finished---');
    }
    
}