global class BatchOutBoundEmployee implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;}                                              

    global BatchOutBoundEmployee() {
        Countrymap = new map<String,String>();
          Uniqueset = new set<String>(); 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound Employee Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);
        
     for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }    

        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'OutBound Employee Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
         sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //this.query = query;
        query = 'Select id,AxtriaSalesIQTM__City__c,AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c,AddressCity__c,AddressPostalCode__c,AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Postal_code__c,AddressStateCode__c,AxtriaSalesIQTM__Street__c,AddressLine1__c,'+
                'AddressCountry__c,CreatedDate,AxtriaSalesIQTM__Email__c,AxtriaSalesIQTM__Employee_ID__c,Employee_Name__c,AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Last_Name__c,AxtriaSalesIQTM__Country_Name__r.Name,AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, '+
                'AxtriaSalesIQTM__Job_Title__c,AxtriaSalesIQTM__Field_Force__c,AxtriaSalesIQTM__Manager__r.name,AssignmentStatusValue__c,LastModifiedDate from AxtriaSalesIQTM__Employee__c';
                //SIQ_Marketing_Code__c,
        if(lastjobDate!=null){
            query = query + ' Where LastModifiedDate  >=:  lastjobDate '; 
        }
        System.debug('query'+ query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundEmployee());
    }
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> scope) {
        System.debug('==========Scope Size::'+scope.size());
        List<SIQ_Employee_O__c>EmpList = new list<SIQ_Employee_O__c>();
        for(AxtriaSalesIQTM__Employee__c emp : scope){
         String key=emp.id; 
           if(!Uniqueset.contains(Key)){
            SIQ_Employee_O__c obj = new SIQ_Employee_O__c();
            obj.SIQ_BillingCity__c = emp.AddressCity__c;
            obj.SIQ_BillingCountry__c = emp.AddressCountry__c;
            obj.SIQ_BillingPostalCode__c = emp.AddressPostalCode__c;
            obj.SIQ_BillingState__c = emp.AddressStateCode__c;
            obj.SIQ_BillingStreet__c= emp.AddressLine1__c;
            //obj.SIQ_Country_Code__c = countrymap.get(emp.AxtriaSalesIQTM__Country_Name__r.Name);
            obj.SIQ_Country_Code__c =emp.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c;
            obj.SIQ_Created_Date__c = emp.CreatedDate;
            obj.SIQ_Email__c = emp.AxtriaSalesIQTM__Email__c;
            //obj.SIQ_Employee_Id__c = String.valueOf(emp.EmployeeID__c); //AxtriaSalesIQTM__Employee_ID__c
            obj.SIQ_Employee_Id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            obj.SIQ_Employee_Name__c = emp.Employee_Name__c;
            obj.Name=emp.Employee_Name__c;  
                                                            
                                            
            obj.SIQ_First_Name__c = emp.AxtriaSalesIQTM__FirstName__c;
            obj.SIQ_Job_Title__c = emp.AxtriaSalesIQTM__Job_Title__c;
            obj.SIQ_Last_Name__c = emp.AxtriaSalesIQTM__Last_Name__c;
           // obj.SIQ_Marketing_Code__c = countrymap.get(emp.AxtriaSalesIQTM__Country_Name__r.Name);
            obj.SIQ_Marketing_Code__c =emp.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
            obj.SIQ_PRID__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            obj.SIQ_Reporting_Manager__c = emp.AxtriaSalesIQTM__Manager__r.name;
            obj.SIQ_Status__c = emp.AssignmentStatusValue__c;
            obj.SIQ_Updated_Date__c = emp.LastModifiedDate;
            obj.Unique_Id__c = emp.id;   
            Uniqueset.add(Key);                                                                                 
            EmpList.add(obj);
            recordsProcessed++;
            //comments
}
        }
        upsert Emplist Unique_Id__c;
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
        //Database.ExecuteBatch(new BatchOutBoundPosEmp(),200);
    }
}