global class changeProductMetricsStatus implements Database.Batchable<sObject> {
    

    List<String> teamInstanceSelected;
    String queryString;
    
    global changeProductMetricsStatus(List<String> teamInstanceSelectedTemp)
    { 

       queryString = 'select id, SIQ_Status__c from SIQ_Product_Metrics_vod_O__c ';// where Team_Instance__c in :allTeamInstances

        teamInstanceSelected = teamInstanceSelectedTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_Product_Metrics_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_Product_Metrics_vod_O__c prodmet : scopePacpProRecs)
        {
            prodmet.SIQ_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        Batch_Integration_ProductMetrics_EU u1 = new Batch_Integration_ProductMetrics_EU(teamInstanceSelected);
        Database.executeBatch(u1, 2000);
    }
}