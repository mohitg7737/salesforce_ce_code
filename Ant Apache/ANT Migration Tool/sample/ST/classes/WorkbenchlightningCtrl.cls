global class WorkbenchlightningCtrl 
{
    
    public String soqlsort {get;set;}
    public boolean sortflag {get;set;}
    public list<CR_Employee_Feed__c> cremplist{get;set;}
    public list<CR_Employee_Feed__c> SortCREmpList = new list<CR_Employee_Feed__c>();
    public static map<string,object> mapRemovedIdToflag;
    public static string test{get;set;}
    public String countryID {get;set;}
    public Country__c Country {get;set;}
    Id profileId = userinfo.getProfileId();
    Map<string,string> successErrorMap;
    
    
    @AuraEnabled
    public static String getCountryAttributes(String countryId){
        List<AxtriaSalesIQTM__Country__c> countryList = SalesIqUtility.getCountryByCountryId(new list<string>{countryId});
        return countryList[0].name+','+countryList[0].AxtriaSalesIQTM__Country_Flag__c;
    }

    
    @AuraEnabled 
    public static map<String,list<String>> getProfileNameMap(){
    	
    	//String namespace = SObjectDescribe.namespaceprefix;

        map<String,list<String>> profileNameMap = new map<String,list<String>>();
        profileNameMap.put('HO',SalesIQGlobalConstants.HO_PROFILE);
        profileNameMap.put('HR',SalesIQGlobalConstants.HR_PROFILE);
        profileNameMap.put('DM',SalesIQGlobalConstants.DM_PROFILE);
        profileNameMap.put('RM',SalesIQGlobalConstants.RM_PROFILE);
        profileNameMap.put('Rep',SalesIQGlobalConstants.REP_PROFILE);
        profileNameMap.put('System Administrator',new list<String>{'System Administrator'});

        return profileNameMap;
        
    }
    
    
    @AuraEnabled
    public static CountWrapper GetCount(string countryId)
    {    
        CountWrapper wrapper=new CountWrapper();
        mapRemovedIdToflag=new map<string,object>();
    	system.debug('countryId++ inside count'+countryId);
        wrapper.terminate_Employee  = 0;
        wrapper.leave_of_Absence    = 0; 
        wrapper.promote_Employee    = 0; 
        wrapper.EmployeeNewHire     = 0;
        wrapper.TransferEmployee    = 0;
        wrapper.Employeedemotion    = 0;
        wrapper.Transfer_to_HO      = 0;
        wrapper.TransfertoField     = 0;
        wrapper.TransferoutOfSales  = 0;
        wrapper.allEvents           = 0;
        wrapper.rehireCount         = 0;
        
        list<CR_Employee_Feed__c> crList = [SELECT Id, Event_Name__c,Employee__r.AxtriaSalesIQTM__Country_Name__c FROM CR_Employee_Feed__c WHERE IsRemoved__c = false and Employee__r.AxtriaSalesIQTM__Country_Name__c =: countryId];
        wrapper.allEvents = crList.size();
        
        for(CR_Employee_Feed__c cr : crList){
            if(cr.Event_Name__c == system.label.Terminate_Employee){
                wrapper.terminate_Employee++;
            }
            if(cr.Event_Name__c == system.label.Transfer_to_HO){
                wrapper.Transfer_to_HO++;
            }
            if(cr.Event_Name__c == system.label.Leave_of_Absence){
                wrapper.leave_of_Absence++;
            }
            if(cr.Event_Name__c == system.label.Promote_Employee){
                wrapper.promote_Employee++;
            }
            if(cr.Event_Name__c == system.label.Employee_NewHire){
                wrapper.EmployeeNewHire++;
            }
            if(cr.Event_Name__c == system.label.Transfer_Employee){
                wrapper.TransferEmployee++ ;
            }
            if(cr.Event_Name__c == system.label.Transfer_to_Field){
                wrapper.TransfertoField++;
            }
            if(cr.Event_Name__c == system.label.Employee_Rehire){
                wrapper.rehireCount++;
            }
            if(cr.Event_Name__c == system.label.Transfer_out_of_Sales_team){
            	wrapper.TransferoutOfSales++;
            }
        }
        return wrapper;
    }
    
    public void methodOne(){}
    
    @AuraEnabled
    public static string isStatusMarkCompleted(string crfeedid){

        System.debug('crfeedid ::'+crfeedid);
        
        list<CR_Employee_Feed__c> updatecreList=new list<CR_Employee_Feed__c>();
        list<CR_Employee_Feed__c> existingcreList = [select status__c,Employee__c,Employee__r.AxtriaSalesIQTM__Current_Territory__c from CR_Employee_Feed__c where id =:crfeedid];
        string posId = '';
        if(existingcreList.size()!=0){
        	posId = existingcreList[0].Employee__r.AxtriaSalesIQTM__Current_Territory__c;
        }
        
              
        for(CR_Employee_Feed__c cr : existingcreList)
        {   
            cr.status__c='Completed Action';
            cr.Position__c = posId;
            
            updatecreList.add(cr); 
        }
        System.debug('+++++Crelist+++++ ' + updatecreList);
        Update updatecreList;
            
        return '';
    } 
    
    @AuraEnabled
    public static string isRemovedEventfromWB(string rowID)
    {  
        list<string>feedIdList=new list<string>();
        feedIdList=rowID.split(',');
        if(rowID!=null&& rowID!=''){
            list<CR_Employee_Feed__c> updatecreList=new list<CR_Employee_Feed__c>();
            list<CR_Employee_Feed__c> existingcreList = [select IsRemoved__c from CR_Employee_Feed__c where id in:feedIdList];
            for(CR_Employee_Feed__c cr : existingcreList)
            {
                cr.IsRemoved__c=true;
                updatecreList.add(cr); 
            }
            
            System.debug('+++++Crelist+++++ ' + updatecreList);
            Update updatecreList;
        }
     /*   else{

            system.debug('error msg+++++++++++++++');
        
            string message = 'Please check atleast one checkbox.';
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        } */
        return null;
    }
    
    Date d=system.today();
    String y=String.valueOf(d);
  
    @AuraEnabled
    public static List<wrapperReport> Feed(string eventType, String countryID)
    {    
        list<wrapperReport> wrapperlist = new list<wrapperReport>();
        list<CR_Employee_Feed__c> cremplistNew= new list<CR_Employee_Feed__c>();
        cremplistNew = [Select id,Position__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c
                        FROM CR_Employee_Feed__c 
                        where  IsRemoved__c=false and Employee__r.AxtriaSalesIQTM__Country__c =: countryID order by id];
      
        set<string> empData= new set<string>();
        map<string,CR_Employee_Feed__c> mapEmp= new map<string,CR_Employee_Feed__c>();
        
        //Get all employees from workday and their respective object
        for(CR_Employee_Feed__c emp : cremplistNew){
            empData.add(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c);
            mapEmp.put(emp.Employee__r.AxtriaSalesIQTM__Employee_ID__c, emp);
        }
        //Get data from position employee
        map<string, AxtriaSalesIQTM__Position__c> mapPos= new map<string, AxtriaSalesIQTM__Position__c>();
       
        for(AxtriaSalesIQTM__Position__c pos : [select id, AxtriaSalesIQTM__Position_Type__c,
                                                AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                                AxtriaSalesIQTM__Parent_Position__c
                                                from AxtriaSalesIQTM__Position__c
                                                where AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c in: empData and AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__c =: countryID])
        {
        	
            mapPos.put(pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, pos);
            system.debug('mapPos++'+mapPos);
        }
        
        list<CR_Employee_Feed__c> lstFeed= new list<CR_Employee_Feed__c>();
        for(string empId:mapPos.keyset())
        {
        	system.debug('empId++'+empId);
            AxtriaSalesIQTM__Position__c pos=mapPos.get(empId);
            CR_Employee_Feed__c feed=mapEmp.get(empId);
            feed.Position__c =pos.id;
            lstFeed.add(feed);
            system.debug('lstFeed++'+lstFeed);
        }
        update lstFeed;
        
        ////
        cremplistNew= new list<CR_Employee_Feed__c>();
        system.debug('eventType---' + eventType);
        if(string.isNotBlank(eventType)){
        cremplistNew = [Select id,
                            ChangeRequest__r.Name, IsRemoved__c,Request_Date1__c, 
                            Position__r.AxtriaSalesIQTM__Position_ID__c, Position__r.Name,
                            Position__r.id, Employee__r.Name, Employee__r.Id,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                            Event_Name__c, Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c, 
                            Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c, 
                            Employee__r.Hire_date__c, Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c,Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c 
                            FROM CR_Employee_Feed__c                          
                            where  IsRemoved__c=false and Event_Name__c =:eventType and Employee__r.AxtriaSalesIQTM__Country_Name__c =: countryID
                            order by id];
        
        }
        else{
        cremplistNew = [Select id,
                            ChangeRequest__r.Name, IsRemoved__c, Request_Date1__c, 
                            Position__r.AxtriaSalesIQTM__Position_ID__c,Position__r.Name,
                            Position__r.id, Employee__r.Name, Employee__r.Id,
                            Training_Completion_Date1__c, Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                            Position__r.AxtriaSalesIQTM__Position_Type__c,Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                            Event_Name__c, Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, Field_Force__c, 
                            Employee__r.AxtriaSalesIQTM__Manager__r.Name, Status__c, 
                            Employee__r.Hire_date__c, Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c,Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c 
                            FROM CR_Employee_Feed__c                          
                            where IsRemoved__c=false and Employee__r.AxtriaSalesIQTM__Country_Name__c =: countryID
                            order by id];
        }
        wrapperReport wrap;
        
        for(CR_Employee_Feed__c cref : cremplistNew)
        {
            String ids = string.ValueOf(cref.id);
            String Remove = string.valueOf(cref.IsRemoved__c);
            Datetime tempdt     = cref.Request_Date1__c;    
            String Request_Date1 = tempdt != null ? tempdt.formatGmt('MM/dd/yyyy') : '';
            String position_id = string.ValueOf(cref.Position__r.id);
            String employee_id = string.ValueOf(cref.Employee__r.Id);
            //String hireDate = String.Valueof(cref.Employee__r.Hire_date__c);
            //String termDate = String.Valueof(cref.Employee__r.AxtriaSalesIQTM__HR_Termination_Date__c);
            String hireDate = '';
            tempdt = cref.Employee__r.Hire_date__c;
            if(tempdt != null)
                hireDate = String.Valueof(tempdt.formatGmt('MM/dd/yyyy'));
            
            tempdt = cref.Employee__r.AxtriaSalesIQTM__HR_Termination_date__c;
            String termDate = '';
            if(tempdt != null)
                termDate = String.Valueof(tempdt.formatGmt('MM/dd/yyyy'));
            
            String teamInstanceEndDate = String.valueOf(cref.Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c);
            // Datetime tempdted     = cref.Training_Completion_Date1__c;    
            // String Training_Completion_Date1 = tempdted != null ? tempdted.formatGmt('MM/dd/yyyy') : '';
            
            wrap = new wrapperReport(
                                        ids,
                                        cref.ChangeRequest__r.Name, 
                                        Remove,
                                        Request_Date1, 
                                        cref.Position__r.AxtriaSalesIQTM__Position_ID__c, 
                                        cref.Position__r.Name,
                                        Position_id, 
                                        cref.Employee__r.Name, 
                                        Employee_id,
                                        cref.Training_Completion_Date1__c, 
                                        cref.Position_Employee__r.AxtriaSalesIQTM__Assignment_Type__c, 
                                        cref.Position__r.AxtriaSalesIQTM__Position_Type__c, 
                                        cref.Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                        cref.Event_Name__c,
                                        cref.Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, 
                                        cref.Field_Force__c, 
                                        cref.Employee__r.AxtriaSalesIQTM__Manager__r.Name, 
                                        cref.Status__c,
                                        hireDate,
                                        termDate,
                                        teamInstanceEndDate 
                                    );
            
            wrapperlist.add(wrap);
            system.debug('==wrapperlist=='+wrapperlist);
        }
        return  wrapperlist;
    }
    
       
    global class wrapperReport
    {
         @AuraEnabled
        global String ids {get;set;}
         @AuraEnabled
        global String Name {get;set;}
         @AuraEnabled
        global String IsRemoved {get;set;}
         @AuraEnabled
        global String Request_Date1 {get;set;}
         @AuraEnabled
        global String Territory_ID {get;set;}
         @AuraEnabled
        global String Position_Name {get;set;}
         @AuraEnabled
        global String Position_id {get;set;}
         @AuraEnabled
        global String Employee_Name {get;set;}
         @AuraEnabled
        global String Employee_record_Id {get;set;}
         @AuraEnabled
        global String Training_Completion_Date1 {get;set;}
         @AuraEnabled       
        global String Assignment_Type {get;set;}
         @AuraEnabled
        global String Position_Type {get;set;}
         @AuraEnabled
        global String Employee_ID {get;set;}
         @AuraEnabled
        global String Event_Name {get;set;}
         @AuraEnabled
        global String Parent_Position_Name {get;set;}
         @AuraEnabled
        global String Field_Force {get;set;}
         @AuraEnabled
        global String Employee_Division {get;set;}
         @AuraEnabled
        global String Manager_Name {get;set;}
         @AuraEnabled
        global String Status {get;set;}
        @AuraEnabled
        global String Hire_Date {get;set;}
        @AuraEnabled
        global String Term_Date {get;set;}

        // CODE ADDED TO GET TEAMINSTANCE DATE FOR ASSIGNMENTS -LAGNIKA
        @AuraEnabled
        global String Team_Instance_End_Date {get;set;}
        
        // Constructor
        public  wrapperReport(   String ids, 
                                 String Name, 
                                 String IsRemoved, 
                                 String Request_Date1, 
                                 String Territory_ID,
                                 String Position_Name, 
                                 String Position_id,
                                 String Employee_Name,
                                 String Employee_record_Id, 
                                 String Training_Completion_Date1, 
                                 String Assignment_Type, 
                                 String Position_Type, 
                                 String Employee_ID, 
                                 String Event_Name, 
                                 String Parent_Position_Name, 
                                 String Field_Force, 
                                 String Manager_Name, 
                                 String Status,
                                 String Hire_Date,
                                 String Term_Date,
                                 String Team_Instance_End_Date 
                              )
        {
             this.ids                         = ids; 
             this.Name                        = Name;
             this.IsRemoved                   = IsRemoved;
             this.Request_Date1               = Request_Date1;
             this.Territory_ID                = Territory_ID;
             this.Position_Name               = Position_Name;
             this.Position_id                 = Position_id;
             this.Employee_Name               = Employee_Name;
             this.Employee_record_Id          = Employee_record_Id;
             this.Training_Completion_Date1   = Training_Completion_Date1;
             this.Assignment_Type             = Assignment_Type;
             this.Position_Type               = Position_Type;
             this.Employee_ID                 = Employee_ID;
             this.Event_Name                  = Event_Name;
             this.Parent_Position_Name        = Parent_Position_Name;
             this.Field_Force                 = Field_Force;
             this.Employee_Division           = Employee_Division;
             this.Manager_Name                = Manager_Name;
             this.Status                      = Status;
             this.Hire_Date					  = Hire_Date;
             this.Term_Date					  = Term_Date;
             this.Team_Instance_End_Date = Team_Instance_End_Date;
        }
    }
}