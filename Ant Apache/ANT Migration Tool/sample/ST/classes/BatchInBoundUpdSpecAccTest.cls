@isTest
private class BatchInBoundUpdSpecAccTest {
     @testSetup 
    static void setup() {
        Account acc = new Account();
        acc.Marketing_Code__c='ES';
        acc.Name='test';
        acc.AxtriaSalesIQTM__Speciality1__c='s1;s2;s3;s4;';
       
        insert acc;
        
       

    }
    static testmethod void test() {        
        Test.startTest();
       
        BatchUpdateSpeciality usa = new BatchUpdateSpeciality();
        Id batchId = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}