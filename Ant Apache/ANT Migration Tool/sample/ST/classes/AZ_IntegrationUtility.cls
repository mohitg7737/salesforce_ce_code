global class AZ_IntegrationUtility implements Database.Batchable<sObject>{
    
    List<SIQ_MC_Cycle_Plan_Product_vod_O__c> mcCyclePlanProduct;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpProRecs;
    List<String> allChannels ;
    String queryString;
    string teamInstanceSelected;
    List<String> allString;

    global AZ_IntegrationUtility(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        queryString = 'select id, Segment__c, Final_TCF__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , Product1__r.Product__r.External_ID__c, P1__c, P2__c, P3__c, P4__c, Product1__r.Product__r.Name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and  AxtriaSalesIQTM__isIncludedCallPlan__c = true and P1__c != null';
        
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        allString = new List<String>();
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Product_vod( List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        system.debug('++++++++++++ mcCyclePlanProduct'+ mcCyclePlanProduct);
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        pacpProRecs = scopePacpProRecs;
        Set<String> allRecs = new Set<String>();

        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : pacpProRecs)
        {
            string teamPos = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlan         =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlanTarget   =   teamPos + '_' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
            
            String accNumPos = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            Integer f2fCalls = Integer.valueOf(pacpPro.Final_TCF__c);
            String channel = 'F2F';

            List<AxtriaARSnT__Product_Catalog__c> pc = [select id, Name, AxtriaARSnT__External_ID__c from AxtriaARSnT__Product_Catalog__c where Name = :pacpPro.p1__c];


            String productExternalID=pc[0].AxtriaARSnT__External_ID__c;
            String productName = pacpPro.P1__c;

            SIQ_MC_Cycle_Plan_Product_vod_O__c mcpp = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
                        
            string cycleChannel;
            
            cycleChannel      =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channel + '-Call2_vod__c';
            
            mcpp.SIQ_Cycle_Plan_Channel_vod__c = teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
            
            if(channel == 'F2F')
            {
                mcpp.SIQ_Product_Activity_Goal_vod__c = Decimal.valueof(f2fCalls);    
            }
           
            
            mcpp.SIQ_Cycle_Product_vod__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '__' + channel + '__' +productExternalID + '_'+ productName;


            mcpp.SIQ_Product_Activity_Max_vod__c = mcpp.SIQ_Product_Activity_Goal_vod__c;
            
            mcpp.SIQ_External_Id_vod__c = mcpp.SIQ_Cycle_Plan_Channel_vod__c + productExternalID;
            mcpp.SIQ_Segment_AZ__c = pacpPro.Segment__c;
            
            mcpp.External_ID_Axtria__c = mcpp.SIQ_External_Id_vod__c;
            mcpp.Rec_Status__c = 'Updated';
            if(!allRecs.contains(mcpp.SIQ_External_Id_vod__c))
            {
                allRecs.add(mcpp.SIQ_External_Id_vod__c);
                mcCyclePlanProduct.add(mcpp);
            }

            
        }
           

        
        system.debug('+++++++++ '+ mcCyclePlanProduct);
        system.debug('+++++++++++++'+mcCyclePlanProduct.size());
        
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        //List<String> allString = new List<String>();
        create_MC_Cycle_Plan_Product_vod(scopePacpProRecs);
        
        insert mcCyclePlanProduct;
        //insert newTempObj;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new MarkMCCPproductDeleted());
    }
}