global class Call_Plan_Data_to_PACP implements Database.Batchable<sObject>  {
    
    global string queri;
    global string teamID;
    public String selectedTeamInstance;
    //global string teamInstance;
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Account__c> accTI=new list<AxtriaSalesIQTM__Team_Instance_Account__c>();
    
    
    global Call_Plan_Data_to_PACP(String teamInstance){
      system.debug('++++teamInstance' );
        selectedTeamInstance=teamInstance;
        
        queri = 'Select Id,CurrencyIsoCode,Name,Cycle__c, Account_ID__c, Team_ID__c, Territory_ID__c, Segment__c, Objective__c, Product_ID__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Position__c, Position_Account__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM Call_Plan__c where Cycle__c= \'' + teamInstance + '\'';
    } //Corresponding_HCP__c,Gain_Loss_Position__c,
    
    global Database.Querylocator start(Database.BatchableContext bc){
        system.debug('*************************Checkpoint3');
        try{
        return Database.getQueryLocator(queri);
        }
        catch(Exception e){
        System.debug(e.getMessage()); 
        return Database.getQueryLocator(queri);
        }
         
    } 
    
    global void execute (Database.BatchableContext BC, List<Call_Plan__c>callplnlist){
        
        system.debug('*************************Checkpoint4');
        list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;
        
        Set<string> accountNumberSet = new Set<string>();
        Set<string> posIDSet = new Set<string>();
        Set<string> brandSet = new Set<string>();
        for(Call_Plan__c cprcd : callplnlist){
            if(!string.IsBlank(cprcd.Account__c)){            //Accounts Set
                accountNumberSet.add(cprcd.Account__c);
            }
            if(!string.IsBlank(cprcd.Position__c)){            //Pos Set
                posIDSet.add(cprcd.Position__c);
            }
            if(!string.IsBlank(cprcd.Product_ID__c)){            //brand Set
                brandSet.add(cprcd.Product_ID__c);
            }
        }
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpListToCheck = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        pacpListToCheck = [Select id,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,Party_ID__c,P1__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.name = :selectedTeamInstance and AxtriaSalesIQTM__Account__c in :accountNumberSet and AxtriaSalesIQTM__Position__c in :posIDSet and P1__c in :brandSet];// limit 30000
        Set<String> pacpSet = new Set<String>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :pacpListToCheck){
            pacpSet.add(String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c)+String.valueof(pacp.AxtriaSalesIQTM__Position__c)+String.valueof(pacp.Party_ID__c)+String.valueof(pacp.P1__c));
        }
        
        system.debug('before callplnlist ' + callplnlist);
       
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
              for(Call_Plan__c cprcd : callplnlist){
                
                AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc= new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                
                if(cprcd.Account__c != null && cprcd.position__c != null  &&  cprcd.Position_Team_Instance__c != null && cprcd.Team_Instance__c != null && cprcd.Position_Account__c != null && cprcd.Product_ID__c != null && cprcd.Product_ID__c != ''){
                    posAcc.AxtriaSalesIQTM__Account__c  =  cprcd.Account__c;
                    posAcc.AxtriaSalesIQTM__Position__c  =  cprcd.Position__c;
                    posAcc.AxtriaSalesIQTM__Position_Team_Instance__c  =  cprcd.Position_Team_Instance__c;
                    posAcc.AxtriaSalesIQTM__Team_Instance__c  =  cprcd.Team_Instance__c;
                    posAcc.Party_ID__c  =  cprcd.Position_Account__c;
                    
                    posAcc.AxtriaSalesIQTM__isincludedCallPlan__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isAccountTarget__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__lastApprovedTarget__c  =  cprcd.Target__c;
                    
                    posAcc.CurrencyIsoCode  =  cprcd.CurrencyIsoCode;
                    
                    
                    posAcc.Previous_Calls__c = cprcd.Previous_Cycle_Calls__c;
                    posAcc.P1__c = cprcd.Product_ID__c;
                    posAcc.Adoption__c = cprcd.Adoption__c;
                    posAcc.Potential__c = cprcd.Potential__c;
                    if(cprcd.Objective__c!=null){
                        posAcc.Final_TCF__c = Integer.valueOf(cprcd.Objective__c);
                        posAcc.Final_TCF_Approved__c = Integer.valueOf(cprcd.Objective__c);
                        posAcc.Final_TCF_Original__c = Integer.valueOf(cprcd.Objective__c);
                    }
                    posAcc.Segment__c = cprcd.Segment__c;
                    posAcc.Segment_Approved__c = cprcd.Segment__c;
                    posAcc.Segment_Original__c = cprcd.Segment__c;
                    
                    /*posAcc.lastApprovedTarget__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget_Approved__c  =  cprcd.Target__c;
                    posAcc.AxtriaSalesIQTM__isTarget_Updated__c  =  cprcd.Target__c;*/
                    
                    /*posAcc.AxtriaSalesIQTM__Picklist1_Updated__c  =  cprcd.Picklist1_Updated__c;
                    posAcc.AxtriaSalesIQTM__Change_Status__c  =  cprcd.Change_Status__c;
                    posAcc.AxtriaSalesIQTM__Checkbox1__c  =  cprcd.Checkbox1__c;
                    posAcc.AxtriaSalesIQTM__Metric1__c  =  cprcd.Metric1__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric__c  =  cprcd.Picklist1_Metric__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric_Approved__c  =  cprcd.Picklist1_Metric_Approved__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Metric_Updated__c  =  cprcd.Picklist1_Metric_Updated__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment__c  =  cprcd.Picklist1_Segment__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment_Approved__c  =  cprcd.Picklist1_Segment_Approved__c;
                    posAcc.AxtriaSalesIQTM__Picklist1_Segment_Updated__c  =  cprcd.Picklist1_Segment_Updated__c;
                    posAcc.AxtriaSalesIQTM__Segment1__c  =  cprcd.Segment1__c;*/
                    
                    
                    if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.Party_ID__c)+String.valueof(posAcc.P1__c))){
                        posAccListUpdate.add(posAcc);
                    }
              }
                
              else{
                tempErrorObj = new Error_object__c();
                  tempErrorObj.Record_Id__c = cprcd.Id;
                  tempErrorObj.Object_Name__c = 'Call_Plan__c';
                  tempErrorObj.Comments__c = 'Problem in Account,Position,PTI,Team Instance,Position Account lookups or Product ID. This record is not inserted in PACP original';
                  tempErrorObj.Team_Instance_Name__c = cprcd.Cycle__c;
                  errorList.add(tempErrorObj);
                
              }
            }
          if(errorList.size() > 0){
                insert errorList;
          }
            upsert posAccListUpdate;
            system.debug('============upsert done===================');
          
      }
    
          global void finish(Database.BatchableContext BC){
            
       }

}