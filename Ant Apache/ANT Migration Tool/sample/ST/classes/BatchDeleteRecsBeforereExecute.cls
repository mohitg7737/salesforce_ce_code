global class BatchDeleteRecsBeforereExecute implements Database.Batchable<sObject> {
    public String query;
    public String ruleId;
    public String whereClause;

    global BatchDeleteRecsBeforereExecute(String ruleId, String whereClause) 
    {       
        this.ruleId = ruleId;
        this.whereClause = whereClause;
        this.query = 'select id from Account_Compute_Final__c where Measure_Master__c = \''+ruleId+'\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) 
    {
        delete scope;
    }

    global void finish(Database.BatchableContext BC) 
    {
        BatchDeleteRecsBeforereExecuteACFcopy batchExecute = new BatchDeleteRecsBeforereExecuteACFcopy(ruleId, WhereClause );
        Database.executeBatch(batchExecute, 2000);
    }
}