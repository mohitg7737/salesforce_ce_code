/* Claass for papulating Shared Zips*/ 

public with sharing class updatePosGeoAZ {
 
 public static void updatePosGeo(ID ti){
    Map<String, Set<String>> mapPosGeo = new Map<String, Set<String>>();
        List<AxtriaSalesIQTM__Position_Geography__c> posGeo = new List<AxtriaSalesIQTM__Position_Geography__c>([select AxtriaSalesIQTM__Geography__r.name , AxtriaSalesIQTM__Position__r.name, AxtriaSalesIQTM__SharedWith__c,AxtriaSalesIQTM__isshared__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__c = :ti]);
        for(AxtriaSalesIQTM__Position_Geography__c pg : posGeo ){
            
            String key = pg.AxtriaSalesIQTM__Geography__r.name;
            if(mapPosGeo.containsKey(key)){
                mapPosGeo.get(key).add(pg.AxtriaSalesIQTM__Position__r.name);
            }
            else{
                mapPosGeo.put(key,new Set<String>{pg.AxtriaSalesIQTM__Position__r.name});
            }   
        }
        
        Database.executeBatch(new batchUpdatePosGeoAZ(JSON.serialize(mapPosgeo), ti));
 }   
}