public with sharing class Integration_Product_vod {

	public List<Staging_Product_vod__c> allProducts;
	public List<Product_Catalog__c> allSalesIQproducts;

    public Integration_Product_vod(List<String> allTeamInstances) 
    {
    	delete [select id from Staging_Product_vod__c where Team_Instance__c in :allTeamInstances];

    	allProducts = new List<Staging_Product_vod__c>();    
    	allSalesIQproducts = new List<Product_Catalog__c>();

    	allSalesIQproducts = [select Name, Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c , External_ID__c from Product_Catalog__c where Team_Instance__c in :allTeamInstances];
    }

    public void createProdTable()
    {
    	allProducts = new List<Staging_Product_vod__c>();

    	for(Product_Catalog__c pc : allSalesIQproducts)
    	{
    		Staging_Product_vod__c spv = new Staging_Product_vod__c();

    		spv.Name = pc.Name + '_' + pc.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
    		spv.Veeva_External_ID__c = pc.External_ID__c;
    		spv.Product_Type__c = 'Detail';
    		spv.Country_Code__c = pc.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
    		spv.Team_Instance__c = pc.Team_Instance__c;

    		allProducts.add(spv);
    	}

    	insert allProducts;
    }
}