global class UpdateSiqAccounts implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     public map<String, String> mapCountryCode ;
     public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
     public list<Account>Acclist {set;get;}
     public set<String>Acc_Id {get;set;}
     
     global UpdateSiqAccounts(){
        mapCountryCode = new map<String, String>();
        countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        for(AxtriaSalesIQTM__Country__c c : countrylist){
            if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
                mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
            }
        }
        
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Accounts Delta' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'Accounts Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        //sJob.CreatedDate = System.today();
    
        insert sJob;
        batchID = sJob.Id;
        
        recordsProcessed =0;
        
        query = 'SELECT Name,CurrencyIsoCode,OwnerId,CreatedById,SIQ_Marketing_Country__c, ' +
        'SIQ_Billing_Country__c, SIQ_Postal_Code_in_Customer_Address__c, SIQ_Billing_State_Province__c, ' +
        'SIQ_Billing_Address__c, SIQ_Country__c, SIQ_Department_ID__c, SIQ_Department_Name__c, SIQ_External_Organization_Type__c, ' +
        'SIQ_External_Account_Id__c, SIQ_External_Account_Number__c, SIQ_External_Parent_Account_Id__c, ' +
        'SIQ_First_Name__c, SIQ_Last_Modified_Date__c, SIQ_Last_Name__c, SIQ_Latitude__c, SIQ_Longitude__c, ' +
        'SIQ_Middle_Name__c, SIQ_Micro_Brick__c, SIQ_Parent_Account_Number__c, SIQ_Primary_Speciality_Id__c, ' +
        'SIQ_Primary_Speciality_Name__c, SIQ_Profile_Consent__c, SIQ_Publish_Date_Active__c, SIQ_Publish_Date_Address__c, ' +
        'SIQ_Publish_Date_Specialty__c, SIQ_Publish_Date_Type__c, SIQ_Publish_Event_Active__c, SIQ_Publish_Event_Address__c, ' +
        'SIQ_Publish_Event_CustomerType__c, SIQ_Publish_Event_Specialty__c, SIQ_Right_to_be_forgotten__c, SIQ_Secondary_Speciality_Id__c, ' +
        'SIQ_Secondary_Speciality_Name__c, SIQ_Segment1__c, SIQ_Segment_10__c, SIQ_Segment_2__c, SIQ_Segment_3__c, ' +
        'SIQ_Segment_4__c, SIQ_Segment_5__c, SIQ_Segment_6__c, SIQ_Segment_7__c, SIQ_Segment_8__c, SIQ_Segment_9__c, ' +
        'LastModifiedById, SIQ_Account_Name__c, SIQ_Account_Number__c, SIQ_Account_Type__c, SIQ_Status__c ' +
        'FROM SIQ_Account_Master__c ' ;
        if(lastjobDate!=null){
            query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
        //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
        
        
            
    }
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<SIQ_Account_Master__c> records){
        // process each batch of records
        List<Account> accounts = new List<Account>();
        for (SIQ_Account_Master__c acc : records) {
           Account account=new Account();
            account.AccountNumber=acc.SIQ_Account_Number__c;
            account.AxtriaSalesIQTM__AccountType__c=acc.SIQ_Account_Type__c;
            account.Name=acc.SIQ_Account_Name__c;
            account.CurrencyIsoCode=acc.CurrencyIsoCode;
            account.OwnerId=acc.OwnerId;
            account.Marketing_Code__c=acc.SIQ_Marketing_Country__c;
            account.Billing_Country__c=acc.SIQ_Billing_Country__c;
            account.Billing_Postal_Code__c=acc.SIQ_Postal_Code_in_Customer_Address__c;
            account.Billing_State__c =acc.SIQ_Billing_State_Province__c ;
            account.Billing_Street__c =acc.SIQ_Billing_Address__c;
            if(mapCountryCode.get(acc.SIQ_Country__c)!=null){
            account.AxtriaSalesIQTM__Country__c =mapCountryCode.get(acc.SIQ_Country__c);
            }
            else
            {
            account.AxtriaSalesIQTM__Country__c=null;
            }
            account.Department_ID__c = acc.SIQ_Department_ID__c;
            account.Department_Name__c =acc.SIQ_Department_Name__c;
            account.External_Organization_Type__c =acc.SIQ_External_Organization_Type__c;
            account.External_Account_Id__c =acc.SIQ_External_Account_Id__c;
            account.External_Account_Number__c =acc.SIQ_External_Account_Number__c;
            account.External_Parent_Account_Id__c = acc.SIQ_External_Parent_Account_Id__c;
            account.AxtriaSalesIQTM__FirstName__c = acc.SIQ_First_Name__c;
            account.Last_Modified_Date__c =acc.SIQ_Last_Modified_Date__c;         
            account.AxtriaSalesIQTM__LastName__c = acc.SIQ_Last_Name__c;
            account.Latitude__c = acc.SIQ_Latitude__c;
            account.Longitude__c =acc.SIQ_Longitude__c;
            account.Middle_Name__c =acc.SIQ_Middle_Name__c;
            account.Micro_Brick__c =acc.SIQ_Micro_Brick__c;
            account.ParentID = acc.SIQ_Parent_Account_Number__c;
            account.Primary_Speciality_Code__c =  acc.SIQ_Primary_Speciality_Id__c;
            account.Primary_Speciality_Name__c = acc.SIQ_Primary_Speciality_Name__c;
            account.Profile_Consent__c = acc.SIQ_Profile_Consent__c;
            account.Publish_Date_Active__c = acc.SIQ_Publish_Date_Active__c;
            account.Publish_Date_Address__c = acc.SIQ_Publish_Date_Address__c;
            account.Publish_Date_Specialty__c =acc.SIQ_Publish_Date_Specialty__c;
            account.Publish_Date_Type__c =acc.SIQ_Publish_Date_Type__c;
            account.Publish_Event_Active__c = acc.SIQ_Publish_Event_Active__c;
            account.Publish_Event_Address__c =acc.SIQ_Publish_Event_Address__c;
            account.Publish_Event_CustomerType__c = acc.SIQ_Publish_Event_CustomerType__c;
            account.Publish_Event_Specialty__c = acc.SIQ_Publish_Event_Specialty__c;
            account.Right_to_be_forgotten__c = acc.SIQ_Right_to_be_forgotten__c;
            account.Secondary_speciality_code__c =acc.SIQ_Secondary_Speciality_Id__c;
            account.Secondary_Speciality_Name__c =acc.SIQ_Secondary_Speciality_Name__c;
            account.Segment1__c = acc.SIQ_Segment1__c;
            account.Segment10__c = acc.SIQ_Segment_10__c;
            account.Segment2__c =  acc.SIQ_Segment_2__c;
            account.Segment3__c = acc.SIQ_Segment_3__c;
            account.Segment4__c =acc.SIQ_Segment_4__c;
            account.Segment5__c = acc.SIQ_Segment_5__c;
            account.Segment6__c = acc.SIQ_Segment_6__c;
            account.Segment7__c =acc.SIQ_Segment_7__c;
            account.Segment8__c =acc.SIQ_Segment_8__c;
            account.Segment9__c = acc.SIQ_Segment_9__c;
            account.Status__c =acc.SIQ_Status__c ;
            accounts.add(account);
            system.debug('recordsProcessed+'+recordsProcessed);
            recordsProcessed++;
            //comments
           
        }
        upsert accounts;
        Acclist = [select id,External_Account_Number__c,ParentID from Account where id IN:accounts];
        for(Account A : Acclist){
            String Key = A.External_Account_Number__c+'_'+A.id+'_'+A.ParentID;
            if(!Acc_Id.contains(Key)){
                Acc_Id.add(Key); 
            }
        }
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        Database.executeBatch(new UpdateAccountAffiliation());
    }   
}