/**
Name        :   MySetUpProd_Insert_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   MySetUpProd_Insert_Utility
*/
global class MySetUpProd_Insert_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,Team_Instance_Product_AZ__c> questionToObjectMap;
    
    global MySetUpProd_Insert_Utility(String teamInstance) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        selectedTeamInstance = teamInstance;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__c]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Team_Instance__r.name,bti.id);
        }
        
        //selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        /*List<Product_Catalog__c> ProdList = new List<Product_Catalog__c>();
        for (Team_Instance_Product_AZ__c TIP : [Select id,Product_Catalogue__c,Team_Instance__c from Team_Instance_Product_AZ__c where Team_Instance__c = :teamInstance]){
            ProdList.add(String.Valueof(TIP.Product_Catalogue__c));
        }*/
        
        query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.External_ID__c, Vacation_Days__c FROM AxtriaSalesIQTM__Position_Product__c  Where AxtriaSalesIQTM__Team_Instance__c = \'' + teamInstance + '\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Product__c> scopeRecs) {
      system.debug('Hello++++++++++++++++++++++++++++');
        
        
        SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
        SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
        Map<String,SIQ_My_Setup_Products_vod_O__c> MySetupProdMap = new Map<String,SIQ_My_Setup_Products_vod_O__c>();
        
        for(AxtriaSalesIQTM__Position_Product__c scsp : scopeRecs){
            
            tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
            tempMySetupProdRec.CurrencyIsoCode = scsp.CurrencyIsoCode;//'EUR'
            /*//tempMySetupProdRec.OwnerId = ;
            tempMySetupProdRec.Name = ;*/
            tempMySetupProdRec.SIQ_Favorite_vod__c = 'false';
            tempMySetupProdRec.SIQ_Product_vod__c = scsp.Product_Catalog__r.External_ID__c;
            
            tempMySetupProdRec.SIQ_Employee_PRID__c = scsp.AxtriaSalesIQTM__Position__r.Employee_PRID__c;
            tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
            
            tempMySetupProdRec.External_ID__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c + '_' + scsp.AxtriaSalesIQTM__Position__r.Employee_PRID__c + '_' + scsp.Product_Catalog__r.External_ID__c;
            
            if(!MySetupProdMap.containsKey(tempMySetupProdRec.External_ID__c) && scsp.AxtriaSalesIQTM__Position__r.Employee_PRID__c != null && scsp.Product_Catalog__r.External_ID__c != null){
                MySetupProdMap.put(tempMySetupProdRec.External_ID__c,tempMySetupProdRec);
                
                if(scsp.Product_Catalog__r.Detail_Group__c != null && scsp.Product_Catalog__r.Detail_Group__c != ''){
                    tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
                    tempMySetupProdRec2 = tempMySetupProdRec.clone();
                    tempMySetupProdRec2.SIQ_Product_vod__c = scsp.Product_Catalog__r.Detail_Group__c;
                    tempMySetupProdRec2.External_ID__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c + '_' + scsp.AxtriaSalesIQTM__Position__r.Employee_PRID__c + '_' + scsp.Product_Catalog__r.Detail_Group__c;
                    if(!MySetupProdMap.containsKey(tempMySetupProdRec2.External_ID__c) && scsp.AxtriaSalesIQTM__Position__r.Employee_PRID__c != null && scsp.Product_Catalog__r.Detail_Group__c != null){
                        MySetupProdMap.put(tempMySetupProdRec2.External_ID__c,tempMySetupProdRec2);
                    }
                }
                
            }
        }
        
        upsert MySetupProdMap.values() External_ID__c;//insert MySetupProdMap.values();//

    }

    global void finish(Database.BatchableContext BC) {
        
    }
}