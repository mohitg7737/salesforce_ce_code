@isTest
private class batchOutBoundPositionGeographyTest {
   @testSetup 
    static void setup() {
     Custom_Scheduler__c cs=new Custom_Scheduler__c();
        cs.Status__c=true;
        cs.Schedule_date__c=Date.today();
        cs.Name='xx';
        insert cs;
        
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Workspace__c w=new AxtriaSalesIQTM__Workspace__c();
        w.AxtriaSalesIQTM__Workspace_Start_Date__c=Date.today();
        w.AxtriaSalesIQTM__Workspace_Description__c='test';
        insert w;
        String wkspcId=w.id;
        AxtriaSalesIQTM__Scenario__c sc=new AxtriaSalesIQTM__Scenario__c();
        sc.AxtriaSalesIQTM__Scenario_Stage__c='Live';
        sc.AxtriaSalesIQTM__Workspace__c=wkspcId;
        insert sc;
        String scId=sc.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='Current';
        ti.AxtriaSalesIQTM__Scenario__c=scId;
        insert ti;
        String teamInstId=ti.id;
        AxtriaSalesIQTM__Geography__c geo=new AxtriaSalesIQTM__Geography__c();
        geo.Team_Name__c='yy';
        geo.Team_Instance__c='y';
        insert geo;
        String geoId=geo.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c(); 
        pos.AxtriaSalesIQTM__Client_Position_Code__c='client position Code';
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        insert pos;
        String posId=pos.id;
        AxtriaSalesIQTM__Position_Geography__c pg=new AxtriaSalesIQTM__Position_Geography__c();
        pg.AxtriaSalesIQTM__Geography__c=geoId;
        pg.AxtriaSalesIQTM__Effective_Start_Date__c=Date.valueOf('2018-01-01');
        pg.AxtriaSalesIQTM__Effective_End_Date__c=date.valueOf('2018-05-05');
        pg.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        pg.AxtriaSalesIQTM__Position__c=posId;
        
       insert pg;
    }
    static testmethod void test() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        batchOutBoundPositionGeography usa = new batchOutBoundPositionGeography();
        Id batchId = Database.executeBatch(usa);
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
    static testmethod void test1() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
         AxtriaSalesIQTM__Workspace__c w=new AxtriaSalesIQTM__Workspace__c();
        w.AxtriaSalesIQTM__Workspace_Start_Date__c=Date.today();
        w.AxtriaSalesIQTM__Workspace_Description__c='test';
        insert w;
        String wkspcId=w.id;
        AxtriaSalesIQTM__Scenario__c sc=new AxtriaSalesIQTM__Scenario__c();
        sc.AxtriaSalesIQTM__Scenario_Stage__c='Live';
        sc.AxtriaSalesIQTM__Workspace__c=wkspcId;
        insert sc;
        String scId=sc.id;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.AxtriaSalesIQTM__Scenario__c=scId;
        teamIns.Name = 'abc';
        insert teamIns;
        
         AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='Current';
        insert ti;
        String teamInstId=ti.id;
        AxtriaSalesIQTM__Geography__c geo=new AxtriaSalesIQTM__Geography__c();
        geo.Team_Name__c='yy';
        geo.Team_Instance__c='y';
        insert geo;
        String geoId=geo.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c(); 
        pos.AxtriaSalesIQTM__Client_Position_Code__c='client position Code';
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        insert pos;
        String posId=pos.id;
        AxtriaSalesIQTM__Position_Geography__c pg=new AxtriaSalesIQTM__Position_Geography__c();
        pg.AxtriaSalesIQTM__Geography__c=geoId;
        pg.AxtriaSalesIQTM__Effective_Start_Date__c=Date.valueOf('2018-01-01');
        pg.AxtriaSalesIQTM__Effective_End_Date__c=date.valueOf('2018-05-05');
        pg.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        pg.AxtriaSalesIQTM__Position__c=posId;
        
       insert pg;
        batchOutBoundPositionGeography usa = new batchOutBoundPositionGeography(teamInstId,1,'xx');
        Id batchId = Database.executeBatch(usa);
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}