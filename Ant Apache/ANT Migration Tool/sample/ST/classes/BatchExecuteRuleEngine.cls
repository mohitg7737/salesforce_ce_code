global class BatchExecuteRuleEngine implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String ruleId;
    public List<BU_Response__c> allBuResponses;
    public Map<String,Map<String,Integer>> segmentSimulationMap;
    public String field1;
    public String field2;
    public Boolean flag;
    public Map<String,Integer> potentialMap;
    public Map<String, Integer> segmentToTCFmap;
    public Map<String,List<String>> keyToDistinct;
    public Map<String,Integer> segmentToDistinct;

    public Map<String,List<String>> adopPotKeyToDistinct;
    public Map<String,Integer> adopPotsegmentToDistinct;
    public Boolean errorFlag;

    public Set<String> allSegments;


    global BatchExecuteRuleEngine(String ruleId, String whereClause) {

        allSegments = new Set<String>();

        errorFlag = false;
        flag = false;
        segmentSimulationMap = new Map<String,Map<String,Integer>>();
        segmentToTCFmap = new Map<String,Integer>();
        potentialMap = new Map<String,Integer>();
        keyToDistinct = new Map<String,List<String>>();

        adopPotsegmentToDistinct = new Map<String,Integer>();
        adopPotKeyToDistinct = new Map<String,List<String>>();
        
        segmentToDistinct = new Map<String,Integer>();

        this.ruleId = ruleId;
        this.query  = 'SELECT Id, Physician__c, Position_Account__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, ';
        this.query += 'Response6__c, Response7__c, Response8__c, Response9__c, Response10__c FROM BU_Response__c';
        if(String.isNotBlank(whereClause)){
            this.query += ' WHERE ' + whereClause;
        }
        allBuResponses = new List<BU_Response__c>();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {

            Map<String, Account_Compute_Final__c> accountFinalMap = new Map<String, Account_Compute_Final__c>();
            Map<String, Map<String,String>> acfNameValue = new Map<String, Map<String, String>>();
            RuleStepFactory rsf = new RuleStepFactory();


            Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__c, Line_2__c, Team_Instance__c,Team_Instance__r.name, Cycle__c FROM Measure_Master__c WHERE Id =: this.ruleId LIMIT 1];
            
            ID bTeamInstance = [select id from Brand_Team_Instance__c where Brand__c = :rule.Brand_Lookup__c and Team_Instance__c = :rule.Team_Instance__c].id;

            list<Metadata_Definition__c> mdd = [SELECT Source_Field__c, Display_Name__c, Type__c, Sequence__c, Name, Id, Brand_Team_Instance__c FROM MetaData_Definition__c  WHERE Source_Object__c = 'BU_Response__c' AND Team_Instance__c =:rule.Team_Instance__c and Brand_Team_Instance__c = :bTeamInstance ORDER BY Sequence__c];
        
            Map<String, String> nameFieldMapping = new Map<String, String>();
            for(Metadata_Definition__c md: mdd){
                nameFieldMapping.put(md.Display_Name__c.toUpperCase(), md.Source_Field__c);
            }

            list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
            Integer counter = 1;
            Boolean isInputParam = false;
            //system.debug('--nameFieldMapping--- ' + nameFieldMapping);
            for(Rule_Parameter__c ruleParam: ruleParams){
                for(Sobject bu : scope){
                    Account_Compute_Final__c acf;
                    String keyAcc = ((String)bu.get('Physician__c'));
                    String key = ((String)bu.get('Position_Account__c')); 
                    if(accountFinalMap.containsKey(key)){
                        acf = accountFinalMap.get(key);
                    }else{
                        acf = new Account_Compute_Final__c();
                        acf.Physician_2__c = (String)bu.get('Physician__c');
                        acf.Physician__c = (String)bu.get('Position_Account__c');
                        acf.Measure_Master__c = ruleParam.Measure_Master__c;
                    }

                    //System.debug('--ruleParam.Parameter_Name__c-- ' +ruleParam.Parameter_Name__c);
                    if(nameFieldMapping.containsKey(ruleParam.Parameter_Name__c.toUpperCase())){
                        isInputParam = true;
                        acf.put('Output_Name_' + counter + '__c', ruleParam.Parameter_Name__c);
                        acf.put('Output_Value_' + counter + '__c', (String)bu.get(nameFieldMapping.get(ruleParam.Parameter_Name__c.toUpperCase())));
                        accountFinalMap.put(key, acf);
                    }else{
                        isInputParam = false;
                    }
                }
                if(isInputParam)
                    counter += 1;
            }

            List<String> allAggregateFunc = new List<String>();
            allAggregateFunc.add('Max_9999');
            allAggregateFunc.add('Min_9999');
            allAggregateFunc.add('Avg_9999');
            allAggregateFunc.add('Sum_0');
            
            list<Step__c> steps = [SELECT Id, Name, Step_Type__c, UI_Location__c, Compute_Master__c, Matrix__c, Measure_Master__c, Grid_Param_1__c, Grid_Param_1__r.Parameter_Name__c, Grid_Param_2__c, Grid_Param_2__r.Parameter_Name__c  FROM Step__c WHERE Measure_Master__c =: this.ruleId ORDER By Sequence__c ASC];

            String segment;
            system.debug('++++++++++ Setps is '+ steps);
            for(Step__c step : steps){
                Step stpObj = rsf.getRuleStep(Step);
                system.debug('+++++++++ Hey Current step is '+ step);
                for(Sobject bu : scope){
                    Account_Compute_Final__c acf;
                    String keyAcc = ((String)bu.get('Physician__c'));
                    String key = ((String)bu.get('Position_Account__c'));
                    if(accountFinalMap.containsKey(key)){
                        acf = accountFinalMap.get(key);
                    }else{
                        acf = new Account_Compute_Final__c();
                        acf.Physician_2__c = (String)bu.get('Physician__c');
                        acf.Physician__c = (String)bu.get('Position_Account__c');
                        acf.Measure_Master__c = step.Measure_Master__c;
                    }

                    Map<String, String> acfMap;
                    if(acfNameValue.containsKey(key)){
                        acfMap = acfNameValue.get(key);
                    }else{
                        acfMap = new Map<String, String>();
                }

                    

                    String output = stpObj.solveStep(step, nameFieldMapping, acfMap, (BU_Response__c)bu, (List<BU_Response__c>)allBuResponses, allAggregateFunc);


                    Integer counterOutput = 1;

                    

                    if(step.UI_Location__c == 'Compute Segment'){
                        system.debug('++++++++++ Hey Acf is '+ acf);
                        if(flag == false)
                        {
                            while(counterOutput <= 20){
                                String localKey = (String)acf.get('Output_Name_'+counterOutput+'__c');
                                if(localKey ==  step.Grid_Param_1__r.Parameter_Name__c){
                                    field1 = 'Output_Value_'+counterOutput+'__c';
                                }

                                if(localKey == step.Grid_Param_2__r.Parameter_Name__c){
                                    field2 = 'Output_Value_'+counterOutput+'__c';
                                }
                                counterOutput += 1;
                            }

                            flag = true;                        
                        }
                        system.debug('Hey ++++++++++++++++ segmentSimulationMap'+ segmentSimulationMap );
                        acf.put('Final_Segment__c', output);

                        if(output == null)
                        {
                            allSegments.add('BLANK');
                        }
                        else
                        {
                            allSegments.add(output);
                        }
                        segment = output;
                    }else if(step.UI_Location__c == 'Compute TCF'){
                        acf.put('Calculated_TCF__c', String.isNotBlank(output) ? Decimal.valueOf(output) : 0);

                        acf.put('Proposed_TCF__c', String.isNotBlank(output) ? Decimal.valueOf(output) : 0);
                        acf.put('Final_TCF__c', String.isNotBlank(output) ? Decimal.valueOf(output) : 0);
                        
                        
                            if(field1 != null)
                            {
                                String segmentQ = String.valueof(acf.get('Final_Segment__c'));
                                String adoptionVal = String.valueof(acf.get(field1));
                                String potentialVal = String.valueof(acf.get(field2));
                                String newkey = adoptionVal + '_' + potentialVal + '_' + segmentQ;
                                if(output != null)
                                {
                                        

                                    if(segmentToTCFmap.containsKey(newkey))
                                    {
                                        segmentToTCFmap.put(newkey, segmentToTCFmap.get(newkey) + Integer.valueof(output));
                                    }
                                    else
                                    {
                                        segmentToTCFmap.put(newkey, Integer.valueof(output));
                                    }
                                }
                               /* else
                                {
                                    //Handle if output is null
                                     String newkey = adoptionVal + '_' + potentialVal + '_' + segment;
                                    if(segmentToTCFmap.containsKey(newkey))
                                    {
                                        segmentToTCFmap.put(newkey, segmentToTCFmap.get(newkey) + Integer.valueof(output));
                                    }
                                    else
                                    {

                                        segmentToTCFmap.put(newkey, Integer.valueof(output));
                                    }
                                }*/
                                system.debug('++++++++ new KEy is '+ newKey);
                                system.debug('++++++++ Hey Segment To TCF Map is '+ segmentToTCFmap);
                            }
                    }
                    else if(step.UI_Location__c == 'Compute Accessibility')
                    {
                        System.debug('=====output::==='+output);
                        acf.put('Proposed_TCF__c', String.isNotBlank(output) ? Decimal.valueOf(output) : 0);
                        acf.put('Final_TCF__c', String.isNotBlank(output) ? Decimal.valueOf(output) : 0);
    /*
                       if(field1 != null)
                            {
                                String adoptionVal = String.valueof(acf.get(field1));
                                String potentialVal = String.valueof(acf.get(field2));

                                if(output == null)
                                {
                                    String seg = 'NULL';
                                    String newkey = adoptionVal + '_' + potentialVal + '_' + seg;

                                    if(segmentToTCFmap.containsKey(newkey))
                                    {
                                        segmentToTCFmap.put(newkey, segmentToTCFmap.get(newkey) + Integer.valueof(output));
                                    }
                                    else
                                    {
                                        segmentToTCFmap.put(newkey, Integer.valueof(output));
                                    }
                                }
                                else
                                {
                                    //Handle if output is null
                                     String newkey = adoptionVal + '_' + potentialVal + '_' + segment;
                                    if(segmentToTCFmap.containsKey(newkey))
                                    {
                                        segmentToTCFmap.put(newkey, segmentToTCFmap.get(newkey) + Integer.valueof(output));
                                    }
                                    else
                                    {

                                        segmentToTCFmap.put(newkey, Integer.valueof(output));
                                    }
                                }
                            }
    */
                        

                        system.debug('=-========== Hey Counter is ' + counter);
                    }
                    //System.debug('--step.Name-- ' + step.Name);
                    //System.debug('--counter-- ' + counter);

                    acf.put('Output_Name_' + counter + '__c', step.Name);
                    acf.put('Output_Value_' + counter + '__c', output);
                    accountFinalMap.put(key, acf);

                    acfMap.put(step.Name, output);

                    flag = false;

                    

                    if(step.UI_Location__c == 'Compute Segment')
                    {
                        system.debug('+++ Hey Field 1 is '+ field1 + '_' + acf);
                        if(field1 != null)
                        {
                             String adoptionVal = String.valueof(acf.get(field1));
                            String potentialVal = String.valueof(acf.get(field2));
                            if(flag == false)
                            {
                                flag = true;

                                /*if(potentialMap.containsKey(potentialVal))
                                {
                                    potentialMap.put(potentialVal, potentialMap.get(potentialVal) + 1);
                                }
                                else
                                {
                                    potentialMap.put(potentialVal, 1);
                                }
    */
                                String concatString =  adoptionVal + '_' + potentialVal;

                                Map<String, Integer> tempMap = new Map<String,Integer>();

                                String segmentVal = String.valueof(acf.get('Final_Segment__c'));

                                if(segmentVal == null)
                                {
                                    segmentVal = 'NULL';
                                    segment = 'NULL';
                                }
                                else
                                {
                                    segment = segmentVal;
                                }

                                if(keyToDistinct.containsKey(segmentVal))
                                {
                                    if(!keyToDistinct.get(segmentVal).contains(acf.Physician_2__c))
                                    {
                                        keyToDistinct.get(segmentVal).add(acf.Physician_2__c);
                                        segmentToDistinct.put(segmentVal, segmentToDistinct.get(segmentVal)+ 1);
                                    }
                                }
                                else
                                {
                                    List<String> tempList = new List<String>();
                                    tempList.add(String.valueof(acf.Physician_2__c));
                                    keyToDistinct.put(segmentVal, tempList);
                                    segmentToDistinct.put(segmentVal, 1);
                                }
                                String key2 = concatString + '_' + segmentVal;

                                if(adopPotKeyToDistinct.containsKey(key2))
                                {
                                    if(!adopPotKeyToDistinct.get(key2).contains(acf.Physician_2__c))
                                    {
                                        adopPotKeyToDistinct.get(key2).add(acf.Physician_2__c);
                                        adopPotsegmentToDistinct.put(key2, adopPotsegmentToDistinct.get(key2)+ 1);
                                    }
                                }
                                else
                                {
                                    List<String> tempList = new List<String>();
                                    tempList.add(String.valueof(acf.Physician_2__c));
                                    adopPotKeyToDistinct.put(key2, tempList);
                                    adopPotsegmentToDistinct.put(key2, 1);
                                }
                               // adopPotKeyToDistinct adopPotsegmentToDistinct

                                    
                                if(segmentSimulationMap.containsKey(concatString))
                                {
                                    tempMap = segmentSimulationMap.get(concatString);

                                    

                                    if(tempMap.containsKey(segmentVal))
                                    {
                                        tempMap.put(segmentVal, tempMap.get(segmentVal) + 1);
                                    }
                                    else
                                    {
                                        tempMap.put(segmentVal,1);   
                                    }
                                }
                                else
                                {
                                    tempMap.put(segmentVal, 1);   
                                }

                                segmentSimulationMap.put(concatString, tempMap);
                            }                       
                        }


                    }
                    acfNameValue.put(key, acfMap);
                }
                counter += 1;
            }
            insert accountFinalMap.values();

            list<Account_Compute_Final_Copy__c> finalCopy = new list<Account_Compute_Final_Copy__c>();
            for(Account_Compute_Final__c acf: accountFinalMap.values()){
                Account_Compute_Final_Copy__c copy = new Account_Compute_Final_Copy__c();
                copy.Measure_Master__c = acf.Measure_Master__c;
                copy.Physician_2__c = acf.Physician_2__c;
                copy.Physician__c = acf.Physician__c;
                copy.Output_Name_1__c = acf.OUTPUT_NAME_1__c;
                copy.Output_Name_2__c = acf.OUTPUT_NAME_2__c;
                copy.Output_Name_3__c = acf.OUTPUT_NAME_3__c;
                copy.Output_Name_4__c = acf.OUTPUT_NAME_4__c;
                copy.Output_Name_5__c = acf.OUTPUT_NAME_5__c;
                copy.Output_Name_6__c = acf.OUTPUT_NAME_6__c;
                copy.Output_Name_7__c = acf.OUTPUT_NAME_7__c;
                copy.Output_Name_8__c = acf.OUTPUT_NAME_8__c;
                copy.Output_Name_9__c = acf.OUTPUT_NAME_9__c;
                copy.Output_Name_10__c = acf.OUTPUT_NAME_10__c;
                copy.Output_Name_11__c = acf.OUTPUT_NAME_11__c;
                copy.Output_Name_12__c = acf.OUTPUT_NAME_12__c;
                copy.Output_Name_13__c = acf.OUTPUT_NAME_13__c;
                copy.Output_Name_14__c = acf.OUTPUT_NAME_14__c;
                copy.Output_Name_15__c = acf.OUTPUT_NAME_15__c;
                copy.Output_Name_16__c = acf.OUTPUT_NAME_16__c;
                copy.Output_Name_17__c = acf.OUTPUT_NAME_17__c;
                copy.Output_Name_18__c = acf.OUTPUT_NAME_18__c;
                copy.Output_Name_19__c = acf.OUTPUT_NAME_19__c;
                copy.Output_Name_20__c = acf.OUTPUT_NAME_20__c;
                copy.Output_Name_21__c = acf.OUTPUT_NAME_21__c;
                copy.Output_Name_22__c = acf.OUTPUT_NAME_22__c;
                copy.Output_Name_23__c = acf.OUTPUT_NAME_23__c;
                copy.Output_Name_24__c = acf.OUTPUT_NAME_24__c;
                copy.Output_Name_25__c = acf.OUTPUT_NAME_25__c;
                copy.Output_Name_26__c = acf.OUTPUT_NAME_26__c;
                copy.Output_Name_27__c = acf.OUTPUT_NAME_27__c;
                copy.Output_Name_28__c = acf.OUTPUT_NAME_28__c;
                copy.Output_Name_29__c = acf.OUTPUT_NAME_29__c;
                copy.Output_Name_30__c = acf.OUTPUT_NAME_30__c;

                copy.Output_Value_1__c = acf.OUTPUT_Value_1__c;
                copy.Output_Value_2__c = acf.OUTPUT_Value_2__c;
                copy.Output_Value_3__c = acf.OUTPUT_Value_3__c;
                copy.Output_Value_4__c = acf.OUTPUT_Value_4__c;
                copy.Output_Value_5__c = acf.OUTPUT_Value_5__c;
                copy.Output_Value_6__c = acf.OUTPUT_Value_6__c;
                copy.Output_Value_7__c = acf.OUTPUT_Value_7__c;
                copy.Output_Value_8__c = acf.OUTPUT_Value_8__c;
                copy.Output_Value_9__c = acf.OUTPUT_Value_9__c;
                copy.Output_Value_10__c = acf.OUTPUT_Value_10__c;
                copy.Output_Value_11__c = acf.OUTPUT_Value_11__c;
                copy.Output_Value_12__c = acf.OUTPUT_Value_12__c;
                copy.Output_Value_13__c = acf.OUTPUT_Value_13__c;
                copy.Output_Value_14__c = acf.OUTPUT_Value_14__c;
                copy.Output_Value_15__c = acf.OUTPUT_Value_15__c;
                copy.Output_Value_16__c = acf.OUTPUT_Value_16__c;
                copy.Output_Value_17__c = acf.OUTPUT_Value_17__c;
                copy.Output_Value_18__c = acf.OUTPUT_Value_18__c;
                copy.Output_Value_19__c = acf.OUTPUT_Value_19__c;
                copy.Output_Value_20__c = acf.OUTPUT_Value_20__c;
                copy.Output_Value_21__c = acf.OUTPUT_Value_21__c;
                copy.Output_Value_22__c = acf.OUTPUT_Value_22__c;
                copy.Output_Value_23__c = acf.OUTPUT_Value_23__c;
                copy.Output_Value_24__c = acf.OUTPUT_Value_24__c;
                copy.Output_Value_25__c = acf.OUTPUT_Value_25__c;
                copy.Output_Value_26__c = acf.OUTPUT_Value_26__c;
                copy.Output_Value_27__c = acf.OUTPUT_Value_27__c;
                copy.Output_Value_28__c = acf.OUTPUT_Value_28__c;
                copy.Output_Value_29__c = acf.OUTPUT_Value_29__c;
                copy.Output_Value_30__c = acf.OUTPUT_Value_30__c;
                copy.Final_Segment__c   = acf.Final_Segment__c;
                copy.Final_TCF__c       = acf.Final_TCF__c;
                copy.Calculated_TCF__c  = acf.Calculated_TCF__c;
                //copy.Priority__C = '100';
                finalCopy.add(copy);
            }

            insert finalCopy;
            system.debug('++++++++ Final Copy is '+ finalCopy);
    }

    global void finish(Database.BatchableContext BC) {
        

            String segmentsString = '';
            if(allSegments.size() > 0)
            {
                List<String> timeList = new List<String>(allSegments);
                timeList.sort();
                system.debug('++++++++++++ timeList ' + timeList);
                for(String key : timeList)
                {
                    segmentsString += key + ',';
                }

                segmentsString = segmentsString.removeEnd(',');            
            }

            if(!Test.isRunningTest()){
                BatchCreateRuleResultsDownload bat = new BatchCreateRuleResultsDownload(ruleId, segmentsString);
                Database.executeBatch(bat, 200);
            }

            List<Segment_Simulation__c> segmentSimulationRecs = new List<Segment_Simulation__c>();
            
            system.debug('+++++++ Hey guys '+ segmentSimulationMap);
            system.debug('+++++++ Hey guys '+ segmentToTCFmap);
            for(String key : segmentSimulationMap.keySet())
            {
                Map<String, Integer> tempMap = segmentSimulationMap.get(key);
                
                String dimesion1 = key.split('_')[0];
                String dimesion2 = key.split('_')[1];
                Boolean flag  = true;
                for(String segments : tempMap.keySet())
                {
                    if(flag)
                    {
                        String newStr = key + '_' + segments;
                        Segment_Simulation__c ssc = new Segment_Simulation__c();
                        ssc.Dimension1__c = dimesion1;
                        ssc.Dimension2__c = dimesion2;
                        ssc.Measure_Master__c = ruleId;
                        ssc.Segment__c = segments;

                        if(segmentToTCFmap.get(newStr) != null)
                            ssc.Sum_TCF__c = segmentToTCFmap.get(newStr);
                        else
                            ssc.Sum_TCF__c = 0;
                        ssc.Name = dimesion1 + '_' + dimesion2 + '_' + segments;
                        ssc.Count_of_Accounts__c = tempMap.get(segments);
                        ssc.Distinct_Recs__c = segmentToDistinct.get(segments);
                        ssc.Sum_Proposed_TCF__c = ssc.Sum_TCF__c;
                        ssc.External_ID__c = ssc.Measure_Master__c +'_'+ key;
                        ssc.AdopPtSegDistinct__c = adopPotsegmentToDistinct.get(key + '_' + segments);
                        segmentSimulationRecs.add(ssc);
                        flag = false;                    
                    }
                }
            }
            upsert segmentSimulationRecs External_ID__c;

        /*List<SalesDataVsBR__c> salesDataBR = new List<SalesDataVsBR__c>();

        Measure_Master__c mm = [SELECT Id, Team_Instance__c,Team_Instance__r.name, Brand_Lookup__c, Brand_Lookup__r.Name, Cycle__c FROM Measure_Master__c WHERE Id =: this.ruleId LIMIT 1];

        for(String key : potentialMap.keySet())
        {
            SalesDataVsBR__c sdb = new SalesDataVsBR__c();
            sdb.Team_Instance__c = mm.Team_Instance__c;
            sdb.Product_Catalog__c = mm.Brand_Lookup__c;
            sdb.Potential__c = key;
            sdb.Count_BR__c = potentialMap.get(key);
            sdb.External_ID__c = sdb.Potential__c + '_' + sdb.Product_Catalog__c + '_' + sdb.Team_Instance__c;

            salesDataBR.add(sdb);
        }

        upsert salesDataBR External_ID__c;*/


        //system.assert(false,'ajfjasf');
        
    }
}