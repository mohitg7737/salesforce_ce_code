global class AZ_Integration_New_Utility implements Database.Batchable<sObject> {
    

	List<String> allChannels;
	String teamInstanceSelected;
	String queryString;
	List<MC_Cycle_Plan_Channel_vod__c> mCyclePlanChannel;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpRecs;
    
	global AZ_Integration_New_Utility(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        queryString = 'select id,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, P2_Picklist1_Updated__c,P2_Picklist2_Updated__c,P2_Picklist3_Updated__c,P2_Picklist4_Updated__c,P2_Picklist5_Updated__c,P3_Picklist1_Updated__c,P3_Picklist2_Updated__c,P3_Picklist3_Updated__c,P3_Picklist4_Updated__c,P3_Picklist5_Updated__c,P4_Picklist1_Updated__c,P4_Picklist2_Updated__c,P4_Picklist3_Updated__c,P4_Picklist4_Updated__c,P4_Picklist5_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,    AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist3_Updated__c, AxtriaSalesIQTM__Picklist4_Metric_Updated__c, AxtriaSalesIQTM__Picklist5_Metric_Updated__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and AxtriaSalesIQTM__isIncludedCallPlan__c = true';        
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
	public void create_MC_Cycle_Plan_Channel_vod(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        pacpRecs = scopePacpProRecs;
        mCyclePlanChannel = new List<MC_Cycle_Plan_Channel_vod__c>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRecs)
        {
            Integer f2fCalls = 0;
            Integer phoneCalls = 0;
            Integer emailCalls = 0;
            Integer remoteCalls = 0;
            Integer webinarCalls = 0;
            Integer sumF2fCalls = 0;
            
            f2fCalls = Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist1_Updated__c) + Integer.valueOf(pacp.P2_Picklist1_Updated__c)+ Integer.valueOf(pacp.P3_Picklist1_Updated__c)+ Integer.valueOf(pacp.P4_Picklist1_Updated__c);

            phoneCalls = phoneCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist2_Updated__c) + Integer.valueOf(pacp.P2_Picklist2_Updated__c)+ Integer.valueOf(pacp.P3_Picklist2_Updated__c)+ Integer.valueOf(pacp.P4_Picklist2_Updated__c);
			emailCalls = emailCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist3_Updated__c) + Integer.valueOf(pacp.P2_Picklist3_Updated__c)+ Integer.valueOf(pacp.P3_Picklist3_Updated__c)+ Integer.valueOf(pacp.P4_Picklist3_Updated__c);
			remoteCalls = remoteCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist4_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist4_Updated__c)+ Integer.valueOf(pacp.P3_Picklist4_Updated__c)+ Integer.valueOf(pacp.P4_Picklist4_Updated__c);
			webinarCalls = webinarCalls + Integer.valueOf(pacp.AxtriaSalesIQTM__Picklist5_Metric_Updated__c) + Integer.valueOf(pacp.P2_Picklist5_Updated__c)+ Integer.valueOf(pacp.P3_Picklist5_Updated__c)+ Integer.valueOf(pacp.P4_Picklist5_Updated__c);

            sumF2fCalls = f2fCalls;

            integer sum = f2fCalls + phoneCalls + emailCalls + remoteCalls + webinarCalls;
            
            string teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            
            for(string channels : allChannels)
            {
                MC_Cycle_Plan_Channel_vod__c mcpc = new MC_Cycle_Plan_Channel_vod__c();
                
                if(!channels.contains('Email'))
                mcpc.Cycle_Channel_vod__c       =   pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';
                else
                mcpc.Cycle_Channel_vod__c       =   pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channels + '-Sent_Email_vod__c';
                //mcpc.Cycle_Plan_vod__c          =   pacp.Team_Instance__r.Name +'_'+pacp.Position__r.Client_Position_Code__c;
                mcpc.Cycle_Plan_Target_vod__c   =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                
                
                
                if(channels == 'f2f')
                {
                    mcpc.Channel_Activity_Goal_vod__c = f2fCalls;    
                }
                else if(channels == 'Remote')
                {
                    mcpc.Channel_Activity_Goal_vod__c = remoteCalls;
                }
                else if(channels == 'Phone')
                {
                    mcpc.Channel_Activity_Goal_vod__c = phoneCalls;
                }
                else if(channels.contains('Email'))
                {
                    mcpc.Channel_Activity_Goal_vod__c = emailCalls;
                }
                else if(channels == 'Webinar')
                {
                    mcpc.Channel_Activity_Goal_vod__c = webinarCalls;
                }
                DateTime dtStart                 =   pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());
                
				String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
                //mcpc.AZ_External_Id__c = mcpc.Cycle_Channel_vod__c +'_'+pacp.Account__r.AccountNumber;
                mcpc.AZ_External_ID__c = teamPos + '-' +channels + '-'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                mcpc.RecordTypeId__c  = '01215000001Cmpn';
                mcpc.Cycle_vod__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c); // Use this external ID
                mcpc.Channel_Activity_max_vod__c = Decimal.valueof(sum);
                mcpc.External_Id_vod__c = mcpc.AZ_External_Id__c;
                
                mCyclePlanChannel.add(mcpc);
            }
            
        }
        //create_MC_Cycle_Plan_Product_vod();
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        create_MC_Cycle_Plan_Channel_vod(scopePacpProRecs);
        
        insert mCyclePlanChannel;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}