global class changeMCTargetStatus implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    
    //List<AxtriaARSnT__Parent_PACP__c> pacpRecs;
    
    global changeMCTargetStatus(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, AxtriaARSnT__Rec_Status__c from AxtriaARSnT__SIQ_MC_Cycle_Plan_Target_vod_O__c ';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<AxtriaARSnT__SIQ_MC_Cycle_Plan_Target_vod_O__c> scopePacpProRecs)
    {
        for(AxtriaARSnT__SIQ_MC_Cycle_Plan_Target_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.AxtriaARSnT__Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        AZ_Integration_MC_Target_MP_EU u1 = new AZ_Integration_MC_Target_MP_EU(teamInstanceSelected, allChannels);
        Database.executeBatch(u1, 2000);
        //Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}