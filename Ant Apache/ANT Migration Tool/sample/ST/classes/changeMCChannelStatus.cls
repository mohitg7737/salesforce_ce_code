global class changeMCChannelStatus implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCChannelStatus(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c ';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        AZ_Integration_New_Utility_MP_EU u1 = new AZ_Integration_New_Utility_MP_EU(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);
        //Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}