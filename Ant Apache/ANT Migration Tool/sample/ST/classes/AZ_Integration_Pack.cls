public class AZ_Integration_Pack {
    
    string teamInstanceSelected;
    String status;
    
    public AZ_Integration_Pack(string teamInstance)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle();
    }
        
    public AZ_Integration_Pack(string teamInstance, Integer count)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle_Plan_Target_vod();
    }
   
    
    public void create_MC_Cycle()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstanceRecs = new List<AxtriaSalesIQTM__Team_Instance__c>();
        
        allTeamInstanceRecs = [select Name,AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected];
        
        List<AxtriaARSnT__SIQ_MC_Cycle_vod_O__c> mcRecs = new List<AxtriaARSnT__SIQ_MC_Cycle_vod_O__c>();
         
        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRecs : allTeamInstanceRecs)
        {
            AxtriaARSnT__SIQ_MC_Cycle_vod_O__c mc = new AxtriaARSnT__SIQ_MC_Cycle_vod_O__c();
            mc.Name                          =   teamInstanceRecs.Name;             // Do Modifications in Name (Add Year to Name with Underscores)
            
            
            DateTime dtStart                 =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            DateTime dtEnd                   =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffendDate__c;
            Date myDateEnd                   =   date.newinstance(dtEnd.year(), dtEnd.month(), dtEnd.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mc.AxtriaARSnT__SIQ_Start_Date_vod__c             =   myDateStart;
            mc.AxtriaARSnT__SIQ_End_Date_vod__c               =   myDateEnd;
            ///mc.AZ_Business_Unit__c          =   teamInstanceRecs.Team_Cycle_Name__c; // CNS or Immunolgy : Take Business Unit of AZ
            //mc.AZ_Country_Code__c           =   teamInstanceRecs.Team__r.Base_Team_Name__c;        // Take from Orbit tables_For Callmax File
            //mc.Calculate_Pull_Through_vod__c =   true;  
            mc.AxtriaARSnT__SIQ_Description_vod_del__c            =   'Cycle for MCCP';
            //mc.AZ_External_ID__c            =   mc.AZ_Country_Code__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            mc.AxtriaARSnT__SIQ_Country_Code_AZ__c            =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                
            if(myDateEnd.daysBetween(Date.today()) > 0)
            {
                mc.AxtriaARSnT__SIQ_Status_vod__c                 =   'Planned_vod';
                mc.AxtriaARSnT__SIQ_Calculate_Pull_Through_vod__c      = true;
                
            }
            else
            {
                mc.AxtriaARSnT__SIQ_Status_vod__c                 =   'In_Progress_vod';
                mc.AxtriaARSnT__SIQ_Calculate_Pull_Through_vod__c      = false;

            }
            status =  mc.AxtriaARSnT__SIQ_Status_vod__c ;
            mc.AxtriaARSnT__SIQ_External_Id_vod__c            =   mc.AxtriaARSnT__SIQ_Country_Code_AZ__c  + '_'+ 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.AxtriaARSnT__Increment_Field__c);
            mcRecs.add(mc);
        }
        
        upsert mcRecs AxtriaARSnT__SIQ_External_Id_vod__c;
        
        createMC_Cycle_Plan();
    }
    
    public void createMC_Cycle_Plan() 
    {
        List<AxtriaSalesIQTM__Position__c> posTeamInstanceRecs = [select id,AxtriaSalesIQTM__Team_Instance__r.Name , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and AxtriaSalesIQTM__Hierarchy_Level__c ='1'];
        
        List<AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c> mcCyclePlan = new List<AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c>();
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__User__r.FederationIdentifier from AxtriaSalesIQTM__User_Access_Permission__c];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
         
        for(AxtriaSalesIQTM__User_Access_Permission__c u: uap)
        {
            string teamInstancePos = u.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__User__r.FederationIdentifier);
        }
        
        system.debug('++++++++ '+posTeamToUser);
        
        for(AxtriaSalesIQTM__Position__c pti : posTeamInstanceRecs)
        {
            AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c mcp = new AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c();
            string teamInstancePos = pti.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pti.AxtriaSalesIQTM__Client_Position_Code__c; // Use Client Position Code for Territory ID
            string userID = posTeamToUser.get(teamInstancePos);
            userID = 'U1234';
            DateTime dtStart                 =   pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mcp.Name                    =   teamInstancePos +'_'+userID;
            mcp.AxtriaARSnT__SIQ_Country_Code_AZ__c = pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            //mcp.AZ_Country_Code__c     =   pti.Team_Instance_ID__r.Team__r.Base_Team_Name__c; // Same as Earlier in MC Cycle
            mcp.AxtriaARSnT__SIQ_Cycle_vod__c            =   mcp.AxtriaARSnT__SIQ_Country_Code_AZ__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Increment_Field__c);
            mcp.AxtriaARSnT__SIQ_Owner_Id__c             =   userID;                                 // Federation ID
            mcp.AxtriaARSnT__SIQ_Description_vod__c      =   pti.AxtriaSalesIQTM__Team_Instance__r.Name ;       // Give Cycle Name Data
            mcp.AxtriaARSnT__SIQ_Territory_vod__c        =   pti.AxtriaSalesIQTM__Client_Position_Code__c;   // Add Client Position Code instead of Salesforce ID
           // mcp.AZ_External_ID_vod__c  =   mcp.Name +'_'+mcp.Territory_vod__c;                          // Create this new field
            mcp.AxtriaARSnT__SIQ_External_Id_vod__c      =   mcp.Name +'_'+mcp.AxtriaARSnT__SIQ_Territory_vod__c;
            mcp.AxtriaARSnT__SIQ_Lock_vod__c             =   'false';                              // Add this new field
            mcp.AxtriaARSnT__SIQ_Channel_Interaction_Status_vod__c    = 'On_Schedule_vod';
            mcp.AxtriaARSnT__SIQ_Product_Interaction_Status_vod__c     = 'On_Schedule_vod'; 
           mcp.AxtriaARSnT__SIQ_RecordType__c             = '012d0000001103d';
            
           
                mcp.AxtriaARSnT__SIQ_Status_vod__c                 =   status;
            
            mcCyclePlan.add(mcp);
        }
        
        upsert mcCyclePlan AxtriaARSnT__SIQ_External_Id_vod__c;
        
        createMC_Cycle_Channel_vod();
    }
    
    public void createMC_Cycle_Channel_vod()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceRecs = [select id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected];
        //List<Channel_Weights__c> channelAndWeights = [select Name, Weights__c, Channel_Criteria_vod__c from Channel_Weights__c];

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected].AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c;
        
        List<AxtriaARSnT__Veeva_Market_Specific__c>  veevaCriteria = [select AxtriaARSnT__Channel_Criteria__c,AxtriaARSnT__Market__c from AxtriaARSnT__Veeva_Market_Specific__c where AxtriaARSnT__Market__c = :selectedMarket];
        
        String selectedCriteria;

        if(veevaCriteria.size()>0)
        {
            selectedCriteria = veevaCriteria[0].AxtriaARSnT__Channel_Criteria__c;
        }
        else
        {
            selectedCriteria = 'Call2_vod__c.Status_vod__c:::eq:::Submitted_vod;;;Call2_vod__c.Delivery_Channel_AZ__c:::eq:::F2F;;;Call2_vod__c.RecordTypeId:::ne:::0120Y000000RXf1,,0120Y000000RXf1,,0120Y000000RXey';
        }
        Set<String> uniqueCheck = new Set<String>();

        List<AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c> mcCycleChannel = new List<AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c>();
        
        for(AxtriaSalesIQTM__Team_Instance__c teamInstance : teamInstanceRecs)
        {
            //for(Channel_Weights__c cw : channelAndWeights)
            //{
                AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c mcc = new AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c();
                
                DateTime dtStart                 =   teamInstance.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

                String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day(); 
                
                //mcc.CHANNEL_LABEL_VOD__C    =   'f2f';
               /* 
                if(cw.Name.contains('Email'))
                {
                    mcc.SIQ_Channel_Object_vod__c   =   'Sent_Email_vod__c';
                }
                else*/
                mcc.AxtriaARSnT__SIQ_Channel_Object_vod__c   =   'Call2_vod__c';
                mcc.AxtriaARSnT__SIQ_Country_Code_AZ__c      =    teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                String country = teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;  
                mcc.AxtriaARSnT__SIQ_Channel_Criteria_vod__c =   selectedCriteria;//the Channels, Add this field in Channel Weights object
                mcc.AxtriaARSnT__SIQ_Channel_Weight_vod__c   =   1;
                mcc.AxtriaARSnT__SIQ_Cycle_vod__c            =   country +'_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstance.AxtriaARSnT__Increment_Field__c);
                mcc.AxtriaARSnT__SIQ_External_Id_vod__c      =   teamInstance.Name + '-f2f-' + mcc.AxtriaARSnT__SIQ_Channel_Object_vod__c;
                //mcc.AZ_External_Id__c      =   teamInstance.Name + '-' + cw.Name + '-' + mcc.SIQ_Channel_Object_vod__c; // Not Required as of now. May be used in future;
                mcc.AxtriaARSnT__SIQ_RecordTypeId__c         =   '012d0000001103V';        // Add this static Field
                mcc.AxtriaARSnT__External_ID_Axtria__c = mcc.AxtriaARSnT__SIQ_External_Id_vod__c;

                if(!uniqueCheck.contains(mcc.AxtriaARSnT__SIQ_External_Id_vod__c))
                {
                    mcCycleChannel.add(mcc);
                    uniqueCheck.add(mcc.AxtriaARSnT__SIQ_External_Id_vod__c);
                }
            //}
        }
        
        upsert mcCycleChannel AxtriaARSnT__External_ID_Axtria__c; 
        createMC_Cycle_Product_vod();
        
    }
    
    List<String> allChannels = new List<String>{'F2F'};
    
    public void createMC_Cycle_Product_vod()
    {
        String teamInstanceName = [select Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected LIMIT 1].Name;

        List<AxtriaARSnT__Product_Catalog__c> allProductsAndWeights = [select Name, AxtriaARSnT__Weights__c, AxtriaARSnT__External_ID__c from  AxtriaARSnT__Product_Catalog__c where AxtriaARSnT__Team_Instance__c = :teamInstanceSelected];

        AxtriaSalesIQTM__Team_Instance__c teamInstance = [select id,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, Name from AxtriaSalesIQTM__Team_Instance__c where id= :teamInstanceSelected];
        
        List<AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c> mcCycleProduct = new List<AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c>();
        
        
        
        for(AxtriaARSnT__Product_Catalog__c pro : allProductsAndWeights)         // What all products we have for this team instance //Get all product code from Vardhan File (Master List)
        {
            
            
            string object1;
            
            for(String channel : allChannels)
            {
                AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c mcp = new AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c();
                
                if(channel == 'Email')
                object1 = 'Sent_Email_vod__c';
                else 
                object1 = 'Call2_vod__c';
                    
                
                //mcp.External_Id_vod__c = teamInstance.Name + '-' + channel + '-' + object1;
                //mcp.Channel_Label_vod__c = channel;
                mcp.AxtriaARSnT__SIQ_Cycle_Channel_vod__c = teamInstance.Name + '-' + channel + '-' + object1;  
                
                mcp.AxtriaARSnT__SIQ_Product_vod__c = pro.AxtriaARSnT__External_ID__c;       // Product Code is unique here
                mcp.AxtriaARSnT__SIQ_Product_Weight_vod__c = 1;        // Keep it Static, 1 for all
                //mcp.AZ_External_Id__c = teamInstance.Name + '__' + channel + '__' +pro.External_ID__c + '_'+pro.Name;
                mcp.AxtriaARSnT__SIQ_External_Id_vod__c = teamInstance.Name + '__' + channel + '__' +pro.AxtriaARSnT__External_ID__c + '_'+pro.Name;
                mcp.AxtriaARSnT__External_ID_Axtria__c = mcp.AxtriaARSnT__SIQ_External_Id_vod__c;
                
                mcCycleProduct.add(mcp);
            }
            
        }
        
        system.debug('+++++++++++'+ mcCycleProduct);
        upsert mcCycleProduct AxtriaARSnT__External_ID_Axtria__c;
        
        create_MC_Cycle_Plan_Target_vod();
    }       
    
    public void create_MC_Cycle_Plan_Target_vod()
    {

        changeMCTargetStatus u1 = new changeMCTargetStatus(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);

        create_MC_Cycle_Plan_Channel_vod();
    }
    
    public void create_MC_Cycle_Plan_Channel_vod() 
    {
        changeMCChannelStatus u1 = new changeMCChannelStatus(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);
        
        
        create_MC_Cycle_Plan_Product_vod();
    }
    
    public  void create_MC_Cycle_Plan_Product_vod()
    {
        changeMCProduct u1 = new changeMCProduct(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);
        
    }
}