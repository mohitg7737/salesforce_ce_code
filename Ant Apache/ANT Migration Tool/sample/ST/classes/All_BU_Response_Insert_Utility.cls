/**
Name        :   All_BU_Response_Insert_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   All_BU_Response_Insert_Utility
*/
global class All_BU_Response_Insert_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,Staging_BU_Response__c> questionToObjectMap;
    public Map<String,Integer> questionToResponseFieldNoMap;
    
    global All_BU_Response_Insert_Utility(String teamInstance) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        selectedTeamInstance = teamInstance;
        
        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Team_Instance__r.name,bti.id);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id, Name, CurrencyIsoCode, Acc_Pos__c, BU__c, Brand_ID__c, Brand__c, Business_Unit__c, Cycle2__c, Cycle__c, External_ID__c, Is_Active__c, LineFormula__c, Line__c, Physician__c, Physician__r.AccountNumber, Position_Account__c, Position__c, Profiling_Response__c, Response10__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Survey_Master__c, Team_Instance__c, Team__c, is_inserted__c, is_updated__c FROM Staging_BU_Response__c  Where Team_Instance__r.name = \'' + teamInstance + '\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_BU_Response__c> scopeRecs) {
        
        system.debug('Hello++++++++++++++++++++++++++++');
        //Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Set<string> brandIDSet = new Set<string>();
        
        //Map<string,id> posCodeIdMap = new Map<string,id>();
        //Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        Map<string,id> accountNumberIdMap = new Map<string,id>();
        Map<string,id> brandIdMap = new Map<string,id>();
        //Map<string,id> posTeamInstMap = new Map<string,id>();
        Map<string,List<id>> accountNumbertoPosAccIdsMap = new Map<string,List<id>>();
        
        
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;

        for(Staging_BU_Response__c StagingBUresp : scopeRecs){      //Positions Set
            /*if(!string.IsBlank(StagingBUresp.Territory_ID__c)){
                posCodeSet.add(StagingBUresp.Territory_ID__c);
            }*/
            if(!string.IsBlank(StagingBUresp.Physician__c)){            //Accounts Set
                accountNumberSet.add(StagingBUresp.Physician__c);
            }
            /*if(!string.IsBlank(StagingBUresp.BRAND_ID__c)){            //Product_Catalog__c Set
                brandIDSet.add(StagingBUresp.BRAND_ID__c);
            }*/
                   
        }
        String key;
        
        
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber from Account where id IN :accountNumberSet ]){ //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of accounts and their lookups
            
        }

        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstances = [select id, Include_Secondary_Affiliations__c from AxtriaSalesIQTM__Team_Instance__c where Name = :selectedTeamInstance];

        if(allTeamInstances[0].Include_Secondary_Affiliations__c)
        {
            for(AxtriaSalesIQTM__Position_Account__c acc : [Select id,AxtriaSalesIQTM__Account__r.AccountNumber from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN :accountNumberSet and (AXTRIASALESIQTM__ASSIGNMENT_STATUS__C ='Active' OR AXTRIASALESIQTM__ASSIGNMENT_STATUS__C ='Future Active' ) and AxtriaSalesIQTM__Team_Instance__r.name = :selectedTeamInstance and AxtriaSalesiqtm__Account_Alignment_Type__c != 'Explicit' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != '00000']){ //,Team_Name__c   and Team_Name__c=: selectedTeam
                List<ID> tempIDslist = new List<ID>();
                if(accountNumbertoPosAccIdsMap.containsKey(acc.AxtriaSalesIQTM__Account__r.AccountNumber)){
                    tempIDslist = accountNumbertoPosAccIdsMap.get(acc.AxtriaSalesIQTM__Account__r.AccountNumber);
                }
                tempIDslist.add(acc.id);
                accountNumbertoPosAccIdsMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber,tempIDslist);
            }
            

        }
        else
        {
            for(AxtriaSalesIQTM__Position_Account__c acc : [Select id,AxtriaSalesIQTM__Account__r.AccountNumber from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN :accountNumberSet and (AXTRIASALESIQTM__ASSIGNMENT_STATUS__C ='Active' OR AXTRIASALESIQTM__ASSIGNMENT_STATUS__C ='Future Active' ) and AxtriaSalesIQTM__Team_Instance__r.name = :selectedTeamInstance and (AxtriaSalesIQTM__Segment_1__c = 'Primary' OR AxtriaSalesIQTM__Segment_1__c = '') and AxtriaSalesiqtm__Account_Alignment_Type__c != 'Explicit' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != '00000']){ //,Team_Name__c   and Team_Name__c=: selectedTeam
                List<ID> tempIDslist = new List<ID>();
                if(accountNumbertoPosAccIdsMap.containsKey(acc.AxtriaSalesIQTM__Account__r.AccountNumber)){
                    tempIDslist = accountNumbertoPosAccIdsMap.get(acc.AxtriaSalesIQTM__Account__r.AccountNumber);
                }
                tempIDslist.add(acc.id);
                accountNumbertoPosAccIdsMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber,tempIDslist);
            }
            

        }
        
        //Staging_BU_Response__c tempBUResponseRec = new Staging_BU_Response__c();
        List<BU_Response__c> BUResponseList = new List<BU_Response__c>();
        
        for(Staging_BU_Response__c StagingBUresp : scopeRecs){
            //tempBUResponseRec = new Staging_BU_Response__c();
            List<ID> listofPosAccs = new List<ID>();
            
            Integer noOfPosAccs = 0;
            
            if(accountNumbertoPosAccIdsMap.get(StagingBUresp.Physician__r.AccountNumber) != null){
                noOfPosAccs = accountNumbertoPosAccIdsMap.get(StagingBUresp.Physician__r.AccountNumber).size();
                listofPosAccs = accountNumbertoPosAccIdsMap.get(StagingBUresp.Physician__r.AccountNumber);
                
                
                for(ID posAccID : listofPosAccs){
                    BU_Response__c tempBUResponseRec = new BU_Response__c();
                    //tempBUResponseRec = StagingBUresp.clone();
                    //tempBUResponseRec.Name = StagingBUresp.Name;
                    tempBUResponseRec.CurrencyIsoCode = StagingBUresp.CurrencyIsoCode;
                    tempBUResponseRec.Brand_ID__c = StagingBUresp.Brand_ID__c;
                    tempBUResponseRec.Brand__c = StagingBUresp.Brand__c;
                    tempBUResponseRec.Business_Unit__c = StagingBUresp.Business_Unit__c;
                    tempBUResponseRec.Cycle2__c = StagingBUresp.Cycle2__c;
                    tempBUResponseRec.Cycle__c = StagingBUresp.Cycle__c;
                    tempBUResponseRec.External_ID__c = StagingBUresp.External_ID__c+posAccID;
                    tempBUResponseRec.Is_Active__c = StagingBUresp.Is_Active__c;
                    tempBUResponseRec.Line__c = StagingBUresp.Line__c;
                    tempBUResponseRec.Physician__c = StagingBUresp.Physician__c;
                    //tempBUResponseRec.Position_Account__c = StagingBUresp.Position_Account__c;
                    tempBUResponseRec.Profiling_Response__c = StagingBUresp.Profiling_Response__c;
                    
                    if(StagingBUresp.Response10__c == 'NULL' || StagingBUresp.Response10__c == 'Null' || StagingBUresp.Response10__c == 'null')
                        StagingBUresp.Response10__c = '9999';
                    
                    if(StagingBUresp.Response1__c == 'NULL' || StagingBUresp.Response1__c == 'Null' || StagingBUresp.Response1__c == 'null')
                        StagingBUresp.Response1__c = '';
                    if(StagingBUresp.Response2__c == 'NULL' || StagingBUresp.Response2__c == 'Null' || StagingBUresp.Response2__c == 'null')
                        StagingBUresp.Response2__c = '';
                    if(StagingBUresp.Response3__c == 'NULL' || StagingBUresp.Response3__c == 'Null' || StagingBUresp.Response3__c == 'null')
                        StagingBUresp.Response3__c = '';
                    if(StagingBUresp.Response4__c == 'NULL' || StagingBUresp.Response4__c == 'Null' || StagingBUresp.Response4__c == 'null')
                        StagingBUresp.Response4__c = '';
                    if(StagingBUresp.Response5__c == 'NULL' || StagingBUresp.Response5__c == 'Null' || StagingBUresp.Response5__c == 'null')
                        StagingBUresp.Response5__c = '';
                    if(StagingBUresp.Response6__c == 'NULL' || StagingBUresp.Response6__c == 'Null' || StagingBUresp.Response6__c == 'null')
                        StagingBUresp.Response6__c = '';
                    if(StagingBUresp.Response7__c == 'NULL' || StagingBUresp.Response7__c == 'Null' || StagingBUresp.Response7__c == 'null')
                        StagingBUresp.Response7__c = '';
                    if(StagingBUresp.Response8__c == 'NULL' || StagingBUresp.Response8__c == 'Null' || StagingBUresp.Response8__c == 'null')
                        StagingBUresp.Response8__c = '';
                    if(StagingBUresp.Response9__c == 'NULL' || StagingBUresp.Response9__c == 'Null' || StagingBUresp.Response9__c == 'null')
                        StagingBUresp.Response9__c = '';
                        
                        
                        
                    tempBUResponseRec.Response10__c = StagingBUresp.Response10__c;
                    tempBUResponseRec.Response1__c = StagingBUresp.Response1__c;
                    tempBUResponseRec.Response2__c = StagingBUresp.Response2__c;
                    tempBUResponseRec.Response3__c = StagingBUresp.Response3__c;
                    tempBUResponseRec.Response4__c = StagingBUresp.Response4__c;
                    tempBUResponseRec.Response5__c = StagingBUresp.Response5__c;
                    tempBUResponseRec.Response6__c = StagingBUresp.Response6__c;
                    tempBUResponseRec.Response7__c = StagingBUresp.Response7__c;
                    tempBUResponseRec.Response8__c = StagingBUresp.Response8__c;
                    tempBUResponseRec.Response9__c = StagingBUresp.Response9__c;
                    tempBUResponseRec.Survey_Master__c = StagingBUresp.Survey_Master__c;
                    tempBUResponseRec.Team_Instance__c = StagingBUresp.Team_Instance__c;
                    tempBUResponseRec.Team__c = StagingBUresp.Team__c;
                    tempBUResponseRec.is_inserted__c = StagingBUresp.is_inserted__c;
                    tempBUResponseRec.is_updated__c = StagingBUresp.is_updated__c;
                    
                    //tempBUResponseRec.External_ID__c = '';
                    tempBUResponseRec.Position_Account__c = posAccID;
                    
                    BUResponseList.add(tempBUResponseRec);
                    
                }
            
            }
            
        }
        
        upsert BUResponseList External_ID__c;

    }

    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new All_BU_Response_Insert_Utility(selectedTeamInstance),2000);  //to duplicate for position account (based on account)
    }
}