@isTest
public class update_PositionGeographyTest {

    @isTest
    static void updatePosGeo(){
        List<AxtriaSalesIQTM__Geography__c> geo = new List<AxtriaSalesIQTM__Geography__c>();
        for(Integer i = 1; i<10 ; i++){
            AxtriaSalesIQTM__Geography__c zip = new AxtriaSalesIQTM__Geography__c();
            zip.name = '0000'+i;
            geo.add(zip);
        }
        insert geo;
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(name = 'T1');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_instance__c();
        ti.name = 'T1-Q1';
       	ti.AxtriaSalesIQTM__Team__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        ti.AxtriaSalesIQTM__IC_EffstartDate__c = date.today() - 20 ;
        ti.AxtriaSalesIQTM__IC_EffEndDate__c = date.today() + 100 ;
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'XYZ';
        pos.AxtriaSalesIQTM__Team_iD__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = [Select id from AxtriaSalesIQTM__Team_Instance__c where name = 'T1-Q1'][0].Id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'X';
        
        insert pos;
        
        AxtriaSalesIQTM__Position_Team_Instance__c pti = new AxtriaSalesIQTM__Position_Team_Instance__c(); 
        pti.AxtriaSalesIQTM__Team_Instance_ID__c = ti.id;
        pti.AxtriaSalesIQTM__Position_ID__c = pos.id;
        pti.AxtriaSalesIQTM__Effective_Start_Date__c =date.today() - 20;
        pti.AxtriaSalesIQTM__Effective_End_Date__c= date.today() + 20;
        insert pti;
        
        List<temp_Zip_Terr__c> zipTerr = new List<temp_Zip_Terr__c >();
        for(Integer j =1; j<9; j++){
            temp_Zip_Terr__c zt = new temp_Zip_Terr__c();
            zt.Territory_ID__c = 'X';
            zt.Position__c = pos.id;
            zt.Zip_Name__c = '000'+j;
            zt.Geography__c = geo[j].id;
            zt.Team_Name__c = 'T1';
            zt.Team__c = team.id;
            zt.team_instance__c = ti.id;
            
            zipTerr.add(zt);
        }
        insert zipTerr;
        Test.startTest();
    
    	Database.executeBatch(new update_PositionGeography(team.id,ti.id));
    
    	Test.stopTest();
    }
}