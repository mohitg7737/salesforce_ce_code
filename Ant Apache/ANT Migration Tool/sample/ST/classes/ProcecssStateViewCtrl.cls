public with sharing class ProcecssStateViewCtrl {
	public string pathvalue {get;set;}

	public pagereference redirecttopage(){
		System.debug('=============pathvalue='+pathvalue);
		
		if(pathvalue != null)
		{
			PageReference	pg = new pagereference(pathvalue);
			pg.setRedirect(true);
			return pg; 
		}
		else
		{
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Cannot directly jump to other steps, Click Save/Save & Next to proceed'));
		}

		return null;
	}
    
}