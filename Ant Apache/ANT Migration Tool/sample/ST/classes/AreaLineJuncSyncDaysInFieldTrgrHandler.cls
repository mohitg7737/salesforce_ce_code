public with sharing class AreaLineJuncSyncDaysInFieldTrgrHandler {
	public  static void UpdateArealinejunction(list<AxtriaSalesIQTM__Position_Product__c>newtrigger){
		system.debug('=============List of AxtriaSalesIQTM__Position_Product__c:'+newtrigger);
		list<AxtriaSalesIQTM__Position_Product__c>posprodlist = new list<AxtriaSalesIQTM__Position_Product__c>();
		map<string,decimal>posprdidmap = new map<string,decimal>();
		map<string,AxtriaSalesIQTM__Position_Product__c>posiotionproductmap = new map<String,AxtriaSalesIQTM__Position_Product__c>();
		posprodlist.addAll(newtrigger);
		system.debug('======posprodlist=='+posprodlist);
		if(posprodlist!=null && posprodlist.size()>0){
			for(AxtriaSalesIQTM__Position_Product__c pp :posprodlist){
				if(!posprdidmap.containskey(pp.id)){
					posprdidmap.put(pp.id,pp.Effective_Days_in_Field_Formula__c);
				}
				if(!posiotionproductmap.containskey(pp.id)){
					posiotionproductmap.put(pp.id,pp);
				}
			}

			List<Area_Line_Junction__c>UpdateList = new list<Area_Line_Junction__c>();
			List<Area_Line_Junction__c>arealist = [select id,Days_In_Field__c,Position_Product__c from Area_Line_Junction__c where Position_Product__c IN:posprodlist];
			system.debug('========List of Area line junction records::'+arealist);
			system.debug('========posprdidmap::'+posprdidmap);
			system.debug('========posiotionproductmap::'+posiotionproductmap);

			if(arealist!=null && arealist.size()>0){
				for(Area_Line_Junction__c area: arealist){
					/*if(posprdidmap.containskey(area.Position_Product__c)){
						area.Days_In_Field__c = posprdidmap.get(area.Position_Product__c);
						UpdateList.add(area);
					}*/
					if(posiotionproductmap.containskey(area.Position_Product__c)){
						AxtriaSalesIQTM__Position_Product__c obj = posiotionproductmap.get(area.Position_Product__c);
						if(obj.Effective_Days_in_Field_Formula__c !=null){
							area.Days_In_Field__c = obj.Effective_Days_in_Field_Formula__c;
						}
						if(obj.Calls_Day__c !=null){
							area.Calls_Per_Day__c = obj.Calls_Day__c;
						}
						
						UpdateList.add(area);
					}

				}
				system.debug('====UpdateList==='+UpdateList);
				if(UpdateList!=null && UpdateList.size()>0){
					update UpdateList;
				}
				

			}

		}


	}
    
}