global class AZ_IntegrationUtility_EU implements Database.Batchable<sObject>{
    
    List<SIQ_MC_Cycle_Plan_Product_vod_O__c> mcCyclePlanProduct;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpProRecs;
    List<String> allChannels ;
    String queryString;
    string teamInstanceSelected;
    List<String> allString;

    global AZ_IntegrationUtility_EU(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        queryString = 'select id, Segment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and  AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null';
        
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        allString = new List<String>();
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Product_vod( List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        system.debug('++++++++++++ mcCyclePlanProduct'+ mcCyclePlanProduct);
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        pacpProRecs = scopePacpProRecs;
        Set<String> allRecs = new Set<String>();
        Set<String> allPros = new Set<String>();
        
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : pacpProRecs)
        {
            allPros.add(pacpPro.P1__c);
        }
        
        List<Product_Catalog__c> pc = [select id, Name, External_ID__c from Product_Catalog__c where Name  in :allPros];
        
        Map<String, String> proToID = new Map<String,String>();
        
        for(Product_Catalog__c pcRec : pc)
        {
            proToID.put(pcRec.Name, pcRec.External_ID__c);
        }
        
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : pacpProRecs)
        {
            string teamPos = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlan         =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlanTarget   =   teamPos + '_' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
            
            String accNumPos = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            Integer f2fCalls = Integer.valueOf(pacpPro.Final_TCF__c);
            String channel = 'F2F';

            


            String productExternalID= proToID.get(pacpPro.P1__c);
            String productName = pacpPro.P1__c;

            SIQ_MC_Cycle_Plan_Product_vod_O__c mcpp = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
                        
            string cycleChannel;
            
            cycleChannel      =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channel + '-Call2_vod__c';
            
            mcpp.SIQ_Cycle_Plan_Channel_vod__c = teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
            
            if(channel == 'F2F')
            {
                mcpp.SIQ_Product_Activity_Goal_vod__c = Decimal.valueof(f2fCalls);    
            }
           
            
            mcpp.SIQ_Cycle_Product_vod__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '__' + channel + '__' +productExternalID + '_'+ productName;


            mcpp.SIQ_Product_Activity_Max_vod__c = mcpp.SIQ_Product_Activity_Goal_vod__c;
            
            mcpp.SIQ_External_Id_vod__c = mcpp.SIQ_Cycle_Plan_Channel_vod__c + productExternalID;
            mcpp.SIQ_Segment_AZ__c = pacpPro.Segment__c;
            
            mcpp.External_ID_Axtria__c = mcpp.SIQ_External_Id_vod__c;
            mcpp.Rec_Status__c = 'Updated';
            mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            
            if(!allRecs.contains(mcpp.SIQ_External_Id_vod__c))
            {
                allRecs.add(mcpp.SIQ_External_Id_vod__c);
                mcCyclePlanProduct.add(mcpp);
            }

            
        }
           

        
        system.debug('+++++++++ '+ mcCyclePlanProduct);
        system.debug('+++++++++++++'+mcCyclePlanProduct.size());
        
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        //List<String> allString = new List<String>();
        create_MC_Cycle_Plan_Product_vod(scopePacpProRecs);
        
        upsert mcCyclePlanProduct External_ID_Axtria__c;
        //insert newTempObj;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new MarkMCCPproductDeleted_EU());
    }
}