@isTest
private class TestRemainingTriggers {

    private static testMethod void test() {
       AxtriaSalesIQTM__Product__c p = new AxtriaSalesIQTM__Product__c();
        p.name = 'abc';
        p.AxtriaSalesIQTM__Effective_End_Date__c = date.parse('01/01/2019');
        p.AxtriaSalesIQTM__Effective_Start_Date__c = date.parse('01/01/2018');
        p.AxtriaSalesIQTM__Product_Code__c = 'Brilique_ES';
        insert p;
        
        AxtriaSalesIQTM__Product__c p2 = new AxtriaSalesIQTM__Product__c();
        p2.id=p.id;
        p2.name = 'abc2';
        update p2;
        
        
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.name = 'abc';
        e.AxtriaSalesIQTM__FirstName__c='abc';
        e.AxtriaSalesIQTM__Last_Name__c='abc';
        insert e;
        
        /*AxtriaSalesIQTM__Employee__c e2 = new AxtriaSalesIQTM__Employee__c();
        e2.id=e.id;
        e2.name = 'abc2';
        update e2;*/
        
        
        Parameter__c pm = new Parameter__c();
        pm.id=pm.id;
        pm.name = 'abc';
        insert pm;
        
        Parameter__c pm2 = new Parameter__c();
        pm2.id=pm.id;
        pm2.name = 'abc2';
        update pm2;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c();
        ti.name='abc';
        ti.AxtriaSalesIQTM__Team__c=team.id;
        insert ti;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Status__c='Pending';
        cr.AxtriaSalesIQTM__Team_Instance_ID__c=ti.id;
        insert cr;
        
        AxtriaSalesIQTM__Change_Request__c cr2 = new AxtriaSalesIQTM__Change_Request__c();
        cr2.id=cr.id;
        cr2.AxtriaSalesIQTM__Status__c='Approved';
        update cr2;
        
        AxtriaSalesIQTM__Change_Request__c cr3 = new AxtriaSalesIQTM__Change_Request__c();
        cr3.id=cr.id;
        delete cr3;
        
       
    } 

}