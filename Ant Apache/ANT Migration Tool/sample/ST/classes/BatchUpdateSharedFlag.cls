global class BatchUpdateSharedFlag implements Database.Batchable<sObject> {
    public String query;
    public String line;
    public String brand;
    public String businessUnit;
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;

    global BatchUpdateSharedFlag(String ruleId){
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__r.Name, Line_2__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        line = rule.Line_2__c;
        brand = rule.Brand_Lookup__r.Name;
        businessUnit = rule.Team_Instance__c;
        this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c, Line__c ';
        this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
        this.query += 'WHERE P1_Original__c = \'' + brand + '\' AND AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
       /* map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c> physician2callplan = new map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        set<String> callPlans = new set<String>();
        for(Sobject cp: scope){
            physician2callplan.put((String)cp.get('AxtriaSalesIQTM__Account__c'), (AxtriaSalesIQTM__Position_Account_Call_Plan__c)cp);
            callPlans.add((String)cp.get('Id'));
        }
        
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callplan2Update = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        set<String> checkDuplicate = new set<String>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c cp: [SELECT Id, AxtriaSalesIQTM__Account__c, Share__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Account__c IN: physician2callplan.keySet() AND AxtriaSalesIQTM__Team_Instance__r.Name =: businessUnit AND AxtriaSalesIQTM__Position__c != null  AND AxtriaSalesIQTM__isIncludedCallPlan__c = true]){
            if(physician2callplan.containsKey(cp.AxtriaSalesIQTM__Account__c)){
                cp.Share__c = true;
                AxtriaSalesIQTM__Position_Account_Call_Plan__c cp2 = physician2callplan.get(cp.AxtriaSalesIQTM__Account__c);
                cp2.Share__c = true;
                callplan2Update.add(cp);
                if(!checkDuplicate.contains((String)cp2.get('Id'))){
                    callplan2Update.add(cp2);
                    checkDuplicate.add((String)cp2.get('Id'));
                }
            }
        }
        
        if(callplan2Update != null && callplan2Update.size() > 0){
            CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
            update callplan2Update;
            CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.
        }*/
        
        Set<String> allAccountNumbers = new Set<String>();
        
        Set<String> allProducts = new Set<String>();
        Set<String> uniqueKey = new Set<String>();

        for(Sobject cp: scope){
            allAccountNumbers.add((String)cp.get('AxtriaSalesIQTM__Account__c'));
            allProducts.add((String)cp.get('P1_Original__c'));
            String unKey = (String)cp.get('AxtriaSalesIQTM__Account__c') + '_' + (String)cp.get('P1_Original__c');
            uniqueKey.add(unKey);
        }
        
        List<Account> allAccounts = [select id, (select id, AxtriaSalesIQTM__Account__c, P1_Original__c, Share__c from AxtriaSalesIQTM__Position_Account_CallPlans__r where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c = :businessUnit AND AxtriaSalesIQTM__isIncludedCallPlan__c = true) from Account where id in :allAccountNumbers];
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpRec;
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
        for(Account acc : allAccounts)
        {
            pacpRec = acc.AxtriaSalesIQTM__Position_Account_CallPlans__r;
            
            if(pacpRec.size()>1)
            {
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                {
                    String unKey = pacp.AxtriaSalesIQTM__Account__c + '_' + pacp.P1_Original__c;

                    if(uniqueKey.contains(unKey))
                    {
                        pacp.Share__c = true;
                        sharedPacp.add(pacp);                        
                    }
                    else
                    {
                        pacp.Share__c = false;
                        sharedPacp.add(pacp);                           
                    }
                }
            }
            else
            {
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                {
                    pacp.Share__c = false;
                    sharedPacp.add(pacp);
                }
            }
        }
        
        if(sharedPacp != null && sharedPacp.size() > 0){
            CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
            customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
            system.debug('==========customsetting========'+customsetting);
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            update customsetting ;
            
            update sharedPacp;
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
            update customsetting ;
            CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new BatchCreateParentPacp(businessUnit), 500);
    }
}