global class BatchOutBoundCustomerSegment implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    global Date today=Date.today();
    public String UnAsgnPos='00000';
    public String UnAsgnPos1='0';
    public List<String> posCodeList=new List<String>();
    public map<String,String>Countrymap {get;set;}
    public map<String,String>Productmap {get;set;} 
    public set<String> Uniqueset {get;set;}                                
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}                            

    global BatchOutBoundCustomerSegment() {
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        
        for(Custom_Scheduler__c cs:mktList){
             if(cs.Status__c==true && cs.Schedule_date__c!=null){
                  if(cs.Schedule_date__c>today.addDays(1)){
                        mkt.add(cs.Marketing_Code__c);
                  }else{
                        //update Custom scheduler record
                       cs.Status__c = False;
                       lsCustomSchedulerUpdate.add(cs);
                  }
             }
        }
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
        String cycle=null;
         if(cycleList.size()>0){
       cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         }
        Countrymap = new map<String,String>();
         Productmap = new map<String,String>();
        Uniqueset = new set<String>();                            
         for(Product_Catalog__c pc: [select id,Team_Instance__c,Name,External_ID__c from Product_Catalog__c]){
         String key=pc.Team_Instance__c+'_'+pc.Name;
            if(!Productmap.containskey(key)){
                Productmap.put(key,pc.External_ID__c);
            }
        }                                                                
                                                                                            
                                            
                                                   

        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound Cust Segment Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound Cust Segment Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
          sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //this.query = query; 
        //SIQ_Country_Code__c,SIQ_Customer_Class__c,SIQ_Event__c,SIQ_Marketing_Code__c,SIQ_Salesforce_Name__c,SIQ_Segment_Type__c ----???????

        query = 'Select id,P1__c,CreatedDate,AxtriaARSnT__Party_ID__r.AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Account_HCP_NUmber__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,LastModifiedDate,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c,' +
                'Segment__c,AxtriaSalesIQTM__Team_Instance__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,Final_TCF__c,SystemModstamp,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.Country_Name__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c '+
                ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__isAccountTarget__c=true and AxtriaSalesIQTM__isincludedCallPlan__c = true and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList ';
        if(lastjobDate!=null){
            query = query + ' And LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        System.debug('query'+ query);
        
        //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundCustomerSegment());
    } 
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
        list<SIQ_Customer_Segment_O__c>CustomerList = new list<SIQ_Customer_Segment_O__c>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
                                                                                                                                                                                                
            String key = pacp.id;
            if(!Uniqueset.contains(Key)){                            
            SIQ_Customer_Segment_O__c obj = new SIQ_Customer_Segment_O__c();
            obj.SIQ_Brand_Name__c= pacp.P1__c;
            if(Productmap.containskey(pacp.AxtriaSalesIQTM__Team_Instance__c+'_'+pacp.P1__c)){
             obj.SIQ_Brand_Identifier__c=Productmap.get(pacp.AxtriaSalesIQTM__Team_Instance__c+'_'+pacp.P1__c);
             }
             else{
             obj.SIQ_Brand_Identifier__c=pacp.P1__c;
             }
            obj.Name=pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c+'_'+pacp.Account_HCP_NUmber__c+'_'+String.valueOf(pacp.Final_TCF__c);
            obj.SIQ_Position_Code__c=pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c;       
           // obj.SIQ_Country_Code__c= Countrymap.get(pacp.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
            obj.SIQ_Country_Code__c=pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            obj.SIQ_Created_Date__c = pacp.CreatedDate;
            obj.SIQ_CUSTOMER_ID__c = pacp.Account_HCP_NUmber__c;
            obj.SIQ_Customer_Class__c=pacp.AxtriaSalesIQTM__Account__r.Type;
           // obj.SIQ_Effective_End_Date__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
            obj.SIQ_Effective_Start_Date__c = pacp.AxtriaARSnT__Party_ID__r.AxtriaSalesIQTM__Effective_Start_Date__c;
            //obj.SIQ_Event__c//no
            //obj.SIQ_Marketing_Code__c = Countrymap.get(pacp.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
             obj.SIQ_Marketing_Code__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
            obj.SIQ_Salesforce_Name__c=pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
            obj.SIQ_Segment__c = pacp.Segment__c;
            obj.SIQ_Objective__c= String.valueOf(pacp.Final_TCF__c); 
           // obj.SIQ_Position_Code__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;                                                        
           // obj.SIQ_Segment_Type__c
            obj.Unique_Id__c =pacp.id;
            obj.SIQ_Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__r.name;
           // obj.SIQ_Updated_Date__c = pacp.LastModifiedDate;
           obj.SIQ_Updated_Date__c =pacp.SystemModstamp;
            CustomerList.add(obj);
            recordsProcessed++;
            Uniqueset.add(Key);                    
            } 
        }
        Upsert CustomerList Unique_Id__c;
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
       //Database.ExecuteBatch( new BatchOutBoundPositionProduct(),200);
        Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
             }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList=[Select Id FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update pacpList;
        }


    }
}