public with sharing class SummaryCtlr extends BusinessRule implements IBusinessRule{
    public List<Rule_Parameter__c> ruleParameters {get;set;}
    public Map<String, List<Step__c>> ruleSteps {get;set;}
    public list<String> summaryPath{get;set;}

    public String pageName {get;set;}
    
    public SummaryCtlr() {
        init();
        summaryPath = new list<String>{'Compute Values', 'Compute Segment', 'Compute TCF', 'Compute Accessibility'};
        ruleParameters = [SELECT Id, Name, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        ruleSteps = new Map<String, List<Step__c>>();
        for(String path : pathList){
            ruleSteps.put(path, new List<Step__c>());
        }

        for(Step__c step: [SELECT Id, Name, Sequence__c, UI_Location__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Sequence__c]){
            List<Step__c> steps = ruleSteps.get(step.UI_Location__c);
            steps.add(step);
            ruleSteps.put(step.UI_Location__c, steps);
        }
    }

    public void save(){

    }

    public PageReference redirectToPageRefer()
    {
        system.debug('+++++++++ Hey Page Name is '+ pageName);
        PageReference pref = new PageReference(pageName);
        pref.setRedirect(true);
        return pref;
    }

    public PageReference saveandnext(){
        PageReference pref = new PageReference('/apex/Business_Rule_list');
        ruleObject.State__c = 'Ready';
        update ruleObject;
        return pref;
    }
}