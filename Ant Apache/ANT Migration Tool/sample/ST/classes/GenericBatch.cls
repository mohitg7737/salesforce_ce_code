global class GenericBatch implements Database.Batchable<sObject> {
    global string Objname;
    global string fieldvalue;
    global string Query;
    global string fieldapi {get;set;}

    global GenericBatch(string ObjApiName , string fieldapiname,string value) {
        Query='';
        Objname='';
        fieldapi='';
        fieldvalue = '';

        Query='Select id from ';
        if(ObjApiName!=null && ObjApiName!=''){
            Objname = ObjApiName;
            system.debug('===========obj Name::'+Objname);
            Query += Objname ;
        }
        if(fieldapiname!=null && fieldapiname!=''){
            fieldapi=fieldapiname;
            fieldvalue = value;
            system.debug('=======fieldapi='+fieldapi);
            system.debug('=======fieldvalue='+fieldvalue);
            Query+= ' Where '+fieldapi +'=:fieldvalue';

        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('=========Query is::'+query);

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {

      Database.DeleteResult[] DR_Dels = Database.delete(scope , false);
      Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 

    }

    global void finish(Database.BatchableContext BC) {
        //Dtabase.ExcuteBatch(newbatch,200);
    }
}