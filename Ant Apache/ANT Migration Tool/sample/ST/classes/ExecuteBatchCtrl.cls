public with sharing class ExecuteBatchCtrl {
    public ExecuteBatchCtrl() {
        
    }
    
    public void runEmployeeFeed(){
       database.executeBatch(new DeleteCurrentPersonFeed(),200);
        
    }
    public void runClassForGlobalAdmins(){
        InsertUserPositionRecords up = new InsertUserPositionRecords();
        up.insertUserPositionsBasesOnProfiles();
    }
    public void callCustomerSegment(){
        database.executeBatch(new BatchOutboundCustomerSegment(),200);
    }
    
    public void callOutboundWorkspace(){
        database.executeBatch(new BatchOutboundWorkspace(),200);
    }    
    public void callOutBoundEmployee(){
        database.executeBatch(new BatchOutboundEmployee(),200);
    }
    
    public void callOutBoundGeography(){
        database.executeBatch(new BatchOutBoundGeography2(),200);
    }
    public void callOutBoundPosEmp(){
        database.executeBatch(new BatchOutBoundPosEmp(),200);
    }
    public void callOutBoundPosition(){
        database.executeBatch(new BatchOutBoundPosition(),200);
    }
    public void callOutBoundUpdPosGeography(){
        database.executeBatch(new batchOutBoundPositionGeography(),200);
    }
    public void callOutboundUpdateteamInstance(){
        database.executeBatch(new BatchOutboundUpdteamInstance(),200);
    }
    public void callPositionProductOutbound(){
        //database.executeBatch(new BatchOutBoundUpdPosGeography(),200);
        PositionProductOutbound ppo=new PositionProductOutbound();
    }
    public void callPositionAccountOutbound(){
        //database.executeBatch(new BatchOutBoundUpdPosGeography(),200);
         database.executeBatch(new BatchOutBounUpdPositionAccount(),200);
    }
   
    public void callInboundAccount(){
        database.executeBatch(new BatchInboundUpdSIQAccounts(),200);
    }
    public void callInboundAccAff(){
        database.executeBatch(new BatchInBoundUpdAccAffiliation(),200);
    }
    public void callInBoundAccAddress(){
        database.executeBatch(new BatchInBoundUpdAccAddress(),200);
        
    }
     public void callOutBoundMergeUnmerge(){
        MergeUnmergeDelta md=new MergeUnmergeDelta();
    }
}