global class ATL_Delete_Recs implements Database.Batchable<sObject> {
    
    public String query;
    public List<String> allCountries;

    global ATL_Delete_Recs(List<String> allCountries) {

        allCountries = new List<String>(allCountries);
        this.query = 'select id from Staging_ATL__c where Country__c in :allCountries';
    
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) {
        delete scope;
    }

    global void finish(Database.BatchableContext BC) 
    {

        Database.executeBatch(new ATL_TransformationBatch(allCountries));
    }
}