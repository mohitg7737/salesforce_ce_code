@istest
public with sharing class NewPhysicianUniverseCtrl_Test {
    
    static testMethod void callPlanTest(){
        
        Profile p = [select id from Profile where name = 'REP1' or name = 'System Administrator' order by name limit 1];
        Profile p2 = [select id from Profile where name = 'DM1' or name = 'System Administrator' order by name limit 1];
        
        User tUser = new User(Alias = 'Rep', Email='repuser@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@astrazeneca.com');
        insert tUser;
        
        User tUser1 = new User(Alias = 'Rep', Email='repuser1@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset1@astrazeneca.com');
        insert tUser1; 
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        User tUser2 = new User(Alias = 'Rep', Email='repuser1@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset2@astrazeneca.com');
        
        insert tUser2; 
        
        User dmuser = new User(Alias = 'DM', Email='repuser5@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p2.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset6@astrazeneca.com');
        
        insert dmuser; 
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        
        Line__c line = new line__c();
        line.Name = 'Mistral';
        line.Team__c = team.id;
        insert line;
        
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
       // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        posNation.Line__c = line.id;
        
        insert posNation;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
        pos2.Line__c= line.id;
       // pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='3';
        insert pos2;
                
        AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
        posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
        posdm.Name = 'Chico CA_SPEC';
        posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
        posdm.Line__c = line.id;
        //posdm.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='2';
        insert posdm;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos.Name = 'Chico CA_SPEC';
        pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos;


        system.debug('pos :::'+pos);
        
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.Name = 'TI';
        insert teamIns;

        Product_Catalog__c pc = new Product_Catalog__c();
        pc.Product_Code__c = 'P1';
        pc.Team_Instance__c = teamIns.ID;
        pc.CurrencyIsoCode = 'USD';
        insert pc;
        
        Team_Instance_Product_AZ__c tip = new Team_Instance_Product_AZ__c();
        tip.Team_Instance__c = teamIns.ID;
        tip.Product_Catalogue__c = pc.ID;

        insert tip;

        
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        insert posTeam;
        
        system.debug('posTeam :::'+posTeam);
        //string soqlfields = 'Picklist1_Updated__c,Picklist1_Segment_Approved__c';
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamAtt = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamAtt.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        teamAtt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamAtt.AxtriaSalesIQTM__isRequired__c = true;
        teamAtt.AxtriaSalesIQTM__Attribute_API_Name__c = 'name';
        teamAtt.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        insert teamatt;
        
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c teamObjDetail = new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
        teamObjDetail.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = teamAtt.id;
        insert teamObjDetail;
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        acc.Marketing_Code__c='ES';
        insert acc;
        
        
        Account acc2 = new Account();
        acc2.Name = 'a b';
        acc2.AxtriaSalesIQTM__External_Account_Number__c='456';
        acc2.BillingStreet='abc';
        acc2.AxtriaSalesIQTM__FirstName__c='a';
        acc2.Marketing_Code__c='ES';
        insert acc2;
        
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        //posAcc.isAccountTarget__c = true;
        posAcc.AxtriaSalesIQTM__Position__c = pos.id;
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        posAcc.AxtriaSalesIQTM__Account__c = acc2.id;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
        //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        posAcc.AxtriaSalesIQTM__Picklist1_Updated__c = '12';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
        posAcc.AxtriaSalesIQTM__Metric6__c = 4;
        posAcc.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
        posAcc.Line__c= line.id;
        insert posAcc;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc5 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        //posAcc.isAccountTarget__c = true;
        posAcc5.AxtriaSalesIQTM__Position__c = pos.id;
        posAcc5.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        posAcc5.AxtriaSalesIQTM__Account__c = acc.id;
        posAcc5.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,11);
        posAcc5.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,18);
        //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        posAcc5.AxtriaSalesIQTM__Picklist1_Updated__c = '24';
        posAcc5.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '11';
        posAcc5.AxtriaSalesIQTM__Picklist1_Segment__c = '13';
        posAcc5.AxtriaSalesIQTM__Metric6__c = 6;
        posAcc5.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
        posAcc5.Line__c= line.id;
        insert posAcc5;
        AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        posAcc1.AxtriaSalesIQTM__Position__c = pos.id;
        posAcc1.AxtriaSalesIQTM__isAccountTarget__c = false;
        posAcc1.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        posAcc1.AxtriaSalesIQTM__Account__c = acc.id;
        posAcc1.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc1.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
        posAcc1.AxtriaSalesIQTM__isincludedCallPlan__c = true;
        posAcc1.AxtriaSalesIQTM__Picklist1_Updated__c = '6';
        posAcc1.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
        posAcc1.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '0';
        posAcc1.AxtriaSalesIQTM__Metric6__c = 4;
        posAcc1.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
        posAcc1.Line__c= line.id;
        insert posAcc1;
        
        list<String> posAccList = new list<string>();
        for(integer i=0; i<2; i++){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
            pacp.AxtriaSalesIQTM__Position__c = pos.id;
            pacp.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
            pacp.AxtriaSalesIQTM__Account__c = acc.id;
            pacp.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
            pacp.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
            //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
            pacp.AxtriaSalesIQTM__Picklist1_Updated__c = '18';
            pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '6';
            pacp.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
            pacp.AxtriaSalesIQTM__Metric6__c = 4;
            pacp.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
            pacp.Line__c= line.id;
            insert pacp;
            posAccList.add(pacp.id);
        }
        
        
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs.AxtriaSalesIQTM__User__c = loggedInUser.id;
        accessRecs.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs.AxtriaSalesIQTM__Position__c =  pos.id;
        
        insert accessRecs;
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs1 = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs1.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs1.AxtriaSalesIQTM__User__c = tuser.id;
        accessRecs1.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs1.AxtriaSalesIQTM__Position__c =  pos2.id;
        insert accessRecs1; 
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs2 = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs2.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs2.AxtriaSalesIQTM__User__c = tuser1.id;
        accessRecs2.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs2.AxtriaSalesIQTM__Position__c =  posNation.id;
        insert accessRecs2;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs3 = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs3.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs3.AxtriaSalesIQTM__User__c = tuser2.id;
        accessRecs3.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs3.AxtriaSalesIQTM__Position__c =  posNation.id;
        insert accessRecs3;
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs4 = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs4.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs4.AxtriaSalesIQTM__User__c = dmuser.id;
        accessRecs4.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs4.AxtriaSalesIQTM__Position__c =  posdm.id;
        insert accessRecs4;

        AxtriaSalesIQTM__Team_Instance_Account__c lstTeamInstanceAccount = new AxtriaSalesIQTM__Team_Instance_Account__c();         
        lstTeamInstanceAccount.AxtriaSalesIQTM__Account_ID__c = acc.id;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        lstTeamInstanceAccount.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        lstTeamInstanceAccount.AxtriaSalesIQTM__Picklist1_Updated__c = '6';
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric1__c = 2.010000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric2__c  =29.000000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric3__c =2.000000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric4__c  =1.431479545000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric5__c  =1.490000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric6__c = 2.010000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric7__c  =29.000000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric8__c =2.000000000000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric9__c  =1.431479545000;
        lstTeamInstanceAccount.AxtriaSalesIQTM__Metric10__c  =1.490000000000;
        
        insert lstTeamInstanceAccount;
        
        List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> TeamInstObjAttList = new List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttrtbutes = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        objAttrtbutes.AxtriaSalesIQTM__Interface_Name__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objAttrtbutes.AxtriaSalesIQTM__isRequired__c = true;
        objAttrtbutes.AxtriaSalesIQTM__Data_Type__c = 'Picklist';
        objAttrtbutes.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Segment1__c';
        objAttrtbutes.AxtriaSalesIQTM__Attribute_Display_Name__c='Test';
        objAttrtbutes.AxtriaSalesIQTM__isEnabled__c = true;
        objAttrtbutes.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        objAttrtbutes.AxtriaSalesIQTM__Team_Instance__c=  TeamIns.id;
        objAttrtbutes.AxtriaSalesIQTM__isStatic__c  = true;

        
        
        TeamInstObjAttList.add(objAttrtbutes);
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttrtbutes1 = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        objAttrtbutes1.AxtriaSalesIQTM__Interface_Name__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objAttrtbutes1.AxtriaSalesIQTM__isRequired__c = true;
        objAttrtbutes1.AxtriaSalesIQTM__Data_Type__c = 'Checkbox';
        objAttrtbutes1.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__isIncludedCallPlan__c';
        objAttrtbutes1.AxtriaSalesIQTM__Attribute_Display_Name__c='Test1';
        objAttrtbutes1.AxtriaSalesIQTM__isEnabled__c = false;
        objAttrtbutes1.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        objAttrtbutes1.AxtriaSalesIQTM__Team_Instance__c=  TeamIns.id;
        objAttrtbutes1.AxtriaSalesIQTM__isStatic__c  = true;
        
        
        TeamInstObjAttList.add(objAttrtbutes1);
        
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttrtbutes2 = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        objAttrtbutes2.AxtriaSalesIQTM__Interface_Name__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objAttrtbutes2.AxtriaSalesIQTM__isRequired__c = true;
        objAttrtbutes2.AxtriaSalesIQTM__Data_Type__c = 'Checkbox';
        objAttrtbutes2.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__isAccountTarget__c';
        objAttrtbutes2.AxtriaSalesIQTM__Attribute_Display_Name__c='Test1';
        objAttrtbutes2.AxtriaSalesIQTM__isEnabled__c = false;
        objAttrtbutes2.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        objAttrtbutes2.AxtriaSalesIQTM__Team_Instance__c=  TeamIns.id;
        objAttrtbutes2.AxtriaSalesIQTM__isStatic__c  = true;

        
        
        TeamInstObjAttList.add(objAttrtbutes2);
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttrtbutes3 = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        objAttrtbutes3.AxtriaSalesIQTM__Interface_Name__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objAttrtbutes3.AxtriaSalesIQTM__isRequired__c = true;
        objAttrtbutes3.AxtriaSalesIQTM__Data_Type__c = 'Picklist';
        objAttrtbutes3.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Picklist1_Updated__c';
        objAttrtbutes3.AxtriaSalesIQTM__Attribute_Display_Name__c='Test2';
        objAttrtbutes3.AxtriaSalesIQTM__isEnabled__c = true;
        objAttrtbutes3.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        objAttrtbutes3.AxtriaSalesIQTM__Team_Instance__c=  TeamIns.id;
        objAttrtbutes3.AxtriaSalesIQTM__isStatic__c  = true;

        
        
        TeamInstObjAttList.add(objAttrtbutes3);
        insert TeamInstObjAttList;
        
        
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c objAttrtbutesDetails = new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
        objAttrtbutesDetails.AxtriaSalesIQTM__isActive__c  = true;
        objAttrtbutesDetails.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        objAttrtbutesDetails.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = objAttrtbutes.id;
        objAttrtbutesDetails.AxtriaSalesIQTM__Object_Value_Name__c = 'AxtriaSalesIQTM__Metrci1__c';
        objAttrtbutesDetails.AxtriaSalesIQTM__Object_Value_Seq__c = '1';
        
        insert objAttrtbutesDetails;
        
        set<string> allFieldsTest = new set<string>();
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Name__c');
        allFieldsTest.add('AxtriaSalesIQTM__Change_Status__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.AccountNumber');
        allFieldsTest.add('AxtriaSalesIQTM__Picklist2_Segment__c');
        allFieldsTest.add('AxtriaSalesIQTM__Picklist1_Segment__c');
        allFieldsTest.add('AxtriaSalesIQTM__Picklist1_Updated__c');
        allFieldsTest.add('AxtriaSalesIQTM__Picklist3_Updated__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric3__c');
        allFieldsTest.add('AxtriaSalesIQTM__isIncludedCallPlan__c');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.BillingCity');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.BillingState');
        allFieldsTest.add('AxtriaSalesIQTM__Account__r.BillingPostalCode');
        allFieldsTest.add('AxtriaSalesIQTM__Segment6__c');
        allFieldsTest.add('AxtriaSalesIQTM__Segment7__c');
        allFieldsTest.add('AxtriaSalesIQTM__Segment8__c');
        allFieldsTest.add('AxtriaSalesIQTM__Segment9__c');
        allFieldsTest.add('AxtriaSalesIQTM__Segment10__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric6__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric7__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric8__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric9__c');
        allFieldsTest.add('AxtriaSalesIQTM__Metric10__c');
        allFieldsTest.add('AxtriaSalesIQTM__Source__c');
        
        AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
        crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
        crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        insert crType;
        
        
        
        AxtriaSalesIQTM__CIM_Config__c cimconfigs = new AxtriaSalesIQTM__CIM_Config__c();
        cimConfigs.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        cimConfigs.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';
        cimConfigs.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Test Display';
        cimConfigs.AxtriaSalesIQTM__Change_Request_Type__c = crType.id;
        cimConfigs.AxtriaSalesIQTM__Enable__c = true;
        cimConfigs.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimConfigs.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimConfigs.AxtriaSalesIQTM__Threshold_Min__c = '-40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Max__c = '40';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-20';
        cimConfigs.AxtriaSalesIQTM__Threshold_Warning_Max__c = '20';
        
        insert cimConfigs;
        
        list<AxtriaSalesIQTM__CIM_Config__c> cimConfigsList = new list<AxtriaSalesIQTM__CIM_Config__c>();
        cimConfigsList.add(cimConfigs);
        cimConfigsList.add(cimConfigs);
        cimConfigsList.add(cimConfigs);
        cimConfigsList.add(cimConfigs);
        cimConfigsList.add(cimConfigs);
        cimConfigsList.add(cimConfigs);
        
        cimConfigsList[0].Name = 'No of Targets';
        cimConfigsList[1].Name = 'No of Calls';
        cimConfigsList[2].Name = 'No of Tier1 Physician';
        cimConfigsList[3].Name = 'No of Tier2 Physician';
        cimConfigsList[4].Name = 'No of Tier3 Physician';
        cimConfigsList[5].Name = 'No of Non-Target Physician';
        Brand_Team_Instance__c br = new Brand_Team_Instance__c();
        br.name='DAPA';
        br.Line__c=line.id;
        br.Team_Instance__c = teamIns.id;
        insert br;
        
        
        
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimsummary = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cimSummary.AxtriaSalesIQTM__Approved__c = '100';
        cimSummary.AxtriaSalesIQTM__Original__c = '100';
        cimSummary.AxtriaSalesIQTM__Proposed__c = '100';
        cimSummary.AxtriaSalesIQTM__CIM_Config__c = cimconfigs.id;
        cimSummary.AxtriaSalesIQTM__Team_Instance__c = TeamIns.id;
        cimSummary.AxtriaSalesIQTM__Position_Team_Instance__c = posteam.id;
        insert cimSummary;
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimSummaryList = new  list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        cimSummaryList.add(cimSummary);
        
        map<String, map<String,String>> changedMap = new map<String, map<String,String>>();
        map<String,String> temp = new map<String,String>();
        temp.put('tierChangedFlag','true');
        temp.put('originalTier','1');
        temp.put('tier','1');
        changedMap.put(posAcc.id, temp);
        
        //NewPhysicianUniverseCtrl.savefunc(changedMap,pos.id,'abc','abc');
        
        /*test.startTest();
        SalesIQUtility a = new SalesIQUtility();
        a.generateRandomNumber()='';
        test.stopTest();*/

        System.runAs(loggedinuser){
            test.startTest();
            PageReference myVfPage = Page.New_Physician_Universe1;
            
            Test.setCurrentPage(myVfPage);

            // Put Id into the current page Parameters
           
           ApexPages.currentPage().getParameters().put('pv0',pos.Id);
            ApexPages.currentPage().getParameters().put('pv1',teamIns.Id);
           /* pg.getParameters().put('selectedregion',regionSelected);
            pg.getParameters().put('selecteddistrict',districtSelected);
           // pg.getParameters().put('selectedline',selectedline);
            pg.getParameters().put('selectedcycle',selectedcycle);
            pg.getParameters().put('countryID',countryID);
*/
            

            NewPhysicianUniverseCtrl cons = new NewPhysicianUniverseCtrl();
            
            cons.selectedPosition = pos.Id;
            cons.selectedTeamInstance = teamIns.Id;
            cons.userType = 'Rep';
            cons.lock = true;
            //cons.teamInstanceChanged();
            //cons.showReasonCodeError();
           // cons.showSalesDirectionError();
           //cons.callSaveError();
            
            
          //  CallPlanCtrl.phyWrapper testWrap = new CallPlanCtrl.phyWrapper(posAcc, cons.segmentToValueMap, allFieldsTest);
            
            posacc.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
            update posAcc;
            
            cons.allFields += 'AxtriaSalesIQTM__Picklist2_Segment__c , AxtriaSalesIQTM__Picklist1_Segment__c';
            
          //  CallPlanCtrl.runQuery(cons.soql,pos.Id,teamIns.Id,cons.segmentToValueMap,cons.allFields,'abc');
            
            changedMap.put(posAcc.Id,temp);
            //CallPlanCtrl.savefunc(changedMap, pos.Id, teamIns.Id);
            
            //changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','24');
            temp.put('tier','0');
            changedMap.put(posAcc1.Id,temp);
    //      CallPlanCtrl.savefunc(changedMap, pos.Id, teamIns.Id);
            
            
         //   CallPlanCtrl.undo(pos.Id,teamIns.Id);
            
            cons.mapParentIdPositionId = new map<ID, list<AxtriaSalesIQTM__Position__c>>();
          //  cons.territoryListChange();
          //  cons.territoryTableChange();
          //  cons.changeTerritory();
          //  cons.showSalesDirectionErrorClass5();
            
//            cons.openFeedbackPopup();
//            cons.closeFeedbackPopup();
            cons.regionList = new list<SelectOption>();
            cons.districtSelected = '';
            cons.allTerritory = new list<SelectOption>();
            //cons.genericCssForBar = '';
            cons.feedbackbody = '';
            cons.feedbackEmailId = '';
            
            
            //CallPlanCtrl.savefunc(changedMap, pos.Id, teamIns.Id);
            //cons.submit();
//            cons.submitFeedback();
            
          // cons.percentageTotalHcps  = 10;
           //cons.percentagenexavarCalls = 20;
           //cons.percentagestivargaCalls = 30;
           //cons.percentagetotalCallsHCP = 15;
           
           
          // cons.colorNexavarCalls = '';
           //cons.colorTotalHCPs = '';
           //cons.colorStivargaCalls = '';
           //cons.colortotalCallsHCP = '';
           
           Map<String,String> segmentToValueMap = new map<string,string>();
           set<string> allFields = new set<string>();
           NewPhysicianUniverseCtrl wrapper = new NewPhysicianUniverseCtrl();
                                 
               // wrapper.metric6 = '4.0';                       
              //  wrapper.metric7 = 6.6;                        
                //wrapper.metric8 = 9;                   
               // wrapper.metric9 = 6;                     
               // wrapper.metric10 = 5 ;                     
               // wrapper.source = '';                            
                            
                test.stoptest();
        }
    
        /*System.runAs(tuser){
            CallPlanCtrl cons1 = new CallPlanCtrl();
            //cons1.submit();

        }*/
        /*System.runAs(tuser1){
            CallPlanCtrl cons2 = new CallPlanCtrl();
            //cons2.check2Phase();
            //cons2.horizontalBar();
            //cons2.submit();
        } 
        System.runAs(dmuser){
            CallPlanCtrl cons1 = new CallPlanCtrl();
            
           // cons1.territoryName='';
            //cons1.teamInsName='';
            //cons1.posType='';
            //cons1.gProfileName='';
            
            //cons1.savefunc();
            posAcc1.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
            cons1.submit();
            

        }*/
        
        /*System.runAs(dmuser){
            
            CallPlanCtrl cons3 = new CallPlanCtrl();
            
        }*/
        
    
    }
    
}