/**
Name        :   Survey_Def_Prof_Para_Meta_Insert_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   Survey_Def_Prof_Para_Meta_Insert_Utility
*/
global class Survey_Def_Prof_Para_Meta_Insert_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,Staging_Cust_Survey_Profiling__c> questionToObjectMap;
    public Map<String,Integer> questionToResponseFieldNoMap;
    public Map<String,Integer> BrandToGlobalCounter;// = 1;
    public List<string> myTIproductsIDs;
    public List<string> myTIproductsnames; 
    public Set<String> alreadyPresent;
  
    global Survey_Def_Prof_Para_Meta_Insert_Utility(String teamInstance) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        questionToObjectMap = new Map<String,Staging_Cust_Survey_Profiling__c>();
        questionToResponseFieldNoMap = new Map<String,Integer>();
        selectedTeamInstance = teamInstance;
        //GlobalCounter = 1;
        BrandToGlobalCounter = new Map<String,Integer>();
        alreadyPresent = new Set<String>();
        
        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Team_Instance__r.name,bti.id);
        }
        
        myTIproductsIDs = new List<string>();
        myTIproductsnames = new List<string>();
        for(Product_Catalog__c pc : [Select id,External_ID__c,name from Product_Catalog__c where Team_Instance__r.name = :teamInstance ]){
            myTIproductsIDs.add(pc.External_ID__c);
            myTIproductsnames.add(pc.name);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id, Name, CurrencyIsoCode, SURVEY_ID__c, SURVEY_NAME__c, BRAND_ID__c, BRAND_NAME__c, PARTY_ID__c, QUESTION_SHORT_TEXT__c, RESPONSE__c, Team_Instance__c, Last_Updated__c, QUESTION_ID__c FROM Staging_Cust_Survey_Profiling__c Where BRAND_ID__c in :myTIproductsIDs or BRAND_NAME__c in :myTIproductsnames  order by PARTY_ID__c, QUESTION_ID__c '; // Where Team_Instance__c = \'' + teamInstance + '\'
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Cust_Survey_Profiling__c> scopeRecs) {

      system.debug('Hello++++++++++++++++++++++++++++');
        //Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Set<string> brandIDSet = new Set<string>();
        
        //Map<string,id> posCodeIdMap = new Map<string,id>();
        //Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        Map<string,id> accountNumberIdMap = new Map<string,id>();
        Map<string,id> brandIdMap = new Map<string,id>();
        //Map<string,id> posTeamInstMap = new Map<string,id>();
        
        alreadyPresent = new Set<String>();
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;

        for(Staging_Cust_Survey_Profiling__c scsp : scopeRecs){      //Positions Set
            /*if(!string.IsBlank(scsp.Territory_ID__c)){
                posCodeSet.add(scsp.Territory_ID__c);
            }*/
            if(!string.IsBlank(scsp.PARTY_ID__c)){            //Accounts Set
                accountNumberSet.add(scsp.PARTY_ID__c);
            }
            /*if(!string.IsBlank(scsp.BRAND_ID__c)){            //Product_Catalog__c Set
                brandIDSet.add(scsp.BRAND_ID__c);
            }*/
                   
        }
        String key;
        
        
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber from Account where AccountNumber IN:accountNumberSet ]){ //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of accounts and their lookups
            
        }
        for(Product_Catalog__c pc : [Select id,External_ID__c from Product_Catalog__c ]){
            brandIdMap.put(pc.External_ID__c,pc.id);                  //Map of brands and their lookups
        }
        
        Survey_Response__c tempSurveyResponseRec = new Survey_Response__c();
        List<Survey_Response__c> surveyResponseList = new List<Survey_Response__c>();
        
        
        for(Staging_Cust_Survey_Profiling__c scsp : scopeRecs){
            
            if(!questionToObjectMap.containsKey(string.valueOf(scsp.QUESTION_SHORT_TEXT__c+scsp.BRAND_ID__c))){
                questionToObjectMap.put(string.valueOf(scsp.QUESTION_SHORT_TEXT__c+scsp.BRAND_ID__c),scsp);
            }
            if(!questionToResponseFieldNoMap.containsKey(string.valueOf(scsp.QUESTION_SHORT_TEXT__c+scsp.BRAND_ID__c))){
                if(!BrandToGlobalCounter.containsKey(scsp.BRAND_ID__c)){
                    BrandToGlobalCounter.put(scsp.BRAND_ID__c,1);
                }
                
                questionToResponseFieldNoMap.put(string.valueOf(scsp.QUESTION_SHORT_TEXT__c+scsp.BRAND_ID__c),BrandToGlobalCounter.get(scsp.BRAND_ID__c));
                //GlobalCounter++;
                BrandToGlobalCounter.put(scsp.BRAND_ID__c,BrandToGlobalCounter.get(scsp.BRAND_ID__c)+1);
            }
            System.debug('in for questionToObjectMap+++++'+questionToObjectMap);
            System.debug('in for questionToResponseFieldNoMap+++++'+questionToResponseFieldNoMap);
            
            tempSurveyResponseRec = new Survey_Response__c();
            
            //
            tempSurveyResponseRec.Question_ID__c = scsp.QUESTION_ID__c;
            tempSurveyResponseRec.Question_Short_Text__c = scsp.QUESTION_SHORT_TEXT__c;
            tempSurveyResponseRec.Response__c = scsp.RESPONSE__c;
            tempSurveyResponseRec.name = scsp.SURVEY_ID__c;
            
            
            //---------- Team Instance lookups
            if(!string.IsBlank(selectedTeamInstance)){//scsp.Team_Instance__c
                if(teamInstanceNametoIDMap.containsKey(selectedTeamInstance)){//scsp.Team_Instance__c
                    tempSurveyResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance); //scsp.Team_Instance__c   //Filling lookups of Team Instance
                    tempSurveyResponseRec.Cycle__c = teamInstanceNametoAZCycleMap.get(selectedTeamInstance);//scsp.Team_Instance__c
                    tempSurveyResponseRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(selectedTeamInstance);//scsp.Team_Instance__c
                    
                }
                else{
                    //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'Team_Instance__c';
                    tempErrorObj.Comments__c = 'Problem in Name of Team Instance: Team Instance name not found in map: Lookup not filled in Survey_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
                }
            }
            else{
              
                //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'Team_Instance__c';
                tempErrorObj.Comments__c = 'Team Instance Name is Blank: Lookup not filled in Survey_Response__c.';
                tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
            }
            //---------- Account lookups
            if(!string.isBlank(scsp.PARTY_ID__c)){
              if(accountNumberIdMap.containsKey(scsp.PARTY_ID__c)){
                tempSurveyResponseRec.Physician__c = accountNumberIdMap.get(scsp.PARTY_ID__c);        //Filling lookups of Accounts
              }
              else{
                //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'PARTY_ID__c';
                    tempErrorObj.Comments__c = 'Account Number not found in map: Lookup not filled in Survey_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'PARTY_ID__c';
                tempErrorObj.Comments__c = 'Account Number is Blank: Lookup not filled in Survey_Response__c.';
                tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
              
            }
            
            //brands
            if(!string.isBlank(scsp.BRAND_ID__c)){
              if(brandIdMap.containsKey(scsp.BRAND_ID__c)){
                tempSurveyResponseRec.Product__c = brandIdMap.get(scsp.BRAND_ID__c);        //Filling lookups of Brands
                
              }
              else{
                //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'BRAND_ID__c';
                    tempErrorObj.Comments__c = 'Account Number not found in map: Lookup not filled in Survey_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'BRAND_ID__c';
                tempErrorObj.Comments__c = 'Account Number is Blank: Lookup not filled in Survey_Response__c.';
                tempErrorObj.Team_Instance_Name__c = selectedTeamInstance;//scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
              
            }
            
            tempSurveyResponseRec.External_ID__c = scsp.SURVEY_ID__c+tempSurveyResponseRec.Team_Instance__c+tempSurveyResponseRec.Physician__c+ tempSurveyResponseRec.Product__c+ scsp.QUESTION_SHORT_TEXT__c;
            
            if(!alreadyPresent.contains(tempSurveyResponseRec.External_ID__c))
                surveyResponseList.add(tempSurveyResponseRec);
            else
                alreadyPresent.add(tempSurveyResponseRec.External_ID__c);
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        

        upsert surveyResponseList External_ID__c;

    }

    global void finish(Database.BatchableContext BC) {
        List<Parameter__c> parameterList = new List<Parameter__c>();
        List<MetaData_Definition__c> metaDataDefinitionList = new List<MetaData_Definition__c>();
        List<Survey_Definition__c> surveyDefinitionList = new List<Survey_Definition__c>();
        Parameter__c tempParameterRec = new Parameter__c();
        MetaData_Definition__c tempMetaDataDefinitionRec = new MetaData_Definition__c();
        Survey_Definition__c tempsurveyDefinitionRec = new Survey_Definition__c();
        
        Map<string,Parameter__c> parameterAlreadyExistList = new Map<string,Parameter__c>();
        for(Parameter__c p : [select id,Name,Team_Instance__c from Parameter__c]){
            parameterAlreadyExistList.put(p.Name+p.Team_Instance__c,p);
        }
        
        Map<string,MetaData_Definition__c> metaDataDefinitionAlreadyExistList = new Map<string,MetaData_Definition__c>();
        for(MetaData_Definition__c mdd : [select id,Display_Name__c,Team_Instance__c,Team_Instance__r.name,Brand_Team_Instance__r.Brand__r.External_ID__c,Brand_Team_Instance__r.Brand__r.name from MetaData_Definition__c where Sequence__c != 10 ]){//and  Sequence__c != 9
            metaDataDefinitionAlreadyExistList.put(mdd.Display_Name__c+mdd.Brand_Team_Instance__r.Brand__r.External_ID__c+mdd.Brand_Team_Instance__r.Brand__r.name+mdd.Team_Instance__c,mdd);
        }
        
        Map<string,Survey_Definition__c> surveyDefinitionAlreadyExistList = new Map<string,Survey_Definition__c>();
        for(Survey_Definition__c sd : [select id,name,Brand_ID__c,Question_Short_Text__c,Team_Instance__c,Cycle__c from Survey_Definition__c]){
            surveyDefinitionAlreadyExistList.put(sd.Brand_ID__c+sd.Question_Short_Text__c+sd.Team_Instance__c,sd);
        }
        System.debug('questionToObjectMap+++++'+questionToObjectMap);
        System.debug('questionToResponseFieldNoMap+++++'+questionToResponseFieldNoMap);
        
        //Integer counter1 = 1;
        //Integer counter2 = 1;
        for(string qtext : questionToObjectMap.keySet()){
            if(!parameterAlreadyExistList.containsKey(questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c + teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/))){
                tempParameterRec = new Parameter__c();
                
                tempParameterRec.Name = questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c;
                tempParameterRec.isActive__c = true;
                tempParameterRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempParameterRec.Team__c = teamInstanceNametoTeamIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempParameterRec.Business_Unit__c = teamInstanceNametoBUMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempParameterRec.Type__c = 'Text'; // ??
               // tempParameterRec.CurrencyIsoCode = 'EUR';
                tempParameterRec.Brand_Team_Instance__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(questionToObjectMap.get(qtext).BRAND_ID__c + selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);  //??

                parameterList.add(tempParameterRec);
            }
            
            if(!metaDataDefinitionAlreadyExistList.containsKey(questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c +questionToObjectMap.get(qtext).BRAND_ID__c +questionToObjectMap.get(qtext).BRAND_NAME__c + teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/))){
                tempMetaDataDefinitionRec = new MetaData_Definition__c();
                
                tempMetaDataDefinitionRec.Source_Object__c = 'BU_Response__c';
                tempMetaDataDefinitionRec.Type__c = 'Text'; // ??
                tempMetaDataDefinitionRec.Sequence__c = Integer.ValueOf(questionToResponseFieldNoMap.get(qtext/*+String.valueof(questionToObjectMap.get(qtext).BRAND_ID__c)*/));//Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1; //counter1;
                tempMetaDataDefinitionRec.Source_Field__c = 'Response'+String.ValueOf(Integer.ValueOf(questionToResponseFieldNoMap.get(qtext/*+String.valueof(questionToObjectMap.get(qtext).BRAND_ID__c)*/)))+'__c';//String.ValueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1)//String.valueOf(counter1)
                tempMetaDataDefinitionRec.Display_Name__c = questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c;
                tempMetaDataDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempMetaDataDefinitionRec.Team__c = teamInstanceNametoTeamIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
               // tempMetaDataDefinitionRec.CurrencyIsoCode = 'EUR';
                tempMetaDataDefinitionRec.Brand_Team_Instance__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(questionToObjectMap.get(qtext).BRAND_ID__c + selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);  //??
                //counter1++;
                metaDataDefinitionList.add(tempMetaDataDefinitionRec);
            }
            
            if(!surveyDefinitionAlreadyExistList.containsKey(questionToObjectMap.get(qtext).BRAND_ID__c + questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c + teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/))){
                tempsurveyDefinitionRec = new Survey_Definition__c();
                
                tempsurveyDefinitionRec.Brand_ID__c = questionToObjectMap.get(qtext).BRAND_ID__c;
                tempsurveyDefinitionRec.Brand_Name__c = questionToObjectMap.get(qtext).BRAND_NAME__c;
                tempsurveyDefinitionRec.Cycle__c = teamInstanceNametoAZCycleMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempsurveyDefinitionRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                tempsurveyDefinitionRec.Cycle_Name__c = teamInstanceNametoAZCycleNameMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/);
                /*tempsurveyDefinitionRec.Cycle2__c = ; //??
                tempsurveyDefinitionRec.MetaData_Input__c = ''; //??*/
                tempsurveyDefinitionRec.Question_ID__c = String.valueOf(Integer.ValueOf(questionToResponseFieldNoMap.get(qtext/*+String.valueof(questionToObjectMap.get(qtext).BRAND_ID__c)*/)));//String.valueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1);//String.valueOf(counter2);//
                tempsurveyDefinitionRec.Question_Short_Text__c = questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c;
                tempsurveyDefinitionRec.Question_Text__c = questionToObjectMap.get(qtext).QUESTION_SHORT_TEXT__c;
                /*tempsurveyDefinitionRec.Read_Only__c = ; //??
                tempsurveyDefinitionRec.Response_Options__c = ; //??*/
                tempsurveyDefinitionRec.Survey_ID__c = questionToObjectMap.get(qtext).Survey_ID__c;
                tempsurveyDefinitionRec.Survey_Name__c = questionToObjectMap.get(qtext).Survey_Name__c;
                tempsurveyDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(selectedTeamInstance/*questionToObjectMap.get(qtext).Team_Instance__c*/); //??
                //tempsurveyDefinitionRec.CurrencyIsoCode = 'EUR';
                //counter2++;
                
                surveyDefinitionList.add(tempsurveyDefinitionRec);
            }
            
        }
        System.debug('parameterList+++++++++++'+parameterList);
        System.debug('metaDataDefinitionList+++++++++++'+metaDataDefinitionList);
        
        insert parameterList;
        insert metaDataDefinitionList;
        insert surveyDefinitionList;
        
        Database.executeBatch(new Staging_BU_Response_Insert_Utility(selectedTeamInstance,questionToResponseFieldNoMap),2000);
    }
}