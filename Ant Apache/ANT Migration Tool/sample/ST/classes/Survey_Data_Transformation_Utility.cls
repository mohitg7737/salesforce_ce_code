/**
Name        :   Survey_Data_Transformation_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   Survey_Data_Transformation_Utility
*/
global class Survey_Data_Transformation_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    public String selectedTeamInstanceIs;
    public Integer GlobalCounter = 1;
  
    global Survey_Data_Transformation_Utility() {//String teamInstance
        teamInstanceNametoIDMap = new Map<String,Id>();
        //selectedTeamInstanceIs = teamInstance;
        //selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        
        
        //selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c, Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Question_ID1__c, Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c  '; // Where Team_Instance__c = \'' + teamInstance + '\' 
    }
    
    global Survey_Data_Transformation_Utility(String teamInstance) {//String teamInstance
        teamInstanceNametoIDMap = new Map<String,Id>();
        selectedTeamInstanceIs = teamInstance;
        //selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        
        
        //selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c, Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Question_ID1__c, Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c  '; // Where Team_Instance__c = \'' + teamInstance + '\' 
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Survey_Data__c> scopeRecs) {
        system.debug('Hello++++++++++++++++++++++++++++');
        
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;
        
        Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec = new Staging_Cust_Survey_Profiling__c();
        List<Staging_Cust_Survey_Profiling__c> surveyResponseList = new List<Staging_Cust_Survey_Profiling__c>();
        
        for(Staging_Survey_Data__c scsp : scopeRecs){
            
            
            tempStagSurveyRespRec = new Staging_Cust_Survey_Profiling__c();
            
            //tempStagSurveyRespRec.name = scsp.SURVEY_ID__c;
            tempStagSurveyRespRec.CurrencyIsoCode = scsp.CurrencyIsoCode;
            
            if(scsp.SURVEY_ID__c != null && scsp.SURVEY_ID__c != ''){
                tempStagSurveyRespRec.SURVEY_ID__c = scsp.SURVEY_ID__c;
            }
            else{
                tempStagSurveyRespRec.SURVEY_ID__c = 'Axtria Offline';
            }
            
            if(scsp.SURVEY_NAME__c != null && scsp.SURVEY_NAME__c != ''){
                tempStagSurveyRespRec.SURVEY_NAME__c = scsp.SURVEY_NAME__c;
            }
            else{
                tempStagSurveyRespRec.SURVEY_NAME__c = 'Axtria Offline';
            }
            
            
            //tempStagSurveyRespRec.Last_Updated__c = scsp.Last_Updated__c;
            tempStagSurveyRespRec.Team_Instance__c = scsp.Sales_Cycle__c;//Sales_Cycle__c  Team_Instance__c  Team__c
            tempStagSurveyRespRec.PARTY_ID__c = scsp.Account_Number__c;
            
            tempStagSurveyRespRec.BRAND_ID__c = scsp.Product_Code__c;
            tempStagSurveyRespRec.BRAND_NAME__c = scsp.Product_Name__c;
            
            tempStagSurveyRespRec.External_ID__c = tempStagSurveyRespRec.SURVEY_ID__c+scsp.Sales_Cycle__c+scsp.Account_Number__c+scsp.Product_Code__c;
            
            if(scsp.Short_Question_Text1__c != null && scsp.Short_Question_Text1__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec1 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec1 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID1__c != null){
                    tempStagSurveyRespRec1.QUESTION_ID__c = scsp.Question_ID1__c;
                }
                else{
                    tempStagSurveyRespRec1.QUESTION_ID__c = 501;
                }
                
                tempStagSurveyRespRec1.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text1__c;
                tempStagSurveyRespRec1.RESPONSE__c = scsp.Response1__c;
                
                tempStagSurveyRespRec1.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text1__c;
                
                surveyResponseList.add(tempStagSurveyRespRec1);
                
            }
            if(scsp.Short_Question_Text2__c != null && scsp.Short_Question_Text2__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec2 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec2 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID2__c != null){
                    tempStagSurveyRespRec2.QUESTION_ID__c = scsp.Question_ID2__c;
                }
                else{
                    tempStagSurveyRespRec2.QUESTION_ID__c = 502;
                }
                
                tempStagSurveyRespRec2.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text2__c;
                tempStagSurveyRespRec2.RESPONSE__c = scsp.Response2__c;
                
                tempStagSurveyRespRec2.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text2__c;
                
                surveyResponseList.add(tempStagSurveyRespRec2);
                
            }
            if(scsp.Short_Question_Text3__c != null && scsp.Short_Question_Text3__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec3 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec3 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID3__c != null){
                    tempStagSurveyRespRec3.QUESTION_ID__c = scsp.Question_ID3__c;
                }
                else{
                    tempStagSurveyRespRec3.QUESTION_ID__c = 503;
                }
                
                tempStagSurveyRespRec3.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text3__c;
                tempStagSurveyRespRec3.RESPONSE__c = scsp.Response3__c;
                
                tempStagSurveyRespRec3.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text3__c;
                
                surveyResponseList.add(tempStagSurveyRespRec3);
                
            }
            if(scsp.Short_Question_Text4__c != null && scsp.Short_Question_Text4__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec4 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec4 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID4__c != null){
                    tempStagSurveyRespRec4.QUESTION_ID__c = scsp.Question_ID4__c;
                }
                else{
                    tempStagSurveyRespRec4.QUESTION_ID__c = 504;
                }
                
                tempStagSurveyRespRec4.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text4__c;
                tempStagSurveyRespRec4.RESPONSE__c = scsp.Response4__c;
                
                tempStagSurveyRespRec4.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text4__c;
                
                surveyResponseList.add(tempStagSurveyRespRec4);
                
            }
            if(scsp.Short_Question_Text5__c != null && scsp.Short_Question_Text5__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec5 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec5 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID5__c != null){
                    tempStagSurveyRespRec5.QUESTION_ID__c = scsp.Question_ID5__c;
                }
                else{
                    tempStagSurveyRespRec5.QUESTION_ID__c = 505;
                }
                
                tempStagSurveyRespRec5.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text5__c;
                tempStagSurveyRespRec5.RESPONSE__c = scsp.Response5__c;
                
                tempStagSurveyRespRec5.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text5__c;
                
                surveyResponseList.add(tempStagSurveyRespRec5);
                
            }
            if(scsp.Short_Question_Text6__c != null && scsp.Short_Question_Text6__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec6 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec6 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID6__c != null){
                    tempStagSurveyRespRec6.QUESTION_ID__c = scsp.Question_ID6__c;
                }
                else{
                    tempStagSurveyRespRec6.QUESTION_ID__c = 506;
                }
                
                tempStagSurveyRespRec6.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text6__c;
                tempStagSurveyRespRec6.RESPONSE__c = scsp.Response6__c;
                
                tempStagSurveyRespRec6.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text6__c;
                
                surveyResponseList.add(tempStagSurveyRespRec6);
                
            }
            if(scsp.Short_Question_Text7__c != null && scsp.Short_Question_Text7__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec7 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec7 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID7__c != null){
                    tempStagSurveyRespRec7.QUESTION_ID__c = scsp.Question_ID7__c;
                }
                else{
                    tempStagSurveyRespRec7.QUESTION_ID__c = 507;
                }
                
                tempStagSurveyRespRec7.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text7__c;
                tempStagSurveyRespRec7.RESPONSE__c = scsp.Response7__c;
                
                tempStagSurveyRespRec7.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text7__c;
                
                surveyResponseList.add(tempStagSurveyRespRec7);
                
            }
            if(scsp.Short_Question_Text8__c != null && scsp.Short_Question_Text8__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec8 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec8 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID8__c != null){
                    tempStagSurveyRespRec8.QUESTION_ID__c = scsp.Question_ID8__c;
                }
                else{
                    tempStagSurveyRespRec8.QUESTION_ID__c = 508;
                }
                
                tempStagSurveyRespRec8.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text8__c;
                tempStagSurveyRespRec8.RESPONSE__c = scsp.Response8__c;
                
                tempStagSurveyRespRec8.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text8__c;
                
                surveyResponseList.add(tempStagSurveyRespRec8);
                
            }
            if(scsp.Short_Question_Text9__c != null && scsp.Short_Question_Text9__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec9 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec9 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID9__c != null){
                    tempStagSurveyRespRec9.QUESTION_ID__c = scsp.Question_ID9__c;
                }
                else{
                    tempStagSurveyRespRec9.QUESTION_ID__c = 509;
                }
                
                tempStagSurveyRespRec9.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text9__c;
                tempStagSurveyRespRec9.RESPONSE__c = scsp.Response9__c;
                
                tempStagSurveyRespRec9.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text9__c;
                
                surveyResponseList.add(tempStagSurveyRespRec9);
                
            }
            if(scsp.Short_Question_Text10__c != null && scsp.Short_Question_Text10__c != ''){
                Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec10 = new Staging_Cust_Survey_Profiling__c();
                tempStagSurveyRespRec10 = tempStagSurveyRespRec.clone();
                if(scsp.Question_ID10__c != null){
                    tempStagSurveyRespRec10.QUESTION_ID__c = scsp.Question_ID10__c;
                }
                else{
                    tempStagSurveyRespRec10.QUESTION_ID__c = 510;
                }
                
                tempStagSurveyRespRec10.QUESTION_SHORT_TEXT__c = scsp.Short_Question_Text10__c;
                tempStagSurveyRespRec10.RESPONSE__c = scsp.Response10__c;
                
                tempStagSurveyRespRec10.External_ID__c = tempStagSurveyRespRec.External_ID__c + scsp.Short_Question_Text10__c;
                
                surveyResponseList.add(tempStagSurveyRespRec10);
                
            }
            
            /*Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, 
            SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c, 
            Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, 
            Short_Question_Text3__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, 
            Response9__c, Response10__c, Response8__c, Short_Question_Text4__c, Short_Question_Text5__c, 
            Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, 
            Short_Question_Text7__c, Question_ID1__c, Question_ID2__c, Question_ID3__c, Question_ID4__c, 
            Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, 
            Question_ID10__c, Product_Name__c, Position_Code__c */
            
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        
        upsert surveyResponseList External_ID__c;

    }

    global void finish(Database.BatchableContext BC) {
        
        Survey_Def_Prof_Para_Meta_Insert_Utility pacpBatch = new Survey_Def_Prof_Para_Meta_Insert_Utility(selectedTeamInstanceIs);
        Database.executeBatch(pacpBatch,2000);
        
        //Database.executeBatch(new Staging_BU_Response_Insert_Utility(selectedTeamInstance,questionToResponseFieldNoMap),2000);
    }
}