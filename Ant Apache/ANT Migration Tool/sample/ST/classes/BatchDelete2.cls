global class BatchDelete2 implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select id,Cycle__c,Team_Instance__c from Bu_response__c';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Bu_response__c> scope) {
        for(Bu_response__c p : scope){
            p.Team_Instance__c = p.Cycle__c;
        }
        Update scope;
        //Database.DeleteResult[] drList = Database.delete( scope, false);
         //Database.Emptyrecyclebin(scope);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}