public class CallCapacityCtrl {
    
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    public List<SelectOption> allTeamInstance                {get;set;}
    public string teaminstanceSelected{get; set;}
    public list<SelectOption>allcycles {get;set;}
    public string selectedcycle {get;set;}
    public String userLocale {get;set;}
    public String fileDelimiter {get;set;}
    public String countryID {get;set;}
    public string numericdelimiter {get;set;}
    Map<string,string> successErrorMap;
     Public AxtriaSalesIQTM__Country__c Country {get;set;}
    public string selectedTeamInstance                       {get;set;}
    Public String TeamInstancename {get;set;}
     public TIProductWrapper TIProduct {get;set;}
     public List<SelectOption> TeamProdLevelList {get;set;}
     public String jSONStringForTIProduct {get;set;}
     //public String selectedProduct_Catalog {get;set;}
     public String ChangedTeamProdLevel {get;set;}
     public String ChangedTeamProdLevelTemp {get;set;}
    public string userType                                   {get;set;}
    public list<AxtriaSalesIQTM__Team_Instance__c>teaminst {get;set;}
    
    public CallCapacityCtrl(){
        
        
        userLocale = UserInfo.getLocale();
      system.debug('@@@@@@@@@qwqw' + userLocale);
      List<Country__c> countryInfo = [select Delimiter__c,Numeric_Delimiter__c from Country__c where name =: userLocale];
      
      
      if(countryInfo.size()>0)
      {
          fileDelimiter = countryInfo[0].Delimiter__c;
          numericdelimiter = countryInfo[0].Numeric_Delimiter__c;

      }
      else
      {
          fileDelimiter = ',';
          numericdelimiter ='.';
      }
      allTeamInstance      = new List<SelectOption>();
      allcycles = new list<SelectOption>();
      
      
      //Fetch Country ID from cookie
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }
        userType = '4';
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId(), countryID);
        if(loggedInUserData!=null && loggedInUserData.size()>0)
        {  
            TeamInstancename = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
            selectedTeamInstance = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
            teaminstanceSelected = selectedTeamInstance;
            
            teaminst =[select id,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id=:selectedTeamInstance limit 1];
            if(teaminst[0].cycle__r.Name!=null){
              allcycles.add(new SelectOption(teaminst[0].cycle__r.Name,teaminst[0].cycle__r.Name));
              selectedcycle=teaminst[0].cycle__r.Name;
            }
            
            //Code added by siva on 26-09-2017
            for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
            {
                allTeamInstance.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
            }
        }
        
        openCallCapacity();
    }
    
    public void teamInstanceChanged()
    {
        
        selectedTeamInstance = teaminstanceSelected;
        
        loggedInUserData = [select AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance  order by AxtriaSalesIQTM__Position__r.Name];
        
        //TeamInstancename = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
              
        //system.debug('----Selected position is:'+selectedPosition);
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {   
            if(userInfo.AxtriaSalesIQTM__Team_Instance__c==teamInstanceSelected){
                
                allcycles = new list<SelectOption>();
                
                teaminst =[select id,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id=:selectedTeamInstance limit 1];
                if(teaminst[0].cycle__r.Name!=null){
                  allcycles.add(new SelectOption(teaminst[0].cycle__r.Name,teaminst[0].cycle__r.Name));
                  selectedcycle=teaminst[0].cycle__r.Name;
                }
                //allTerritory=new list<selectOption>();
                
                //loggedInUsercheck(userInfo);
                TeamInstancename = userInfo.AxtriaSalesIQTM__Team_Instance__r.Name;
                TIProduct=null;
                openCallCapacity();
            }
        }
        
    }
    
    
    public void openCallCapacity(){
         //TIProduct = new TIProductWrapper();
         List<Team_Instance_Product_AZ__c> ppList = [Select Id, Name, CurrencyIsoCode, Effective_End_Date__c, Effective_Start_Date__c, 
         Team_Instance__c, Team_Instance__r.name, 
         Business_Days_in_Cycle_Formula__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, 
         Other_Days_Off__c, Vacation_Days__c, Product_Catalogue__c, Product_Catalogue__r.name
         FROM Team_Instance_Product_AZ__c 
         where Team_Instance__c = :selectedTeamInstance];// and AxtriaSalesIQTM__isActive__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition
         // and Product_Catalog__c = 
         
         string radiostring='Only This Position';
         if(userType != '1'){ //user type will be 4
             radiostring='All Positions';
         }
         
         TeamProdLevelList = new List<SelectOption>();
         TeamProdLevelList.add(new SelectOption('By Team','By Team'));
         TeamProdLevelList.add(new SelectOption('By Product','By Product'));
         
         Map<String,CallPlanCallCapacity__c> CallCapacityCustSetting =  new Map<String,CallPlanCallCapacity__c>();
         CallCapacityCustSetting=CallPlanCallCapacity__c.getAll();
         //Boolean isTeamLevel = false; // default false-> product level
         ChangedTeamProdLevel= 'By Product';
         ChangedTeamProdLevelTemp= 'By Product';
         //String aggFunc = 'SUM';
         
         if(CallCapacityCustSetting.containsKey(TeamInstancename)){ // || CallCapacityCustSetting!=null
             //isTeamLevel = CallCapacityCustSetting.get(TeamInstancename).is_Team_Level__c;
             if(CallCapacityCustSetting.get(TeamInstancename).is_Team_Level__c==false){
                 ChangedTeamProdLevel= 'By Product';
                 ChangedTeamProdLevelTemp= 'By Product';
             }
             else{
                 ChangedTeamProdLevel= 'By Team';
                 ChangedTeamProdLevelTemp= 'By Team';
             }
             /*if(CallCapacityCustSetting[0].Aggregate_Function__c == 'SUM' || CallCapacityCustSetting[0].Aggregate_Function__c == 'MAX' || CallCapacityCustSetting[0].Aggregate_Function__c == 'AVG'){
                aggFunc = CallCapacityCustSetting[0].Aggregate_Function__c;
             }*/
         }
         
         List<TIProductInnerWrapper> TIProductInnerList = new List<TIProductInnerWrapper>();
         
         Integer totalCallCap = 0;
         
         for(Team_Instance_Product_AZ__c pprec : ppList){
            totalCallCap += Integer.ValueOf(pprec.Call_Capacity_Formula__c);
            TIProductInnerList.add(new TIProductInnerWrapper(pprec.id,pprec.Product_Catalogue__r.name,Integer.valueOf(pprec.Call_Capacity_Formula__c),Integer.valueOf(pprec.Calls_Day__c)));
         }
         
         
         if(ppList.size()>0 && ppList!=null){
             System.debug('ffffffff'+TIProduct);
             if(ChangedTeamProdLevel=='By Product'){
                TIProduct = new TIProductWrapper(ppList[0],radiostring,totalCallCap,TIProductInnerList);
             }
             else{
                 List<TIProductInnerWrapper> TIProductInnerListTemp = new List<TIProductInnerWrapper>();
                 TIProductInnerListTemp.add(new TIProductInnerWrapper(/*TIProductInnerList[0].id*/'All Products','All Products',Integer.valueOf(TIProductInnerList[0].CallCapFormula),Integer.valueOf(TIProductInnerList[0].CallsPerDay)));
                 TIProduct = new TIProductWrapper(ppList[0],radiostring,/*totalCallCap*/Integer.valueOf(ppList[0].Call_Capacity_Formula__c),TIProductInnerListTemp);
             }
             //ChangedTeamProdLevel=TIProduct.ProductCatalog;
         }
         else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Call Capacity Not Configured, Kindly contact your System Administrator!'));
         }
         //selectedProduct_Catalog = ppList[0].Product_Catalog__c;
         
     }
     
     public void TeamProdLevelChanged(){
         ChangedTeamProdLevel=ChangedTeamProdLevelTemp;
         List<Team_Instance_Product_AZ__c> ppList = [Select Id, Name, CurrencyIsoCode, Effective_End_Date__c, Effective_Start_Date__c, 
         Team_Instance__c, Team_Instance__r.name, 
         Business_Days_in_Cycle_Formula__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, 
         Other_Days_Off__c, Vacation_Days__c, Product_Catalogue__c, Product_Catalogue__r.name
         FROM Team_Instance_Product_AZ__c 
         where Team_Instance__c = :selectedTeamInstance];// and AxtriaSalesIQTM__isActive__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition
         // and Product_Catalog__c = 
         
         string radiostring='Only This Position';
         if(userType != '1'){ //user type will be 4
             radiostring='All Positions';
         }
         
         TeamProdLevelList = new List<SelectOption>();
         TeamProdLevelList.add(new SelectOption('By Team','By Team'));
         TeamProdLevelList.add(new SelectOption('By Product','By Product'));
         
         List<TIProductInnerWrapper> TIProductInnerList = new List<TIProductInnerWrapper>();
         
         Integer totalCallCap = 0;
         
         for(Team_Instance_Product_AZ__c pprec : ppList){
            totalCallCap += Integer.ValueOf(pprec.Call_Capacity_Formula__c);
            TIProductInnerList.add(new TIProductInnerWrapper(pprec.id,pprec.Product_Catalogue__r.name,Integer.valueOf(pprec.Call_Capacity_Formula__c),Integer.valueOf(pprec.Calls_Day__c)));
         }
         
         
         if(ppList.size()>0 && ppList!=null){
             System.debug('ffffffff'+TIProduct);
             if(ChangedTeamProdLevel=='By Product'){
                TIProduct = new TIProductWrapper(ppList[0],radiostring,totalCallCap,TIProductInnerList);
             }
             else{
                 List<TIProductInnerWrapper> TIProductInnerListTemp = new List<TIProductInnerWrapper>();
                 TIProductInnerListTemp.add(new TIProductInnerWrapper(/*TIProductInnerList[0].id*/'All Products','All Products',Integer.valueOf(TIProductInnerList[0].CallCapFormula),Integer.valueOf(TIProductInnerList[0].CallsPerDay)));
                 TIProduct = new TIProductWrapper(ppList[0],radiostring,/*totalCallCap*/Integer.valueOf(ppList[0].Call_Capacity_Formula__c),TIProductInnerListTemp);
             }
             //ChangedTeamProdLevel=TIProduct.ProductCatalog;
         }
         else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Call Capacity Not Configured, Kindly contact your System Administrator!'));
         }
         //selectedProduct_Catalog = ppList[0].Product_Catalog__c;
         
     }
     
     public void savecompleted(){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Saved Succesfully!'));
     }
     
     @RemoteAction
     public static void saveCallCapacity(String teamInstance, Map<String,String> changedTIProductMap, String TIProductLevel,String TIProductselectedradio, String userType){
         //Map<String,String> changedTIProductMap = (Map<String,String>) JSON.deserialize(jSONStringForTIProduct, Map<String,String>.Class);
         System.debug('TIProduct.selectedradio'+TIProductselectedradio);

         /*Map<String,CallPlanCallCapacity__c> CallCapacityCustSetting =  new Map<String,CallPlanCallCapacity__c>();
         CallCapacityCustSetting=CallPlanCallCapacity__c.getAll();
         
         CallPlanCallCapacity__c callplancapToUpdate = new CallPlanCallCapacity__c();
         
         String TeamInstancenametemp = '';
         TeamInstancenametemp = [select name from AxtriaSalesIQTM__Team_Instance__c where id= :teamInstance].name;
         
         if(CallCapacityCustSetting.containsKey(TeamInstancenametemp)){
             callplancapToUpdate.id=CallCapacityCustSetting.get(TeamInstancenametemp).id;
             if(TIProductLevel=='By Team'){
                 callplancapToUpdate.is_Team_Level__c= true;
             }
             else{
                 callplancapToUpdate.is_Team_Level__c= false;
             }
         }
         update callplancapToUpdate;*/
         
         
         /*if(posProdList.size()>0){
             if(posProdList[0]!=null){
                 if(posProdList[0].id=='All Products'){
                     
                 }
             }
         }*/
         List<Team_Instance_Product_AZ__c> posProdList = new List<Team_Instance_Product_AZ__c>([Select Id, Name, CurrencyIsoCode, Effective_End_Date__c, Effective_Start_Date__c, 
         Team_Instance__c, Team_Instance__r.name, 
         Business_Days_in_Cycle_Formula__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, 
         Other_Days_Off__c, Vacation_Days__c, Product_Catalogue__c, Product_Catalogue__r.name
         FROM Team_Instance_Product_AZ__c 
         where Team_Instance__c = :teamInstance]);
         List<Team_Instance_Product_AZ__c> TIPToUpdateList = new List<Team_Instance_Product_AZ__c>();
             Team_Instance_Product_AZ__c TIPTempRec = new Team_Instance_Product_AZ__c();
             
         if(TIProductselectedradio=='All Positions'){
             //RunTeamInstanceProduct_AZ_Trigger =true;
             //Integer tempCallsperd= Integer.valueof(posProdList[0].CallsPerDay);
             
             for(Team_Instance_Product_AZ__c az : posProdList){
                TIPTempRec = new Team_Instance_Product_AZ__c();
                 TIPTempRec.id = az.id;
                 TIPTempRec.Vacation_Days__c = Integer.ValueOf(changedTIProductMap.get('VacationDays'));
                 TIPTempRec.Holidays__c = Integer.ValueOf(changedTIProductMap.get('Holidays'));
                 TIPTempRec.Other_Days_Off__c = Integer.ValueOf(changedTIProductMap.get('OtherDaysOff'));
                 //TIPTempRec.Calls_Day__c = tipiw.CallsPerDay;//Integer.ValueOf(changedTIProductMap.get('Calls_Day'));
                 TIPToUpdateList.add(TIPTempRec);
             }
             update TIPToUpdateList;
             
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Saved Succesfully!'));
             
         }
             
         else if(TIProductselectedradio=='Unchanged Positions'){ //  || (TIProduct.selectedradio=='Only This Position' && TIProduct.ProductCatalog == 'All')
             
             List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData = [select AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQTM__Team_Instance__c = :teamInstance  order by AxtriaSalesIQTM__Position__r.Name];
        
             string myPosition = loggedInUserData[0].AxtriaSalesIQTM__Position__c;
        
             List<AxtriaSalesIQTM__Position_Product__c> ppToUpdateList = new List<AxtriaSalesIQTM__Position_Product__c>();
             
             string ppsql= 'Select Id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.name, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Master__r.name, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Product__r.name,  AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, AxtriaSalesIQTM__Team_Instance__r.name,  Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c,  Holidays__c, Other_Days_Off__c, Vacation_Days__c, Product_Catalog__c, Product_Catalog__r.name FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstance and AxtriaSalesIQTM__isActive__c = true ';
             
             if(TIProductLevel != 'By Team'){
             ppsql+= ' and Product_Catalog__c = \''+TIProductLevel +'\' ';
             }
             
             if(TIProductselectedradio=='Unchanged Positions'){
                 ppsql+= ' and (Call_Capacity_Formula__c = null or Call_Capacity_Formula__c = 0) ';
             }
             
             //if(TIProduct.selectedradio!='Only This Position'){
                 if(userType=='4'){
                     ppsql+= ' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c = :myPosition ';
                 }
                 else if(userType=='3'){
                     ppsql+= ' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c = :myPosition ';
                 }
                 else if(userType=='2'){
                    ppsql+= ' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :myPosition ';
                 }
                 else{// if(userType=='1'){
                    ppsql+= ' and AxtriaSalesIQTM__Position__c = :myPosition ';
                 }
             /*}
             else{
                 ppsql+= ' and AxtriaSalesIQTM__Position__c = :selectedPosition ';
             }*/
             
             System.debug('ppsql'+ppsql);
             ppToUpdateList = Database.query(ppsql);
             
             for(AxtriaSalesIQTM__Position_Product__c ppToUpdate: ppToUpdateList){
                 //ppToUpdate.id = TIProduct.id;
                 /*ppToUpdate.Vacation_Days__c = TIProduct.Vacation_Days;
                 ppToUpdate.Holidays__c = TIProduct.Holidays;
                 ppToUpdate.Other_Days_Off__c = TIProduct.Other_Days_Off;*/
                 ppToUpdate.Vacation_Days__c = Integer.ValueOf(changedTIProductMap.get('VacationDays'));
                 ppToUpdate.Holidays__c = Integer.ValueOf(changedTIProductMap.get('Holidays'));
                 ppToUpdate.Other_Days_Off__c = Integer.ValueOf(changedTIProductMap.get('OtherDaysOff'));
                 //ppToUpdate.Calls_Day__c = Integer.ValueOf(changedTIProductMap.get('Calls_Day'));
             }
             
             System.debug('ppToUpdateList'+ppToUpdateList);
             
             update ppToUpdateList;
             //cimConfig();
             
             //RunTeamInstanceProduct_AZ_Trigger =false;
             
             for(Team_Instance_Product_AZ__c az : posProdList){
                TIPTempRec = new Team_Instance_Product_AZ__c();
                 TIPTempRec.id = az.id;
                 TIPTempRec.Vacation_Days__c = Integer.ValueOf(changedTIProductMap.get('VacationDays'));
                 TIPTempRec.Holidays__c = Integer.ValueOf(changedTIProductMap.get('Holidays'));
                 TIPTempRec.Other_Days_Off__c = Integer.ValueOf(changedTIProductMap.get('OtherDaysOff'));
                 //TIPTempRec.Calls_Day__c = tipiw.CallsPerDay;//Integer.ValueOf(changedTIProductMap.get('Calls_Day'));
                 TIPToUpdateList.add(TIPTempRec);
             }
             update TIPToUpdateList;
             
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Saved Succesfully!'));
             
         }
         else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing Saved!'));
         }
         
         //displayPopUpcallCapacity = false;
     }
     
     @RemoteAction
     public static void saveCallCapacity2(String teamInstance, String TIProductLevel,String TIProductselectedradio, String userType, Map<String,Integer> posProdMap){
         //Map<String,Integer> posProdMap = (Map<String,Integer>) JSON.deserialize(TIsList, Map<String,Integer>.Class);
         //Map<String,String> changedTIProductMap = (Map<String,String>) JSON.deserialize(jSONStringForTIProduct, Map<String,String>.Class);
         System.debug('TIProduct.selectedradio'+TIProductselectedradio);

         Map<String,CallPlanCallCapacity__c> CallCapacityCustSetting =  new Map<String,CallPlanCallCapacity__c>();
         CallCapacityCustSetting=CallPlanCallCapacity__c.getAll();
         
         CallPlanCallCapacity__c callplancapToUpdate = new CallPlanCallCapacity__c();
         
         String TeamInstancenametemp = '';
         TeamInstancenametemp = [select name from AxtriaSalesIQTM__Team_Instance__c where id= :teamInstance].name;
         
         if(CallCapacityCustSetting.containsKey(TeamInstancenametemp)){
             callplancapToUpdate.id=CallCapacityCustSetting.get(TeamInstancenametemp).id;
             if(TIProductLevel=='By Team'){
                 callplancapToUpdate.is_Team_Level__c= true;
             }
             else{
                 callplancapToUpdate.is_Team_Level__c= false;
             }
         }
         update callplancapToUpdate;
         
         
         /*if(posProdList.size()>0){
             if(posProdList[0]!=null){
                 if(posProdList[0].id=='All Products'){
                     
                 }
             }
         }*/
         
         
         
         //if(TIProductselectedradio=='All Positions'){
             
             //RunTeamInstanceProduct_AZ_Trigger =true;
             
             List<Team_Instance_Product_AZ__c> TIPToUpdateList = new List<Team_Instance_Product_AZ__c>();
             Team_Instance_Product_AZ__c TIPTempRec = new Team_Instance_Product_AZ__c();
             
             
             if(TIProductLevel=='By Team'){
                 
                 Integer tempCallsperd= Integer.valueof(posProdMap.get('All Products'));
                 
                 posProdMap = new Map<String,Integer>();
                 for(Team_Instance_Product_AZ__c az : [Select Id, Name, CurrencyIsoCode, Effective_End_Date__c, Effective_Start_Date__c, 
                 Team_Instance__c, Team_Instance__r.name, 
                 Business_Days_in_Cycle_Formula__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, 
                 Other_Days_Off__c, Vacation_Days__c, Product_Catalogue__c, Product_Catalogue__r.name
                 FROM Team_Instance_Product_AZ__c where Team_Instance__c = :teamInstance]){
                     posProdMap.put(az.id, tempCallsperd);
                 }
         
             }
             
             for(String tipiw : posProdMap.keySet()){
                 TIPTempRec = new Team_Instance_Product_AZ__c();
                 TIPTempRec.id = tipiw;
                 /*TIPTempRec.Vacation_Days__c = Integer.ValueOf(changedTIProductMap.get('VacationDays'));
                 TIPTempRec.Holidays__c = Integer.ValueOf(changedTIProductMap.get('Holidays'));
                 TIPTempRec.Other_Days_Off__c = Integer.ValueOf(changedTIProductMap.get('OtherDaysOff'));*/
                 TIPTempRec.Calls_Day__c = posProdMap.get(tipiw);//Integer.ValueOf(changedTIProductMap.get('Calls_Day'));
                 TIPToUpdateList.add(TIPTempRec);
             }
             update TIPToUpdateList;
             
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Saved Succesfully!'));
             
         
         /*else if(TIProductselectedradio=='Only This Position'){ // || userType=='1'
             AxtriaSalesIQTM__Position_Product__c ppToUpdate = new AxtriaSalesIQTM__Position_Product__c();
             ppToUpdate.id = TIProductid;
             ppToUpdate.Vacation_Days__c = Integer.ValueOf(changedTIProductMap.get('VacationDays'));
             ppToUpdate.Holidays__c = Integer.ValueOf(changedTIProductMap.get('Holidays'));
             ppToUpdate.Other_Days_Off__c = Integer.ValueOf(changedTIProductMap.get('OtherDaysOff'));
             ppToUpdate.Calls_Day__c = Integer.ValueOf(changedTIProductMap.get('Calls_Day'));
             update ppToUpdate;
             //cimConfig();
         }*/
         /*else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing Saved!'));
         }*/
         
         /*System.debug('TIProduct.Vacation_Days'+TIProduct.Vacation_Days);
         System.debug('TIProduct.Holidays'+TIProduct.Holidays);
         System.debug('TIProduct.Other_Days_Off'+TIProduct.Other_Days_Off);
         
         System.debug('TIProduct.selectedradio'+TIProduct.selectedradio);
         
         System.debug('ppToUpdate.Vacation_Days__c'+ppToUpdate.Vacation_Days__c);
         System.debug('ppToUpdate.Holidays__c'+ppToUpdate.Holidays__c);
         System.debug('ppToUpdate.Other_Days_Off__c'+ppToUpdate.Other_Days_Off__c);*/
         
         //System.debug('ppToUpdate'+ppToUpdate);
         
         
         //displayPopUpcallCapacity = false;
     }
    
    public class TIProductInnerWrapper
    {
        public string id                                 {get;set;}
        public Integer CallsPerDay                       {get;set;}
        public Integer CallCapFormula                    {get;set;}
        public string ProdName                           {get;set;}

        public TIProductInnerWrapper(String id1, String ProdName1,Integer CallCapFormula1, Integer CallsPerDay1)
        {
            id                               =       id1;
            ProdName                         =       ProdName1;
            CallCapFormula                   =       CallCapFormula1;
            CallsPerDay                      =       CallsPerDay1;
            
        }
    }
    
    public class TIProductWrapper
    {
        public string id                                 {get;set;}
        public string name                               {get;set;}
        public date startDate                            {get;set;}
        public date endDate                              {get;set;}
        public string currencyiso                        {get;set;}
        public Integer metric1                           {get;set;}
        public Integer metric2                           {get;set;}
        public Integer metric3                           {get;set;}
        public Integer metric4                           {get;set;}
        public Integer metric5                           {get;set;}
        public Integer metric6                           {get;set;}
        public Integer metric7                           {get;set;}
        public Integer metric8                           {get;set;}
        public Integer metric9                           {get;set;}
        public Integer metric10                          {get;set;}
        public string position                           {get;set;}
        public string product                            {get;set;}
        public string productMaster                      {get;set;}
        public string positionName                       {get;set;}
        public string productName                        {get;set;}
        public string productMasterName                  {get;set;}
        public string teamInstance                       {get;set;}
        public string teamInstanceName                   {get;set;}
        public boolean isActive                          {get;set;}
        public Integer productWeight                     {get;set;}
        public Integer Business_Days_in_Cycle_Formula    {get;set;}
        public Integer Business_Days_in_Cycle            {get;set;}
        public Integer Call_Capacity_Formula             {get;set;}
        public Integer Calls_Day                         {get;set;}
        public Integer Effective_Days_in_Field_Formula   {get;set;}
        public Integer Holidays                          {get;set;}
        public Integer Other_Days_Off                    {get;set;}
        public Integer Vacation_Days                     {get;set;}
        public string selectedradio                      {get;set;}
        public string ProductCatalog                     {get;set;}
        public string ProductCatalogName                 {get;set;}
        public Integer Total_Call_Capacity               {get;set;}
        
        public List<TIProductInnerWrapper> posProdList    {get;set;}
        public String posProdListString
        {get{
            return JSON.serialize((List<TIProductInnerWrapper>) posProdList);
        }
        
        set;}
        
        public Map<String,Integer> posProdInnerMap
        {get{
            Map<String,Integer> temp = new Map<String,Integer>();
            for(TIProductInnerWrapper a: posProdList){
                temp.put(a.id,a.CallsPerDay);
            }
            return temp;
        }
        set;}
        
        public String posProdInnerMapString
        {get{
            return JSON.serialize((Map<String,Integer>) posProdInnerMap);
        }
        set;}
        
        
        
        
        public List<SelectOption> getOptions() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All Positions','All Positions'));
            options.add(new SelectOption('Unchanged Positions','Unchanged Positions'));
            //options.add(new SelectOption('Only This Position','Only This Position'));
            return options;
        }

        public TIProductWrapper(Team_Instance_Product_AZ__c posProd ,string radioselected,Integer totalCallCap,List<TIProductInnerWrapper> posProdListTemp )
        {
            selectedradio=radioselected;//'Only This Position';
            id                               =       posProd.ID;
            name                             =       posProd.Name;
            currencyiso                      =       posProd.CurrencyIsoCode;
            endDate                          =       posProd.Effective_End_Date__c;
            startDate                        =       posProd.Effective_Start_Date__c;
            posProdList                     =     posProdListTemp;
            /*metric10                         =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric10__c);
            metric1                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric1__c);
            metric2                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric2__c);
            metric3                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric3__c);
            metric4                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric4__c);
            metric5                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric5__c);
            metric6                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric6__c);
            metric7                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric7__c);
            metric8                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric8__c);
            metric9                          =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Metric9__c);*/
            /*position                         =       posProd.AxtriaSalesIQTM__Position__c;
            productMaster                    =       posProd.AxtriaSalesIQTM__Product_Master__c;
            product                          =       posProd.AxtriaSalesIQTM__Product__c;*/
            teamInstance                     =       posProd.Team_Instance__c;
            teamInstanceName                 =       posProd.Team_Instance__r.name;
            //isActive                         =       posProd.AxtriaSalesIQTM__isActive__c;
            //positionName                         =       posProd.AxtriaSalesIQTM__Position__r.Name;
            //productMasterName                    =       posProd.AxtriaSalesIQTM__Product_Master__r.Name;
            //productName                          =       posProd.AxtriaSalesIQTM__Product__r.Name;
            ProductCatalog                          =       posProd.Product_Catalogue__c;
            ProductCatalogName                          =       posProd.Product_Catalogue__r.name;
            
            /*if(posProd.AxtriaSalesIQTM__Product_Weight__c!=null){
                productWeight                    =       Integer.ValueOf(posProd.AxtriaSalesIQTM__Product_Weight__c);
            }
            else{
                productWeight = 0;
            }*/
            
            if(posProd.Business_Days_in_Cycle_Formula__c!=null){
                Business_Days_in_Cycle_Formula                          =       Integer.ValueOf(posProd.Business_Days_in_Cycle_Formula__c);
            }
            else{
                Business_Days_in_Cycle_Formula = 0;
            }
            /*if(posProd.Business_Days_in_Cycle__c!=null){
                Business_Days_in_Cycle                          =       Integer.ValueOf(posProd.Business_Days_in_Cycle__c);
            }
            else{
                Business_Days_in_Cycle = 0;
            }*/
            if(posProd.Call_Capacity_Formula__c!=null){
                Call_Capacity_Formula                          =       Integer.ValueOf(posProd.Call_Capacity_Formula__c);
            }
            else{
                Call_Capacity_Formula = 0;
            }
            if(posProd.Calls_Day__c!=null){
                Calls_Day                          =       Integer.ValueOf(posProd.Calls_Day__c);
            }
            else{
                Calls_Day = 0;
            }
            if(posProd.Effective_Days_in_Field_Formula__c!=null){
                Effective_Days_in_Field_Formula                          =       Integer.ValueOf(posProd.Effective_Days_in_Field_Formula__c);
            }
            else{
                Effective_Days_in_Field_Formula = 0;
            }
            if(posProd.Holidays__c!=null){
                Holidays                          =       Integer.ValueOf(posProd.Holidays__c);
            }
            else{
                Holidays = 0;
            }
            if(posProd.Other_Days_Off__c!=null){
                Other_Days_Off                          =       Integer.ValueOf(posProd.Other_Days_Off__c);
            }
            else{
                Other_Days_Off = 0;
            }
            if(posProd.Vacation_Days__c!=null){
                Vacation_Days                          =       Integer.ValueOf(posProd.Vacation_Days__c);
            }
            else{
                Vacation_Days = 0;
            }
            
            
            if(totalCallCap!=-1){
                Total_Call_Capacity=totalCallCap;
            }
            else{
                Total_Call_Capacity=Call_Capacity_Formula;
            }
            
        }
    }
    
    

}