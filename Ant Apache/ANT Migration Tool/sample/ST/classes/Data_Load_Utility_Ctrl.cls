public with sharing class Data_Load_Utility_Ctrl {
  public List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceNames = new List<AxtriaSalesIQTM__Team_Instance__c>();
    public string selectedTeamInstanceName { get; set; }
    public string selectedDate {get;set;}
    public string accField {get;set;}
    public string qname {get;set;}
    
    public Boolean displayPACP {get;set;}
    public Boolean displayPopulate_Staging_Survey_Data {get;set;}
    public Boolean displayServey_Response {get;set;}
    public Boolean displayBU_Response_Account_Update {get;set;}
    public Boolean displayBU_Response_TSF_Accessibility {get;set;}
    public Boolean displayProd_Metrics_to_Survey_Data {get;set;}
    public Boolean displayPos_Acc_Accessibility_Update {get;set;}
    
    
    
    public Data_Load_Utility_Ctrl() {
        
        displayPACP = true;
        displayPopulate_Staging_Survey_Data = true;
        displayServey_Response = true;
        displayBU_Response_Account_Update = true;
        displayBU_Response_TSF_Accessibility = true;
        displayProd_Metrics_to_Survey_Data = true;
        displayPos_Acc_Accessibility_Update = true;
        
        /*displayPACP = false;
        displayPopulate_Staging_Survey_Data = false;
        displayServey_Response = false;
        displayBU_Response_Account_Update = false;
        displayBU_Response_TSF_Accessibility = false;
        displayProd_Metrics_to_Survey_Data = false;
        displayPos_Acc_Accessibility_Update = false;*/
        
        list<Data_Load_Display_Items__c> Data_LoadCustSetting =  new list<Data_Load_Display_Items__c>();
        Data_LoadCustSetting.add(Data_Load_Display_Items__c.getValues('Data_Load_Utility_Run_Page'));
        //Boolean show = false;
        
        if(Data_LoadCustSetting.size()>0 && Data_LoadCustSetting!=null){ // || Data_LoadCustSetting!=null
            
            for(Data_Load_Display_Items__c dldi : Data_LoadCustSetting){
                
                if(dldi!=null){
                    if(dldi.Batch_Name__c.contains('PACP'))
                        displayPACP = dldi.Show__c;
                    
                    if(dldi.Batch_Name__c.contains('Populate_Staging_Survey_Data'))
                        displayPopulate_Staging_Survey_Data = dldi.Show__c;
                        
                    if(dldi.Batch_Name__c.contains('Servey_Response'))
                        displayServey_Response = dldi.Show__c;
                    
                    if(dldi.Batch_Name__c.contains('BU_Response_Account_Update'))
                        displayBU_Response_Account_Update = dldi.Show__c;
                    
                    if(dldi.Batch_Name__c.contains('BU_Response_TSF_Accessibility'))
                        displayBU_Response_TSF_Accessibility = dldi.Show__c;
                    
                    if(dldi.Batch_Name__c.contains('Prod_Metrics_to_Survey_Data'))
                        displayProd_Metrics_to_Survey_Data = dldi.Show__c;
                    
                    if(dldi.Batch_Name__c.contains('Pos_Acc_Accessibility_Update'))
                        displayPos_Acc_Accessibility_Update = dldi.Show__c;
                }
                
            }
            
        }
        
        
    }
    
    
    public list<selectoption> teamInstanceList
    {
        get{
            teamInstanceNames = [Select Name From AxtriaSalesIQTM__Team_Instance__c];
            teamInstanceList = new List<SelectOption>();
 
           // UserList = new List<SelectOption>();
            teamInstanceList.add(new SelectOption('All', 'All'));
            for(AxtriaSalesIQTM__Team_Instance__c tiName : teamInstanceNames)
            {
                teamInstanceList.add(new SelectOption(tiName.Name, tiName.Name));
            }
            return teamInstanceList;

        }
        set;
    }
    /*public list<selectoption> createdDateList
    {
      get{
        createdDateList = new List<SelectOption>();
        createdDateList.add(new SelectOption('None','Please Select'));
        createdDateList.add(new SelectOption('Today','Today'));
        createdDateList.add(new SelectOption('Yesterday','Yesterday'));
        return createdDateList;
      }
      set;
    }*/
    
    /*public void run_BU_Response_Batch(){
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        BU_Response_Insert_Utility pacpBatch = new BU_Response_Insert_Utility(selectedTeamInstanceName);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'BU Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }*/
    
    public void run_Prod_Metrics_to_Survey_Data_Batch(){
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        Prod_Metrics_to_Survey_Data_Utility pacpBatch = new Prod_Metrics_to_Survey_Data_Utility(selectedTeamInstanceName);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Prod Metrics to Survey Data Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
    
    public void run_BU_Response_Account_Update_Batch(){
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        BU_Response_Account_Update_Utility_new pacpBatch = new BU_Response_Account_Update_Utility_new(selectedTeamInstanceName,accField,qname);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
    
    public void run_BU_Response_TSF_Accessibility_Batch(){
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        BU_Response_TSF_Update_Utility_new pacpBatch = new BU_Response_TSF_Update_Utility_new(selectedTeamInstanceName,qname);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
    
    public void run_Populate_Staging_Survey_Data_Batch(){
        /*if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{*/
        Survey_Data_Transformation_Utility pacpBatch = new Survey_Data_Transformation_Utility();//selectedTeamInstanceName
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey_Data_Transformation Batch Ran Successfully!!'));// for Selected Team Instance
      //}
    }
    
    public void run_Servey_Response_Batch(){
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        Survey_Def_Prof_Para_Meta_Insert_Utility pacpBatch = new Survey_Def_Prof_Para_Meta_Insert_Utility(selectedTeamInstanceName);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
    /*public void runPACPBatch(){
    }*/

    
    public void run_Pos_Acc_Accessibility_Update_Batch(){
      if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new PACP_NS_Upload_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        Position_Account_TSFAcc_Update_Utility pacpBatch = new Position_Account_TSFAcc_Update_Utility(selectedTeamInstanceName);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Position_Account_TSFAcc_Update_Utility Batch Ran for Selected Team Instance Successfully!!'));
      }
      
    }
    
    public void run_PACP_Batch(){
      if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new PACP_NS_Upload_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else{
        PACP_lookups_Utility pacpBatch = new PACP_lookups_Utility(selectedTeamInstanceName);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'PACP Batch Ran for Selected Team Instance Successfully!!'));
      
        /*if(selectedDate == 'None'){
          Database.executeBatch(new PACP_NS_Upload_Utility(selectedTeamInstanceName),2000);
        }
        else{
          Database.executeBatch(new PACP_NS_Upload_Utility(selectedTeamInstanceName,selectedDate),2000);
        }*/
      }
      
    }
}