@isTest
private class GenericBatchTest {
    static testMethod void testMethod1() {
        	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User tUser = new User(Alias = 'Siva', Email='SG@Gmail.com', 
                                    EmailEncodingKey='UTF-8', LastName='SIVA', LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', ProfileId = p.Id, 
                                    TimeZoneSidKey='America/Los_Angeles', UserName='A1266@Axtria.com.dev',
                                 ManagerId = UserInfo.getUserId());
            insert tUser; 
             //List <Account> lstAccount = new List<Account>();
            system.runas(tuser)
            {
             Account testAccount = new Account();
             testAccount.Name='Account1' ;
             testAccount.AxtriaARSnT__Marketing_Code__c='ES' ;
             insert testAccount;

             Account Account2 = new Account();
             Account2.Name='Account2' ;
             Account2.AxtriaARSnT__Marketing_Code__c='ES' ;
             insert Account2;

             test.StartTest();
             Database.executeBatch(new GenericBatch('Account','Name','Account1'),1);
             //Database.executeBatch(new GenericBatch('Account','','Account1'),1);
             //Database.executeBatch(new GenericBatch('Account','',''),1);
             test.StopTest();
         }

    }
}