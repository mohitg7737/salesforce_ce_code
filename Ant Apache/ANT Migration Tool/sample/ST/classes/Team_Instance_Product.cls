public with sharing class Team_Instance_Product {
    
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    public List<SelectOption> allTeamInstance                {get;set;}
    public string teaminstanceSelected{get; set;}
    public list<SelectOption>allcycles {get;set;}
    public string selectedcycle {get;set;}

    List<Product_Catalog__c> allProductsAtTeamInstance {get;set;}
    
    public String countryID {get;set;}
    public Map<string,string> successErrorMap;
    public AxtriaSalesIQTM__Country__c Country;
    
 
    public List<ProductTeamInstanceRefer> allPosProductsRefer {get;set;} 
    public String phyRecs {get;set;}
    public Set<String> currentTeamInstanceProducts;
    public list<AxtriaSalesIQTM__Team_Instance__c>teaminst {get;set;}
    public string userType                                   {get;set;}

    public string selectedTeamInstance                       {get;set;}
    Public String TeamInstancename {get;set;}
    public String jsonString                                {get;set;}

    public Team_Instance_Product()
    {
        allTeamInstance = new list<SelectOption>();
        allcycles = new list<SelectOption>();
        countryID = SalesIQUtility.getCookie('CountryID');
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }
        userType = '4';
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId(), countryID);

        if(loggedInUserData!=null && loggedInUserData.size()>0)
        {  
            TeamInstancename = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
            selectedTeamInstance = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
            teaminstanceSelected = selectedTeamInstance;
            
            teaminst =[select id,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id=:selectedTeamInstance limit 1];
            if(teaminst[0].cycle__r.Name!=null){
              allcycles.add(new SelectOption(teaminst[0].cycle__r.Name,teaminst[0].cycle__r.Name));
              selectedcycle=teaminst[0].cycle__r.Name;
            }
            
            //Code added by siva on 26-09-2017
            for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
            {
                allTeamInstance.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
            }
        }
        

        

       // populateTeamInstanceProduct();
    }

    public void teamInstanceChanged()
    {
        
        selectedTeamInstance = teaminstanceSelected;
        
        loggedInUserData = [select AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance  order by AxtriaSalesIQTM__Position__r.Name];
        
        //TeamInstancename = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
              
        //system.debug('----Selected position is:'+selectedPosition);
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {   
            if(userInfo.AxtriaSalesIQTM__Team_Instance__c==teamInstanceSelected){
                
                allcycles = new list<SelectOption>();
                
                teaminst =[select id,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id=:selectedTeamInstance limit 1];
                if(teaminst[0].cycle__r.Name!=null){
                  allcycles.add(new SelectOption(teaminst[0].cycle__r.Name,teaminst[0].cycle__r.Name));
                  selectedcycle=teaminst[0].cycle__r.Name;
                }
                //allTerritory=new list<selectOption>();
                
                //loggedInUsercheck(userInfo);
                TeamInstancename = userInfo.AxtriaSalesIQTM__Team_Instance__r.Name;
            }
        }
        
    }

    @RemoteAction
    public static List<ProductTeamInstanceRefer> populateTeamInstanceProduct(String selectedTeamInstance, String countryID)
    {
        system.debug('++++++++++++ Hey Country ID is '+ countryID);

        List<Product_Catalog__c> allProductsAtTeamInstance = [select id, Name,Veeva_External_ID__c,External_Id__c, Detail_Group__c, Team_Instance__r.Name, Team_Instance__c  from Product_Catalog__c where Country__c = :countryID];

        system.debug('++++++++++++ Hey All Products are '+ allProductsAtTeamInstance);
        Set<String> uniqueRecs = new Set<String>();
        List<ProductTeamInstanceRefer> allPosProductsRefer = new List<ProductTeamInstanceRefer>();

        List<Product_Catalog__c> allCurrentTeamInstanceProducts = [select id, Name, Veeva_External_ID__c, External_Id__c, Detail_Group__c, Team_Instance__r.Name, Team_Instance__c  from Product_Catalog__c where Team_Instance__c = :selectedTeamInstance];
        
        system.debug('++++++++++++ Hey All Products In MY Team are '+ allCurrentTeamInstanceProducts);
        Set<String> currentTeamInstanceProductsSet = new Set<String>();

        for(Product_Catalog__c pc : allCurrentTeamInstanceProducts)
        {
            currentTeamInstanceProductsSet.add(pc.Veeva_External_ID__c);        
        }

        for(Product_Catalog__c pc : allProductsAtTeamInstance)
        {
            String uniqueKey = pc.Veeva_External_ID__c;
            system.debug('+++++++++++ Hey Unique key is '+ uniqueKey);

            if(!uniqueRecs.contains(uniqueKey))
            {
                uniqueRecs.add(uniqueKey);

                ProductTeamInstanceRefer p1 = new ProductTeamInstanceRefer();
                p1.id = pc.ID;
                p1.name = pc.Name;
                p1.externalID = pc.Veeva_External_ID__c;
                p1.detailGroup = pc.Detail_Group__c;

                if(currentTeamInstanceProductsSet.contains(uniqueKey))
                {
                    p1.presentInMyTeam = true;
                }
                else
                {
                    p1.presentInMyTeam = false;
                }
                allPosProductsRefer.add(p1);
            }
            system.debug('++++++++ Hey Unique Recs are '+ uniqueRecs);
        }
        return allPosProductsRefer;
        //jsonString = JSON.serialize(allPosProductsRefer); 
    }

    public void addDropProductsTeam()
    {
        system.debug('+++++++ Hye PhyRecs are '+ phyRecs);
        if(phyRecs.length() > 0)
        {
            List<Product_Catalog__c> allCurrentTeamInstanceProducts = [select id, Name, Veeva_External_ID__c, External_Id__c, Detail_Group__c, Team_Instance__r.Name, Team_Instance__c  from Product_Catalog__c where Team_Instance__c = :selectedTeamInstance];
        
            Set<String> currentTeamInstanceProductsSet = new Set<String>();

            for(Product_Catalog__c pc : allCurrentTeamInstanceProducts)
            {
                currentTeamInstanceProductsSet.add(pc.Veeva_External_ID__c);        
            }

            List<String> allRecsIds = phyRecs.split(',');
                
            List<Product_Catalog__c> allProducsCatalogRecs = [select id, Name, Veeva_External_ID__c, External_Id__c, Detail_Group__c, Team_Instance__r.AxtriaSalesIQTM__Team__c , Team_Instance__c  from Product_Catalog__c where Veeva_External_ID__c in :allRecsIds];

            List<String> newAssignments = new List<String>();

            List<Product_Catalog__c> newPcRecs = new List<Product_Catalog__c>();
            List<String> alreadyProcessed = new LIst<String>();

            for(Product_Catalog__c pc : allProducsCatalogRecs)
            {
                if(!(currentTeamInstanceProductsSet.contains(pc.Veeva_External_ID__c) || alreadyProcessed.contains(pc.Veeva_External_ID__c)))
                {
                    Product_Catalog__c newpc = new Product_Catalog__c();
                    newpc.Name                   =  pc.Name;
                    newpc.External_Id__c         =  pc.Veeva_External_ID__c + '_'+ selectedTeamInstance;

                    newpc.Veeva_External_ID__c   =  pc.Veeva_External_ID__c;
                    newpc.Team__c                =  pc.Team_Instance__r.AxtriaSalesIQTM__Team__c;
                    newpc.Team_Instance__c       =  selectedTeamInstance;
                    newpc.Product_Code__c        =  pc.Veeva_External_ID__c;
                    newpc.Detail_Group__c        =  pc.Detail_Group__c;
             
                    newPcRecs.add(newpc);
                    alreadyProcessed.add(pc.Veeva_External_ID__c);
                }
            }

            insert newPcRecs;

            Set<String> removedIds = new Set<String>();
            for(String productID : currentTeamInstanceProductsSet)
            {
                if(!allRecsIds.contains(productID))
                {
                    removedIds.add(productID);
                }
            }   

            if(removedIds.size()>0)
                delete [select id from Product_Catalog__c where Veeva_External_ID__c in :removedIds and Team_Instance__c = :selectedTeamInstance];

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Sucessfully Added/Removed Products'));
        }
        
    }

    public class ProductTeamInstanceRefer
    {
        public String id {get;set;}
        public String name {get;set;}
        public String externalID {get;set;}
        public String detailGroup {get;set;}
        public String teamInstanceName {get;set;}
        public Boolean presentInMyTeam {get;set;}

    }

}