public with sharing class MergeUnmergeDelta implements Schedulable  {
  Public list<SIQ_CM_Merge_Unmerge__c> MergeList {get;set;}
  public String query {get;set;}
  public DateTime lastjobDate;
  public Integer recordsProcessed;
  Public map<string,string>AccountIdmap {get;set;}
  public set<String> Accountnumbers {get;set;}
  public map<String, String> AccCountryMarket{get;set;} 
  public list<Merge_Unmerge__c> Insertmergelist{get;set;}
  public map<String, String> mapCountryCode ;
    public map<String, String> mapVeevaCode ;
     public map<String, String> mapMarketingCode ;
     public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
    public Integer errorcount;
    Public String nonProcessedAcs {get;set;}
    AxtriaSalesIQTM__TriggerContol__c customsetting;

  public String batchID;
    public MergeUnmergeDelta(){
      customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('PositionAccountTrigger');  
      MergeList = new list<SIQ_CM_Merge_Unmerge__c>();
      AccCountryMarket=new map<String,string>();
      query = '';
      nonProcessedAcs = '';
      recordsProcessed = 0;
      errorcount=0;
      lastjobDate=null;
      AccountIdmap = new map<String,string>();
      Accountnumbers = new set<String>();
      Insertmergelist = new list<Merge_Unmerge__c>();
      PopulateData();
    }

     public void execute(System.SchedulableContext SC){
       MergeUnmergeDelta ppo=new MergeUnmergeDelta ();
    } 
    public void PopulateData(){
      customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true;
      update customsetting;  
      mapCountryCode = new map<String, String>();
         mapVeevaCode=new map<String, String>();
        mapMarketingCode =new map<String, String>();
        countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        for(AxtriaSalesIQTM__Country__c c : countrylist){
            if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
                mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
                                                                                    
            }
                                                                             
                                                                             
             
        }
            list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
        for(SIQ_MC_Country_Mapping__c c : countryMap){
            if(!mapVeevaCode.containskey(c.SIQ_Country_Code__c)){
                mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
            }
              if(!mapMarketingCode.containskey(c.SIQ_Country_Code__c)){
                mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
            }
        }
        
      List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=null;
         if(cycleList.size()>0){
        cycle= cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         }
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MergeUnmerge Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'MergeUnmerge Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;

        batchID = sJob.Id;
        recordsProcessed =0;

        query = 'select SIQ_Country_Code__c,SIQ_Customer_Class__c,SIQ_External_ID__c,SIQ_Losing_Source_ID__c,' +
            'SIQ_Market_Code__c,SIQ_MDM_Customer_GUID_1__c,SIQ_MDM_Customer_GUID_2__c,SIQ_MDM_Customer_ID_1__c, '+
            'SIQ_MDM_Customer_ID_2__c,SIQ_MDM_Previous_Customer_GUID__c,SIQ_Surviving_Source_ID__c '+
            ' from SIQ_CM_Merge_Unmerge__c';

        if(lastjobDate!=null){
            query = query + ' Where LastModifiedDate  >=:  lastjobDate '; 
        }
        System.debug('query'+ query);

        MergeList = Database.query(query);

        System.debug('=====MergeList====='+MergeList);
        System.debug('======MergeList.Size():::'+MergeList.size());
        set<String>Loosingset = new set<String>();
        for(SIQ_CM_Merge_Unmerge__c  obj : MergeList){
          //Accountnumbers
          //if(Accountnumbers.contains(merge.SIQ_MDM_Customer_GUID_1__c))
          AccCountryMarket.put(obj.SIQ_MDM_Customer_GUID_1__c,obj.SIQ_Country_Code__c);
          Accountnumbers.add(obj.SIQ_MDM_Customer_GUID_1__c);
          Accountnumbers.add(obj.SIQ_MDM_Customer_GUID_2__c);
          Loosingset.add(obj.SIQ_MDM_Customer_GUID_2__c);
        }

        System.debug('======Accountnumbers::=='+Accountnumbers);
        set<String>Accountset = new set<String>();
        list<Account>AccountList = [select id,External_HCP_No__c from account where External_HCP_No__c IN:Accountnumbers];
        for(Account acou : AccountList){
            Accountset.add(acou.External_HCP_No__c);
        }
        set<String>notpreset = new set<String>();
        for(String Ac : Accountnumbers){
            if(!Accountset.contains(Ac)){
              if(!Loosingset.contains(Ac) ){
                notpreset.add(Ac);
                }
            }

        }
        if(notpreset.size()>0){
            list<Account>templist = new list<Account>();
            for(String hcp : notpreset){
                Account ac = new Account ();
                ac.Name = 'DUMMY';
               
               ac.AxtriaSalesIQTM__Country__c=mapCountryCode.get(mapVeevaCode.get( AccCountryMarket.get(hcp)));
                 ac.Country_Code__c=mapVeevaCode.get(AccCountryMarket.get(hcp));
                 ac.Marketing_Code__c=mapMarketingCode.get(AccCountryMarket.get(hcp));
                ac.External_HCP_No__c = hcp;
                ac.External_Account_Id__c= hcp;
                ac.External_Account_Number__c= hcp;
                ac.AxtriaSalesIQTM__External_Account_Number__c= hcp;
                templist.add(ac);
            }
            insert templist;
            for(Account temp :templist){
                if(!AccountIdmap.containskey(temp.External_HCP_No__c)){
                    AccountIdmap.put(temp.External_HCP_No__c,temp.id);
                }
            }

        }
        System.debug('=======Accountlist Size++++'+AccountList.size());
        for(Account a : AccountList){
          if(!AccountIdmap.containskey(a.External_HCP_No__c)){
            AccountIdmap.put(a.External_HCP_No__c,a.id);
          }
        }
        set<String> uniqueid = new set<String>();    
        if(!MergeList.isEmpty()){
          //Insertmergelist----Merge_Unmerge__c
          for(SIQ_CM_Merge_Unmerge__c  obj : MergeList){
          String key=obj.SIQ_MDM_Customer_GUID_1__c+'_'+obj.SIQ_MDM_Customer_GUID_2__c;
            Merge_Unmerge__c newobj = new Merge_Unmerge__c();
            if(!uniqueid.contains(key)){
           // newobj.Country_Code__c=obj.SIQ_Country_Code__c;
            if(mapVeevaCode.get(obj.SIQ_Country_Code__c)!=null){
           // account.AxtriaSalesIQTM__Country__c =mapCountryCode.get(mapVeevaCode.get(acc.SIQ_Country__c));
            newobj.Country_Code__c=mapVeevaCode.get(obj.SIQ_Country_Code__c);
            }
            
            newobj.Customer_Class__c=obj.SIQ_Customer_Class__c;
            //newobj.External_ID__c=obj.SIQ_External_ID__c;
            newobj.External_ID__c=key;
            newobj.Losing_Source_ID__c = obj.SIQ_Losing_Source_ID__c;
           // newobj.Market_Code__c=obj.SIQ_Market_Code__c;
           if(mapMarketingCode.get(obj.SIQ_Country_Code__c)!=null){
                                       
              newobj.Market_Code__c=mapMarketingCode.get(obj.SIQ_Country_Code__c);
              
             
            }
            newobj.MDM_Customer_GUID_1__c = obj.SIQ_MDM_Customer_GUID_1__c;
            newobj.MDM_Customer_GUID_2__c = obj.SIQ_MDM_Customer_GUID_2__c;
                newobj.GUID_1_ID__c = AccountIdmap.get(obj.SIQ_MDM_Customer_GUID_1__c);
                newobj.GUID_2_ID__c = AccountIdmap.get(obj.SIQ_MDM_Customer_GUID_2__c);

            newobj.MDM_Customer_ID_1__c = obj.SIQ_MDM_Customer_ID_1__c;
            newobj.MDM_Customer_ID_2__c = obj.SIQ_MDM_Customer_ID_2__c;
            newobj.MDM_Previous_Customer_GUID__c =obj.SIQ_MDM_Previous_Customer_GUID__c;
           // newobj.Publish_Date__c = obj.SIQ_Publish_Date__c;
           // newobj.Publish_Event__c = obj.SIQ_Publish_Event__c;
            newobj.Surviving_Source_ID__c = obj.SIQ_Surviving_Source_ID__c;
             uniqueid.add(key);
            recordsProcessed ++;

            Insertmergelist.add(newobj);
            }
            else
            {nonProcessedAcs +=newobj.GUID_1_ID__c+';';
                errorcount++;
            }
          }
         // Upsert Insertmergelist External_ID__c;
          Schema.SObjectField f = Merge_Unmerge__c.Fields.External_ID__c;
          database.upsert(Insertmergelist ,f,false);

        }

        if(Insertmergelist!=null && Insertmergelist.size()>0){
          System.debug('=====Calling Merge Event Processing+++++++');
          MergeEventProcessing mep = new MergeEventProcessing();
          mep.fillvalues(lastjobDate);
        }

        System.debug(recordsProcessed + ' records processed. ');
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs;         
        Scheduler_Log__c sJob2 = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob2);
        sJob2.No_Of_Records_Processed__c=recordsProcessed;
        sJob2.Job_Status__c='Successful';
         sjob.Changes__c = ErrorMsg;      
        system.debug('sJob++++++++'+sJob2);
        update sJob2;
        customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
        update customsetting;

    }
}