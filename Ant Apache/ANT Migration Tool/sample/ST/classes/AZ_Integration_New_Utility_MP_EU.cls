global class AZ_Integration_New_Utility_MP_EU implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> mCyclePlanChannel;
    List<Parent_PACP__c> pacpRecs; 
    
    global AZ_Integration_New_Utility_MP_EU(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        queryString = 'select id, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Final_TCF__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null LIMIT 50) from Parent_PACP__c where Team_Instance__c = :teamInstanceSelected';       
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        system.debug('+ HEy '+ queryString);
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Channel_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
        pacpRecs = scopePacpProRecs;
        mCyclePlanChannel = new List<SIQ_MC_Cycle_Plan_Channel_vod_O__c>();
        Set<String> allRecs = new Set<String>();

        
        for(Parent_PACP__c ppacp : pacpRecs)
        {
            Integer f2fCalls = 0;
            
            Boolean flag = false;
            SIQ_MC_Cycle_Plan_Channel_vod_O__c mcpc = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();

            String channels = 'F2F';
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
                if(pacp.Final_TCF__c == null)
                    pacp.Final_TCF__c = 0;
                    
                flag = true;
                f2fCalls = f2fCalls + Integer.valueof(pacp.Final_TCF__c);
            
                string teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                mcpc.SIQ_Cycle_Channel_vod__c       =   pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';
                mcpc.SIQ_Channel_Activity_Goal_vod__c = f2fCalls;    
                DateTime dtStart                 =   pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());
                String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
                mcpc.SIQ_Cycle_Plan_Target_vod__c   =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                
                
                mcpc.SIQ_External_Id_vod__c =teamPos + '-' +channels + '-'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                
                
            }
            mcpc.Rec_Status__c = 'Updated';
            mcpc.External_ID_Axtria__c = mcpc.SIQ_External_Id_vod__c;

            if(flag && !allRecs.contains(mcpc.SIQ_External_Id_vod__c))
            {
                mcpc.SIQ_RecordTypeId__c  = '012d0000001103Y';
                mcpc.SIQ_Channel_Activity_Goal_vod__c = Decimal.valueof(f2fCalls);
                mcpc.SIQ_Channel_Activity_Max_vod__c = Decimal.valueof(f2fCalls);
                mCyclePlanChannel.add(mcpc);
                allRecs.add(mcpc.SIQ_External_Id_vod__c);

            }

            
        }
        //create_MC_Cycle_Plan_Product_vod();
    }
    
    global void execute(Database.BatchableContext BC, List<Parent_PACP__c> scopePacpProRecs)
    {
        create_MC_Cycle_Plan_Channel_vod(scopePacpProRecs);
        
        upsert mCyclePlanChannel External_ID_Axtria__c;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new MarkMCCPchannelDeleted_EU());
    }
}