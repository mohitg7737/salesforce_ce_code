@isTest
public class update_AccTerrTest {
	@isTest
    static void updateAccTerr(){
        List<Account> acc = new List<Account>();
        for(Integer i = 1 ; i< 10;i++){
            Account ac = new Account(name = 'Test ac'+i, AccountNumber = 'Test'+i,AxtriaARSnT__Marketing_Code__c = 'Test');
            acc.add(ac);
        }
        insert acc;
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(name = 'T1');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_instance__c();
        ti.name = 'T1-Q1';
       	ti.AxtriaSalesIQTM__Team__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'XYZ';
        pos.AxtriaSalesIQTM__Team_iD__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = [Select id from AxtriaSalesIQTM__Team_Instance__c where name = 'T1-Q1'][0].Id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'X';
        
        insert pos;
        List<temp_acc_Terr__c> accTerr = new List<temp_acc_Terr__c >();
        for(Integer i =1; i<10; i++){
            temp_acc_Terr__c zt = new temp_acc_Terr__c();
            zt.Territory_ID__c = 'X';
            zt.AccountNumber__c = 'Test ac'+i;
            zt.TeamName__c = 'T1';
            accTerr.add(zt);
        } 
        insert accterr;
        Test.startTest();
    
    	Database.executeBatch(new update_accTerr(team.id,ti.id));
    
    	Test.stopTest();
    }
}