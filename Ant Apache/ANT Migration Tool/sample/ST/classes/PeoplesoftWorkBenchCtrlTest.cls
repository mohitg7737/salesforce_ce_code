@IsTest
public class PeoplesoftWorkBenchCtrlTest{
    /*
    static List<CR_Employee_Feed__c> empFeed = new List<CR_Employee_Feed__c>();
    static List<CR_Employee_Feed__c> empFeed1 = new List<CR_Employee_Feed__c>();
    static List<AxtriaSalesIQTM__Employee__c> employees;
    static list<AxtriaSalesIQTM__Position__c> positions;
    
         
    public static void createData(){
      
        List<AxtriaSalesIQTM__Team__c> teams = TestDataFactory.createTeam();
        List<AxtriaSalesIQTM__Team_Instance__c> teaminstances =  TestDataFactory.createTeamInstance(teams);
        
        positions = TestDataFactory.createPositions('TestPos',3,'KAOM',teaminstances);
        for(AxtriaSalesIQTM__Position__c pos : positions){
          pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 4, 15);  
            pos.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(2017, 8, 31);
            pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        }
        insert positions ;
        
        employees = TestDataFactory.createEmployees('Test','Emp','YES','Leave of Absence','1001',date.today(), 3);
        for(AxtriaSalesIQTM__Employee__c emp : employees){
            emp.Hire_Date__c = date.newinstance(2017, 6, 1);
        }
        insert employees ;
        
        List<AxtriaSalesIQTM__Position_Employee__c> posEmps = TestDataFactory.createPositionEmployees(employees ,positions ) ;
        for(AxtriaSalesIQTM__Position_Employee__c posemp : posEmps){
            posemp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
            posemp.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 6, 15);
            posemp.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(2017, 6, 31);
        }
        Insert posEmps ;
        
        
        empFeed = TestDataFactory.createEmpFeed('testEmpFeed','Leave of Absence', employees,positions, 3) ;
        for(CR_Employee_Feed__c crfeed : empFeed){
            crfeed.IsRemoved__c = false;
        }
        empFeed[0].Event_Name__c = 'Separation';
        empFeed[1].Event_Name__c = 'Promotion';
        empFeed[2].Event_Name__c = 'New Hire';
        insert empFeed ;
        
        InsmedRosterConfig__c rosConfig = new InsmedRosterConfig__c();
        rosConfig.Number_Of_Days__c=5;
        rosConfig.Name='Config1';
        insert rosConfig;
        
       
    }        
    
    public TestMethod static void validateController(){
        createData();
        PeoplesoftWorkBenchCtrl wrkbnch = new PeoplesoftWorkBenchCtrl ();
        wrkbnch.JsonMap = '{"a10W0000001QsItIAK":true}';
        wrkbnch.rowId = empFeed[0].id;
        wrkbnch.methodOne() ;
        wrkbnch.refresh();
        PeoplesoftWorkBenchCtrl.feed('New Hire');
        wrkbnch.toggleSort();
        wrkbnch.sendEmail();
        wrkbnch.closePopup();
        wrkbnch.showPopup();
        wrkbnch.Cancel_remove_from_workbench();
        wrkbnch.removeEventFromWorkbench();
        wrkbnch.sendemailcolumnhidden = true;
    }
	*/
}