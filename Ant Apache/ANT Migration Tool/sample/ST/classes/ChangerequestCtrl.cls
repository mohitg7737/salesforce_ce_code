public class ChangerequestCtrl {
	public map<string,list<AxtriaSalesIQTM__Change_Request__c>> crs {get;set;}
	public List<AxtriaSalesIQTM__Change_Request__c> counter {get;set;}
	public map<string,decimal> userCRCountMap {get;set;}
	public ChangerequestCtrl(){
		counter=new list<AxtriaSalesIQTM__Change_Request__c>();
		crs = new map<string,list<AxtriaSalesIQTM__Change_Request__c>>();
		userCRCountMap = new map<string,decimal>();
		fetchdata();
	}
	
	public void fetchdata(){
		for( AxtriaSalesIQTM__Change_Request__c cr: [select id,AxtriaSalesIQTM__Submitted_By__c,CreatedByID from AxtriaSalesIQTM__Change_Request__c]){
			/*if(!crs.containsKey(cr.CreatedByID)){
				crs.put(cr.CreatedByID,cr);
			}else{
				crs.put(cr.CreatedByID,cr);
			}*/
			if(!crs.containsKey(cr.CreatedByID)){
				crs.put(cr.CreatedByID,new list<AxtriaSalesIQTM__Change_Request__c>());
			}
			crs.get(cr.CreatedByID).add(cr);
			
			
		}
		for(User u : [Select id from user]){
			decimal counts = 0;
			if(crs.containsKey(u.id)){
				counts = crs.get(u.id).size();
				 //counter.add(cc);
			}
			userCRCountMap.put(u.id,counts);
			System.debug('****User id:'+u.id);
			System.debug('######CR Count:'+counts);
		}
		
	}

}