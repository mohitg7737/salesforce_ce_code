public class OutboundPositionProcessing {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;}                                
    public List<AxtriaSalesIQTM__Position__c> nationPosition {get;set;}
    public List<AxtriaSalesIQTM__Organization_Master__c> orgList {get;set;}
    public map<string,String> orgmap {get;set;}
    public map<string,AxtriaSalesIQTM__Organization_Master__c> organization {get;set;}


  public OutboundPositionProcessing (List<AxtriaSalesIQTM__Position__c> position,Integer records,String batchID){
    nationPosition = new List<AxtriaSalesIQTM__Position__c>(position);
    organization =new map<String,AxtriaSalesIQTM__Organization_Master__c>();
    orgmap=new map<String,String>();
        system.debug(nationPosition);
        orgList = new List<AxtriaSalesIQTM__Organization_Master__c>();
        orgList=[select id,Name,AxtriaSalesIQTM__Parent_Organization_Name__r.Name from AxtriaSalesIQTM__Organization_Master__c ];
        for(AxtriaSalesIQTM__Organization_Master__c org:orgList){
        orgmap.put(org.id,org.AxtriaSalesIQTM__Parent_Organization_Name__c);
        organization .put(org.id,org);
        }
        if(nationPosition.size()>0){
       positionOrganization(nationPosition,records,batchID); 
        }
  }
   public void positionOrganization(List<AxtriaSalesIQTM__Position__c> position,Integer records,String batchID){
      List<SIQ_Position_O__c> orgPosList=new List<SIQ_Position_O__c>();
      for(AxtriaSalesIQTM__Position__c pos:position){
        System.debug('***'+pos.AxtriaSalesIQTM__Position_Type__c);
        List<String> orgList=new List<String>();
        String org=pos.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c;
        System.debug('++org'+org);
         SIQ_Position_O__c obj=new SIQ_Position_O__c();
            AxtriaSalesIQTM__Organization_Master__c orgObj=organization .get(org);
            obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
            obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
            obj.SIQ_POSITION_CODE__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Team_Instance_Category__c+'_admin';
            obj.SIQ_POSITION_NAME__c=orgObj.Name;                                                  
            obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
            obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
            obj.SIQ_Partner_Flag__c='N';
            obj.SIQ_Channel__c=pos.Channel_AZ__c;  
            obj.SIQ_Sales_Team_Attribute__c=pos.Sales_Team_Attribute__c;
            obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
            obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
            obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
            obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
            obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
            obj.SIQ_Created_Date__c=pos.CreatedDate;
            obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
            obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
            obj.Unique_Id__c=orgObj.Name+'_'+pos.id;
            orgPosList.add(obj);
            records++;
            org=orgmap.get(org);
         while(orgmap.get(org)!=null){
       //  orgList.add(orgmap.get(org));
            obj=new SIQ_Position_O__c();
            orgObj=organization .get(org);
            obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
            obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
            obj.SIQ_POSITION_CODE__c=orgObj.Name;
            obj.SIQ_POSITION_NAME__c=orgObj.Name;                                                  
            obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
            obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
            obj.SIQ_Partner_Flag__c='N';
            obj.SIQ_Channel__c=pos.Channel_AZ__c;  
            obj.SIQ_Sales_Team_Attribute__c=pos.Sales_Team_Attribute__c;
            obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
            obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
            obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
            obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
            obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
            obj.SIQ_Created_Date__c=pos.CreatedDate;
            obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
            obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
            obj.Unique_Id__c=orgObj.Name+'_'+pos.id;
            orgPosList.add(obj);
            records++;
            System.debug('**records'+records);
            org=orgmap.get(org);
         }
        
         
         
      }
      if(orgPosList.size()>0){
      upsert orgPosList Unique_Id__c;
      }
       Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
       system.debug('schedulerObj++++before'+sJob);
       sJob.Job_Status__c='Successful';
       sJob.No_Of_Records_Processed__c=records;
       update sJob;
   }
  }