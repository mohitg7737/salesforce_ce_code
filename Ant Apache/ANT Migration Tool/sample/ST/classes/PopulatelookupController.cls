public with sharing class PopulatelookupController {

public Boolean pollerbool {get;set;}
public Boolean batchStatusBool {get;set;}
public String batchStatus {get;set;}
public Integer batchsize {get;set;}
public Id BC {get;set;}

public List<SelectOption> TeamLst {get;set;}
public String selectedTeam {get;set;}
public String selectedTeamDel {get;set;}
public List<SelectOption> TeamInsLst {get;set;}
public String selectedTeamIns {get;set;}

public List<SelectOption> JuncList {get;set;}
public String selectedJuncObj {get;set;}
public Boolean showobj {get;set;}
public List<SelectOption> ObjList {get;set;}
public String selectedObj {get;set;}
public boolean rerunbool{get;set;}
public string selectedObject{get;set;}
public list<SelectOption> objectLst{get;set;}

public Class BatchJob{
public AsyncApexJob job {get; set;}
public Integer percentComplete {get; set;}
 }
 
public List<BatchJob> batchJobs {get;set;} 


public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public String[] csvFileLines{get;set;}
public List <temp_Acc_Terr__c> acclist {get;set;}


public PopulatelookupController(){
  batchsize = 500;
  selectedTeam='--None--';

  TeamLst = new list<SelectOption>();
  TeamInsLst = new List<SelectOption>();
      
  list<AxtriaSalesIQTM__Team__c> Team = [select id, name from AxtriaSalesIQTM__Team__c];
    TeamLst.add(new SelectOption('', '--None--'));
  for(AxtriaSalesIQTM__Team__c objTeam: Team){
    TeamLst.add(new SelectOption(objTeam.id,objTeam.name));
  }
  showobj=false;  
  batchStatusBool =false;
  
  JuncList = new List<SelectOption>();
  
  JuncList.add(new SelectOption('','--None--'));
  
  
}

public void teaminsfetch(){
    System.debug('selectedTeam11=='+selectedTeam);
     list<AxtriaSalesIQTM__Team_Instance__c> teamInstances = [select id, name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c=:selectedTeam];
      System.debug('selectedTeam=='+selectedTeam);
     System.debug('teamInstances =='+teamInstances );
     TeamInsLst.clear();
     TeamInsLst.add(new SelectOption('', '--None--'));
      for(AxtriaSalesIQTM__Team_Instance__c objTeamInstance: teamInstances){
        TeamInsLst.add(new SelectOption(objTeamInstance.id,objTeamInstance.name));
      }
      JuncList.clear();
      list<AxtriaSalesIQTM__Team__c> selectedTeamFinal=[select name from AxtriaSalesIQTM__Team__c where id=:selectedTeam];
   if(selectedTeam=='--None--')
   {
   JuncList.add(new SelectOption('','--None--'));
   }
   
  else
  {
    JuncList.add(new SelectOption('','--None--'));
    JuncList.add(new SelectOption('Acc_Terr','Account to Position Mapping'));
    JuncList.add(new SelectOption('Zip_Terr','Geography to Position Mapping'));
    JuncList.add(new SelectOption('Position_Employee', 'Employee To Position'));
    JuncList.add(new SelectOption('Call_Plan','Call Plan'));
    JuncList.add(new SelectOption('Survey','Survey'));
    //JuncList.add(new SelectOption('Position_Geography','AxtriaSalesIQTM__Position_Geography'));
    //JuncList.add(new SelectOption('Position_Account','AxtriaSalesIQTM__Position_Account'));

  }
}
 public void  dataDeleteCntroller(){
  rerunbool=false;
  batchStatusBool =false;
  TeamLst=new List<SelectOption>();
  TeamLst.add(new SelectOption('','--None--'));
    for(AxtriaSalesIQTM__Team__c teamname:[select id,Name from AxtriaSalesIQTM__Team__c])
    {
        TeamLst.add(new SelectOption(teamname.id,teamname.Name));
    }
    
    
    objectLst = new List<SelectOption>();
    
  
      objectLst.add(new SelectOption('','--None--'));
      
      objectLst.add(new SelectOption('temp_Zip_Terr__c','temp_Zip_Terr'));
      objectLst.add(new SelectOption('temp_Acc_Terr__c','temp_Acc_Terr'));
      objectLst.add(new SelectOption('Call_Plan__c','Call_plan'));
      
     
      
     
}
  
 public void deleteAll(){
  BC = Database.executeBatch(new batchdelete3(selectedTeamDel,selectedObject),batchsize);     //batch to update parent_position,team lookups and RGB
  batchStatusBool  = true;
  checkBatchStatus();
}
 
public void checkBatchStatus(){
     string batchclassID;
     Datetime batchStarttime;
     String jname = ApexPages.CurrentPage().getParameters().get('Junc');
     batchJobs = new List<BatchJob>();
     
     list<AsyncApexJob> ajob = new list<AsyncApexJob>();
     if(BC!=null){
                
    ajob = [select TotalJobItems, Status,ExtendedStatus, NumberOfErrors, JobItemsProcessed, CreatedDate, ApexClassId, ApexClass.Name    From AsyncApexJob  where id =: BC  ]; 
       Double itemsProcessed = ajob[0].JobItemsProcessed;
        Double totalItems = ajob[0].TotalJobItems;
        batchStatus=ajob[0].Status;
        batchStarttime=ajob[0].CreatedDate;
        batchclassID =ajob[0].ApexClassId;
        BatchJob j = new BatchJob();
        j.job = ajob[0];
        if (totalItems == 0) {
        j.percentComplete = 0;
        } else {
        j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
        }
        batchjobs.add(j);
    
 }
     if(batchStatus == 'Completed'){
        
         pollerbool=false;
          
         Integer jobs = [Select count() From AsyncApexJob Where JobType =: 'BatchApexWorker' and  Status =: 'Completed' and ApexClassId = : batchclassID and CreatedDate >=: batchStarttime];   

         if(ajob[0].NumberOfErrors != 0){
             
             ajob = [SELECT id, ExtendedStatus, NumberOfErrors FROM AsyncApexJob WHERE id = :BC];
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Some error occured while populating '+jname+' : '+ ajob[0].ExtendedStatus));
         } 
         else if ((ajob[0].NumberOfErrors == 0) && (jobs==0)){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,' No record inserted/updated...!!')); 
           }
         else{
            
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,jname+' populated successfully'));
         }
             
         } 
         else if(batchStatus == 'Failed')   {
         pollerbool=false;   
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in populating lookup fields in '+jname+' : '+ ajob[0].ExtendedStatus));
         }
        else{
        pollerbool=true; 
        }
     }
 

public void populateJuncObj(){
   batchsize = 500;
    
    try{    
            batchStatusBool=true;
             if (selectedJuncObj == 'Zip_Terr'){
               BC = Database.executeBatch(new  update_ZipTerr(selectedTeam,selectedTeamIns),batchsize);     //batch to update geography and posiition lookups
               checkBatchStatus();
            }
            else if (selectedJuncObj == 'Acc_Terr'){
               BC = Database.executeBatch(new  update_AccTerr(selectedTeam,selectedTeamIns),batchsize);     //batch to update position and account lookups
               checkBatchStatus();
            }
            else if (selectedJuncObj == 'Position_Account' ){ 
                 BC = Database.executeBatch(new  update_position_Acc(selectedTeam,selectedTeamIns),batchsize);     //batch to update position_account
                 checkBatchStatus();
            }
            else if (selectedJuncObj == 'Position_Geography' ){
               BC = Database.executeBatch(new  update_PositionGeography(selectedTeam,selectedTeamIns),batchsize);     //batch to update position_geography
               checkBatchStatus();
            }
            else if(selectedJuncObj == 'Position_Employee' ){
               BC = Database.executeBatch(new  BatchPositionEmpDirectLoad(selectedTeam,selectedTeamIns),batchsize);     //batch to insert position_employee
               checkBatchStatus();
             }
            else if(selectedJuncObj == 'Call_Plan' ){
                String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamIns].name;
               BC = Database.executeBatch(new  PACP_lookups_Utility(TIname),batchsize);     //batch to insert Call plan Data
               checkBatchStatus();
             } 
             //Survey
             else if(selectedJuncObj == 'Survey' ){
                 String TIname = [select name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamIns].name;
               BC = Database.executeBatch(new  Survey_Data_Transformation_Utility(TIname),batchsize);     //batch to insert Survey Data
               checkBatchStatus();
             } 


            else if (selectedJuncObj == '' )   {
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select an object to populate from picklist'));
            } 
           }
           catch(DMLException e){
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'There is something wrong with inputs.Please Check...'+e.getMessage()));
           }
}
public void importCSVFile(){
       try{
          list<temp_Acc_Terr__c> acclist = new list<temp_Acc_Terr__c>();
           csvAsString = csvFileBody.toString();
           List<STRING> csvFileLines = new List<STRING>();
           csvFileLines = csvAsString.split('\n'); 

            
           for(Integer i=1;i<csvFileLines.size();i++){
                             
               temp_Acc_Terr__c accObj = new temp_Acc_Terr__c() ;
               string[] csvRecordData = new string[]{};
               csvRecordData = csvFileLines[i].split(',');
                             
               accObj.TeamName__c = csvRecordData[0];
                
               accObj.AccountNumber__c = csvRecordData[1];
             
               accObj.Territory_ID__c = csvRecordData[2];  
               
                                                                     
               acclist.add(accObj);   
             
           }
        insert acclist;


        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
            ApexPages.addMessage(errorMessage);
        }  
       
  }
}