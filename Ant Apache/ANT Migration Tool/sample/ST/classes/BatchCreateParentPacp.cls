global class BatchCreateParentPacp implements Database.Batchable<sObject>,Database.stateful {
    public String query;
    public string selectedteaminstance {get;set;}
    public set<string>uniquevalues{get;set;}
    public string ruleidis {get;set;}

    global BatchCreateParentPacp(String teaminstance) {
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }

    global BatchCreateParentPacp(String teaminstance,string ruleID) {
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        ruleidis = '';
        ruleidis = ruleID;
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {

           
        list<Parent_PACP__c>insertparentlist = new list<Parent_PACP__c>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
            string key=pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
            if(!uniquevalues.contains(key)){
                Parent_PACP__c parentpacp = new Parent_PACP__c();
                parentpacp.Account__c = pacp.AxtriaSalesIQTM__Account__c;
                parentpacp.Position__c = pacp.AxtriaSalesIQTM__Position__c;
                parentpacp.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
                parentpacp.External_Id__c = pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                insertparentlist.add(parentpacp);
                uniquevalues.add(key);
            }
        }
        
        if(insertparentlist !=null && insertparentlist.size()>0){
            system.debug('=====insertparentlist======='+insertparentlist);
            upsert insertparentlist External_Id__c;
        }
    }

    global void finish(Database.BatchableContext BC) {
        database.executeBatch(new BatchUpdateParentPacpOnPacp(selectedteaminstance,ruleidis), 2000);
    }
}