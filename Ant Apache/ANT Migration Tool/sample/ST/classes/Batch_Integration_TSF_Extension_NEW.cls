global class Batch_Integration_TSF_Extension_NEW implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public List<String> allProcessedIds;
    public List<String> allTeamInstances;
    public Map<String,String> pacpToSegment10Map;

    public List<SIQ_TSF_vod_O__c> allStagingTSF;

    global Batch_Integration_TSF_Extension_NEW(List<String> allTeamInstances) {
        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();
        pacpToSegment10Map = new Map<String,String>();
        
        /*for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : [select id,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AZ_VeevaID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Segment10__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances ]){
            pacpToSegment10Map.put(pacp.AxtriaSalesIQTM__Team_Instance__c+pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c ,pacp.AxtriaSalesIQTM__Segment10__c);
        }*/
        
        this.query = 'select id, Status__c,SIQ_My_Target_vod__c,SIQ_Territory_vod__c,SIQ_Account_vod__c,Team_Instance__c from SIQ_TSF_vod_O__c where Team_Instance__c in :allTeamInstances and Status__c != \'Updated\' ';//and SIQ_External_ID_AZ__c NOT IN :allProcessedIds

       
        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_TSF_vod_O__c> scope) {
        
        Set<string> accountNumberSet = new Set<string>();
        Set<string> posCodeSet = new Set<string>();
        //Map<string,id> accountNumberIdMap = new Map<string,id>();
        //Map<string,id> brandIdMap = new Map<string,id>();

        for(SIQ_TSF_vod_O__c scsp : scope){      //Positions Set
            if(!string.IsBlank(scsp.SIQ_Territory_vod__c)){
                posCodeSet.add(scsp.SIQ_Territory_vod__c);
            }
            if(!string.IsBlank(scsp.SIQ_Account_vod__c)){            //Accounts Set
                accountNumberSet.add(scsp.SIQ_Account_vod__c);
            }
                   
        }
        
        pacpToSegment10Map = new Map<String,String>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : [select id,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AZ_VeevaID__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Segment10__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Account__r.AccountNumber in :accountNumberSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :posCodeSet and AxtriaSalesIQTM__lastApprovedTarget__c = false]){
            pacpToSegment10Map.put(pacp.AxtriaSalesIQTM__Team_Instance__c+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c ,pacp.AxtriaSalesIQTM__Segment10__c);
        }
         
         for(SIQ_TSF_vod_O__c tsf : scope)
         {
            tsf.SIQ_My_Target_vod__c = false;
            
            if(pacpToSegment10Map.containsKey(tsf.Team_Instance__c+tsf.SIQ_Account_vod__c+tsf.SIQ_Territory_vod__c)){
                if(pacpToSegment10Map.get(tsf.Team_Instance__c+tsf.SIQ_Account_vod__c+tsf.SIQ_Territory_vod__c) == 'Inactive'){
                    tsf.Status__c = 'Deleted';
                }
                else if(pacpToSegment10Map.get(tsf.Team_Instance__c+tsf.SIQ_Account_vod__c+tsf.SIQ_Territory_vod__c) == 'Loser Account'){
                    tsf.Status__c = 'Deleted';
                }
                else{ //if(pacpToSegment10Map.get(tsf.Team_Instance__c+tsf.SIQ_Account_vod__c+tsf.SIQ_Territory_vod__c) == '')
                    tsf.Status__c = 'Updated';
                }
            }
            
         }

         update scope;

    }

    global void finish(Database.BatchableContext BC) {

    }
}