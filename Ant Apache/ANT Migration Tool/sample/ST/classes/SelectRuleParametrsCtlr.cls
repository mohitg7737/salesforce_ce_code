global with sharing class SelectRuleParametrsCtlr extends BusinessRule implements IBusinessRule{
    public list<Parameter__c> parameterList{get;set;}
    public String selectedParameters{get;set;}
    public SelectRuleParametrsCtlr(){
        init();
        selectedParameters = '';
        getParameterList();
    }

    public void getParameterList()
    {
        parameterList = new list<Parameter__c>();

        List<Brand_Team_Instance__c> bTeamInstance = [select id from Brand_Team_Instance__c where Brand__c = :ruleObject.Brand_Lookup__c and Team_Instance__c = :ruleObject.Team_Instance__c];

        list<Rule_Parameter__c> destParams = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id];

        Set<String> destParamsSet = new Set<String>();
        for(Rule_Parameter__c rp : destParams){
            destParamsSet.add(rp.Parameter__c);
        }

        if(bTeamInstance.size() > 0)
        {
            for(Parameter__c param: [Select ID, Name FROM Parameter__c WHERE Id NOT IN :destParamsSet AND isActive__c=true AND Team_Instance__c =:ruleObject.Team_Instance__c and Brand_Team_Instance__c = :bTeamInstance[0].ID ORDER BY CreatedDate DESC])
            {
                parameterList.add(param);
            }
        }
        /*else
        {
            for(Parameter__c param: [Select ID, Name FROM Parameter__c WHERE Id NOT IN :destParamsSet AND isActive__c=true AND Team_Instance__c =:ruleObject.Team_Instance__c ORDER BY CreatedDate DESC])
            {
                parameterList.add(param);   
            }
        }*/
        system.debug('@@parameterList:'+parameterList);
    }

    public list<Rule_Parameter__c> getDestParameterList(){
        return [SELECT Id, Parameter__c, Parameter__r.Name FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
    }

    @RemoteAction
    global static Parameter__c addNewParameter(String newParameter){
        if(String.isNotBlank(newParameter)){
            Parameter__c parameter = new Parameter__c();
            parameter.Name = newParameter;
            insert parameter;
            return parameter;
        }
        return null;
    }

    public void saveFinal(){
        list<Rule_Parameter__c> avalRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        set<String> paramSet = new set<String>();
        for(Rule_Parameter__c rp: avalRuleParam){
            paramSet.add(rp.Parameter__c);
        }
        list<Rule_Parameter__c> ruleParameters = new list<Rule_Parameter__c>();
        list<String> parameters = (list<String>)JSON.deserialize(selectedParameters, list<String>.class);
        system.debug('--selectedParameters-- ' + parameters);
        for(String param: parameters){
            if(String.isNotBlank(param) && !paramSet.contains(param)){
                Rule_Parameter__c rp = new Rule_Parameter__c();
                rp.Measure_Master__c = ruleObject.Id;
                rp.Parameter__c = param;
                ruleParameters.add(rp);
            }
        }
        insert ruleParameters;
    }

    public void save(){
        saveFinal();
        list<Rule_Parameter__c> allRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        if(allRuleParam != null && allRuleParam.size() > 0){
            updateRule('Profiling Parameter', 'Basic Information');
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
        }
    }

    public PageReference saveAndNext(){
        saveFinal();
        list<Rule_Parameter__c> allRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        if(allRuleParam != null && allRuleParam.size() > 0){
            updateRule('Compute Values', 'Profiling Parameter');
            return nextPage('ComputeValues');
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
        }
        return null;
    }
}