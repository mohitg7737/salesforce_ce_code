global class Batch_Populate_User_Position implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public Set<String> uapExists;
    
    global Batch_Populate_User_Position() {
        
        this.query = 'select id, AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c , AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c = \'Active\' OR AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\'';
        
        
        uapExists = new Set<String>();
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Is_Active__c = true];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
            uapExists.add(concat);           
        }
        system.debug('+++++++++++++ uap Existing' + uapExists);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {
        
        List<String> allUsers = new List<String>();
        
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            allUsers.add(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c);   
        }
        
        Map<String, ID> fedIdToUserId = new Map<String, ID>();
        
        for(User users : [select FederationIdentifier, id from User where FederationIdentifier in :allUsers])
        {
            if(users.FederationIdentifier != null)
            {
                fedIdToUserId.put(users.FederationIdentifier, users.id);
            }
        }
        
        system.debug('+++++++++++++ fedIdToUserId' + fedIdToUserId);
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPInsert = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
            
            uap.AxtriaSalesIQTM__Position__c = posEmp.AxtriaSalesIQTM__Position__c;
            uap.AxtriaSalesIQTM__Team_Instance__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c;
            uap.AxtriaARSnT__isCallPlanEnabled__c = true;
            uap.AxtriaSalesIQTM__Map_Access_Position__c = posEmp.AxtriaSalesIQTM__Position__c;
            
            if(fedIdToUserId.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c))
            {
                uap.AxtriaSalesIQTM__User__c = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c);
                
                String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
                
                if(!uapExists.contains(concat))
                {
                    uapExists.add(concat);
                    allUAPInsert.add(uap);
                }
            }
        }
        
        system.debug('+++++++++++ Hey'+ allUAPInsert);
        insert allUAPInsert;
        
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}