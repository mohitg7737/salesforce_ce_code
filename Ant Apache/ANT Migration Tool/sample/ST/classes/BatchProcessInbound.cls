/**********************************************************************************************
@author     : Ankit Manendra
@date       : 1 December 2017
@description: This Batch Class is used for creating events w.r.t Delta calculations. 
@Client     : Insmed 
Revison(s)  : By Ritu on 5th May 2018
**********************************************************************************************/

global class BatchProcessInbound implements Database.Batchable<sObject>, Database.Stateful,schedulable {   
    global string query;
    public String batchID;
    DateTime firstCreatedInTheBatch;
    DateTime lastCreatedInTheBatch;
    set<string> emplList= new set<string>();
    list<AxtriaSalesIQTM__Employee__c> prevEmployeeList;
    map<string,AxtriaSalesIQTM__Employee__c> prevEmployeeMap = new map<string,AxtriaSalesIQTM__Employee__c>();
    Date todayDate;
    Map<String,set<CurrentPersonFeed__c>> emListEmpMap;
    Map<String,List<AxtriaSalesIQTM__Employee__c>> empMapForTrnsfrToHO;
    list<AxtriaSalesIQTM__employee__c> lstInsertEmployee;
     Map<string,list<AxtriaSalesIQTM__Position_Employee__c>> positionEmpMap;
     list<AxtriaSalesIQTM__Position_Employee__c> lstPositionEmployee;
     list<AxtriaSalesIQTM__Position_Employee__c> lstPEToBeDeleted;
     Map<string,AxtriaSalesIQTM__Position__c> positionRecMap;
     map<string,Integer> mapEvent2Priority = new map<string,Integer>();
     map<string,string> mapEmpID2Event = new map<string,string>();
     map<string,AxtriaSalesIQTM__Employee__c> newEmployeeMap = new map<string,AxtriaSalesIQTM__Employee__c>();
     global map<string,Date> mapEmployee2EventTypeDate = new map<string,Date>();
     global map<string,list<string>> mapId2FieldEmpDetails = new map<string,list<string>>();
     global set<string> fieldEmpDepartmentSet = new set<string>();
     map<string,Date> mapAssId2TermDate = new map<string,Date>();
     
    global BatchProcessInbound(String Id){
        batchID=Id;
        /*for(FieldEmployee__c fieldEmp : Database.Query('select id,Department__c,Job_Title__c,Operator__c from fieldemployee__c')){
            list<string> fieldEmpSet = new list<string>();
            fieldEmpSet.add(fieldEmp.Department__c);
            fieldEmpSet.add(fieldEmp.Operator__c); 
            fieldEmpSet.add(fieldEmp.Job_Title__c);
            mapId2FieldEmpDetails.put((string)fieldEmp.id,fieldEmpSet);
            fieldEmpDepartmentSet.add(fieldEmp.Department__c);
            
        }*/
        
        system.debug('===mapId2FieldEmpDetails===='+mapId2FieldEmpDetails);
        
        
        query = 'select id,AddressCity__c, AddressCountry__c,AddressLine1__c,AddressLine2__c,'+
                'AddressPostalCode__c,AddressStateCode__c,AssignmentStatusValue__c,Department__c,'+
                'Department_Code__c,Email__c,Employee_ID__c,PRID__c,Last_Name__c,First_Name__c,'+
                'JobChangeReason__c,JobCode__c,JobCodeName__c,LegalAddressCityName__c,LegalAddressCountryCode__c,LegalAddressLine1__c,'+
                'LegalAddressLine2__c,LegalAddressPostalCode__c,NickName__c,OriginalHireDate__c,Rehire_Date__c,ReportingToWorkerName__c,'+
                'ReportsToAssociateOID__c,StateTerritory__c,TerminationDate__c,Worker_Category__c,WorkPhone__c,HR_Status__c '+
                ' from CurrentPersonFeed__c where isRejected__c=false ';
        
        system.debug('query'+query);        
    }
     
    public void InitialiseVariables(){
        prevEmployeeList = new list<AxtriaSalesIQTM__Employee__c>(); 
        prevEmployeeList = [select id,
                            AxtriaSalesIQTM__HR_Termination_Date__c,Re_hire_Date__c,Employee_Status__c,Department__c,JobCodeName__c,
                            AxtriaSalesIQTM__Original_Hire_Date__c,AxtriaSalesIQTM__Employee_ID__c,Transferred_to_HO__c  
                            from AxtriaSalesIQTM__Employee__c order by AxtriaSalesIQTM__Employee_ID__c desc /* where Transferred_to_HO__c = false */];
        system.debug('prevEmployeeList.size()'+prevEmployeeList.size());   
        system.debug('prevEmployeeList'+prevEmployeeList);                  
        emListEmpMap        = new Map<String,set<CurrentPersonFeed__c>>();
        lstInsertEmployee = new list<AxtriaSalesIQTM__employee__c>();
        positionEmpMap = new Map<string,list<AxtriaSalesIQTM__Position_Employee__c>>();
        lstPositionEmployee = new list<AxtriaSalesIQTM__Position_Employee__c>();
        lstPEToBeDeleted = new list<AxtriaSalesIQTM__Position_Employee__c>();
         positionRecMap  = new Map<string,AxtriaSalesIQTM__Position__c>(); 
         mapEvent2Priority.put(system.label.Employee_Newhire,1);
         mapEvent2Priority.put(system.label.Terminate_Employee,2);
         mapEvent2Priority.put(system.label.transfer_to_field,3);
         //mapEvent2Priority.put(system.label.Leave_of_Absence,4);
         //mapEvent2Priority.put(system.label.Promote_Employee,5);
         mapEvent2Priority.put(system.label.Transfer_out_of_Sales_team,6);
    }
    
    public void CreateMaps(List<CurrentPersonFeed__c> scopeCurrentEmpList){
        map<string,CurrentPersonFeed__c> currentEmployeeMap = new map<string,CurrentPersonFeed__c>();
        prevEmployeeMap = new map<string,AxtriaSalesIQTM__Employee__c>();
        system.debug('prevEmployeeList.size()'+prevEmployeeList.size());
        for(AxtriaSalesIQTM__Employee__c Emp : prevEmployeeList){
            prevEmployeeMap.put(Emp.AxtriaSalesIQTM__Employee_ID__c,Emp);            
        }
        system.debug('prevEmployeeMap'+prevEmployeeMap);
        
        for(AxtriaSalesIQTM__Position_Employee__c PosEmp : [SELECT id,AxtriaSalesIQTM__Reason_Code__c,AxtriaSalesIQTM__Effective_Start_Date__c,
                                                               AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                                               AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Employee__r.Department__c,
                                                               AxtriaSalesIQTM__Assignment_Type__c, AxtriaSalesIQTM__Employee__r.Transferred_to_HO__c,
                                                               AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Employee__r.Employee_Status__c FROM 
                                                               AxtriaSalesIQTM__Position_Employee__c WHERE     
                                                               AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c 
                                                               in:prevEmployeeMap.keySet()]){
            if(!positionEmpMap.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c)){
                positionEmpMap.put(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, new list<AxtriaSalesIQTM__Position_Employee__c>());
            }
            positionEmpMap.get(posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c).add(posEmp);
        }
         for(AxtriaSalesIQTM__Position__c pos:[Select id, AxtriaSalesIQTM__Employee__c, 
                                              AxtriaSalesIQTM__employee__r.AxtriaSalesIQTM__Employee_ID__c,
                                              AxtriaSalesIQTM__Client_Territory_Code__c
                                              FROM AxtriaSalesIQTM__Position__c //Effective_Start_Date__c, Effective_End_Date__c
                                              WHERE
                                              AxtriaSalesIQTM__employee__r.AxtriaSalesIQTM__Employee_ID__c in:prevEmployeeMap.Keyset()])                      
            positionRecMap.put(pos.AxtriaSalesIQTM__employee__r.AxtriaSalesIQTM__Employee_ID__c,pos);
    }
    
    public void createNewEmployees(){
        set<CurrentPersonFeed__c> allRecords = new set<CurrentPersonFeed__c>();
        if(emListEmpMap.containsKey(system.label.Employee_Newhire) || emListEmpMap.containsKey(system.label.transfer_to_field)){
            if(emListEmpMap.containsKey(system.label.Employee_Newhire)){
                system.debug('====inside new hire========');
                allRecords = emListEmpMap.get(system.label.Employee_Newhire);
                for(CurrentPersonFeed__c ES : allRecords){
                    AxtriaSalesIQTM__employee__c insertEmployee = new AxtriaSalesIQTM__employee__c();
                    if(prevEmployeeMap.containsKey(ES.PRID__c)){
                        insertEmployee.id =  prevEmployeeMap.get(ES.PRID__c).id;
                        insertEmployee.Transferred_to_HO__c = false;
                        insertEmployee.Transferred_to_HO_Date__c = null;
                    }    
                    insertEmployee = CopyEmployeeFields.copyAllFields(ES,insertEmployee);
                    insertEmployee.AxtriaSalesIQTM__Field_Status__c = 'Unassigned';
                    insertEmployee.name = ES.First_Name__c +' '+ ES.Last_Name__c;
                    insertEmployee.AxtriaSalesIQTM__Last_Name__c=ES.Last_Name__c;
                    insertEmployee.AxtriaSalesIQTM__FirstName__c=ES.First_Name__c;
                    insertEmployee.AxtriaSalesIQTM__Employee_ID__c=ES.PRID__c;
                    insertEmployee.AxtriaSalesIQTM__Original_Hire_Date__c=ES.OriginalHireDate__c;
                    insertEmployee.Employee_Id_AZ__c=ES.Employee_ID__c;
                    insertEmployee.AxtriaSalesIQTM__HR_Status__c = ES.HR_Status__c;
                    insertEmployee.Employee_Status__c = ES.HR_Status__c;
                    
                    lstInsertEmployee.add(insertemployee);
                }
            }
            /*if(emListEmpMap.containsKey(system.label.transfer_to_field)){
                allRecords = emListEmpMap.get(system.label.transfer_to_field);
                for(CurrentPersonFeed__c ES : allRecords){
                    AxtriaSalesIQTM__employee__c insertEmployee = new AxtriaSalesIQTM__employee__c();
                    if(prevEmployeeMap.containsKey(ES.PRID__c)){
                        insertEmployee.id =  prevEmployeeMap.get(ES.PRID__c).id;
                        insertEmployee.Transferred_to_HO__c = false;
                        insertEmployee.Transferred_to_HO_Date__c = null;
                    }
                    insertEmployee = CopyEmployeeFields.copyAllFields(ES,insertEmployee); 
                    insertEmployee.AxtriaSalesIQTM__Field_Status__c = 'Unassigned';
                    insertEmployee.name = ES.First_Name__c +' '+ ES.Last_Name__c;
                    insertEmployee.AxtriaSalesIQTM__Last_Name__c=ES.Last_Name__c;
                    insertEmployee.AxtriaSalesIQTM__FirstName__c=ES.First_Name__c;
                    lstInsertEmployee.add(insertemployee);
                }
            }*/
            system.debug('===lstInsertEmployee==='+lstInsertEmployee);
            If(lstInsertEmployee.size() > 0){ 
                upsert lstInsertEmployee;
                list<AxtriaSalesIQTM__Employee_Status__c> toBeInsertedEmpStatuslist = new list<AxtriaSalesIQTM__Employee_Status__c>();
                for(AxtriaSalesIQTM__employee__c emp : [select id,AxtriaSalesIQTM__Original_Hire_Date__c,Employee_Status__c,AxtriaSalesIQTM__Employee_ID__c,Transferred_to_HO__c,Department__c,JobCodeName__c from AxtriaSalesIQTM__employee__c where id in: lstInsertEmployee]){
                    prevEmployeeMap.put(emp.AxtriaSalesIQTM__Employee_ID__c, emp);
                    // Add new record in employee status for each newly created employee
                    AxtriaSalesIQTM__Employee_Status__c newEmpStatus = new AxtriaSalesIQTM__Employee_Status__c();
                    newEmpStatus.AxtriaSalesIQTM__Employee__c = emp.id;
                    newEmpStatus.AxtriaSalesIQTM__Employee_Status__c = emp.Employee_Status__c;
                    newEmpStatus.AxtriaSalesIQTM__Effective_Start_Date__c = emp.AxtriaSalesIQTM__Original_Hire_Date__c;
                    newEmpStatus.AxtriaSalesIQTM__Effective_End_Date__c = Date.newinstance(4000,12,31); 
                    toBeInsertedEmpStatuslist.add(newEmpStatus);
                }
                //insert toBeInsertedEmpStatuslist;
                upsert toBeInsertedEmpStatuslist;
            }
        }
        for(AxtriaSalesIQTM__employee__c emp : lstInsertEmployee){
            if(!prevEmployeeMap.containsKey(emp.AxtriaSalesIQTM__Employee_ID__c)){
                newEmployeeMap.put(emp.AxtriaSalesIQTM__Employee_ID__c,emp);
            }
        }
    } 
    
    public void CreateEmployeeFeed(){
        system.debug('emListEmpMap.keyset().size()'+emListEmpMap.keyset().size());
        list<CR_Employee_Feed__c> crFeedList = new list<CR_Employee_Feed__c>();
        //set<string> fieldEmpSet = new set<string>((system.label.Field_Employee).split(';'));
        //fieldEmpSet= (system.label.Field_Employee).split(';');
        system.debug('emListEmpMap+++++++'+emListEmpMap);
        for(String key : emListEmpMap.keyset()){
            set<CurrentPersonFeed__c> allRecords = emListEmpMap.get(key);
            for(CurrentPersonFeed__c Est : allRecords){
                AxtriaSalesIQTM__employee__c emp = new AxtriaSalesIQTM__employee__c();
                CR_Employee_Feed__c crEmp = new CR_Employee_Feed__c();
                crEmp.Request_Date1__c = system.today();
                crEmp.Rep_Name__c = Est.First_Name__c + ' ' +  Est.Last_Name__c;
                crEmp.Event_Name__c =   key;   
                crEmp.Status__c = 'Pending Action';
                if(prevEmployeeMap.containsKey(Est.PRID__c)){
                    crEmp.Employee__c = prevEmployeeMap.get(Est.PRID__c).id;
                }
                if(positionRecMap.containsKey(Est.PRID__c) && positionRecMap.get(Est.PRID__c) != null){
                    if(key!=system.label.Transfer_out_of_Sales_team && key != system.label.Terminate_Employee){
                        crEmp.position__C = positionRecMap.get(Est.PRID__c).id;
                    }
                }
                if(key == system.label.Employee_Newhire){
                    if(crEmp.Employee__c==null){
                        crEmp.Employee__c=newEmployeeMap.get(Est.PRID__c).id;
                    }
                }
                else if(key == system.label.transfer_to_field){
                }
                else if(key == system.label.Promote_Employee){
                    
                }
                else if(key == system.label.Demote_Employee){
                    
                }
                else if(key == system.label.transfer_employee){
                    
                }
                else if(key==system.label.Transfer_out_of_Sales_team){
                    crEmp.Transferred_to_HO__c = true;  
                    crEmp.Termination_Date__c  = Est.TerminationDate__c;
                    //For Past Dated Terminations End assignments
                    if(Est.TerminationDate__c <= system.today()){
                            if(positionEmpMap.containsKey(Est.PRID__c)){
                                for(AxtriaSalesIQTM__Position_Employee__c PE : positionEmpMap.get(Est.PRID__c)){
                                    if(PE.AxtriaSalesIQTM__Effective_End_Date__c > Est.TerminationDate__c && PE.AxtriaSalesIQTM__Effective_Start_Date__c < Est.TerminationDate__c){
                                        PE.AxtriaSalesIQTM__Effective_End_Date__c = Est.TerminationDate__c;
                                        PE.AxtriaSalesIQTM__Reason_Code__c = system.label.Transfer_out_of_Sales_team;   //modified by Akanksha on 1/17/18
                                        lstPositionEmployee.add(PE);
                                    }
                                    if(PE.AxtriaSalesIQTM__Effective_Start_Date__c > Est.TerminationDate__c){
                                        lstPEToBeDeleted.add(PE);
                                        mapAssId2TermDate.put(Est.PRID__c,Est.TerminationDate__c);    
                                    }
                                }
                            } 
                            crEmp.Status__c = 'Completed Action';
                    } 
                }
                else if(key == system.label.Terminate_Employee){
                    crEmp.Termination_Date__c  = Est.TerminationDate__c;
                    //For Past Dated Terminations End assignments
                    if(Est.TerminationDate__c <= system.today()){
                            if(positionEmpMap.containsKey(Est.PRID__c)){
                                for(AxtriaSalesIQTM__Position_Employee__c PE : positionEmpMap.get(Est.PRID__c)){
                                    if(PE.AxtriaSalesIQTM__Effective_End_Date__c > Est.TerminationDate__c && PE.AxtriaSalesIQTM__Effective_Start_Date__c <= Est.TerminationDate__c){
                                        PE.AxtriaSalesIQTM__Effective_End_Date__c = Est.TerminationDate__c;
                                        PE.AxtriaSalesIQTM__Reason_Code__c = system.label.Terminate_Employee;   //modified by Akanksha on 1/17/18
                                        lstPositionEmployee.add(PE);
                                    }
                                    if(PE.AxtriaSalesIQTM__Effective_Start_Date__c > Est.TerminationDate__c){
                                        lstPEToBeDeleted.add(PE);
                                        mapAssId2TermDate.put(Est.PRID__c,Est.TerminationDate__c);    
                                    }
                                }
                            } 
                            crEmp.Status__c = 'Completed Action';
                    }
                }
                else if(key == system.label.Leave_of_Absence){
                    //crEmp.Leave_Start_Date__c   = Est.LOA_Start_Date__c; 
                    //crEmp.Leave_End_Date__c     = Est.LOA_End_Date__c;
                }
                if(mapEmpID2Event.containsKey(Est.PRID__c) && mapEmpID2Event.get(Est.PRID__c)==crEmp.Event_Name__c){
                    system.debug('inside cr emp feed');
                    crFeedList.add(crEmp);
                    if(crEmp.Event_Name__c == system.label.Terminate_Employee
                        || crEmp.Event_Name__c == system.label.Transfer_out_of_Sales_team){
                            system.debug(crEmp.Event_Name__c);
                            if(Est.TerminationDate__c!=null){
                                mapEmployee2EventTypeDate.put(Est.PRID__c,(Est.TerminationDate__c).addDays(1));
                            }
                    }
                    else if(crEmp.Event_Name__c == system.label.transfer_to_field){
                        if(Est.Hire_Date__c!=null){
                            mapEmployee2EventTypeDate.put(Est.PRID__c,Est.Hire_Date__c);
                        }
                        system.debug(crEmp.Event_Name__c);
                    }
                    else if(crEmp.Event_Name__c == system.label.Employee_Newhire){
                        if(Est.OriginalHireDate__c!=null){
                            mapEmployee2EventTypeDate.put(Est.PRID__c,Est.OriginalHireDate__c);
                        }
                        system.debug(crEmp.Event_Name__c);
                    }
                }
                
            }
        }
        system.debug('mapEmployee2EventTypeDate'+mapEmployee2EventTypeDate);
        insert crFeedList;
        
    }
    
    public void mailSender(){
        list<CR_Employee_Feed__c> crFeed = new list<CR_Employee_Feed__c>();
        map<string,string> eventNameCSVFieldMap = new map<string,string>();
        eventNameCSVFieldMap.put(system.label.Employee_Newhire,'NewHire_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.transfer_to_field,'Transfer_to_Sales_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.Promote_Employee,'Promotion_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.transfer_employee,'Transfer_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.Terminate_Employee,'Termination_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.Leave_of_Absence,'Leave_of_Absence_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.Transfer_out_of_Sales_team,'Transfer_Out_of_Sales_Mail_CSV__c');
        eventNameCSVFieldMap.put(system.label.Employee_Rehire,'Rehire_Mail_CSV__c');
        
        crFeed  = [select id,Event_Name__c from CR_Employee_Feed__c where createdDate = TODAY];
        if(crFeed.size()>0){
            map<string,list<CR_Employee_Feed__c>> eventNameCRListMap = new map<string,list<CR_Employee_Feed__c>>(); 
            for(CR_Employee_Feed__c crf : crFeed){
                if(!eventNameCRListMap.containsKey(crf.Event_Name__c)){
                    eventNameCRListMap.put(crf.Event_Name__c,new list<CR_Employee_Feed__c>());
                }
                eventNameCRListMap.get(crf.Event_Name__c).add(crf);
            }
            Contact con = new Contact();
                            con.FirstName = 'Test';
                            con.LastName = 'Contact';
                            con.Email = 'ritu.pandey@axtria.com';
                            insert con;
            RMS_Email_Configurations__c RMC = RMS_Email_Configurations__c.getOrgDefaults();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); // add rms email config?
            boolean sendEmail=false;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Workbench updated'); 
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false); 
            mail.setTargetObjectID(con.ID);
            mail.setSenderDisplayName('Insmed Portal');
            String body= '<html><body>'+
                             '<p>This is an automated email.Please do not reply to this email.';
                     body+=  '<br/><br/>Date: '+Date.Today().format()+'<br/>';
            for(string key : eventNameCRListMap.keySet()){
                system.debug('===key====='+key+'==eventNameCSVFieldMap=='+eventNameCSVFieldMap);
                string sendToCSV = string.valueOf(RMC.get(eventNameCSVFieldMap.get(key))); 
                list<string> sendTo;
                if(sendTOCSV != null){
                    sendTo = sendToCSV.split(',');    
                }
                mail.setToAddresses(sendTo);
                //mail.setBccAddresses(new String[] {'Perumalla.Venkata@axtria.com','Ruchi.Jain@axtria.com'});
                 body+=  '<br/>'+ '> '+
                             eventNameCRListMap.get(key).size()+' new '+ key + ' event/events have been created. <br/>';
                             
                sendEmail=true;
                
            }
            
            if(sendEmail==true){ 
                body+='</p>Kindly login to view the events.<br/><br/>Thank you,<br/>Axtria SalesIQ Support</body></html>';
                mail.setHtmlBody(body);
                    mails.add(mail);
                }
            
            
            try{
                if(!mails.isEmpty()){
                    //Messaging.sendEmail(mails);
                }
            }
            catch(Exception e){}
            Delete con;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        system.debug('inside getQueryLocator ');
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchProcessInbound(batchID));
    }
    
    global void execute(Database.BatchableContext BC, List<CurrentPersonFeed__c> scopeCurrentEmpList){
        InitialiseVariables();
        createMaps(scopeCurrentEmpList); 
        system.debug('Check Execute'); 
        system.debug('=======scopeCurrentEmpList==='+scopeCurrentEmpList);
        system.debug('====prevEmployeeMap===='+prevEmployeeMap);
        //preparing variables for field employee check
        list<CR_Employee_Feed__c> crFeedList = new list<CR_Employee_Feed__c>();
        //set<string> fieldEmpSet = new set<string>((system.label.Field_Employee).split(';'));
        boolean currentField;
        boolean prevField;
        
        for(CurrentPersonFeed__c EmpStg : scopeCurrentEmpList){
            
            //If Employee is not present previously
            
            //Commented by Ritu for AZ currentField=checkFieldEmployee(EmpStg.Department__c,EmpStg.JobCodeName__c);
            system.debug('EmpStg++'+EmpStg);
            system.debug('EmpStg.PRID__c++'+EmpStg.PRID__c);
            if(!prevEmployeeMap.containsKey(EmpStg.PRID__c)){
                if(  EmpStg.AssignmentStatusValue__c=='Active'){ //currentField &&
                    //New Hire Check
                    system.debug('inside new hire');
                    if(!emListEmpMap.containsKey(system.label.Employee_Newhire)){
                                emListEmpMap.put(system.label.Employee_Newhire,new set<CurrentPersonFeed__c>());
                            }
                            emListEmpMap.get(system.label.Employee_Newhire).add(EmpStg);
                            //added for priority check
                            if(!mapEmpID2Event.containsKey(EmpStg.PRID__c)){
                                mapEmpID2Event.put(EmpStg.PRID__c,system.label.Employee_Newhire);
                            }
                            else{
                                if(mapEvent2Priority.get(mapEmpID2Event.get(EmpStg.PRID__c)) < mapEvent2Priority.get(system.label.Employee_Newhire)){
                                    mapEmpID2Event.put(EmpStg.PRID__c,system.label.Employee_Newhire);
                                }
                            }
                }
            }
            //Employee Present in prev day
            else{
                //prevField=checkFieldEmployee(prevEmployeeMap.get(EmpStg.Employee_ID__c).Department__c,prevEmployeeMap.get(EmpStg.Employee_ID__c).JobCodeName__c);
                // Else, it's transfer to sales/Field
                if(EmpStg.AssignmentStatusValue__c == 'Active' && prevEmployeeMap.containskey(EmpStg.PRID__c) && prevEmployeeMap.get(EmpStg.PRID__c).Employee_Status__c=='Inactive')
                { //&& (!prevField) && currentField
                    system.debug('inside transfer1');
                        EmpStg.TerminationDate__c = null;
                        if(!emListEmpMap.containsKey(system.label.transfer_to_field)){
                            system.debug('inside transfer2');
                    
                            emListEmpMap.put(system.label.transfer_to_field,new set<CurrentPersonFeed__c>());
                        }
                        emListEmpMap.get(system.label.transfer_to_field).add(EmpStg);
                        system.debug('emListEmpMap++'+emListEmpMap);
                        //added for priority check
                            if(!mapEmpID2Event.containsKey(EmpStg.PRID__c)){
                                mapEmpID2Event.put(EmpStg.PRID__c,system.label.transfer_to_field);
                                system.debug('mapEmpID2Event1++'+mapEmpID2Event);
                            }
                            else{
                                if(mapEvent2Priority.get(mapEmpID2Event.get(EmpStg.PRID__c)) < mapEvent2Priority.get(system.label.transfer_to_field)){
                                    mapEmpID2Event.put(EmpStg.PRID__c,system.label.transfer_to_field);
                                    system.debug('mapEmpID2Event2222++'+mapEmpID2Event);
                                }
                            }
                        
                 }
                //terminate employee case
                else if(EmpStg.TerminationDate__c != null && EmpStg.TerminationDate__c != prevEmployeeMap.get(EmpStg.PRID__c).AxtriaSalesIQTM__HR_Termination_Date__c
                    && EmpStg.TerminationDate__c > prevEmployeeMap.get(EmpStg.PRID__c).AxtriaSalesIQTM__Original_Hire_Date__c && (prevEmployeeMap.get(EmpStg.PRID__c).Employee_Status__c=='Active' || prevEmployeeMap.get(EmpStg.PRID__c).Employee_Status__c=='Inactive')
                    && EmpStg.AssignmentStatusValue__c == 'Terminated'
                    ){ //&& prevField
                        system.debug('inside terminate');
                        if(!emListEmpMap.containsKey(system.label.Terminate_Employee)){
                                emListEmpMap.put(system.label.Terminate_Employee,new set<CurrentPersonFeed__c>());
                        }
                        emListEmpMap.get(system.label.Terminate_Employee).add(EmpStg);  
                        
                        //added for priority check
                        if(!mapEmpID2Event.containsKey(EmpStg.PRID__c)){
                            mapEmpID2Event.put(EmpStg.PRID__c,system.label.Terminate_Employee);
                        }
                        else{
                            if(mapEvent2Priority.get(mapEmpID2Event.get(EmpStg.PRID__c)) < mapEvent2Priority.get(system.label.Terminate_Employee)){
                                mapEmpID2Event.put(EmpStg.PRID__c,system.label.Terminate_Employee);
                            }
                        }
                        system.debug('====inside termination========');
                        
                    }
                //Transfer out of Sales/Field
                else if(EmpStg.TerminationDate__c != null && EmpStg.TerminationDate__c != prevEmployeeMap.get(EmpStg.PRID__c).AxtriaSalesIQTM__HR_Termination_Date__c
                    && EmpStg.TerminationDate__c > prevEmployeeMap.get(EmpStg.PRID__c).AxtriaSalesIQTM__Original_Hire_Date__c && prevEmployeeMap.get(EmpStg.PRID__c).Employee_Status__c=='Active'
                    && EmpStg.AssignmentStatusValue__c == 'Active'
                    ){ //&& prevField
                        system.debug('inside out of sales');
                        if(!emListEmpMap.containsKey(system.label.Transfer_out_of_Sales_team)){
                            emListEmpMap.put(system.label.Transfer_out_of_Sales_team,new set<CurrentPersonFeed__c>());
                        }
                        emListEmpMap.get(system.label.Transfer_out_of_Sales_team).add(EmpStg);   
                        
                        //added for priority check
                        if(!mapEmpID2Event.containsKey(EmpStg.PRID__c)){
                            mapEmpID2Event.put(EmpStg.PRID__c,system.label.Transfer_out_of_Sales_team);
                        }
                        else{
                            if(mapEvent2Priority.get(mapEmpID2Event.get(EmpStg.PRID__c)) < mapEvent2Priority.get(system.label.Transfer_out_of_Sales_team)){
                                mapEmpID2Event.put(EmpStg.PRID__c,system.label.Transfer_out_of_Sales_team);
                            }
                        }
                }           
            }
        }
        createNewEmployees();
        CreateEmployeeFeed();
        
        if(lstPositionEmployee.size() > 0){
            Update lstPositionEmployee;    
        }
        if(lstPEToBeDeleted.size() > 0){
            list<Deleted_Position_Employee__c> toBeInsertedList = new list<Deleted_Position_Employee__c>();
            for(AxtriaSalesIQTM__Position_Employee__c PEtoDel : lstPEToBeDeleted){
                Deleted_Position_Employee__c newDPE = new Deleted_Position_Employee__c();
                newDPE.Assignment_Type__c = PEtoDel.AxtriaSalesIQTM__Assignment_Type__c;
                newDPE.Employee__c = PEtoDel.AxtriaSalesIQTM__Employee__c;
                if(mapAssId2TermDate.containsKey(PEtoDel.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c)){
                    newDPE.End_Date__c = mapAssId2TermDate.get(PEtoDel.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c);
                }
                else{
                    newDPE.End_Date__c = PEtoDel.AxtriaSalesIQTM__Effective_End_Date__c;
                }
                newDPE.Position__c = PEtoDel.AxtriaSalesIQTM__Position__c;
                newDPE.Reason_Code__c = PEtoDel.AxtriaSalesIQTM__Reason_Code__c;
                newDPE.Start_Date__c = PEtoDel.AxtriaSalesIQTM__Effective_Start_Date__c;
                
                toBeInsertedList.add(newDPE);    
            }
            insert toBeInsertedList;
            delete lstPEToBeDeleted;    
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
            
        BatchCopyEmployeeFields copyfields = new BatchCopyEmployeeFields(batchID,mapEmployee2EventTypeDate);
        Database.executeBatch(copyfields, 100);
        
        mailSender(); 
    }    
}