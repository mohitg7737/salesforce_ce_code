public with sharing class ComputeValues extends BusinessRule implements IBusinessRule{

    public ComputeValues(){
        init();
        isAddNew = false;
        uiLocation = 'Compute Values';
        selectedMatrix = '';
        retUrl = '/apex/ComputeValues?mode=' +mode+'&rid='+ruleId; 
        if(!Test.isRunningTest())
        initStep();
    }

    public void save(){
        string result = validateStep();
        if(step != null && String.isBlank(result)){
            step.UI_Location__c = uiLocation;
            /*System.debug('---step.Step_Type__c--- ' + step.Step_Type__c);*/
            if(step.Step_Type__c == 'Matrix'){
                step.Matrix__c = selectedMatrix;
                step.Grid_Param_1__c = gridParam1;
                if(gridMap.Grid_Type__c == '2D'){
                    step.Grid_Param_2__c = gridParam2;
                }
            }else if(step.Step_Type__c == 'Compute'){
                /*System.debug('--selectedCompF2-- ' + selectedCompF2);*/
                /*System.debug('--selectedCompF1-- ' + selectedCompF1);*/
                computeObj.Field_1__c = selectedCompF1;
                computeObj.Field_2_Type__c = field2Type;
                if(String.isNotBlank(selectedCompF2))
                    computeObj.Field_2__c = selectedCompF2;
                else{
                    computeObj.Field_2_val__c = selectedCompF3;
                }
                upsert computeObj;
                step.Compute_Master__c = computeObj.Id;
            }else if(step.Step_Type__c == 'Cases'){
                computeObj.Expression__c = expression;
                upsert computeObj;
                step.Compute_Master__c = computeObj.Id;
            }
            /*System.debug('---saving step -- ' + step);*/
            upsert step;

            if(!isStepEditMode){
                //Update Next steps if any
                list<Step__c> allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c ORDER BY Sequence__c];
                if(allNextSteps != null && allNextSteps.size() > 0){
                    for(Step__c st: allNextSteps){
                        if(st.Id != step.Id)
                            st.Sequence__c += 1;
                    }
                }
                Update allNextSteps;
            }
            Rule_Parameter__c rp = new Rule_Parameter__c();
            if(String.isNotBlank(step.Id)){
                list<Rule_Parameter__c> rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id];
                if(rps != null && rps.size() > 0){
                    rp.Id = rps[0].Id;
                }
            }
            rp.Measure_Master__c = ruleObject.Id;
            rp.Step__c = step.Id;
            rp.Type__c = step.Type__c;
            upsert rp;

            updateRule(uiLocation, 'Profiling Parameter');
        }

        if(String.isNotBlank(result)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
        }
    }

    public PageReference saveAndNext(){
        save();
        updateRule('Compute Segment', uiLocation);
        return nextPage('ComputeSegment');
    }

    public PageReference skipStep()
    {
        updateRule('Compute Segment', uiLocation);
        return nextPage('ComputeSegment');
    }

    public void Openpopup(){
        System.debug('============INSIDE OPEN POPUP FUNCTION');
        System.debug('=========Select Matrix is::'+selectedMatrix);
        list<Step__c> Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix];
        if(!step.isEmpty()){
            Measure_Master__c MM = [SELECT Id, Name FROM Measure_Master__c WHERE id =:ruleObject.Id];
            Grid_Master__c GM = [select id,name,Brand__c,Brand_Team_Instance__c,Col__c,Country__c,Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c,Output_Name__c,Output_Type__c,Row__c from Grid_Master__c where id =:selectedMatrix];
                Grid_Master__c CloneGM = GM.Clone();
                String name =GM.Name ;
                CloneGM.Name = MM.Name+'_'+name;
                CloneGM.Country__c = GM.Country__c;
                CloneGM.CurrencyIsoCode = 'EUR';
                system.debug('====CloneGM===:'+CloneGM);
                try{
                insert CloneGM;
                matrixList.add(new SelectOption(CloneGM.Id, CloneGM.Name));

                list<Grid_Details__c> GD = new list<Grid_Details__c>();
                list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();

                GD = [select id,Name,Grid_Master__c,colvalue__c,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c  from Grid_Details__c where Grid_Master__c =:selectedMatrix];
                for(Grid_Details__c g : GD){
                    Grid_Details__c newGD = g.clone();
                        newGD.Grid_Master__c = CloneGM.id;
                        newGD.CurrencyIsoCode = 'EUR';
                        newGD.Name = g.name;
                        CloneGD.add(newGD);
                    
                }
                insert CloneGD;
                System.debug('========CloneGD====:'+CloneGD);
                
                selectedMatrix= CloneGM.id;
            }
            catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Matrix Name Should be Unique'));
            }
        }
    }
}