global class BatchPrimaryAffiliationIndicator implements Database.Batchable <sObject> , Database.Stateful{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Parent_Account__c FROM AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c = \'Active\' and Primary_Affiliation_Indicator__c = \'Y\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Account_Affiliation__c> scope) {
        List<Account> accounts = new List<Account>();
        Set<ID> accs = new Set<ID>();

         for(AxtriaSalesIQTM__Account_Affiliation__c accaff : scope)
         {
            // for(Account acc : AxtriaSalesIQTM__Account_Affiliation__c.accounts)
                if(!accs.contains(accaff.AxtriaSalesIQTM__Account__c))
                {
                     Account acc = new Account(id = accaff.AxtriaSalesIQTM__Account__c);
                     acc.ParentId = accaff.AxtriaSalesIQTM__Parent_Account__c;
                     accounts.add(acc);
                     accs.add(accaff.AxtriaSalesIQTM__Account__c);                    
                }
          }
         update accounts;
    }   
    
    global void finish(Database.BatchableContext bc) {
    }

}