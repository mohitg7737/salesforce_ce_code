@isTest
public class ChnageRequestTriggerHandlerAZ_test {
    @istest static void metadatatest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'GR Sales IMM';
        insert team;
        
         AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        insert teamIns;
        Line__c line = new line__c();
        line.Name = 'Mistral';
        line.Team__c = team.id;
        insert line;
        
        
        
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
       // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        posNation.Line__c = line.id;
        insert posNation;
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
        pos2.Line__c= line.id;
       // pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='3';
        insert pos2;
                
        AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
        posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
        posdm.Name = 'Chico CA_SPEC';
        posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
        posdm.Line__c = line.id;
        //posdm.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='2';
        insert posdm;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos.Name = 'Chico CA_SPEC';
        pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos;
        AxtriaSalesIQTM__Position__c pos22 = new AxtriaSalesIQTM__Position__c();
        pos22.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos22.Name = 'Chico CA_SPEC2';
        pos22.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos22.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos22.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos22;
        
		
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = posdm.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        insert posTeam;
        
        
         Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        acc.Marketing_Code__c='ES';
        insert acc;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c plan=new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        plan.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        plan.AxtriaSalesIQTM__Account__c = acc.id;
        insert plan;
        
        AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
        crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
        crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        insert crType;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__RecordTypeID__c = crType.id;
        cr.AxtriaSalesIQTM__Approver1__c = loggedInUser.id;
       // cr.AxtriaSalesIQTM__Is_Auto_Approved__c = True;
        cr.AxtriaSalesIQTM__Approver2__c=loggedInUser.id;
        cr.AxtriaSalesIQTM__Approver3__c = loggedInUser.id;
        cr.AxtriaSalesIQTM__Source_Position__c = pos.id;
        cr.AxtriaSalesIQTM__Destination_Position__c = pos22.id;
        cr.AxtriaSalesIQTM__Status__c = 'Pending';
        cr.AxtriaSalesIQTM__Team_Instance_ID__c=teamIns.id;
        cr.AxtriaSalesIQTM__Account_Moved_Id__c = '123';
        insert cr;
        list<AxtriaSalesIQTM__Change_Request__c>crlist = new list<AxtriaSalesIQTM__Change_Request__c>();
        crlist.add(cr);
        AxtriaSalesIQTM__CR_Call_Plan__c ccc=new AxtriaSalesIQTM__CR_Call_Plan__c();
        ccc.AxtriaSalesIQTM__Account__c=acc.id;
        ccc.AxtriaSalesIQTM__Change_Request_ID__c = cr.id;
        insert ccc;
        
          
        
            AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamAtt = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamAtt.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        teamAtt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamAtt.AxtriaSalesIQTM__isRequired__c = true;
        teamAtt.AxtriaSalesIQTM__isEnabled__c = true;
        teamAtt.AxtriaSalesIQTM__Attribute_Display_Name__c='Test1';
        teamAtt.AxtriaSalesIQTM__Attribute_API_Name__c ='Position_Account_Call_Plan__r.AxtriaSalesIQTM__Account__r.Name';
        teamAtt.AxtriaSalesIQTM__Object_Name__c = 'Position_Account_Call_Plan_Product__c';
        insert teamatt;     
       
        Test.startTest();
         System.runAs(loggedinuser){
        ChnageRequestTriggerHandlerAZ obj=new ChnageRequestTriggerHandlerAZ();
        ChnageRequestTriggerHandlerAZ.updatePositionAccountCallPlan(crlist);
        ChnageRequestTriggerHandlerAZ.updateMetricSummaryTable('abc',pos.id,teamIns.id);
         }
        
        Test.stopTest();

    }
}