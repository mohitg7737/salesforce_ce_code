global class BatchCloneCallPlanForMultiplePosition implements Database.Batchable<sObject> {
    public String query;
    public String line;
    public String brand;
    public String businessUnit;
    public String ruleId;
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}

    global BatchCloneCallPlanForMultiplePosition(String ruleId){

      customsetting = new AxtriaSalesIQTM__TriggerContol__c();
      customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
      customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
      
        Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__r.Name, Line_2__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        this.ruleId = ruleId;
        //line = rule.Line_2__c;
        brand = rule.Brand_Lookup__r.Name;
        businessUnit = rule.Team_Instance__c;
        this.query  = createQuery();
        this.query += 'WHERE (P1_Original__c = \'' + brand + '\' OR P1__c = \'' + brand + '\') AND AxtriaSalesIQTM__Position__c != null'; //Line__c = \'' + line +'\' AND
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        set<String> physicians = new set<String>();
        for(sObject s: scope){
            physicians.add((String)s.get('AxtriaSalesIQTM__Account__c'));
        }
        
        String q = createQuery();
        q += 'WHERE AxtriaSalesIQTM__Account__c IN:physicians  AND (P1_Original__c = \'' + brand + '\' OR P1__c = \'' + brand + '\') AND AxtriaSalesIQTM__Position__c != null'; //AND Line__c = \'' + line +'\'
        list<sObject> callPlans = Database.query(q);
        
        map<String, map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c>> accountCallPlan = new map<String, map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c>>();
        for(sObject s: callPlans){
            map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c> tempMap;
            if(accountCallPlan.containsKey((String)s.get('AxtriaSalesIQTM__Account__c'))){
                tempMap = accountCallPlan.get((String)s.get('AxtriaSalesIQTM__Account__c'));
            }else{
                tempMap = new map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
            }
            tempMap.put((String)s.get('AxtriaSalesIQTM__Position__c'), (AxtriaSalesIQTM__Position_Account_Call_Plan__c)s);
            accountCallPlan.put((String)s.get('AxtriaSalesIQTM__Account__c'), tempMap);
        }
        
        list<AxtriaSalesIQTM__Change_Request__c> changeRequests = [SELECT Id, AxtriaSalesIQTM__Destination_Position__c, AxtriaSalesIQTM__Status__c FROM AxtriaSalesIQTM__Change_Request__c WHERE AxtriaSalesIQTM__Status__c = 'Submitted' OR AxtriaSalesIQTM__Status__c = 'Approved'];
        set<String> postionChangeStatus = new set<String>();
        for(AxtriaSalesIQTM__Change_Request__c cr : changeRequests){
            postionChangeStatus.add(cr.AxtriaSalesIQTM__Destination_Position__c);
        }
        
        map<String, set<String>> accountPostion = new map<String, set<String>>();
        list<AxtriaSalesIQTM__Position_Account__c> positionAccount = [SELECT AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Call_Plan_Status__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Account__c IN:physicians AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c =: businessUnit];
        for(AxtriaSalesIQTM__Position_Account__c pa: positionAccount){
            set<String> tempSet;
            if(accountPostion.containsKey(pa.AxtriaSalesIQTM__Account__c)){
                tempSet = accountPostion.get(pa.AxtriaSalesIQTM__Account__c);
            }else{
                tempSet = new set<String>();
            }
            tempSet.add(pa.AxtriaSalesIQTM__Position__c);
            accountPostion.put(pa.AxtriaSalesIQTM__Account__c, tempSet);
        }
        
        system.debug('--accountPostion-- ' + accountPostion);
        system.debug('--accountCallPlan-- ' + accountCallPlan);
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> cloneCallPlan = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(String accountId : accountPostion.keyset()){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlan2Clone;
            Set<String> clonePosition = new Set<String>();
            if(accountCallPlan.containsKey(accountId)){
                for(String positionId: accountPostion.get(accountId)){
                    if(accountCallPlan.get(accountId).containsKey(positionId)){
                        callPlan2Clone = accountCallPlan.get(accountId).get(positionId);
                    }else{
                        if(!postionChangeStatus.contains(positionId)){
                            clonePosition.add(positionId);
                        }
                    }
                }
            }
            
            system.debug('--callPlan2Clone--- ' + callPlan2Clone);
            if(callPlan2Clone != null && (clonePosition != null && clonePosition.size() > 0)){
                for(String pos: clonePosition){
                    AxtriaSalesIQTM__Position_Account_Call_Plan__c cp = callPlan2Clone.clone(false, true, false, false);
                    cp.AxtriaSalesIQTM__Position__c = pos;
                    cp.TCF_P1__c = cp.Final_TCF_Original__c;
                    cp.Final_TCF__c = cp.Final_TCF_Original__c;
                    cp.AxtriaSalesIQTM__isincludedCallPlan__c = true;
                    cp.AxtriaSalesIQTM__isAccountTarget__c = true;
                    cp.AxtriaSalesIQTM__lastApprovedTarget__c = true;
                    cp.P1__c = cp.P1_Original__c;
                    cp.isFinalTCFApproved__c = false;
                    cp.AxtriaSalesIQTM__Change_Status__c = 'No Change';
                    cloneCallPlan.add(cp);
                }
            }
        }
        
        customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
        customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
        system.debug('==========customsetting========'+customsetting);
        customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;

        customsettinglist.add(customsetting);
        customsettinglist.add(customsetting2);
        update customsettinglist;
        
        //update customsetting ;

        insert cloneCallPlan;
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
        //update customsetting ;
        customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
        //update customsetting ;
        customsettinglist.add(customsetting);
        customsettinglist.add(customsetting2);
        update customsettinglist;
       
    }
    
    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new BatchUpdateSharedFlag(this.ruleId));
    }
    
    private string createQuery(){
        String query  = 'SELECT ';
               query += 'Id, Name, AxtriaSalesIQTM__Account_Alignment_type__c, ';
               query += 'AxtriaSalesIQTM__Account__c, ';
               query += 'AxtriaSalesIQTM__Alignment_Change_Action__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Approved__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Original__c, ';
               query += 'AxtriaSalesIQTM__Call_Sequence_Updated__c, ';
               query += 'AxtriaSalesIQTM__Calls_Approved__c, ';
               query += 'AxtriaSalesIQTM__Calls_Original__c, ';
               query += 'AxtriaSalesIQTM__Calls_Updated__c, ';
               query += 'AxtriaSalesIQTM__Change_Status__c, ';
               query += 'AxtriaSalesIQTM__CheckBox1_Approved__c, ';
               query += 'AxtriaSalesIQTM__CheckBox1_Updated__c, ';
               query += 'AxtriaSalesIQTM__CheckBox2_Approved__c, ';
               query += 'AxtriaSalesIQTM__CheckBox2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox10__c, ';
               query += 'AxtriaSalesIQTM__Checkbox1__c, ';
               query += 'AxtriaSalesIQTM__Checkbox2__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox3__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox4__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Checkbox5__c, ';
               query += 'AxtriaSalesIQTM__Checkbox6__c, ';
               query += 'AxtriaSalesIQTM__Checkbox7__c, ';
               query += 'AxtriaSalesIQTM__Checkbox8__c, ';
               query += 'AxtriaSalesIQTM__Checkbox9__c, ';
               query += 'AxtriaSalesIQTM__Comments__c, ';
               query += 'AxtriaSalesIQTM__Effective_End_Date__c, ';
               query += 'AxtriaSalesIQTM__Effective_Start_Date__c, ';
               query += 'AxtriaSalesIQTM__Metric10__c, ';
               query += 'AxtriaSalesIQTM__Metric1_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric1__c, ';
               query += 'AxtriaSalesIQTM__Metric2_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric2__c, ';
               query += 'AxtriaSalesIQTM__Metric3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric3__c, ';
               query += 'AxtriaSalesIQTM__Metric4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric4__c, ';
               query += 'AxtriaSalesIQTM__Metric5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Metric5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Metric5__c, ';
               query += 'AxtriaSalesIQTM__Metric6__c, ';
               query += 'AxtriaSalesIQTM__Metric7__c, ';
               query += 'AxtriaSalesIQTM__Metric8__c, ';
               query += 'AxtriaSalesIQTM__Metric9__c, ';
               query += 'AxtriaSalesIQTM__NexavarCalls__c, ';
               query += 'AxtriaSalesIQTM__Nexavar_Original__c, ';
               query += 'AxtriaSalesIQTM__OverAll_Target__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Segment_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Segment__c, ';
               query += 'AxtriaSalesIQTM__Picklist3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist4_Metric__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric_Approved__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric_Updated__c, ';
               query += 'AxtriaSalesIQTM__Picklist5_Metric__c, ';
               query += 'AxtriaSalesIQTM__Position_Team_Instance__c, ';
               query += 'AxtriaSalesIQTM__Position__c, ';
               query += 'AxtriaSalesIQTM__Rank__c, ';
               query += 'AxtriaSalesIQTM__ReasonAdd__c, ';
               query += 'AxtriaSalesIQTM__ReasonDrop__c, ';
               query += 'AxtriaSalesIQTM__Sample_Flag__c, ';
               query += 'AxtriaSalesIQTM__Segment10__c, ';
               query += 'AxtriaSalesIQTM__Segment1_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment1_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment1__c, ';
               query += 'AxtriaSalesIQTM__Segment2_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment2_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment2__c, ';
               query += 'AxtriaSalesIQTM__Segment3_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment3_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment3__c, ';
               query += 'AxtriaSalesIQTM__Segment4_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment4_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment4__c, ';
               query += 'AxtriaSalesIQTM__Segment5_Approved__c, ';
               query += 'AxtriaSalesIQTM__Segment5_Updated__c, ';
               query += 'AxtriaSalesIQTM__Segment5__c, ';
               query += 'AxtriaSalesIQTM__Segment6__c, ';
               query += 'AxtriaSalesIQTM__Segment7__c, ';
               query += 'AxtriaSalesIQTM__Segment8__c, ';
               query += 'AxtriaSalesIQTM__Segment9__c, ';
               query += 'AxtriaSalesIQTM__Source__c, ';
               query += 'AxtriaSalesIQTM__StivargaCalls__c, ';
               query += 'AxtriaSalesIQTM__Stivarga_Original__c, ';
               query += 'AxtriaSalesIQTM__Target_Type__c, ';
               query += 'AxtriaSalesIQTM__Team_Instance_Account__c, ';
               query += 'AxtriaSalesIQTM__Team_Instance__c, ';
               query += 'AxtriaSalesIQTM__WasEverInCallPlan__c, ';
               query += 'AxtriaSalesIQTM__isAccountTarget__c, ';
               query += 'AxtriaSalesIQTM__isAddedFromAlignment__c, ';
               query += 'AxtriaSalesIQTM__isModified__c, ';
               query += 'AxtriaSalesIQTM__isincludedCallPlan__c, ';
               query += 'AxtriaSalesIQTM__lastApprovedTarget__c, ';
               query += 'Party_ID__c, ';
               query += 'Calculated_TCF__c, ';
               query += 'Proposed_TCF__c, ';
               query += 'Final_TCF__c, ';
               query += 'Segment__c, ';
               query += 'P1__c, ';
               query += 'P2__c, ';
               query += 'P3__c, ';
               query += 'P4__c, ';
               query += 'TCF_P1__c, ';
               query += 'TCF_P2__c, ';
               query += 'TCF_P3__c, ';
               query += 'TCF_P4__c, ';
               query += 'DYNAMIC_DPP_IV__c, ';
               query += 'DYNAMIC_SGLT2__c, ';
               query += 'DYNAMIC_DAPA__c, ';
               query += 'POTENTIAL_INN_ORAL__c, ';
               query += 'KOL_DAPA__c, ';
               query += 'Parameter1__c, ';
               query += 'Parameter2__c, ';
               query += 'Parameter3__c, ';
               query += 'Parameter4__c, ';
               query += 'Parameter5__c, ';
               query += 'Parameter6__c, ';
               query += 'isparmmodified__c, ';
               query += 'Brand_Team_Instance__c, ';
               query += 'Speciality__c, ';
               query += 'isLocked__c, ';
               query += 'Product1__c, ';
               query += 'Product2__c, ';
               query += 'Product3__c, ';
               query += 'Product4__c, ';
               query += 'Parameter7__c, ';
               query += 'Parameter8__c, ';
              // query += 'Line__c, ';
               query += 'Parameter1_Original__c, ';
               query += 'Parameter2_Original__c, ';
               query += 'Parameter3_Original__c, ';
               query += 'Parameter4_Original__c, ';
               query += 'Parameter5_Original__c, ';
               query += 'Parameter6_Original__c, ';
               query += 'Parameter7_Original__c, ';
               query += 'Parameter8_Original__c, ';
               query += 'Parameter1_Approved__c, ';
               query += 'Parameter2_Approved__c, ';
               query += 'Parameter3_Approved__c, ';
               query += 'Parameter4_Approved__c, ';
               query += 'Parameter5_Approved__c, ';
               query += 'Parameter6_Approved__c, ';
               query += 'Parameter7_Approved__c, ';
               query += 'Parameter8_Approved__c, ';
               query += 'Initial_Count__c, ';
               query += 'Updated_Count__c, ';
               query += 'Count_Final_TCF__c, ';
               query += 'CL_Count__c, ';
               query += 'A_Count__c, ';
               query += 'B_Count__c, ';
               query += 'C_Count__c, ';
               query += 'ND_Count__c, ';
               query += 'Share__c, ';
               query += 'P2_Parameter1__c, ';
               query += 'P2_Parameter2__c, ';
               query += 'P2_Parameter3__c, ';
               query += 'P2_Parameter4__c, ';
               query += 'P2_Parameter5__c, ';
               query += 'P2_Parameter6__c, ';
               query += 'P2_Parameter7__c, ';
               query += 'P2_Parameter8__c, ';
               query += 'P3_Parameter1__c, ';
               query += 'P3_Parameter2__c, ';
               query += 'P3_Parameter3__c, ';
               query += 'P3_Parameter4__c, ';
               query += 'P3_Parameter5__c, ';
               query += 'P3_Parameter6__c, ';
               query += 'P3_Parameter7__c, ';
               query += 'P3_Parameter8__c, ';
               query += 'P4_Parameter1__c, ';
               query += 'P4_Parameter2__c, ';
               query += 'P4_Parameter3__c, ';
               query += 'P4_Parameter4__c, ';
               query += 'P4_Parameter5__c, ';
               query += 'P4_Parameter6__c, ';
               query += 'P4_Parameter7__c, ';
               query += 'P4_Parameter8__c, ';
               query += 'P2_Parameter1_Original__c, ';
               query += 'P2_Parameter2_Original__c, ';
               query += 'P2_Parameter3_Original__c, ';
               query += 'P2_Parameter4_Original__c, ';
               query += 'P2_Parameter5_Original__c, ';
               query += 'P2_Parameter6_Original__c, ';
               query += 'P2_Parameter7_Original__c, ';
               query += 'P2_Parameter8_Original__c, ';
               query += 'P3_Parameter1_Original__c, ';
               query += 'P3_Parameter2_Original__c, ';
               query += 'P3_Parameter3_Original__c, ';
               query += 'P3_Parameter4_Original__c, ';
               query += 'P3_Parameter5_Original__c, ';
               query += 'P3_Parameter6_Original__c, ';
               query += 'P3_Parameter7_Original__c, ';
               query += 'P3_Parameter8_Original__c, ';
               query += 'P4_Parameter1_Original__c, ';
               query += 'P4_Parameter2_Original__c, ';
               query += 'P4_Parameter3_Original__c, ';
               query += 'P4_Parameter4_Original__c, ';
               query += 'P4_Parameter5_Original__c, ';
               query += 'P4_Parameter6_Original__c, ';
               query += 'P4_Parameter7_Original__c, ';
               query += 'P4_Parameter8_Original__c, ';
               query += 'teamcall__c, ';
               query += 'allcall__c, ';
               query += 'Final_TCF_Approved__c, ';
               query += 'Final_TCF_Original__c, ';
               query += 'Record_Updated__c, ';
               query += 'Rule__c, ';
               query += 'isFinalTCFApproved__c, ';
               query += 'CL_finaltcf__c, ';
               query += 'C_Finaltcf_c__c, ';
               query += 'A_Finaltcf_c__c, ';
               query += 'B_Finaltcf_c__c, ';
               query += 'ND_Finaltcf_c__c, ';
               query += 'isClone__c, ';
               query += 'Adoption__c, ';
               query += 'Potential__c, ';
               query += 'Calculated_TCF2__c, ';
               query += 'Proposed_TCF2__c, ';
               query += 'Segment2__c, ';
               query += 'Accessibility_Range__c, ';
               query += 'Brand_Name__c, ';
               query += 'isproductswapped__c, ';
               query += 'initialflag__c, ';
               query += 'P1_Original__c, ';
               query += 'is_LoggedIn__c, ';
               query += 'Account_HCP_NUmber__c, ';
               query += 'IsDMlogged__c, ';
               query += 'concatPosTeamInstance__c, ';
               query += 'Consent__c, ';
               query += 'Rep_Name__c, ';
               query += 'HCP__c, ';
               query += 'Area__c, ';
               query += 'Consent_Value__c, ';
               query += 'CountOriginal__c, ';
               query += 'CountApproved__c, ';
               query += 'CountTCForiginal__c, ';
               query += 'CountTCFapproved__c, ';
               query += 'Segment_Value_Name__c, ';
               query += 'Event__c, ';
               query += 'Hospital_Formula__c, ';
               query += 'CL1__c, ';
               query += 'CL2__c ';
               query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
        return query;
    }

}