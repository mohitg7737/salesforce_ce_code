global class BatchUpdateParentPacpOnPacp implements Database.Batchable<sObject>,Database.stateful {
    public String query;
    public string teaminstanceis {get;set;}
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}
    public string ruleidis {get;set;}

    global BatchUpdateParentPacpOnPacp(string selectedteaminstance) {
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c=:teaminstanceis';
        }
        system.debug('=======The Query is:'+query);
    }

    global BatchUpdateParentPacpOnPacp(string selectedteaminstance,string ruleID) {
        //this.query = query;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        ruleidis = '';
        ruleidis = ruleID;

        teaminstanceis = '';
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and  Parent_PACP__c =null ';
        if(selectedteaminstance!=null){
            teaminstanceis = selectedteaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c=:teaminstanceis';
        }
        system.debug('=======The Query is:'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=======The Query2 is:'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
        set<string>teaminstances = new set<string>();
        set<string>positions = new set<string>();
        set<string>accounts = new set<string>();
        map<string,string>parentpacpidmap = new map<string,string>();
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> updatepacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
            teaminstances.add(pacp.AxtriaSalesIQTM__Team_Instance__c);
            positions.add(pacp.AxtriaSalesIQTM__Position__c);
            accounts.add(pacp.AxtriaSalesIQTM__Account__c);
        }
        if(accounts!=null && accounts.size()>0 && positions!=null && positions.size()>0 && teaminstances!=null && teaminstances.size()>0){
            system.debug('============Inside the if condition==========');
            list<Parent_PACP__c>ParentList = [select id,Account__c,Position__c,Team_Instance__c from Parent_PACP__c where Account__c IN :accounts and Position__c IN:positions and Team_Instance__c IN:teaminstances];
            if(ParentList!=null && ParentList.size()>0){
                for(Parent_PACP__c parentpacp : ParentList){
                    string key=parentpacp.Account__c+'_'+parentpacp.Position__c+'_'+parentpacp.Team_Instance__c;
                    if(!parentpacpidmap.containskey(key)){
                        parentpacpidmap.put(key,parentpacp.id);
                    }
                }
            }
        }
        
        if(parentpacpidmap!=null && parentpacpidmap.size()>0){
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
                string key2 = pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                if(parentpacpidmap.containskey(key2)){
                   pacp.Parent_PACP__c =  parentpacpidmap.get(key2);
                   updatepacp.add(pacp);
                }
            }

        }
        CallPlanSummaryTriggerHandler.execute_trigger = false;
        customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
        customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
        system.debug('==========customsetting========'+customsetting);
        customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        //update customsetting ;
        customsettinglist.add(customsetting);
        customsettinglist.add(customsetting2);
        update customsettinglist;
        
        update updatepacp;

        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        CallPlanSummaryTriggerHandler.execute_trigger = true;
        customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
        //update customsetting ;
        customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
        customsettinglist.add(customsetting);
        customsettinglist.add(customsetting2);
        update customsettinglist;
       


    }

    global void finish(Database.BatchableContext BC) {
        system.debug('=======FINISHED THE PARENT PACP BATCHES=====');
        system.debug('+++++++++ Hey ' + teaminstanceis + ' ' +ruleidis);
        Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis,ruleidis), 2000);

    }
}