global class BatchOutboundWorkspace implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global Date today=null;
     global String query;
     public map<String,String>Countrymap {get;set;}
      public set<String> Uniqueset {get;set;} 

     global BatchOutboundWorkspace (){//set<String> Accountid
        Countrymap = new map<String,String>();
        Uniqueset = new set<String>(); 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Workspace' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);


        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
                                                                                                                                     
                                                      
                                                                                      
             
         
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'workspace';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
       
        recordsProcessed =0;
        today=Date.today();
        System.debug('today++'+today);
       query = 'SELECT AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Number_of_Scenarios__c,AxtriaSalesIQTM__Workspace_Description__c, ' +
               'AxtriaSalesIQTM__Workspace_End_Date__c,AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,AxtriaSalesIQTM__Workspace_Start_Date__c,Id,Name FROM AxtriaSalesIQTM__Workspace__c ' +
               'where AxtriaSalesIQTM__Workspace_Start_Date__c <=: today and AxtriaSalesIQTM__Workspace_End_Date__c >=: today ' ;
                          
       
        
        if(lastjobDate!=null){
            query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
        
            
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchOutboundWorkspace() );
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Workspace__c > records){
        // process each batch of records
       

        List<Siq_Workspace_O__c> workspaces = new List<Siq_Workspace_O__c>();
        for (AxtriaSalesIQTM__Workspace__c workspace : records) {
            String key=workspace.id;
           if(!Uniqueset.contains(Key)){
               Siq_Workspace_O__c ws=new Siq_Workspace_O__c();
                      ws.Siq_Workspace_Country_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                      ws.Siq_Workspace_End_Date__c =workspace.AxtriaSalesIQTM__Workspace_End_Date__c;
                      ws.Siq_Workspace_Marketing_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c  ;    
                      ws.Siq_Workspace_Name__c  =workspace.Name;
                      ws.Siq_Workspace_Start_Date__c= workspace.AxtriaSalesIQTM__Workspace_Start_Date__c ; 
                      ws.Unique_Id__c =key;
                workspaces.add(ws);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                   Uniqueset.add(key);
                //comments
            }
        }
     
        upsert workspaces Unique_Id__c;
       
      
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                 sjob.Object_Name__c = 'Workspace';
                //sjob.Changes__c        
                                         
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
         //Database.ExecuteBatch(new BatchOutBoundEmployee(),200); 
        
    }   
}