global class BatchCreateRuleResultsDownload implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public String ruleId;
    global String results;
  public Measure_Master__c rule;

   public String allSegments;

    global BatchCreateRuleResultsDownload(String ruleId) {
        this.ruleId = ruleId;
    rule = [SELECT Id, Name, File_Download_Id__c, State__c, Cycle__c, Line_2__c, Brand_Lookup__c, Team_Instance__c FROM Measure_Master__c WHERE Id =:ruleId];
        this.query  =  'SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.Name, Physician_2__c, ';
        this.query  += 'Measure_Master__c, Measure_Master__r.Name, OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, ';
        this.query  += 'OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, ';
        this.query  += 'OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, ';
        this.query  += 'OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c, ';
        this.query  += 'OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, ';
        this.query  += 'OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c, OUTPUT_VALUE_1__c, ';
        this.query  += 'OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, ';
        this.query  += 'OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c, ';
        this.query  += 'OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c, ';
        this.query  += 'OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, ';
        this.query  += 'OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c ';
        this.query  += 'FROM Account_Compute_Final__c WHERE Measure_Master__c = \'' + this.ruleId + '\'';
        
        results = '';
        results = createCSVHeader(this.ruleId);
    }

    global BatchCreateRuleResultsDownload(String ruleId, String allSegments) {
        this.ruleId = ruleId;
        this.allSegments = allSegments;
    rule = [SELECT Id, Name, File_Download_Id__c, State__c, Cycle__c, Line_2__c, Brand_Lookup__c, Team_Instance__c FROM Measure_Master__c WHERE Id =:ruleId];
        this.query  =  'SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.Name, Physician_2__c, ';
        this.query  += 'Measure_Master__c, Measure_Master__r.Name, OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, ';
        this.query  += 'OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, ';
        this.query  += 'OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, ';
        this.query  += 'OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c, ';
        this.query  += 'OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, ';
        this.query  += 'OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c, OUTPUT_VALUE_1__c, ';
        this.query  += 'OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, ';
        this.query  += 'OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c, ';
        this.query  += 'OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c, ';
        this.query  += 'OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, ';
        this.query  += 'OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c, Final_TCF__c  ';
        this.query  += 'FROM Account_Compute_Final__c WHERE Measure_Master__c = \'' + this.ruleId + '\'';
        
        results = '';
        results = createCSVHeader(this.ruleId);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
    for(sObject acf : scope){
      results += createCSVRow(acf, rule.Name);
    }
  }

    global void finish(Database.BatchableContext BC) {
    ContentVersion file = new ContentVersion(title = rule.Name + '.csv', versionData = Blob.valueOf( results ), pathOnClient = '/' + rule.Name + '.csv');
      insert file;
        
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: file.Id LIMIT 1];
        ContentDistribution cd = new ContentDistribution();
        cd.Name = rule.Name + '.csv';
        //cd.ContentDocumentId = cv.ContentDocumentId;
        cd.ContentVersionId = cv.Id;
        insert cd;
        
        ContentDistribution cdUrl = [SELECT Id, ContentDownloadUrl, DistributionPublicUrl FROM ContentDistribution WHERE Id=:cd.Id];
        list<Measure_Master__c> oldRules = [SELECT Id FROM Measure_Master__c WHERE Cycle__c =:rule.Cycle__c AND Team_Instance__c =: rule.Team_Instance__c  AND Brand_Lookup__c =: rule.Brand_Lookup__c AND is_complete__c = true AND State__c = 'Complete'];//AND Line_2__c =: rule.Line_2__c
        rule.is_executed__c = true;
        rule.File_Download_Id__c = cdUrl.ContentDownloadUrl;    
        if(oldRules != null && oldRules.size() > 0){
            rule.State__c = 'Publish Not Allowed';
        }else{
            rule.State__c = 'Executed';
        }
        rule.All_Segments__c = allSegments;
        update rule;
  }
    
    private String createCSVRow(sObject acf, String ruleName){
        String headers = '"';
    headers += (String)acf.getsObject('Physician_2__r').get('Name') != null ? (String)acf.getsObject('Physician_2__r').get('Name') : '';
        headers +=  '","' +(String)acf.getsObject('Physician_2__r').get('AxtriaSalesIQTM__External_Account_Number__c');
        headers +=  '","' +ruleName;
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_1__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_1__c') != null ? (String)acf.get('OUTPUT_VALUE_1__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_2__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_2__c') != null ? (String)acf.get('OUTPUT_VALUE_2__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_3__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_3__c') != null ? (String)acf.get('OUTPUT_VALUE_3__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_4__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_4__c') != null ? (String)acf.get('OUTPUT_VALUE_4__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_5__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_5__c') != null ? (String)acf.get('OUTPUT_VALUE_5__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_6__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_6__c') != null ? (String)acf.get('OUTPUT_VALUE_6__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_7__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_7__c') != null ? (String)acf.get('OUTPUT_VALUE_7__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_8__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_8__c') != null ? (String)acf.get('OUTPUT_VALUE_8__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_9__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_9__c') != null ? (String)acf.get('OUTPUT_VALUE_9__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_10__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_10__c') != null ? (String)acf.get('OUTPUT_VALUE_10__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_11__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_11__c') != null ? (String)acf.get('OUTPUT_VALUE_11__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_12__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_12__c') != null ? (String)acf.get('OUTPUT_VALUE_12__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_13__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_13__c') != null ? (String)acf.get('OUTPUT_VALUE_13__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_14__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_14__c') != null ? (String)acf.get('OUTPUT_VALUE_14__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_15__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_15__c') != null ? (String)acf.get('OUTPUT_VALUE_15__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_16__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_16__c') != null ? (String)acf.get('OUTPUT_VALUE_16__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_17__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_17__c') != null ? (String)acf.get('OUTPUT_VALUE_17__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_18__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_18__c') != null ? (String)acf.get('OUTPUT_VALUE_18__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_19__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_19__c') != null ? (String)acf.get('OUTPUT_VALUE_19__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_20__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_20__c') != null ? (String)acf.get('OUTPUT_VALUE_20__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_21__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_21__c') != null ? (String)acf.get('OUTPUT_VALUE_21__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_22__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_22__c') != null ? (String)acf.get('OUTPUT_VALUE_22__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_23__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_23__c') != null ? (String)acf.get('OUTPUT_VALUE_23__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_24__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_24__c') != null ? (String)acf.get('OUTPUT_VALUE_24__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_25__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_25__c') != null ? (String)acf.get('OUTPUT_VALUE_25__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_26__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_26__c') != null ? (String)acf.get('OUTPUT_VALUE_26__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_27__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_27__c') != null ? (String)acf.get('OUTPUT_VALUE_27__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_28__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_28__c') != null ? (String)acf.get('OUTPUT_VALUE_28__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_29__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_29__c') != null ? (String)acf.get('OUTPUT_VALUE_29__c') : '');
        if(String.isNotBlank((String)acf.get('OUTPUT_NAME_30__c')))
            headers +=  '","' + ((String)acf.get('OUTPUT_VALUE_30__c') != null ? (String)acf.get('OUTPUT_VALUE_30__c') : '');
        headers += '"\n';
    return headers;
    }
    
    private string createCSVHeader(String ruleId){
        Account_Compute_Final__c acf = [SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.Name, Physician_2__c, Measure_Master__c, Measure_Master__r.Name, 
          OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c,
          OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,
          OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c, OUTPUT_VALUE_1__c,
          OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c,
          OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c, OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c,
          OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c
          FROM Account_Compute_Final__c WHERE Measure_Master__c =: ruleId LIMIT  1];
        String headers = '';
        headers += '"Physician"';
        headers += ',"HCP Number"';
        headers += ',"Rule Name';
        if(String.isNotBlank(acf.OUTPUT_NAME_1__c))
            headers += '","' +acf.OUTPUT_NAME_1__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_2__c))
            headers += '","' +acf.OUTPUT_NAME_2__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_3__c))
            headers += '","' +acf.OUTPUT_NAME_3__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_4__c))
            headers += '","' +acf.OUTPUT_NAME_4__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_5__c))
            headers += '","' +acf.OUTPUT_NAME_5__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_6__c))
            headers += '","' +acf.OUTPUT_NAME_6__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_7__c))
            headers += '","' +acf.OUTPUT_NAME_7__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_8__c))
            headers += '","' +acf.OUTPUT_NAME_8__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_9__c))
            headers += '","' +acf.OUTPUT_NAME_9__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_10__c))
            headers += '","' +acf.OUTPUT_NAME_10__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_11__c))
            headers += '","' +acf.OUTPUT_NAME_11__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_12__c))
            headers += '","' +acf.OUTPUT_NAME_12__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_13__c))
            headers += '","' +acf.OUTPUT_NAME_13__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_14__c))
            headers += '","' +acf.OUTPUT_NAME_14__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_15__c))
            headers += '","' +acf.OUTPUT_NAME_15__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_16__c))
            headers += '","' +acf.OUTPUT_NAME_16__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_17__c))
            headers += '","' +acf.OUTPUT_NAME_17__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_18__c))
            headers += '","' +acf.OUTPUT_NAME_18__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_19__c))
            headers += '","' +acf.OUTPUT_NAME_19__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_20__c))
            headers += '","' +acf.OUTPUT_NAME_20__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_21__c))
            headers += '","' +acf.OUTPUT_NAME_21__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_22__c))
            headers += '","' +acf.OUTPUT_NAME_22__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_23__c))
            headers += '","' +acf.OUTPUT_NAME_23__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_24__c))
            headers += '","' +acf.OUTPUT_NAME_24__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_25__c))
            headers += '","' +acf.OUTPUT_NAME_25__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_26__c))
            headers += '","' +acf.OUTPUT_NAME_26__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_27__c))
            headers += '","' +acf.OUTPUT_NAME_27__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_28__c))
            headers += '","' +acf.OUTPUT_NAME_28__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_29__c))
            headers += '","' +acf.OUTPUT_NAME_29__c;
        if(String.isNotBlank(acf.OUTPUT_NAME_30__c))
            headers += '","' +acf.OUTPUT_NAME_30__c;
        headers += '"\n';
        
        return headers;
    }
}