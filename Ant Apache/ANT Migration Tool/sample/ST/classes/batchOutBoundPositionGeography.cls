global class batchOutBoundPositionGeography implements Database.Batchable<sObject>, Database.Stateful,schedulable{
        global Integer recordsProcessed;
        global String batchID;
        global DateTime lastjobDate=null;
        global String query;
        public List<String> posCodeList=new List<String>();
        global Date today=Date.today();
        public String teamInstanceId;                            
        public set<String> Uniqueset {get;set;}
        public set<String> mkt {get;set;} 
        global List<String> teamInstList =new List<String>(); 
        global List<AxtriaSalesIQTM__Position__c> posList=new List<AxtriaSalesIQTM__Position__c>();
        public list<Custom_Scheduler__c> mktList {get;set;} 
        public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
        static boolean recursive = false;
 

    global batchOutBoundPositionGeography (String TeamInstance,Integer records,String bID){//set<String> Accountid
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        recordsProcessed=records;
        batchID=bID;
        teamInstanceId=TeamInstance;
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();

        for(Custom_Scheduler__c cs:mktList){
          if(cs.Status__c==true && cs.Schedule_date__c!=null){
            if(cs.Schedule_date__c>today.addDays(1)){
               mkt.add(cs.Marketing_Code__c);
            }else{
                //update Custom scheduler record
               cs.Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            } 
          }
        }
        System.debug('inside parameterized constructor');
        System.debug('ppp+++***'+teamInstList);
        posList=[Select Id,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,
                AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,CreatedDate,
                AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,LastModifiedDate
                from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c=:teamInstanceId and AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList];

        Uniqueset = new set<String>(); 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
        String cycle=cycleList.get(0).Name;
               cycle=cycle.substring(cycle.length() - 3);
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Position Geography' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);

        
        //Last Bacth run ID
       System.debug('record processed in parameterized constructor'+recordsProcessed);
        query = 'Select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Geography__r.Name,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, ' +
                'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,AxtriaSalesIQTM__Geography__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c, ' +
                'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Type__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__c ' +
                'FROM AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__c=:teamInstanceId and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList  ' ;
      
        
        if(lastjobDate!=null){
            query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        System.debug('query'+ query);

        //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        } 
            
    }
    
    global batchOutBoundPositionGeography (){//set<String> Accountid
        System.debug('***label'+System.label.PosGeoPositionType);
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        for(Custom_Scheduler__c cs:mktList){
          if(cs.Status__c==true && cs.Schedule_date__c!=null){
            if(cs.Schedule_date__c>today.addDays(1)){
               mkt.add(cs.Marketing_Code__c);
            }
            else{
                //update Custom scheduler record
               cs.Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            } 
          }
        }
        Uniqueset = new set<String>(); 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
        String cycle=cycleList.get(0).Name;
               cycle=cycle.substring(cycle.length() - 3);
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Position Geography' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);

        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'Position Geography';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
       
        recordsProcessed =0;
        //teamInstList=new List<String>();
        for(AxtriaSalesIQTM__Team_Instance__c ti:[Select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')]){
            teamInstList.add(ti.id);
        }
        System.debug('+++teamList+++'+teamInstList);
        teamInstanceId=teamInstList.get(0);
        posList=[Select Id,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,
         AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,CreatedDate,
         AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,LastModifiedDate
          from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c=:teamInstanceId and AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList];
        
        teamInstList.remove(0);
        query = 'Select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Geography__r.Name,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, ' +
                'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,AxtriaSalesIQTM__Geography__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c, ' +
                'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Type__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__c ' +
                'FROM AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__c=:teamInstanceId and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList  ' ;
      
        
        if(lastjobDate!=null){
            query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        System.debug('query'+ query);
       if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        } 
           
            
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
        database.executeBatch(new batchOutBoundUpdPosGeography());
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Geography__c> records){//
        // process each batch of records
       

       set<SIQ_Position_Geography_O__c> positionGeographies = new set<SIQ_Position_Geography_O__c>();
       list<SIQ_Position_Geography_O__c> updPositionGeographies = new List<SIQ_Position_Geography_O__c>();

       
        for (AxtriaSalesIQTM__Position_Geography__c geography : records) {
            //String key = geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_ID__c+'_'+geography.AxtriaSalesIQTM__Geography__r.Name+'_'+geography.AxtriaSalesIQTM__Effective_End_Date__c;
            String key=geography.id;
            if(!Uniqueset.contains(Key) && geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c =='1' && geography.AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Type__c==System.label.PosGeoGeographyType){  
               SIQ_Position_Geography_O__c positionGeography=new SIQ_Position_Geography_O__c();
               // if((geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c==System.label.PosGeoPositionType )
               //  && geography.AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Type__c==System.label.PosGeoGeographyType){
                    //|| geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c.equalsIgnorecase('District')
                 //if((geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c =='1' )
                // && geography.AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Type__c==System.label.PosGeoGeographyType){
                    if(geography.AxtriaSalesIQTM__Position__c!=null){
                        positionGeography.SIQ_POSITION_CODE__c=geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    }
                    if(geography.AxtriaSalesIQTM__Geography__c!=null && geography.AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__c!=null){
                        positionGeography.SIQ_ZIP__c=geography.AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__r.Name;
                    }
               // }
                /*else{
                    if(geography.AxtriaSalesIQTM__Position__c!=null){
                        positionGeography.SIQ_POSITION_CODE__c=geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    }
                    if(geography.AxtriaSalesIQTM__Geography__c!=null){
                    positionGeography.SIQ_ZIP__c=geography.AxtriaSalesIQTM__Geography__r.Name;  
                    }
                }*/
                if(geography.AxtriaSalesIQTM__Team_Instance__c!=null){
                    positionGeography.SIQ_Team_Instance__c=geography.AxtriaSalesIQTM__Team_Instance__r.Name; 
                } 
                if(geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c!=null){
                    positionGeography.SIQ_Team__c=geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;  
                }
                if(geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c!=null){
                    positionGeography.SIQ_Country_Code__c=geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;                                                                                                                                                                            
                }
                if(geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c!=null){
                    positionGeography.SIQ_Marketing_Code__c= geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;                                                                                                                                                                                                                 
                }
                //positionGeography.SIQ_Marketing_Code__c= geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;                                                                                                                                                                                                                 
                //positionGeography.SIQ_Country_Code__c=geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;                                                                                                                                                                        
                //positionGeography.SIQ_Team__c=geography.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
                //positionGeography.SIQ_Team_Instance__c=geography.AxtriaSalesIQTM__Team_Instance__r.Name;                        
                positionGeography.SIQ_EFFECTIVE_START_DATE__c=geography.AxtriaSalesIQTM__Effective_Start_Date__c;
                positionGeography.SIQ_EFFECTIVE_END_DATE__c=geography.AxtriaSalesIQTM__Effective_End_Date__c;
                positionGeography.SIQ_Created_Date__c=geography.CreatedDate;
                positionGeography.SIQ_Updated_Date__c=geography.LastModifiedDate;
                positionGeography.Unique_Id__c=geography.Id;
                System.debug('set of posgeo'+positionGeography);                                        
                positionGeographies.add(positionGeography);
                //system.debug('recordsProcessed+'+recordsProcessed);
                Uniqueset.add(Key);     
                recordsProcessed++;
                //comments
            }
            
        }

        for(AxtriaSalesIQTM__Position__c pos:posList){

            if(pos.AxtriaSalesIQTM__Hierarchy_Level__c!= '1' ){
                SIQ_Position_Geography_O__c positionGeography=new SIQ_Position_Geography_O__c();
                positionGeography.SIQ_POSITION_CODE__c=pos.AxtriaSalesIQTM__Client_Position_Code__c;
                positionGeography.SIQ_ZIP__c=pos.AxtriaSalesIQTM__Client_Position_Code__c;
                if(pos.AxtriaSalesIQTM__Team_Instance__c!=null){
                    positionGeography.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name; 
                } 
                if(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c!=null){
                    positionGeography.SIQ_Team__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;  
                }
                if(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c!=null){
                    positionGeography.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;                                                                                                                                                                            
                }
                if(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c!=null){
                    positionGeography.SIQ_Marketing_Code__c= pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;                                                                                                                                                                                                                 
                }
                                       
                positionGeography.SIQ_EFFECTIVE_START_DATE__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                positionGeography.SIQ_EFFECTIVE_END_DATE__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                positionGeography.SIQ_Created_Date__c=pos.CreatedDate;
                positionGeography.SIQ_Updated_Date__c=pos.LastModifiedDate;
                positionGeography.Unique_Id__c=pos.Id;
                System.debug('set of posgeo'+positionGeography);                                        
                positionGeographies.add(positionGeography);
                    

            }
        }
        updPositionGeographies.addAll(positionGeographies);
        upsert updPositionGeographies Unique_Id__c;
    }    
    global void finish(Database.BatchableContext bc){
        if(!Test.isRunningTest()){
           // recursive = true;
        // execute any post-processing operations
        System.debug('batchID***'+batchID);
        System.debug('xxxxx');
       // if(teamInstList.size()>0){
        while(teamInstList.size()>0){
          System.debug('+++***'+teamInstList);
          batchOutBoundPositionGeography ne = new batchOutBoundPositionGeography(teamInstList.get(0),recordsProcessed,batchID);
          teamInstList.remove(0);
          Database.executeBatch(ne, 200);
        }

       
        if(teamInstList.size()==0){
                System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sjob.Object_Name__c = 'Position Geography';
                //sjob.Changes__c                
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        
        Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
             }
            }
         }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position_Geography__c> posGeoList=[Select Id FROM AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update posGeoList;
         }
        }
        }
    }  
}