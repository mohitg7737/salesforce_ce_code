public with sharing class TeamInstanceProductTriggerHandler {
    public static void insertTrigger(List <Team_Instance_Product_AZ__c> sntrec1) {

    
    List<String> allIds = new List<String>();

    for(Team_Instance_Product_AZ__c ti : sntrec1)
    {
      allIds.add(ti.ID);
    }

    List<Team_Instance_Product_AZ__c> sntrec = [select id, Product_Catalogue__r.External_ID__c, Product_Catalogue__r.Product_Code__c,Team_Instance__c, Team_Instance__r.AxtriaSalesIQTM__Team__c from Team_Instance_Product_AZ__c where id IN :allIds];

    //Map<String,String> insertTeamProduct = new Map<String, String>();
    Map<String,String> insertProduct = new Map<String, String>();
    Map<String,String> insertTeam = new Map<String, String>();
    Map<String,String> updateTeamProduct = new Map<String, String>();
    List<AxtriaSalesIQTM__Team_Instance_Product__c> inserttpList = new List<AxtriaSalesIQTM__Team_Instance_Product__c>();
    Set<String> allTeamInstanceIds = new Set<String>();
    Set<String> allProductExtIds = new Set<String>();
    
    for ( Team_Instance_Product_AZ__c snttp : sntrec){
            allProductExtIds.add(snttp.Product_Catalogue__r.Product_Code__c);
            allTeamInstanceIds.add(snttp.Team_Instance__c);
            //insertTeamProduct.put(snttp.Team_Instance__c, snttp.Product_Catalogue__r.External_ID__c);
            
        }
        system.debug('+++++++++ Hey '+ allProductExtIds);

    List<AxtriaSalesIQTM__Product__c> pSalesIq = [Select AxtriaSalesIQTM__External_ID__c, id from AxtriaSalesIQTM__Product__c where AxtriaSalesIQTM__External_ID__c IN :allProductExtIds];
    List<AxtriaSalesIQTM__Team_Instance__c> teamID = [Select AxtriaSalesIQTM__Team__c, id from AxtriaSalesIQTM__Team_Instance__c where id IN :allTeamInstanceIds];

    system.debug('++++++++++++++ Hey Ext ID pSalesIq is '+ pSalesIq);
    for(AxtriaSalesIQTM__Product__c p : pSalesIq){
            insertProduct.put(p.AxtriaSalesIQTM__External_ID__c, p.id);
    }

    system.debug('++++++++++++++ Hey Ext ID pSalesIq is '+ pSalesIq);

    for(AxtriaSalesIQTM__Team_Instance__c tid : teamID){
            insertTeam.put(tid.id, tid.AxtriaSalesIQTM__Team__c);
    }
   
    for(Team_Instance_Product_AZ__c obj : sntrec){
        AxtriaSalesIQTM__Team_Instance_Product__c teamInstanceProductRec = new AxtriaSalesIQTM__Team_Instance_Product__c();
        teamInstanceProductRec.AxtriaSalesIQTM__Team_Instance__c = obj.Team_Instance__c;
        teamInstanceProductRec.AxtriaSalesIQTM__Team__c = obj.Team_Instance__r.AxtriaSalesIQTM__Team__c;
        system.debug('+++++++++++++ Hey ' + insertProduct.get(obj.Product_Catalogue__r.Product_Code__c));
        teamInstanceProductRec.AxtriaSalesIQTM__Product_Master__c = insertProduct.get(obj.Product_Catalogue__r.Product_Code__c);

        inserttpList.add(teamInstanceProductRec);
    }
          
    if(inserttpList!=null && inserttpList.size()>0){
      insert inserttpList;
    
    }     
     
   }

    public static void deleteTrigger(List <Team_Instance_Product_AZ__c> delsntrec1) {
        
        system.debug('+++++++++++++++ Hey delsntrec1 '+ delsntrec1);
        
         List<String> allIds = new List<String>();

        for(Team_Instance_Product_AZ__c ti : delsntrec1)
        {
          allIds.add(ti.ID);
        }
    
        List<Team_Instance_Product_AZ__c> delsntrec = [select id, Product_Catalogue__r.External_ID__c, Product_Catalogue__r.Product_Code__c,Team_Instance__c, Team_Instance__r.AxtriaSalesIQTM__Team__c from Team_Instance_Product_AZ__c where id IN :allIds];


      Set<String> delProductExtIds = new Set<String>();
      Set<String> allTeamInstances = new Set<String>();
      Set<String> allProTeamInstancesConcat = new Set<String>();
      List<AxtriaSalesIQTM__Team_Instance_Product__c> deltpList = new List<AxtriaSalesIQTM__Team_Instance_Product__c>();
      

      for(Team_Instance_Product_AZ__c deltip : delsntrec){
        delProductExtIds.add(deltip.Product_Catalogue__r.Product_Code__c);
        allTeamInstances.add(deltip.Team_Instance__c);
        allProTeamInstancesConcat.add(deltip.Product_Catalogue__r.Product_Code__c + '_' + deltip.Team_Instance__c);
      }
        
        system.debug('+++++++++++++ Hey '+ delProductExtIds);
        
    for(AxtriaSalesIQTM__Team_Instance_Product__c tip : [Select id, AxtriaSalesIQTM__Product_Master__r.AxtriaSalesIQTM__External_ID__c, AxtriaSalesIQTM__Team_Instance__c  from AxtriaSalesIQTM__Team_Instance_Product__c  where AxtriaSalesIQTM__Product_Master__r.AxtriaSalesIQTM__External_ID__c IN :delProductExtIds and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances])
    {
      String concatString = tip.AxtriaSalesIQTM__Product_Master__r.AxtriaSalesIQTM__External_ID__c + '_'+ tip.AxtriaSalesIQTM__Team_Instance__c;
    
    system.debug('+++++++ Concat String is '+ concatString);
    
    if(allProTeamInstancesConcat.contains(concatString))
    {
      deltpList.add(tip);
    }      
    }


    delete deltpList;

    }
}