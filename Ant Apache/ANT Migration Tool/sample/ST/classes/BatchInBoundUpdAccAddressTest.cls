@isTest
private class BatchInBoundUpdAccAddressTest {
   @testSetup 
    static void setup() {
        List<SIQ_Account_Address__c> siqaddresses = new List<SIQ_Account_Address__c>();
         List<Account> accounts = new List<Account>();
        List<AxtriaSalesIQTM__Account_Address__c> addresses = new List<AxtriaSalesIQTM__Account_Address__c>();
       
        
        // insert 10 accounts
        for (Integer i=0;i<5;i++) {
            accounts.add(new Account(name='Account '+i,AccountNumber= 'Account '+i, 
                 External_Account_Number__c='1'+i,External_Account_id__c='Account '+i, Marketing_Code__c='USA'));
        } 
        insert accounts;
        for (Integer i=0;i<5;i++) {
            siqaddresses.add(new SIQ_Account_Address__c(SIQ_Account_Number__c='Account '+i, 
                 SIQ_External_ID__c='1'+i,SIQ_Billing_Country__c='USA',SIQ_Primary_Address_Indicator__c='true',SIQ_Status__c='Active',SIQ_Latitude__c='50',SIQ_Longitude__c='49.9'));
        } 
        insert siqaddresses;
      
      //  for (SIQ_Account_Address__c siqaddress : [select SIQ_Account_Number__c,SIQ_External_ID__c,SIQ_Status__c,SIQ_Billing_Country__c from SIQ_Account_Address__c]) {
        //    addresses.add(new AxtriaSalesIQTM__Account_Address__c(AxtriaSalesIQTM__Account__c=accounts.get(0).id,External_Id__c=siqaddress.SIQ_External_ID__c,Status__c=siqaddress.SIQ_Status__c,AxtriaSalesIQTM__Country__c=siqaddress.SIQ_Billing_Country__c,AxtriaSalesIQTM__Latitude__c=50.0,AxtriaSalesIQTM__Longitude__c=50.0));
      //  }
      //  upsert addresses External_Id__c;
    }
    static testmethod void test() {        
        Test.startTest();
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchInBoundUpdAccAddress usa = new BatchInBoundUpdAccAddress();
        Id batchId = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}