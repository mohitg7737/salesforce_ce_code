global class BatchOutBoundPosEmp implements Database.Batchable<sObject>,Database.stateful,Schedulable  {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    public String UnAsgnPos='00000';
    public String UnAsgnPos1='0';
    public List<String> posCodeList=new List<String>();
    global DateTime lastjobDate=null;
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;} 
     

    global BatchOutBoundPosEmp() {
         posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        Countrymap = new map<String,String>();
        Uniqueset = new set<String>();
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         String cycle=cycleList.get(0).Name;
         cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound PosEmp Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);

        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound PosEmp Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
         sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
        
        recordsProcessed =0;
        query = 'Select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, '+
                'Country_Code__c,CreatedDate ,Employee_ID__c,Territory_ID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.name,LastModifiedDate,AxtriaSalesIQTM__Position__r.Team_Name__c,AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c  from AxtriaSalesIQTM__Position_Employee__c '+
                'where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Position__c!=null AND AxtriaSalesIQTM__Employee__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList ';
                //Event,SIQ_isActive__c,SIQ_Marketing_Code__c,SIQ_Permanent_Position_Flag__c,SIQ_Salesforce_Name__c,
         if(lastjobDate!=null){
            query = query + ' and LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundPosEmp());
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {
        List<SIQ_Position_Employee_O__c>PosEmpList = new List<SIQ_Position_Employee_O__c>();
        for(AxtriaSalesIQTM__Position_Employee__c obj : scope){
        // String key=obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+obj.AxtriaSalesIQTM__Employee__r.Name+'_'+obj.AxtriaSalesIQTM__Assignment_Status__c ;
         String key=obj.id;
          //position employee status
           if(!Uniqueset.contains(Key)){
            SIQ_Position_Employee_O__c pe = new SIQ_Position_Employee_O__c();
            pe.SIQ_ASSIGNMENT_END_DATE__c = obj.AxtriaSalesIQTM__Effective_End_Date__c;
            pe.SIQ_ASSIGNMENT_START_DATE__c = obj.AxtriaSalesIQTM__Effective_Start_Date__c;
            pe.SIQ_ASSIGNMENT_TYPE__c = obj.AxtriaSalesIQTM__Assignment_Type__c;
            //pe.SIQ_Country_Code__c = Countrymap.get(obj.Country_Code__c);
            pe.SIQ_Country_Code__c = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            pe.SIQ_Created_Date__c   = obj.CreatedDate;
            pe.SIQ_EMP_ID__c = obj.Employee_ID__c;
            //pe.SIQ_POSITION_CODE__c=obj.Territory_ID__c;
            pe.SIQ_POSITION_CODE__c=obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            pe.SIQ_Team_Instance__c = obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.name;
            pe.SIQ_Updated_Date__c= obj.LastModifiedDate;
            pe.SIQ_isActive__c= obj.AxtriaSalesIQTM__Assignment_Status__c;
            //pe.SIQ_Event__c = 
            pe.Unique_Id__c = key;
            //pe.SIQ_Marketing_Code__c=Countrymap.get(obj.Country_Code__c);
            pe.SIQ_Marketing_Code__c= obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
            
            pe.SIQ_Permanent_Position_Flag__c=obj.AxtriaSalesIQTM__Assignment_Type__c;
            pe.SIQ_Salesforce_Name__c = obj.AxtriaSalesIQTM__Position__r.Team_Name__c;
            PosEmpList.add(pe);
            Uniqueset.add(Key);    
            recordsProcessed++;
        }
        }
        upsert PosEmpList Unique_Id__c ;//SIQ_POSITION_CODE__c
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
  // Database.ExecuteBatch(new BatchOutBounUpdPositionAccount(),200);
    }
}