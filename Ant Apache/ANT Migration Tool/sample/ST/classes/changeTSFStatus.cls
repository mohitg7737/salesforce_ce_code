global class changeTSFStatus implements Database.Batchable<sObject> {
    

    List<String> teamInstanceSelected;
    String queryString;
    
    global changeTSFStatus(List<String> teamInstanceSelectedTemp)
    { 

       queryString = 'select id, Status__c from SIQ_TSF_vod_O__c where Team_Instance__c in :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_TSF_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_TSF_vod_O__c tsf : scopePacpProRecs)
        {
            tsf.Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        Batch_Integration_TSF_NEW u1 = new Batch_Integration_TSF_NEW(teamInstanceSelected);
        Database.executeBatch(u1, 2000);
    }
}