global class UpdatePositionGeography implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
    public map<String,String>Countrymap {get;set;}
    
    global UpdatePositionGeography(){//set<String> Accountid
        Countrymap = new map<String,String>();
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Position Geography' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);

        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        //Last Bacth run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'Position Geography';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        //sJob.CreatedDate = System.today();
    
        insert sJob;
        batchID = sJob.Id;
       
        recordsProcessed =0;
       query = 'Select AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c, ' +
       'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_ID__c, ' +
        'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp ' +
        'FROM AxtriaSalesIQTM__Position_Geography__c ' ;
      
        
        if(lastjobDate!=null){
            query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
        
            
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Geography__c> records){//
        // process each batch of records
       

       List<SIQ_Position_Geography_O__c> positionGeographies = new List<SIQ_Position_Geography_O__c>();
       
      for (AxtriaSalesIQTM__Position_Geography__c geography : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
               SIQ_Position_Geography_O__c positionGeography=new SIQ_Position_Geography_O__c();
           
                positionGeography.SIQ_Marketing_Code__c=Countrymap.get(geography.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
                positionGeography.SIQ_Country_Code__c=Countrymap.get(geography.AxtriaSalesIQTM__Team_Instance__r.Country_Name__c);
                positionGeography.SIQ_POSITION_CODE__c=geography.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            //  positionGeography.SIQ_ZIP__c=geography.Geography__r.Name;
                positionGeography.SIQ_EFFECTIVE_START_DATE__c=geography.AxtriaSalesIQTM__Effective_Start_Date__c;
                positionGeography.SIQ_EFFECTIVE_END_DATE__c=geography.AxtriaSalesIQTM__Effective_End_Date__c;
                positionGeography.SIQ_Created_Date__c=geography.CreatedDate;
                positionGeography.SIQ_Updated_Date__c=geography.LastModifiedDate;
                positionGeography.Unique_Id__c=geography.Id;                                        
                positionGeographies.add(positionGeography);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
     //   }
        upsert positionGeographies Unique_Id__c;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sjob.Object_Name__c = 'Position Geography';
                //sjob.Changes__c                
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        
    }  
}