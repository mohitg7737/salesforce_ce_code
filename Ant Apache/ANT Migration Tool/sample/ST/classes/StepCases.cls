public with sharing class StepCases implements Step{
    public Compute_Master__c cmpMaster;
    public StepCases(Step__c step){
        cmpMaster = [SELECT Id, Expression__c FROM Compute_Master__c WHERE Id=:step.Compute_Master__c LIMIT 1];
    }
    
    public String solveStep(Step__c step, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu , List<BU_Response__c> allBuResponses, List<String> allAggregateFunc){
        ExpressionWrapper expression = (ExpressionWrapper)JSON.deserialize(cmpMaster.Expression__c, ExpressionWrapper.class);
        String result = '';

        //system.debug('++++++++ Hey expression.ifCase' + expression.ifCase);
        result = evalIf(expression.ifCase, nameFieldMap, acfMap, bu);
        if(String.isNotBlank(result))
            return val3(result, nameFieldMap, acfMap, bu);
        for(IfCaseWrapper elifCase: expression.elif){
          //  system.debug('++++++++ Hey Trying '+ elifCase);
            result = evalIf(elifCase, nameFieldMap, acfMap, bu);
            if(String.isNotBlank(result))
                return val3(result, nameFieldMap, acfMap, bu);
        }
        if(String.isBlank(result)){
            if(expression.elseCase != null)
                result = expression.elseCase.returVal;
            else
                result = '';
        }
        //system.debug('---step--- ' + step);
        //system.debug('---result---- ' + result);
        return val3(result, nameFieldMap, acfMap, bu);
    }
    


    public String val1(String val1, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu){
        String expr0;
       // system.debug('++++++++ Hey Namefield is '+ nameFieldMap);
        //system.debug('++++++++ Hey ACF MAP  is '+ acfMap);
        //system.debug('+++++++ Hey Val1 is '+ val1);

        if(nameFieldMap.containsKey(val1.toUpperCase())){
          //  system.debug('+++++++ Inside Name field');
            expr0 = (String)bu.get(nameFieldMap.get(val1.toUpperCase()));
        }else{
            //system.debug('+++++++ Inside acfMap field');
            expr0 = (String)acfMap.get(val1);
        }

        //System.debug('++++++ Hey Value is '+ expr0);
        return expr0;
    }

    public String val2(String val2, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu){
        String expr1;
        if(nameFieldMap.containsKey(val2.toUpperCase())){
            expr1 = (String)bu.get(nameFieldMap.get(val2.toUpperCase()));
        }else if(acfMap.containsKey(val2)) {
            expr1 = (String)acfMap.get(val2);
        }else{
            expr1 = val2;
        }
        return expr1;
    }

    public String val3(String val3, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu){
        String expr1;
        if(nameFieldMap.containsKey(val3.toUpperCase())){
            expr1 = (String)bu.get(nameFieldMap.get(val3.toUpperCase()));
        }else if(acfMap.containsKey(val3)) {
            expr1 = (String)acfMap.get(val3);
        }else{
            expr1 = val3;
        }
        return expr1;
    }

    public String evalIf(IfCaseWrapper expr0, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu){
        String result = null;
        Boolean result0 = evalCondition(val1(expr0.input, nameFieldMap, acfMap, bu), val2(expr0.match, nameFieldMap, acfMap, bu), expr0.condition);
        Boolean evaluation = result0;
        for(IfCaseWrapper expr1: expr0.andOr){
            Boolean result1 = evalCondition(val1(expr1.input, nameFieldMap, acfMap, bu), val2(expr1.match, nameFieldMap, acfMap, bu), expr1.condition);
            if(expr1.type == 'or'){
                evaluation = evalOr(result0, result1);
            }else if(expr1.type == 'and'){
                evaluation = evalAnd(result0, result1);
            }
            result0 = evaluation;
        }
        if(evaluation){
            result = expr0.returVal;
        }
        return result;
    }

    public Boolean evalCondition(String expr0, String expr1, String operator){
        Boolean result = false;
        try{
            Decimal ex0 = Decimal.valueOf(expr0);
            Decimal ex1 = Decimal.valueOf(expr1);
            if(operator == 'equal'){
                if(ex0 == ex1){
                    result = true;
                }
            }else if(operator == 'not equal'){
                if(ex0 != ex1){
                    result = true;
                }
            }else if(operator == 'less than'){
                //system.debug('--ex0---' + ex0);
                //system.debug('--ex1---' + ex1);
                if(ex0 < ex1){
                    result = true;
                }
            }else if(operator == 'greater than'){
                if(ex0 > ex1){
                    result = true;
                }
            }else if(operator == 'less than or equal to'){
                if(ex0 <= ex1){
                    result = true;
                }
            }else if(operator == 'greater than or equal to'){
                if(ex0 >= ex1){
                    result = true;
                }
            }
        }catch(Exception e){
            String ex0 = expr0;
            String ex1 = expr1;
            if(operator == 'equal'){
                if(ex0 == ex1){
                    result = true;
                }
            }else if(operator == 'not equal'){
                if(ex0 != ex1){
                    result = true;
                }
            }else if(operator == 'less than'){
                if(ex0 < ex1){
                    result = true;
                }
            }else if(operator == 'greater than'){
                if(ex0 > ex1){
                    result = true;
                }
            }else if(operator == 'less than or equal to'){
                if(ex0 <= ex1){
                    result = true;
                }
            }else if(operator == 'greater than or equal to'){
                if(ex0 >= ex1){
                    result = true;
                }
            }
        }
        return result;
    }
    
    public Integer maxEval()
    {
        return 1;
    }
    
    public Boolean evalAnd(Boolean expr0, Boolean expr1){
        if(expr0 && expr1){
            return true;
        }
        return false;
    }

    public Boolean evalOr(Boolean expr0, Boolean expr1){
        if(expr0 || expr1){
            return true;
        }
        return false;
    }

    public class ExpressionWrapper{
        public IfCaseWrapper ifCase;
        public list<IfCaseWrapper> elif;
        public ElseWrapper elseCase;
    }

    public class IfCaseWrapper{
        public string input;
        public string condition;
        public string match;
        public String returVal;
        public String type;
        public list<IfCaseWrapper> andOr;
    }

    public class ElseWrapper{
        public string returVal;
    }
}