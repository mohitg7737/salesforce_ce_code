public class BulkAction {
    
    List<AxtriaSalesIQTM__Change_Request__c> records;
    List<AxtriaSalesIQTM__Change_Request__c> allRecords;
    
    public String reasonCode {get;set;}
    
    public BulkAction(ApexPages.StandardSetController controller) {
        records = (AxtriaSalesIQTM__Change_Request__c[])controller.getSelected();
        system.debug('+++++++++++++ '+records);
        List<Id> allSfdcIDs = new List<Id>();
        
        for(AxtriaSalesIQTM__Change_Request__c request : records)
        {
            allSfdcIDs.add(request.ID);
        }
        
        allRecords = [select id, AxtriaSalesIQTM__Status__c from AxtriaSalesIQTM__Change_Request__c where id in :allSfdcIDs];
    }
    
    public PageReference approveAll()
    {
        Map<String, Object> response = new Map<String, Object>();
        
            List<String> ids = new List<String>();
            
            for(AxtriaSalesIQTM__Change_Request__c changeRequest : allRecords)
            {
                if(changeRequest.AxtriaSalesIQTM__Status__c == 'Pending')
                {
                    ids.add(changeRequest.Id);
                }
            }
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        
            List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :ids ];
            for(ProcessInstanceWorkitem workItem : workItems){
              Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
              req.setWorkitemId(workItem.Id);
              //Valid values are: Approve, Reject, or Removed. 
              //Only system administrators can specify Removed.
              req.setAction('Approve');
              req.setComments('No Comment.');
              requests.add(req);
            }
            Approval.ProcessResult[] processResults = Approval.process(requests);
            PageReference pg = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list');
            pg.setRedirect(true);
            return pg;
    }
    
    public PageReference rejectRequests()
    {
        system.debug('+++++++++++ Hey Comment is '+reasonCode);
        Map<String, Object> response = new Map<String, Object>();
        
            List<String> ids = new List<String>();
            
            for(AxtriaSalesIQTM__Change_Request__c changeRequest : allRecords)
            {
                if(changeRequest.AxtriaSalesIQTM__Status__c == 'Pending')
                {
                    ids.add(changeRequest.Id);
                }
            }
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        
            List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :ids ];
            for(ProcessInstanceWorkitem workItem : workItems){
              Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
              req.setWorkitemId(workItem.Id);
              //Valid values are: Approve, Reject, or Removed. 
              //Only system administrators can specify Removed.
              req.setAction('Reject');
              req.setComments(reasonCode);
              requests.add(req);
            }
            Approval.ProcessResult[] processResults = Approval.process(requests);
            PageReference pg = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list');
            pg.setRedirect(true);
            return pg;
       
    }
}