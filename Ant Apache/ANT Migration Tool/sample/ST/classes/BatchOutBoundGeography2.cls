global class BatchOutBoundGeography2 implements Database.Batchable<sObject>,Database.stateful,schedulable {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    global Date today=Date.today();
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;}    
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
                            

    global BatchOutBoundGeography2() {
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        
        for(Custom_Scheduler__c cs:mktList){
          if(cs.Status__c==true && cs.Schedule_date__c!=null){
            if(cs.Schedule_date__c>today.addDays(1)){
               mkt.add(cs.Marketing_Code__c);
            }else{
                //update Custom scheduler record
               cs.Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            }
          }
        }
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
        String cycle=null;
         if(cycleList.size()>0){
           cycle=cycleList.get(0).Name;
           cycle=cycle.substring(cycle.length() - 3);
         }
        Countrymap = new map<String,String>();
        Uniqueset = new set<String>();                            

        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound Geography Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound Geography Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        //this.query = query; 
        //SIQ_Country_Code__c,SIQ_Customer_Class__c,SIQ_Event__c,SIQ_Marketing_Code__c,SIQ_Salesforce_Name__c,SIQ_Segment_Type__c ----???????

        /*query = 'Select Name,id,AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,Marketing_Code__c,Country_Code__c,Team__c,Team_Instance__c,ZIP__c,ZIP_Name__c,Parent_ZIP__c,CreatedDate,LastModifiedDate, ' +
                'ZIP_Type__c,AxtriaSalesIQTM__Zip_Name__c,Team_Name__c,Geography_Type1__c,Team_Instance_Name__c,Effective_End_Date__c,Geo_Code__c,Effective_Start_Date__c,Updated_Date__c,AxtriaSalesIQTM__Parent_Zip_Code__r.Name ' +
                'from AxtriaSalesIQTM__Geography__c ';*/
        query = 'Select Name,id,SIQ_Country_Code__c,SIQ_Marketing_Code__c,SIQ_Team__c,SIQ_Team_Instance__c,SIQ_Team_Name__c,SIQ_ZIP__c,SIQ_ZIP_Name__c,CreatedDate,LastModifiedDate, ' +
                'SIQ_ZIP_Type__c,SIQ_Effective_End_Date__c,SIQ_Effective_Start_Date__c,SIQ_Updated_Date__c,SIQ_Created_Date__c,SIQ_Parent_ZIP__c,Unique_Id__c ' +
                'from SIQ_Geography_Interface__c';
        
        
        if(lastjobDate!=null){
            query = query + ' Where LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and SIQ_Country_Code__c not in: mkt ' ;
        }
        System.debug('query'+ query);
        
        //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundGeography2());
    }

 global void execute(Database.BatchableContext BC, list<SIQ_Geography_Interface__c> scope) {
    list<SIQ_Geography_O__c> GeographyList = new list<SIQ_Geography_O__c>();

    Date myDateTime = Date.newInstance(4000, 12, 31);
    List<AxtriaSalesIQTM__Team__c> TeamList=[Select Id, Name from AxtriaSalesIQTM__Team__c];
    System.debug('teamList'+TeamList);
    for(SIQ_Geography_Interface__c pos : scope){
        //String key = pos.Team_Instance_Name__c+'_'+pos.Geo_Code__c;
        String key = pos.SIQ_Team_Name__c+'_'+pos.id;
      if(!Uniqueset.contains(Key)){
        SIQ_Geography_O__c obj = new SIQ_Geography_O__c();  
        obj.SIQ_Marketing_Code__c= pos.SIQ_Marketing_Code__c;//pos.AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c ;            
        obj.SIQ_Country_Code__c= pos.SIQ_Country_Code__c;//pos.AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
        obj.SIQ_Team__c=pos.SIQ_Team_Name__c;
        obj.SIQ_Team_Instance__c=pos.SIQ_Team_Instance__c;
        obj.SIQ_ZIP__c=pos.SIQ_ZIP__c ;
        obj.SIQ_ZIP_Name__c=pos.SIQ_ZIP_Name__c ;
        obj.SIQ_Parent_ZIP_c__c=pos.SIQ_Parent_ZIP__c;// pos.AxtriaSalesIQTM__Parent_Zip_Code__r.Name;
        obj.SIQ_ZIP_Type__c= pos.SIQ_ZIP_Type__c;//pos.AxtriaSalesIQTM__Zip_Name__c;
        obj.SIQ_Created_Dat__c=pos.CreatedDate;
        obj.SIQ_Updated_Dat__c =pos.LastModifiedDate;
        obj.SIQ_Effective_Start_Dat__c=pos.CreatedDate;
        obj.SIQ_Effective_End_Dat__c=myDateTime ;
        obj.Unique_Id__c=pos.SIQ_Team_Name__c+'_'+pos.id;
        GeographyList.add(obj);
        recordsProcessed++;
        Uniqueset.add(Key);                    
        } 
    }
    Upsert GeographyList Unique_Id__c;
        
}

    global void finish(Database.BatchableContext BC) {
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
       //Database.ExecuteBatch( new BatchOutBoundPositionProduct(),200);
        Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
             }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Geography__c> GeoList=[Select Id FROM AxtriaSalesIQTM__Geography__c where Country_Code__c in: updMkt]; 
            update GeoList;
        }
    }
}