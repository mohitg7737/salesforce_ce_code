@isTest
public class CallPlanTestDataFactory {
    /*
    public static void createReadOnlySettings(){
        AxtriaSalesIQTM__ReadOnlyApplication__c setting  = new AxtriaSalesIQTM__ReadOnlyApplication__c();
        setting.AxtriaSalesIQTM__Alignment__c = false;
        setting.AxtriaSalesIQTM__CallPlan__c = false;
        setting.AxtriaSalesIQTM__Roster__c = false;
        insert setting;
    }
    
    public static AxtriaSalesIQTM__User_Access_Permission__c createUserAccessPerm(AxtriaSalesIQTM__Position__c pos, User userObj, AxtriaSalesIQTM__Team_Instance__c teamIntance ){
        AxtriaSalesIQTM__User_Access_Permission__c objUserAccessPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
        objUserAccessPerm.AxtriaSalesIQTM__Position__c = pos.id;
        objUserAccessPerm.name='access';
        objUserAccessPerm.AxtriaSalesIQTM__Team_Instance__c =teamIntance.id;   
        if(userObj != null){
            objUserAccessPerm.AxtriaSalesIQTM__User__c = userObj.id;
        }
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = true;
        
        return objUserAccessPerm;
    }
    
    public static AxtriaSalesIQTM__CR_Employee_Assignment__c cr_EmployeeAssignment(AxtriaSalesIQTM__Change_Request__c reqId, AxtriaSalesIQTM__Employee__c emp, AxtriaSalesIQTM__Position__c pos ){
       
        AxtriaSalesIQTM__CR_Employee_Assignment__c empAssigment = new AxtriaSalesIQTM__CR_Employee_Assignment__c();
        empAssigment.Name = 'Test';
        empAssigment.AxtriaSalesIQTM__Position_ID__c = pos.id;
        empAssigment.AxtriaSalesIQTM__Change_Request_ID__c = reqId.id;   
        empAssigment.AxtriaSalesIQTM__Employee_ID__c = emp.id;
        empAssigment.AxtriaSalesIQTM__Is_Active__c = true;
        
        return empAssigment;
        
      }
    
    // Arun Kumar added new parameter 'team Instace' in order to pass call plan test cases
    //Make the changes if any one is using   
    public static AxtriaSalesIQTM__Change_Request__c createChangeRequest(User usr, AxtriaSalesIQTM__Position__c pos, AxtriaSalesIQTM__Team_Instance__c TeamInstance, string status ){
        AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
        objChangeRequest.AxtriaSalesIQTM__Approver1__c = usr.id;
        objChangeRequest.AxtriaSalesIQTM__Destination_Position__c = pos.id;
        objChangeRequest.AxtriaSalesIQTM__Source_Position__c = pos.id;
   
         objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c = '11788';
        objChangeRequest.AxtriaSalesIQTM__Status__c = status;
         objChangeRequest.OwnerID  =usr.id;
         objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = TeamInstance.id;
         objChangeRequest.AxtriaSalesIQTM__scenario_name__c='Scenario';
         objChangeRequest.AxtriaSalesIQTM__Execution_Date__c = date.today();
        
        return objChangeRequest;
    }
    
    
     public static AxtriaSalesIQTM__Change_Request__c createChangeRequestChild(AxtriaSalesIQTM__Change_Request__c Parent, User usr, AxtriaSalesIQTM__Position__c pos, AxtriaSalesIQTM__Position__c Dstination, AxtriaSalesIQTM__Team_Instance__c TeamInstance, string status ){
        AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
        objChangeRequest.AxtriaSalesIQTM__Approver1__c = usr.id;
        objChangeRequest.AxtriaSalesIQTM__Destination_Position__c = Dstination.id;
         objChangeRequest.AxtriaSalesIQTM__Source_Position__c = pos.id;
   
         objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c = '11788,';
        objChangeRequest.AxtriaSalesIQTM__Status__c = status;
         objChangeRequest.OwnerID  =usr.id;
         objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = TeamInstance.id;
         objChangeRequest.AxtriaSalesIQTM__scenario_name__c='Scenario';
         objChangeRequest.AxtriaSalesIQTM__Parent_Change_Request_ID__c = Parent.id;
        return objChangeRequest;
    }
    
    
    public static list<AxtriaSalesIQTM__Change_Request__c> createChangeRequestDestSourc(User usr, AxtriaSalesIQTM__Position__c source, AxtriaSalesIQTM__Position__c dest, AxtriaSalesIQTM__Change_Request_Type__c recType, AxtriaSalesIQTM__Change_Request_Approver_Detail__c objCRAD, AxtriaSalesIQTM__Position_Team_Instance__c posteamInsSource, AxtriaSalesIQTM__Position_Team_Instance__c posteamInsDestination, AxtriaSalesIQTM__Team_Instance__c TeamInstance){
        list<AxtriaSalesIQTM__Change_Request__c> listChangeRequest = new list<AxtriaSalesIQTM__Change_Request__c>();
        Integer iCount;
        for(iCount=1;iCount<4;iCount++){
            AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
            objChangeRequest.AxtriaSalesIQTM__Approver1__c = usr.id;
            objChangeRequest.AxtriaSalesIQTM__Destination_Position__c = dest.id;
            objChangeRequest.AxtriaSalesIQTM__Source_Position__c = source.id;
            objChangeRequest.AxtriaSalesIQTM__RecordTypeID__c = recType.id;
         
            objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c='11788';
            objChangeRequest.AxtriaSalesIQTM__status__c = 'Pending';
            objChangeRequest.AxtriaSalesIQTM__Request_Type_Change__c = recType.AxtriaSalesIQTM__CR_Type_Name__c;
            objChangeRequest.AxtriaSalesIQTM__CurrentApprover__c = objCRAD.id;
            objChangeRequest.AxtriaSalesIQTM__Source_Positions__c = posteamInsSource.id;
            objChangeRequest.AxtriaSalesIQTM__Destination_Positions__c = posteamInsDestination.id;
            objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = TeamInstance.id;
            
            listChangeRequest.add(objChangeRequest);
        }
        system.debug('======listChangeRequest---------'+listChangeRequest);
        
        return listChangeRequest;
    }
    
    public static AxtriaSalesIQTM__Change_Request_Approver_Detail__c createCRAD(AxtriaSalesIQTM__Change_Request__c crObj, AxtriaSalesIQTM__Position__c pos, User usr){
        AxtriaSalesIQTM__Change_Request_Approver_Detail__c obj = new AxtriaSalesIQTM__Change_Request_Approver_Detail__c();
        obj.AxtriaSalesIQTM__Approver_Position__c = pos.id;
        obj.AxtriaSalesIQTM__Change_Request__c = crObj.id;
        obj.AxtriaSalesIQTM__Approver_User__c = usr.id;
        obj.AxtriaSalesIQTM__Status__c = crObj.AxtriaSalesIQTM__status__c;
        obj.AxtriaSalesIQTM__Comments__c = 'CRAD created';       
        return obj;
    }
   
    public static AxtriaSalesIQTM__Change_Request_Approver_Detail__c createCRADsecond( AxtriaSalesIQTM__Position__c pos, User usr){
        AxtriaSalesIQTM__Change_Request_Approver_Detail__c obj = new AxtriaSalesIQTM__Change_Request_Approver_Detail__c();
        obj.AxtriaSalesIQTM__Approver_Position__c = pos.id;
        obj.AxtriaSalesIQTM__Approver_User__c = usr.id;
        obj.AxtriaSalesIQTM__Comments__c = 'CRAD created';       
        return obj;
    }
    
    public static list<AxtriaSalesIQTM__CR_Call_Plan__c> createCRCallPlan(AxtriaSalesIQTM__Change_Request__c crObj, Account acc, string status){
        list<AxtriaSalesIQTM__CR_Call_Plan__c> listCrCallPlan = new list<AxtriaSalesIQTM__CR_Call_Plan__c>();
        Integer iCount;
        for(iCount=1;iCount<2;iCount++){
            AxtriaSalesIQTM__CR_Call_Plan__c objChangeRequest = new AxtriaSalesIQTM__CR_Call_Plan__c();
            objChangeRequest.Name = 'Test';
            objChangeRequest.AxtriaSalesIQTM__Change_Request_ID__c = crObj.id;
            objChangeRequest.AxtriaSalesIQTM__Account__c = acc.id;
            objChangeRequest.AxtriaSalesIQTM__After_Value_Metric1__c = 2;
            objChangeRequest.AxtriaSalesIQTM__After_Value_Metric2__c = 2;
            objChangeRequest.AxtriaSalesIQTM__Before_Value_Metric1__c = 2;
            objChangeRequest.AxtriaSalesIQTM__Before_Value_Metric2__c = 2;
            objChangeRequest.AxtriaSalesIQTM__Comments__c = 'Test';
            objChangeRequest.AxtriaSalesIQTM__isExcludedCallPlan__c = false;
            objChangeRequest.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
            objChangeRequest.AxtriaSalesIQTM__Metric2__c = 'Methonol-Disprin';
            objChangeRequest.AxtriaSalesIQTM__ReasonAddDrop__c = 'Rep retired';
            objChangeRequest.AxtriaSalesIQTM__Rep_Goal_After__c = '23';
            objChangeRequest.AxtriaSalesIQTM__Rep_Goal_Before__c = '22';
            objChangeRequest.AxtriaSalesIQTM__Rep_Goal_Updated__c = '23';
            objChangeRequest.AxtriaSalesIQTM__Rep_Reason_After__c ='Rep Died';
            objChangeRequest.AxtriaSalesIQTM__Rep_Reason_Before__c ='Rep Died';
            objChangeRequest.AxtriaSalesIQTM__Sales_Direction_After__c = 'Methonol-Disprin';
            objChangeRequest.AxtriaSalesIQTM__Sales_Direction_Before__c = 'Disprin-Methonol';
            objChangeRequest.AxtriaSalesIQTM__Status__c = status;
            listCrCallPlan.add(objChangeRequest);
        }
        
        return listCrCallPlan;
    }
    
     public static AxtriaSalesIQTM__CR_Position__c createCRPosition(AxtriaSalesIQTM__Change_Request__c chReq){
        AxtriaSalesIQTM__CR_Position__c objCRPosition = new AxtriaSalesIQTM__CR_Position__c();
        objCRPosition.AxtriaSalesIQTM__Change_Request__c = chReq.id;
        
        return objCRPosition;
        
      
      }
    
    //TYpe = Zip Aligned or Direct Aligned
    //Added new parameter AccountType in order to handle Acount type- Physician or Hospital
    public static Account CreateAccount(string Type, string PostalCode,string AccountType){
        Account obj = new Account();
        obj.name = '11788';
        obj.AxtriaSalesIQTM__FirstName__c = 'Abby';
        obj.BillingCity='NJ';
        obj.BillingState='NY';
        
        obj.AccountNumber='11788';
        //obj.type ='Physician';
        obj.type =AccountType;
        obj.AxtriaSalesIQTM__AccountType__c =AccountType;
        obj.BillingPostalCode=PostalCode;
        obj.AxtriaSalesIQTM__Speciality__c='11788Oncology';
        obj.AxtriaSalesIQTM__Alignment_Type__c = Type;
        obj.BillingLatitude  =40.5571389900;
        obj.BillingLongitude = -74.2123329900;
        
        
        createaccount2(Type, PostalCode);
        //removed comments here
        decimal xmin, decimal ymin, decimal xmax, decimal ymax
        '-74.2123329900,40.5571389900,-71.8561499800,41.2072030200';
        //removed comments here
        return obj;
    }
    
    private static void createaccount2(string Type, string PostalCode){
        Account obj = new Account();
        obj.name = '11788';
        obj.AxtriaSalesIQTM__FirstName__c = '11788';
        obj.BillingCity='NJ';
        obj.BillingState='NY';
        obj.AccountNumber='11788';
        obj.type ='Physician';
        obj.BillingPostalCode=PostalCode;
        obj.AxtriaSalesIQTM__Speciality__c='Oncology11788';
        obj.AxtriaSalesIQTM__Alignment_Type__c = Type;
        obj.BillingLatitude  =41.2072030200;
        obj.BillingLongitude = -71.8561499800;
        insert obj;
    }
    
    // Type are:- Zip,Account,Hybrid
    public static AxtriaSalesIQTM__Team_Instance_Account__c createTeamInstanceAccount(Account Acc,AxtriaSalesIQTM__Team_Instance__c TeamInstance){
        AxtriaSalesIQTM__Team_Instance_Account__c obj = new AxtriaSalesIQTM__Team_Instance_Account__c();
        //obj.Name = 'Team Instnce Acocunt';
        obj.AxtriaSalesIQTM__Account_ID__c = Acc.id;
        obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Metric1__c = 2.010000000000;
        obj.AxtriaSalesIQTM__Metric2__c  =29.000000000000;
        obj.AxtriaSalesIQTM__Metric3__c =2.000000000000;
        obj.AxtriaSalesIQTM__Metric4__c  =1.431479545000;
        obj.AxtriaSalesIQTM__Metric5__c  =1.490000000000;
        obj.AxtriaSalesIQTM__Metric6__c = 2.010000000000;
        obj.AxtriaSalesIQTM__Metric7__c  =29.000000000000;
        obj.AxtriaSalesIQTM__Metric8__c =2.000000000000;
        obj.AxtriaSalesIQTM__Metric9__c  =1.431479545000;
        obj.AxtriaSalesIQTM__Metric10__c  =1.490000000000;
        return obj; 
        
    }
    
    
    public static AxtriaSalesIQTM__Position_Team_Instance__c CreatePosTeamInstance(AxtriaSalesIQTM__Position__c pos, AxtriaSalesIQTM__Position__c ParentPos,AxtriaSalesIQTM__Team_Instance__c TeamInstance){
        AxtriaSalesIQTM__Position_Team_Instance__c obj = new AxtriaSalesIQTM__Position_Team_Instance__c();
        //obj.name='Pos Team Instance';
        obj.AxtriaSalesIQTM__Position_ID__c = pos.id;
        if(ParentPos!=null)
            obj.AxtriaSalesIQTM__Parent_Position_ID__c = ParentPos.id;
        obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Team_Instance_ID__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__X_Max__c=-72.6966429900;
        obj.AxtriaSalesIQTM__X_Min__c=-73.9625820000;
        obj.AxtriaSalesIQTM__Y_Max__c=40.9666490000;
        obj.AxtriaSalesIQTM__Y_Min__c=40.5821279800;     
        
        return obj;
    }
    
    public static AxtriaSalesIQTM__Position_Account__c createPositionAccount(Account Acc, AxtriaSalesIQTM__Position__c Position, AxtriaSalesIQTM__Team_Instance__c TeamInstance, AxtriaSalesIQTM__Position_Team_Instance__c PosTeamInstance, AxtriaSalesIQTM__Team_Instance_Account__c TeamInstanceAcc){
        AxtriaSalesIQTM__Position_Account__c obj = new AxtriaSalesIQTM__Position_Account__c();
        //obj.Name='PosAccount';
        obj.AxtriaSalesIQTM__Position__c = Position.id;
        obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__Position_Team_Instance__c = PosTeamInstance.id;
        obj.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.AxtriaSalesIQTM__Change_Status__c = 'Approved';
        return obj;
        
    }
    
     public static AxtriaSalesIQTM__Position_Account__c createPositionAccountPending(Account Acc, AxtriaSalesIQTM__Position__c Position, AxtriaSalesIQTM__Team_Instance__c TeamInstance, AxtriaSalesIQTM__Position_Team_Instance__c PosTeamInstance, AxtriaSalesIQTM__Team_Instance_Account__c TeamInstanceAcc){
        AxtriaSalesIQTM__Position_Account__c obj = new AxtriaSalesIQTM__Position_Account__c();
        //obj.Name='PosAccount';
        obj.AxtriaSalesIQTM__Position__c = Position.id;
        obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__Position_Team_Instance__c = PosTeamInstance.id;
        obj.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.AxtriaSalesIQTM__Change_Status__c = 'Pending';
        return obj;
        
    }
    
    public static AxtriaSalesIQTM__Geography__c createGeography(string ziptype,AxtriaSalesIQTM__Geography__c geo,string name){
        AxtriaSalesIQTM__Geography__c obj = new AxtriaSalesIQTM__Geography__c();
        obj.name = name;
        obj.AxtriaSalesIQTM__City__c='Test 11788';
        obj.AxtriaSalesIQTM__State__c = '11788';
        obj.AxtriaSalesIQTM__Centroid_Latitude__c = 0.0;
        obj.AxtriaSalesIQTM__Centroid_Longitude__c = 0.0;
        obj.AxtriaSalesIQTM__Zip_Type__c=ziptype;
        if(geo!=null)
        obj.AxtriaSalesIQTM__Parent_Zip_Code__c=geo.id;
        return obj;     
    }
    
    public static AxtriaSalesIQTM__Team_Instance_Geography__c CreateTeamInstanceGeoGraphy(AxtriaSalesIQTM__Geography__c geo, AxtriaSalesIQTM__Team_Instance__c teamInstance){
        AxtriaSalesIQTM__Team_Instance_Geography__c obj = new AxtriaSalesIQTM__Team_Instance_Geography__c();
        //obj.Name ='TeamInstacneGeo';
        obj.AxtriaSalesIQTM__Geography__c  =geo.id;
        obj.AxtriaSalesIQTM__Team_Instance__c = teamInstance.id;
        obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Metric1__c = 2.010000000000;
        obj.AxtriaSalesIQTM__Metric2__c  =29.000000000000;
        obj.AxtriaSalesIQTM__Metric3__c =2.000000000000;
        obj.AxtriaSalesIQTM__Metric4__c  =1.431479545000;
        obj.AxtriaSalesIQTM__Metric5__c  =1.490000000000;
        obj.AxtriaSalesIQTM__Metric6__c = 2.010000000000;
        obj.AxtriaSalesIQTM__Metric7__c  =29.000000000000;
        obj.AxtriaSalesIQTM__Metric8__c =2.000000000000;
        obj.AxtriaSalesIQTM__Metric9__c  =1.431479545000;
        obj.AxtriaSalesIQTM__Metric10__c  =1.490000000000;
        return obj;
    }
    
    public static list<AxtriaSalesIQTM__CR_Employee_Assignment__c>  CreateEmployeeAssignment(AxtriaSalesIQTM__Change_Request__c objCr, AxtriaSalesIQTM__Employee__c objEmp, AxtriaSalesIQTM__Position__c pos){
        Integer iCount;
        list<AxtriaSalesIQTM__CR_Employee_Assignment__c>  objList = new list<AxtriaSalesIQTM__CR_Employee_Assignment__c>();
        for(iCount = 0; iCount<=1 ; iCount++){
            AxtriaSalesIQTM__CR_Employee_Assignment__c obj = new AxtriaSalesIQTM__CR_Employee_Assignment__c();
            obj.name = 'Test';
            obj.AxtriaSalesIQTM__Change_Request_ID__c = objCr.id;
            obj.AxtriaSalesIQTM__Employee_ID__c = objEmp.id;
            obj.AxtriaSalesIQTM__Position_ID__c = pos.id;
            obj.AxtriaSalesIQTM__After_Assignment_Type__c = ' ' ;
            obj.AxtriaSalesIQTM__Before_Assignment_Type__c = ' ' ;           
            obj.AxtriaSalesIQTM__Before_Effective_End_Date__c = Date.today();
            obj.AxtriaSalesIQTM__Before_Effective_Start_Date__c = Date.today();
            obj.AxtriaSalesIQTM__After_Effective_End_Date__c = Date.today();
            obj.AxtriaSalesIQTM__After_Effective_Start_Date__c = Date.today();
            obj.AxtriaSalesIQTM__is_Active__c = true;
            objList.add(obj);
        }
        
        return objList;
    }
    
    
    public static AxtriaSalesIQTM__Position_Geography__c createPosGeography(AxtriaSalesIQTM__Geography__c geo, AxtriaSalesIQTM__Position__c pos,AxtriaSalesIQTM__Team_Instance_Geography__c TeamInstanceGeo,AxtriaSalesIQTM__Position_Team_Instance__c PosteamInstance, AxtriaSalesIQTM__Team_Instance__c TeamInstance ){
        AxtriaSalesIQTM__Position_Geography__c obj  = new AxtriaSalesIQTM__Position_Geography__c();
        //obj.name = 'name';
        obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Geography__c = geo.id;
        obj.AxtriaSalesIQTM__Position__c = pos.id;
        obj.AxtriaSalesIQTM__Team_Instance__c =TeamInstance.id ;
        obj.AxtriaSalesIQTM__Team_Instance_Geography__c = TeamInstanceGeo.id;
        obj.AxtriaSalesIQTM__Position_Team_Instance__c  =PosteamInstance.id;
        obj.AxtriaSalesIQTM__Change_Status__c = 'Approved';
        return obj;     
        
    }
    
    
    public static AxtriaSalesIQTM__Position__c createRegionPosition(AxtriaSalesIQTM__Team__c Team){
        
        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Test Region';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Test Region';
       
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30001';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30001';
        pos.AxtriaSalesIQTM__Position_Type__c='Region';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        
        return pos;
    }
    
    
    public static AxtriaSalesIQTM__Position__c createPositionParent(AxtriaSalesIQTM__Position__c region, AxtriaSalesIQTM__Team__c Team){
        
        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'New York, NY';
        if(region!=null)
            pos.AxtriaSalesIQTM__Parent_Position__c = region.id;
        pos.AxtriaSalesIQTM__Client_Territory_Name__c='New York, NY';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'P10';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30000';
        pos.AxtriaSalesIQTM__Position_Type__c='District';
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
         pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        
        
        return pos;
    }
    
    public static AxtriaSalesIQTM__Position__c createPosition(AxtriaSalesIQTM__Position__c p1,AxtriaSalesIQTM__Team__c Team){
        
        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Long Island East, NY';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Long Island East, NY';
        if(p1!=null){
            pos.AxtriaSalesIQTM__Parent_Position__c = p1.ID;
        }
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
        pos.AxtriaSalesIQTM__X_Max__c=-72.6966429900;
        pos.AxtriaSalesIQTM__X_Min__c=-73.9625820000;
        pos.AxtriaSalesIQTM__Y_Max__c=40.9666490000;
        pos.AxtriaSalesIQTM__Y_Min__c=40.5821279800;
        
        return pos;
    }
    
    
    
    
    
     public static AxtriaSalesIQTM__Position__c createDestinationPosition(AxtriaSalesIQTM__Position__c p1,AxtriaSalesIQTM__Team__c Team){
        
        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Test Destination Terr';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Test Destination Terr';
        if(p1!=null){
            pos.AxtriaSalesIQTM__Parent_Position__c = p1.ID;
         }
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30002';
        pos.AxtriaSalesIQTM__Position_Type__c='Territory';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
        pos.AxtriaSalesIQTM__X_Max__c=-71.8561499800;
        pos.AxtriaSalesIQTM__X_Min__c=-73.6513570200;
        pos.AxtriaSalesIQTM__Y_Max__c=41.2072030200;
        pos.AxtriaSalesIQTM__Y_Min__c=40.5729730200;
        
        return pos;
    }
    
    public static AxtriaSalesIQTM__Employee__c createEmployee(){
        AxtriaSalesIQTM__Employee__c obj = new AxtriaSalesIQTM__Employee__c();
        obj.Name = 'Test Empl';
        obj.AxtriaSalesIQTM__Employee_ID__c = '12345';
        obj.AxtriaSalesIQTM__Last_Name__c = 'Empl';
        obj.AxtriaSalesIQTM__FirstName__c = 'Test';
        obj.AxtriaSalesIQTM__Gender__c = 'M';
        obj.AxtriaSalesIQTM__Change_status__c = 'Pending';
        return obj;
    }
    
    public static AxtriaSalesIQTM__Position_Employee__c createPosEmploye(AxtriaSalesIQTM__Position__c p1,AxtriaSalesIQTM__Employee__c emp){
        AxtriaSalesIQTM__Position_Employee__c obj = new AxtriaSalesIQTM__Position_Employee__c();
        obj.name = 'Test';
        obj.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        obj.AxtriaSalesIQTM__Position__c = p1.id;
        obj.AxtriaSalesIQTM__Employee__c = emp.id;
        obj.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-1;
        obj.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        
        return obj;
    }
    
    public static AxtriaSalesIQTM__Team__c createTeam(String Type){
        AxtriaSalesIQTM__Team__c objTeam = new AxtriaSalesIQTM__Team__c();
        objTeam.Name='HTN';
        objTeam.AxtriaSalesIQTM__Type__c =Type; 
        
        return objTeam;
    }    
   
    
    //Type will defins wheather it is Zipp Terr or Account Terr Team Instance
    public Static AxtriaSalesIQTM__Team_Instance__c CreateTeamInstance(AxtriaSalesIQTM__Team__c Team,string Type){
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'HTN_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Team_Cycle_Name__c = 'HTN_Q1_2016';
        objTeamInstance.AxtriaSalesIQTM__Team__c = Team.id;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = Type;
        objTeamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        return objTeamInstance;
    }
    
    
    public static AxtriaSalesIQTM__Team_Instance_Object_Attribute__c createObjectAttributes(AxtriaSalesIQTM__Team_Instance__c TeamInstance, string ObjectName){
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c obj = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        obj.AxtriaSalesIQTM__Attribute_API_Name__c = 'Segment1__c';
        obj.AxtriaSalesIQTM__Attribute_Display_Name__c='Test';
        obj.AxtriaSalesIQTM__isEnabled__c = true;
        //obj.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Team_Instance_Geography__c';
        obj.AxtriaSalesIQTM__Object_Name__c=ObjectName;
        //obj.AxtriaSalesIQTM__Display_Name__c='Test display';
        obj.AxtriaSalesIQTM__Team_Instance__c=  TeamInstance.id;
        obj.AxtriaSalesIQTM__isStatic__c  =true;
        obj.AxtriaSalesIQTM__Data_Type__c='';
        return obj;
    }
    
    public static AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c createAttributesDetails(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttributes, AxtriaSalesIQTM__Team_Instance__c TeamInstance){
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c obj = new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
        obj.AxtriaSalesIQTM__isActive__c  =true;
        obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = objAttributes.id;
        obj.AxtriaSalesIQTM__Object_Value_Name__c = 'Metrci1__c';
        obj.AxtriaSalesIQTM__Object_Value_Seq__c='1';
        return obj;
    }
    
    public static AxtriaSalesIQTM__Change_Request_Type__c CreateRequestType(string reqType){
        AxtriaSalesIQTM__Change_Request_Type__c obj = new AxtriaSalesIQTM__Change_Request_Type__c();
        obj.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
        obj.AxtriaSalesIQTM__CR_Type_Name__c = reqType;
        return obj;
    }
    
    public static AxtriaSalesIQTM__Change_Request_Type__c CreateZipRequestType(){
        AxtriaSalesIQTM__Change_Request_Type__c obj = new AxtriaSalesIQTM__Change_Request_Type__c();
        obj.AxtriaSalesIQTM__Change_Request_Code__c='Request  Code';
        obj.AxtriaSalesIQTM__CR_Type_Name__c='ZIP Movement';
        return obj;
    }
    
     public static AxtriaSalesIQTM__Change_Request_Type__c CreateAccountRequestType(){
        AxtriaSalesIQTM__Change_Request_Type__c obj = new AxtriaSalesIQTM__Change_Request_Type__c();
        obj.AxtriaSalesIQTM__Change_Request_Code__c='Request  Code';
        obj.AxtriaSalesIQTM__CR_Type_Name__c='Account Movement';
        return obj;
    }
    
     public static AxtriaSalesIQTM__Change_Request_Type__c CreateCallPlanRequestType(){
        AxtriaSalesIQTM__Change_Request_Type__c obj = new AxtriaSalesIQTM__Change_Request_Type__c();
        obj.AxtriaSalesIQTM__Change_Request_Code__c='Request  Code';
        obj.AxtriaSalesIQTM__CR_Type_Name__c='Call_Plan_Change';
        return obj;
    }
    
    public static AxtriaSalesIQTM__Change_Request_Type__c CreateScenarioRequestType(){
        AxtriaSalesIQTM__Change_Request_Type__c obj = new AxtriaSalesIQTM__Change_Request_Type__c();
        obj.AxtriaSalesIQTM__Change_Request_Code__c='Request  Code';
        obj.AxtriaSalesIQTM__CR_Type_Name__c='Scenario';
        return obj;
    }
    
    
    public static list<AxtriaSalesIQTM__CIM_Config__c> createCIMCOnfig(AxtriaSalesIQTM__Change_Request_Type__c Type,AxtriaSalesIQTM__Team_Instance__c TeamInstance, string ObjectName){
        list<AxtriaSalesIQTM__CIM_Config__c> lstCIM = new list<AxtriaSalesIQTM__CIM_Config__c>();
        Integer iCount;
        for(iCount=1;iCount<=10;iCount++){
            AxtriaSalesIQTM__CIM_Config__c obj = new AxtriaSalesIQTM__CIM_Config__c();
            obj.Name = 'Test CIM'+iCount;
            obj.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
            obj.AxtriaSalesIQTM__Attribute_API_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Metric'+iCount+'__c';
            obj.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Test Display'+iCount;
            obj.AxtriaSalesIQTM__Change_Request_Type__c = Type.id;
            obj.AxtriaSalesIQTM__Enable__c = true;
            obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
            obj.AxtriaSalesIQTM__Object_Name__c = ObjectName;
            obj.AxtriaSalesIQTM__Threshold_Min__c = '-40';
            obj.AxtriaSalesIQTM__Threshold_Max__c = '40';
            obj.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-20';
            obj.AxtriaSalesIQTM__Threshold_Warning_Max__c = '20';
            lstCIM.add(obj);
        }
        system.debug('lstCIM is '+lstCIM);
        return lstCIM;
    }
    
    public static list<AxtriaSalesIQTM__CIM_Change_Request_Impact__c> createCIMchangeRequestImpact(AxtriaSalesIQTM__Change_Request__c crObj, AxtriaSalesIQTM__CIM_Config__c objCIMconfig, AxtriaSalesIQTM__Position_Team_Instance__c posTeamInstance){
        list<AxtriaSalesIQTM__CIM_Change_Request_Impact__c> lstCIMchangeRequestImpact = new list<AxtriaSalesIQTM__CIM_Change_Request_Impact__c>();
        Integer iCount;
        for(iCount=1;iCount<2;iCount++){
            AxtriaSalesIQTM__CIM_Change_Request_Impact__c obj = new AxtriaSalesIQTM__CIM_Change_Request_Impact__c();
            obj.AxtriaSalesIQTM__CIM_Config__c = objCIMconfig.id; 
            obj.AxtriaSalesIQTM__Position_Team_Instance__c = crObj.AxtriaSalesIQTM__Source_Positions__c;
            obj.AxtriaSalesIQTM__Change_Request__c = crObj.id;
            obj.AxtriaSalesIQTM__Impact__c = '-2.24';
            lstCIMchangeRequestImpact.add(obj);
        }
        system.debug('lstCIMchangeRequestImpact is '+lstCIMchangeRequestImpact);
        return lstCIMchangeRequestImpact;
    }
    
    public static list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> createCIMMetricSummary(list<AxtriaSalesIQTM__CIM_Config__c> CIMConfig,AxtriaSalesIQTM__Position_Team_Instance__c PosTeamInstance, AxtriaSalesIQTM__Team_Instance__c TeamInstance){
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> lstCIMSummary = new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        Integer iCount;
        for(AxtriaSalesIQTM__CIM_Config__c COnfig:CIMConfig){
            AxtriaSalesIQTM__CIM_Position_Metric_Summary__c obj = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
            obj.AxtriaSalesIQTM__Approved__c = '100';
            obj.AxtriaSalesIQTM__Original__c = '100';
            obj.AxtriaSalesIQTM__Proposed__c = '100';
            obj.AxtriaSalesIQTM__CIM_Config__c = COnfig.id;
            obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
            obj.AxtriaSalesIQTM__Position_Team_Instance__c = PosTeamInstance.id;
            lstCIMSummary.add(obj);
        }
        return lstCIMSummary;
    }
    
    
    public static AxtriaSalesIQTM__Position_Account_Call_Plan__c CreatePosAccCallPlan(Account Acc, AxtriaSalesIQTM__Position__c Pos,AxtriaSalesIQTM__Team_Instance__c TeamInstance, AxtriaSalesIQTM__Position_Team_Instance__c PosTeamInstance, AxtriaSalesIQTM__Team_Instance_Account__c TeamInstanceAcc, string Status){
        AxtriaSalesIQTM__Position_Account_Call_Plan__c obj = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
        obj.AxtriaSalesIQTM__Account_Alignment_type__c='Zip Aligned';
        obj.AxtriaSalesIQTM__Account__c = Acc.id;
        obj.AxtriaSalesIQTM__Position__c = Pos.id;
        obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
        obj.AxtriaSalesIQTM__Position_Team_Instance__c =  PosTeamInstance.id;
        obj.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.AxtriaSalesIQTM__isIncludedCallPlan__c   =false;
        obj.AxtriaSalesIQTM__Calls_Original__c  =6;
        obj.AxtriaSalesIQTM__isAccountTarget__c = true;
        obj.AxtriaSalesIQTM__Segment1__c = '3';
        obj.AxtriaSalesIQTM__ReasonAdd__c='Existing Physician Relationship';
        obj.AxtriaSalesIQTM__ReasonDrop__c = 'test drop';
        //obj.AxtriaSalesIQTM__Change_Status__c='Revise';
        obj.AxtriaSalesIQTM__Change_Status__c=Status;
        CreatePosAccCallPlanlist(Acc,Pos,TeamInstance,PosTeamInstance,TeamInstanceAcc);
        return obj;
    }    
    
    
    
     public static void CreatePosAccCallPlanlist(Account Acc, AxtriaSalesIQTM__Position__c Pos,AxtriaSalesIQTM__Team_Instance__c TeamInstance, AxtriaSalesIQTM__Position_Team_Instance__c PosTeamInstance, AxtriaSalesIQTM__Team_Instance_Account__c TeamInstanceAcc){
        
        list<string> reason = new list<string>();
        reason.add('Favorable Formulary/Managed Care Coverage');
        reason.add('Group Practice Physician');
        reason.add('Non-targeted Clinic');
        reason.add('Original Call Plan Physician');
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> lst = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(string readoncode:reason){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c obj = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
            obj.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today();
            obj.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today();
            obj.AxtriaSalesIQTM__Account_Alignment_type__c='Zip Aligned';
            obj.AxtriaSalesIQTM__Account__c = Acc.id;
            obj.AxtriaSalesIQTM__Position__c = Pos.id;
            obj.AxtriaSalesIQTM__Team_Instance__c = TeamInstance.id;
            obj.AxtriaSalesIQTM__Position_Team_Instance__c =  PosTeamInstance.id;
            obj.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
            obj.AxtriaSalesIQTM__isIncludedCallPlan__c   =true;
            obj.AxtriaSalesIQTM__Calls_Original__c  =6;
            obj.AxtriaSalesIQTM__isAccountTarget__c = true;
            obj.AxtriaSalesIQTM__Segment1__c = '4';
            obj.AxtriaSalesIQTM__ReasonAdd__c=readoncode;
            obj.AxtriaSalesIQTM__ReasonDrop__c = 'test drop';
            obj.AxtriaSalesIQTM__Change_Status__c='Revise';
            lst.add(obj);
        }
                
        insert lst;
    }    
    
     public static Contact createcontact(Account Acc){
        Contact objcon = new Contact();
        objcon.FirstName ='FirstName';
        objcon.LastName  ='last';
        objcon.AxtriaSalesIQTM__Trx__c='2';
        objcon.AxtriaSalesIQTM__Calls__c=4;
        objcon.AxtriaSalesIQTM__Sales__c = 300;
        objcon.AxtriaSalesIQTM__Type__c='Nurse';
        objcon.AccountId = Acc.id;
        return objcon;
    }
       
    public static AxtriaSalesIQTM__CR_Account__c createCRaccount( AxtriaSalesIQTM__Change_Request__c crObj, Account Acc, AxtriaSalesIQTM__Position__c desPos, AxtriaSalesIQTM__Position__c sourcePos, AxtriaSalesIQTM__Team_Instance__c TeamInstance, AxtriaSalesIQTM__Position_Team_Instance__c sourcePosTeamIns, AxtriaSalesIQTM__Position_Team_Instance__c desPosTeamIns, AxtriaSalesIQTM__Team_Instance_Account__c TeamInstanceAcc){
        AxtriaSalesIQTM__CR_Account__c obj = new AxtriaSalesIQTM__CR_Account__c();
        obj.AxtriaSalesIQTM__Account__c = Acc.id;
        obj.AxtriaSalesIQTM__Destination_Position__c = desPos.id;
        obj.AxtriaSalesIQTM__Source_Position__c = sourcePos.id;
        obj.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = desPosTeamIns.id;
        obj.AxtriaSalesIQTM__Source_Position_Team_Instance__c = sourcePosTeamIns.id;
        obj.AxtriaSalesIQTM__Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.AxtriaSalesIQTM__Change_Request__c = crObj.Id;
        obj.AxtriaSalesIQTM__IsImpactCallPlan__c = true;
        
        return obj;
    }
    
    public static User createUser(){
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='Rep']; 
            User tUser = new User(Alias = 'Rep', Email='repuser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@testorg.com');
            return tUser;
    }    
    */
}