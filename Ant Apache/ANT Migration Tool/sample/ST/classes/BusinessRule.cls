public virtual class BusinessRule {
    public list<Rule_Parameter__c> ruleParameters {get;set;}
    public Measure_Master__c ruleObject {get;set;}
    public Map<String, String> pathMap{get;set;}
    public list<String> pathList {get;set;}
    public string ruleId{get;set;}
    public String mode{get;set;}
    public String selectedBuisnessUnit {get;set;}
    public String selectedLine {get;set;}
    public String selectedBrand {get;set;}
    public String selectedCycle {get;set;}
    public list<SelectOption> prametersList{get;set;}
    public list<Step__c> steps{get;set;}
    public string selStepType{get;set;}
    public list<SelectOption> matrixList{get;set;}
    public Step__c step{get;set;}
    public boolean visibleGridFields {get;set;}
    public Grid_Master__c gridMap {get;set;}
    public String expression{get;set;}
    public Boolean isAddNew{get;set;}
    public String selectedMatrix{get;set;}
    public Integer stepCount{get;set;}
    public Compute_Master__c computeObj{get;set;}
    public String selectedCompF1{get;set;}
    public String selectedCompF2{get;set;}
    public Decimal selectedCompF3{get;set;}
    public String uiLocation;
    public String field2Type{get;set;}
    public String retUrl{get;set;}
    public Map<String, String> linkMap{get;set;}
    public Map<String,String> pathPageMap{get;set;}
    public String stepEditId{get;set;}
    public Boolean isStepEditMode{get;set;}
    public String gridParam1{get;set;}
    public String gridParam2{get;set;}

    public String countryID {get;set;}
    Map<string,string> successErrorMap;

    public void init(){

        //Fetch Country ID from cookie
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
        }
        
        uiLocation = '';
        field2Type = '';
        retUrl = '';
        stepEditId = '';
        isStepEditMode = false;
        mode = ApexPages.currentPage().getParameters().get('mode');
        if(String.isBlank(mode)){
            mode = 'new';
        }
        mode.toLowerCase();
        gridParam1 = '';
        gridParam2 = '';
        ruleId = ApexPages.currentPage().getParameters().get('rid');
        if((mode == 'edit' || mode == 'clone' || mode == 'view' ) ||  String.isNotBlank(ruleId)){
            ruleObject = [SELECT Id, Name, Stage__c, Team__c,Team__r.Name, Line_2__c, Line_2__r.Name, Line_Lookup__r.Name, isGlobal__c, Brand_Lookup__c, Brand_Lookup__r.Name, Cycle__c, Cycle__r.Name, Team_Instance__r.Name,Team_Instance__c FROM Measure_Master__c WHERE Id=:ruleId and Country__c = :countryID];
            selectedBuisnessUnit = ruleObject.Team_Instance__c;
            //selectedLine = ruleObject.Line_2__c;
            selectedBrand = ruleObject.Brand_Lookup__c;
            selectedCycle = ruleObject.Cycle__c;
        }else{
            ruleObject = new Measure_Master__c();
            ruleObject.Stage__c = 'Basic Information';
            selectedBuisnessUnit = '';
            selectedLine = '';
            selectedBrand = '';
            selectedCycle = '';
        } 
        fillPathMap();
    }

    public void initStep(){
        if(mode == 'view'){
            isAddNew = true;
        }
        steps = [SELECT Id, Name, Compute_Master__c, Compute_Master__r.Operator__c, Compute_Master__r.Field_1__C,
                        Compute_Master__r.Field_2__c, Compute_Master__r.Expression__c, Compute_Master__r.Field_2_Type__c, Compute_Master__r.Field_2_val__c ,Description__c,
                        Matrix__c,Grid_Param_1__c, Grid_Param_2__c, Sequence__c, Step_Type__c, Type__c,
                        Matrix__r.Dimension_1_Name__c, Matrix__r.Dimension_2_Name__c, Matrix__r.Name, Matrix__r.Grid_Type__c,
                        Grid_Param_1__r.Parameter_Name__c, Grid_Param_2__r.Parameter_Name__c
                 FROM STEP__c 
                 WHERE Measure_Master__c =: ruleObject.Id 
                       AND UI_Location__c =: uiLocation
                 ORDER BY Sequence__c];
        system.debug('--ruleObject-- ' + ruleObject);
        stepCount = database.countQuery('SELECT count() FROM STEP__c WHERE Measure_Master__c = \'' +ruleObject.Id + '\' AND UI_Location__c = \''+uiLocation+'\'');
        fillRuleParameters();
    }
    public void fillRuleParameters(){
        ruleParameters = [SELECT Id, Name, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        prametersList = new list<SelectOption>();
        prametersList.add(new SelectOption('None', '--None--'));
        system.debug('--ruleParameters-- ' + ruleParameters);
        for(Rule_Parameter__c param : ruleParameters){
            prametersList.add(new SelectOption(param.Id, param.Parameter_Name__c));
        }
    }

    public void updateRule(String newStage, String previousStage){
        if(mode != 'view'){
            ruleObject.Team_Instance__c = selectedBuisnessUnit;
            //ruleObject.Line_2__c = selectedLine;
            ruleObject.Brand_Lookup__c = selectedBrand;
            ruleObject.Cycle__c = selectedCycle;
            ruleObject.State__c = 'In Progress';
            ruleObject.is_executed__c = false;
            
            
            if(mode == 'new' || ruleObject.Stage__c == previousStage)
                ruleObject.Stage__c = newStage;
            upsert ruleObject;
        }
    }

    public PageReference nextPage(String page){
    
        PageReference pref = new PageReference('/apex/' +page+'?rid=' + ruleObject.Id + '&mode=' + mode);
        pref.setRedirect(false);
        return pref;
    }

    private void fillPathMap(){
        pathList = new list<String>{'Basic Information', 'Profiling Parameter',
                                 'Compute Values', 'Compute Segment', 'Compute TCF', 'Compute Accessibility', 'Summary'};
        pathPageMap = new Map<String, String>();
        pathPageMap.put('Basic Information', '/apex/' +'BusinessRuleConstruct?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Profiling Parameter', '/apex/' +'SelectRuleParameters?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Compute Values', '/apex/' +'ComputeValues?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Compute Segment', '/apex/' +'ComputeSegment?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Compute TCF', '/apex/' +'ComputeTCF?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Compute Accessibility', '/apex/' +'ComputeAccessiblity?mode='+mode+'&rid='+ruleObject.Id);
        pathPageMap.put('Summary', '/apex/' +'SummaryPage?mode='+mode+'&rid='+ruleObject.Id);
        pathMap = new Map<String, String>();
        linkMap = new Map<String, String>();
        String sldsClass = 'slds-is-complete';
        for(string pe : pathList){
            pathMap.put(pe, sldsClass);
            linkMap.put(pe, '');
            if(ruleObject != null && ruleObject.Stage__c == pe){
                pathMap.put(pe, 'slds-is-current');
                linkMap.put(pe, pathPageMap.get(pe));
                sldsClass = 'slds-is-incomplete';
            }

            if(sldsClass == 'slds-is-complete')
                linkMap.put(pe, pathPageMap.get(pe));
        }
        system.debug('--linkMap-- ' + linkMap);
    }

    public void addNewStep(){
        initStep();

        step = new Step__c();
        step.Step_Type__c = selStepType;
        step.Measure_Master__c = ruleObject.Id;

        list<String> stepLocations = new list<String>{'Compute Values', 'Compute Segment', 'Compute TCF', 'Compute Accessibility'};
        list<Step__c> allSteps = [SELECT Id, UI_Location__c, Sequence__c FROM Step__c WHERE Measure_Master__c =: ruleObject.Id ORDER BY Sequence__c];
        Map<String, list<Integer>> uiSeqMap = new Map<String, list<Integer>>();
        if(allSteps != null && allSteps.size() > 0){
            for(Step__c st: allSteps){
                list<Integer> tempList;
                if(uiSeqMap.containsKey(st.UI_Location__c)){
                    tempList = uiSeqMap.get(st.UI_Location__c);
                }else{
                    tempList = new list<Integer>();
                }

                tempList.add(Integer.valueOf(st.Sequence__c));
                uiSeqMap.put(st.UI_Location__c, tempList);
            }

            system.debug('--uiSeqMap-- ' + uiSeqMap);
            if(uiSeqMap.containsKey(uiLocation)){
                list<Integer> tempList = uiSeqMap.get(uiLocation);
                stepCount = tempList[tempList.size() -1] + 1;
                step.Sequence__c = stepCount;
            }else{
                Integer uiLocIndex = listIndex(stepLocations, uiLocation);
                if(uiLocIndex > 0){
                    String prevLocation = stepLocations[uiLocIndex - 1];
                    if(uiSeqMap.containsKey(prevLocation)){
                        List<Integer> tempList = uiSeqMap.get(prevLocation);
                        stepCount = tempList[tempList.size() - 1] + 1;
                    }else{
                        uiLocIndex = listIndex(stepLocations, prevLocation);
                        String prev2 = stepLocations[uiLocIndex - 1];
                        if(uiSeqMap.containsKey(prev2)){
                            List<Integer> tempList = uiSeqMap.get(prev2);
                            stepCount = tempList[tempList.size() - 1] + 1;
                        }else{
                            uiLocIndex = listIndex(stepLocations, prev2);
                            String prev3 = stepLocations[uiLocIndex - 1];
                            if(uiSeqMap.containsKey(prev3)){
                                List<Integer> tempList = uiSeqMap.get(prev3);
                                stepCount = tempList[tempList.size() - 1] + 1;
                            }
                        }
                    }
                }else if(uiLocIndex == 0){
                    stepCount = 1;
                }
                step.Sequence__c = stepCount;
            }
        }else{
            stepCount = 1;
            step.Sequence__c = 1;
        }

        isAddNew = true;
        system.debug('--step-- ' + step);
        if(selStepType == 'Matrix' || selStepType == 'Matrix_Compute'){
            system.debug('--grid-method-- ' + step);
            fillMatrixList();
            selectedMatrix = '';
            visibleGridFields = false;
        }else if(selStepType  == 'Compute' || selStepType  == 'Cases'){
            computeObj = new Compute_Master__c();
            expression = '';
            selectedCompF1 = '';
            selectedCompF2 = '';
            selectedCompF3 = 0.0;
        }
    }

    private Integer listIndex(list<String> listOfString, String value){
        Integer count = 0;
        for(String item: listOfString){
            if(item == value){
                return count;
            }
            count += 1;
        }
        return -1;
    }

    private void fillMatrixList(){
        matrixList = new list<SelectOption>();
        matrixList.add(new SelectOption('None', '--None--'));
        for(Grid_Master__c matrix:[SELECT Id, Name, Dimension_1_Name__c, Dimension_2_Name__c, Grid_Type__c FROM Grid_Master__c where Country__c = :countryID]){
            matrixList.add(new SelectOption(matrix.Id, matrix.Name));
        }
    }

    public void showGridFields(){
        visibleGridFields = true;
        system.debug('--visibleGridFields-- ' + visibleGridFields);
        system.debug('--visibleGridFields-- ' + selectedMatrix);
        system.debug('--visibleGridFields-- ' + step);
        gridMap = [SELECT Id, Name, Dimension_1_Name__c, Dimension_2_Name__c, Grid_Type__c FROM Grid_Master__c WHERE Id =:selectedMatrix];
        system.debug('++++++++++++++ Hey Grid Map is '+ gridMap);
    }

    public String validateStep(){
        String result = '';
        if(step != null){
            if(String.isBlank(step.Name)){
                result += 'Step Name cannot be blank<br/>';
            }else{
                system.debug('+++++++++++++ Hey Step name is '+ step);
                system.debug('++++++++ Rule is '+ ruleObject);
                list<Rule_Parameter__c> ruleParams = [SELECT Id, Parameter_Name__c, Step__c FROM Rule_Parameter__c WHERE Parameter_Name__c = :step.Name AND Measure_Master__c =:ruleObject.Id];
                Boolean issue = false;
                if(!isStepEditMode && ruleParams.size() > 0){
                    for(Rule_Parameter__c rp: ruleParams){
                        if(step.Id != null && step.Id != rp.Step__c)
                            issue = true;
                    }
                    
                    if(issue){
                        result += 'Step Name Cannot be same as any Parameter Name or Previous Step Name<br/>';
                    }
                }
            }

            if(step.Step_Type__c == 'Matrix'){
                system.debug('++++++++++ Hey Selected Matrix is '+ selectedMatrix);
                gridMap = [SELECT Id, Name, Dimension_1_Name__c, Dimension_2_Name__c, Grid_Type__c FROM Grid_Master__c WHERE Id =:selectedMatrix];
                system.debug('+++++++++ Hey Grid Map is '+ gridMap);
                if(selectedMatrix == 'None'){
                    result += 'select a matrix or create new to use<br/>';
                }else{
                    if(String.isBlank(gridParam1)){
                        result += 'Please select input for dimention one<br/>';
                    }

                    if(gridMap.Grid_Type__c == '2D' && String.isBlank(gridParam2)){
                        result += 'Please select input for dimention two<br/>';
                    }
                }
            }else if(step.Step_Type__c == 'Compute'){
                if(selectedCompF1 == 'None'){
                    result += 'Select input field 1 value<br/>';
                }

                if(field2Type == 'Parameters'){
                    if(selectedCompF2 == 'None')
                        result += 'Select input field 2 value<br/>';
                }
            }
        }
        return result;
    }

    public void editStep(){
        isStepEditMode = true;
        isAddNew = true;
        if(String.isNotBlank(stepEditId)){
            fillRuleParameters();
            step = [SELECT Id, Name, Compute_Master__c, Compute_Master__r.Operator__c, Compute_Master__r.Expression__c, Compute_Master__r.Field_2_val__c, Compute_Master__r.Field_2_Type__c, Compute_Master__r.Field_1__c, Compute_Master__r.Field_2__c, Compute_Master__r.Type__c, Description__c, Matrix__c, Measure_Master__c, Output__c, Grid_Param_1__c, Grid_Param_2__c, Sequence__c, Step_Type__c, Type__c, UI_Location__c FROM Step__c WHERE Id=:stepEditId];
            selStepType = step.Step_Type__c;
            if(selStepType == 'Matrix' || selStepType == 'Matrix_Compute'){
                system.debug('--grid-method-- ' + step);
                fillMatrixList();
                selectedMatrix = step.Matrix__c;
                visibleGridFields = false;
                showGridFields();
                gridParam1 = step.Grid_Param_1__c;
                gridParam2 = step.Grid_Param_2__c;
            }else if(selStepType  == 'Compute' || selStepType  == 'Cases'){
                computeObj = new Compute_Master__c();
                computeObj.Id = step.Compute_Master__c;
                if(selStepType  == 'Cases'){
                    expression = step.Compute_Master__r.Expression__c;
                }else{
                    field2Type = step.Compute_Master__r.Field_2_Type__c;
                    selectedCompF1 = step.Compute_Master__r.Field_1__c;
                    selectedCompF2 = step.Compute_Master__r.Field_2__c;
                    selectedCompF3 = step.Compute_Master__r.Field_2_val__c;
                    computeObj.Operator__c = step.Compute_Master__r.Operator__c;
                }
            }
        }
    }

    public void clearComputeValues(){
        selectedCompF2 = '';
        selectedCompF3 = 0.0;
        computeObj.Field_2__c = null;
        computeObj.Field_2_val__c = 0.0;
    }

    public PageReference cancel()
    {
        List<Measure_Master__c> mm = [select Stage__c from Measure_Master__c where id = :ruleId];

        if(mm.size()>0)
        {
            if(mm[0].Stage__c == 'Basic Information')
            {
                delete mm;
            }
        }
        return new PageReference('/apex/' +'Business_Rule_list');
    }
}