global class update_PositionGeography implements Database.Batchable<sObject>, Database.Stateful  {
    
    global string query;
    global string teamID;
    global string teamInstance;
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Geography__c> geoTI=new list<AxtriaSalesIQTM__Team_Instance_Geography__c>();
    global list<temp_Zip_Terr__c> zipTerrlist=new list<temp_Zip_Terr__c>();
    
    
    global update_PositionGeography(String Team,String TeamIns){
        teamInstance=TeamIns;
        teamID=Team;
        teminslst=[select AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c=:teamID];
        query='SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Zip_Terr__c where Team__c=:teamID and Team_Instance__c=:teamInstance and Position__c != null';
       

    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Zip_Terr__c>zipTerrlist){
       
        list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
         set<string> setPosGeoTeamInstance  = new set<string>();
        list<temp_Zip_Terr__c> lsTempRecordToUpdate = new list<temp_Zip_Terr__c>();
        
        for(temp_Zip_Terr__c rec:zipterrlist){
            list1.add(rec.Geography__c);
            list2.add(rec.Position__c);
        }
        
         //query existing position Geography with the key = Position+Geography+TeamInstanceName
        list<AxtriaSalesIQTM__Position_Geography__c> lsposGeo = [select id,name,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Geography__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Geography__c in:list1 and AxtriaSalesIQTM__Assignment_Status__c = 'Active']; 
        
        for(AxtriaSalesIQTM__Position_Geography__c pg : lsposGeo){
            setPosGeoTeamInstance.add(string.valueof(pg.AxtriaSalesIQTM__Position__c)+string.valueof(pg.AxtriaSalesIQTM__Geography__c)+string.valueof(pg.AxtriaSalesIQTM__Team_Instance__c));
        }
        
        map<string,string> posmap=new map<string,string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI:[select AxtriaSalesIQTM__Position_ID__c,id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstance])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c,posTI.id);
        }
        map<string,string> geomap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Geography__c geoTI:[select AxtriaSalesIQTM__Geography__c,id from AxtriaSalesIQTM__Team_Instance_Geography__c where AxtriaSalesIQTM__Geography__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            geomap.put(geoTI.AxtriaSalesIQTM__Geography__c,geoTI.id);
        }
       
        
        list<AxtriaSalesIQTM__Position_Geography__c> posgeoListUpdate = new list<AxtriaSalesIQTM__Position_Geography__c>();
        
        for(temp_Zip_Terr__c rec:zipTerrlist){
            if(!setPosGeoTeamInstance.contains(string.valueof(rec.Position__c)+string.valueof(rec.Geography__c)+string.valueof(teamInstance))){
                AxtriaSalesIQTM__Position_Geography__c obj=new AxtriaSalesIQTM__Position_Geography__c();
                
                obj.AxtriaSalesIQTM__Geography__c                = rec.Geography__c;
                obj.AxtriaSalesIQTM__Position__c                 = rec.Position__c;
                obj.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                obj.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(rec.Position__c);
                obj.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                obj.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                obj.AxtriaSalesIQTM__Position_Id_External__c     = rec.Position__c;
                obj.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                obj.AxtriaSalesIQTM__Proposed_Position__c        = rec.Position__c;
                
                posgeoListUpdate.add(obj);
                //Update Temp Record
                rec.status__c = 'Processed';
                lsTempRecordToUpdate.add(rec);
            }else{
                rec.status__c = 'Rejected';
                lsTempRecordToUpdate.add(rec);
            
            }   
            
        }
        
        if(posgeoListUpdate.size()!=0){     
            insert posgeoListUpdate;
        }
        if(lsTempRecordToUpdate.size()!=0){
            update lsTempRecordToUpdate;
        }
        
        system.debug('******************************upsert done************************************');
          
    }
    
          global void finish(Database.BatchableContext BC){
            /*string selectedObject = 'temp_Zip_Terr__c';
            Database.executeBatch(new batchdelete3(teamID,selectedObject),1000);*/
       }
}