@istest
public class AreaLineJuncSyncDaysInFieldtest {
    @istest static void AreaLineJuncSyncDaysInFieldtest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        insert ti;
        String teamInstId=ti.id;
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = teamId;
       // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        
        insert posNation;
        Product_Catalog__c pc = new Product_Catalog__c();
        pc.Name='BRILIQUE';
        pc.Team_Instance__c = teamInstId;
        insert pc;
        list<AxtriaSalesIQTM__Position_Product__c> poslist = new list<AxtriaSalesIQTM__Position_Product__c>();
        
            AxtriaSalesIQTM__Position_Product__c pos = new AxtriaSalesIQTM__Position_Product__c();
            pos.Business_Days_in_Cycle__c=5;
            pos.Calls_Day__c = 3;
            pos.AxtriaSalesIQTM__Position__c = posNation.id;
            pos.Product_Catalog__c = pc.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        pos.AxtriaSalesIQTM__Product_Weight__c = 3;
            insert pos;
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Area__c = posNation.id;
        area.Position_Product__c = pos.id;
        area.Team_Instance__c = teamInstId;
        insert area;
        
            poslist.add(pos);
        if(poslist!=null && poslist.size()>0){
            update poslist;
        }
        
        
        
        Test.startTest();
         System.runAs(loggedinuser){
             
         AreaLineJuncSyncDaysInFieldTrgrHandler obj=new AreaLineJuncSyncDaysInFieldTrgrHandler();
             
         }
        Test.stopTest();
    }

}