<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <formFactors>Small</formFactors>
    <formFactors>Medium</formFactors>
    <formFactors>Large</formFactors>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Task</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Event</tab>
    <tab>standard-Case</tab>
    <tab>standard-Forecasting3</tab>
    <tab>Team_Instance_Product_AZ__c</tab>
    <tab>ExecuteBatch</tab>
    <tab>Schedule_Processes</tab>
    <tab>Direct_Load</tab>
    <tab>Scenario_Product</tab>
    <tab>Call_Capacity</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
