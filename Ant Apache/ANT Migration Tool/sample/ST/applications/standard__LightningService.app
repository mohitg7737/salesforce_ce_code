<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <formFactors>Large</formFactors>
    <navType>Console</navType>
    <tab>standard-Case</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Team_Instance_Product_AZ__c</tab>
    <tab>ExecuteBatch</tab>
    <tab>Schedule_Processes</tab>
    <tab>Direct_Load</tab>
    <tab>Scenario_Product</tab>
    <tab>Call_Capacity</tab>
    <uiType>Lightning</uiType>
    <utilityBar>LightningService_UtilityBar</utilityBar>
</CustomApplication>
