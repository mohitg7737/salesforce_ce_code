<apex:page sidebar="false" controller="SelectRuleParametrsCtlr" docType="html-5.0">
    <apex:slds />
    <apex:stylesheet value="{!$Resource.appCss}"/>
    <apex:stylesheet value="{!URLFor($Resource.font_awesome, 'css/font-awesome.css')}"/>
    <apex:stylesheet value="{!URLFor($Resource.listswap, 'listswap.css')}"/>
    <apex:includeScript value="{!$Resource.jquery_1_12_4_min}" />
    <apex:includeScript value="{!URLFor($Resource.listswap, 'jquery.listswap.js')}" />
    <style type="text/css">
        .overflow_y{
            overflow-y: auto;
        }
    </style>
    <apex:form >
        <apex:outputPanel id="pgmsg">
            <apex:pageMessages escape="false"/>
        </apex:outputPanel>
        <apex:actionstatus id="status">
            <apex:facet name="start">
                <div class="slds-spinner_container slds-is-fixed">
                    <div role="status" class="slds-spinner slds-spinner_medium">
                        <span class="slds-assistive-text">Loading</span>
                        <div class="slds-spinner__dot-a"></div>
                        <div class="slds-spinner__dot-b"></div>
                    </div>
                </div>
            </apex:facet>
        </apex:actionstatus>
        <div class="slds-p-left_x-small slds-p-right_x-small">
            <div class="slds-page-header  slds-p-around_x-small">
                <div class="slds-media">
                    <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-opportunity" title="Description of icon when needed">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="slds-media__body">
                        <h1 class="slds-page-header__title slds-truncate slds-align-middle" title="Business Rule">Call Plan Generation Business Rule</h1>
                    </div>
                </div>
            </div>
            <div class="slds-p-vertical_medium">
                <div class="slds-path-coach">
                    <div class="slds-grid slds-p-bottom_small">
                        <c:ProcessStateView pathMap="{!pathMap}" pathAtribs="{!pathList}" linkMap="{!linkMap}"/>
                        <!-- <button class="slds-button slds-button--brand slds-path__mark-complete slds-no-flex slds-m-horizontal--small slds-m-right_none">
                        <i class="fa fa-check" aria-hidden="true"></i> Mark Status as Complete</button> -->
                    </div>
                    <div id="content-path-2" class=" slds-show" role="tabpanel" aria-labelledby="tab-path-2">
                        <!-- <legend class="slds-form-element__legend slds-form-element__label">New Parameter</legend>
                        <input type="text" name="New Parameter" id="add_parm_txt"/>
                        <button class="slds-button slds-button_brand" type="button" id="add_parm_btn">Add</button> -->
                        <div class="slds-grid slds-p-bottom_x-small">
                            <div class="slds-frame slds-size--5-of-5">
                                <div class="slds-form slds-form_stacked">
                                    <fieldset class="slds-form-element">
                                        <legend class="slds-form-element__legend slds-form-element__label">{!$Label.sp_profile_parameters}</legend>
                                        <select id='select_source' data-search="Search parameters">
                                            <apex:repeat var="result" value="{!parameterList}">
                                                <option value="{!result.Id}">{!result.Name}</option>
                                            </apex:repeat>
                                        </select>
                                        <select id="select_destination">
                                            <apex:repeat var="result" value="{!DestParameterList}">
                                                <option value="{!result.Parameter__c}">{!result.Parameter__r.Name}</option>
                                            </apex:repeat>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="slds-text-align_center">
                            <button class="slds-button slds-button_brand" type="button" id="save_nxt_btn">{!$Label.btn_save_next}</button>
                            <button class="slds-button slds-button_brand" type="button" id="save_btn">{!$Label.btn_save}</button>
                            <apex:commandButton styleClass="slds-button slds-button_brand" action="{!cancel}" value="{!$Label.axtriaarsnt__btn_cancel}" immediate="true" style="margin-left: 7px;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <apex:actionFunction action="{!save}" name="save" rerender="pgmsg" status="status">
            <apex:param value="parameters" assignTo="{!selectedParameters}" name="params"/>
        </apex:actionFunction>
        <apex:actionFunction action="{!saveAndNext}" name="saveAndNext" rerender="pgmsg" status="status">
            <apex:param value="parameters" assignTo="{!selectedParameters}" name="params"/>
        </apex:actionFunction>
    </apex:form>
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        $j(document).ready(function() {
            $j(".slds-accordion h3").each(function(){
                $j(this).click(function(){
                    if (!($j(this).parent().parent().hasClass('slds-is-open'))) {
                        $j(".slds-accordion__section").removeClass("slds-is-open");
                        $j(this).parent().parent().addClass("slds-is-open");
                    } else {
                        $j(".slds-accordion__section").removeClass("slds-is-open");
                    }
                    return false;
                });
            });
            //Fill select list after page load.
            /* -------Start------- Select List Code -------Start------- */
            var lst = $j('#select_source, #select_destination').listswap({
                truncate:true, 
                height:162,
                label_add:'', 
                label_remove:'', 
                btn_cls:'slds-button slds-button_brand',
            });

            if(lst && ('{!LOWER(mode)}' == 'view' )){
                $j('.listbox_controls ul').attr('style', 'display:none;');
            }

            $j('.listbox_round_class').addClass('overflow_y');
            /* -------Start------- Add parammeter btn -------Start------- */
            $j('#add_parm_btn').on('click', function(){
                $j('[id$="status.start"]').removeAttr('style');
                var parameter = $j('#add_parm_txt').val();
                if(parameter){
                    Visualforce.remoting.Manager.invokeAction(
                        '{!$RemoteAction.SelectRuleParametrsCtlr.addNewParameter}', parameter, function(result, event) {
                            if(event.status){
                                console.log('adding--');
                                $j('#select_source').append('<option value="'+result.Id+'">'+result.Name+'</option>');
                                lst.add_new_option(result.Name, result.Id); 
                                $j('#add_parm_txt').val(''); //clear search after addition
                                $j('[id$="status.start"]').attr('style', 'display:none');
                            }
                        }
                    );
                }
            });
        });

        //Save button click
        $j('#save_btn').on('click', function(){
            console.log('save_btn_clicked');
            var selected_params = [] 
            $j('#listbox_select_destination_wrapper ul>li').each(function(node){
                //console.log('selected_param : ' + $j(this).attr('data-value'));
                selected_params.push($j(this).attr('data-value'));
            });
            console.log(selected_params);
            save(JSON.stringify(selected_params));
        });

        //Save and next button click
        $j('#save_nxt_btn').on('click', function(){
            console.log('save_nxt_btn_clicked');
            var selected_params = [] 
            $j('#listbox_select_destination_wrapper ul>li').each(function(node){
                //console.log('selected_param : ' + $j(this).attr('data-value'));
                selected_params.push($j(this).attr('data-value'));
            });
            console.log(selected_params);
            saveAndNext(JSON.stringify(selected_params));
        });
    </script>
</apex:page>