({
    // this function automatic call by aura:waiting event 

    showSpinner: function(component, event, helper) {

        // make Spinner attribute true for display loading spinner

        component.set("v.Spinner", true);

        component.set("v.Spinner", true);

    },

   

    // this function automatic call by aura:doneWaiting event

    hideSpinner : function(component,event,helper){

        // make Spinner attribute to false for hide loading spinner   

        component.set("v.Spinner", false);

    },

 

    createDatatable : function(component, event, helper) {
        
     
        //GET COUNTRY COOKIE VALUE
        var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
        var cookieValue = (' '+document.cookie).match(regex);
        
        if(cookieValue != undefined){
            component.set("v.countryId",cookieValue[1]);
            console.log(component.get("v.countryId"));
    
            if(component.get("v.countryId") != ''){
                var flagAction = component.get("c.getCountryAttributes");
                flagAction.setParams({
                    countryId : component.get("v.countryId")
                });
                 flagAction.setCallback(this,function(response){
                    var state = response.getState();
                    if(state === 'SUCCESS'){   
                        var attrs = response.getReturnValue().split(',');
                        component.set("v.countryName",attrs[0]);
                        component.set("v.countryFlag",attrs[1]);
                    }
                 });
                  $A.enqueueAction(flagAction);
            }
    
            var message = component.get("v.isSuccess");
    
            var action = component.get("c.isStatusMarkCompleted");
            //message == 'success' && 
            if(message == 'success' && component.get("v.feeId") != ''){
    
              action.setParams({
    
                  "crfeedid" : component.get("v.feeId")
    
              });
    
     
    
              action.setCallback(this,function(response){
    
                  if(response.getReturnValue() != undefined){
    
                      component.set("v.crEmployeefeedId",response.getReturnValue());
    
                      console.log('Completed Action');
    
                  }
    
              });      
    
            }
    
            $A.enqueueAction(action);
            component.showSpinner();
    
            var action = component.get("c.getProfileNameMap");
    
            action.setCallback(this,function(response)
    
            {  
    
                if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
    
                      console.log('profilemap :',response.getReturnValue());
                      var profileMap = response.getReturnValue();
                      console.log('------------- in else loop');
    
                      //var convertedProfileMap = {};
                      Object.keys(profileMap).forEach(function(key) {
                          console.log(key, profileMap[key]);
                          var profileKey = profileMap[key];
                          for (var i = 0; i < profileMap[key].length; i++) {
                              var temp = profileMap[key][i].split(' ').join('_');
                              var profileName = "$Label.c." + temp;
                              console.log('profileName : '+profileName);
                              component.set("v.tempProfileName", $A.getReference(profileName));
                              profileKey[i] = component.get("v.tempProfileName");
                          }
                      });
    
                      console.log('---- profile map ----');
                      console.log(profileMap);
                      component.set("v.profileMap", profileMap);
    
                    
                }
    
                     
            });
    
            $A.enqueueAction(action);
            
            
            //GET COUNTRY COOKIE VALUE
           /* var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
            var cookieValue = (' '+document.cookie).match(regex);
            
            
                component.set("v.countryId",cookieValue[1]);
                console.log(component.get("v.countryId"));
            
            if(component.get("v.countryId") != ''){
                var flagAction = component.get("c.getCountryAttributes");
                flagAction.setParams({
                    countryId : component.get("v.countryId")
                });
                 flagAction.setCallback(this,function(response){
                    var state = response.getState();
                    if(state === 'SUCCESS'){   
                        var attrs = response.getReturnValue().split(',');
                        component.set("v.countryName",attrs[0]);
                        component.set("v.countryFlag",attrs[1]);
                    }
                 });
                  $A.enqueueAction(flagAction);
            }*/
    
    
            var workbenchTable;
    
            var $j = jQuery.noConflict();
    
            var actionCardCount = component.get("c.GetCount");
            actionCardCount.setParams({
                    countryId : component.get("v.countryId")
            });
    
            actionCardCount.setCallback(this,function(response)
    
            {                    
    
                var state = response.getState();
    
                console.log('Please run1');
    
                console.log('state-->'+state);
    
                if(state === 'SUCCESS' && response.getReturnValue() != null)
    
                { 
    
                    component.set("v.Count",response.getReturnValue());
    
                    console.log(component.get("v.Count"));
    
                }
    
            } );
    
           
    
            $A.enqueueAction(actionCardCount);
    
            var actionFeed = component.get("c.Feed");
    
            actionFeed.setParams({
    
                "eventType": '',
                "countryID" : component.get("v.countryId")
    
            });
    
            actionFeed.setCallback(this,function(response)
    
            {                    
    
                var wrapperFeed;
    
                var state = response.getState();
    
                console.log('Please run3');
    
                console.log('state-->'+state);
    
                    if(state === 'SUCCESS' && response.getReturnValue() != null)
    
                    {
    
                        try
    
                        {
    
                            var json=response.getReturnValue();
    
                            console.log(json);
    
                             workbenchTable = $j('.tableWb').DataTable({
    
                                 "info": false,
    
                                 "search": {
    
                                        "regex": true,
    
                                     "smart": false,
    
                                    },
    
                                 "order": [],
    
                                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 200]],
    
                                 destroy : true,
    
                                 "processing": false,
    
                                 "data": response.getReturnValue(),
    
                                 //"order" : [[2,"asc"]],
    
                                 scrollX : true,
    
                                 scrollY:     "305px",
    
                                 scrollCollapse: true,
    
                               "lengthChange": false,
    
                               "pageLength": 6,
    
                                 "language":
    
                                 {
    
                                     "emptyTable": "No matching records found",
    
                                     "sLengthMenu": "_MENU_",
    
                                     "search": "",
    
                                     "searchPlaceholder": "Search..."
    
                                 },
    
                                 columns:[
    
                                     /*0 */   { data: 'IsRemoved', title : '', defaultContent: ''},
    
                                   /*1*/    { data: 'Event_Name', title: 'Event Name',defaultContent:'' },
    
                                     /*2 */   { data: 'Request_Date1',title: 'Requested Date', defaultContent:''},
    
                                     /*3 */   { data: 'Employee_Name',title: 'Employee Name', defaultContent:'' },
    
                                     /*4 */   { data: 'Employee_ID', title: 'Employee PRID', defaultContent:'' },
    
                                     /*5 */   { data: 'Employee_Division', title: 'Division',visible:false, defaultContent:'' },
    
                                     /*6 */   { data: 'Position_Name', title: 'Position Name', defaultContent:'' },
    
                                     /*7 */   { data: 'Position_Type', title: 'Position Type' , defaultContent:''},
    
                                     /*8 */   { data: 'Parent_Position_Name', title: 'Parent',visible:false, defaultContent:'' },
    
                                     /*9 */   { data: 'Manager_Name', title: 'Manager Name',visible:false, defaultContent:'' },
    
                                     /*10*/   { data: 'Training_Completion_Date1', title: 'Training Completion Date', visible:false, defaultContent:'' },
    
                                     /*11*/   { data: 'Assignment_Type',visible:false, title:  'Assignment Type' , defaultContent:''},
    
                                     /*12*/   { data: 'Field_Force', title: 'Field Force', visible:false,defaultContent:'' },
    
                                     /*13*/   { data: 'Status', title: 'Status', defaultContent:'' },
    
                                     /*14*/   { data: 'Employee_ID', title: 'empId' ,visible:false, defaultContent:''},
    
                                     /*15*/   { data: 'ids', title: 'id',visible:false, defaultContent:'' },
                                     /*16*/   { data: 'Hire_Date', title: 'Hire Date',visible:true, defaultContent:'' } ,
                                     /*17*/   { data: 'Term_Date', title: 'Termination Date',visible:true, defaultContent:'' } ,                     
                                     /*18*/   { date: 'Team_Instance_End_date', title : 'Manage Assignment End Date', visible : false, defaultContent:''}
    
                                     
    
                                 ],
    
                                 
    
                                 columnDefs :[   
    
                               {                       
    
                                   targets: [0],
    
                                   "title": "<input type='checkbox' class='example-select-all'></input>",
    
                                  "orderable": false,
    
                                   "createdCell": function(td, cellData, rowData, row, col) {
    
                                      
    
                                       var crEmpfeedID = rowData.ids;
    
                                       //console.log('feed--->'+crEmpfeedID);
    
                                       var stringQuery = 'return getRowId(\'' + crEmpfeedID + '\',this)';
    
                                       var selectHtml = '<input data-sfid=\"'+crEmpfeedID+'\" type="Checkbox" onclick="' + stringQuery + '"  name="chooseEmp" class="eventrowID"/>';
    
                                        //console.log(selectHtml);
    
                                       $j(td).html(selectHtml);
    
                                   }
    
                               },
    
                        {
    
                            targets:[3],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            { 
    
                                if(rowData.Employee_record_Id != undefined){ 
    
                                    var empId   = rowData.Employee_record_Id;  
    
                                    var empName = rowData.Employee_Name;                               
    
                                    var imgHtml = '<a href=/'+empId+' target="_top"> '+empName+'  </a>';
    
                                    $j(td).html(imgHtml);
    
                                }
    
                            }
    
                        },
    
                        {
    
                            targets:[4],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                                if(rowData.Employee__record_Id != undefined){ 
    
                                    var empId   = rowData.Employee__record_Id;
    
                                    var empName = rowData.Employee_ID;                               
    
                                    var imgHtml = '<a href=/'+empId+' target="_top"> '+empName+'  </a>';
    
                                    $j(td).html(imgHtml);
    
                                }
    
                            }
    
                        },
    
                        {
    
                            targets:[16],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                                    var startDate = rowData.Hire_Date;         
                                    console.log('---------- startDate ',startDate);                      
                                    component.set("v.AssignmentStartDate",startDate);
                                    
                            }
    
                        },
                        /*{
    
                            targets:[18],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                                    console.log(rowData);
                                    var endDate = rowData.Team_Instance_End_date;         
                                    console.log('---------- endDate ',endDate);                      
                                    component.set("v.AssignmentEndDate",endDate);
                                    
                            }
    
                        },
    */
                        {
    
                            targets:[6],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                                //alert('pos: '+rowData.Position__c);                               
    
                                if(rowData.Position__id != undefined){                               
    
                                    var posId   = rowData.Position_id;  
    
                                    var posName = rowData.Position_Name;                               
    
                                    var imgHtml = '<a href=/'+posId+' target="_top"> '+posName+'  </a>';
    
                                    $j(td).html(imgHtml);
    
                                }
    
                            }
    
                        },
    
                        {
    
                            targets:[1],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                                var data        = rowData.Status;
    
                                var eventVal    = rowData.Event_Name;
    
                                var empId       = rowData.Employee_record_Id;
    
                                var crEmpfeedID = rowData.ids;
    
                                var empName     = rowData.Employee_Name;
    
                                /*console.log('eventVal ', eventVal);
    
                                console.log('sfid-->',crEmpfeedID);
    
                                console.log('eName-->',empName);
    */
                                var imgHtml;
    
                                if(data == 'Pending Action'){
    
                                    imgHtml=  '<input data-crId=\"'+crEmpfeedID+'\"  data-empNameid=\"'+empName+'\" data-sfid=\"'+empId+'\" type=\"button\" class=\"slds-button slds-button_success"\ style=\"min-width: 157px; color: #fff;\"';
    
                                    imgHtml = imgHtml+ ' value="'+eventVal+'"/>';
    
                                    $j(td).addClass('pendingActionButton');
    
                                   
    
                                }
    
                                else if(data == 'Completed Action' || data == 'No Action'){
    
                                      imgHtml = '<input data-sfid=\"'+empId+'\" type=\"button\" class=\"slds-button slds-button_neutral"\ style=\"min-width: 157px;\" disabled=\"true\" value="'+eventVal+'" />'                             
    
                                }
    
                                $j(td).html(imgHtml);
    
                            }
    
                        },
    
                        {
    
                            targets:[13],
    
                            "createdCell" : function(td,cellData,rowData,row,col)
    
                            {
    
                               
    
                                var data        = rowData.Status;
    
                                var crEmpfeedID = component.get("v.crEmployeefeedId");
    
                                var imgHtml     = '';
    
                               
    
                            //    if(component.get("v.crEmployeefeedId") == rowData.ids && component.get("v.isSuccess") == 'success'){
    
                            //        debugger;
    
                            //        imgHtml = '<img src='+ $A.get('$Resource.Completed')+' />';
    
                                    
    
                            //    }
    
                            //    else{
    
                                    if(data == 'Pending Action'){
    
                                        imgHtml = '<img src='+ $A.get('$Resource.ActionRequired')+' />';     
    
                                       // console.log('imgHtml 123--->'+$A.get('$Resource.ActionRequired'));
    
                                    }
    
                                    else if(data == 'Completed Action'){
    
                                        //imgHtml = '<img src='+ $A.get('$Resource.Completed')+' />';   
                                        imgHtml = '<img src='+ $A.get('$Resource.NoAction')+' />'; 
    
                                    }
    
                                    else if(data == 'No Action'){
    
                                        imgHtml= '<img src='+ $A.get('$Resource.NoAction')+' />';                             
    
                                    }
    
                            //    }
    
                               // console.log(imgHtml);
    
                                $j(td).html(imgHtml);
    
                            }
    
                        }]
    
                    });
    
                    component.hideSpinner();
                         console.log('-- cookieValue value 23' );
                            
            
    
                              
    
                        $j(document).on("click",".pendingActionButton", function (event) {
    
                            component.gotoURL($j(event.target).attr('data-sfid'),$j(event.target).attr('data-empNameid'),$j(event.target).attr('data-crId'));
    
                        });
    
     
    
                        $j(document).on("click",".eventrowID", function (event) {
    
                            component.clickEnableDisable($j(this).prop("checked"),$j(event.target).attr("data-sfid"));
    
                        });
    
     
    
                        $j(document).on("click",".example-select-all", function(event){
    
                            var rows = workbenchTable.rows({ 'page': 'current' }).nodes();
    
                            if($j('#example-select-all').is(':checked')){
    
                                $j('input[type="checkbox"]', rows).prop('checked', true);
    
                            }
    
                            else{
    
                                $j('input[type="checkbox"]', rows).prop('checked', false);
    
                            }   
    
                        });
    
                    }
    
                    catch(err)
    
                    {
    
                       // console.log('Printing Error');
    
                        console.log(err.message);
    
                    }
    
                }
    
                else if(state == "ERROR"){
    
                    console.log('Error in calling server side action');
    
                }
    
            });
    
            $A.enqueueAction(actionFeed);
    
        }else{
            
            console.log('----- error while country id is undefined');
            var toastEvent = $A.get("e.force:showToast");
	                    toastEvent.setParams({
	                        mode: 'dismissible',
	                        message: 'Please select country',
	                        type : 'Error'	                        
	                    });
		               toastEvent.fire();
		               //component.hideSpin();

        }

      
     
    },

   

    // It shows Events in main data table on the basis of CardLabel click

    cardLabelClick : function(component, event, helper){

        component.showSpinner();

        console.log('---------------'+event);

        var eventType = event.currentTarget;

        console.log('22222222222 '+eventType);

        var id_str = eventType.dataset.value;
		console.log(id_str);
        var actionFeed = component.get("c.Feed");

        actionFeed.setParams({

            "eventType": id_str,
            "countryID" : component.get("v.countryId")

        });

       

        actionFeed.setCallback(this,function(response){

            var state = response.getState();

              console.log('state-->'+state);

              if(state === 'SUCCESS' && response.getReturnValue() != null)

              {

                try{

                    var json=response.getReturnValue();

                    console.log(json);

                    var $j = jQuery.noConflict();

                    $j('.tableWb').DataTable().clear();

                    $j('.tableWb').DataTable().rows.add(json).draw();

                    component.hideSpinner();

                }

                catch(err){

                    console.log(err);

                }

            }

        });

           

        var $j=jQuery.noConflict();

        $j("li.listBox").on("click",function(){

            $j("li.listBox").removeClass('slds-theme--shade').addClass('slds-theme--default');

            $j(this).removeClass('slds-theme--default').addClass('slds-theme--shade');

        }) ;

       

        $A.enqueueAction(actionFeed);

    },

 

    // Navigation to Employee Universe Component

    gotoURL : function (component, event, helper){

      console.log('calling gotoURL -------------------');
      var startD = new Date();

        startD.setMonth(startD.getMonth()-6);

        startD.setDate(1);

      var endD = new Date();

        endD.setMonth(endD.getMonth()+6);

        endD.setDate(0);

      
        /* THIS CODE IS ADDDE ON LOAD OF WORKBENCH IN CREATEDATATABLE METHOD , LABELS WERE NOT GETTING VALUES ON LOAD.

        var action = component.get("c.getProfileNameMap");

        action.setCallback(this,function(response)

        {  

            if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){

                  console.log('profilemap :',response.getReturnValue());
                  var profileMap = response.getReturnValue();
                  console.log('------------- in else loop');

                  //var convertedProfileMap = {};
                  Object.keys(profileMap).forEach(function(key) {
                      console.log(key, profileMap[key]);
                      var profileKey = profileMap[key];
                      for (var i = 0; i < profileMap[key].length; i++) {
                          var temp = profileMap[key][i].split(' ').join('_');
                          var profileName = "$Label.c." + temp;
                          console.log('profileName : '+profileName);
                          component.set("v.tempProfileName", $A.getReference(profileName));
                          profileKey[i] = component.get("v.tempProfileName");
                      }
                  });

                  console.log('---- profile map ----');
                  console.log(profileMap);
                  component.set("v.profileMap", profileMap);

                  //component.set("v.profileMap",response.getReturnValue());

            }

                 
        });

        $A.enqueueAction(action);

        console.log('---- profile map ----');
                  
        console.log(component.get("v.profileMap"));*/
  
        var evt = $A.get("e.force:navigateToComponent");

        evt.setParams({

            componentDef : "AxtriaSalesIQTM:SalesIQManageAssignment",

            componentAttributes: {

                recordId     : event.getParam('arguments').employeesfId,

                assigneeName : event.getParam('arguments').employeeName,

                moduleName   : 'Employee Universe',

                selectedTeamInstance : '',

                namespace    : 'AxtriaSalesIQTM__',

                startDate    : startD,

                endDate      : endD,

                profileMap     : component.get("v.profileMap"),

                externalInvokeWorkbench : 'true',

                workbenchId  : event.getParam('arguments').crEmployeefeedId,
                
                isSuccess : ''

            },   

            isredirect : true

        });

        evt.fire();

      },




 

     // Remove from workbench Button enable/disable

    clickEnableDisable : function(component, event, helper) {

        var $j=jQuery.noConflict();

        var btn = component.find('button');

        var isChecked=event.getParam('arguments').checked;

        var sfid=event.getParam('arguments').sfid;

        var checkedsfIds=component.get("v.checkedSfids");   

        if(isChecked){

            checkedsfIds.push(sfid);

        }

        else{

            var index = checkedsfIds.indexOf(sfid);

            if (index > -1) {

                checkedsfIds.splice(index, 1);

            }

        }

        component.set("v.checkedSfids",checkedsfIds);

        if(checkedsfIds.length==0){

            $j(".remove").attr("disabled","disabled");

        }

        else{

            $j(".remove").removeAttr("disabled");

        }

    },
	showAlert: function (component, event, helper){
         var j$=jQuery.noConflict();
        j$('.dialog').show();
    },

    hideAlert: function (component, event, helper){
        var j$=jQuery.noConflict();
        j$('.dialog').hide();
    }, 
 

    // Events remove from workbench

    removefromWorkbench : function (component, event, helper){
		
        component.hideAlert(); 
        component.showSpinner();

        var sfids=[];

        var j$=jQuery.noConflict();

        j$('input:checkbox.eventrowID').each(function(index){

            if(j$(this).prop("checked")){

               sfids.push(j$(this).attr("data-sfid"));

            }

        });

 

        var actionRemoveWB = component.get("c.isRemovedEventfromWB");

        actionRemoveWB.setParams({

            "rowID": sfids.join(',')

        });

        actionRemoveWB.setCallback(this,function(response)

        {                    

            var state = response.getState();

            console.log('Please run WB');

            console.log('state-->'+state);

            component.set("v.isRemovedEventfromWB",response.getReturnValue());

            component.createDatatable();

 

            var toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({

                title: 'Success!',

                type: 'success',

                message: 'Event(s) have been removed from the workbench.'

            });

                toastEvent.fire();

                component.hideSpinner();

        });

 

        $A.enqueueAction(actionRemoveWB);

    }   

})