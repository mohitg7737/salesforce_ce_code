global class Batch_Populate_User_Position implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    public String query;
    public Set<String> uapExists;
    public Set<String> posIDs;
    public Set<String> duplicateKey;
    public string countryname;
    public Map<String,String> mapUSerPosTeamInsKey2Id;
    /* issue :we are facing 50000 Query rows issue at allUapRecs as we are querying all the user positions so here i am filtering based on country 
        Author :Siva
        Method name:Batch_Populate_User_Position (country name) // pass the country name as attribute
        Date: 28-12-2018 12:46 AM
    */
    global Batch_Populate_User_Position(String country) {
        countryname='';
        countryname = country;
        //mapUSerPosTeamInsKey2Id = new Map<String,String>();
        //posIDs = new Set<String>();
        
        this.query = 'select id, Country_Code__c,AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c , AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true  and Country_Code__c =: countryname and AxtriaSalesIQTM__Assignment_Status__c =\'Active\'';
        
        
        /*uapExists = new Set<String>();
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Country__c =: countryname and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
            uapExists.add(concat);
            mapUSerPosTeamInsKey2Id.put(concat,uap.Id);         
        }
        system.debug('+++++++++++++ uap Existing----' + uapExists);*/
    }
    
    global Batch_Populate_User_Position() {
        
        this.query = 'select id, AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.User_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.User_Type_Vod__c ,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Assignment_Status__c =\'Active\'';
        
        
        // uapExists = new Set<String>();
        // mapUSerPosTeamInsKey2Id = new Map<String,String>();
        // posIDs = new Set<String>();
        
        // List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true];
        
        // for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        // {
        //     String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
        //     uapExists.add(concat);
        //     mapUSerPosTeamInsKey2Id.put(concat,uap.Id);          
        // }
        // system.debug('+++++++++++++ uap Existing' + uapExists);
    }
    
    //@Deprecated
    global Batch_Populate_User_Position(Date changedRecs) {
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('=======Query is:::'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){

        database.executeBatch(new Batch_Populate_User_Position());                                                           
       
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) {
        
        List<String> allUsers = new List<String>();
        posIDs = new Set<String>();
        duplicateKey = new Set<String>();
        uapExists = new Set<String>();
        mapUSerPosTeamInsKey2Id = new Map<String,String>();
        
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            allUsers.add(posEmp.AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c);
            posIDs.add(posEmp.AxtriaSalesIQTM__Position__c);
        }


        List<AxtriaSalesIQTM__User_Access_Permission__c> allUapRecs = [select id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__isCallPlanEnabled__c,Enable_Call_Plan_Report__c,AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true and AxtriaSalesIQTM__Position__c in :posIDs];
        
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUapRecs)
        {
            String concat = uap.AxtriaSalesIQTM__Position__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__c + '_' + uap.AxtriaSalesIQTM__User__c;
            uapExists.add(concat);
            mapUSerPosTeamInsKey2Id.put(concat,uap.Id);         
        }
        system.debug('+++++++++++++ uap Existing----' + uapExists);
        
        Map<String, ID> fedIdToUserId = new Map<String, ID>();
        
        for(User users : [select FederationIdentifier, id from User where FederationIdentifier in :allUsers and IsActive = true])
        {
            if(users.FederationIdentifier != null)
            {
                fedIdToUserId.put(users.FederationIdentifier, users.id);
            }
        }
        
        system.debug('+++++++++++++ fedIdToUserId' + fedIdToUserId);
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPInsert = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAPUpdate = new List<AxtriaSalesIQTM__User_Access_Permission__c>();

        for(AxtriaSalesIQTM__Position_Employee__c posEmp : scope)
        {
            /*AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
            
            uap.AxtriaSalesIQTM__Position__c = posEmp.AxtriaSalesIQTM__Position__c;
            uap.AxtriaSalesIQTM__Team_Instance__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c;
            uap.isCallPlanEnabled__c = true;
            uap.AxtriaSalesIQTM__Map_Access_Position__c = posEmp.AxtriaSalesIQTM__Position__c;
            uap.AxtriaSalesIQTM__is_Active__c = true;
            uap.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';*/
            
            if(fedIdToUserId.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c))
            {
                String userRec = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c);
                
                String concat = posEmp.AxtriaSalesIQTM__Position__c + '_' + posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c + '_' + userRec;
                
                if(!uapExists.contains(concat))
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uap = new AxtriaSalesIQTM__User_Access_Permission__c();
            
                    uap.AxtriaSalesIQTM__Position__c = posEmp.AxtriaSalesIQTM__Position__c;
                    uap.AxtriaSalesIQTM__User__c = fedIdToUserId.get(posEmp.AxtriaSalesIQTM__Employee__r.DSI_Email_Id__c);
                    uap.AxtriaSalesIQTM__Team_Instance__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c;
                    
                    if(posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.User_Type__c == 'Sales')
                    {
                        uap.AxtriaSalesIQST__isCallPlanEnabled__c = true;
                        uap.Enable_Call_Plan_Report__c = true;
                    }
                    uap.AxtriaSalesIQTM__Map_Access_Position__c = posEmp.AxtriaSalesIQTM__Position__c;
                    uap.AxtriaSalesIQTM__is_Active__c = true;
                    uap.AxtriaSalesIQTM__Sharing_Type__c = 'Implicit';
                    uapExists.add(concat);
                    allUAPInsert.add(uap);
                    system.debug('+++++++++++ Uap Exidts1'+ uapExists);
                    system.debug('+++++++++++ Uap Exidts1Insert'+ allUAPInsert);
                }
                else
                {
                    AxtriaSalesIQTM__User_Access_Permission__c uapExist = new AxtriaSalesIQTM__User_Access_Permission__c(id=mapUSerPosTeamInsKey2Id.get(concat));
                    if(posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.User_Type__c == 'Sales')
                    {
                        uapExist.AxtriaSalesIQST__isCallPlanEnabled__c = true;
                        uapExist.Enable_Call_Plan_Report__c = true;
                    }
                    uapExist.AxtriaSalesIQTM__is_Active__c = true;

                    if(!duplicateKey.contains(concat))
                    {
                        allUAPUpdate.add(uapExist);
                        duplicateKey.add(concat);
                        system.debug('+++++++++++ Uap Exidts12'+ uapExist);
                        system.debug('+++++++++++ Uap Exidts12Insert'+ duplicateKey);
                    }
                }
            }
        }
        
        system.debug('+++++++++++ Hey'+ allUAPInsert);
        if(allUAPInsert.size() > 0)
            insert allUAPInsert;
        if(allUAPUpdate.size() > 0)
            update allUAPUpdate;
        
        
    }
    
    public void dummyFunction(){
    Integer  i = 0;
    i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
    }

    global void finish(Database.BatchableContext BC) {

    }
}