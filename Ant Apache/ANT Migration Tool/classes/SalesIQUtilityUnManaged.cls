public with sharing class SalesIQUtilityUnManaged {    
     public static list < AxtriaARSnT__SIQ_Position_Employee_O__c > fetchOrgMasterPositionEmployee(map < string, list < AxtriaSalesIQTM__Organization_Master__c >> mapC2OM, Set < String > allMks) {
         map < string, set < string >> mapAddPubGrp2PRID = new map < string, set < string >> ();
         list < AxtriaSalesIQTM__Country__c > lsCountry = new list < AxtriaSalesIQTM__Country__c > ();
         map < string, list < AxtriaSalesIQTM__Organization_Master__c >> mapCountry2OrgMaster = new map < string, list < AxtriaSalesIQTM__Organization_Master__c >> ();
         map < string, list < AxtriaSalesIQTM__Team_Instance__c >> mapCon2TeamInstances = new map < string, list < AxtriaSalesIQTM__Team_Instance__c >> ();
         list < AxtriaSalesIQTM__Organization_Master__c > allOrgMasters = new list < AxtriaSalesIQTM__Organization_Master__c > ();
         set < string > setCountryToSend = new set < string > ();
         Date today = Date.today();
         mapCountry2OrgMaster = mapC2OM;
         map < string, string > usercountry = new map < string, string > ();

         for (list < AxtriaSalesIQTM__Organization_Master__c > rec: mapC2OM.values()) {
             allOrgMasters.addAll(rec);
         }
         system.debug('=================mapCountry2OrgMaster::' + mapCountry2OrgMaster);
         system.debug('============allMks:11111111::::::' + allMks);
         //exclude countries mentioned in the custom setting
         set < String > mkt = new set < String > ();
         list < AxtriaARSnT__Custom_Scheduler__c > mktList = new list < AxtriaARSnT__Custom_Scheduler__c > ();
         mktList = AxtriaARSnT__Custom_Scheduler__c.getall().values();
         list < AxtriaARSnT__Custom_Scheduler__c > lsCustomSchedulerUpdate = new list < AxtriaARSnT__Custom_Scheduler__c > ();

         for (AxtriaARSnT__Custom_Scheduler__c cs: mktList) {
             if (cs.AxtriaARSnT__Status__c == true && cs.AxtriaARSnT__Schedule_date__c != null) {
                 if (cs.AxtriaARSnT__Schedule_date__c > today.addDays(1)) {
                     mkt.add(cs.AxtriaARSnT__Marketing_Code__c);
                 } else {
                     //update Custom scheduler record
                     cs.AxtriaARSnT__Status__c = False;
                     lsCustomSchedulerUpdate.add(cs);
                 }
             }
         }
         //-----------End------------------------
         if (mkt.size() == 0) {
             lsCountry = [SELECT id, AxtriaSalesIQTM__Parent_Organization__c from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Country_Code__c in: allMks];
         } else {
             lsCountry = [SELECT id, AxtriaSalesIQTM__Country_Code__c, AxtriaSalesIQTM__Parent_Organization__c from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Country_Code__c not in: mkt];
         }
         for (AxtriaSalesIQTM__Country__c conRec: lsCountry) {
             setCountryToSend.add(conRec.id);
         }

         //-------query current team instance------------------
         list < AxtriaSalesIQTM__Team_Instance__c > lsCurrentTeamIns = new list < AxtriaSalesIQTM__Team_Instance__c > ();
         lsCurrentTeamIns = [select id, name, AxtriaSalesIQTM__Team__r.name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, AxtriaSalesIQTM__Team__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c IN: setCountryToSend and AxtriaSalesIQTM__Alignment_Period__c = 'Current'
             and(AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'
                 OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published')
         ];


         map < string, string > mapPositionName2GroupID = new map < string, string > ();
         map < string, string > mapGroupID2PositionName = new map < string, string > ();
         for (AxtriaSalesIQTM__Organization_Master__c orgM: allOrgMasters) {
             //setPublicGroups.add(orgM.Public_Group_SFDC_id__c);
             if (orgM != null && orgM.AxtriaARSnT__Public_Group_SFDC_id__c != null) {
                 mapPositionName2GroupID.put(orgM.Name, orgM.AxtriaARSnT__Public_Group_SFDC_id__c);
                 mapGroupID2PositionName.put(orgM.AxtriaARSnT__Public_Group_SFDC_id__c, orgM.Name);
             }
         }
         system.debug('======================mapGroupID2PositionName:::' + mapGroupID2PositionName);
         system.debug('======================mapPositionName2GroupID:::' + mapPositionName2GroupID);

         //Query Public Group Members
         list < GroupMember > lsPublicGroups = new list < GroupMember > ();
         set < string > setUserIds = new set < string > ();
         map < string, set < string >> mapPublicGrp2lsPRIDs = new map < string, set < string >> ();
         map < string, string > mapUserId2PRID = new map < string, string > ();

         lsPublicGroups = [Select id, GroupId, UserOrGroupId from GroupMember where GroupId IN: mapPositionName2GroupID.values()];
         for (GroupMember GM1: lsPublicGroups) {
             setUserIds.add(GM1.UserOrGroupId);
         }
         system.debug('=============setUserIds==' + setUserIds);
         list < User > lsUsers = [select id, FederationIdentifier, Country from User where id in: setUserIds];
         for (User ur: lsUsers) {
             mapUserId2PRID.put(ur.id, ur.FederationIdentifier);
             usercountry.put(ur.FederationIdentifier, ur.country);
         }
         system.debug('=========lsPublicGroups.size():::' + lsPublicGroups.size());
         for (GroupMember GM: lsPublicGroups) {
             if (!mapPublicGrp2lsPRIDs.containsKey(GM.GroupId)) {
                 set < string > temp = new set < string > ();
                 temp.add(mapUserId2PRID.get(GM.UserOrGroupId));
                 String s = GM.GroupId;
                 if (s.length() == 18) {
                     string newid = String.valueOf(s).substring(0, 15);
                     mapPublicGrp2lsPRIDs.put(newid, temp);
                 } else {

                     mapPublicGrp2lsPRIDs.put(GM.GroupId, temp);
                 }
             } else {
                 String s = GM.GroupId;
                 if (s.length() == 18) {
                     string newid = String.valueOf(s).substring(0, 15);
                     mapPublicGrp2lsPRIDs.get(newid).add(mapUserId2PRID.get(GM.UserOrGroupId));

                 } else {
                     mapPublicGrp2lsPRIDs.get(GM.GroupId).add(mapUserId2PRID.get(GM.UserOrGroupId));
                 }
             }
         }
         system.debug('mapPublicGrp2lsPRIDs++' + mapPublicGrp2lsPRIDs);

         List < AxtriaSalesIQTM__Organization_Master__c > org = [select id, Name, AxtriaARSnT__Public_Group_SFDC_id__c from AxtriaSalesIQTM__Organization_Master__c where AxtriaARSnT__Public_Group_SFDC_id__c IN: mapPublicGrp2lsPRIDs.keySet() and AxtriaARSnT__Marketing_Code__c in: allMks];
         system.debug('---org---' + org);
         map < string, string > pubgrptoorg = new map < string, string > ();
         for (AxtriaSalesIQTM__Organization_Master__c OM: org) {
             pubgrptoorg.put(Om.AxtriaARSnT__Public_Group_SFDC_id__c, Om.Name);
         }

         list < AxtriaARSnT__SIQ_Position_Employee_O__c > PosEmpList = new list < AxtriaARSnT__SIQ_Position_Employee_O__c > ();

         system.debug('----publicorgname---' + pubgrptoorg);
         system.debug('======SG=====mapAddPubGrp2PRID   MAP IS:' + mapAddPubGrp2PRID);
         system.debug('====SG======mapPublicGrp2lsPRIDs::::::::' + mapPublicGrp2lsPRIDs);
         system.debug('===========lsCurrentTeamIns::' + lsCurrentTeamIns);
         for (string pubGrp: mapPublicGrp2lsPRIDs.keySet()) {
             if (pubGrp != null && mapPublicGrp2lsPRIDs.get(pubGrp) != null) {
                 string Pos = pubgrptoorg.get(pubGrp);
                 system.debug('===================POSCODE IS::' + pos);
                 system.debug('==========pubGrp:::::' + pubGrp);
                 for (string prid: mapPublicGrp2lsPRIDs.get(pubGrp)) {
                     for (AxtriaSalesIQTM__Team_Instance__c obj: lsCurrentTeamIns) {
                         if (usercountry.containskey(prid) && Pos != null) {
                             if (usercountry.get(prid) == obj.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c) {
                                 AxtriaARSnT__SIQ_Position_Employee_O__c pe = new AxtriaARSnT__SIQ_Position_Employee_O__c();
                                 pe.AxtriaARSnT__SIQ_ASSIGNMENT_START_DATE__c = obj.AxtriaSalesIQTM__IC_EffstartDate__c;
                                 pe.AxtriaARSnT__SIQ_ASSIGNMENT_END_DATE__c = obj.AxtriaSalesIQTM__IC_EffEndDate__c;
                                 pe.AxtriaARSnT__SIQ_ASSIGNMENT_TYPE__c = 'Primary';
                                 pe.AxtriaARSnT__SIQ_Country_Code__c = obj.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                                 pe.AxtriaARSnT__SIQ_Created_Date__c = system.now();
                                 pe.AxtriaARSnT__SIQ_EMP_ID__c = prid;
                                 pe.AxtriaARSnT__SIQ_POSITION_CODE__c = Pos;
                                 pe.AxtriaARSnT__SIQ_Team_Instance__c = obj.name;
                                 pe.AxtriaARSnT__SIQ_Updated_Date__c = system.now();
                                 pe.AxtriaARSnT__SIQ_isActive__c = 'Active';
                                 pe.AxtriaARSnT__Unique_Id__c = pubGrp.substring(0, 14) + prid + obj.name;
                                 pe.AxtriaARSnT__SIQ_Marketing_Code__c = obj.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
                                 pe.AxtriaARSnT__SIQ_Permanent_Position_Flag__c = 'Primary';
                                 pe.AxtriaARSnT__SIQ_Salesforce_Name__c = obj.AxtriaSalesIQTM__Team__r.name;
                                 pe.AxtriaARSnT__SIQ_Position_Type__c = 'Global Admins';
                                 PosEmpList.add(pe);
                             }
                         }
                     }
                 }
             }
         }

         system.debug('PosEmpList++' + PosEmpList);
         system.debug('mapAddPubGrp2PRID++' + mapAddPubGrp2PRID);
         return PosEmpList;
     }
    public static  map<string, list<AxtriaSalesIQTM__Organization_Master__c>> fetchOrganizationMasterRecords(DateTime lastBatchRun, Set<String> allMks){
        list<AxtriaSalesIQTM__Country__c>             lsCountry         = new list<AxtriaSalesIQTM__Country__c>();
        list<AxtriaSalesIQTM__Organization_Master__c> orgMasterRec      = new list<AxtriaSalesIQTM__Organization_Master__c>();
        map<string,string>                            mapCountry2ParentOrg  = new map<string,string>();
        map<string, list<AxtriaSalesIQTM__Organization_Master__c>> mapCon2LsOrgMasterRecords = new map<string, list<AxtriaSalesIQTM__Organization_Master__c>>();
        
        Map<String,AxtriaSalesIQTM__Organization_Master__c> organization     = new map<String,AxtriaSalesIQTM__Organization_Master__c>();
        Map<String,String>                                  orgmap           = new map<String,String>();
        Date today=Date.today();
        
        //exclude countries mentioned in the custom setting
        system.debug('============allMks:22222222222::::::'+allMks);
        set<String> mkt = new set<String>();
        list<AxtriaARSnT__Custom_Scheduler__c> mktList=new list<AxtriaARSnT__Custom_Scheduler__c>();
        mktList=AxtriaARSnT__Custom_Scheduler__c.getall().values();
        list<AxtriaARSnT__Custom_Scheduler__c> lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();
        
        for(AxtriaARSnT__Custom_Scheduler__c cs:mktList){
          if(cs.AxtriaARSnT__Status__c==true && cs.AxtriaARSnT__Schedule_date__c!=null){
            if(cs.AxtriaARSnT__Schedule_date__c>today.addDays(1)){
               mkt.add(cs.AxtriaARSnT__Marketing_Code__c);
            }else{
               //update Custom scheduler record
               cs.AxtriaARSnT__Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            }
          }
        }
        //-----------End------------------------
        if(mkt.size()==0){
            lsCountry = [SELECT id, AxtriaSalesIQTM__Parent_Organization__c from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Country_Code__c in :allMks];
        }else{
            lsCountry = [SELECT id,AxtriaSalesIQTM__Country_Code__c, AxtriaSalesIQTM__Parent_Organization__c from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Country_Code__c not in: mkt ];
        }
        for(AxtriaSalesIQTM__Country__c conRec : lsCountry){
            mapCountry2ParentOrg.put(conRec.id,conRec.AxtriaSalesIQTM__Parent_Organization__c);
        }
        
        if(lastBatchRun!=null){
            orgMasterRec = [select id,Name,AxtriaARSnT__Public_Group_SFDC_id__c,AxtriaARSnT__Public_Group_link__c,AxtriaARSnT__Public_Group__c,AxtriaSalesIQTM__Parent_Organization_Name__c,AxtriaSalesIQTM__Parent_Organization_Name__r.Name from AxtriaSalesIQTM__Organization_Master__c where LastModifiedDate >= :lastBatchRun and AxtriaARSnT__Marketing_Code__c in :allMks];
            
        }else{
            orgMasterRec = [select id,Name,AxtriaARSnT__Public_Group_SFDC_id__c,AxtriaARSnT__Public_Group_link__c,AxtriaARSnT__Public_Group__c,AxtriaSalesIQTM__Parent_Organization_Name__c,AxtriaSalesIQTM__Parent_Organization_Name__r.Name from AxtriaSalesIQTM__Organization_Master__c where AxtriaARSnT__Marketing_Code__c in :allMks];
            
        }
        for(AxtriaSalesIQTM__Organization_Master__c org : orgMasterRec){
            orgmap.put(org.id,org.AxtriaSalesIQTM__Parent_Organization_Name__c);
            organization.put(org.id,org);
        }
        system.debug('orgmap+'+orgmap);
        system.debug('organization+'+organization);
        
        
        
        system.debug('mapCountry2ParentOrg+'+mapCountry2ParentOrg);
        for(string con: mapCountry2ParentOrg.keySet()){
            system.debug('inside for+con+'+con);
            string parentOrgInCountry = mapCountry2ParentOrg.get(con);
            system.debug('inside for+parent+'+parentOrgInCountry);
            for(AxtriaSalesIQTM__Organization_Master__c OM : orgMasterRec){
                string org = parentOrgInCountry;
                system.debug('org+'+org);
                system.debug('OM.id-----'+OM.id);
                if(OM.id == parentOrgInCountry){
                    system.debug('inside if+'+OM);
                    if(!mapCon2LsOrgMasterRecords.containsKey(con)){
                        list<AxtriaSalesIQTM__Organization_Master__c> temp = new list<AxtriaSalesIQTM__Organization_Master__c>();
                        temp.add(OM);
                        mapCon2LsOrgMasterRecords.put(con,temp);
                        mapCon2LsOrgMasterRecords.get(con).add(organization.get(orgmap.get(org))); //Add its immediate parent here itself
                    }else{
                        mapCon2LsOrgMasterRecords.get(con).add(OM);
                        mapCon2LsOrgMasterRecords.get(con).add(organization.get(orgmap.get(org))); //Add its immediate parent here itself
                    }
                    
                    org = orgmap.get(org);
                    system.debug('org+'+org);
                    while(orgmap.get(org)!=null){
                        system.debug('inside while');
                        /*if(!mapCon2LsOrgMasterRecords.containsKey(con)){
                            list<AxtriaSalesIQTM__Organization_Master__c> temp = new list<AxtriaSalesIQTM__Organization_Master__c>();
                            temp.add(organization.get(orgmap.get(org)));
                            mapCon2LsOrgMasterRecords.put(con,temp);
                        }else{*/
                            
                            mapCon2LsOrgMasterRecords.get(con).add(organization.get(orgmap.get(org)));
                        //}
                        org = orgmap.get(org);
                    }
                }
            }
        }
        //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }  
        system.debug('mapCon2LsOrgMasterRecords+'+mapCon2LsOrgMasterRecords);
        //fetchOrgMasterPositionEmployee(mapCon2LsOrgMasterRecords);
        return mapCon2LsOrgMasterRecords;
     }
     
}