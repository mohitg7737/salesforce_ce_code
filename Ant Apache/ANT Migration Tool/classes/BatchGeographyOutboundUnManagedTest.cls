@IsTest(SeeAllData=true)
private class BatchGeographyOutboundUnManagedTest {
     
    static testmethod void BatchGeographyOutboundUnManaged() {
    
    
    String countrycode;
    map<string,set<string>>geotype2teammap = new map<string,set<string>>();
    map<string,set<string>>team2teaminstance = new map<string,set<string>>();
    set<string>geotype1 = new set<string>();
    map<String,String>Countrymap = new map<String,String>();
 
        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;
        
        
        
        list<AxtriaSalesIQTM__Team_Instance__c> tt = new list<AxtriaSalesIQTM__Team_Instance__c>();
       
        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
        
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active',Name='USA'));
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='ES',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active',Name='Spain'));

        insert country;
        
        countrycode=country[0].AxtriaSalesIQTM__Country_Code__c;
        
        country[0].AxtriaSalesIQTM__Country_Code__c=',';
        
        AxtriaARSnT__SIQ_MC_Country_Mapping__c MC = new AxtriaARSnT__SIQ_MC_Country_Mapping__c ();
        MC.AxtriaARSnT__SIQ_Veeva_Country_Code__c=country[0].AxtriaSalesIQTM__Country_Code__c;
        
        AxtriaSalesIQTM__Team__c TN = new AxtriaSalesIQTM__Team__c();
TN.Name='ABC';
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today()-1;
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today();
TN.AxtriaSalesIQTM__Alignment_Type__c='Account';
TN.AxtriaSalesIQTM__Team_Code__c='TE01111';
TN.AxtriaSalesIQTM__Country__c=country[0].Id;
TN.AxtriaSalesIQTM__Type__c='BASE';
    insert TN;
        
        AxtriaSalesIQTM__Team_Instance__c  TI = new AxtriaSalesIQTM__Team_Instance__c ();
    TI.Name='ESABCD';
    TI.AxtriaSalesIQTM__Team__c=TN.id;
    insert TI;
    
        AxtriaSalesIQTM__Geography_Type__c geotype = new AxtriaSalesIQTM__Geography_Type__c();
        geotype.Name = 'ZIP';
        geotype.AxtriaSalesIQTM__Country__c = country[0].Id;
        insert geotype;
        
        AxtriaSalesIQTM__Geography__c geo=new AxtriaSalesIQTM__Geography__c();
        geo.AxtriaARSnT__Team_Name__c=TN.Name;
        geo.AxtriaSalesIQTM__External_Country_Id__c=country[0].Id;
        geo.AxtriaSalesIQTM__Geography_Type__c = geotype.id;
        geo.AxtriaSalesIQTM__Zip_Name__c='1';
        geo.AxtriaSalesIQTM__Neighbor_Geography__c='0';
        geo.AxtriaSalesIQTM__Parent_Zip__c='yyyyyy';
        geo.AxtriaSalesIQTM__Zip_Type__c='Minibrick';
        geo.AxtriaSalesIQTM__External_Geo_Type__c=geotype.id;
        insert geo;
        
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        sJob.AxtriaARSnT__Job_Name__c = 'OutBound Geography Delta';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Outbound';
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
        sJob.AxtriaARSnT__No_of_Records_Processed__c=2000;
        insert sJob;
        
                        AxtriaARSnT__SIQ_Geography_O__c obj = new AxtriaARSnT__SIQ_Geography_O__c(); 
                        obj.AxtriaARSnT__SIQ_Team__c=TN.name;
                        obj.AxtriaARSnT__SIQ_Team_Instance__c=TI.name;
                        obj.AxtriaARSnT__SIQ_ZIP__c='Name';
                        obj.AxtriaARSnT__SIQ_ZIP_Name__c=geo.name;
                        obj.AxtriaARSnT__SIQ_Parent_ZIP_c__c=geo.name;
                        obj.AxtriaARSnT__SIQ_ZIP_Type__c=geo.AxtriaSalesIQTM__Zip_type__c;
                        obj.AxtriaARSnT__SIQ_Created_Dat__c= DateTime.now();
                        obj.AxtriaARSnT__SIQ_Updated_Dat__c = DateTime.now();
                        obj.AxtriaARSnT__Unique_Id__c='TN.name'+'_'+'geo.id';
                        upsert obj AxtriaARSnT__Unique_Id__c;
        
    }
    static testmethod void test() {        
        Test.startTest();
       // DummyClassCoverage t = new DummyClassCoverage ();
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchGeographyOutboundUnManaged usa = new BatchGeographyOutboundUnManaged();
        usa.dummyFunction();
        
        Id batchId = Database.executeBatch(usa);
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    
    }
}