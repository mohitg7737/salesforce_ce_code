@isTest
private class BatchOutboundAllLevelPGUnManagedTest {
     @testSetup 
    static void setup() {
        
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        sJob.AxtriaARSnT__Job_Name__c = 'Position Account';
        sJob.AxtriaARSnT__Job_Status__c = 'failed';
        sJob.AxtriaARSnT__Job_Type__c='Outbound';
        sJob.AxtriaARSnT__Cycle__c='Sc2019';
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();

         sjob.AxtriaARSnT__Object_Name__c = 'Position Account';

        sjob.AxtriaARSnT__Job_Status__c='Successful';

        insert sJob;

        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;

        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active'));
        insert country;

        AxtriaSalesIQTM__Country__c Contr = new AxtriaSalesIQTM__Country__c();
        Contr.name= 'USA';
        Contr.AxtriaSalesIQTM__Parent_Organization__c= orgId;
        contr.CurrencyIsoCode= 'USD';
        contr.AxtriaSalesIQTM__Status__c= 'Active';
        insert contr;

        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;

        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;

        Schema.DescribeFieldResult Pickvalue= AxtriaSalesIQTM__Position__c.AxtriaARSnT__Sales_Team_Attribute_MS__c.getDescribe();
       List<Schema.PicklistEntry> PickListValue = Pickvalue.getPicklistValues();

         AxtriaSalesIQTM__Position__c pos1=new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        //pos1.AxtriaARSnT__Sales_Team_Attribute_MS__c = 'BH_SALES_ALLTA';
        pos1.AxtriaARSnT__Channel_AZ__c = 'MR';
        pos1.AxtriaARSnT__Position_Description__c = 'hhhh';
        Pos1.AxtriaARSnT__Sales_Team_Attribute_MS__c = String.valueof(PickListValue[0].getValue());
        Pos1.AxtriaARSnT__Channel_Id_AZ__c= 'MR';
        insert pos1;
        String parentposId=pos1.id;

        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Parent_Position__c=parentposId;
        //pos.AxtriaARSnT__Sales_Team_Attribute_MS__c = 'BH_SALES_ALLTA';
        pos.AxtriaARSnT__Channel_AZ__c = 'MR';
        Pos.AxtriaARSnT__Channel_Id_AZ__c= 'MR';
        pos.AxtriaARSnT__Position_Description__c = 'hhhh';
        Pos.AxtriaARSnT__Sales_Team_Attribute_MS__c = String.valueof(PickListValue[0].getValue());
        insert pos;
        String posId=pos.id;

        Account ACC = new Account();
        ACC.name= 'Katie Elizabeth Dennis';
        ACC.AxtriaARSnT__Marketing_Code__c= 'GB';
        ACC.CurrencyIsoCode= 'USD';
        ACC.Type = 'HCA';
        ACC.AxtriaSalesIQTM__Country__c = contr.id;
        insert ACC;
         
        system.debug('ACC^^^^^^^^^^'+ACC.id);

      AxtriaSalesIQTM__Position_Team_Instance__c PTI = new AxtriaSalesIQTM__Position_Team_Instance__c();
       PTI.AxtriaSalesIQTM__Effective_Start_Date__c = System.today();
       PTI.AxtriaSalesIQTM__Effective_End_Date__c = System.today()+1;
       PTI.AxtriaSalesIQTM__Position_ID__c = Pos.id;
       PTI.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstId;
       PTI.CurrencyIsoCode = 'USD';
       Insert PTI;
       system.debug('PTI^^^^^^^^^^'+PTI.id);

        String code = ' ';
        set<String> Uniqueset;
        Uniqueset = new set<String>();
        list<AxtriaSalesIQTM__Position_Account__c> insertnewPosAccount = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(Integer i = 0; i <1000 ; i ++)
         {
           
           String key = posId;

           AxtriaSalesIQTM__Position_Account__c newobj = new AxtriaSalesIQTM__Position_Account__c();
           newobj.AxtriaSalesIQTM__Position__c = posId;
           newobj.AxtriaARSnT__Accessibility_Range__c = '9999';
           newobj.AxtriaARSnT__isDirectLoad__c = 'TRUE';
           newobj.AxtriaSalesIQTM__Account__c = ACC.id;
           newobj.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
           newobj.AxtriaSalesIQTM__Effective_End_Date__c = System.today()+1;
           newobj.AxtriaSalesIQTM__Effective_Start_Date__c = System.today();
           newobj.AxtriaSalesIQTM__Team_Instance__c = teamInstId;
           newobj.AxtriaSalesIQTM__Segment_1__c= 'Primary';
           newobj.AxtriaSalesIQTM__Position_Team_Instance__c = PTI.id;
           newobj.AxtriaSalesIQTM__Proposed_Position__c = posId;

      /*     Str1 = String.valueof(newobj.AxtriaSalesIQTM__Account__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Position__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Team_Instance__c).substring(0, 15)+'_'+'Active';*/

           insertnewPosAccount.add(newobj);

        }

        insert insertnewPosAccount;

        //AxtriaSalesIQTM__Position_Account__c pa=new AxtriaSalesIQTM__Position_Account__c();
        //pa.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        //pa.AxtriaSalesIQTM__Position__c=posId;
        //insert pa;
        
       
    }
    static testmethod void test() {
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;   

        Test.startTest();
        BatchOutboundAllLevelPGUnManaged usa = new BatchOutboundAllLevelPGUnManaged();
        usa.dummyFunction();
        usa.dummy2Func();
        Id batchId = Database.executeBatch(usa);
        
        Test.stopTest();

    }
}