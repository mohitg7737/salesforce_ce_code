global class BatchOutBoundPositionProductUnManaged implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    public String query;
    public list<AxtriaSalesIQTM__Position_Product__c>posrodlist ;
    public map<String,String>Countrymap {get;set;}
    public map<string,string>mcmap {get;set;}
    public List<String> posCodeList=new List<String>();
    public set<String> mkt {get;set;} 
    public list<AxtriaARSnT__Custom_Scheduler__c> mktList {get;set;}                                       
    public List<AxtriaSalesIQTM__Position__c> nationPosition {get;set;}  
    public list<AxtriaARSnT__Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;} 
    public String cycle {get;set;}
    global Date today=Date.today();
    global DateTime lastjobDate=null;
    public String batchID;
    public Integer recordsProcessed=0;
    global boolean flag=true;
    global string Country_1;
    public list<String> CountryList;
    public  List<String> DeltaCountry ;

    
    global BatchOutBoundPositionProductUnManaged(String Country1){

        Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        flag=false;
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        mkt = new set<String>();
        mktList=new list<AxtriaARSnT__Custom_Scheduler__c>();
        mktList=AxtriaARSnT__Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();
        Countrymap = new map<string,string>();
        mcmap = new map<string,string>();
        
        

        query='select id,CreatedDate,LastModifiedDate,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaARSnT__Product_Catalog__c,AxtriaARSnT__Product_Catalog__r.Name,AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c,AxtriaSalesIQTM__isActive__c from AxtriaSalesIQTM__Position_Product__c'+
        ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :CountryList and AxtriaARSnT__Product_Catalog__c!=null and AxtriaSalesIQTM__Position__c != null and AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c != null ';
        
         System.debug('====================query'+ query);

                
        list<AxtriaSalesIQTM__Country__c>CountryList = [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        list<AxtriaARSnT__SIQ_MC_Country_Mapping__c>SIQCountry = [SELECT AxtriaARSnT__SIQ_BOT_Market_Code__c,AxtriaARSnT__SIQ_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c,AxtriaARSnT__SIQ_Veeva_Country_Code__c FROM AxtriaARSnT__SIQ_MC_Country_Mapping__c];

        //Fill the maps
        for(AxtriaSalesIQTM__Country__c country : CountryList){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(Country.Name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(SIQCountry !=null && SIQCountry.size() >0){
            for(AxtriaARSnT__SIQ_MC_Country_Mapping__c mc : SIQCountry){
                mcmap.put(mc.AxtriaARSnT__SIQ_Veeva_Country_Code__c ,mc.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }
       
        
        
    }



    global BatchOutBoundPositionProductUnManaged() {
        //query='';
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        mkt = new set<String>();
        mktList=new list<AxtriaARSnT__Custom_Scheduler__c>();
        mktList=AxtriaARSnT__Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();
        Countrymap = new map<string,string>();
        mcmap = new map<string,string>();
        
        for(AxtriaARSnT__Custom_Scheduler__c cs:mktList){
          if(cs.AxtriaARSnT__Status__c==true && cs.AxtriaARSnT__Schedule_date__c!=null){
            if(cs.AxtriaARSnT__Schedule_date__c>today.addDays(1)){
               mkt.add(cs.AxtriaARSnT__Marketing_Code__c);
            }else{
               //update Custom scheduler record
               cs.AxtriaARSnT__Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            }
          }
        }
        List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name,AxtriaARSnT__Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
        if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.AxtriaARSnT__Cycle__r.Name !=null && t1.AxtriaARSnT__Cycle__r.Name !='')
                    cycle = t1.AxtriaARSnT__Cycle__r.Name;
            }
            
        }
        schLogList=[Select Id,CreatedDate,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c='OutBound Position Product Delta' and AxtriaARSnT__Job_Status__c='Successful' Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].AxtriaARSnT__Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceListUnManaged.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>' +DeltaCountry);
        

        query='select id,CreatedDate,LastModifiedDate,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaARSnT__Product_Catalog__c,AxtriaARSnT__Product_Catalog__r.Name,AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c,AxtriaSalesIQTM__isActive__c from AxtriaSalesIQTM__Position_Product__c'+
        ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Position__c != null and AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c != null and AxtriaARSnT__Product_Catalog__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c IN: DeltaCountry ';
        
        if(lastjobDate!=null){
            query = query + ' and LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        this.query = query;
        
        list<AxtriaSalesIQTM__Country__c>CountryList = [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        list<AxtriaARSnT__SIQ_MC_Country_Mapping__c>SIQCountry = [SELECT AxtriaARSnT__SIQ_BOT_Market_Code__c,AxtriaARSnT__SIQ_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c,AxtriaARSnT__SIQ_Veeva_Country_Code__c FROM AxtriaARSnT__SIQ_MC_Country_Mapping__c];

        //Fill the maps
        for(AxtriaSalesIQTM__Country__c country : CountryList){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(Country.Name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(SIQCountry !=null && SIQCountry.size() >0){
            for(AxtriaARSnT__SIQ_MC_Country_Mapping__c mc : SIQCountry){
                mcmap.put(mc.AxtriaARSnT__SIQ_Veeva_Country_Code__c ,mc.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        
        sJob.AxtriaARSnT__Job_Name__c = 'OutBound Position Product Delta';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Outbound';
         if(cycle!=null && cycle!='')
         sJob.AxtriaARSnT__Cycle__c=cycle;
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Product__c> scope) {
        List<AxtriaARSnT__SIQ_Position_Product_O__c> Posproduct = new List<AxtriaARSnT__SIQ_Position_Product_O__c>();
        set<string>uniqueset = new set<string>();
        for(AxtriaSalesIQTM__Position_Product__c pos : scope){
            string key = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c)+'_'+ pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            if(!uniqueset.contains(key)){
                AxtriaARSnT__SIQ_Position_Product_O__c obj = new AxtriaARSnT__SIQ_Position_Product_O__c();
                obj.AxtriaARSnT__SIQ_Position_ID__c = pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                obj.AxtriaARSnT__SIQ_Product_Id__c = pos.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c;
                obj.AxtriaARSnT__SIQ_Product_Name__c = pos.AxtriaARSnT__Product_Catalog__r.Name;
                obj.AxtriaARSnT__SIQ_Salesforce_Name__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                obj.AxtriaARSnT__SIQ_Team_Instance__c = pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                obj.AxtriaARSnT__SIQ_Team_Name__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                obj.AxtriaARSnT__SIQ_Created_Date__c = pos.CreatedDate;
                obj.AxtriaARSnT__SIQ_Updated_Date__c = pos.LastModifiedDate;
                obj.AxtriaARSnT__SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                if(pos.AxtriaSalesIQTM__isActive__c == true){
                    obj.AxtriaARSnT__SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c; 
                }
                else
                {
                    obj.AxtriaARSnT__SIQ_Effective_End_Date__c=System.today().addDays(-1); 
                }
                
                obj.AxtriaARSnT__SIQ_Country_Code__c = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c);
                if(mcmap!=null ){
                    String Countrycode = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c);
                    obj.AxtriaARSnT__SIQ_Marketing_Code__c = mcmap.get(Countrycode) !=null ? mcmap.get(Countrycode) : Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c);
                }
                obj.AxtriaARSnT__Unique_Id__c =Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c)+'_'+ pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.AxtriaARSnT__Product_Catalog__r.AxtriaARSnT__Veeva_External_ID__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                Posproduct.add(obj);
                uniqueset.add(key);
                recordsProcessed++;
            }

        }
        if(Posproduct!=null && Posproduct.size()>0)
            Upsert Posproduct AxtriaARSnT__Unique_Id__c;
        
    }

    global void finish(Database.BatchableContext BC) {
        if(flag)
        {
            System.debug(recordsProcessed + ' records processed. ');
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
        sJob.AxtriaARSnT__Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
        
        Set<String> updMkt = new set<String>();
         for(AxtriaARSnT__Custom_Scheduler__c cs:mktList){
           if(cs.AxtriaARSnT__Status__c==true && cs.AxtriaARSnT__Schedule_date__c!=null){
             if(cs.AxtriaARSnT__Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.AxtriaARSnT__Marketing_Code__c);
             }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position_Product__c> posList=[Select Id FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update posList;
        }
        }

    }

    global void execute(System.SchedulableContext SC){
       //PositionProductOutBound ppo=new PositionProductOutBound ();
        Database.executeBatch(new BatchOutBoundPositionProductUnManaged(), 2000);
    }
    public void dummyFunction()
    {
      Integer i = 0;
      i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    } 
}