public with sharing class StaticTeaminstanceListUnManaged {


    public static list<string> filldata(){
        list<string>teaminstancelist = new list<string>();
        set<string>teaminstanceset = new set<string>();
        list<AxtriaSalesIQTM__Team_Instance__c>teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published') ];

        if(teamlist!=null && teamlist.size()>0){
            for(AxtriaSalesIQTM__Team_Instance__c ti:teamlist){
                teaminstanceset.add(ti.id);
            }
            teaminstancelist.addall(teaminstanceset);
        }
        
        return teaminstancelist;

    }

    public static list<string> filldataCallPlan(){
        list<string>teaminstancelist = new list<string>();
        set<string>teaminstanceset = new set<string>();
        list<AxtriaSalesIQTM__Team_Instance__c>teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published') and AxtriaSalesIQTM__Team__r.AxtriaARSnT__hasCallPlan__c = true ];

        if(teamlist!=null && teamlist.size()>0){
            for(AxtriaSalesIQTM__Team_Instance__c ti:teamlist){
                teaminstanceset.add(ti.id);
            }
            teaminstancelist.addall(teaminstanceset);
        }
        
        return teaminstancelist;

    }

     public static List<string> getAllCountries(){
        list<string>countryList = new list<string>();
        set<string>countrySet = new set<string>();   
        
        list<AxtriaSalesIQTM__Team_Instance__c>teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and  (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published' ) ];

        if(teamlist!=null && teamlist.size()>0){
            for(AxtriaSalesIQTM__Team_Instance__c ti:teamlist){
                countrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
            }
            countryList.addall(countrySet);
        }
        
        return countryList;

    }
    
    
    public static List<string> getCountries(){
        list<string>countryList = new list<string>();
        set<string>countrySet = new set<string>();
        set<string> veevaCountrySet= new set<string>();
        
        for( AxtriaARSnT__Veeva_Market_Specific__c  veevaMarket :[select id,AxtriaARSnT__market__c,AxtriaARSnT__Add_Alignment_in_TSF__c FROM AxtriaARSnT__Veeva_Market_Specific__c where AxtriaARSnT__Add_Alignment_in_TSF__c =true]) 
        {
             veevaCountrySet.add(veevaMarket.AxtriaARSnT__market__c);
        }
        
        
        list<AxtriaSalesIQTM__Team_Instance__c>teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and AxtriaARSnT__Country_Name__c in: veevaCountrySet and  (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published' ) ];

        if(teamlist!=null && teamlist.size()>0){
            for(AxtriaSalesIQTM__Team_Instance__c ti:teamlist){
                countrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
            }
            countryList.addall(countrySet);
        }
        
        return countryList;

    }

    public static List<String> getDeltaCountries()
    {
        List<AxtriaARSnT__Veeva_Job_Scheduling__c> allCountries = [select id, AxtriaARSnT__Country__c from AxtriaARSnT__Veeva_Job_Scheduling__c where AxtriaARSnT__Load_Type__c = 'Delta'];

        List<String> allCountriesList = new List<String>();

        for(AxtriaARSnT__Veeva_Job_Scheduling__c vcs : allCountries)
        {
            allCountriesList.add(vcs.AxtriaARSnT__Country__c);
        }

        return allCountriesList;
    }

    //New added class by Ayushi

    public static Set<String> getCountriesSet()
    {
        List<AxtriaARSnT__Veeva_Job_Scheduling__c> allCountries =[select id, AxtriaARSnT__Country__c from AxtriaARSnT__Veeva_Job_Scheduling__c where AxtriaARSnT__Load_Type__c = 'Delta' or AxtriaARSnT__Load_Type__c = 'Full Load'];

        Set<String> allCountriesList = new Set<String>();

        for(AxtriaARSnT__Veeva_Job_Scheduling__c vcs : allCountries)
        {
            allCountriesList.add(vcs.AxtriaARSnT__Country__c);
        }

        return allCountriesList;
    }

    //till here................

    public static List<String> getFulloadCountries()
    {
        List<AxtriaARSnT__Veeva_Job_Scheduling__c> allCountries = [select id, AxtriaARSnT__Country__c from AxtriaARSnT__Veeva_Job_Scheduling__c where AxtriaARSnT__Load_Type__c = 'Full Load'];

        List<String> allCountriesList = new List<String>();

        for(AxtriaARSnT__Veeva_Job_Scheduling__c vcs : allCountries)
        {
            allCountriesList.add(vcs.AxtriaARSnT__Country__c);
        }

        return allCountriesList;
    }

    public static List<String> getDeltaTeamInstances()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getDeltaCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getFullLoadTeamInstances()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getFulloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getDeltaTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getDeltaCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.AxtriaARSnT__hasCallPlan__c = true])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getFullLoadTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getFulloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.AxtriaARSnT__hasCallPlan__c = true])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static Set<String> getCompleteRuleTeamInstances()
    {
        Set<String> teaminstancelist = new Set<String>();
        List<AxtriaARSnT__Measure_Master__c> mm= [Select id,AxtriaARSnT__State__c,AxtriaARSnT__Team_Instance__c from AxtriaARSnT__Measure_Master__c where AxtriaARSnT__State__c='Complete'];
        if(mm!=null && mm.size()>0)
        {
           for(AxtriaARSnT__Measure_Master__c measure:mm)
            {
                teaminstancelist.add(measure.AxtriaARSnT__Team_Instance__c);
            } 
        }
        return teaminstancelist;
    }
    
    public static List<String> getSFEDeltaCountries()
    {
         List<String> DeltaCountry = new List<String>();

        for(AxtriaARSnT__Veeva_Job_Scheduling__c vjs : [select AxtriaARSnT__Country_Name__c from AxtriaARSnT__Veeva_Job_Scheduling__c where AxtriaARSnT__SFE_Load__c = 'Delta Load']){
            DeltaCountry.add(vjs.AxtriaARSnT__Country_Name__c);
        }

        return DeltaCountry;
    }
}