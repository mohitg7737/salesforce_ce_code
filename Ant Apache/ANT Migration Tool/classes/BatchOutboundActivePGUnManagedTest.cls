@IsTest
public with sharing class BatchOutboundActivePGUnManagedTest {
    
    static void testPositionGeographyInsert(){
    
        AxtriaARSnT__Scheduler_Log__c SL = new AxtriaARSnT__Scheduler_Log__c();
       SL.AxtriaARSnT__Job_Name__c = 'OutBound Position Geography Delta';
        SL.AxtriaARSnT__Job_Status__c = 'Failed';
        SL.AxtriaARSnT__Job_Type__c='Outbound';
        insert sL;
        system.debug('Sche::::::::'+SL);
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'name';
        geo.AxtriaSalesIQTM__City__c='Test 11788';
        geo.AxtriaSalesIQTM__State__c = '11788';
        geo.AxtriaSalesIQTM__Centroid_Latitude__c = 0.0;
        geo.AxtriaSalesIQTM__Centroid_Longitude__c = 0.0;
        geo.AxtriaSalesIQTM__Zip_Type__c='ziptype';
        insert geo; 
        system.debug('geo::::::::'+ geo );  
        
        AxtriaSalesIQTM__Team__c Team = new AxtriaSalesIQTM__Team__c();
        Team.Name='HTN';
        Team.AxtriaSalesIQTM__Type__c ='Type'; 
        Team.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addMonths(50);
        Insert Team;
        system.debug('Team::::::::'+ Team); 
        
        AxtriaSalesIQTM__Team_Instance__c TeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        TeamInstance.name = 'HTN_Q1_2016';
        TeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        TeamInstance.AxtriaSalesIQTM__Team__c = Team.id;
        TeamInstance.AxtriaSalesIQTM__Alignment_Type__c = 'Type';
        TeamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        TeamInstance.AxtriaSalesIQTM__Team_Instance_Code__c = 'DI-00001'; 
        insert TeamInstance;
        system.debug('TeamInstance::::::::'+ TeamInstance); 
        
        AxtriaSalesIQTM__Position__c pos= new AxtriaSalesIQTM__Position__c();
        pos.name = 'Test Region';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'Test Region';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30001';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c='1NE30001';
        pos.AxtriaSalesIQTM__Position_Type__c='Region';
        pos.AxtriaSalesIQTM__inactive__c = false;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c  =date.today()-5;
        pos.AxtriaSalesIQTM__Effective_End_Date__c  =date.today()+5;
        pos.AxtriaSalesIQTM__Related_Position_Type__c  ='Base';
        Insert pos;
        system.debug('pos::::::::'+ pos);
        
        
        AxtriaSalesIQTM__Position_Team_Instance__c pti = new AxtriaSalesIQTM__Position_Team_Instance__c();
        pti.AxtriaSalesIQTM__Position_ID__c = pos.id;
        pti.AxtriaSalesIQTM__Effective_End_Date__c  = Date.newInstance(2018,1,1);
        pti.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.newInstance(2015,1,1);
        pti.AxtriaSalesIQTM__Team_Instance_ID__c = TeamInstance.id;
        pti.AxtriaSalesIQTM__X_Max__c=-72.6966429900;
        pti.AxtriaSalesIQTM__X_Min__c=-73.9625820000;
        pti.AxtriaSalesIQTM__Y_Max__c=40.9666490000;
        pti.AxtriaSalesIQTM__Y_Min__c=40.5821279800;
        insert pti;
        system.debug('pti::::::::'+ pti);
        


        AxtriaSalesIQTM__Position_Geography__c Pg  = new AxtriaSalesIQTM__Position_Geography__c();
        pg.AxtriaSalesIQTM__Effective_Start_Date__c  = Date.today().addMonths(-1);
        pg.AxtriaSalesIQTM__Effective_End_Date__c  = Date.today().addYears(1);
        pg.AxtriaSalesIQTM__Geography__c = geo.id;
        pg.AxtriaSalesIQTM__Position__c = pos.id;
        pg.AxtriaSalesIQTM__Team_Instance__c =TeamInstance.id ;
        pg.AxtriaSalesIQTM__Position_Team_Instance__c  =pti.id;
        pg.AxtriaSalesIQTM__Change_Status__c = 'Approved';
        pg.AxtriaSalesIQTM__Metric1__c  =2.010000000000;
        pg.AxtriaSalesIQTM__Metric2__c  =29.000000000000;
        pg.AxtriaSalesIQTM__Metric3__c  =2.000000000000;
        pg.AxtriaSalesIQTM__Metric4__c  =1.431479545000;
        pg.AxtriaSalesIQTM__Metric5__c  =1.490000000000;
        pg.AxtriaSalesIQTM__Metric6__c  =2.010000000000;
        pg.AxtriaSalesIQTM__Metric7__c  =29.000000000000;
        pg.AxtriaSalesIQTM__Metric8__c  =2.000000000000;
        pg.AxtriaSalesIQTM__Metric9__c  =1.431479545000;
        pg.AxtriaSalesIQTM__Metric10__c =1.490000000000;
        Insert pg;
        system.debug('pg::::::::'+ pg);
    }
    
    static testmethod void test() {    
            Test.startTest();
            BatchOutboundActivePGUnManaged usa = new BatchOutboundActivePGUnManaged();
            usa.dummyFunction();
            usa.dummy2Func();
            Id batchId = Database.executeBatch(usa);
            Test.stopTest();
        
    }
    
    
}