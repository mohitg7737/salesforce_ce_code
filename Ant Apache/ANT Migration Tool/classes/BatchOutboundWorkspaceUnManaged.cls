global class  BatchOutboundWorkspaceUnManaged implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global Date today=null;
     global String query;
     public map<String,String>Countrymap {get;set;}
     public map<String,String>mapVeeva2Mktcode {get;set;}
      public set<String> Uniqueset {get;set;}
      public String cycle {get;set;}
      global boolean flag=true;
      global string Country_1;
      global List<AxtriaARSnT__Siq_Workspace_O__c> uniqueIdOutbound = new List<AxtriaARSnT__Siq_Workspace_O__c>();
      public list<String> CountryList;
      public list<AxtriaARSnT__ClusterWorkspace__c> cluster {get;set;}
      public set<string>clustermrkt {get;set;}
      public  List<String> DeltaCountry ;


     global BatchOutboundWorkspaceUnManaged (String Country1){

        Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        flag=false;
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode = new map<String,String>();
        Uniqueset = new set<String>(); 
        cluster = new list<AxtriaARSnT__ClusterWorkspace__c>();
        clustermrkt = new set<string>();

        List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cluster =[select id,name from AxtriaARSnT__ClusterWorkspace__c];
        if(cluster!=null && cluster.size()>0)
        {
            for(AxtriaARSnT__ClusterWorkspace__c CL : cluster){
                clustermrkt.add(CL.name);
            }

        }
        


        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
                                                                                                                                     
         ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        //Till here..                                             
                                                                                      
           
        today=Date.today();
        System.debug('today++'+today);     
         
    
       query = 'SELECT AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Number_of_Scenarios__c,AxtriaSalesIQTM__Workspace_Description__c, ' +
               'AxtriaSalesIQTM__Workspace_End_Date__c,AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,AxtriaSalesIQTM__Workspace_Start_Date__c,Id,Name FROM AxtriaSalesIQTM__Workspace__c ' +
               'where AxtriaSalesIQTM__Workspace_Start_Date__c <=: today and AxtriaSalesIQTM__Workspace_End_Date__c >=: today and AxtriaSalesIQTM__Country__r.name IN :CountryList ' ;
                          
       
        
        System.debug('query'+ query);
        
            
    }


     global BatchOutboundWorkspaceUnManaged (){//set<String> Accountid
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode = new map<String,String>();
        Uniqueset = new set<String>(); 
        List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cluster = new list<AxtriaARSnT__ClusterWorkspace__c>();
        clustermrkt = new set<string>();
        cycleList=[Select Name,AxtriaARSnT__Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
        if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.AxtriaARSnT__Cycle__r.Name !=null && t1.AxtriaARSnT__Cycle__r.Name !='')
                    cycle = t1.AxtriaARSnT__Cycle__r.Name;
            }
            
        }
        cluster =[select id,name from AxtriaARSnT__ClusterWorkspace__c];
        if(cluster!=null && cluster.size()>0)
        {
            for(AxtriaARSnT__ClusterWorkspace__c CL : cluster){
                clustermrkt.add(CL.name);
            }

        }

        // String cycle=cycleList.get(0).Name;
         //cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c='Workspace' and AxtriaARSnT__Job_Status__c='Successful' Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].AxtriaARSnT__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);


        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
                                                                                                                                     
         ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        //Till here..                                             
                                                                                      
             
         
        //Last Bacth run ID
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        
        sJob.AxtriaARSnT__Job_Name__c = 'workspace';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
    sJob.AxtriaARSnT__Cycle__c=cycle;
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
    
        insert sJob;
        batchID = sJob.Id;
       
        recordsProcessed =0;

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceListUnManaged.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>'+DeltaCountry);

        today=Date.today();
        System.debug('today++'+today);
       query = 'SELECT AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Number_of_Scenarios__c,AxtriaSalesIQTM__Workspace_Description__c, ' +
               'AxtriaSalesIQTM__Workspace_End_Date__c,AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,AxtriaSalesIQTM__Workspace_Start_Date__c,Id,Name FROM AxtriaSalesIQTM__Workspace__c ' +
               'where AxtriaSalesIQTM__Workspace_Start_Date__c <=: today and AxtriaSalesIQTM__Workspace_End_Date__c >=: today and AxtriaSalesIQTM__Country__r.name IN :DeltaCountry ' ;
                          
       
        
        if(lastjobDate!=null){
            query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
        
            
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchOutboundWorkspaceUnManaged() );
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Workspace__c > records){
        // process each batch of records
      
        //**********************SAL -187(Added by Dhiren on 20/12/2018)****************************

        uniqueIdOutbound=[select id,AxtriaARSnT__Unique_Id__c from AxtriaARSnT__Siq_Workspace_O__c];
        Set<String> idOutbound = new set<String>();
        for(AxtriaARSnT__Siq_Workspace_O__c wo:uniqueIdOutbound)
        {
            String uid=String.valueOf(wo.AxtriaARSnT__Unique_Id__c);
           // uid=uid.substring(0,14);
            idOutbound.add(uid);
            system.debug('<<<<<<<Unique id in Siq_Outbound_c>>>>>'+idOutbound);
        }

        //********************************End of code by Dhiren************************************

        system.debug('=================clustermrkt::::'+clustermrkt);
        String code = ' ';
        List<AxtriaARSnT__Siq_Workspace_O__c> workspaces = new List<AxtriaARSnT__Siq_Workspace_O__c>();
        Map<String,AxtriaSalesIQTM__Workspace__c> existingWorkspace = new Map<String,AxtriaSalesIQTM__Workspace__c>(); //--------Added By Dhiren---------------------

        for (AxtriaSalesIQTM__Workspace__c workspace : records) {
            String key=workspace.Name;
            system.debug('<<<<<outbound>>>>>'+key+'    <<<<<siq_Outbound>>>>'+idOutbound);

           if(!idOutbound.contains(key)){
                if(!Uniqueset.contains(Key))
                {
                    if(clustermrkt == null || clustermrkt.size() == 0)
                    {
                        system.debug('=============NORMAL FLOW------------');
                        AxtriaARSnT__Siq_Workspace_O__c ws=new AxtriaARSnT__Siq_Workspace_O__c();
                        ws.AxtriaARSnT__Siq_Workspace_Country_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        code=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        ws.AxtriaARSnT__Siq_Workspace_End_Date__c =workspace.AxtriaSalesIQTM__Workspace_End_Date__c;
                        //ws.Siq_Workspace_Marketing_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c  ;    
                        //Added by Ayushi
                        if(mapVeeva2Mktcode.get(code) != null)
                        {
                            ws.AxtriaARSnT__Siq_Workspace_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                        }
                        else
                        {
                            ws.AxtriaARSnT__Siq_Workspace_Marketing_Code__c = 'MC code does not exist';
                        }
                        //Till here..
                        ws.AxtriaARSnT__Siq_Workspace_Name__c  =workspace.Name;
                        ws.AxtriaARSnT__Siq_Workspace_Start_Date__c= workspace.AxtriaSalesIQTM__Workspace_Start_Date__c ; 
                        ws.AxtriaARSnT__Unique_Id__c =key;
                        workspaces.add(ws);
                        system.debug('recordsProcessed+'+recordsProcessed);
                        recordsProcessed++;
                        Uniqueset.add(key);

                    }
                    else{
                        code=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        system.debug('===============Cluster market flow============='+clustermrkt);

                        if(!clustermrkt.contains(code)){
                            system.debug('===============country code is:::'+code);
                            AxtriaARSnT__Siq_Workspace_O__c ws=new AxtriaARSnT__Siq_Workspace_O__c();
                            ws.AxtriaARSnT__Siq_Workspace_Country_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                            code=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                            ws.AxtriaARSnT__Siq_Workspace_End_Date__c =workspace.AxtriaSalesIQTM__Workspace_End_Date__c;
                            //ws.Siq_Workspace_Marketing_Code__c=workspace.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c  ;    
                            //Added by Ayushi
                            if(mapVeeva2Mktcode.get(code) != null)
                            {
                                ws.AxtriaARSnT__Siq_Workspace_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                            }
                            else
                            {
                                ws.AxtriaARSnT__Siq_Workspace_Marketing_Code__c = 'MC code does not exist';
                            }
                            //Till here..
                            ws.AxtriaARSnT__Siq_Workspace_Name__c  =workspace.Name;
                            ws.AxtriaARSnT__Siq_Workspace_Start_Date__c= workspace.AxtriaSalesIQTM__Workspace_Start_Date__c ; 
                            ws.AxtriaARSnT__Unique_Id__c =key;
                            workspaces.add(ws);
                            system.debug('recordsProcessed+'+recordsProcessed);
                            recordsProcessed++;
                            Uniqueset.add(key);


                        }

                    }
                    
                //comments
               }
            }

            //----------------------------------Added by Dhiren nov release---------------------------------------------

            else
            {
                existingWorkspace.put(key, workspace);
            }
        }

          

     
        upsert workspaces AxtriaARSnT__Unique_Id__c;

          //----------------------------------Added by Dhiren nov release---------------------------------------------

        
        List<AxtriaARSnT__Siq_Workspace_O__c> workspaceoutbound = new List<AxtriaARSnT__Siq_Workspace_O__c>();

        for(AxtriaARSnT__Siq_Workspace_O__c wrkspc : [Select AxtriaARSnT__Siq_Workspace_Country_Code__c,AxtriaARSnT__Siq_Workspace_End_Date__c,AxtriaARSnT__Siq_Workspace_Start_Date__c,AxtriaARSnT__Unique_Id__c,AxtriaARSnT__Siq_Workspace_Marketing_Code__c from AxtriaARSnT__Siq_Workspace_O__c where AxtriaARSnT__Unique_Id__c IN: existingWorkspace.keySet()])
        {
            String Key = wrkspc.AxtriaARSnT__Unique_Id__c;

            AxtriaSalesIQTM__Workspace__c wrkspcRec = existingWorkspace.get(Key);

            if(wrkspc.AxtriaARSnT__Siq_Workspace_End_Date__c != wrkspcRec.AxtriaSalesIQTM__Workspace_End_Date__c)
            {
                wrkspc.AxtriaARSnT__Siq_Workspace_End_Date__c = wrkspcRec.AxtriaSalesIQTM__Workspace_End_Date__c;
                workspaceoutbound.add(wrkspc);
            }
            if(wrkspc.AxtriaARSnT__Siq_Workspace_Start_Date__c != wrkspcRec.AxtriaSalesIQTM__Workspace_Start_Date__c)
            {
                wrkspc.AxtriaARSnT__Siq_Workspace_Start_Date__c = wrkspcRec.AxtriaSalesIQTM__Workspace_Start_Date__c;
                workspaceoutbound.add(wrkspc);
            }

        }

        upsert workspaceoutbound AxtriaARSnT__Unique_Id__c;


       
      
        
    }    
    global void finish(Database.BatchableContext bc){
       if(flag){
         // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
                 sjob.AxtriaARSnT__Object_Name__c = 'Workspace';
                //sjob.Changes__c        
                                         
                sJob.AxtriaARSnT__Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
         //Database.ExecuteBatch(new BatchOutBoundEmployee(),200); 
        
       }
    }
    public void dummyFunction()
    {
      Integer i = 0;
      i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
   }   
}