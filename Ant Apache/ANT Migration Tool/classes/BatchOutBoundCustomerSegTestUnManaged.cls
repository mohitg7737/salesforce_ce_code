@isTest
private class BatchOutBoundCustomerSegTestUnManaged {
   @testSetup 
    static void setup() {

        /*Deprecated due to Object purge activity on Jan 13th, 2020*/
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        List<AxtriaARSnT__SIQ_Customer_Segment_O__c> cs = new List<AxtriaARSnT__SIQ_Customer_Segment_O__c>();
          // insert 10 accounts
        Account ac=new Account();
        ac.Name='xyz';
        ac.AxtriaARSnT__Marketing_Code__c='US';
        insert ac;
        String acId=ac.id;
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;
        
        Schema.DescribeFieldResult Pickvalue= AxtriaSalesIQTM__Position__c.AxtriaARSnT__Sales_Team_Attribute_MS__c.getDescribe();
       List<Schema.PicklistEntry> PickListValue = Pickvalue.getPicklistValues();
         AxtriaSalesIQTM__Position__c pos1=new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        Pos1.AxtriaSalesIQTM__Description__c= 'XYZ';
        Pos1.AxtriaARSnT__Position_Description__c= 'XYA';
        Pos1.AxtriaARSnT__Sales_Team_Attribute_MS__c = String.valueof(PickListValue[0].getValue());
        Pos1.AxtriaARSnT__Channel_Id_AZ__c= 'MR';
        Pos1.AxtriaARSnT__Channel_AZ__c= 'MR';
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        insert pos1;
        String parentposId=pos1.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Parent_Position__c=parentposId;
        Pos.AxtriaSalesIQTM__Description__c= 'XYZ';
        Pos.AxtriaARSnT__Position_Description__c= 'XYA';
        Pos.AxtriaARSnT__Sales_Team_Attribute_MS__c = String.valueof(PickListValue[0].getValue());
        Pos.AxtriaARSnT__Channel_Id_AZ__c= 'MR';
        Pos.AxtriaARSnT__Channel_AZ__c= 'MR';
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        insert pos;
        String posId=pos.id;
        pacp.add(new AxtriaSalesIQTM__Position_Account_Call_Plan__c(AxtriaSalesIQTM__Account__c=acId,AxtriaSalesIQTM__Team_Instance__c=teamInstId,
                                                                   AxtriaSalesIQTM__isAccountTarget__c=true,AxtriaSalesIQTM__isincludedCallPlan__c = true,
                                                                   AxtriaSalesIQTM__Position__c=posId));
        
        insert pacp;
       
    }
    static testmethod void test() {     
        /*Deprecated due to Object purge activity on Jan 13th, 2020*/

        Test.startTest();
        BatchOutBoundCustomerSegmentUnManaged usa = new BatchOutBoundCustomerSegmentUnManaged();
        Id batchId = Database.executeBatch(usa);
        usa.dummyFunction();

      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();   /*Deprecated due to Object purge activity on Jan 13th, 2020*/

        // after the testing stops, assert records were updated 
    }
}