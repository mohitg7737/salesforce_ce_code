@isTest
private class BatchOutboundWorkspaceUnManagedTest {
     @testSetup 
    static void setup() {
 
        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;
        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active'));
        insert country;
        Date dt=Date.today();
        AxtriaSalesIQTM__TriggerContol__c aaa=new AxtriaSalesIQTM__TriggerContol__c(Name='OverlappingWorkspace',AxtriaSalesIQTM__IsStopTrigger__c=false);
        insert aaa;
        
        if(!AxtriaSalesIQTM__TriggerContol__c.getValues('OverlappingWorkspace').AxtriaSalesIQTM__IsStopTrigger__c)
           {

     AxtriaSalesIQTM__Workspace__c ws=new AxtriaSalesIQTM__Workspace__c();
        ws.AxtriaSalesIQTM__Workspace_Description__c='xyz';
        ws.AxtriaSalesIQTM__Workspace_Start_Date__c=dt.addDays(-1);
        ws.AxtriaSalesIQTM__Workspace_End_Date__c=dt.addDays(5);
        System.debug('today'+dt);
        System.Debug('today++'+dt.addDays(-1));
        System.Debug('today++++'+dt.addMonths(1));
        
        ws.Name='xyz';
        insert ws;
        }
    }
    static testmethod void test() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchOutboundWorkspaceUnManaged usa = new BatchOutboundWorkspaceUnManaged();
        Id batchId = Database.executeBatch(usa);
        usa.dummyFunction();
        
        Test.stopTest();

    }
}