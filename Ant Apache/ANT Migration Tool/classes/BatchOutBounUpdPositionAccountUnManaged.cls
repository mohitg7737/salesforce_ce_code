global class BatchOutBounUpdPositionAccountUnManaged implements Database.Batchable<sObject>, Database.Stateful,schedulable{
   public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     global Date today=Date.today();
     public String UnAsgnPos='00000';
     public String UnAsgnPos1='0';
     public List<String> posCodeList=new List<String>();
     public map<String,String>Countrymap {get;set;}
     public map<String,String>mapVeeva2Mktcode {get;set;}
     public set<String> Uniqueset {get;set;}  
      public set<String> mkt {get;set;} 
      public list<AxtriaARSnT__Custom_Scheduler__c> mktList {get;set;}  
      public list<AxtriaARSnT__Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
      public String cycle {get;set;}
      global boolean flag=true;
      global String country_1;
      public list<String> CountryList;
      public  List<String> DeltaCountry ;

   



  global BatchOutBounUpdPositionAccountUnManaged (String Country1,String Differentiator){//set<String> Accountid
        
        flag=false;
        country_1=Country1;// 'Differentiator' is a purposeless variable used for constructor overloading 
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
       Countrymap = new map<String,String>();
        Uniqueset = new set<String>(); 
        mapVeeva2Mktcode= new map<String,String>();
        lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();

     
     

    for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap1: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap1.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }
    
      
     query = 'Select id, AxtriaARSnT__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,Event__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, ' +
     'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, ' +
      'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Account__r.AxtriaARSnT__External_Account_Id__c, AxtriaSalesIQTM__Account__r.Type ' +
      'FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c IN :CountryList AND AxtriaSalesIQTM__Assignment_Status__c = \'Active\' ' ;
    
  

    System.debug('query'+ query);
         
  }


      global BatchOutBounUpdPositionAccountUnManaged (String newteaminstanceid){
        string teaminstncid = newteaminstanceid;
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode= new map<String,String>();
        Uniqueset = new set<String>(); 
        lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();
        mkt = new set<String>();
        mktList=new list<AxtriaARSnT__Custom_Scheduler__c>();
        mktList=AxtriaARSnT__Custom_Scheduler__c.getall().values();
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }
        query = 'Select id, AxtriaARSnT__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,Event__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, ' +
              'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, ' +
              'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Account__r.AxtriaARSnT__External_Account_Id__c, AxtriaSalesIQTM__Account__r.Type ' +
              'FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__c =:teaminstncid' ;
                  //if(mkt.size()>0){
                    //query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
                  //}
          System.debug(' HEy Siva Your Query is:::::::'+ query);
     
      

      }


     global BatchOutBounUpdPositionAccountUnManaged (){//set<String> Accountid
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
       Countrymap = new map<String,String>();
        Uniqueset = new set<String>(); 
        mapVeeva2Mktcode= new map<String,String>();
        lsCustomSchedulerUpdate = new list<AxtriaARSnT__Custom_Scheduler__c>();

      mkt = new set<String>();
      mktList=new list<AxtriaARSnT__Custom_Scheduler__c>();
      mktList=AxtriaARSnT__Custom_Scheduler__c.getall().values();
      for(AxtriaARSnT__Custom_Scheduler__c cs:mktList){
        if(cs.AxtriaARSnT__Status__c==true && cs.AxtriaARSnT__Schedule_date__c!=null){
           if(cs.AxtriaARSnT__Schedule_date__c>today.addDays(1)){
              mkt.add(cs.AxtriaARSnT__Marketing_Code__c);
            }
            else{
                    //update Custom scheduler record
                   cs.AxtriaARSnT__Status__c = False;
                   lsCustomSchedulerUpdate.add(cs);
                }
        }
      }
      
        List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
         List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
          cycleList=[Select Name,AxtriaARSnT__Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
         if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.AxtriaARSnT__Cycle__r.Name !=null && t1.AxtriaARSnT__Cycle__r.Name !='')
                    cycle = t1.AxtriaARSnT__Cycle__r.Name;
            }
            
        }
         //String cycle=cycleList.get(0).Name;
         //cycle=cycle.substring(cycle.length() - 3);
         System.debug(cycle);
        schLogList=[Select Id,CreatedDate,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c='Position Account' and AxtriaARSnT__Job_Status__c='Successful' Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].AxtriaARSnT__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
      else{
        lastjobDate=null;       //else we set the lastjobDate to null
      }
      System.debug('last job'+lastjobDate);
        //Last Bacth run ID

    for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap1: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap1.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }
        system.debug('=============mapVeeva2Mktcode:::'+mapVeeva2Mktcode);
        //Till here..

    AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
    
    sJob.AxtriaARSnT__Job_Name__c = 'Position Account';
    sJob.AxtriaARSnT__Job_Status__c = 'Failed';
    sJob.AxtriaARSnT__Job_Type__c='Outbound';
    if(cycle!=null && cycle!='')
    sJob.AxtriaARSnT__Cycle__c=cycle;
    sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
  
    insert sJob;
      batchID = sJob.Id;
     
      recordsProcessed =0;

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceListUnManaged.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>'+DeltaCountry);

     query = 'Select id, AxtriaARSnT__Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaARSnT__Event__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, ' +
     'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c, ' +
      'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp,AxtriaSalesIQTM__Account__r.AxtriaARSnT__External_Account_Id__c, AxtriaSalesIQTM__Account__r.Type ' +
      'FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c IN :DeltaCountry ' ;
    
  
        if(lastjobDate!=null){
          query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
   
          
   
    if(mkt.size()>0){
        query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
    }
    System.debug('query'+ query);
       //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }        
    }
    
    
    
   global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchOutBounUpdPositionAccountUnManaged(),2000);                                                           
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Account__c> records){
        // process each batch of records
       Uniqueset = new set<String>();

        List<AxtriaARSnT__SIQ_Position_Account_O__c> positionAccounts = new List<AxtriaARSnT__SIQ_Position_Account_O__c>();
        String code = ' ';
        for (AxtriaSalesIQTM__Position_Account__c position : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
            //String key = position.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Effective_End_Date__c;
            String key = position.id;
            if(!Uniqueset.contains(Key)){
               if(position.AxtriaSalesIQTM__Effective_End_Date__c >=position.AxtriaSalesIQTM__Effective_Start_Date__c)
               {
                  AxtriaARSnT__SIQ_Position_Account_O__c positionAccount=new AxtriaARSnT__SIQ_Position_Account_O__c();

                  // positionAccount.SIQ_Marketing_Code__c=countrymap.get(position.AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c);
                  //positionAccount.SIQ_Marketing_Code__c=position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
                  //positionAccount.SIQ_Country_Code__c=countrymap.get(position.AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Country_Name__c);
                  positionAccount.AxtriaARSnT__SIQ_Country_Code__c=position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                  code =position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                  //Added by Ayushi
                  if(mapVeeva2Mktcode.get(code) != null){
                     positionAccount.AxtriaARSnT__SIQ_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                  }
                  else{
                     positionAccount.AxtriaARSnT__SIQ_Marketing_Code__c = 'MC code does not exist';
                  }
                  //Till here..
                  positionAccount.AxtriaARSnT__SIQ_Event__c=position.AxtriaARSnT__Event__c;
                  positionAccount.AxtriaARSnT__SIQ_Salesforce_Name__c=position.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
                  positionAccount.AxtriaARSnT__SIQ_CUSTOMER_ID__c=position.AxtriaSalesIQTM__Account__r.AccountNumber;
                  positionAccount.AxtriaARSnT__SIQ_Customer_Class__c= position.AxtriaSalesIQTM__Account__r.Type;
                  positionAccount.AxtriaARSnT__SIQ_Team_Instance__c=position.AxtriaSalesIQTM__Team_Instance__r.name;
                  positionAccount.Name=position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Account__r.AxtriaARSnT__External_Account_Id__c;
                  positionAccount.AxtriaARSnT__SIQ_Created_Date__c=position.CreatedDate;
                  positionAccount.AxtriaARSnT__SIQ_Updated_Date__c=position.LastModifiedDate;
                  positionAccount.AxtriaARSnT__SIQ_POSITION_CODE__c=position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                  positionAccount.AxtriaARSnT__SIQ_EFFECTIVE_START_DATE__c=position.AxtriaSalesIQTM__Effective_Start_Date__c;
              
                  positionAccount.AxtriaARSnT__SIQ_EFFECTIVE_END_DATE__c=position.AxtriaSalesIQTM__Effective_End_Date__c;
                  //positionAccount.Unique_Id__c=position.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+position.AxtriaSalesIQTM__Effective_End_Date__c;                                            
                  positionAccount.AxtriaARSnT__Unique_Id__c=position.id;
                  positionAccounts.add(positionAccount);
                  system.debug('recordsProcessed+'+recordsProcessed);
                  recordsProcessed++;
                  Uniqueset.add(Key);    
               }
            }
        }
        upsert positionAccounts AxtriaARSnT__Unique_Id__c;
       
    }    
    global void finish(Database.BatchableContext bc){
       if(flag){
         // execute any post-processing operations
       
      System.debug(recordsProcessed + ' records processed. ');
      AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
      system.debug('schedulerObj++++before'+sJob);
      //Update the scheduler log with successful
      sjob.AxtriaARSnT__Object_Name__c = 'Position Account';
      //sjob.Changes__c
      sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
      sJob.AxtriaARSnT__Job_Status__c='Successful';
      system.debug('sJob++++++++'+sJob);
      update sJob;
      //Database.ExecuteBatch(new BatchOutBoundUpdPosGeography(),200);
      Set<String> updMkt = new set<String>();
      for(AxtriaARSnT__Custom_Scheduler__c cs:mktList){
        if(cs.AxtriaARSnT__Status__c==true && cs.AxtriaARSnT__Schedule_date__c!=null){
          if(cs.AxtriaARSnT__Schedule_date__c==today.addDays(2)){
            updMkt.add(cs.AxtriaARSnT__Marketing_Code__c);
          }
        }
      }
      if(updMkt.size()>0){
        List<AxtriaSalesIQTM__Position_Account__c> posAccList=[Select Id FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
        update posAccList;
      }
    }
  }  
  public void dummyFunction()
    {
      Integer i = 0;
      i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
  }   
}